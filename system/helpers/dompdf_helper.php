<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function pdf_create($html, $filename='', $stream=TRUE) 
{
    require_once("dompdf/dompdf_config.inc.php");

    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    
    
    $canvas = $dompdf->get_canvas();
    $canvas->page_text(500, 750, "Page: {PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));//distance from left:500, top is 600
    
    
    
    if ($stream) {
        $dompdf->stream($filename.".pdf");
    } else {
        return $dompdf->output();
    }
}
?>
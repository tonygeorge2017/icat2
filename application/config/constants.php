<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


//Constant created by Autograde

define('REQUIRED',		 'Y');
define('NOT_REQUIRED',	 'N');
define('ACTIVE', 		 '1');
define('NOT_ACTIVE', 	 '0');
define('PRIMARY',		 '1');
define('NOT_PRIMARY',	 '0');
define('ROW_PER_PAGE',	 10); // for pagination
define('URI_SEGMENT_FOR_FOUR',	 4); // to pass four perameters in the URL
define('URI_SEGMENT_FOR_FIVE',	 5); // to pass four perameters in the URL
define('AUTOGRADE_USER', '1');
define('INDIVIDUAL_CLIENT', '2');
define('OHTER_CLIENT_USER', '3');
define('DISTRIBUTOR_USER', '4');
define('DEALER_USER', '5');
define('PARENT_USER', '6');
define('INDIVIDUAL_DRIVER_GP', '2');
define('INDIVIDUAL_VEHICLE_GP', '2');
define('MAIL_LOG_PATH', "E:/MAIL_LOGS/");
define('NEW_DEVICE_UPTO_TIME_DIFF', 30); //the difference between the chk_new_device_last_datetime and chk_new_device_first_datetime in vts_check_new_device is less than this, then consider as new device.
define('DEVICE_ALERT_TIME_DIFF', 10);// the difference between the current local datetime and last datetime in vts_check_new_device is exceed this, then consider as gps data not receives properly.
define('SPEED_VIO_LIMIT_VALUE','5');
define('SPEED_VIO_DATE_DIFF','10 days');
//constants for mail
define('CC_TO','gapp@autograde.in');
define('BCC_TO','gapp@autograde.in');
define('REPLY_TO','mail@autograde.in');
//constants used for the validation rules
define('MIN_FIELD_LENGTH', 5);
define('MAX_FIELD_LENGTH', 100);



//VTS Screen list and screen id.
define('FORGOT_PASSWORD', 1);
define('LOGIN', 2);
define('CHANGE_PASSWORD', 3);
define('HOME', 4);
define('VEHICLE_TRACKING', 5);
define('INSURANCE_AGENCY', 6);
define('VEHICLE_GROUP', 7);
define('VEHICLE', 8);
define('VEHICLE_SERVICE', 9);
define('DRIVER_GROUP', 10);
define('DRIVER', 11);
define('DEVICE_INSTALLATION', 12);
define('TRIP_REGISTER', 13);
define('USERS', 14);
define('VEHICLE_STOPS', 15);
define('PARENT_DETAILS', 16);
define('DISTRIBUTOR', 17);
define('DEALER', 18);
define('DEVICE_TYPE', 19);
define('DEVICE', 20);
define('DEVICE_ALLOCATION_TO_DEALER', 21);
define('DEVICE_ALLOCATION_TO_CLIENT', 22);
define('CLIENT', 23);
define('CLIENT_LICENSE', 24);
define('SERVICE', 25);
define('SETTINGS', 26);
define('INDIVIDUAL_USER', 27);
define('PERSONAL_TRACKING', 28);
define('DEVICE_MONITORING', 29);
define('VEHICLE_REPORT ', 30);
define('SPEED_VIOLATIONS', 31);
define('TRACKING_DATA', 32);
define('DEVICE_LIST', 33);
define('CONTACT_US', 34);
define('PARENT_VEHICLE_STOP_MANAGEMENT', 35);
define('VEHICLE_ROUTE', 51);
define('VEHICLE_ROUTE_MANAGEMENT', 52);


/* Client Access Permissions*/
define('ACCESSPERMISSIONS', 'AccessPermissions');

/* vehicle screen */
define('FUELTYPE', 'FuelType');		// Fuel type
define('VOLTAGETYPE', 'VoltageType');	//Voltage type

/* Fuel Sensor controller */
define('VOLTAGE_REF', 3.2);
define('FRAME_VAL', 4096);
define('SCALING_FACTOR_B', 3.7);
define('SCALING_FACTOR_A', 7.5);


define('MAP_API_KEY','AIzaSyBJBfH56jBVrSYnXcddys5RnlV5PVh7KgQ');
/*Geo-Fence & Route Limitation*/
define('MAX_NO_OF_GEO_FENCE_PER_VEHICLE', 5);
define('MAX_NO_OF_ROUTE_PER_VEHICLE', 5);
/* End of file constants.php */
/* Location: ./application/config/constants.php */
<script src="http://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>"></script>
<script src="<?php echo base_url('assets/js/markerclusterer.js')?>"></script>
<script type="text/javascript">
var map;
var startMarke;
var image_url="<?php echo base_url('assets/images/')?>";
var arrayMarker=[];
var rfIDMarker=[];
var markerCluster =null;
var startMarkeInfo = null;
var infowindow=[];
var rfIDinfowindow=[];
var zoomLevel=15;
var monitor_vehicle_id=0;
var move = false;
var rfid_swipe_icon = {
          url: "<?php echo base_url('assets/images')?>/swipe.png",
          // This marker is 20 pixels wide by 32 pixels high.
          size: new google.maps.Size(25, 25),
          // The origin for this image is (0, 0).
          origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          anchor: new google.maps.Point(0, 25)
        };
var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:2,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
		  };

var car ="M17.402,0H5.643C2.526,0,0,3.467,0,6.584v46.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";

car_marker = {
		path: car,
		scale: 0.6,
		strokeColor: '#807776',
		strokeWeight: .9,
		fillOpacity: 1,
		fillColor: '#3D9B55',
		offset: '5%',
		rotation : parseInt(0),
		//fixedRotation : true,
		anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
	};
	
var car_rotate_adj=	parseInt(0) * -1;

$(function(){
	var urls=$('#URL').val();	
	var alertSount = new Audio(urls+'/sounds/alertS1.mp3');					  
	map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	google.maps.event.addListener(map, 'zoom_changed', function() {
				 zoomLevel=map.getZoom();				 
			});
	startMarke=new google.maps.Marker({map:map});
	var timers;	
	
	timers=setInterval(dashboard_update,10000);
	dashboard_update();	

	function dashboard_update()
	{
		vehicle_status();
	}	

	function vehicle_status()
	{	
		var clr='#333';
		$.ajax({
			type:'GET',
			url: 'parent_dashboard_ctrl/dashboard/',
			dataType: 'json',				
			success: function(result) {						
				$('#Grid tbody').html('');
				$('#GridLog tbody').html('');
				$('#GridPickup tbody').html('');
				$('#GridSpeed tbody').html('');
				
				$('#alerts').html(result['pickup_alert'].length + result['speed_alert'].length);
				$('#SpeedAlert').html(result['speed_alert'].length);
				$('#RFIDAlert').html(result['pickup_alert'].length);
				
				if(result['crnt'].length>0)
				{
					$.each(result['crnt'], function(k,v){						
						if(parseInt(v["diff"]) >= parseInt(v["lost"]))
						{							
							clr='#286090';
						}
						else if(v["ig"]=='N')
						{							
							clr='#c9302c';
						}
						else if(v["ig"]=='Y' && (parseFloat(v['sp'])*1.852) > parseFloat(v['haltspeed']))
						{							
							clr='#5cb85c';
						}
						else if(v["ig"]=='Y' && (parseFloat(v['sp'])*1.852) <= parseFloat(v['haltspeed']))
						{							
							clr='#f0ad4e';
						}
						$('#Grid tbody').append('<tr><td>'+v['st_name']+'</td><td>'+v['vh_reg']+'</td><td>'+v['rec']+'</td><td><i style="color:'+clr+'" title="'+v['vh_reg']+'" class="fa fa-map-marker fa-2x cur" aria-hidden="true" onclick="show_marker({lat:'+parseFloat(v['lat'])+',lng:'+parseFloat(v['lng'])+', vhid:'+v['vhid']+', vh:\''+v["vh_reg"]+'\', clr:\''+clr+'\', ang:'+parseFloat(v['ang'])+'})"></i></td></tr>');
						if(monitor_vehicle_id==v['vhid'])
							move_marker({lat:parseFloat(v['lat']),lng:parseFloat(v['lng'])}, clr, parseFloat(v['ang']));
						
						if(arrayMarker.length!=result['crnt'].length)
						{        			
							arrayMarker[k]=new google.maps.Marker({ position: {lat: parseFloat(v['s_lat']), lng: parseFloat(v['s_lng'])}, title: 'Stop Name : '+v['s_name'], map: map});
							infowindow[k] = new google.maps.InfoWindow({ content:'<div id="content"><h3 id="firstHeading" class="firstHeading">Stop Detail of '+v['st_name']+'</h3><div id="bodyContent"><p><b>Stop Name : </b>'+v['s_name']+'</p></div></div>'});
							
							arrayMarker[k].addListener('click', function() {
								infowindow[k].open(map, arrayMarker[k]);
								zoom_check(15);
								map.panTo(arrayMarker[k].getPosition());
								move = false;
							});
						}
						else
						{					
							arrayMarker[k].setPosition({lat: parseFloat(v['s_lat']), lng: parseFloat(v['s_lng'])});
							arrayMarker[k].setTitle('Stop Name : '+v['s_name']);
							infowindow[k].setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">Stop Detail of '+v['st_name']+'</h3><div id="bodyContent"><p><b>Stop Name : </b>'+v['s_name']+'</p></div></div>');
						}						
					});						
					/*
					if(markerCluster)
						markerCluster.clearMarkers();
					markerCluster = new MarkerClusterer(map, arrayMarker,{imagePath: image_url+"/m"});
					*/
				}else{
					$('#Grid tbody').append('<tr><td colspan="4" style="text-align:center;">-- No Data --</td></tr>');
				}
				/*check for recent 10 details of the pickup and drop*/
				/*"hist":[{"st":"1","dr":null,"tm":"09-11-2017  11:05:19","vh":null,"vhn":null,"drname":null,"drmobile":null,"stname":"Ramesh G","stclass":"X - B","swip_lat":"10.0100833333333","swip_lng":"76.3039833333333"}]*/
				if(result['hist'].length>0)
				{
					$.each(result['hist'], function(k,v){	
						$('#GridLog tbody').append('<tr><td>'+(k+1)+'</td><td>'+v['stname']+'</td><td>'+v['vhn']+'</td><td>'+v['drname']+'</td><td>'+v['drmobile']+'</td><td>'+v['tm']+'</td></tr>');
						if(rfIDMarker.length!=result['hist'].length)
						{        			
							rfIDMarker[k]=new google.maps.Marker({ 
								position: {lat: parseFloat(v['swip_lat']), lng: parseFloat(v['swip_lng'])}, 
								title: 'RF ID Swipe by '+v['stname'],
								icon: rfid_swipe_icon, 
								label: {
									text: ''+(k+1)+'',
									color: 'blue',
									fontSize: "10px" 
									},
								map: map
							});
							rfIDinfowindow[k] = new google.maps.InfoWindow({ content:'<div id="content"><h3 id="firstHeading" class="firstHeading">RF ID Swipe Detail of '+v['stname']+'</h3><div id="bodyContent"><p><b>Timestamp : </b>'+v['tm']+'<br><b>vehicle : </b>'+v['vhn']+'<br><b>Driver : </b>'+v['drname']+'<br><b>Mobile : </b>'+v['drmobile']+'</p></div></div>'});
							
							rfIDMarker[k].addListener('click', function() {
								rfIDinfowindow[k].open(map, rfIDMarker[k]);
								zoom_check(15);
								map.panTo(rfIDMarker[k].getPosition());
								move = false;
							});
							if(result['hist'].length == k+1)
							{
								rfIDMarker[k].setAnimation(google.maps.Animation.DROP);
								rfIDinfowindow[k].open(map, rfIDMarker[k]);
								zoom_check(15);
								map.panTo(rfIDMarker[k].getPosition());
							}
						}
						/*else
						{					
							rfIDMarker[k].setPosition({lat: parseFloat(v['swip_lat']), lng: parseFloat(v['swip_lng'])});
							rfIDMarker[k].setTitle('RF ID Swipe by '+v['s_name']);
							rfIDinfowindow[k].setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">RF ID Swipe Detail of '+v['stname']+'</h3><div id="bodyContent"><p><b>Timestamp : </b>'+v['tm']+'<br><b>vehicle : </b>'+v['vhn']+'<br><b>Driver : </b>'+v['drname']+'<br><b>Mobile : </b>'+v['drmobile']+'</p></div></div>');
							rfIDMarker[k].setAnimation(google.maps.Animation.DROP);
						}*/
					});
				}
				else{
					$('#GridLog tbody').append('<tr><td colspan="6" style="text-align:center;">-- No Data --</td></tr>');
				}
				
				/*check for recent 10 details of the pickup and drop Alert*/
				if(result['pickup_alert'].length>0)
				{
					$.each(result['pickup_alert'], function(k,v){	
						$('#GridPickup tbody').append('<tr><td>'+v['stname']+'</td><td>'+v['vhn']+'</td><td>'+v['drname']+'</td><td>'+v['drmobile']+'</td><td>'+v['tm']+'</td></tr>');
					});
				}else{
					$('#GridPickup tbody').append('<tr><td colspan="5" style="text-align:center;">-- No Data --</td></tr>');
				}
				
				/*check for recent 20 details of the over speed alert*/
				if(result['speed_alert'].length>0)
				{
					$.each(result['speed_alert'], function(k,v){	
						$('#GridSpeed tbody').append('<tr><td>'+v['stname']+'</td><td>'+v['drname']+'</td><td>'+v['drmobile']+'</td><td>'+v['tm']+'</td><td>'+v['msg']+'</td></tr>');
					});
				}else{
					$('#GridSpeed tbody').append('<tr><td colspan="5" style="text-align:center;">-- No Data --</td></tr>');
				}
			}
		});
	}

	$('#go-to-top').click(function(){ $("body").animate({ scrollTop: 100}, 600);});
		
	$('#update_config_btn').on('click',function(){
		var nrfid = ($('#nrfid').is(':checked')) ? '1' : '0';
		var nroute = ($('#nroute').is(':checked')) ? '1' : '0';
		var nspeed = ($('#nspeed').is(':checked')) ? '1' : '0';		
		
		$.getJSON("<?php echo current_url().'/update/' ?>", {'nrfid':nrfid, 'nroute':nroute, 'nspeed': nspeed}, function(data){			
			alert(data['msg']);
		});
		
	});

});
function show_marker(crt)
{	
	car_marker.rotation=crt.ang + car_rotate_adj;
	car_marker.fillColor=crt.clr;
	startMarke.setIcon(car_marker);
	startMarke.setPosition({lat:crt.lat, lng:crt.lng});
	startMarke.setTitle(crt.vh);
	monitor_vehicle_id=crt.vhid;
	startMarke.setAnimation(google.maps.Animation.DROP);
	//startMarke.setAnimation(google.maps.Animation.BOUNCE);
	zoom_check();
	move = true;
	map.panTo(crt);
	if(! startMarkeInfo)
	{
		startMarkeInfo = new google.maps.InfoWindow({ content:'<div id="content"><h3 id="firstHeading" class="firstHeading">'+  crt.vh +'</h3></div>'});
	}
	
	startMarke.addListener('click', function() {
								startMarkeInfo.open(map, startMarke);
								zoom_check(15);
								map.panTo(startMarke.getPosition());
								move = true;
							});
	startMarkeInfo.setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">'+  crt.vh +'</h3></div>');
	startMarkeInfo.setPosition({lat:crt.lat, lng:crt.lng});
	startMarkeInfo.open(map, startMarke);
}

function move_marker(loc, clr, ang)
{
	car_marker.rotation=ang + car_rotate_adj;
	car_marker.fillColor=clr;
	startMarke.setIcon(car_marker);
	startMarke.setPosition(loc);
	startMarkeInfo.setPosition(loc);
	if(move)
		map.panTo(loc);
}

function zoom_check(zoom_level=12)
{
	if(zoomLevel < zoom_level)
		zoomLevel=zoom_level;
	
	map.setZoom(zoomLevel);
}
</script>

<style type="text/css">
	.cur{
		cursor: pointer
	}
	table {
		width:100%;
	}
	td{		
		padding-left: 5px;
		padding-right: 5px
	}
	#update_config_btn{
		margin-top: 5px;		
	}
</style>
<input type="Text" style="display : none;" id="URL" value="<?php echo base_url('assets/')?>"/>
<div class="container">	
	<!-- RFID Log Model -->
	<div class="modal fade" id="RFID_Model" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Pickup &amp; Drop Alert Details</h4>
				</div>
				<div class="modal-body">
					<p Style="color:red"> <b>* Note : </b> Recent 10 pickup &amp; drop details. </p>
					<table id="GridPickup" border="1" >
						<thead>
							<tr>
								<th style="text-align: center;">Student</th>
								<th style="text-align: center;">Vehicle</th>
								<th style="text-align: center;">Driver</th>
								<th style="text-align: center;">Mobile</th>								
								<th style="text-align: center;">Date &amp; Time</th>					
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" style="text-align:center;">-- No Data --</td>
							</tr>
	    				</tbody>										
					</table>
				</div>				
			</div>
		</div>
	</div>
	
		<!-- Speed Model -->
	<div class="modal fade" id="Speed_Model" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Over Speed Alert Details</h4>
				</div>
				<div class="modal-body">
					<p Style="color:red"> <b>* Note : </b> Recent 20 over speed alert details.</p>
					<table id="GridSpeed" border="1" >
						<thead>
							<tr>								
								<th style="text-align: center;">Student</th>
								<th style="text-align: center;">Driver</th>	
								<th style="text-align: center;">Mobile</th>								
								<th style="text-align: center;">Date &amp; Time</th>					
								<th style="text-align: center;">Notification</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" style="text-align:center;">-- No Data --</td>
							</tr>
	    				</tbody>										
					</table>
				</div>				
			</div>
		</div>
	</div>
	
			<!-- Speed Model -->
	<div class="modal fade" id="Alert_Config_Model" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Alert Configuration</h4>
				</div>
				<div class="modal-body">					
					<table border="1" >
						<thead>
							<tr>
								<th style="text-align: center;">Alert</th>
								<th style="text-align: center;">Config</th>								
							</tr>
						</thead>
						<tbody>
							<?php if(count($alertConfig) > 0): ?>
							<tr>
								<td> RF ID Notification </td>
								<td><input type="checkbox" id="nrfid" value='1' <?php echo (($alertConfig[0]['fcm_alert_rfid'])?'checked':''); ?> /></td>
							</tr>
							<tr>
								<td> Route Violation Notification </td>
								<td><input type="checkbox" id="nroute" value='1' <?php echo (($alertConfig[0]['fcm_alert_route'])?'checked':''); ?> /></td>
							</tr>
							<tr>
								<td> Over-Speed Notification </td>
								<td><input type="checkbox" id="nspeed" value='1' <?php echo (($alertConfig[0]['fcm_alert_speed'])?'checked':''); ?> /></td>
							</tr>
							<?php else: ?>
							<tr>
								<td colspan='2'>--No Config --</td>								
							</tr>
							<?php endif;?>
	    				</tbody>										
					</table>	
					<div>
						<button id="update_config_btn">Update</button>					
					</div>
				</div>				
			</div>
		</div>
	</div>
	
	<div class="row">
		<!-- For Alert detail-->
		<div class="col-md-6 col-xs-12" style=" overflow: auto; padding: 15px; border-radius:10px; height:400px; background-color:#ECECEC">
			<div class="row" style="margin: 0px; background-color: #3b318f;">
				<div class="col-md-2 col-xs-3">
					<a title="Alert Configuration" data-toggle="modal" data-target="#Alert_Config_Model">
						<i id="config" class="fa fa-cog fa-lg" style="color:white; padding:10px 0px 0px 0px;" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-2 col-md-offset-8 col-xs-3 col-xs-offset-6">					
					<ul class="nav navbar-top-links navbar-right nav-to-right">
						<li class="dropdown">
				            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="javascript:void(0);">
				                <i  id="bell"  class="fa fa-bell"></i> <span id="alerts" class="label label-primary">0</span>
				            </a>
				            <ul class="dropdown-menu dropdown-alerts">
								<li>
				                    <a class="cur" data-toggle="modal" data-target="#RFID_Model">
				                        <div>
				                            <i title="Pickup &amp; Drop Alert" class="fa fa-truck fa-fw"></i> You have <span id="RFIDAlert">0</span> Pickup &amp; Drop Alerts
				                        </div>
				                    </a>
				                </li>	
								<li>
				                    <a class="cur" data-toggle="modal" data-target="#Speed_Model">
				                        <div>
				                            <i title="Pickup &amp; Drop Alert" class="fa fa-truck fa-fw"></i> You have <span id="SpeedAlert">0</span> Over Speed Alerts
				                        </div>
				                    </a>
				                </li>									
				            </ul>
				        </li>
			        </ul>
		        </div>			        
	        </div>	
	        <!-- For Vehicle detail-->
	        <div class="row">
	        	<div class="col-md-12 col-xs-12">
					<div class="table-responsive" > 
						<table id="Grid" border="1" >
							<thead>
								<tr>
									<th style="text-align: center;">Student</th>
									<th style="text-align: center;">Vehicle</th>
									<th style="text-align: center;">Date &amp; Time</th>
									<th style="text-align: center;">Map</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4" style="text-align:center;">-- No Data --</td>
								</tr>
		    				</tbody>										
						</table>	 
					</div>
				</div>
			</div>
		</div>
		<!-- For Map View-->
		<div class="col-md-6 col-xs-12 dashboard-main" style="padding: 5px; border-radius:10px; height:400px; background-color: lightblue;">
			<div id="googleMap" style=" border-style: inset; border-radius:10px; height: 100%">
			</div>
			<button id="go-to-top"  style=" border-style: solid; border-width: 2px; border-color:#758C30; background-color:#90C551; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1.428571429; border-radius: 15px; position: absolute;right: 0px; top:200px;">^</button>			
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 col-xs-12" style="padding: 5px; margin-top: 7px; border-radius:10px; background-color: lightblue;">
			<table id="GridLog" border="1" width="100%" >
				<thead>
					<tr>
						<th colspan="6" style="text-align: center;"> Today (<?php echo date('Y-m-d') ?>) Pickup &amp; Drop Log</th>
					</tr>
					<tr>
						<th style="text-align: center;">Sl. No.</th>
						<th style="text-align: center;">Student</th>
						<th style="text-align: center;">Vehicle</th>
						<th style="text-align: center;">Driver</th>
						<th style="text-align: center;">Driver Mob.</th>
						<th style="text-align: center;">Date &amp; Time</th>						
					</tr>
				</thead>
				<tbody>					
				</tbody>										
			</table>
		</div>
	</div>
</div>



<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
</style>

<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/service_ctrl/vts_service_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="ServiceId" style="display:none;"  name="ServiceId" value="<?php echo((isset($srvc_id))? $srvc_id:null) ?>" />
					<h1>Service Details</h1>
					
          			<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
          			<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
          			
          			</br>
					<div class="user-fields">
						<div class="field">
							<label>Service Name:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="40" id="ServiceName" name="ServiceName" value="<?php if(!isset($outcome))echo $srvc_name?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ServiceName'))echo form_error('ServiceName',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Interval:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="5" id="ServiceInterval" name="ServiceInterval" value="<?php if(!isset($outcome))echo $srvc_inval?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ServiceInterval'))echo form_error('ServiceInterval',' ',' ');?>" />
						</div>
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($srvc_id==null)?"Save":"Update")?> </button>
            			<?php $reloadURL=base_url("index.php/service_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/device_type_ctrl/editOrDelete_vts_service/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						  <tr>
							 <th>Type</th>
							 <th>Interval</th>
					 		 <th>Action</th>
						  </tr>
			              <?php foreach ($ServiceList as $row): ?> 
			              <tr>
							 <td><?php echo trim($row['service_name'])?></td>
							 <td><?php echo trim($row['service_interval'])?></td>
							 <td>
							    <a href="<?php echo(base_url("index.php/service_ctrl/editOrDelete_vts_service/?service_id=edit-".trim($row['service_id'])))?>" title="Edit the Service Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
								<a href="<?php echo(base_url("index.php/service_ctrl/editOrDelete_vts_service/?service_id=delete-".trim($row['service_id'])))?>" title="Delete the Service" onclick="return confirm('Do you want to delete the selected Service?')"><i class="fa fa-trash-o"></i></a>
							 </td>
						  </tr>
			              <?php endforeach; ?>
            		  </table>
				 </div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
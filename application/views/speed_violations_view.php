<script type="text/javascript">

function goCSVButton() //This function is called inside csvButton function
{
	$value = $("#csv").val();
	$("#clicked_btn").val($value);
}
function csvButton() //if we click on Explored to CSV button this function is called and progress bar and loading the data to CSV will happen
{
	myApp.showPleaseWait();
	goCSVButton();
}
function goPDFButton() //This function is called inside pdfButton function
{
	$value = $("#pdf").val();
	$("#clicked_btn").val($value);
}
function pdfButton() //if we click on Explored to PDF button this function is called and progress bar and loading the data to PDF will happen
{
	goPDFButton();
	myApp.showPleaseWait();
}
function goButton()
{
	myApp.showPleaseWait(); 
	getValues();	
}

var myApp;
myApp = myApp || (function () {
	var pleaseWaitDiv = $('<div class="modal_progress_bar show" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ><div class="modal-header"><h3>Please wait. Your request is being processed.</h3></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
	var pleaseWaitDiv_test = $('<div class="modal_progress_bar show" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ><div class="modal-header"><h3>Please wait. Your request is being processed.</h3></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

	return {
		showPleaseWait: function() {
			pleaseWaitDiv.modal();
		},
		hidePleaseWait: function () {
			pleaseWaitDiv_test.model();
		},
	};
})();

	//This function is to get the selected vehicle group and driver group values to the controller, inorder to create the Subtitle for the CSV and PDF creation 
	function getValues()
	{
		var vGrop = $("#VehicleGroup option:selected").text();
		$("#hiddenVehicleGroup").val(vGrop);

		var vGrop = $("#DriverGroupID option:selected").text();
		$("#hiddenDriverGroup").val(vGrop);

		document.getElementById("speed_voilation_form").submit();
	}
	//This function is to get the datepicker for the startDate and endDate fields 
	$(function()
	{
		$("#SpeedVioStartDate").datepicker({ maxDate: new Date(), dateFormat: 'yy-mm-dd' });
		$("#SpeedVioEndDate").datepicker({ maxDate: new Date(), dateFormat: 'yy-mm-dd' });
		$("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
	});

	//get related vehiclegroup and drivergroup related to that perticular client, this will reload the page
	function clientName()
	{
		document.getElementById("temp").value=document.getElementById("ClientName").value;
		document.getElementById("speed_voilation_form").submit();
	}

	//to get vehicle list related to perticular vehicle group in vehicle field dropdown once we select the vehicle group
	function vh_gp_change()
	{
		var baseUrl=document.getElementById('URL').value;
		var vehiclegp=document.getElementById('VehicleGroup').value;;	
	
		if(vehiclegp!='')
		{	
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_vehicle_gp/"+vehiclegp;
		
		xmlhttp.onreadystatechange=function() {
		    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			{
		    	vehicle_for_vehicle_gp(xmlhttp.responseText);
		    }
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
		}
		else
		{
			document.getElementById("test12").innerHTML=null;
		}
	}

	//this function is called in the above function(vh_gp_change)
	function vehicle_for_vehicle_gp(response)
	{
		document.getElementById("test12").innerHTML=null;
		var arrs = JSON.parse(response);
		var id="";var vehicleName="";

		for(j=0;j<arrs.length;j++)
		{
			id=arrs[j].vehicle_id;
			vehicleName=arrs[j].vehicle_regnumber;
			adds_vehicle(id,vehicleName);    
		}
	}

	//this fucntion is added in the above function(vehicle_for_vehicle_gp) in order to create the checkbox and the label
	function adds_vehicle(id,vehicleName)
	{
		$('#test12').append('<input name="Vehicle[]" type="checkbox" value="'+id+'" /><label>' + vehicleName + '</label>');
	}
	
	//to get driver list related to perticular driver group in driver field dropdown once we select the driver group
	function driver_gp_change()
	{
		var baseUrl=document.getElementById('URL').value;
		var drivergpid=document.getElementById('DriverGroupID').value;
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_driver/"+drivergpid;
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				driver_for_driver_gp(xmlhttp.responseText);
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}

	//this function is called in the above function(driver_gp_change)
	function driver_for_driver_gp(response)
	{
		document.getElementById("test123").innerHTML=null;
		var arrs = JSON.parse(response);
		var id="";var deviceName="";  

		for(j=0;j<arrs.length;j++)
		{
			id=arrs[j].driver_id;
			driverName=arrs[j].driver_name;
			adds_driver(id,driverName);    
		}
	}

	//this fucntion is added in the above function(driver_for_driver_gp) in order to create the checkbox and the label
	function adds_driver(id,driverName)
	{
		$('#test123').append('<input name="DriverID[]" type="checkbox" value="'+id+'" /><label>' + driverName + '</label>');
	}
</script>

<style>
	#outcome1 {
		color: green;
	}
	#outcome2 {
		color: red;
	}
	#Spot {
		width: 15px;
		height: 15px;
		margin-right: -15px;
	}
	.modal_progress_bar{
		position: fixed;
		top: 10%;
		margin-left: 393px;
		margin-top: 105px;
		z-index: 1050;
		width: 550px;
		height:170px;
		background-color: #fff;
		border: 1px solid #999;
		border: 1px solid rgba(0,0,0,0.3);
		border-color: white;
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		outline: 0;
		-webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
		-moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
		box-shadow: 0 3px 7px rgba(0,0,0,0.3);
		-webkit-background-clip: padding-box;
		-moz-background-clip: padding-box;
		background-clip: padding-box; 
	}

</style>

<div class="">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="speed_voilation_form" action="<?php echo(base_url("index.php/speed_violations_ctrl/vts_speed_violations_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
					<input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/speed_violations_ctrl/")?>"/>
					
					<h1>Speed Violations</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div></br>

					<div class="user-fields">
					
					<?php if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER):?>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
							<select name="ClientName" id="ClientName" onchange="clientName()">
							<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
							<?php foreach ($clientList as $row):?>
							<option value="<?php echo $row['client_id']?>" <?php echo(($spd_vio_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php endforeach;?>
							</select>
						</div>
					<?php endif; ?>
						<div class="field">
							<label>From Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="SpeedVioStartDate" name="SpeedVioStartDate" class="form-control input-lg" value="<?php if(!isset($outcome))echo $spd_vio_strt_date?>" placeholder="<?php if(null!=form_error('SpeedVioStartDate'))echo form_error('SpeedVioStartDate',' ',' ');?>" />
						</div>
						<div class="field">
							<label>To Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="SpeedVioEndDate" name="SpeedVioEndDate" class="form-control input-lg" value="<?php if(!isset($outcome))echo $spd_vio_end_date?>" placeholder="<?php if(null!=form_error('SpeedVioEndDate'))echo form_error('SpeedVioEndDate',' ',' ');?>" />
						</div>

						<div class="field">
							<label>Vehicle Group:</label>
							<select name="VehicleGroup" id="VehicleGroup" onchange="vh_gp_change(this)">
							<option value=""><?php if(null!=form_error('VehicleGroup'))echo form_error('VehicleGroup',' ',' '); ?></option>
							<?php if($vehicleGroupList!=null): foreach ($vehicleGroupList as $row):?>
							<option value="<?php echo $row['vehiclegroupid']?>" <?php echo(($spd_vio_vclegroup_id==$row['vehiclegroupid'])?'selected':'')?>><?php echo $row['vehiclegroupname']?> </option>
							<?php endforeach; endif;?>
							</select>
						</div>
						<input style="display:none;" id="hiddenVehicleGroup" name="hiddenVehicleGroup" value=""></input>
						<div class="field" id="checkboxes">
							<label>Vehicle:</label>
							<fieldset id="test12">
								<?php if($vehicleList!=null): foreach ($vehicleList as $row):?> 
								<?php 
									$checked=false;
									if($spd_vio_vehicle_id!=null)
									{
										$checked=false;
										foreach ($spd_vio_vehicle_id as $selected)
										{
											if($row['vehicle_id']==$selected['vh_link_vehicle_id'])
											{
												$checked=true;//$accessCount++;
												break; 
											}
										}
									}
								?>
								<input name="Vehicle[]" type="checkbox" value="<?php echo $row['vehicle_id']?>" <?php echo($checked?'checked':'')?>/>
								<label><?php echo $row['vehicle_regnumber']?></label>
								<?php endforeach; endif;?>
							</fieldset>
						</div>
						<div style="display:none;"  class="field">
							<label>Driver Group:</label>
							<select id="DriverGroupID"  name="DriverGroupID" onchange="driver_gp_change()">              
							<option value=""><?php if(null!=form_error('DriverGroupID'))echo form_error('DriverGroupID',' ',' '); ?></option>
								<?php if($driverGroupList!=null):  foreach ($driverGroupList as $row):?>
								<option value="<?php echo $row['driver_group_id']?>" <?php echo(($spd_vio_dvrgroup_id==$row['driver_group_id'])?'selected':'')?>><?php echo $row['driver_group']?> </option>
								<?php endforeach; endif;?>
							</select>
						</div>
						<input style="display:none;" id="hiddenDriverGroup" name="hiddenDriverGroup" value=""></input>
						<div style="display:none;" class="field">
							<label>Driver:</label>
							<fieldset id="test123">
								<?php if($driverList!=null): foreach ($driverList as $row):  
									$checked=false;
									if($spd_vio_driver_id!=null)
									{
										$checked=false;
										foreach ($spd_vio_driver_id as $selected)
										{
											if($row['driver_id']==$selected['dr_link_driver_id'])
											{
												$checked=true;//$accessCount++;
												break; 
											}
										}
									}
								?>
								<input name="DriverID[]" type="checkbox" value="<?php echo $row['driver_id']?>" <?php echo($checked?'checked':'')?>/>
								<label><?php echo $row['driver_name']?></label>
								<?php endforeach; endif;?>
							</fieldset>
						</div>
						<div style="display:none;" class="login-actions">
							<span class="login-checkbox">
							<input id="Spot" name="Spot" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>>
							<label Style="width:180px;cursor: auto;">Show Spot Name</label>
							</span>
						</div>
					</div>
					<!-- /login-fields-->

					<div class="field">
						<button class="btn btn-primary" type="button" onClick="goButton();" ><?php  echo "GO"; ?> </button>
						<?php $reloadURL=base_url("index.php/speed_violations_ctrl/")?>	
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>	
					</div>
			</form>
		</div>
		<!-- /content -->
	</div>
		
<!-- 		to get the details in table -->
	<?php if(!empty($get_gridDetails)) { ?>
	<form id="myform1" action="<?php echo(base_url("index.php/speed_violations_ctrl/vts_speed_violations_validation/".$spd_vio_client_id))?>" method="post" class="form-horizontal">
			</br>
			<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
		<div class="container">
		</br>
		<div class="table-responsive" style="height: 250px;">  
			<table width="100%" class="user-dts" border=1>
				<tr>
					<th style="padding-left: 30px;">Vehicle Name(Car)</th>
					<th style="padding-left: 14px;">Driver</th><th style="padding-left: 22px;">From Date/Time</th>
					<th>To Date/Time</th><th style="padding-left: 30px;">Avg Speed(km/h)(Speed Limit)</th><th style="padding-left: 15px;">Max Speed</th>
					<th>Duration</th>
					<th>Spot</th>
				</tr>
				<?php foreach ($get_gridDetails as $row): //log_message("debug", "row data...".print_r($row,true)); ?>
				<tr>
					<td style="text-align: center;"><?php echo $row['vehicle']?></td>
					<td style="text-align: center;"><?php echo $row['driver'] ?></td>
					<td style="text-align: center;"><?php echo $row['from_datetime'] ?></td>
					<td style="text-align: center;"><?php echo $row['to_datetime'] ?></td>
					<td style="text-align: center;"><?php echo $row['avgspeed']." (".$row['speedlimit'].")"?></td>
					<td style="text-align: center;"><?php echo $row['maxspeed'] ?></td>
					<td style="text-align: center;"><?php echo $row['duration'] ?></td>
					<td style="width: 24em;word-wrap: break-word;"><?php echo $row['spot'] //echo getAddress($row['lat'],$row['long']); ?></td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
		<div class="field" >
				<button style="display:none" id="csv" name="csv" class="btn btn-primary" type="submit" value="csv" onClick="goCSVButton();"><?php echo "Export to CSV"; ?> </button>
				<button id="pdf" name="pdf" class="btn btn-primary" type="submit" value="pdf" onClick="goPDFButton();"><?php echo "Export to PDF"; ?> </button>
				<?php $reloadURL=base_url("index.php/speed_violations_ctrl/map_view/");?>
				<button class="btn btn-primary" type="button" onclick="window.open('<?php echo $reloadURL; ?>')">View Map</button>
				<button style="display:none" class="btn btn-primary" type="button" onclick="window.open('<?=base_url("index.php/speed_violations_ctrl/Speed_violation_graph/");?>')" >View Graph</button>
				
				<input id="clicked_btn" name="clicked_btn" style="display:none;"></input>
		</div>
	</div>

	</form>
	<?php } else if(empty($get_gridDetails) && $no_data) { ?>
		<div class="container">
			</br>
			<table width="100%" class="user-dts" border=0>
				<tr >
					<th>Vehicle Name(Car)</th> <th>Driver</th>
					<th>Date/Time</th><th>Speed(km/h)</th>
					<th>Spot</th>
				</tr>
				<tr><td></td><td></td><td style="color:red;padding:20px;font-size:18px;">No Records found</td><td></td><td></td></tr>
			</table>
		</div>
		
		
		

		<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<div id="graph_container">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
<!-- table ends here....-->
	</div>
</div>

<script>
$(document).ready( function() {

	$("#btn_speed_voil_graph").on("click", function() {

		/* $.post("../speed_violations_ctrl/get_speed_violation_graph", function( result ) {

			$( "#graph_container" ).html(result);
		}); */

		$("#graph_container").load("../speed_violations_ctrl/get_speed_violation_graph", function() {

			$(this).fadeIn("slow");
		});
		
	});
});
</script>
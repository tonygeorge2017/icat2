<script type="text/javascript">
function submit_form(vale)
{
	document.getElementById("CheckID").value=vale.value;
	document.getElementById("myform").submit();
}
$(function() {
// 	$( "#DevicePrepareDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });
	$( "#givenDateTime" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <!-- style="display: none;" -->
        <form id="myform" action="<?php echo(base_url("index.php/device_allocation_dealer_ctrl/device_allocation_dealer_validation/"))?>"	method="post" class="form-horizontal">
        <input type="text"  id="IsEditing" style="display: none;" name="IsEditing" value="<?php echo $editing ?>"/>
        <input type="text"  id="CheckID" style="display: none;" name="CheckID" value=""/>
        <input type="text"  id="DeviceCount" style="display: none;" name="DeviceCount" value="<?php echo count($availableDeviceList)?>"/>
          <h1>Device Allocation To Dealer</h1>
          <?php if(isset($outcome)) echo $outcome;?>
          <br/>
          <div class="user-fields">
             <div class="field">
              <label for="password">Distributor:<span style="color:red;"> *</span></label>
              <select id="DistributorId" name="DistributorId" onchange="submit_form(this)">
               <?php if($distributorList!=null): foreach ($distributorList as $row): ?>
                <?php if($editing==ACTIVE && $distributorID==$row['dist_id']):?>
               	<option value="<?php echo $row['dist_id']?>"><?php echo $row['dist_name']?> </option>
               	<?php elseif($editing==NOT_ACTIVE):?>
               	<option value="<?php echo $row['dist_id']?>" <?php echo(($distributorID==$row['dist_id'])?'selected':'')?>><?php echo $row['dist_name']?> </option>
               <?php endif; endforeach; endif;?> 
             </select>
            </div>
            <?php $client_time=false?>
			<div class="field">
			<label for="confirm password">Available Device(s):<span <?php echo(($availableDeviceList==null)?'style="display: none"':'style="color:red"')?>> *</span></label>
			<fieldset>
				<?php if($availableDeviceList!=null): foreach ($availableDeviceList as $row):?>
				<?php 
					$checked=false;
					$fun='';
					if($selectedDevice!=null)
					{
						$checked=false;
						foreach ($selectedDevice as $selected)
						{
							if($row['device_id']==$selected['device_id'])
							{
								$checked=true;
								if(isset($selected['device_client_id']))
								{
									if($selected['device_client_id']!=null)
										$fun='onclick="return false"';
									else 
										$fun="";
								}
								break;
							}
							
						}
					}
				?>
			<input name="deviceID[]" type="checkbox" value="<?php echo $row['device_id']?>" <?php echo($checked?'checked':'')?> <?php echo $fun?>/>
			<label><?php echo $row['device_imei']." (".$row['device_slno'].")"?> <?php echo(($fun!=null)?'<style="color:red;">* (assigned)</style>':'');?></label>
			<?php endforeach; endif;?>
			</fieldset>
			</div>
			<?php
			//log_message('error','-----------'.$givenDate);
			if($givenDate!=null)
			{
				$dates=date_create_from_format("Y-m-d H:i:s",$givenDate);
				$date_now=date_format($dates,"Y-m-d H:i:s");
				$date_value=new DateTime($date_now);
				$client_time=$date_value->format('Y-m-d H:i:s').'T'.$date_value->format('H:i');
			}
            else {
            	$client_time=$currDate->format('Y-m-d').'T'.$currDate->format('H:i');
            }
			?> 
            <div class="field">
        		<label>Date:<span style="color:red;"> *</span></label>
        		<?php if($editing==NOT_ACTIVE){?>
        		<input style="font-size: 15px;" id="givenDateTime" name="givenDateTime" class="form-control input-lg" value="<?php echo substr_replace($client_time, '', 10);//$client_time ?>"/>
        		<?php }else{?>
        		<input style="font-size: 15px;" id="givenDateTime" name="givenDateTime" class="form-control input-lg" value="<?php echo substr_replace($client_time, '', 10);//$client_time ?>" readonly />
        		<?php }?>
      		</div>
      		
            
             <div class="field">
              <label for="useremail">Dealer:<span style="color:red;"> *</span></label>
              <select id="DealerID"  name="DealerID" >   
               <?php if($editing==NOT_ACTIVE) if(null!=form_error('DealerID'))echo '<option value="">'.form_error('DealerID',' ',' ').'</option>'; else echo '<option value=""></option>'; ?>
               <?php if($dealerList!=null): foreach ($dealerList as $row):?>
                <?php if($editing==ACTIVE && $dealerID==$row['dealer_id']):?>
               	<option value="<?php echo $row['dealer_id']?>"><?php echo $row['dealer_name']?> </option>
               	<?php elseif($editing==NOT_ACTIVE):?>
               	<option value="<?php echo $row['dealer_id']?>"><?php echo $row['dealer_name']?> </option>
               <?php endif; endforeach; endif;?>
              </select>
            </div>  
            
          </div> 
          <div class="field">
            <button class="btn btn-primary" type="submit"><?php echo(($editing==NOT_ACTIVE)?"Save":"Update")?></button>
            <?php $reloadURL=base_url("index.php/device_allocation_dealer_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          </form>
		<form method="post" action="<?php echo(base_url("index.php/device_allocation_dealer_ctrl/edit_deviceAllocation/"))?>">
          <div class="table-responsive">
            <h6 class="linkred">Allocated Device Details</h6>
            <table width="100%" class="user-dts">
              <tr>
                <th>Dealer</th>
                <th>Date</th>
                <th>IMEI No</th>
                <th>Actions</th>
              </tr>
              <?php if($deviceDetailList!=null): foreach ($deviceDetailList as $row):?>
              <tr>
              	<td><?php echo $row['dealer_name']?></td>
                <td><?php echo $row['device_release_dealer_date']?></td>
                <td><?php echo $row['device_imei']?></td>
				<td><a href="<?php echo(base_url("index.php/device_allocation_dealer_ctrl/edit_deviceAllocation/".md5($row['device_distributor_id'])."/".md5($row['device_dealer_id'])."/".md5($row['device_release_dealer_date'])."/".md5($row['device_imei'])."/".md5('edit')))?>" ><i title="Edit device(s)"  class="fa fa-pencil-square-o"></i></a>&nbsp;
 					<a href="<?php echo(base_url("index.php/device_allocation_dealer_ctrl/edit_deviceAllocation/".md5($row['device_distributor_id'])."/".md5($row['device_dealer_id'])."/".md5($row['device_release_dealer_date'])."/".md5($row['device_imei'])."/".md5('delete')))?>" onclick="return confirm('Do you want to remove all the device(s) which are allocated to the dealer?')"><i title="Remove all device(s)" class="fa fa-trash-o"></i></a>
				</td>
              </tr>
              <?php endforeach; endif; ?>
            </table>
          </div>
          </form>
          <nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $pageLink;?>
			</ul>
		 </nav>
          <div class="clearfix"></div>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
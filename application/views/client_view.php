<style>
	#outcome1 {
		color: green;
	}
	#outcome2 {
		color: red;
	}
	#ClientIsActive{
		width:15px;
		height:15px;
		margin-bottom: -14px;
		margin-right: -14px;
	}
	#ClientIsSchool{
		width:15px;
		height:15px;	
		margin-right: -14px;
	}
</style>
<div class="container">
	<div class="row">
	<div class="user-container stacked"><br>
		<div class="content clearfix">
		<form id="myform" action="<?php echo(base_url("index.php/client_ctrl/vts_client_validation/"))?>"	method="post" class="form-horizontal">
			<!-- style="display: none;" -->
			<input type="text" id="ClientId" style="display: none;" name="ClientId" value="<?php echo((isset($client_id))? $client_id:null) ?>"/>
			<h1>Client Details</h1>

		<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
		<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div></br>
		<div class="user-fields">
			<div class="field">
				<label>Client Name:<span style="color:red;"> *</span></label>
				<input type="text" maxlength="40" id="ClientName" name="ClientName"  value="<?php if(!isset($outcome))echo $client_name?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' ');?>" />
			</div>
			<div class="field">
				<label>Dealer:<span style="color:red;"> *</span></label>
				<?php //if($GLOBAL['ID']['sess_user_type'] == DEALER_USER){?>
				<select name="ClientDealerId">
				<!--  <option value=""><?php //if(null!=form_error('ClientDealerId'))echo form_error('ClientDealerId',' ',' '); ?></option>-->
				<?php foreach ($dealerList as $row):?>
				<option value="<?php echo $row['dealer_id']?>" <?php echo(($client_dealer_id==$row['dealer_id'])?'selected':'')?>><?php echo $row['dealer_name']?> </option><!-- $row['dealer_id']."-".$row['dealer_name'] -->
				<?php endforeach;?>
				</select>
			</div>
			<div class="field">
				<label>Client Address:</label>
				<textarea type="text" maxlength="500" id="ClientAddress" name="ClientAddress" class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientAddress'))echo form_error('ClientAddress',' ',' ');?>"><?php if(!isset($outcome))echo $client_address?></textarea>
			</div>
			<div class="field">
				<label>Client Postal Code:</label>
				<input type="text" maxlength="20" id="ClientPostCode" name="ClientPostCode" value="<?php if(!isset($outcome))echo $client_post_code?>"   class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientPostCode'))echo form_error('ClientPostCode',' ',' ');?>"></input>
			</div>
			<div class="field">
				<label>Client Phone:<span style="color:red;"> *</span></label>
				<input type="text" maxlength="10" id="ClientPhone" name="ClientPhone" value="<?php if(!isset($outcome))echo $client_phone?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientPhone'))echo form_error('ClientPhone',' ',' ');?>"></input>
			</div>
			<div class="field">
				<label>Client Email:<span style="color:red;"> *</span></label>
				<input type="text" maxlength="40" id="ClientEmail" name="ClientEmail" value="<?php if(!isset($outcome))echo $client_email?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientEmail'))echo form_error('ClientEmail',' ',' ');?>"></input>
			</div>
			<div class="field">
				<label>Contact Person:<span style="color:red;"> *</span></label>
				<input type="text" maxlength="40" id="ClientContactPerson" name="ClientContactPerson" value="<?php if(!isset($outcome))echo $client_contact_person?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientContactPerson'))echo form_error('ClientContactPerson',' ',' ');?>"></input>
			</div>
			<div class="field">
				<label>Contact Designation:</label>
				<input type="text" maxlength="40" id="ClientContactDesignation" name="ClientContactDesignation" value="<?php if(!isset($outcome))echo $client_contact_designation?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientContactDesignation'))echo form_error('ClientContactDesignation',' ',' ');?>"></input>
			</div>
			<div class="field">
				<label>Contact Email:<span style="color:red;"> *</span></label>
				<input type="text" maxlength="40" id="ClientContactEmail" name="ClientContactEmail" value="<?php if(!isset($outcome))echo $client_contact_email?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientContactEmail'))echo form_error('ClientContactEmail',' ',' ');?>" />
			</div>
			<div class="field">
				<label>Contact Phone:<span style="color:red;"> *</span></label>
				<input type="text" maxlength="10" id="ClientContactPhone" name="ClientContactPhone" value="<?php if(!isset($outcome))echo $client_contact_phone?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientContactPhone'))echo form_error('ClientContactPhone',' ',' ');?>"></input>
			</div>
			<div class="field">
				<label>Client Remark:</label>
				<textarea type="text" maxlength="500" id="ClientRemark" name="ClientRemark" class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientRemark'))echo form_error('ClientRemark',' ',' ');?>"><?php if(!isset($outcome))echo $client_remark?></textarea>
			</div>
			<div class="login-actions">      
				<span class="login-checkbox">
				<input style="" id="ClientIsSchool" name="ClientIsSchool" type="checkbox" class="field login-checkbox" value="1" tabindex="3" <?php echo(($isschool==1)? 'checked':'')?>>
				<label class="choice" for="Field">School</label>
				</span>
			</div>
			<div class="login-actions"> <span class="login-checkbox">
				<input style="" id="ClientIsActive" name="ClientIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>>
				<label class="choice" for="Field">Active</label>
				</span>
			</div>
			<br><br><br>
			<h4 style="color: #d95045;">Location</h4>
			<h1></h1>
			<div class="field">
				<label for="useremail">Time Zone :<span style="color:red;"> *</span></label>
				<select id="TimeZoneID"  name="TimeZoneID">              
				<option value=""><?php if(null!=form_error('TimeZoneID'))echo form_error('TimeZoneID',' ',' '); ?></option>
				<?php if($timeZoneList!=null):  foreach ($timeZoneList as $row):?>
					<option value="<?php echo $row['time_zone_id']?>" <?php echo(($timeZoneID==$row['time_zone_id'])?'selected':'')?>><?php echo $row['time_zone_name']?> </option>
				<?php endforeach; endif;?>
				</select>
			</div>
			<br/><br/>
			<?php if(!isset($client_id)):?>
			<div>
			<?php	$today = date("d");
					$thisMonth = date("m");
					$thisYear = date("Y");
					list($today,$thisMonth,$thisYear) = explode(" ", date("d m Y"));
					$end_date = date("Y-m-d", mktime(0,0,0, $thisMonth, $today-1, $thisYear+1));
			?>
			<h4 style="color: #d95045;">License Details</h4>
				<h1></h1>
				<div class="field">
					<label>Issue Date:<span style="color:red;"> *</span></label>
					<input type="text" id="ClientLicenseStartDate" name="ClientLicenseStartDate" value="<?php echo((isset($client_id))? $client_license_start_date : date('Y-m-d'))?>" readonly class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientLicenseStartDate'))echo form_error('ClientLicenseStartDate',' ',' ');?>"></input>
				</div>
				<div class="field">
					<label>Expiry Date:<span style="color:red;"> *</span></label>
					<input type="text" id="ClientLicenseExpiryDate" name="ClientLicenseExpiryDate" value="<?php echo((isset($client_id))? $client_license_expiry_date : $end_date)?>" readonly class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientLicenseExpiryDate'))echo form_error('ClientLicenseExpiryDate',' ',' ');?>"></input>
				</div>
				<div class="field">
					<label>Max. Users:</label>
					<input type="text" maxlength="5" id="ClientLicenseUsers" name="ClientLicenseUsers" value="<?php if(!isset($outcome))echo $client_license_users?>"   class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientLicenseUsers'))echo form_error('ClientLicenseUsers',' ',' ');?>"></input>
				</div>
				<div class="field">
					<label>Max. Vehicles:</label>
					<input type="text" maxlength="5" id="ClientLicenseVehicles" name="ClientLicenseVehicles" value="<?php if(!isset($outcome))echo $client_license_vehicles?>"   class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientLicenseVehicles'))echo form_error('ClientLicenseVehicles',' ',' ');?>"></input>
				</div>
				<div class="field">
					<label>Max. Drivers:</label>
					<input type="text" maxlength="5" id="ClientLicenseDrivers" name="ClientLicenseDrivers" value="<?php if(!isset($outcome))echo $client_license_drivers?>"  class="form-control input-lg" placeholder="<?php if(null!=form_error('ClientLicenseDrivers'))echo form_error('ClientLicenseDrivers',' ',' ');?>"></input>
				</div>
			</div>
			<?php endif; ?>
			</br>
			<h4 style="color: #d95045;">Access Permissions</h4>
				<h1></h1>
<!-- 			<div> -->
<!-- 				<label>Client Category:</label> -->
<!-- 				<select name="clientCategory"> -->
				<!-- <option value="">Select...</option>
					<option value=<?php //echo BASIC; ?>>Basic</option>
					<option value=<?php //echo ADVANCED; ?>>Advanced</option>
					<option value=<?php //echo PREMIUM; ?>>Premium</option> -->
<!-- 				</select> -->
<!-- 			</div> -->
			
			<div class="field">
			<label for="confirm password">Alerts:</label>
			<fieldset>
				<?php if($clientCategorySMSList!=null): foreach ($clientCategorySMSList as $row):?>
				<?php 
					$checked=false;
					$fun='';
					if($selectedPermissions!=null)
					{
						$checked=false;
						foreach ($selectedPermissions as $selected)
						{
							if($row['config_item']==$selected['permission'])
							{
								$checked=true;
								break;
							}
						}
					}
				?>
			<input name="permissionID[]" type="checkbox" value="<?php echo $row['config_item']?>" <?php echo($checked?'checked':'')?> <?php echo $fun?>/>
			<label><?php echo $row['config_item']; ?></label>
			<?php endforeach; endif;?>
			</fieldset>
			</div>
			
		</div>
		<!-- /login-fields -->
		<div class="field">
			<button class="btn btn-primary" type="submit"><?php echo(($client_id==null)?"Save":"Update")?> </button>
			<?php $reloadURL=base_url("index.php/client_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
			<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
		</div>	
		</form>
		<form method="post" action="<?php echo(base_url("index.php/client_ctrl/editOrDelete_vts_client/"))?>">
			<div class="table-responsive">
			<table width="100%" class="user-dts">
			<tr>
				<th>Client Name</th>
				<th>Client Email</th>
				<th></th>
				<th>Action</th>
			</tr>
			<?php foreach ($clientList as $row): ?> 
			<tr>
				<td><?php echo trim($row['client_name'])?></td>
				<td><?php echo trim($row['client_email'])?></td>
				<td></td>
				<td><a href="<?php echo(base_url("index.php/client_ctrl/editOrDelete_vts_client/?client_id=edit-".trim($row['client_id'])))?>" title="Edit Client Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
					<!--  <a href="<?php //echo(base_url("index.php/client_ctrl/editOrDelete_vts_client/?client_id=delete-".trim($row['client_id'])))?>" onclick="return confirm('Do you want to delete the selected Client?')"><i class="fa fa-trash-o"></i></a>-->
				</td>
			</tr>
			<?php endforeach; ?>
			</table>
			</div>
		</form>
		<nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $pageLink;?>
			</ul>
		</nav>
		<div class="clearfix"></div>
	</div>
	<!-- /content -->
	</div>
  </div>
</div>
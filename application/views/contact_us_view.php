<!-- Login -->
<div class="container">
  <div class="row">
    <div class="account-container stacked"><br>
      <div class="content clearfix">
      <form action="<?php echo(base_url("index.php/contact_us/validate_contactus/"))?>"	method="post" class="form-horizontal">
          <!-- style="display: none" -->
          <input type="text" id="ClientID" style="display: none" name="ClientID"  value="<?php echo $clientID?>"/>
          <input type="text" id="UserID" style="display: none" name="UserID"  value="<?php echo $userID?>"/>
          <h1>Contact Us</h1>
           <?php if(isset($outcome)) echo $outcome;?>
          <div class="user-fields">
            <div class="field">
			<label>Name:<span style="color:red;"> *</span></label>
              <input type="text" id="UserFullName" name="UserFullName" value="<?php echo $userName?>" placeholder="<?php echo((null!=form_error('UserFullName'))?form_error('UserFullName',' ',' '):''); ?>"  class="form-control input-lg" maxlength="100" />
            </div>
            <!-- /field -->
           <div class="field">
			<label>Email:<span style="color:red;"> *</span></label>
              <input type="text" id="UserEmail" name="UserEmail" value="<?php echo $userEmail?>" placeholder="<?php echo((null!=form_error('UserEmail'))?form_error('UserEmail',' ',' '):''); ?>"  class="form-control input-lg" maxlength="50" />
            </div>      
            <div class="field">
			<label>Mobile:<span style="color:red;"> *</span></label>
              <input type="text" id="MobileNo" name="MobileNo" value="<?php echo $mobileNo?>" placeholder="<?php echo((null!=form_error('MobileNo'))?form_error('MobileNo',' ',' '):''); ?>"  class="form-control input-lg" maxlength="10" />
            </div>   
            <div class="field">
        		<label>Message:<span style="color:red;"> *</span></label>
       			<textarea  id="Message" name="Message" cols="" rows="" placeholder="<?php echo((null!=form_error('Message'))?form_error('Message',' ',' '):''); ?>" class="form-control input-lg" maxlength="500"><?php echo $message ?></textarea>
     	 	</div>    
          </div>
          <div class="field">
            <button type="submit" class="btn btn-primary">Send</button>
            <?php $reloadURL=base_url("index.php/contact_us/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>          
          <div class="clearfix"></div>
          <!-- .actions -->
        </form>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
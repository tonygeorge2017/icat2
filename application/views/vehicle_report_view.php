<script type="text/javascript">

	//below 2 functions are to down load the CSV and PDF from the form
	function goCSVButton()
	{ 
		$value = $("#csv").val();
		$("#clicked_btn").val($value);
	}
	
	function goPDFButton()
	{ 
		$value = $("#pdf").val();
		$("#clicked_btn").val($value);
	}
	function goGraphButton()
	{ 
		$value = $("#graph").val();
		$("#clicked_btn").val($value);
	}


	//This function is to get the values to CSV which are selected in the View(UI)
	function getValues()
	{
		var vGrop = $("#VehicleGroup option:selected").text();
		$("#hiddenVehicleGroup").val(vGrop);
	
		document.getElementById("myform").submit();
	}

	function goButton()
	{
		progressBar.showPleaseWait(); 
		getValues();	
	}

	var progressBar;
	progressBar = progressBar || (function () {
		var pleaseWaitDiv = $('<div class="modal_progress_bar show" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ><div class="modal-header"><h3>Please wait. Your request is being processed.</h3></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
			return {
			showPleaseWait: function() {
				pleaseWaitDiv.modal();
			},
			hidePleaseWait: function () {
				pleaseWaitDiv.model('hide');
			},
		};
	})();
	
	$(function() {
		$( "#VehicleReportFromDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy/m/d' });
		$( "#VehicleReportToDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy/m/d' });
		$( "#datepicker" ).datepicker({ dateFormat: 'yy/m/d' });
		});

	//get related vehiclegroup and drivergroup related to that perticular client
	function myFunction()
	{
		document.getElementById("temp").value=document.getElementById("ClientName").value;//ClientName
		document.getElementById("myform").submit();
	}
	
	//to get vehicle list related to perticular vehicle group in vehicle field dropdown once we select the vehicle group
	function vh_gp_change()
	{
		var baseUrl=document.getElementById('URL').value;
		var vehiclegp=document.getElementById('VehicleGroup').value;;
	
		if(vehiclegp!='')
		{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_vehicle_gp/"+vehiclegp;
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			{
				vehicle_for_vehicle_gp(xmlhttp.responseText);
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
		}
		else
		{
			document.getElementById("test12").innerHTML=null;
		}
	}
	
	function vehicle_for_vehicle_gp(response)
	{
		document.getElementById("test12").innerHTML=null;
		var arrs = JSON.parse(response);
		var id="";var vehicleName="";  

		for(j=0;j<arrs.length;j++)
		{
			id=arrs[j].vehicle_id;
			vehicleName=arrs[j].vehicle_regnumber;
			adds_vehicle(id,vehicleName);    
		}
	}
	function adds_vehicle(id,vehicleName)
	{
		$('#test12').append('<input name="Vehicle[]" type="checkbox" value="'+id+'" /><label>' + vehicleName + '</label>');
	}

</script>

<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
#VehicleGroupIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
.modal_progress_bar
{
position: fixed;
	  top: 10%;
	  margin-left: 393px;
	  margin-top: 105px;
	  z-index: 1050;
	  width: 550px;
	  height:170px;
	  background-color: #fff;
	  border: 1px solid #999;
	  border: 1px solid rgba(0,0,0,0.3);
	  border-color: white;
	  -webkit-border-radius: 6px;
	  -moz-border-radius: 6px;
	  border-radius: 6px;
	  outline: 0;
	  -webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	  -moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	  box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	  -webkit-background-clip: padding-box;
	  -moz-background-clip: padding-box;
	  background-clip: padding-box; 
}
</style>

<div class="">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/vehicle_report_ctrl/vts_vehicle_report_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
					<input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/vehicle_report_ctrl/")?>"/>
					
					<h1>Vehicle Report</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div></br>

					<div class="user-fields">
					<?php if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER):?>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
							<select name="ClientName" id="ClientName" onchange="myFunction()">
							<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
							<?php foreach ($clientList as $row):?>
							<option value="<?php echo $row['client_id']?>" <?php echo(($vcle_rept_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php endforeach;?>
							</select>
						</div>
					<?php endif; ?>
						<div class="field">
							<label>From Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="VehicleReportFromDate" name="VehicleReportFromDate" class="form-control input-lg" value="<?php if(!isset($outcome))echo $vcle_rept_from_date?>" placeholder="<?php if(null!=form_error('VehicleReportFromDate'))echo form_error('VehicleReportFromDate',' ',' ');?>" />
						</div>
						<div class="field">
							<label>To Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="VehicleReportToDate" name="VehicleReportToDate" class="form-control input-lg" value="<?php if(!isset($outcome))echo $vcle_rept_to_date?>" placeholder="<?php if(null!=form_error('VehicleReportToDate'))echo form_error('VehicleReportToDate',' ',' ');?>" />
						</div>
 						<div class="field">
							<label>Vehicle Group:<span style="color:red;"> *</span></label>
							<select name="VehicleGroup" id="VehicleGroup" onchange="vh_gp_change(this)">
							<option value=""><?php if(null!=form_error('VehicleGroup'))echo form_error('VehicleGroup',' ',' '); ?></option>
							<?php if($vehicleGroupList!=null): foreach ($vehicleGroupList as $row):?>
							<option value="<?php echo $row['vehiclegroupid']?>" <?php echo(($vcle_rept_vclegroup_id==$row['vehiclegroupid'])?'selected':'')?>><?php echo $row['vehiclegroupname']?> </option>
							<?php endforeach; endif;?>
							</select>
						</div>
						<input style="display:none;" id="hiddenVehicleGroup" name="hiddenVehicleGroup" value=""></input>
						<div class="field">
						<label>Vehicle:<span style="color:red;"> *</span></label>
							<fieldset id="test12">
							<?php if($vehicleList!=null): foreach ($vehicleList as $row):?> 
							<?php 
							$checked=false;
							if($vcle_rept_vehicle_id !=null)
							{
								$checked=false;
									foreach ($vcle_rept_vehicle_id as $selected)
									{
										if($row['vehicle_id']==$selected['vh_link_vehicle_id'])
										{
											$checked=true;
											break; 
										}
									}
							}
							?>
							<input name="Vehicle[]" type="checkbox" value="<?php echo $row['vehicle_id']?>" <?php echo($checked?'checked':'')?>/>
							<label><?php echo $row['vehicle_regnumber']?></label>
							<?php endforeach; endif;?>
							</fieldset>
						</div>
					</div>
					<!-- /login-fields-->

					<div class="field">
						<button class="btn btn-primary" type="submit" onClick="goButton();"><?php  echo "GO"; ?> </button>
						<?php $reloadURL=base_url("index.php/vehicle_report_ctrl/")?>	
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>	
					</div>
					
			</form>
		</div>
			<!-- /content -->
		</div>
<!-- 		to get the details in table -->
		
		<!-- Login -->
		<?php if(!empty($get_gridDetails)) { ?>
		<form id="myform1" action="<?php echo(base_url("index.php/vehicle_report_ctrl/vts_vehicle_report_validation/".$vcle_rept_client_id))?>" method="post" class="form-horizontal">
		</br> 
		
		<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
	<div class="container">
	</br>
	
	<div class="table-responsive" style="height: 250px;">

	<table width="100%" class="user-dts" border=1>
		<tr>
			<th style="padding-left: 40px;">Vehicle</th><th style="padding-left: 40px;">Driver</th><th style="padding-left: 40px;">From Date/Time</th><th style="padding-left: 40px;">To Date/Time</th>
			<th style="padding-left: 40px;">Duration</th><th style="padding-left: 40px;">Status</th>
			<th>Distance</th>
		</tr>
		<?php $total_dist=0;?>
		<?php foreach ($get_gridDetails as $row): ?>
		<tr>
			<td style="text-align: center;"><?php if(isset($row['vehicle'])) echo $row['vehicle']; else echo "No value";?></td>
			<td style="text-align: center;"><?php if(isset($row['driver'])) echo $row['driver']; else echo "No value";?></td>
			<td style="text-align: center;"><?php if(isset($row['from_datetime'])) echo $row['from_datetime'];  else echo "No value";?></td>
			<td style="text-align: center;"><?php if(isset($row['to_datetime'])) echo $row['to_datetime'];  else echo "No value";?></td>
			<td style="text-align: center;"><?php if(isset($row['duration'])) echo $row['duration'];  else echo "No value";?></td>
			<td style="text-align: center;"><?php if(isset($row['status'])) echo $row['status'];  else echo "No value";?></td>
			<td style="text-align: center;"><?php if(isset($row['distance'])) echo $row['distance'];  else echo "No value";?></td>
			<?php 
				$temp_curr_dist=(isset($row['distance']))?$row['distance']:0; 
				$total_dist+=$temp_curr_dist;
			?>
		</tr>
		<?php endforeach; ?>
	</table>
	</div>
	<b><p style="margin-top:20px;" >Total Distance travelled for selected vehicles : <?php echo round($total_dist,1)." KMS";//echo round($this->session->userdata('SESSION_TOTAL_DISTANCE'), 1)." KMS";?></p></b>	
	<div class="field">
		<button id="csv" name="csv" class="btn btn-primary" type="submit" value="csv" onClick="goCSVButton();"><?php echo "Export to CSV"; ?> </button>
		<button id="pdf" name="pdf" class="btn btn-primary" type="submit" value="pdf" onClick="goPDFButton();"><?php echo "Export to PDF"; ?> </button>
		
		<?php $reloadURL=base_url("index.php/vehicle_report_ctrl/vts_vehicle_report_graph/");?>
		<button id="graph" name="graph" class="btn btn-primary" type="button" value="graph" onclick="window.open('<?php echo $reloadURL; ?>')"><?php echo "Show Graph"; ?> </button>

		<input style="display:none;" id="clicked_btn" name="clicked_btn" ></input><!--  style="display:none;" --> 
	</div>
	</div>
</form>

		<?php } else if(empty($get_gridDetails) && $no_data) { ?>
		<div class="container">
		</br>
			<table width="100%" class="user-dts" border=0>
			<tr>
				<th>From Date/Time</th><th>To Date/Time</th>
				<th>Duration</th><th>Status</th>
				<th>Distance</th>
			</tr>
			<tr>
				<td></td><td></td><td style="color:red;padding:20px;font-size:18px;">No Records found</td><td></td><td></td>
			</tr>
			</table>
		</div>
		<?php } ?>
		
		<div class="clearfix"></div>
<!-- 		table ends here.... -->
	</div>
</div>


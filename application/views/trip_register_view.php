<script type="text/javascript">
$(function(){
  var shiftDetails=[];
  var driver_details=[];
  $('#ClientID').on('change', function(){
    $('OnClientID').val($(this).val());
    $('#myform').submit();
  });
  if($('#TripRegID').val())
  {
    //get_drivers();
    //get_vehicle();
  }
  if($('#IsShiftBased').val()==0)
  {
    $('#ShiftCat').hide();
    $('#Shift').hide();
    $('.tohide').hide();    
  }else{    
    $('.noshift').hide();
  }

  $('#StartDateTime').on('change',function(){
    dateTime_change();
  });

  $('#EndDateTime').on('change',function(){
    dateTime_change();
  });
  function dateTime_change()
  { 
    $('#ErrorMsg').val(null);
    if($('#ClientID').val()!='')
    {
      get_drivers();
      get_vehicle();
    }
  }
  $('#VehicleGroupID').on('change', function(){
    get_vehicle();
  }); 
  $('#shiftCategory').on('change',function(){    
    $('#shiftDetail').html('');
    $('#DriverID').html('')
    shiftDetails=[];    
    if($(this).val())
    {
      $.ajax({
        type:'GET',
        dataType:'json',
        url:$('#URL').val()+'/get_shift/'+$(this).val(),
        success: function(data){
          if(data.length>0)
          {          
            $('#shiftDetail').append($('<option></option>').val('').text('-- Select --'));
            $.each(data, function(key, value){
              $('#shiftDetail').append($('<option></option>').val(value['shift_id']).text(value['shift_name']));
              shiftDetails[value['shift_id']]={'name':value['shift_name'],'start':value['shift_start_time'],'end':value['shift_end_time']};
            });
            
            //console.log(shiftDetails);
          }
          else{
            $('#shiftDetail').append($('<option></option>').val('').text('-- None --'));          
          }
        },
        error:function(e){
          $('#shiftDetail').html('<option value="">-- Error --</option>');        
          console.log(e.responseText);
        }
      });
    }else{
      $('#shiftDetail').html('<option value="">-- Choose Shift Category First --</option>');  
    }
  });

  $('#shiftDetail').on('change',function(){
    get_drivers();
    get_vehicle()
  });
  $('#DriverGroupID').on('change',function(){
    get_drivers();
  });
  function get_drivers()
  {
    driver_details=[];
    $('#DriverID').html('');
    var tripid=$('#TripRegID').val();
    tripid=(tripid!='')?tripid:'-1';
    var drivergpid=$('#DriverGroupID').val();
    var startdate=$('#StartDateTime').val();
    var enddate=$('#EndDateTime').val();
    var shift=$('#shiftDetail').val();
    var can_append=false;
    var is_last=false;
    var pre_driver_id=0;
    $.ajax({
      type:'GET',
      url:$('#URL').val()+"/get_driver/"+tripid+"/"+drivergpid+"/"+startdate+"/"+enddate,
      dataType:'json',
      success:function(data){
        if(data.length>0)
        {
          $('#DriverID').append('<option value="-1">-- Select --</option>');
          pre_driver_id=data[0]['driver_id'];
          driver_details[pre_driver_id]=true;
          //console.log(data);
          $.each(data,function(key,value){
            if(value['driver_id']!=pre_driver_id)
              driver_details[value['driver_id']]=true;
            //If it is a shift based trip register            
            if(value['trip_register_shift'] && driver_details[value['driver_id']])
            { 
              //console.log('Clinet using shift');
              if((startdate >= value['o_from'] && startdate<=value['o_end']) || (enddate >= value['o_from'] && enddate<=value['o_end']))
              {                
                var c_stt = new Date("November 13, 2013 " + shiftDetails[shift]['start']);
                var c_endt = new Date("November 13, 2013 " + shiftDetails[shift]['end']);
                //console.log('c_stt:'+c_stt+' ,'+'c_endt:'+c_endt);
                c_stt = c_stt.getTime();                
                c_endt = c_endt.getTime();
                var stt = new Date("November 13, 2013 " + shiftDetails[value['trip_register_shift']]['start']);
                var endt = new Date("November 13, 2013 " + shiftDetails[value['trip_register_shift']]['end']);
                //console.log('stt:'+stt+' ,'+'endt:'+endt);
                stt = stt.getTime();                
                endt = endt.getTime();
                if(((c_stt > stt) && (c_stt < endt)) || ((c_endt > stt) && (c_endt < endt)))
                  can_append = false;
                else if(shift==value['trip_register_shift'])
                  can_append = false;
                else
                  can_append=true;
              }else{
                can_append=true;
              }
            }
            else if(((startdate >= value['trip_register_time_start'] && startdate<=value['trip_register_time_end']) || (startdate >= value['trip_register_time_start'] && startdate<=value['trip_register_time_end'])) && driver_details[value['driver_id']]){
              //console.log('Client not using shift');
              can_append=false;
            }
            else{
              can_append=true;
            }
			if(driver_details[value['driver_id']])
				driver_details[value['driver_id']]=can_append;
            pre_driver_id=value['driver_id'];          
          });
          //console.log(driver_details);
          var drListArray=[];
          $.each(data,function(key,value){
            if(driver_details[value['driver_id']] && ($.inArray(value['driver_id'],drListArray)==-1))
            {
              $('#DriverID').append($('<option></option>').val(value['driver_id']).text(value['driver_name'])); 
              drListArray.push(value['driver_id']);            
            }
          }); 
          drListArray=[];          
        }else{
          $('#DriverID').append('<option value="-1">-- None --</option>');
        }
      },
      error:function(e)
      {
        console.log(e.responseText);
      }
    });
  }
  function get_vehicle()
  {
    vehicle_details=[];
    $('#VehicleID').html('');
    var tripid=$('#TripRegID').val();
    tripid=(tripid!='')?tripid:'-1';
    var vehiclegpid=$('#VehicleGroupID').val();
    var startdate=$('#StartDateTime').val();
    var enddate=$('#EndDateTime').val();
    var shift=$('#shiftDetail').val();
    var can_append=false;
    var is_last=false;
    var pre_vehicle_id=0;
    $.ajax({
      type:'GET',
      url:$('#URL').val()+"/get_vhehicle_gp/"+tripid+"/"+vehiclegpid+"/"+startdate+"/"+enddate,
      dataType:'json',
      success:function(data){
        //console.log(data);
        if(data.length>0)
        {
          $('#VehicleID').append('<option value="-1">-- Select --</option>');
          pre_vehicle_id=data[0]['vehicle_id'];
          vehicle_details[pre_vehicle_id]=true;
          //console.log(data);
          $.each(data,function(key,value){
            if(value['vehicle_id']!=pre_vehicle_id)
              vehicle_details[value['vehicle_id']]=true;
            //If it is a shift based trip register            
            if(value['trip_register_shift'] && vehicle_details[value['vehicle_id']])
            { 
              //console.log('Clinet using shift');
              if((startdate >= value['o_from'] && startdate<=value['o_end']) || (enddate >= value['o_from'] && enddate<=value['o_end']))
              {                
                var c_stt = new Date("November 13, 2013 " + shiftDetails[shift]['start']);
                var c_endt = new Date("November 13, 2013 " + shiftDetails[shift]['end']);
                //console.log('c_stt:'+c_stt+' ,'+'c_endt:'+c_endt);
                c_stt = c_stt.getTime();                
                c_endt = c_endt.getTime();
                var stt = new Date("November 13, 2013 " + shiftDetails[value['trip_register_shift']]['start']);
                var endt = new Date("November 13, 2013 " + shiftDetails[value['trip_register_shift']]['end']);
                //console.log('stt:'+stt+' ,'+'endt:'+endt);
                stt = stt.getTime();                
                endt = endt.getTime();
                if(((c_stt > stt) && (c_stt < endt)) || ((c_endt > stt) && (c_endt < endt)))
                  can_append = false;
                else if(shift==value['trip_register_shift'])
                  can_append = false;
                else
                  can_append=true;
              }else{
                can_append=true;
              }
            }
            else if(((startdate >= value['trip_register_time_start'] && startdate<=value['trip_register_time_end']) || (startdate >= value['trip_register_time_start'] && startdate<=value['trip_register_time_end'])) && vehicle_details[value['vehicle_id']]){
              //console.log('Client not using shift');
              can_append=false;
            }
            else{
              can_append=true;
            }
			if(vehicle_details[value['vehicle_id']])
				vehicle_details[value['vehicle_id']]=can_append;
            pre_vehicle_id=value['vehicle_id'];          
          });
          //console.log(vehicle_details);
		  var vhListArray=[];         
          $.each(data,function(key,value){
            if(vehicle_details[value['vehicle_id']] && ($.inArray(value['vehicle_id'],vhListArray)==-1))
            {
              $('#VehicleID').append($('<option></option>').val(value['vehicle_id']).text(value['vehicle_regnumber']));
              vhListArray.push(value['vehicle_id']);
            }
          }); 
          vhListArray=[]; 
        }else{
          $('#VehicleID').append('<option value="-1">-- None --</option>');
        }
      },
      error:function(e)
      {
        console.log(e.responseText);
      }
    });
  }
});
</script>
<style type="text/css">
  .tohide{

  }
  .noshift{}
</style>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <form id="myform" action="<?php echo(base_url("index.php/trip_register_ctrl/validate_trip_list/"))?>" method="post" class="form-horizontal">
           <!-- style="display:none;" -->
          <input type="text" id="OnClientID" name="OnClientID" style="display: none" value="-1" />  
          <input type="text" id="TripRegID" name="TripRegID" style="display: none" value="<?php echo $tripRegID ?>" />  
          <input type="hidden" id="IsShiftBased" value="<?php echo count($shiftCategoryList) ?>">       
          <input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/trip_register_ctrl/"))?>" />
          <h1>Trip Register</h1>
          <?php if(isset($outcome)) echo $outcome;?>
          <div id="ErrorMsg" style="color: red;"></div>
          <div class="user-fields">
          <div class="field">
              <label for="useremail" <?php if($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER) echo 'style="display: none"'?>>Client:<span <?php echo(($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <select id="ClientID"  name="ClientID"  <?php if($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER) echo 'style="display: none"'?>>  <!--onchange="submit_form(this)" -->
               <?php if(null!=form_error('ClientID'))echo '<option value="">'.form_error('ClientID',' ',' ').'</option>'; ?>            
               <?php if($clientList!=null): if(($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER ) & null==form_error('ClientID')){echo'<option value=""></option>';} foreach ($clientList as $row):?>
               	<?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
              		<option value="<?php echo $row['client_id']?>" <?php echo(($clientID==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
                <?php elseif($sessClientID==$row['client_id']): ?>
              		<option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
              	<?php endif;?>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div id="ShiftCat" class="field">
              <label>Shift Category:<span style="color:red;"> *</span></label>
              <select id="shiftCategory" name="shiftCategoryID" >
                <?php if(count($shiftCategoryList)>0):?>
                  <?php echo "<option value=''>-- Select --</option>";?>
                  <?php foreach ($shiftCategoryList as $key => $value): ?>                    
                    $selectedCategory=($value["shift_category_id"]==$shiftCategoryID)?"selected":" ";
                    <?php echo '<option value="'.$value['shift_category_id'].'" '.$selectedCategory.'>'.$value['shift_category_name'].'</option>'; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>  
            <div id="Shift" class="field">
              <label>Shift:<span style="color:red;"> *</span></label>
               <select id="shiftDetail" name="shiftID" >
                  <?php if(count($shiftList)>0):?>
                    <?php echo "<option>-- Select --</option>";?>
                    <?php foreach ($variable as $key => $value): ?>
                      $selectedShift=(value["shift_id"]==$shiftID)?"selected":" ";
                      <?php echo '<option value="'.$value['shift_id'].'" '.$selectedShift.'>'.$value['shift_name'].'</option>'; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
               </select>              
            </div>
            <?php            	
        	    $diff=0;
          		$time=gmdate('Y-m-d H:i:s');
            	if($clientTimeDiff!=null)
            		$diff=$clientTimeDiff*60;
            	$current_date=new DateTime($time);
            	$current_date=$current_date->modify($diff." minutes");
            	$start_date=null;
            	$end_date=null;
            if($startDate==null && $endDate==null)
            {              	
            	$start_date=new DateTime($time);
            	$end_date=new DateTime($time);
            	$start_date=$start_date->modify($diff." minutes");            	
            	$end_date=$end_date->modify(($diff+60)." minutes");            	
            }   
            else
            {	
            	if($startDate!=null)
            	{
	            	$sdates=date_create_from_format("Y-m-d H:i:s",$startDate);
	            	$sdate_now=date_format($sdates,"Y-m-d H:i:s");
	            	$start_date=new DateTime($sdate_now);
            	}            	
            	if($endDate!=null)
            	{
	            	$edates=date_create_from_format("Y-m-d H:i:s",$endDate);
	            	$edate_now=date_format($edates,"Y-m-d H:i:s");
	            	$end_date=new DateTime($edate_now);
            	}            	
            }        
			?>
		<div class="field">
		 <label>Start Date &amp; time:<span style="color:red;"> *</span></label>
     	   <input style="font-size: 15px;" type="datetime-local" id="StartDateTime" name="StartDateTime" class="form-control input-lg" <?php echo(($start_date<=$current_date & !empty($tripRegID))?'readonly':'');?> value="<?php echo(($start_date!=null)?$start_date->format('Y-m-d').'T'.$start_date->format('H:i:s'):'') ?>" /> <!--  onchange="dateTime_change()" -->
    </div>
   		   <div class="field">
    	    <label>End Date &amp; time:</label>
   	  	   <input style="font-size: 15px;" type="datetime-local" id="EndDateTime" name="EndDateTime" class="form-control input-lg" value="<?php echo(($end_date!=null)? $end_date->format('Y-m-d').'T'.$end_date->format('H:i:s'):'') ?>" /> <!--  onchange="dateTime_change()" -->
   		   </div>
            <div class="field">
              <label for="useremail">Vehicle Group:<span style="color:red;"> *</span></label>
              <select id="VehicleGroupID"  name="VehicleGroupID"><!-- onchange="vh_gp_change(this)" -->
              <option value="-1"><?php if(null!=form_error('VehicleGroupID'))echo form_error('VehicleGroupID',' ',' '); ?></option>
               <?php if($vehicleGroupList!=null):  foreach ($vehicleGroupList as $row):?>
               		<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vehicleGroupID==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>                       
            
            <div class="field">
              <label for="useremail">Vehicle:<span style="color:red;"> *</span></label>
              <select id="VehicleID"  name="VehicleID">   
              <option value="-1"><?php if(null!=form_error('VehicleID'))echo form_error('VehicleID',' ',' '); ?></option>           
               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
               		<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vehicleID==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            
            <div class="field">
              <label for="useremail">Driver Group:<span style="color:red;"> *</span></label>
              <select id="DriverGroupID"  name="DriverGroupID" ><!-- onchange="driver_gp_change()" -->              
              <option value="-1"><?php if(null!=form_error('DriverGroupID'))echo form_error('DriverGroupID',' ',' '); ?></option>
               <?php if($driverGroupList!=null):  foreach ($driverGroupList as $row):?>
               		<option value="<?php echo $row['driver_group_id']?>" <?php echo(($driverGroupID==$row['driver_group_id'])?'selected':'')?>><?php echo $row['driver_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>                       
            
            <div class="field">
              <label for="useremail">Driver:<span style="color:red;"> *</span></label>
              <select id="DriverID"  name="DriverID">   
              <option value=""><?php if(null!=form_error('DriverID'))echo form_error('DriverID',' ',' '); ?></option>           
               <?php if($driverList!=null): foreach ($driverList as $row):?>
               		<option value="<?php echo $row['driver_id']?>" <?php echo(($driverID==$row['driver_id'])?'selected':'')?>><?php echo $row['driver_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>     
			
        	     		
			<div class="field">
        		<label>Remarks:</label>
       			<textarea style="font-size: 13px;" id="Remarks" name="Remarks" cols="" rows="" class="form-control input-lg" maxlength="500"><?php echo $remarks ?></textarea>
     	 	</div>
          </div>
          <!-- /login-fields -->
		 <div class="field">
            <button class="btn btn-primary" type="submit"><?php echo(($tripRegID==null)?"Save":"Update")?></button>
            <?php $reloadURL=base_url("index.php/trip_register_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          <div class="table-responsive">
            <table width="100%" class="user-dts">
              <tr>
                <th>Driver</th>
                <th>Vehicle</th>
                <th class="tohide">Shift</th>
                <th>Start &amp; End</th>
                <!--<th>End</th>-->
                <th>Action</th>
              </tr>
              <?php if($tripList!=null): foreach ($tripList as $row):?>
               	<tr>
              		<td><?php echo $row['driver_name']?></td>
               		<td><?php echo $row['vehicle_name']?></td>
                  <td class="tohide"><?php echo $row['shift_name']?></td>
               		<td class="noshift"><?php echo $row['trip_register_time_start'].' to<br>'.$row['trip_register_time_end']?></td>
                  <td class="tohide"><?php echo $row['o_from'].' to<br>'.$row['o_end']?></td>
               		<!--<td><?php //echo $row['trip_register_time_end']?></td>-->
               		<td><!--<a href="<?php //echo(base_url("index.php/trip_register_ctrl/manage_trip/".md5(trim($row['trip_register_client_id']))."/".md5(trim($row['trip_id'])))."/".md5('edit'))?>" ><i title="Edit Trip Register" class="fa fa-pencil-square-o"></i></a> &nbsp; -->
               		<a href="<?php echo(base_url("index.php/trip_register_ctrl/manage_trip/".md5(trim($row['trip_register_client_id']))."/".md5(trim($row['trip_id'])))."/".md5('delete'))?>" onclick="return confirm('Do you want to cancel this trip?')"><i title="Delete Trip Register" class="fa fa-trash-o"></i></a></td>
              	</tr>
               <?php endforeach; endif;?>
            </table>
             <nav class="pull-right">
              <ul class="pagination pagination-sm">
              <?php echo $pageLink?>
              </ul>
            </nav>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
      <!-- /content -->
    </div>
  </div>
</div>
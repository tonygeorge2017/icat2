<script type="text/javascript">
/*
*function to get dynamic values depend on client selection
*/
function get_values_dealer(val) {
		//var baseUrl = document.getElementById('URL').value;
		//var clientid =document.getElementById('ClientID').value;
		document.forms["myform"].submit();
		//xmlhttp.open("POST",baseUrl+"/get_drivers/"+drvgpid,true);
	}
</script>

<div class="container">
	<div class="row">
		<div class="col-lg-12 dashboard-main">
			<form id="myform"
				action="<?php echo(base_url("index.php/dealer_dashboard_ctrl/view"))?>"
				method="post">
				<div align=center>
 					<table border=0> 
						<td><h1><?php echo(($sessClientID==AUTOGRADE_USER)?"Dealer DashBoard For" :"Dealer Dashboard");?></h1></td>
 						<td>&nbsp;&nbsp; 
 						<select id="DealerID" name="DealerID" 
 							onChange="get_values_dealer(this.val);" 
							<?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?>>   
							<?php if(null!=form_error('DealerID'))echo '<option value="">'.form_error('DealerID',' ',' ').'</option>'; ?>            
							<?php if($dealerList!=null):if($sessClientID==AUTOGRADE_USER & null==form_error('DealerID'))
								{
									echo'<option value=""> </option>';
								} 
								foreach ($dealerList as $row): ?>
							<?php if($sessClientID==AUTOGRADE_USER):?>
								<!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
								<option value="<?php echo $row['dealer_id']?>"
								<?php echo(($dealerID==$row['dealer_id'])?'selected':'')?>><?php echo $row['dealer_name']?> </option>
								<?php elseif($sessDealerID==$row['dealer_id']): ?>
								<option value="<?php echo $row['dealer_id']?>"><?php echo $row['dealer_name']?> </option>
								<?php endif;?>
							<?php endforeach; endif;?>
					</select> 
						</td> 
					</table> 
					<!-- <h1>Dealer Dashboard</h1>-->
				</div>


				<div class="dash-tiles row">
					<div class="col-sm-3">
						<div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
 							<div class="dash-tile-header">

 								<i class="fa fa-car"></i> Purchase Details 
								</div>
<!--						<input type="Text" style="display: none;" id="URL" name="URL"
								value="<?php //echo base_url("index.php/dealer_dashboard_ctrl/")?>" />-->
								<ul class="dash-tile-ul">
<!-- 								<li><select id="VehicleGroupID" name="VehicleGroupID" -->
<!-- 									onChange="get_no_of_vehicles(this.val);"> -->
<!-- 									<option value="">All</option> -->
									<?php //foreach ($vhgp as $row ){?>
<!--									<option value="<?php //echo $row['vehiclegroupid'];?>"><?php //echo $row['vehiclegroupname'];?></option>
									<?php //} ;?>
<!-- 								</select></li> -->
									<li>Purchase This Month : 
									<?php foreach ($dealer_device_details as $row):?>
									<div class="dash-tile-text1" id="NoPurDevicesMnth"><?php echo $row['purchase_this_month'] ;?></div>
								</li> 
								<li>Purchase This Year : 
									<div class="dash-tile-text1" id="NoPurDevicesYr"><?php   echo $row['purchase_this_year'] ;?></div> 
								</li> 
								</ul>
							</div>
						</div> 
					<div class="col-sm-3">
 						<div class="dash-img clearfix"> 
 							<img src="<?php echo base_url('assets/images/Dashboard_img1.jpg')?>" alt="" /> 
 						</div> 
 					</div> 
					<div class="col-sm-3">
						<div class="dash-tile dash-tile-flower clearfix animation-pullDown"> 
 							<div class="dash-tile-header"> 
 								<i class="fa fa-male"></i> Sales Details
							</div> 
							<ul class="dash-tile-ul"> 
<!-- 								<li><select id="DriverGroupID" name="DriverGroupID" -->
<!-- 									onChange='get_no_of_drivers();'> -->
<!-- 										<option value="">All</option> -->
										<?php //foreach ($drgp as $row ){?>
<!--										<option value="<?php //echo $row['drivergroupid'];?>"><?php //echo $row['drivergroupname'];?></option> -->
										<?php //};?>
<!-- 								</select></li> -->
								<li>Sales This Month:
									<div class="dash-tile-text1" id="NoSalesDevicesMnth"><?php echo $row['sales_this_month']  ;?></div>
								</li>
								<li>Sales This Year : 
									<div class="dash-tile-text1" id="NoSalesDevicesYr" onclick="get_details_yr()"><?php echo $row['sales_this_year']  ;?></div>
									<?php endforeach;?> 
 								</li> 
							</ul> 
 						</div>
					</div> 
					<div class="col-sm-3">
						<div class="dash-img clearfix">
							<img src="<?php echo base_url('assets/images/Dashboard_img2.jpg')?>" alt="" />
						</div>
					</div>
					<div class="col-sm-12">
						<div style="background-color: Lavender;"
							class="dash-tile dash-tile-oil clearfix animation-pullDown">
							<div class="table-responsive table-thing" id="grid">
								<table>
									<tr>
										<th>No of Devices Sold Details</th>
										<th>Today</th>
										<th>This Month</th>
										<th>Last Month</th>
										<th>This Year</th>
									</tr>
								<?php //foreach ($grid_data as $row){?>
								<tr>
										<td><?php //echo $row['title']; ?></td>
										<td><?php //echo $row['today'];?></td>
										<td><?php //echo $row['thismonth'];?></td>
										<td><?php //echo $row['lastmonth'];?></td>
										<td><?php //echo $row['thisyear'];?></td>
									</tr>
								<?php //} ?>
							</table>
							</div>
						</div>
					</div>
<!-- 					<div class="col-sm-6" 
						onclick="location.href='speed_violations_ctrl/';">
<!-- 						<div> -->
							<a href="<?php //echo base_url('index.php/speed_violations_ctrl/');?>">
<!-- 							<strong>Speed Violations</strong> -->
<!-- 						</div> -->
<!-- 						</a> -->
<!-- 						<div style="background-color: Thistle;"
							class="dash-tile dash-tile-balloon clearfix animation-pullDown">
							<div class="table-responsive table-thing" id="speed"> -->
<!-- 								<table> -->
<!-- 									<tr> -->
<!-- 										<th>Car</th> -->
<!-- 										<th>From Date</th> -->
<!-- 										<th>To Date</th> -->
<!-- 										<th>Speed</th> -->
<!-- 										<th>Driver</th> -->
<!-- 										<th>Spot</th> -->
<!-- 									</tr> -->
									<?php //$i = 0; $getIn = false;
// 											if ($speed_data != null) :
// 											if (isset($speed_data [0] ['vehicle'])) :
// 											foreach ( $speed_data as $row ) :
// 									?>
<!-- 									<tr> -->
										<td><?php //echo $row['vehicle'];?></td>
										<td><?php //echo $row['from_datetime'];?></td>
										<td><?php //echo $row['to_datetime'];?></td>
										<td><?php //echo $row['maxspeed'];?></td>
										<td><?php //echo $row['driver'];?></td>
										<td><?php //echo $spot[$i];$i++;?></td>
<!-- 									</tr> -->
									<?php //endforeach ;?>
									<?php  //else: ?>
<!-- 									<tr> -->
										<td style="padding-top: 10px;" colspan="4"><?php //echo "No records found." ?></td>
<!-- 									</tr> -->
									<?php //endif; ?>
									<?php  //else: ?>
<!-- 									<tr> -->
										<td style="padding-top: 10px;" colspan="4"><?php //echo "No records found." ?></td>
<!-- 									</tr> -->
									<?php //endif;?>
<!-- 								</table> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="col-sm-6"> -->
<!-- 						<div> -->
<!-- 							<strong>Service Status</strong> -->
<!-- 						</div> -->
<!-- 						<div style="background-color: Wheat;"
							class="dash-tile dash-tile-doll clearfix animation-pullDown">
<!-- 							<div class="table-responsive table-thing"> -->
<!-- 								<table> -->
<!-- 									<tr> -->
<!-- 										<th>Car</th> -->
<!-- 										<th>Date</th> -->
<!-- 										<th>Service</th> -->
<!-- 										<th>Status</th> -->
<!-- 									</tr> -->
									<?php //if($string2!=null):?>
									<?php  //foreach ($string2 as $row): ?>
<!-- 									<tr> -->
										<td><?php //echo $row[1]; ?></td>
										<td><?php //echo $row[2];?></td>
										<td><?php //echo $row[4]; ?></td>
										<td><?php //echo $row[5];?></td>
<!-- 									</tr> -->
									<?php //endforeach; ?>
									<?php //else: ?>
<!-- 									<tr> -->
										<td style="padding-top: 10px;" colspan="4"><?php //echo "No records found." ?></td>
<!-- 									</tr> -->
									<?php //endif;?>
<!-- 								</table> -->
<!-- 							</div> -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Footer -->

<link href="src/css/bootstrapValidator.css" rel="stylesheet">
<script src="src/js/bootstrapValidator.js"></script>
<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- Script to Activate the Carousel -->
</body>
</html>


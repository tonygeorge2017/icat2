<!-- Login -->
<div class="container">
  <div class="row">
    <div class="account-container stacked"><br>
      <div class="content clearfix">
      <form action="<?php echo(base_url("index.php/login_ctrl/validate_user/"))?>"	method="post" class="form-horizontal">
          <h1>Sign In</h1>
          <div class="login-fields">
          <font color="red"> <?php echo validation_errors('<p class ="form_error">','</p>'); ?></font><br />
            <div class="field control-group">
              <label for="username">Username:</label>
              <input type="text" id="UserName" name="UserName" value="<?php echo $user_name?>" placeholder="<?php echo((null!=form_error('User Name'))?form_error('User Name',' ',' '):'User Name'); ?>" class="form-control input-lg username-field" />
            </div>
            <!-- /field -->
            <div class="field">
              <label for="password">Password:</label>
              <input type="password" id="Password" name="Password" value="" placeholder="<?php echo((null!=form_error('Password'))?form_error('Password',' ',' '):'Password'); ?>" class="form-control input-lg password-field" />
            </div>            
            <!-- /password --> 
          </div>
          <div class="field">
            <button class="btn btn-primary">Ok</button>
            <?php $reloadURL=base_url("index.php/login_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          <div class="field"> <a href="<?php echo(base_url("index.php/forget_password_ctrl/"))?>"> Forgot Password ?</a></div>
          <div class="clearfix"></div>
          <!-- .actions -->
        </form>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
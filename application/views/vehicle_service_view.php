<script type="text/javascript">

		$(function() {
			$( "#VehicleServiceDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });
			$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
		});

		function vh_gp_change()
		{
			var baseUrl=document.getElementById('URL').value;
			var vehiclegp=document.getElementById('VehicleGroup').value;;

			if(vehiclegp!='')
			{
			var xmlhttp = new XMLHttpRequest();
			var url = baseUrl+"/get_vehicle_gp/"+vehiclegp;
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					vehicle_for_vehicle_gp(xmlhttp.responseText);
				}
			}
			xmlhttp.open("GET", url, true);
			xmlhttp.send();
			}
			else
			{
				document.getElementById("Vehicle").innerHTML=null;
			}
		}
		
		function vehicle_for_vehicle_gp(response)
		{
			document.getElementById("Vehicle").innerHTML=null;
			var x=document.getElementById("Vehicle");
			var option=document.createElement("option");
			var arrs = JSON.parse(response);
			var id="";var vehicleName="";
			for(j=0;j<arrs.length;j++)
			{
				id=arrs[j].vehicle_id;
				vehicleName=arrs[j].vehicle_regnumber;
				adds_vehicle(id,vehicleName);    
			}
		}
		function adds_vehicle(id,vehicleName)
		{
			var x=document.getElementById("Vehicle");
			var option=document.createElement("option");
			option.text=vehicleName;
			option.value=id;
			x.add(option);
		}
</script>
<script>
function myFunction()
{
	document.getElementById("temp").value=document.getElementById("ClientName").value;//ClientName
	document.getElementById("myform").submit();
}

</script>
<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}

</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/vehicle_service_ctrl/vts_vehicle_service_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/vehicle_service_ctrl/")?>"/>
					<input type="text" style="display : none;" id="VehicleServiceId"  name="VehicleServiceId" value="<?php echo((isset($vcle_srvc_id))? $vcle_srvc_id:null) ?>" />
					<input type="text" style="display : none;" id="temp"   name="temp" value="-1" />
					<h1>Vehicle Service Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>
					<div class="user-fields">
					
					<?php if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
							<select name="ClientName" id="ClientName" onchange="myFunction()">
							<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
							<?php foreach ($clientList as $row):?>
							<option value="<?php echo $row['client_id']?>" <?php echo(($vcle_srvc_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php endforeach;?>
							</select>
						</div>
					 <?php endif; ?>
						<div class="field">
							<label>Vehicle Group:<span style="color:red;"> *</span></label>
							<select name="VehicleGroup" id="VehicleGroup" onchange="vh_gp_change(this)">
							<option value=""><?php if(null!=form_error('VehicleGroup'))echo form_error('VehicleGroup',' ',' '); ?></option>
							<?php if($vehicleGroupList!=null): foreach ($vehicleGroupList as $row):?>
							<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vcle_srvc_vclegroup_id==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
							<?php endforeach; endif;?>
							</select>
						</div>
						<div class="field">
							<label>Vehicle:<span style="color:red;"> *</span></label>
							<select name="Vehicle" id="Vehicle">
							<option value=""><?php if(null!=form_error('Vehicle'))echo form_error('Vehicle',' ',' '); ?></option>
							<?php foreach ($vehicleList as $row):?>
							<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vcle_srvc_vehicle_id==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
							<?php endforeach; ?>
							</select>
						</div>
						<div class="field">
							<label>Service:<span style="color:red;"> *</span></label>
							<select name="Service">
							<option value=""><?php if(null!=form_error('Service'))echo form_error('Service',' ',' '); ?></option>
							<?php foreach ($serviceList as $row):?>
							<option value="<?php echo $row['service_id']?>" <?php echo(($vcle_srvc_service_id==$row['service_id'])?'selected':'')?>><?php echo $row['service_name']?> </option><!-- $row['dealer_id']."-".$row['dealer_name'] -->
							<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>Date: </label>
							<input style="font-size: 13px;" type="text" id="VehicleServiceDate" name="VehicleServiceDate" class="form-control input-lg" value="<?php if(!isset($outcome)) echo substr_replace($vcle_srvc_date, '', 10);?>" />
						</div>
						<div class="field">
							<label>Remarks:</label>
							<textarea type="text" maxlength="100" id="VehicleServiceRemarks" name="VehicleServiceRemarks" class="form-control input-lg" placeholder="<?php if(null!=form_error('VehicleServiceRemarks'))echo form_error('VehicleServiceRemarks',' ',' ');?>"><?php if(!isset($outcome))echo $vcle_srvc_remarks?></textarea>
						</div><br/>
						
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($vcle_srvc_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/vehicle_service_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/vehicle_service_ctrl/editOrDelete_vts_vehicle_service/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						<tr>
							<th>Vehicle</th>
							<th>Service</th>
							<th>Date</th>
							<th>Actions</th>
						</tr>
						<?php if($vehicleServiceList!=null): foreach ($vehicleServiceList as $row): ?>
						<tr>
							<td><?php echo trim($row['vehicle_name'])?></td>
							<td><?php echo trim($row['service_name'])?></td>
							<td><?php echo trim($row['vh_ser_service_date'])?></td>
							<td>
							<?php $md_id = md5(trim($row['vehicle_group_client_id']));?>
								<a href="<?php echo(base_url("index.php/vehicle_service_ctrl/editOrDelete_vts_vehicle_service/".$md_id."/".md5('edit')."/".md5(trim($row['vh_ser_id']))))?>" title="Edit the Vehicle Service"><i class="fa fa-pencil-square-o"></i></a> &nbsp;
								<a href="<?php echo(base_url("index.php/vehicle_service_ctrl/editOrDelete_vts_vehicle_service/".$md_id."/".md5('delete')."/".md5(trim($row['vh_ser_id']))))?>" title="Delete the Vehicle Service" onclick="return confirm('Do you want to delete the selected Vehicle Service?')"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php endforeach; endif;?>
						</table>
				</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
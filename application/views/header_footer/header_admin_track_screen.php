<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<!-- jQuery -->
<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>"></script>
<script src="<?php echo base_url('assets/js/jqBootstrapValidation.js')?>"></script>
<!-- Bootstrap Core JavaScript -->

<script>
$(document).ready(function(){
  $('.dropdown-submenu a.not_close_onclick_menu').on("click", function(e){
    //$(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
 // $('#go-to-top').click(function(){ $("body").scrollTop(100);});
  $('#go-to-top').click(function(){ $("body").animate({ scrollTop: 100}, 600);});    
  $('#startTimer').click(function(){$("body").animate({ scrollTop: $('body').height()}, 600);});  
});

</script>
<script>
var map;var backTrackRoute;var route;var startMarke,endMarker,selectedVehicleMarker,bulk_start,bulk_end; var wayPointMarkers=[];var LastMarkers;
var latLngPoint=[];var latLngPoint_Animation =[]; var stabilizerPoint; var myVar;var zoomCount=0; var Animation;var arr;
var arrayMarker=[];      		
var tool_tip=[];
var i;
var j;
var out ="";    
var distance=0; 
var Animation_loop=0;
var image_url;
var slow_icon;
var speed_icon;
var ignition_status=[];
var correct_speed=false;
var cur_lat,cur_lng,pre_lat,pre_lng;
var ignition_icon;
var is_AnimationComplete=true;
var latlngbounds;
var setBoundCount=0;
function function_onload_page()
{
	if(zoomCount==0)
	{
		initMap();
		document.getElementById('Live').checked=true;
    	document.getElementById("TimeSelection").readOnly=true;
   		document.getElementById("report").disabled=true;
    	timerFunction();
	}	
}


function timerFunction()
{
	//alert("in Timetfunction");
	if(is_AnimationComplete)
	{
	//	alert("is true");
		clearAllMarker();
	}
		
	document.getElementById("stopTimer").disabled =false;
	document.getElementById("startTimer").disabled =true;
	zoomCount=0;
	if(is_AnimationComplete)
	{	
	//	alert("is true");
		startTrack();
		myVar=setInterval(function(){startTrack()},10000); //10 secs once update the map.
	}
	else
	{
		//alert("is fasle");
		Animation=setInterval(function(){animation_function()},100);
	}
			
}

function myTimer() {
	startTrack();
}

function stopTimerFunction()
{
	if(document.getElementById('Live').checked==true)
		document.getElementById('info').value="";
	cursor_status("auto");
	document.getElementById("stopTimer").disabled =true;
	document.getElementById("startTimer").disabled =false;	
	clearInterval(myVar);	
	clearInterval(Animation);		
}

function initMap()
{		
	document.getElementById('Live').checked=true;
 	document.getElementById("Fdaytime").readOnly=true;
 	document.getElementById("Tdaytime").readOnly=true;
	document.getElementById("TimeSelection").disabled=true;
	document.getElementById("report").disabled=true;
	image_url = document.getElementById("wayPntURL").value;
	slow_icon={url:image_url+"/vts_waypoints_blue.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	slow_icon_bl={url:image_url+"/vts_waypoints_blue_BL.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	slow_icon_br={url:image_url+"/vts_waypoints_blue_BR.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	slow_icon_b={url:image_url+"/vts_waypoints_blue_B.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	slow_icon_l={url:image_url+"/vts_waypoints_blue_L.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	slow_icon_r={url:image_url+"/vts_waypoints_blue_R.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	slow_icon_t={url:image_url+"/vts_waypoints_blue_T.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	slow_icon_tl={url:image_url+"/vts_waypoints_blue_TL.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	slow_icon_tr={url:image_url+"/vts_waypoints_blue_TR.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	
	speed_icon={url:image_url+"/vts_waypoints_red.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(15,15)};
	speed_icon_bl={url:image_url+"/vts_waypoints_red_BL.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	speed_icon_br={url:image_url+"/vts_waypoints_red_BR.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	speed_icon_b={url:image_url+"/vts_waypoints_red_B.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	speed_icon_l={url:image_url+"/vts_waypoints_red_L.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	speed_icon_r={url:image_url+"/vts_waypoints_red_R.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	speed_icon_t={url:image_url+"/vts_waypoints_red_T.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};
	speed_icon_tl={url:image_url+"/vts_waypoints_red_TL.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	speed_icon_tr={url:image_url+"/vts_waypoints_red_TR.png", origin:new google.maps.Point(0,0), anchor:new google.maps.Point(5,5)};
	//ignition_OFF_icon,ignition_ON_icon
	ignition_OFF_icon={url:image_url+"/vts_ignition_off.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(15,15)};
	ignition_ON_icon={url:image_url+"/vts_ignition_on.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(15,15)};
	ignition_ON_speed_icon={url:image_url+"/vts_ignition_on_speed.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(15,15)};
	way_igoff_icon={scaledSize: new google.maps.Size(20, 20),url:image_url+"/vts_waypoints_red.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(5,5)};

	
	start_icon={url:image_url+"/vts_start_new2.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(15,15)};
	end_icon={url:image_url+"/test_end.png", origin:new google.maps.Point(0,0),	anchor:new google.maps.Point(15,15)};//image_url+"/test_end.png";
	document.getElementById("stopTimer").disabled =true;
	document.getElementById("startTimer").disabled =false;
	latlngbounds = new google.maps.LatLngBounds();
	var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:2,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			  };
			  
	map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	
	startMarke=new google.maps.Marker({map:map, icon:start_icon});
	google.maps.event.addListener(startMarke, 'click', function() {
		document.getElementById('info').value=this.getTitle(); });
	
	endMarker=new google.maps.Marker({map:map});//, icon:end_icon	
	google.maps.event.addListener(endMarker, 'click', function() {
			document.getElementById('info').value=this.getTitle(); });
//bulk_start,bulk_end
	bulk_start=new google.maps.Marker({map:map, icon:start_icon});//, icon:end_icon	
	google.maps.event.addListener(bulk_start, 'click', function() {
			document.getElementById('info').value=this.getTitle(); });
	bulk_end=new google.maps.Marker({map:map});//, icon:end_icon	
	google.maps.event.addListener(bulk_end, 'click', function() {
			document.getElementById('info').value=this.getTitle(); });
	
	selectedVehicleMarker=new google.maps.Marker({map:map});
	
	wayPointMarkers=new google.maps.Marker({ map: map });
	backTrackRoute=new google.maps.Polyline({
		  strokeColor:"#0000FF",
		  strokeOpacity:0.8,
		  strokeWeight:3,
		  editable:false
		  });
	backTrackRoute.setMap(map);
	LastMarkers=new google.maps.Marker({ map: map, animation:google.maps.Animation.BOUNCE });
	google.maps.event.addListener(map, 'zoom_changed', function() {
				var zoomLevel=map.getZoom();			
				if(zoomLevel < 2)
				{	
					map.setZoom(2);			
				}		
				//alert(zoomLevel);	
			});
} 
function startTrack()
{	
	if(zoomCount==0)
        cursor_status("wait");
	var imei=document.getElementById('imeino').value;
	imei=imei.trim();
	var fdate=document.getElementById('Fdaytime').value;
    var tdate=document.getElementById('Tdaytime').value;
    var baseUrl=document.getElementById('URL').value;
    var tester_speed=document.getElementById('speedlimit').value;
    if(tester_speed=="")
    	tester_speed=3;	
    var trackType="";
    if(document.getElementById('Replay').checked==true)
    {    	
    	trackType="replay";
    	clearInterval(myVar);
    }
    else
    	trackType="live";
	if(trackType=="replay" && fdate >= tdate)
	{
		stopTimerFunction();
		cursor_status("auto");
		document.getElementById('info').value="Start date & time must be less than End date & time..";
		alert(document.getElementById('info').value);
		clearInterval(myVar);	
		return true;
	}else if(imei!="" && fdate!="" && tdate!="")
	{		
		var xmlhttp = new XMLHttpRequest();
		if(document.getElementById('StepByStepPlotting').checked==true)
			var url = baseUrl+"/fetch_gps_data/"+tester_speed+"/"+imei+"/"+fdate+"/"+tdate+"/"+trackType+"/1";
	    else
	    	var url = baseUrl+"/fetch_gps_data/"+tester_speed+"/"+imei+"/"+fdate+"/"+tdate+"/"+trackType;
		
		//alert(url);
		xmlhttp.onreadystatechange=function() {
		    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				cursor_status("auto");
		        //alert(xmlhttp.responseText);
		        myFunction(xmlhttp.responseText);
		    }	    
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	else
	{
		stopTimerFunction();
		cursor_status("auto");
		document.getElementById('info').value="Please give all details to proceed..";	
		alert(document.getElementById('info').value);
	}
}
function myFunction(response) {
	latLngPoint=[];
    arr = JSON.parse(response);
    //alert("received:"+arr.length);    
    if(arr.length!=0)
    {
        if(arr[0].lat!="Na")
        {
    		for(i = 0; i < arr.length; i++)
    		{    		
    			latLngPoint[i]=new google.maps.LatLng(arr[i].lat,arr[i].lng);
    			if(arr[i].status=="Y")
    				ignition_status[i]="ON";
    			else
    				ignition_status[i]="OFF"; 
    			latlngbounds.extend(latLngPoint[i]);   			        	
    		}
    		if(setBoundCount == 0)
			{
				map.setCenter(latlngbounds.getCenter());
				map.fitBounds(latlngbounds);
				setBoundCount = 1;
				i=0;
			}	
			if(document.getElementById('Live').checked==true && document.getElementById('imeino').value==0)
			{								
				for(i = 0; i < arr.length; i++)
       		 	{      					  
        			if(arrayMarker.length!=arr.length)
					{        			
						arrayMarker[i]=new google.maps.Marker({ position: latLngPoint[i], map: map});	       		 			        		 			        		
					}
					else
					{					
						arrayMarker[i].setPosition(latLngPoint[i]);							        		
					} 
        			if(arr[i].status=="Y")
        			{	
	    				//arrayMarker[i].setIcon(ignition_ON_icon);
	    				if((parseFloat(arr[i].speed)*1.852) < arr[i].speedLimit)
	    					arrayMarker[i].setIcon(ignition_ON_icon);
						else
							arrayMarker[i].setIcon(ignition_ON_speed_icon);
	        			arrayMarker[i].setTitle(arr[i].imeino+"\n"+arr[i].vehicle+"\nIgnition : "+ignition_status[i]+"\nLat : "+latLngPoint[i].lat()+"\nLng : "+latLngPoint[i].lng()+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno);
        			}
	    			else
	    			{
	    				arrayMarker[i].setIcon(ignition_OFF_icon);
	        			arrayMarker[i].setTitle("Last Seen\n"+arr[i].imeino+"\n"+arr[i].vehicle+"\nIgnition : "+ignition_status[i]+"\nLat : "+latLngPoint[i].lat()+"\nLng : "+latLngPoint[i].lng()+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno);
	    			}
        		
        			google.maps.event.addListener(arrayMarker[i], 'click', function() {
         			document.getElementById('info').value=this.getTitle();  			
         			});         			      				
        		}		
			}
			else if(document.getElementById('Live').checked==true && document.getElementById('imeino').value!=0)
			{
				selectedVehicleMarker.setPosition(latLngPoint[0]);
				if(arr[0].status=="Y")
    			{						
					if((parseFloat(arr[0].speed)*1.852) < arr[0].speedLimit)
						selectedVehicleMarker.setIcon(ignition_ON_icon);
					else
						selectedVehicleMarker.setIcon(ignition_ON_speed_icon);
					out="Current position\nIMEI No:"+arr[0].imeino+" ["+arr[0].digitalio+"]\nAnalog{A:"+arr[0].adca+", B:"+arr[0].adcb+"}\nIgnition : "+ignition_status[0]+"\nLat : "+latLngPoint[0].lat()+"\nLng : "+latLngPoint[0].lng()+"\nSpeed["+arr[0].speedLimit+"] : "+(parseFloat(arr[0].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[0].dateTime+"\nGPS DateTime :"+arr[0].gpsdatetime+"\nRunnig No:"+arr[0].runno+"\nDigital IO :"+arr[0].digitalio;       			
    			}
    			else
    			{
    				selectedVehicleMarker.setIcon(ignition_OFF_icon);
    				out="Last seen\nIMEI No:"+arr[0].imeino+" ["+arr[0].digitalio+"]\nAnalog{A:"+arr[0].adca+", B:"+arr[0].adcb+"}\nIgnition : "+ignition_status[0]+"\nLat : "+latLngPoint[0].lat()+"\nLng : "+latLngPoint[0].lng()+"\nSpeed["+arr[0].speedLimit+"] : "+(parseFloat(arr[0].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[0].dateTime+"\nGPS DateTime :"+arr[0].gpsdatetime+"\nRunnig No:"+arr[0].runno +"\nDigital IO :"+arr[0].digitalio;        			
    			}
				selectedVehicleMarker.setTitle("Lat:"+latLngPoint[0].lat()+"\nLng : "+latLngPoint[0].lng()+"\nSpeed["+arr[0].speedLimit+"] : "+(parseFloat(arr[0].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[0].dateTime+"\nGPS DateTime :"+arr[0].gpsdatetime+"\nRunnig No:"+arr[0].runno+"\nDigital IO:"+arr[0].digitalio);
				map.setCenter(latLngPoint[0]);						
				if(zoomCount==0)
				{
					selectedVehicleMarker.setVisible(true);
					map.setZoom(16);zoomCount = 1;
					selectedVehicleMarker.setMap(map);
					startMarke.setMap(null);
					endMarker.setMap(null);					
				}				
				//out="position"+"\nIgnition : "+ignition_status[0]+"\nLat : "+latLngPoint[0].lat()+"\nLng : "+latLngPoint[0].lng()+"\nSpeed : "+(parseFloat(arr[0].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[0].dateTime;
				document.getElementById('info').value=out;				
			}
			else if(document.getElementById('Live').checked==false && document.getElementById('imeino').value!=0)
			{	
				latLngPoint_Animation=[];
				clearInterval(myVar);  						
				if(latLngPoint.length > 2)
				{
					latLngPoint_Animation[0]=latLngPoint[0];					
					stabilizerPoint=latLngPoint[0];
					backTrackRoute.setMap(map);						
					i=0;
					j=0;
					if(document.getElementById('StepByStepPlotting').checked==true)
					{
						is_AnimationComplete=false;
						startMarke.setMap(map);
						endMarker.setMap(map);
						Animation=setInterval(function(){animation_function()},100);
					}
					else
					{
						//alert("1");
						bulk_start.setMap(map);
						bulk_end.setMap(map);
						route=latLngPoint;
						backTrackRoute.setMap(map);
						backTrackRoute.setPath(route);
						bulk_start.setPosition(latLngPoint[0]);
						bulk_start.setTitle("Start Point:\nIMEI No:"+arr[0].imeino+"\nIgnition : "+ignition_status[0]+"\nLat : "+latLngPoint[0].lat()+"\nLng : "+latLngPoint[0].lng()+"\nSpeed["+arr[0].speedLimit+"] : "+(parseFloat(arr[0].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[0].dateTime+"\nGPS DateTime :"+arr[0].gpsdatetime+"\nRunnig No:"+arr[0].runno+"\nAngle :"+arr[0].angle+"\nDigital IO :"+arr[0].digitalio);
						bulk_end.setPosition(latLngPoint[latLngPoint.length-1]);
						//alert("2");
						if(ignition_status[arr.length-1]=="ON")
						{
							if((parseFloat(arr[arr.length-1].speed)*1.852)<arr[arr.length-1].speedLimit)
								bulk_end.setIcon(ignition_ON_icon);
							else
								bulk_end.setIcon(ignition_ON_speed_icon);
						}
						else
							bulk_end.setIcon(ignition_OFF_icon);
						bulk_end.setTitle("End Point:\nIMEI No:"+arr[arr.length-1].imeino+"\nIgnition : "+ignition_status[arr.length-1]+"\nLat : "+latLngPoint[arr.length-1].lat()+"\nLng : "+latLngPoint[arr.length-1].lng()+"\nSpeed["+arr[arr.length-1].speedLimit+"] : "+(parseFloat(arr[arr.length-1].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[arr.length-1].dateTime+"\nGPS DateTime :"+arr[arr.length-1].gpsdatetime+"\nRunnig No:"+arr[arr.length-1].runno+"\nAngle :"+arr[arr.length-1].angle+"\nDigital IO :"+arr[arr.length-1].digitalio);
						document.getElementById("stopTimer").disabled =true;
						document.getElementById("startTimer").disabled =false;
						//alert("Test");
					}
				}
			}
        }
        else
        {  
        	if(document.getElementById('Live').checked==true)       	
        		document.getElementById('info').value="Please wait...";
        	else if(document.getElementById('StepByStepPlotting').checked==true && arr[0].lng=="TimeOutRange")
        	{       	
        		clearAllMarker();        	
            	map.setZoom(2);            	
        		document.getElementById('info').value="Sorry\nTime interval should be within 24Hr. Or go for bulk plotting";
        		alert(document.getElementById('info').value);
        	}
        	else
        	{
        		clearAllMarker();        	
            	map.setZoom(2);
        		document.getElementById('info').value="Sorry\nNo data found between\n"+arr[0].fdateTime+"\n       to\n"+arr[0].tdateTime;
        		alert(document.getElementById('info').value);
        	}        	
        }
    }
    else
    {
    	document.getElementById('info').value="No data found";    	
    }
}

function animation_function()
{
	i++;
	
	//if((ignition_status[i]=="ON") && ((parseFloat(arr[i].speed)*1.852) >= parseFloat(3)))
	//{
		latLngPoint_Animation[i]=latLngPoint[i];
		//stabilizerPoint=latLngPoint[i];
		correct_speed=true;			
	//}
	//else
	//{
	//	latLngPoint_Animation[i]=stabilizerPoint;
	//	correct_speed=false;
	//}

	
	if(i < latLngPoint.length)
	{		
		route=latLngPoint_Animation;
		backTrackRoute.setPath(route);
		startMarke.setPosition(latLngPoint_Animation[0]);
		startMarke.setTitle("Start Point"+"\nIgnition : "+ignition_status[0]+"\nLat : "+latLngPoint_Animation[0].lat()+"\nLng : "+latLngPoint_Animation[0].lng()+"\nSpeed["+arr[0].speedLimit+"] : "+(parseFloat(arr[0].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[0].dateTime+"\nGPS DateTime :"+arr[0].gpsdatetime+"\nRunnig No:"+arr[0].runno+"\nAngle :"+arr[0].angle+"\nDigital IO :"+arr[0].digitalio);
		if(i == latLngPoint.length-1)		
		{
			endMarker.setPosition(latLngPoint_Animation[i]);
			if(ignition_status[i]=="ON")
			{
				if((parseFloat(arr[i].speed)*1.852)<arr[i].speedLimit)
					endMarker.setIcon(ignition_ON_icon);
				else
					endMarker.setIcon(ignition_ON_speed_icon);
			}
			else
				endMarker.setIcon(ignition_OFF_icon);
			endMarker.setTitle("End Point "+"\nIgnition : "+ignition_status[i]+"\nLat : "+latLngPoint_Animation[i].lat()+"\nLng : "+latLngPoint_Animation[i].lng()+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno+"\nAngle :"+arr[i].angle+"\nDigital IO :"+arr[i].digitalio);
			//LastMarkers.setPosition(latLngPoint_Animation[i]);
			//LastMarkers.setTitle("Current position\nLat : "+latLngPoint_Animation[i].lat()+"\nLng : "+latLngPoint_Animation[i].lng()+"\nSpeed(km/h):"+(parseFloat(arr[i].speed)*1.852).toFixed(2)+"\nDateTime : "+arr[i].dateTime);
			stopTimerFunction();			
		}
		else
		{	
			endMarker.setPosition(latLngPoint_Animation[i]);
			if(ignition_status[i]=="ON")
			{
				if((parseFloat(arr[i].speed)*1.852) < arr[i].speedLimit)
					endMarker.setIcon(ignition_ON_icon);
				else
					endMarker.setIcon(ignition_ON_speed_icon);
			}
			else
				endMarker.setIcon(ignition_OFF_icon);
			endMarker.setTitle("Ignition : "+ignition_status[i]+"\nLat : "+latLngPoint_Animation[i].lat()+"\nLng : "+latLngPoint_Animation[i].lng()+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno+"\nAngle :"+arr[i].angle+"\nDigital IO :"+arr[i].digitalio);
			if(correct_speed)
			{
 				pre_lat=parseFloat(latLngPoint_Animation[i].lat());
 				pre_lng=parseFloat(latLngPoint_Animation[i].lng());
 				if(i < latLngPoint.length-1)
 				{	
 					cur_lat=parseFloat(latLngPoint[i+1].lat());
 					cur_lng=parseFloat(latLngPoint[i+1].lng());
 				}
 				else
 				{
 					cur_lat=parseFloat(latLngPoint_Animation[i].lat());
 					cur_lng=parseFloat(latLngPoint_Animation[i].lng());
 				}
				if(cur_lat >= 0 && cur_lng >= 0)// Only for North-East (i.e latitude[0 to +90] and langitude[0 to +180])
				{
					if(cur_lat == pre_lat && cur_lng == pre_lng)//ok
					{
						slow_icon=slow_icon;
						speed_icon=speed_icon;
					}
					else if(cur_lat < pre_lat && cur_lng == pre_lng)//ok
					{
						slow_icon=slow_icon_b;
						speed_icon=speed_icon_b;
					}
					else if(cur_lat > pre_lat && cur_lng == pre_lng)//ok
					{
						slow_icon=slow_icon_t;
						speed_icon=speed_icon_t;
					}
					else if(cur_lat == pre_lat && cur_lng > pre_lng)//ok
					{
						slow_icon=slow_icon_r;
						speed_icon=speed_icon_r;
					}
					else if(cur_lat == pre_lat && cur_lng < pre_lng)//ok
					{
						slow_icon=slow_icon_l;
						speed_icon=speed_icon_l;
					}
					else if(cur_lat > pre_lat && cur_lng > pre_lng)//ok
					{
						slow_icon=slow_icon_tr;
						speed_icon=speed_icon_tr;
					}
					else if(cur_lat < pre_lat && cur_lng < pre_lng)//ok
					{
						slow_icon=slow_icon_bl;
						speed_icon=speed_icon_bl;
					}
					else if(cur_lat < pre_lat && cur_lng > pre_lng)//ok
					{
						slow_icon=slow_icon_br;
						speed_icon=speed_icon_br;
					}
					else if(cur_lat > pre_lat && cur_lng < pre_lng)//ok
					{
						slow_icon=slow_icon_tl;
				 		speed_icon=speed_icon_tl;
					}
				}

				
				if((parseFloat(arr[i].speed)*1.852)<arr[i].speedLimit)
					wayPointMarkers[j]=new google.maps.Marker({position:latLngPoint_Animation[i], map:map , icon: slow_icon});
				else if((parseFloat(arr[i].speed)*1.852)>=arr[i].speedLimit)
					wayPointMarkers[j]=new google.maps.Marker({position:latLngPoint_Animation[i], map:map , icon: speed_icon});
						
				wayPointMarkers[j].setTitle("Ignition : "+ignition_status[i]+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDate&Time : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno+"\nAngle :"+arr[i].angle+"\nDigital IO :"+arr[i].digitalio);
				google.maps.event.addListener(wayPointMarkers[j], 'click', function() {
         			document.getElementById('info').value=this.getTitle(); }); 
				if(ignition_status[i]=="OFF")
				{		
					j++;			
					wayPointMarkers[j]=new google.maps.Marker({position:latLngPoint_Animation[i], map:map , icon: way_igoff_icon});
					wayPointMarkers[j].setTitle("Ignition : "+ignition_status[i]+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDate&Time : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno+"\nAngle :"+arr[i].angle+"\nDigital IO :"+arr[i].digitalio);
					google.maps.event.addListener(wayPointMarkers[j], 'click', function() {
						document.getElementById('info').value=this.getTitle(); });
				}
				j++;
			}else if(ignition_status[i]=="OFF")
			{				
				wayPointMarkers[j]=new google.maps.Marker({position:latLngPoint_Animation[i], map:map , icon: way_igoff_icon});
				wayPointMarkers[j].setTitle("Ignition : "+ignition_status[i]+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDate&Time : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno+"\nAngle :"+arr[i].angle+"\nDigital IO :"+arr[i].digitalio);
				google.maps.event.addListener(wayPointMarkers[j], 'click', function() {
         			document.getElementById('info').value=this.getTitle(); });
				j++;	
			}
		}
		map.setCenter(latLngPoint_Animation[i]);
		if(zoomCount==0)//to allow auto zoom once
		{
			map.setZoom(12);zoomCount = 1;
			selectedVehicleMarker.setMap(null);
			startMarke.setMap(map);
			endMarker.setMap(map);
			backTrackRoute.setMap(map);			
		}    		
		out="Ignition : "+ignition_status[i]+"\nLat : "+(latLngPoint_Animation[i].lat()).toFixed(6)+"\nLng : "+(latLngPoint_Animation[i].lng()).toFixed(6)+"\nSpeed["+arr[i].speedLimit+"] : "+(parseFloat(arr[i].speed)*1.852).toFixed(2)+" (km/h)\nDateTime : "+arr[i].dateTime+"\nGPS DateTime :"+arr[i].gpsdatetime+"\nRunnig No:"+arr[i].runno+"\nAngle :"+arr[i].angle+"\nDigital IO :"+arr[i].digitalio;
		distance=(google.maps.geometry.spherical.computeLength(latLngPoint_Animation)).toFixed(3);
		if(distance>=1000)
		{
			distance=(distance/1000).toFixed(3);
			distance+="km";
		}
		else
			distance+="m";
		out+="\nDistance : "+distance;
		endMarker.setTitle(out);
		document.getElementById('info').value="Playing-->"+i+"/"+(latLngPoint.length-1)+"\n"+out;	
	}	
	if(i == latLngPoint.length-1)
	{
		//clearInterval(Animation);
		document.getElementById('info').value="Completed\n"+out;
		is_AnimationComplete=true;	
		//alert(i+"/"+latLngPoint_Animation.length+"/ "+latLngPoint.length)	;
	}
}
function cursor_status(status)
{
	//wait or auto
	document.getElementById("bodyTag").style.cursor = status;
	document.getElementById('imeino').style.cursor = status;
	document.getElementById('Fdaytime').style.cursor = status;
    document.getElementById('Tdaytime').style.cursor = status;
    document.getElementById('googleMap').style.cursor = status;
}


function clearAllMarker()//to clear all markers from the map 
{
	var selectedPeriod=document.getElementById('TimeSelection').value; 		
 	if(selectedPeriod == "custom" && document.getElementById('Replay').checked==true )
 	{
 		document.getElementById("Fdaytime").readOnly=false;
 		document.getElementById("Tdaytime").readOnly=false;
 	}
 	else
 	{
 		document.getElementById("Fdaytime").readOnly=true;
 		document.getElementById("Tdaytime").readOnly=true;
 	}	
	stopTimerFunction();
	//alert("clearAllMarker--1");
	for( j = 0; j < arrayMarker.length; j++)
	{
		arrayMarker[j].setMap(null);
	}
	//alert("arrayMarker_last--2");
	//alert("waypoints"+wayPointMarkers.length)
	for( j = 0; j < wayPointMarkers.length; j++)
	{
		wayPointMarkers[j].setMap(null);
	}
	//alert("wayPointMarkers_last--3");	
	wayPointMarkers=[];
	arrayMarker=[];
	selectedVehicleMarker.setMap(null);
	startMarke.setMap(null);
	endMarker.setMap(null);	
	bulk_start.setMap(null);
	bulk_end.setMap(null);
	backTrackRoute.setMap(null);
	LastMarkers.setMap(null);
	zoomCount=0;
	setBoundCount=0;
	document.getElementById('info').value="";
	is_AnimationComplete=true;
	latlngbounds = new google.maps.LatLngBounds();
	//alert("clearAllMarker_last--4");	
}

function radio_btn_click()
{
	clearAllMarker();
	show_message();		
	zoomCount=0;
	stopTimerFunction();
	if(document.getElementById('Replay').checked==true)
	{
		document.getElementById('BulkPlotting').checked=true;
		document.getElementById("PlottingType").style.display ="";		
	    document.getElementById("report").disabled=false;
	    document.getElementById("TimeSelection").disabled=false;
	}
	else
	{
		document.getElementById("PlottingType").style.display ="none";		
		document.getElementById("TimeSelection").disabled=true;				 
 	    document.getElementById("Fdaytime").readOnly=true;
 	    document.getElementById("Tdaytime").readOnly=true;
	    document.getElementById("report").disabled=true;
	}	    
}


function plotting_radio_btn_click()
{
	clearAllMarker();	
	stopTimerFunction();
	show_message();	    
}

function reportFunction()
{
// 	var vh_id=document.getElementById('vehicle').value;
// 	var fdate=document.getElementById('Fdaytime').value;
//     var tdate=document.getElementById('Tdaytime').value;
//     var baseUrl=document.getElementById('ReportURL').value;
// 	var url = baseUrl+"/"+vh_id+"/"+fdate+"/"+tdate;
// 	window.location.assign(url);
}

/*
 * This function is used to change the From and To date&time corresponding to the
 * Time selection choise.
 */
 function time_selection_change()
 { 
 	clearAllMarker();
 	var baseUrl=document.getElementById('URL').value;
 	var clientID=document.getElementById('ClientID').value; 	
 	var selectedPeriod=document.getElementById('TimeSelection').value; 	
 	if(clientID=='')
 	{ 	 	
 		document.getElementById('info').value="Please select the client first.";
 		alert(document.getElementById('info').value);
 	}
 	else
 	{
 		var xmlhttp = new XMLHttpRequest();
 		var url = baseUrl+"/get_time/"+clientID+"/"+selectedPeriod;	
 		if(selectedPeriod != "custom" )
	 	{
	 		document.getElementById("Fdaytime").readOnly=true;
 			document.getElementById("Tdaytime").readOnly=true; 		
 			xmlhttp.onreadystatechange=function() 
 			{
 		    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
 	    	
 		        	show_time(xmlhttp.responseText);
 		    	}
 			}
 			xmlhttp.open("GET", url, true);
 			xmlhttp.send();
 		}
 		else
 		{	
 			show_message();
 			document.getElementById("Fdaytime").readOnly=false;
 			document.getElementById("Tdaytime").readOnly=false;
 		} 	
 	}
 }
function show_message()
{
	if(document.getElementById('StepByStepPlotting').checked==true) 
			document.getElementById("info").value="Note: Date & Time interval must be within 24Hr..";
}
 function show_time(response)
 { 	
	 show_message(); 	    
     var arrs = JSON.parse(response);         
     document.getElementById("Fdaytime").value=arrs[0].FromDate;
     document.getElementById("Tdaytime").value=arrs[0].ToDate;  
 }

 function distance_test(lat1, lon1, lat2, lon2) 
 {
 		//alert("Hi");
 	    var radlat1 = Math.PI * lat1/180;
 	    var radlat2 = Math.PI * lat2/180;
 	    var radlon1 = Math.PI * lon1/180;
 	    var radlon2 = Math.PI * lon2/180;
 	    var theta = lon1-lon2;
 	    var radtheta = Math.PI * theta/180;
 	    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
 	    dist = Math.acos(dist);
 	    dist = dist * 180/Math.PI;
 	    dist = dist * 60 * 1.1515;
 	    dist = dist * 1.609344;
 	    //alert("test3 "+ dist);
 	    return dist;
 }


</script>
<style type="text/css">
.nav {
	line-height: 14px;/*30px;*/
	background-color: #eeeeee;
	float: left;
	padding: 3px;
}
.divx {
	/*position: absolute;
	width: 100%;*/
	height: 637px;/*574px;508px;*/
	/*float: left;
	margin-right: 5px;*/
}
@CHARSET "ISO-8859-1";
</style>
</head>

<body id="bodyTag" onload="initMap()"><!--initMap  function_onload_page-->
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
      <?php
      $home_link="";
      if($this->session->userdata('login')!=null)
      {
      	$sess_details=$this->session->userdata('login');
      	$user_type=$sess_details['sess_user_type'];
      	$is_admin=$sess_details['sess_is_admin'];
      if($user_type == AUTOGRADE_USER)
      	$home_link="home_ctrl";
      else if($user_type == OHTER_CLIENT_USER)
      {
      	if($is_admin == '1')
      		$home_link="home_ctrl";
      	else
      		$home_link="vehicle_admin_tracking_ctrl";
      }
      else if($user_type == INDIVIDUAL_CLIENT)
      	$home_link="individual_vt_ctrl";
      else if($user_type==DISTRIBUTOR_USER)
      	$home_link="distributor_dashboard_ctrl";
      else if($user_type==DEALER_USER)
      	$home_link="dealer_dashboard_ctrl";
      else if($user_type==PARENT_USER)
      	$home_link="individual_vt_ctrl";
      }else{$home_link="login_ctrl";}
      ?>
        <h3><a href="<?php echo base_url('index.php/'.$home_link)?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
         <h5><?php if ($this->session->userdata ( 'login' )){ echo $ID['sess_client_name']; }?></h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
      <?php if ($this->session->userdata ( 'login' )){ ?>
       <h6><?php echo $ID['sess_user_name']; ?></h6>
       <h6><a href= "<?php echo base_url('index.php/change_password_ctrl')?>">Change Password</a> | <a href= "<?php echo base_url('index.php/home_ctrl/logout')?>">Logout</a></h6>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>
           <!-- --------------------------------------------------------------- -->
      	    
<?php if ($this->session->userdata ( 'login' )):?>
<?php echo $GLOBALS ['main_menu']; ?>
<?php endif; ?>
			
          <!-- -------------------------------------------------------- --> 
          </div>
        </div>
        <div>
          <h5>Autograde T.A.N.K. Service</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank"> by Autograde </a></em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right"><em>
       <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?></em></div>
    </div>
  </div>
</div>
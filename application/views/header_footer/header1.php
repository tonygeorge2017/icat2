<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">

<link href="<?php echo base_url('assets/css/dashboard.css')?>" rel="stylesheet">

<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
</head>
<body>
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h3><a href="<?php echo base_url('index.php/login_ctrl')?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
        <h5>Autograde International Pvt. Ltd.</h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
       <h6><?php  if ($this->session->userdata ( 'login' )){
       echo ((isset($ID['sess_user_name']))?$ID['sess_user_name']:'');} ?></h6>
             <?php if ($this->session->userdata ( 'login' )){?>
        <h6><a href= "<?php echo base_url('index.php/home_ctrl/logout') ?> ">Logout</a></h6><?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
        
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <?php if($screen=="login"):?>           
            <li><a href="<?php echo base_url("index.php/forget_password_ctrl")?>">Forgot Password</a></li>
            <?php else:?>
            <li><a href="<?php echo base_url("index.php/login_ctrl")?>">Login</a></li>
            <?php endif;?>
            <li><a href="#">Help</a></li>
               <!-- <li> <a href="<?php echo base_url("index.php/user_management_ctrl")?>">User Management</a> </li>
              <li> <a href="<?php echo base_url("index.php/client_ctrl/")?>">Client Details</a> </li>
              <li> <a href="<?php echo base_url("index.php/client_license_ctrl")?>">Client License </a> </li>
              <li> <a href="<?php echo base_url("index.php/vehicle_group_ctrl")?>">Vehicle Group</a> </li>
              <li> <a href="<?php echo base_url("index.php/driver_group_ctrl")?>">Driver Group</a> </li>
              <li> <a href="<?php echo base_url("index.php/dealer_ctrl")?>">Dealer</a> </li>
              <li> <a href="<?php echo base_url("index.php/device_type_ctrl")?>">Device Type</a> </li>
              <li> <a href="<?php echo base_url("index.php/device_ctrl")?>">Device</a> </li>
              <li> <a href="<?php echo base_url("index.php/driver_ctrl")?>">Driver</a> </li>-->
              
            </ul>
          </div>
        </div>
        <div>
          <h5>Autograde T.A.N.K. Service</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank"> by Autograde </a></em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right"><em>
       <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?></em></div>
    </div>
  </div>
</div>
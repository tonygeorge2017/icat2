<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/dashboard.css')?>" rel="stylesheet">
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/simple-sidebar.css')?>" rel="stylesheet">
<script src="//maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>&libraries=places"></script>
<link href="<?php echo base_url('assets/src/css/bootstrapValidator.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/src/js/bootstrapValidator.js')?>"></script>
<!-- jQuery -->
<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>

<link href="<?php echo base_url('assets/css/jquery-ui.css')?>" rel="stylesheet"></link>
 	<script src="<?php echo base_url('assets/js/jquery-1.10.2.js')?>"></script>
 	<script src="<?php echo base_url('assets/js/jquery-ui.js')?>"></script>

<script>
$(document).ready(function(){
  $('.dropdown-submenu a.not_close_onclick_menu').on("click", function(e){
    //$(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});
</script>
<script>
var map, toZoom, arr, i, j, stopSearch, stopFree, stopSelected, searchMarker;
var arrayMarker=[];
var zoomCount=0, animateZoomCount=0, operation="", jsonClient="", jsonVhGp="", jsonVehicle="", vehicleID="", jsonPlaceTitle="", jsonLat="", jsonLng="", jsonRemarks="", stopID=0;
const MIN_ZOOM_SIZE=2, MEDIUM_ZOOM_SIZE=4, EXTRA_ZOOM_SIZE=10, MAX_ZOOM_SIZE=17;//set zoom size constant;
const ZOOM_STEP_CENTRE=1, ZOOM_STEP_1=2, ZOOM_STEP_2=3;

/*
 * This function used to initilize the map related this like define the
 * property of map, markers, icons and add listener to the map and markers.
 */
function initMap()
{	
	stopSearch = document.getElementById("ImageURL").value+"/stop_search.png";
	stopFree = document.getElementById("ImageURL").value+"/stop_free.png";
	stopSelected = document.getElementById("ImageURL").value+"/stop_selected.png";

	var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:MIN_ZOOM_SIZE,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			  };			  
	map=new google.maps.Map(document.getElementById("GoogleMap"),mapProp);
	//add event listener to the map(click) 	
	google.maps.event.addListener(map, 'click', function(event) {
		
			var lat = event.latLng.lat();
		    var lng = event.latLng.lng();
		    var l_latLngPoint=new google.maps.LatLng(lat,lng);    	
		    show_marker_mapClick(l_latLngPoint);
		 });
	//add event listener to the map(Zoom change)
	google.maps.event.addListener(map, 'zoom_changed', function() {
		var zoomLevel=map.getZoom();			
		if(zoomLevel < MIN_ZOOM_SIZE)
		{	
			map.setZoom(MIN_ZOOM_SIZE);			
		}		
		//alert(zoomLevel);	
	});
	/* Add click event listener to searchMarker to add are remove the stops in stop table*/
	searchMarker = new google.maps.Marker({icon:stopSearch, draggable:true});
	google.maps.event.addListener(searchMarker, 'click', function() {
		marker_clicked(this);
	});
	
	var input = (document.getElementById('SearchLocation'));	
	var searchBox = new google.maps.places.SearchBox((input));
	google.maps.event.addListener(searchBox, 'places_changed', function() {
		document.getElementById('StopID').value="";
	    var places = searchBox.getPlaces();	     
	    map.setZoom(MIN_ZOOM_SIZE);
	    animateZoomCount=0;
	    toZoom=setInterval(function(){animate_zoom(places)},1300);
	    });	
} 
/*
 * This function called by the map double click event and place the marker (i.e searchMarker)
 * exactly on the place where we done double click.
 *
 * @param latLang   
 */
function show_marker_mapClick(latLng)
{
	//alert("show_marker_mapClick function");
	searchMarker.setTitle(" ");
	searchMarker.setPosition(latLng);
	searchMarker.setMap(map);	
	searchMarker.setVisible(true);
}
/*
 * This function will check whether the current marker 
 * is selected one or not and change the it.
 * (i.e if click on selected marker then, it will be free and change the icon as free icon)
 *
 * @param marker
 */
function marker_clicked(marker)
{
	//alert("marker_clicked function: "+marker.getIcon());
	if((document.getElementById('ClientID').value).trim()!="" && (document.getElementById('VehicleGroup').value).trim()!="")
	{
		//assign values like clientID, vehicelGpId, vehicleID, lat & lng to hidden field of the model
		// and place name to stop name field.
		this.assign_value(marker);
		if(marker.getIcon()==stopSearch)
		{
			//alert("marker is stopSearch");
			//Invoke the model to add the details to vts_stops_list table.
			document.getElementById('AddBtn').click();
		}else if(marker.getIcon()==stopFree)
		{
			//alert("marker is stopFree");
			if(vehicleID!="")
			{
				var r = confirm("Do you want to add "+marker.getTitle()+" stop to this vehicle?");
				if (r == true) {
					//alert("marker is stopFree 1");
					operation="add";
					add_remove_stop("add", marker);
				}
			}
			else
				{
					document.getElementById("Info").value = "Error!\nPlease select the vehicle first..";
					alert("Error!\nPlease select the vehicle first..");
				}
		}
		else
		{
			//alert("marker is stopSelected");
			if(vehicleID!="")
			{
				var r = confirm("Do you want to remove "+marker.getTitle()+" stop from this vehicle?");
				if (r == true) {
					//alert("marker is stopSelected 1");
					operation="remove";
					add_remove_stop("remove", marker);
				}
			}
			else
			{
				document.getElementById("Info").value = "Error!\nPlease select the vehicle first..";
				alert("Error!\nPlease select the vehicle first..");
			}
		}
	}else
	{		
		document.getElementById("Info").value = "Error!\nPlease select the client and vehicle group..";
		alert("Error!\nPlease select the client and vehicle group..");
	}
}
/*
 * This function used to add or remove the current vehicle from vts_vehicle_stops table.
 */
function add_remove_stop(operation, marker)
{
	//alert("add_remove_stop function");
	var baseUrl=document.getElementById('URL').value;
	if(vehicleID!="")
	{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/add_remove_vhStop/"+stopID+"/"+vehicleID+"/"+operation;	
		//alert("add_remove_stop url"+url);
		xmlhttp.onreadystatechange=function() {
    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {    	
    		var l_arr = JSON.parse(xmlhttp.responseText);
    		if(l_arr.result=="1"){
	    		if(operation=="add")
	    			marker.setIcon(stopSelected);
	    		else if(operation=="remove")
	    			marker.setIcon(stopFree);
    			}
	    	}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}else{
		document.getElementById("Info").value = "Error!\nPlease select the vehicle first..";
		alert("Error!\nPlease select the vehicle first..");			
	}
}
/*
 * In this function I just try to do the three set zooming effect.
 * It first pan the map to the search location. And Zoom little bit show the
 * country first, State second and finally search location along with marker.
 * 
 *@param place object 
 */
function animate_zoom(places)
{
	//alert("animate_zoom function "+animateZoomCount);
	animateZoomCount++;		
	searchMarker.setPosition(places[0].geometry.location);
	searchMarker.setTitle(places[0].name);
	
	if(animateZoomCount==ZOOM_STEP_CENTRE)
	{				
		map.panTo(searchMarker.getPosition());
		map.setZoom(MEDIUM_ZOOM_SIZE);
	}
	else if(animateZoomCount==ZOOM_STEP_1)
	{
		map.setZoom(EXTRA_ZOOM_SIZE);		
	}
	else if(animateZoomCount==ZOOM_STEP_2)
	{
		map.setZoom(MAX_ZOOM_SIZE);
		searchMarker.setMap(map);
	}
	else
    	clearInterval(toZoom);    
}
/*
 * This function is used to assign all the values to following global variables
 * jsonClient, jsonVhGp, jsonVehicle, jsonPlaceTitle, jsonLat & jsonLng;
 */
function assign_value(marker)
{	
	//alert("assign_value function "+marker.getZIndex());
	stopID=marker.getZIndex();
	var l_clientID=document.getElementById('ModelClientID').value=((document.getElementById('ClientID').value).trim()!="")?document.getElementById('ClientID').value:"";
	jsonClient="\"clientID\":\""+l_clientID+"\"";
	var l_title=document.getElementById('StopName').value=marker.getTitle();
	jsonPlaceTitle="\"placeTitle\":\""+l_title+"\"";
	var l_vhGpID=document.getElementById('ModelVehicleGroup').value=((document.getElementById('VehicleGroup').value).trim()!="")?document.getElementById('VehicleGroup').value:"";
	jsonVhGp="\"vhGpID\":\""+l_vhGpID+"\"";
	var l_vhID = vehicleID = document.getElementById('ModelVehicle').value=((document.getElementById('Vehicle').value).trim()!="")?document.getElementById('Vehicle').value:"";
	jsonVehicle="\"vhID\":\""+l_vhID+"\"";	
	var l_lat=document.getElementById('Latitude').value=marker.position.lat();
	jsonLat="\"lat\":\""+l_lat+"\"";	
	var l_lng=document.getElementById('Longitude').value=marker.position.lng();
	jsonLng="\"lng\":\""+l_lng+"\"";	
	
}
/*
 * This function is used to save the stop details in
 * vts_stops_list table.
 */
function save_stop_details()
{
	//alert("save_stop_details function");
	
	if((document.getElementById('StopName').value).trim()!="")
	{
		var l_title=(document.getElementById('StopName').value).trim();
		jsonPlaceTitle="\"placeTitle\":\""+l_title+"\"";
		
		var l_remarks=((document.getElementById('Remarks').value).trim()!="")?document.getElementById('Remarks').value:"";
		jsonRemarks="\"remarks\":\""+l_remarks+"\"";
		
		var json_array="{"+jsonClient+","+jsonPlaceTitle+","+jsonVhGp+","+jsonVehicle+","+jsonLat+","+jsonLng+","+jsonRemarks+"}";
		
		var assignValue=encodeURIComponent(json_array);		
		var xmlhttp = new XMLHttpRequest();
		var l_url = document.getElementById("SaveURL").value+"/store_stop_details/"+assignValue;
		//alert(l_url);			
		//alert("Test");	
		xmlhttp.onreadystatechange=function() {
	    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
	    		//alert("got respond");
	    		push_marker(xmlhttp.responseText);		    			
	    	}
		}
		xmlhttp.open("GET", l_url, true);
		xmlhttp.send();
	}
	else{
		document.getElementById("Info").value = "Error!\nPlease enter the stop name..";
		alert("Error!\nPlease enter the stop name..");
	}
}
/*
 * This function push the new marker into the arrayMarker[] array.
 */
function push_marker(response)
{
	//alert("push_marker function");
	//alert(response);   
	var l_arr = JSON.parse(response);	
	if(l_arr.result=="success")
	{
		var l_latLngPoint=new google.maps.LatLng(l_arr.lat,l_arr.lng);	
		var l_marker=new google.maps.Marker({
			map: map, 
			position: l_latLngPoint, 
			icon: stopFree, 
			title: l_arr.placeTitle, 
			visible: true,
			zIndex: parseInt(l_arr.stopID)
			});
		
		google.maps.event.addListener(l_marker, 'click', function() {
			marker_clicked(this);
		});		
		//alert("push_marker function: success");
		if(l_arr.selected=="yes")
		{
			//alert("push_marker function: yes");
			l_marker.setIcon(stopSelected);
		}
		arrayMarker.push(l_marker);//push the marker to the marker array.		
		searchMarker.setVisible(false);
		//Close the model after add the details to vts_stops_list table.
		document.getElementById('CloseModelBtn').click();
		//alert("push_marker function:last");
	}
	else
	{
		document.getElementById("Info").value = "Error!\nPlease check all fields..";
		alert("Error!\nPlease check all fields..");
	}
}
/*
 This function is used to refesh the arrayMarker and plot the marker
 * on the map for selected vehicle group and vehicle. This function will
 * called when onselection change of client, vehicle group & vehicle.
 */
function refresh_arrayMarker()
{	
	//alert("refresh_arrayMarker function");
	clearAllMarker();
	var latlngbounds = new google.maps.LatLngBounds();
	var l_vhGpID=document.getElementById('ModelVehicleGroup').value=((document.getElementById('VehicleGroup').value).trim()!="")?document.getElementById('VehicleGroup').value:"";
	var l_vhID=document.getElementById('ModelVehicle').value=((document.getElementById('Vehicle').value).trim()!="")?document.getElementById('Vehicle').value:"";
	var xmlhttp = new XMLHttpRequest();
	var l_url = document.getElementById("SaveURL").value+"/get_vhgrp_markers/"+l_vhGpID;
	l_url=(l_vhID!="")?l_url+"/"+l_vhID : l_url;
	xmlhttp.onreadystatechange=function() {
    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
    		//alert("refresh_arrayMarker:got respond");    		
    		var l_arr = JSON.parse(xmlhttp.responseText);
    		var l_icon=stopFree;
    		if(l_arr.length!=0)
    	    {
    			for(i = 0; i < l_arr.length; i++)
        		{   
    				 //alert(l_arr[i].stop_id); 		
    				var l_latLngPoint=new google.maps.LatLng(l_arr[i].stop_latitude,l_arr[i].stop_longitude);    				   					
    				arrayMarker[i]=new google.maps.Marker({
    					map: map, 
    					position: l_latLngPoint, 
    					icon: stopFree, 
    					title: l_arr[i].stop_name,    					 
    					visible: true,
    					zIndex: parseInt(l_arr[i].stop_id)	
    					});		
    				google.maps.event.addListener(arrayMarker[i], 'click', function() {
    					marker_clicked(this);    				
    				});  
    				l_icon=(l_vhID!="" && l_vhID == l_arr[i].vh_vehicle_id)? stopSelected : stopFree;
    				//alert("SelVhid: "+l_vhID+", Vhid: "+l_arr[i].vh_vehicle_id+", Icon: "+l_icon);
    				arrayMarker[i].setIcon(l_icon); 
    				latlngbounds.extend(arrayMarker[i].position);				  			    			        	
        		}
    			map.setCenter(latlngbounds.getCenter());
				map.fitBounds(latlngbounds);
    	    }	    			
    	}
	}
	xmlhttp.open("GET", l_url, true);
	xmlhttp.send();	
}
/*
 * This function is used to clear all the arraymarker,
 */
function clearAllMarker() 
{	
	document.getElementById('StopID').value="";
	document.getElementById('SearchLocation').value="";
	for( j = 0; j < arrayMarker.length; j++)
	{
		arrayMarker[j].setMap(null);//to clear all markers from the map
	}
	arrayMarker=[];//Delete all the arrayMarker
}

function get_vhl_gp_vehicle()
{
	clearAllMarker();
	var baseUrl=document.getElementById('URL').value;
	var vehicleGp=document.getElementById('VehicleGroup').value;
	if(VehicleGroup!=null)
	{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_vhl_grp_vehicle/"+vehicleGp;	
		xmlhttp.onreadystatechange=function() {
	    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	        	vhl_gp_vehicle(xmlhttp.responseText);
	    	}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	refresh_arrayMarker();
}

function vhl_gp_vehicle(response)
{
	document.getElementById("Vehicle").innerHTML=null;
	var x=document.getElementById("Vehicle");
	var option=document.createElement("option");
	option.text="";
    option.value="";
    x.add(option);    
    var arrs = JSON.parse(response);
    var id="";var vehicleName="";
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vh_id;
      vehicleName=arrs[j].vh_name;
      adds(id,vehicleName);    
    }
}
function adds(id,vehicleName)
{
	var x=document.getElementById("Vehicle");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}
function get_client()
{
	clearAllMarker();
	var baseUrl=document.getElementById('URL').value;
	var clientID=document.getElementById('ClientID').value;
	var sessClientID=document.getElementById('SessClient').value;
	var sessUserID=document.getElementById('SessUser').value;
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_client_vhgp/"+clientID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	        client_vhl_gp(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function client_vhl_gp(response)
{	
	document.getElementById("VehicleGroup").innerHTML=null;
	document.getElementById("Vehicle").innerHTML=null;
	var x=document.getElementById("VehicleGroup");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleGp="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vh_gp_id;
      vehicleGp=arrs[j].vh_gp_name;
      adds_vh_gp(id,vehicleGp);    
    }
    get_vhl_gp_vehicle();    
}
function adds_vh_gp(id,vehicleGp)
{
	var x=document.getElementById("VehicleGroup");
	var option=document.createElement("option");
	option.text=vehicleGp;
    option.value=id;
    x.add(option);
}


</script>
<style type="text/css">
.nav {
	line-height: 14px;/*30px;*/
	background-color: #eeeeee;
	float: left;
	padding: 3px;
}
.divx {
	/*position: absolute;
	width: 100%;*/
	height: 637px;/*574px;508px;*/
	/*float: left;
	margin-right: 5px;*/
}
@CHARSET "ISO-8859-1";
</style>
</head>

<body id="BodyTag" onload="initMap()"><!--initMap  function_onload_page-->
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
      <?php
      $home_link="";
      if($this->session->userdata('login')!=null)
      {
      	$sess_details=$this->session->userdata('login');
      	$user_type=$sess_details['sess_user_type'];
      	$is_admin=$sess_details['sess_is_admin'];
      if($user_type == AUTOGRADE_USER)
      	$home_link="home_ctrl";
      else if($user_type == OHTER_CLIENT_USER)
      {
      	if($is_admin == '1')
      		$home_link="home_ctrl";
      	else
      		$home_link="vehicle_tracking_ctrl";
      }
      else if($user_type == INDIVIDUAL_CLIENT)
      	$home_link="individual_vt_ctrl";
      else if($user_type==DISTRIBUTOR_USER)
      	$home_link="distributor_dashboard_ctrl";
      else if($user_type==DEALER_USER)
      	$home_link="dealer_dashboard_ctrl";
      else if($user_type==PARENT_USER)
      	$home_link="individual_vt_ctrl";
      }else{$home_link="login_ctrl";}
      ?>
        <h3><a href="<?php echo base_url('index.php/'.$home_link)?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
         <h5><?php if ($this->session->userdata ( 'login' )){ echo $ID['sess_client_name']; }?></h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
      <?php if ($this->session->userdata ( 'login' )){ ?>
       <h6><?php echo $ID['sess_user_name']; ?></h6>
       <h6><a href= "<?php echo base_url('index.php/change_password_ctrl')?>">Change Password</a> | <a href= "<?php echo base_url('index.php/home_ctrl/logout')?>">Logout</a></h6>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>    	    
 <!-- --------------------------------------------------------------- -->
      	    
<?php if ($this->session->userdata ( 'login' )):?>
<?php echo $GLOBALS ['main_menu']; ?>
<?php endif; ?>

          </div>
        </div>
        <div>
          <h5>Vehicle Tracking Software</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank">by Autograde</a> </em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right">
      <em> <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?>
        <?php // echo date('d - F Y')?></em></div>
    </div>
  </div>
</div>
<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12"><a href="<?php echo base_url('index.php/terms_of_use');?>">Terms of Use</a> | <a href="<?php echo base_url('index.php/about_autograde');?>">About Autograde</a> | <a href="<?php echo base_url('index.php/contact_us');?>">Contact Us</a> | &copy; <?php echo date("Y") . "&nbsp" ;echo "Vehicle Tracking System" ;?> </div>
    </div>
  </div>
</footer>
<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-table/src/bootstrap-table.css')?>">
<!-- jQuery -->
<script src="<?php //echo base_url('assets/js/jquery.js')?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script><!-- This is required for menu and Progress bar -->
<script src="<?php echo base_url('assets/bootstrap-table/src/bootstrap-table.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table/src/extensions/group-by-v2/bootstrap-table-group-by.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table/src/extensions/export/bootstrap-table-export.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table/src/extensions/jsPDF/jspdf.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table/src/extensions/jsPDF-AutoTable/jspdf.plugin.autotable.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table/src/tableExport.js')?>"></script>
<?php if( file_exists( 'assets/js/'.( strtolower( $this->router->fetch_class() ) ).'.js' ) ): ?>
	<script src="<?= base_url( 'assets/js/'.( strtolower( $this->router->fetch_class() ) ).'.js' ) ?> "></script>
<?php endif; ?>
<!-- Script to Activate the Carousel -->
</body>
</html>
<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12"><a href="<?php echo base_url('index.php/terms_of_use');?>">Terms of Use</a> | <a href="<?php echo base_url('index.php/about_autograde');?>">About Autograde</a> | <a href="<?php echo base_url('index.php/contact_us');?>">Contact Us</a> | &copy; <?php echo date("Y") . "&nbsp" ;echo "Vehicle Tracking System" ;?> </div>
    </div>
  </div>
</footer>
<!-- jQuery -->
<script src="<?php //echo base_url('assets/js/jquery.js')?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script><!-- This is required for menu and Progress bar -->
<!-- Script to Activate the Carousel -->
</body>
</html>
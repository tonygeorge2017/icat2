<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/simple-sidebar.css')?>" rel="stylesheet">
<!-- jQuery -->
<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<script src="//maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>&libraries=places"></script>
<script src="<?php echo base_url('assets/js/jqBootstrapValidation.js')?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script>
$(document).ready(function(){
  $('.dropdown-submenu a.not_close_onclick_menu').on("click", function(e){
    //$(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});
</script>
<script>

var map, start_icon, startMarke, endMarke, searchMarker, stopSearch , vhRoutePolyLine, GeoFence;
var arrayVhRouteMarker=[];
var latLngPoint=[];
var arrayVhRouteMarkerDetails={'client_id':'','route_id':'','route_name':'','details':[],'undo':[]};
var j;
var slno=0;
var distance=0;
var animateZoomCount=0;
const MIN_ZOOM_SIZE=2, MEDIUM_ZOOM_SIZE=4, EXTRA_ZOOM_SIZE=10, MAX_ZOOM_SIZE=17;//set zoom size constant;
const ZOOM_STEP_CENTRE=1, ZOOM_STEP_1=2, ZOOM_STEP_2=3;
const MARKER_COLOR = "009BEE", MARKER_TEXT_COLOR = "FFFFFF";
var routeName="";
var latlngbounds;
var isGeoFence=0;
var infowindow;
var geocoder;
function initMap()
{	
	image_url = document.getElementById("MarkerPntURL").value;
	stopSearch = image_url+"/stop_search.png";	
	start_icon={
			url:image_url+"/test_end.png", 
			origin:new google.maps.Point(0,0),
			anchor:new google.maps.Point(15,15)
			};	
	latlngbounds = new google.maps.LatLngBounds();
	geocoder = new google.maps.Geocoder;
    infowindow = new google.maps.InfoWindow;
	var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:2,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			  };			  
	map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	map.addListener('click', addLatLng);
		
	startMarke=new google.maps.Marker({map:map, title:"Start Point", icon:start_icon, zIndex:0});
	endMarke=new google.maps.Marker({map:map, title:"Start Point", icon:start_icon, zIndex:0});
	google.maps.event.addListener(startMarke, 'click', function() {		
		document.getElementById('info').value=this.getTitle(); });	
	google.maps.event.addListener(endMarke, 'click', function() {
		document.getElementById('info').value=this.getTitle(); });	
	searchMarker = new google.maps.Marker({icon:stopSearch});
	google.maps.event.addListener(searchMarker, 'click', function() {
		searchMarker.setMap(null);
	});
	GeoFence = new google.maps.Polygon({	    
	    strokeColor: '#4DB849',
	    strokeOpacity: 0.8,
	    strokeWeight: 3,
	    fillColor: '#FF0000',
	    fillOpacity: 0.35
	  });
	GeoFence.setMap(map);
	vhRoutePolyLine=new google.maps.Polyline({
		  strokeColor:"#4DB849",
		  strokeOpacity:0.8,
		  strokeWeight:3,
		  editable:false
		  });
	vhRoutePolyLine.setMap(map);
	google.maps.event.addListener(map, 'zoom_changed', function() {
		var zoomLevel=map.getZoom();			
		if(zoomLevel < 2)
		{	
			map.setZoom(2);			
		}	
	});	
	var input = (document.getElementById('SearchLocation'));	
	var searchBox = new google.maps.places.SearchBox((input));
	google.maps.event.addListener(searchBox, 'places_changed', function() {		
	    var places = searchBox.getPlaces();	     
	    map.setZoom(MIN_ZOOM_SIZE);
	    animateZoomCount=0;
	    toZoom=setInterval(function(){animate_zoom(places)},1300);
	    });	
    get_route();
    routeName=document.getElementById("Route").value;
    //alert("Hi");
    if(latLngPoint.length >= 0)
	{
    	//alert("Inside if");
		document.getElementById("UndoBtn").disabled=false;
	}else{
		//alert("Inside else");
		document.getElementById("UndoBtn").disabled=true;
		}    
}
function geocodeLatLng(mak) {    
    var latlng = mak.getPosition();
    //alert(latlng);
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[2]) {                 
          infowindow.setContent(results[2].formatted_address);
          document.getElementById('info').value=results[1].formatted_address;
          infowindow.open(map, mak);
        } else {
          //window.alert('No results found');
          document.getElementById('info').value='No results found';
        }
      } else {
        //window.alert('Geocoder failed due to: ' + status);
        document.getElementById('info').value='Server busy';
      }
    });
  }
function get_route()
{	
	isGeoFence=document.getElementById("IsGeoFence").value;
	var routeID=document.getElementById("RouteID").value;	
	var baseUrl=document.getElementById("URL").value;	
	var json_markerDetails=JSON.stringify(arrayVhRouteMarkerDetails);
	//alert(json_markerDetails);
	var xmlhttp = new XMLHttpRequest();		
	var url = baseUrl+"/get_route_details/"+routeID;		
	xmlhttp.onreadystatechange=function() 
	{
		  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
		  {
			  //alert(xmlhttp.responseText);
			  var arr = JSON.parse(xmlhttp.responseText);
			  if(arr.length!=0)
			  {
				  //alert(arr.length);
				  arrayVhRouteMarkerDetails.details=arr;
				  for(i=0; i<arr.length; i++)
				  {
					  latLngPoint[i]=new google.maps.LatLng(arr[i].lat,arr[i].lng)
					  arrayVhRouteMarker[i] = new google.maps.Marker({
						    position: latLngPoint[i],
						    title: arr[i].name,
						    map: map,
						    icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + latLngPoint.length + "|" + MARKER_COLOR + "|" + MARKER_TEXT_COLOR,
						    zIndex:i
						  });
					  google.maps.event.addListener(arrayVhRouteMarker[i], 'click', function() {
							document.getElementById('info').value=this.getTitle();
							marker_clicked(this);});
					  google.maps.event.addListener(arrayVhRouteMarker[i], 'rightclick', function() {
						  geocodeLatLng(this);});
					  latlngbounds.extend(arrayVhRouteMarker[i].position);						
				  }
				  if(isGeoFence == 1)
				  {
				  	GeoFence.setPath(latLngPoint);
				  }
				  else
				  {				  
				  	vhRoutePolyLine.setPath(latLngPoint);
				  	startMarke.setPosition(arrayVhRouteMarker[0].getPosition());
				  	if((arrayVhRouteMarker.length)>0)				  		  
					  	endMarke.setPosition(arrayVhRouteMarker[(arrayVhRouteMarker.length)-1].getPosition());
				  }
				  map.setCenter(latlngbounds.getCenter());
		          map.fitBounds(latlngbounds);	
		          document.getElementById("ClearAll").disabled =true;					  	
			  }		       
		  }
	}
	xmlhttp.open("GET", url, true);	
	xmlhttp.send();
}
// function marker_clicked(marker)
// {
// 	document.getElementById("info").value="";
// 	marker.setMap(null);
// }
/*
 * In this function I just try to do the three set zooming effect.
 * It first pan the map to the search location. And Zoom little bit show the
 * country first, State second and finally search location along with marker.
 * 
 *@param place object 
 */
function animate_zoom(places)
{
	//alert("animate_zoom function "+animateZoomCount);
	document.getElementById("info").value="";
	animateZoomCount++;		
	searchMarker.setPosition(places[0].geometry.location);
	searchMarker.setTitle(places[0].name);
	
	if(animateZoomCount==ZOOM_STEP_CENTRE)
	{				
		map.panTo(searchMarker.getPosition());
		map.setZoom(MEDIUM_ZOOM_SIZE);
	}
	else if(animateZoomCount==ZOOM_STEP_1)
	{
		map.setZoom(EXTRA_ZOOM_SIZE);		
	}
	else if(animateZoomCount==ZOOM_STEP_2)
	{
		map.setZoom(MAX_ZOOM_SIZE);
		searchMarker.setMap(map);
	}
	else
    	clearInterval(toZoom);    
}
//Handles click events on a map, and adds a new point to the Polyline.
function addLatLng(event) {	
	document.getElementById("info").value="";
	  isGeoFence=document.getElementById("IsGeoFence").value;
	  latLngPoint.push(event.latLng);	  
	  vhRoutePolyLine.setPath(latLngPoint);
	  vhRoutePolyLine.setMap(map);	 
	  arrayVhRouteMarker[(latLngPoint.length)-1] = new google.maps.Marker({
		    position: event.latLng,
		    title: '#' + latLngPoint.length+" "+routeName,
		    map: map,
		    icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + latLngPoint.length + "|" + MARKER_COLOR + "|" + MARKER_TEXT_COLOR,
		    zIndex:latLngPoint.length-1
		  });
	  google.maps.event.addListener(arrayVhRouteMarker[(latLngPoint.length)-1], 'click', function() {
			document.getElementById('info').value=this.getTitle();			
			marker_clicked(this);});	  
	  google.maps.event.addListener(arrayVhRouteMarker[(latLngPoint.length)-1], 'rightclick', function() {
		  geocodeLatLng(this);});
	  arrayVhRouteMarkerDetails.details.push({'stop_id':'', 'name':'#' + latLngPoint.length+" "+routeName, 'lat':event.latLng.lat(),'lng':event.latLng.lng(),'is_busstop':'0','timelimit':'0'});
	  startMarke.setTitle("Start Point");
	  startMarke.setPosition(arrayVhRouteMarker[0].getPosition());
	  if((arrayVhRouteMarker.length)>0)
	  {			
		  document.getElementById("UndoBtn").disabled=false;  
		  endMarke.setTitle("End Point");  
		  endMarke.setPosition(arrayVhRouteMarker[(arrayVhRouteMarker.length)-1].getPosition());
		  endMarke.setMap(map);	
		  //alert("isGeoFence:"+isGeoFence);
		  if(isGeoFence == 1)
		  {		
			  if((arrayVhRouteMarker.length) > 2)
			  {			  	
				  	GeoFence.setPath(latLngPoint);
				  	GeoFence.setMap(map);
			  }
			  else
			  {
				  	GeoFence.setMap(null);
			  }			  	
		  }		  
	  }
	  startMarke.setMap(map);	 
}
function marker_clicked(marker)
{		
	var array_index=marker.getZIndex();
	//alert(array_index);
	document.getElementById("info").value="";
	document.getElementById('ArrayIndex').value=array_index;
	document.getElementById('StopName').value=marker.getTitle();
	document.getElementById('StopTimeLimit').value=arrayVhRouteMarkerDetails.details[array_index].timelimit;
	if(arrayVhRouteMarkerDetails.details[array_index].is_busstop == '1')
		document.getElementById('IsBusStop').checked=true;
	else
		document.getElementById('IsBusStop').checked=false;	
	document.getElementById('AddBtn').click();
}
function change_stop_name()
{
	var array_index=document.getElementById('ArrayIndex').value;
	var title=document.getElementById('StopName').value;
	arrayVhRouteMarker[array_index].setTitle(title);
	arrayVhRouteMarkerDetails.details[array_index].name=title;
	if(document.getElementById('IsBusStop').checked)
	{
		arrayVhRouteMarkerDetails.details[array_index].is_busstop='1';
		arrayVhRouteMarkerDetails.details[array_index].timelimit=document.getElementById('StopTimeLimit').value;
	}
	else
	{
		arrayVhRouteMarkerDetails.details[array_index].is_busstop='0';
		arrayVhRouteMarkerDetails.details[array_index].timelimit='1'
	}
	document.getElementById('info').value=title;	
	document.getElementById('ArrayIndex').value="";
	document.getElementById('StopName').value="";
	document.getElementById('StopTimeLimit').value="0";
	document.getElementById('IsBusStop').checked=false;
	document.getElementById('CloseModelBtn').click();	
}
function cursor_status(status)
{
    document.getElementById('googleMap').style.cursor = status;
}

function clearAllMarker()//to clear all markers from the map 
{	

	if(latLngPoint.length==0){
		alert("There are no markers to clear.");
	} else if(confirm("Do you want to delete all the Markers.")) {
		document.getElementById("info").value="";
		latLngPoint=[];
		startMarke.setMap(null);
		endMarke.setMap(null);
		for( j = 0; j < arrayVhRouteMarker.length; j++)
		{
			arrayVhRouteMarker[j].setMap(null);
		}
		arrayVhRouteMarker=[];
		vhRoutePolyLine.setMap(null);
		GeoFence.setMap(null);
		document.getElementById('ArrayIndex').value="";
		document.getElementById('StopName').value="";
		document.getElementById('StopTimeLimit').value="0";
		document.getElementById('IsBusStop').checked=false;
		arrayVhRouteMarkerDetails={'client_id':'','route_id':'','route_name':'','details':[],'undo':[]};	
		latlngbounds = new google.maps.LatLngBounds();
	}
}

function clearAllPoints()//to clear all markers from the map 
{	

		document.getElementById("info").value="";
		latLngPoint=[];
		startMarke.setMap(null);
		endMarke.setMap(null);
		for( j = 0; j < arrayVhRouteMarker.length; j++)
		{
			arrayVhRouteMarker[j].setMap(null);
		}
		arrayVhRouteMarker=[];
		vhRoutePolyLine.setMap(null);
		GeoFence.setMap(null);
		document.getElementById('ArrayIndex').value="";
		document.getElementById('StopName').value="";
		document.getElementById('StopTimeLimit').value="0";
		document.getElementById('IsBusStop').checked=false;
		arrayVhRouteMarkerDetails={'client_id':'','route_id':'','route_name':'','details':[],'undo':[]};	
		latlngbounds = new google.maps.LatLngBounds();
}

function saveFunction()
{	
	if((arrayVhRouteMarkerDetails.details.length)>=2 || isGeoFence == 0)
	{
		arrayVhRouteMarkerDetails.client_id=document.getElementById("ClientID").value;
		arrayVhRouteMarkerDetails.route_id=document.getElementById("RouteID").value;
		arrayVhRouteMarkerDetails.route_name=document.getElementById("Route").value;	
		isGeoFence=document.getElementById("IsGeoFence").value;
		var baseUrl=document.getElementById("URL").value;	
		var json_markerDetails=JSON.stringify(arrayVhRouteMarkerDetails);
		//alert(json_markerDetails);
		var xmlhttp = new XMLHttpRequest();		
		var url = baseUrl+"/save_marker/";
		if(confirm("Press OK to save the Route.")) {
			xmlhttp.onreadystatechange=function() 
			{
				  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
				  {
					  var result=JSON.parse(xmlhttp.responseText);
					  if(result.status=="Yes")
					  {		
						  document.getElementById("info").value=result.msg;		  
						  alert(result.msg);					  
						  //alert(arrayVhRouteMarkerDetails.details.length);
						  //for(j=0; j < arrayVhRouteMarkerDetails.details.length; j++)
							//  arrayVhRouteMarkerDetails.details[j].stop_id=-1;
							for(j=0;j<result.stop_id.length;j++)
							{
								arrayVhRouteMarkerDetails.details[j].stop_id=result.stop_id[j].stopid;
								arrayVhRouteMarkerDetails.details[j].is_busstop=result.stop_id[j].is_busstop;
								arrayVhRouteMarkerDetails.details[array_index].timelimit=result.stop_id[j].timelimit;
							}
							arrayVhRouteMarkerDetails.undo=[];
						  document.getElementById("ClearAll").disabled =true;			  
					  }	
					  else
						  {
						  	document.getElementById("info").value=result.msg;
						  	alert(result.msg);
						  }
				  }
			}
			xmlhttp.open("POST", url, true);	
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("marker_details="+json_markerDetails);
		}
		
		
	}else
	{
		document.getElementById("info").value="Minimun two points required to define route.";
		alert("Minimun two points required to define route.");		
	}
	
}
function deleteFunction()
{

	if(latLngPoint.length==0){
		alert("There are no markers to Delete.");
	} else if(confirm("Do you want to delete all the markers?"))
	 {
		var route_id=(document.getElementById("RouteID").value!="")?document.getElementById("RouteID").value:0;		
		var baseUrl=document.getElementById("URL").value;	
		var xmlhttp = new XMLHttpRequest();		
		var url = baseUrl+"/delete_allMarkers/"+route_id;		
		xmlhttp.onreadystatechange=function() 
		{
			  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			  {				 
				  var result=JSON.parse(xmlhttp.responseText);
				  if(result.status=="Yes")
				  {
					  document.getElementById("info").value=result.msg;
					  alert(result.msg);
					  document.getElementById("ClearAll").disabled =false;
					  clearAllPoints();
				  }
				  else
				  {
					  document.getElementById("info").value=result.msg;
					  alert(result.msg);
				  }		       
			  }
		}
		xmlhttp.open("GET", url, true);		
		xmlhttp.send();
	 }
}
function undo_fun(){
	if(latLngPoint.length > 0)
	{
		startMarke.setPosition(arrayVhRouteMarker[0].getPosition());
		document.getElementById("info").value="";
		//alert("Route ID:"+arrayVhRouteMarkerDetails.details[arrayVhRouteMarker.length-1].stop_id);
		if(arrayVhRouteMarkerDetails.details[arrayVhRouteMarker.length-1].stop_id !="")
			arrayVhRouteMarkerDetails.undo.push(arrayVhRouteMarkerDetails.details[arrayVhRouteMarker.length-1].stop_id);
		//alert("Undo1");
		latLngPoint.pop();
		//alert("Undo2");
		vhRoutePolyLine.setPath(latLngPoint);
		//alert("Undo3");
		vhRoutePolyLine.setMap(map);
		//alert("Undo4");
		arrayVhRouteMarker[arrayVhRouteMarker.length-1].setMap(null);
		arrayVhRouteMarker.pop();
		//alert("Now length:"+arrayVhRouteMarker.length);
		//alert("Undo5");
		arrayVhRouteMarkerDetails.details.pop();	
		//alert("Undo6");
	
	
		if(latLngPoint.length > 1)
			endMarke.setPosition(arrayVhRouteMarker[arrayVhRouteMarker.length-1].getPosition());
		else
			endMarke.setMap(null);			
		document.getElementById("UndoBtn").disabled=false;
		if(isGeoFence == 1)
		{		
			  if((arrayVhRouteMarker.length) > 2)
			  {			  	
				  	GeoFence.setPath(latLngPoint);
				  	GeoFence.setMap(map);
			  }
			  else
			  {
				  	GeoFence.setMap(null);
			  }			  	
		}
		if(latLngPoint.length == 0)
		{				
			startMarke.setMap(null);	
			document.getElementById("UndoBtn").disabled=true;
		}	
	}
	else
	{
		document.getElementById("info").value="Nothing to Undo.";
	}
}
</script>
<style type="text/css">
.nav {
	line-height: 14px;/*30px;*/
	background-color: #eeeeee;
	float: left;
	padding: 3px;
}
.divx {
	/*position: absolute;
	width: 100%;*/
	height: 637px;/*574px;508px;*/
	/*float: left;
	margin-right: 5px;*/
}
@CHARSET "ISO-8859-1";
</style>
</head>

<body id="bodyTag" onload="initMap()"><!--initMap  function_onload_page-->
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
      <?php
      $home_link="";
      if($this->session->userdata('login')!=null)
      {
      	$sess_details=$this->session->userdata('login');
      	$user_type=$sess_details['sess_user_type'];
      	$is_admin=$sess_details['sess_is_admin'];
      if($user_type == AUTOGRADE_USER)
      	$home_link="home_ctrl";
      else if($user_type == OHTER_CLIENT_USER)
      {
      	if($is_admin == '1')
      		$home_link="home_ctrl";
      	else
      		$home_link="vehicle_tracking_ctrl";
      }
      else if($user_type == INDIVIDUAL_CLIENT)
      	$home_link="individual_vt_ctrl";
      else if($user_type==DISTRIBUTOR_USER)
      	$home_link="distributor_dashboard_ctrl";
      else if($user_type==DEALER_USER)
      	$home_link="dealer_dashboard_ctrl";
      else if($user_type==PARENT_USER)
      	$home_link="individual_vt_ctrl";
      }else{$home_link="login_ctrl";}
      ?>
        <h3><a href="<?php echo base_url('index.php/'.$home_link)?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
         <h5><?php if ($this->session->userdata ( 'login' )){ echo $ID['sess_client_name']; }?></h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
      <?php if ($this->session->userdata ( 'login' )){ ?>
       <h6><?php echo $ID['sess_user_name']; ?></h6>
       <h6><a href= "<?php echo base_url('index.php/change_password_ctrl')?>">Change Password</a> | <a href= "<?php echo base_url('index.php/home_ctrl/logout')?>">Logout</a></h6>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>    	    
 <!-- --------------------------------------------------------------- -->
      	    
<?php if ($this->session->userdata ( 'login' )):?>
<?php echo $GLOBALS ['main_menu']; ?>
<?php endif; ?>

          </div>
        </div>
        <div>
          <h5>Autograde T.A.N.K. Service</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank">by Autograde</a> </em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right">
      <em> <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?>
        <?php // echo date('d - F Y')?></em></div>
    </div>
  </div>
</div>
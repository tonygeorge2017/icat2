<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">

<link href="<?php echo base_url('assets/css/dashboard.css')?>" rel="stylesheet">

<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">

	 	
	 	<!-- below 3 links are required for menu and Progress bar and datepicker -->
	 	<link href="<?php echo base_url('assets/css/jquery-ui.css')?>" rel="stylesheet"></link>
	 	<script src="<?php echo base_url('assets/js/jquery-1.10.2.js')?>"></script>
	 	<script src="<?php echo base_url('assets/js/jquery-ui.js')?>"></script>
	 	
	 	<script src="<?php //echo base_url('assets/js/jquery.js')?>"></script> <!-- This is required for menu and Progress bar -->

      
</head>
<body>
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
     <?php
      $home_link="";
      if($this->session->userdata('login')!=null)
      {
      	$sess_details=$this->session->userdata('login');
      	$user_type=$sess_details['sess_user_type'];
      	$is_admin=$sess_details['sess_is_admin'];
      if($user_type == AUTOGRADE_USER)
      	$home_link="home_ctrl";
      else if($user_type == OHTER_CLIENT_USER)
      {
      	if($is_admin=='1')
      		$home_link="home_ctrl";
      	else
      		$home_link="vehicle_tracking_ctrl";
      }
      else if($user_type == INDIVIDUAL_CLIENT)
      	$home_link="individual_vt_ctrl";
      else if($user_type==DISTRIBUTOR_USER)
      	$home_link="distributor_dashboard_ctrl";
      else if($user_type==DEALER_USER)
      	$home_link="dealer_dashboard_ctrl";
      else if($user_type==PARENT_USER)
      	$home_link="individual_vt_ctrl";
      }else{$home_link="login_ctrl";}
      ?>
      <h3><a href="<?php echo base_url('index.php/'.$home_link)?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
        <h5> <?php if ($this->session->userdata ( 'login' ))
        {
        	if($ID['sess_client_name'] != "") 
        	{
        		echo $ID['sess_client_name'];
        	}
        	if($ID['sess_user_type'] == DEALER_USER)
        	{
        		echo $ID['sess_dealer_name'];
        	}
        	if($ID['sess_user_type'] == DISTRIBUTOR_USER)
        	{
        		echo $ID['sess_distributor_name'];
        	}
        }?></h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
      <?php if ($this->session->userdata ( 'login' )){ ?>
       <h6><?php echo $ID['sess_user_name']; ?></h6>
        <h6><a href= "<?php echo base_url('index.php/change_password_ctrl')?>">Change Password</a> | <a href= "<?php echo base_url('index.php/home_ctrl/logout')?>">Logout</a></h6>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>
           <!-- --------------------------------------------------------------- -->
      	    
<?php if ($this->session->userdata ( 'login' )):?>
<?php echo $GLOBALS ['main_menu']; ?>
<?php endif; ?>
			
          <!-- -------------------------------------------------------- --> 
          </div>
        </div>
        <div>
          <h5>Autograde T.A.N.K. Service</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank"> by Autograde </a></em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right"><em>
       <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?></em></div>
    </div>
  </div>
</div>

<!-- Login -->

<div class="container">
  <div class="row">
    <div class="account-container stacked"><br>
      <div class="content clearfix">
        <form method="post" class="form-horizontal">
           <h1>About Autograde</h1>
           <p> Autograde came into existence in the year 2007 under the name of Aasma Techno Products Pvt. Ltd, 
           with its head office in Cochin, India. Aasma's Road Speed Limiter was marketed under the brand Autograde. 
           Autograde Industries LLC was established in UAE to manufacture and market Autograde Brand products in Middle East.</p>
           
           <p>Presently, the organization has manufacturing facilities in Cochin, India and Ras-Al-Khaimah, UAE,
            Administrative offices in Bangalore, Cochin, Dubai and Kenya, and also sales network across 25 countries.
             Autograde Technical Center, the R&amp;D division, was established in March, 2014 
             and is based out of International Technology Park, Bangalore.</p>
           
          <!-- <div class="login-fields">
            <div class="field control-group">
              <label for="username">Username:</label>
              <input type="text" id="username" name="username" value="" placeholder="Username" class="form-control input-lg username-field" required />
            </div>-->
            <!-- /field -->
           <!--  <div class="field">
              <label for="password">Password:</label>
              <input type="password" id="password" name="password" value="" placeholder="Password" class="form-control input-lg password-field" required />
            </div> -->
            
            <!-- /password --> 
          </div>
          <!-- /login-fields -->
         <!--  <div class="login-actions"> <span class="login-checkbox">
            <input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4">
            <label class="choice" for="Field">Keep me signed in</label>
            </span> </div>
          <div class="field">
            <button class="btn btn-primary">Ok</button>
            <button class="btn btn-primary">Cancel</button>
          </div>
          <div class="field"> <a href="#">Help </a> | <a href="#"> Forgot Password</a></div>
          <div class="clearfix"></div> -->
          <!-- .actions -->
        </form>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
<!-- Footer -->

<link href="src/css/bootstrapValidator.css" rel="stylesheet">
<script src="src/js/bootstrapValidator.js"></script> 
<!-- jQuery --> 
<script src="js/jquery.js"></script> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.min.js"></script> 
<!-- Script to Activate the Carousel --> 
</body>
</html>

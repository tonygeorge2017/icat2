<script type="text/javascript">

$(function() {
	$( "#ExpiryDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
	// $( "#ExpiryDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy/m/d' });
	$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$('[data-toggle="tooltip"]').tooltip();

  });
  
function myFunction()
{
	document.getElementById("temp").value=document.getElementById("ClientName").value;//ClientName
	document.getElementById("myform").submit();
}

function company_change()
{
	var baseUrl=document.getElementById('URL').value;
	var companyId=document.getElementById('CompanyID').value;
// 	alert("jjj--0"+companyId);
	if(companyId!='')
	{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_cmpy_model_name/"+companyId;
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				model_name_for_company(xmlhttp.responseText);
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	else
	{
		document.getElementById("ModelID").innerHTML=null;
	}
}

function model_name_for_company(response) {
	var arrs = JSON.parse(response);
	var htm = '<option value="">--Select Model Name--</option>';
	$.each(arrs, function( index, model) {

		htm+='<option value="'+model.fsv_id+'">'+model.fsv_model+'</option>';
		
	});

	$("#ModelID").html(htm);
}

</script>
<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
#VehicleIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
.cur{
		cursor: pointer;
		color:#5CD9D0;
}
</style>

<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/vehicle_ctrl/"))?>"/>
				<form id="myform" action="<?php echo(base_url("index.php/vehicle_ctrl/vts_vehicle_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="VehicleId" style="display: none;" name="VehicleId" value="<?php echo((isset($vcle_id))? $vcle_id:null) ?>" />
					<input type="text" id="temp" style="display: none;"  name="temp" value="-1" />
					<h1>Vehicle Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>
					<div class="user-fields">
					
					<?php if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
							<select name="ClientName" id="ClientName" onchange="myFunction()">
							<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
							<?php foreach ($clientList as $row):?>
							<option value="<?php echo $row['client_id']?>" <?php echo(($vcle_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php endforeach;?>
							</select>
						</div>
					<?php endif; ?>
						<div class="field">
							<label>Regn. Number:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="15" id="RegisterNumber" name="RegisterNumber" value="<?php if(!isset($outcome))echo $vcle_reg_number?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('RegisterNumber'))echo form_error('RegisterNumber',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Vehicle Group:<span style="color:red;"> *</span></label>
							<select name="VehicleGroup">
							<option value=""><?php if(null!=form_error('VehicleGroup'))echo form_error('VehicleGroup',' ',' '); ?></option>
							<?php foreach ($vehicleGroupList as $row):?>
							<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vcle_vehiclegroup_id==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
							<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label for="companyName">Company Name:<span style="color:red;"> *</span></label>
							<select id="CompanyID"  name="CompanyID" onchange="company_change()">  
							<option value=""><?php if(null!=form_error('CompanyID'))echo form_error('CompanyID',' ',' '); ?></option>
							<?php if($companyList!=null):  foreach ($companyList as $row):?>
							<option value="<?php echo $row['fsv_id']?>" <?php echo(($companyID==$row['fsv_id'])?'selected':'')?>><?php echo $row['fsv_model']?> </option>
							<?php endforeach; endif;?>
							</select>
						</div>
						<div class="field">
							<label for="modelName">Model Name:<span style="color:red;"> *</span></label>
							<select id="ModelID"  name="ModelID">
							<option value=""><?php if(null!=form_error('ModelID'))echo form_error('ModelID',' ',' '); ?></option>
							<?php if( $modelList != null ): foreach( $modelList AS $row ):  ?>
							<!--  $all_models = json_decode( $modelList ); --> 
								<option value="<?php echo $row['fsv_id']; ?>" <?php echo ( ( $modelID==$row['fsv_id'] ) ? 'selected' : '' ); ?>><?php echo $row['fsv_model']; ?> </option>
							<?php endforeach; endif;?>
							</select>
						</div>
						<div class="field">
							<label>Fuel Type:<span style="color:red;"> *</span></label>
							<select name="FuelType">
<!-- 							<option value=""></option> -->
							<?php foreach ($FuelTypeList as $row):?>
								<option value="<?php echo $row['config_id']?>" <?php echo(($fuel_type==$row['config_id'])?'selected':'')?>><?php echo $row['config_item']?> </option>
							<?php endforeach;?>
							</select>
						</div>						
						<div class="field">
							<label>Mileage (Km/Ltr) : <span data-toggle="tooltip" data-placement="right" title="Input mileage information of the this vehicle (i.e. No. of Km per Liter)." class="glyphicon glyphicon-question-sign cur" aria-hidden="true"></span></label>
							<input type="text"  id="Mileage"  name="Mileage" value="<?php echo $Mileage ?>" class="form-control input-lg" />
						</div>	
						<div class="field">
							<label>Fuel Consumption : <span data-toggle="tooltip" data-placement="right" title="Input fuel consumption per Hr of this vehcile while Ignition status is ON.(i.e. Vehcile at ideal state)" class="glyphicon glyphicon-question-sign cur" aria-hidden="true"></span></label>
							<input type="text"  id="FuelConsumption"  name="FuelConsumption" value="<?php echo $FuelConsumption ?>" class="form-control input-lg"  />
						</div>
						<div class="field">
							<label>Ideal Speed(Km/Hr): <span data-toggle="tooltip" data-placement="right" title="Speed consider as vehicle in ideal state." class="glyphicon glyphicon-question-sign cur" aria-hidden="true"></span></label>
							<input type="text"  id="IdealSpeed"  name="IdealSpeed" value="<?php echo (isset($IdealSpeed))?$IdealSpeed:'0' ?>" class="form-control input-lg"  />
						</div>
						<div class="field">
							<label>Input Voltage Type:</label>
							<select name="VoltageType">
							<option value=""></option>
							<?php foreach ($VoltageTypeList as $row):?>
								<option value="<?php echo $row['config_id']?>" <?php echo(($voltage_type==$row['config_id'])?'selected':'')?>><?php echo $row['config_item']?> </option>
							<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>Chassis Number: </label>
							<input type="text" maxlength="20" id="ChassisNumber" name="ChassisNumber" value="<?php if(!isset($outcome))echo $vcle_chassis_number?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ChassisNumber'))echo form_error('ChassisNumber',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Engine Number: </label>
							<input type="text" maxlength="20" id="EngineNumber" name="EngineNumber" value="<?php if(!isset($outcome))echo $vcle_engine_number?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('EngineNumber'))echo form_error('EngineNumber',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Color: </label>
							<input type="text" maxlength="20" id="Color" name="Color" value="<?php if(!isset($outcome))echo $vcle_color?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('Color'))echo form_error('Color',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Mfr. Year: </label>
							<input type="text" maxlength="4" id="MfrYear" name="MfrYear" value="<?php if(!isset($outcome))echo $vcle_mfr_year?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('MfrYear'))echo form_error('MfrYear',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Reg. Year: </label>
							<input type="text" maxlength="4" id="RegnYear" name="RegnYear" value="<?php if(!isset($outcome))echo $vcle_reg_year?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('RegnYear'))echo form_error('RegnYear',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Speed Limit(kmph):<span style="color:red;"> *</span></label>
							<input type="text" maxlength="4" id="SpeedLimit" name="SpeedLimit" value="<?php if(!isset($outcome))echo $vcle_speed_limit?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('SpeedLimit'))echo form_error('SpeedLimit',' ',' ');?>" />
						</div><br/>
						
						<h4 style="color: #d95045;">Insurance Details</h4>
						<h1></h1>
						
						<div class="field">
							<label>Insurance Agency:</label>
							<select name="VehicleInsuranceAgencyId" id="VehicleInsuranceAgencyId">
							<option value=""><?php if(null!=form_error('VehicleInsuranceAgencyId'))echo form_error('VehicleInsuranceAgencyId',' ',' '); ?></option>
							<?php foreach ($insuranceAgencyList as $row):?>
								<option value="<?php echo $row['insurance_agency_id']?>" <?php echo(($vcle_insurance_agency_id==$row['insurance_agency_id'])?'selected':'')?>><?php echo $row['insurance_agency']?> </option><!-- $row['dealer_id']."-".$row['dealer_name'] -->
							<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>Policy Number: </label>
							<input type="text" id="PolicyNumber" maxlength="20" name="PolicyNumber" value="<?php if(!isset($outcome))echo $vcle_policy_number?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('PolicyNumber'))echo form_error('PolicyNumber',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Insurance Type:</label>
							<select name="VehicleInsurancePolicyType">
							<option value=""><?php if(null!=form_error('VehicleInsurancePolicyType'))echo form_error('VehicleInsurancePolicyType',' ',' '); ?></option>
							<?php foreach ($vehicleInsurancePolicyTypeList as $row):?>
								<option value="<?php echo $row['config_id']?>" <?php echo(($vcle_insurance_policy_type==$row['config_id'])?'selected':'')?>><?php echo $row['config_item']?> </option>
							<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>Expiry Date: </label>
							<input type="text" id="ExpiryDate" name="ExpiryDate" value="<?php if(!isset($outcome))echo $vcle_insurance_expiry_date?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ExpiryDate'))echo form_error('ExpiryDate',' ',' '); ?>" />
						</div>
						<div class="field">
							<label>Terms:</label>
							<textarea type="text" id="Terms" maxlength="500" name="Terms" class="form-control input-lg" placeholder="<?php if(null!=form_error('Terms'))echo form_error('Terms',' ',' ');?>"><?php if(!isset($outcome))echo $vcle_insurance_terms?></textarea>
						</div>
						<div class="field">
							<label>Remarks:</label>
							<textarea type="text" id="Remark" maxlength="500" name="Remark" class="form-control input-lg" placeholder="<?php if(null!=form_error('Remark'))echo form_error('Remark',' ',' ');?>"><?php if(!isset($outcome))echo $vcle_remark?></textarea>
						</div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="VehicleIsActive" name="VehicleIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>> 
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br />
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($vcle_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/vehicle_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/vehicle_ctrl/editOrDelete_vts_vehicle/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
							<tr>
								<th>Registration Number</th>
								<th>Vehicle Group</th>
								<th>Actions</th>
							</tr>
						<?php if($vehicleList!=null): foreach ($vehicleList as $row): ?> 
							<tr>
								<td><?php echo trim($row['vehicle_regnumber'])?></td>
								<td><?php echo trim($row['vehicle_group_name'])?></td>
								<td>
								<?php $md_id = md5(trim($row['vehicle_client_id'])); $model_id = trim($row['vehicle_model_id']); ?>
								<a href="<?php echo(base_url("index.php/vehicle_ctrl/editOrDelete_vts_vehicle/".$md_id."/".md5('edit')."/".md5(trim($row['vehicle_id']))."/".$model_id))?>" title="Edit the Vehicle Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
								<a href="<?php echo(base_url("index.php/vehicle_ctrl/editOrDelete_vts_vehicle/".$md_id."/".md5('delete')."/".md5(trim($row['vehicle_id']))."/".$model_id))?>" title="Delete the Vehicle Details" onclick="return confirm('Do you want to delete the selected Vehicle?')"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php endforeach; endif;?>
						</table>
				 	</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
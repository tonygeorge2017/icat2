<script type="text/javascript">
$(function(){
	var pre_val=1;	
	var userType=$('#userType').val();
	var clid=$('#clientID').val();
	var maxNoShift=4;
	var urls=$('#URL').val();
	$('#checkBx').hide();
	$.ajax({
       url: urls+'/get_client',
       dataType: 'json',
       success: function(data) {			
           $.each( data, function( key, value ) {
				if(userType=='1')
					$('#client').append($('<option></option>').val(value['client_id']).html(value['client_name'] ));
				else if(clid==value['client_id']){
					$('#client').html('');
					$('#client').append($('<option></option>').val(value['client_id']).html(value['client_name'] ));
				}			   
        	 });			 
			 if(clid)
				$('#client').val(clid);
       },
       error: function(e) {
		   $('#client').append($('<option></option>').html("--- No Client Found ---"));
           console.log(e.responseText);
       }
    });
	$('#client').on('change',function(){
		$('#clientChangeID').val($(this).val());
		$( "#shiftCategoryForm" ).submit();
	});
	$('#noOfShift').on('change',function(){	
		pre_val=parseInt(pre_val);
		var crnt=parseInt($(this).val());
		if(pre_val<crnt){
			for(var i=pre_val;i<crnt;i++)
				add_row(i+1);
			pre_val=crnt;			
		}
		else if(pre_val>crnt){			
			for(var j=pre_val;j>crnt;j--)
				del_row();
			pre_val=crnt;
		}
	});
	
	$("#wkHr").on('change',function(){
        reset_time();
    });
    $("#wkMnt").on('change',function(){
        reset_time();
    });
    function reset_time()
    {
        var no_shift=$('#noOfShift').val();
        for(var lp=1;lp<=no_shift;lp++)
        {
            $('#SH_'+lp).val('00');
            $('#MSH_'+lp).val('00');
            $('#EMSH_'+lp).val('');
        }
    } 
	
	function add_row(ids)
	{
		$('#shiftList').append('<tr><td style="width: 40%;"><input name="shift[]"  style="width: 90%;" class="form-control input-lg" type="text"></td><td style="width: 40%;margin-left:5px;"><select id="SH_'+ids+'" style="display: inline;width: 65px;" name="SHrs[]" onchange="hr_chage(this)"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select> : <select id="MSH_'+ids+'" style="display: inline; width: 65px;" name="SMint[]" onchange="mnt_chage(this)"><option value="00">00</option><option value="15">15</option><option value="30">30</option><option value="45">45</option></select></td><td style="width: 20%; margin-left:5px;"><input id="EMSH_'+ids+'" name="end[]" style="display: inline;width: 100%;" class="form-control input-lg" type="text" readonly></td></tr>');
	}
	function del_row()
	{		
		$('#shiftList tr:last').remove();
	}	
});
function hr_chage(cnt){	
	var shiftHrs=parseInt($("#wkHr").val());
	var shiftMnt=parseInt($("#wkMnt").val());	
	var id=$(cnt).attr("id");		
	var hr=parseInt($(cnt).val());
	var mt=parseInt($('#M'+id).val());
	if((hr+shiftHrs)>24)
	{
		alert("Shift time should not exceed to next day");
	}
	else{
		$('#EM'+id).val(hr+shiftHrs+':'+(mt+shiftMnt));
	}
}
function mnt_chage(cnt){	
	var shiftHrs=parseInt($("#wkHr").val());
	var shiftMnt=parseInt($("#wkMnt").val());
	var id=$(cnt).attr("id");
	var hrid=trim_char(id,'M');		
	var hr=parseInt($('#'+hrid).val());		
	var mt=parseInt($(cnt).val());		
	if((hr+shiftHrs)>24)
	{
		alert("Shift time should not exceed to next day");
	}
	else{
		$('#E'+id).val(hr+shiftHrs+':'+(mt+shiftMnt));
	}
}
function trim_char(str,chr)
{
	return str.replace(chr,'');
}
</script>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<div class="content clearfix">
				<?php $reloadURL=base_url("index.php/shift_setting_ctrl/")?>
				<input type="hidden" id="userType" value="<?php echo $sess_user_type; ?>"/>
				<input type="hidden" id="URL" value="<?php echo $reloadURL ?>" />
				<form id="shiftCategoryForm" action="<?php echo(base_url("index.php/shift_setting_ctrl/client_change/"))?>" method="post" class="form-horizontal">
				<input type="hidden" id="clientChangeID" name="clientChangeID" />
				</form>
				<form  action="<?php echo(base_url("index.php/shift_setting_ctrl/input_validation/"))?>" method="post" class="form-horizontal">
					<input type="hidden" id="categoryID" name="categoryID" value="<?php echo $categoryID ?>" />
					<input type="hidden" id="clientID" name="clientID" value="<?php echo $clientID ?>" />
					<h1>Shift Setting</h1>
					<div class="user-fields">
						<div class="field">
							<label for="client">Client</label>							
							<select name="client" id="client" required ><!--onchange="window.location='<?php //echo $reloadURL.'/client_change/'?>' + this.value;"-->
								<option>--- Select Client --- </option>
							</select>
						</div>
						<div class="field">
							<label>Shift Category:<span style="color:red;"> *</span></label>
							<input type="text"  id="categoryName" name="categoryName" value="<?php if(!isset($outcome))echo $categoryName?>" class="form-control input-lg" placeholder=" " required/>
						</div>
						<div class="field">
							<label>No. Of Shifts Per Day:<span style="color:red;"> *</span></label>
							<select id="noOfShift"  style="display: inline;width: 75px;" name="noOfShift" required>				
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
								<option value="4">04</option>								
							</select>							
						</div>
						<div class="field">
							<label>Working Hrs Per Shift(HH:MM):<span style="color:red;"> *</span></label>
							<select id="wkHr"  style="display: inline;width: 75px;" name="wkHr" required>										
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
									</select>
									:
									<select id="wkMnt" style="display: inline; width: 75px;" name="wkMnt" required>										
										<option value="00">00</option>
										<option value="15">15</option>
										<option value="30">30</option>
										<option value="45">45</option>
									</select>
						</div>
						<div class="field">
							<table id="shiftList">
								<tr>
									<th><span style="display: inline;width: 100%;">Shift Name</span></th>
									<th><span style="display: inline;width: 100%;">Start Time (HH:MM)</span></th>
									<th><span style="display: inline;width: 100%;">End Time</span></th>
								</tr>
								<tr id="row1">
									<?php //style="display: inline;width: 100%;" ?>
									<td style="width: 40%;"><input name="shift[]"  style="width: 90%;" class="form-control input-lg" type="text"></td>
									<td style="width: 40%;margin-left:5px;">
									<select id="SH_1"  style="display: inline;width: 65px;" name="SHrs[]" onchange="hr_chage(this)">
										<option value="00">00</option>
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
									</select>
									:
									<select id="MSH_1" style="display: inline; width: 65px;" name="SMint[]" onchange="mnt_chage(this)">
										<option value="00">00</option>
										<option value="15">15</option>
										<option value="30">30</option>
										<option value="45">45</option>
									</select>
									</td>
									<td style="width: 20%; margin-left:5px;">
										<input id="EMSH_1" name="end[]" style="display: inline;width: 100%;" class="form-control input-lg" type="text" readonly>
									</td>
								</tr>
							</table>
						</div>
						<div class="field">
							<label>Remarks:</label>
							<textarea style="font-size: 13px;" id="description" name="description" cols="" rows="" class="form-control input-lg" maxlength="500"><?php echo $description ?></textarea>
						</div>
						<div id="checkBx" class="login-actions">
							<span class="login-checkbox"> <input id="isactive" name="isactive" type="checkbox" class="field login-checkbox" value="1" <?php //echo(($isactive==1)? 'checked':'')?> checked>
							<label class="choice" for="Field">Active</label>
							</span>
						</div>
						<div class="field">
							<button class="btn btn-primary" type="submit"><?php echo(($categoryID==null)?"Save":"Update")?> </button>							
							<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
						</div>
					</div>
					<div class="table-responsive">
						<table width="100%" class="user-dts" border="1">
							  <tr>
								 <th>Shift Category</th>
								 <th>Shift (Start &amp; End Time)</th>
								 <th>Action</th>
							  </tr>
							  <?php $pre_shift_category_id=0;?>
							  <?php foreach ($categoryList as $row): ?> 
							  <tr>
								 <?php if($pre_shift_category_id != $row['shift_category_id']):?>
									 <td rowspan=<?php echo $row['no_of_shift'] ?> style="text-align: left; padding: 3px;">
										<?php echo trim($row['shift_category_name']).'<br>';?>
										<?php echo '('.($row['working_hrs']).'Hrs per shift)'?>
									 </td>								 
								 <?php endif; ?>
								 <td style="text-align: left; padding: 3px ;"><?php echo trim($row['shift_name']).' ( '.$row['shift_start_time'].' - '.$row['shift_end_time'].')'; ?></td>
								 <?php if($pre_shift_category_id != $row['shift_category_id']):?>
									 <td rowspan=<?php echo $row['no_of_shift'] ?>>									
										<a href="<?php echo(base_url("index.php/shift_setting_ctrl/delete/".md5($row['client_id'])."/".md5($row['shift_category_id'])))?>" title="Delete the Device Type" onclick="return confirm('Do you want to delete the selected shift category?\nWarning: It will remove all details which is map with this shift category.')"><i class="fa fa-trash-o"></i></a>
									 </td>
								 <?php endif; ?>
								 <?php $pre_shift_category_id=$row['shift_category_id'];?>
							  </tr>
							  <?php endforeach; ?>
						</table>
					 </div>
				</form>				
			</div>
		</div>
	</div>
</div>	
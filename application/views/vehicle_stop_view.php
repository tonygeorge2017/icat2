<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="CloseModelBtn" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Stop Detail</h4>
        <?php if(isset($modeloutcome)) echo $modeloutcome;?>
      </div>      
      <div class="modal-body">      
        <input style="display : none;" type="text"  id="SaveURL" value="<?php echo(base_url("index.php/vehicle_stop_ctrl/"))?>" />
        <input style="display : none;" type="text" id="StopID" name="StopID"  value=""/>
        <input style="display : none;" type="text" id="ModelClientID" name="ClientID"  value=""/>
        <input style="display : none;" type="text" id="ModelVehicleGroup" name="VehicleGroup"  value=""/>
        <input style="display : none;" type="text" id="ModelVehicle" name="Vehicle"  value=""/>
        <input style="display : none;" type="text" id="Latitude" name="Latitude"  value=""/>
        <input style="display : none;" type="text" id="Longitude" name="Longitude"  value=""/>        
          <div class="user-fields">          
            <div class="field">
              <label for="Stop">Stop Name:<span style="color:red;"> *</span></label>
              <input type="text" id="StopName" name="StopName" value="" class="form-control input-lg" placeholder="Stop Name" maxlength="100"/>
            </div> 
            <div class="field">
        		<label>Remarks: </label>
        		<textarea style="font-size: 15px;" id="Remarks" name="Remarks" cols="" rows="6" class="form-control input-lg"  maxlength="500"></textarea>
      		</div>       
          </div>
          <div class="clearfix"></div>       
      <div class="modal-footer">
        <input type="button" class="btn btn-primary" name="submit" id="submit" value="Save" onclick="save_stop_details()" />       
      </div>       
      </div>
    </div>
  </div>
</div>

<div id="wrapper"> 
  <!-- style="display : none;" -->  
  <input type="Text" style="display : none;" id="ImageURL" name="ImageURL" value="<?php echo base_url("assets/images/")?>"/>
  <input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/vehicle_stop_ctrl/")?>"/>  
  <input type="Text" style="display : none;" id="SessClient" name="SessClient" value="<?php echo $sessClientID ?>"/>
  <input type="Text" style="display : none;" id="SessUser" name="SessUser" value="<?php echo $sessUserID ?>"/>
  
  
  <div id="sidebar-wrapper">
    <div class="nav">      
      <input id="EditOrError"  name="EditOrError" style="display: none;" type="checkbox" class="field login-checkbox" value="1" <?php echo(($editOrError == ACTIVE)? 'checked':'')?> />
       <?php if(isset($outcome)) echo $outcome;?>
      <div class="field">
        <label <?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?>>Client:<span style="color:red;"> *</span></label>
          <select style="font-size: 15px;<?php if($sessClientID!=AUTOGRADE_USER) echo 'display: none;'?>" id="ClientID"  name="ClientID" class="form-control input-lg" onchange="get_client()" >                     
           <?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER){echo'<option value=""></option>';} foreach ($clientList as $row):?>
           <?php if($sessClientID==AUTOGRADE_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
           <option value="<?php echo $row['client_id']?>" <?php echo(($clientID==$row['client_id'])?'selected':'')?>><?php echo $row['client_name']?> </option>
            <?php elseif($sessClientID==$row['client_id']): ?>
           <option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
           <?php endif;?>
          <?php endforeach; endif;?>
         </select>
       </div>
      <div class="field">  
	  <label>vehicle Group:<span style="color:red;"> *</span></label>
      <select style="font-size: 15px;" id="VehicleGroup" name="VehicleGroup" class="form-control input-lg" onchange="get_vhl_gp_vehicle()">
      <?php
      	if(!empty($vehicleGroupList)) 
      	{
      		echo '<option value=""></option>';
			foreach ($vehicleGroupList as $row)
			{
				$select=($vehicleGroupID==$row['vh_gp_id'])?'selected':'';
				echo '<option value="'.$row['vh_gp_id'].'" '.$select.'>'.$row['vh_gp_name'].'</option>';
			}
      	}
	 ?>
	  </select>
      </div>
      <div class="field">
        <label>Vehicle:</label>
        <select style="font-size: 15px;" id="Vehicle" name="Vehicle" class="form-control input-lg" onchange="refresh_arrayMarker()">
          <option value=""></option>
          <?php 
			if($vehicleList!=null)
			{			 	
	         	foreach ($vehicleList as $row)
	         	{
	         		$select=($vehicleID==$row['vh_id'])?'selected':'';
		     		echo '<option value="'.$row['vh_id'].'" '.$select.'>'.$row['vh_name'].'</option>';
			 	}
			}
		 ?>
        </select>
      </div>   
       <div class="field">
              <label for="employee">Search Location:</label>
              <input type="text" id="SearchLocation" name="SearchLocation" value=" " class="form-control input-lg"  placeholder="Search Location"/>
              <button style="display: none;" id="AddBtn" type="button" title="Add device" class="btn btn-primary" data-toggle="modal" data-target="#myModal">ADD Device</button>
      </div>
      <div class="field">           
            <?php $reloadURL=base_url("index.php/vehicle_stop_ctrl/")?>
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div> 
      <div class="field">
      	<label>Info.: </label>
       	<textarea style="font-size: 15px;" id="Info" name="Info" cols="" rows="6" class="form-control input-lg"  maxlength="500"></textarea>        		
      </div>    
    </div>
  </div>
  <div id="page-content-wrapper">
    <div class="divx" id="GoogleMap"></div>
  </div>
</div>
</body>
</html>
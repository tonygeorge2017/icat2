<div class="container">
  <div class="row">
    <div class="account-container stacked"><br>
      <div class="content clearfix">      
          <h1 style="color:<?php echo (($isErrorMsg==ACTIVE)?'red':'green')?>"><?php echo $title?></h1>
          <div class="login-fields">
            <!-- /field -->
            <div class="field" style="color:<?php echo (($isErrorMsg==ACTIVE)?'red':'green')?>">
              <P><?php echo $outcome?></P>              
            </div>
          </div>
          <?php if(isset($linkText)):if($linkText!=null):?>
         	 <div class="field"> <a href="<?php echo(base_url("index.php/".$ctrlName."/"))?>">
         	 <img alt="Home" title="<?php echo $linkText?>" src="<?php echo base_url("assets/images/home_icon.png")?>"/>
         	 </a></div>
          <?php endif;endif;?>
         <div class="clearfix"></div>
      </div>      
    </div>
  </div>
</div>
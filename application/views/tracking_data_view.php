<style>
#outcome1 {
	color: green;
}

#outcome2 {
	color: red;
}

#Spot {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}

.modal_progress_bar {
	position: fixed;
	top: 10%;
	margin-left: 393px;
	margin-top: 105px;
	z-index: 1050;
	width: 550px;
	height: 170px;
	background-color: #fff;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	border-color: white;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	outline: 0;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
</style>
<script type="text/javascript">
function get_client()
{
	//clearAllMarker();
	//show_message();
	var baseUrl=document.getElementById('URL').value;
	var clientID=document.getElementById('ClientID').value;
	var sessClientID=document.getElementById('SessClient').value;
	var sessUserID=document.getElementById('SessUser').value;
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_client_vhgp/"+clientID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	        client_vhl_gp(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function client_vhl_gp(response)
{	
	document.getElementById("vehicleGroup").innerHTML=null;
	document.getElementById("vehicle").innerHTML=null;
	var x=document.getElementById("vehicleGroup");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleGp="";
    option.text="";
	option.value="0";
	x.add(option);
    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vh_gp_id;
      vehicleGp=arrs[j].vh_gp_name;
      adds_vh_gp(id,vehicleGp);    
    }
    get_vhl_gp_vehicle();
}
function adds_vh_gp(id,vehicleGp)
{
	var x=document.getElementById("vehicleGroup");
	var option=document.createElement("option");
	option.text=vehicleGp;
    option.value=id;
    x.add(option);
}


function get_vhl_gp_vehicle()
{
	//clearAllMarker();
	//alert("hiiiiiiii");
	var baseUrl=document.getElementById('URL').value;
	var vehicleGp=document.getElementById('vehicleGroup').value;
	if(vehicleGroup!=null)
	{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_vhl_grp_vehicle/"+vehicleGp;	
		xmlhttp.onreadystatechange=function() {
	    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	        	vhl_gp_vehicle(xmlhttp.responseText);
	    	}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
}

function vhl_gp_vehicle(response)
{
	document.getElementById("vehicle").innerHTML=null;
	var x=document.getElementById("vehicle");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleName="";
   
    	//option.text="All Vehicles";
    	//option.value="0";
    	x.add(option);
    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].imei_no;
      vehicleName=arrs[j].vh_name;
      adds(id,vehicleName);    
    }
}
function adds(id,vehicleName)
{
	var x=document.getElementById("vehicle");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}

function progressBar()
{
	$val = $("#go").val();
	$("#hiddenGo").val($val);
	myApp.showPleaseWait(); 
	reportFunction();
}

var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal_progress_bar show" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ><div class="modal-header"><h3>Please wait. Your request is being processed.</h3></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//     alert("suni"+pleaseWaitDiv);
	    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
//             alert("h"+ pleaseWaitDiv.modal());
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();

function reportFunction()
{
	var vh_id=document.getElementById('vehicle').value;
	var fdate=document.getElementById('TrackStartDate').value;
    var tdate=document.getElementById('TrackEndDate').value;
    var baseUrl=document.getElementById('ReportURL').value;
    document.getElementByID("myform").submit();
// 	var url = baseUrl+"/"+vh_id+"/"+fdate+"/"+tdate;
// 	window.location.assign(url);
}

$(function() {
    $( "#TrackStartDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy/mm/dd' });
    $( "#TrackEndDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy/mm/dd' });
	$( "#datepicker" ).datepicker({ dateFormat: 'yy/m/d' });
  });
  
function goCSVButton()
{ 
	$value = $("#csv").val();
	$("#clicked_btn").val($value);
}

function goPDFButton()
{ 
	$value = $("#pdf").val();
	$("#clicked_btn").val($value);
}


</script>

<div class="">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform"
					action="<?php echo(base_url("index.php/tracking_data_ctrl/report_generation/"))?>"
					method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<!--  <input type="text" id="VehicleGroupId" style="display:none;" name="VehicleGroupId" value="<?php //echo((isset($vcle_group_id))? $vcle_group_id:null) ?>" />-->
					<input type="Text" style="display: none;" id="URL" name="URL"
						value="<?php echo base_url("index.php/tracking_data_ctrl/")?>" />
					<input type="Text" style="display: none;" id="ReportURL"
						name="ReportURL"
						value="<?php echo base_url("index.php/tracking_data_ctrl/report_generation/")?>" />
					<input type="Text" style="display: none;" id="SessClient"
						name="SessClient" value="<?php echo $sessClientID ?>" /> <input
						type="Text" style="display: none;" id="SessUser" name="SessUser"
						value="<?php echo $sessUserID ?>" />
					<h1>Tracking Data</h1>

					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>

					<div class="user-fields">


						<div <?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?> class="field">
							<label <?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?>>
								<?php if($sessClientID!=AUTOGRADE_USER)?>Client:<span
								style="color: red;"> *</span>
							</label> <select 
								<?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none;"'?>" id="ClientID"
								name="ClientID" class="form-control input-lg"
								onchange="get_client()">
								<?php if(null!=form_error('ClientID'))echo '<option value="">'.form_error('ClientID',' ',' ').'</option>'; ?>
			              	 <?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER){echo'<option value=""></option>';} foreach ($clientList as $row):?>
                                 <?php if($sessClientID==AUTOGRADE_USER):?>
								<!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
								<option value="<?php echo $row['client_id']?>"><?php echo $row['client_name'];?></option>
			              	 <?php elseif($sessClientID==$row['client_id']): ?>
			              <option value="<?php echo $row['client_id']?>"><?php echo $row['client_name']?> </option>
           <?php endif;?>
          <?php endforeach; endif;?>
			              </select>
						</div>
						<input style="display: none;" id="hiddenVehicleGroup"
							name="hiddenVehicleGroup" value=""></input>
						<div class="field">
							<label>Vehicle Group:<span style="color: red;"> *</span></label>
							<select id="vehicleGroup" name="vehicleGroup" class="form-control input-lg" onchange="get_vhl_gp_vehicle()">
								<option value=""><?php if(null!=form_error("vehicleGroup"))echo form_error('vehicleGroup',' ',' '); ?></option>
<!-- 								<option value="null"></option>"; -->
                          <?php if (! empty ( $vehicleGroupList )) :?>
							<?php foreach ( $vehicleGroupList as $row ): ?>
								<option value="<?php echo $row ['vehicle_group_id']?>"> 
								<?php echo $row ['vehicle_group'] ?> </option>
								<?php endforeach; endif; ?>
							}
						?>
	  </select>
</div>
	<input style="display: none;" id="hiddenVehicle" name="hiddenVehicle" value=""></input>
	<div class="field">
	<label>Vehicle:<span style="color: red;"> *</span></label> 
	<select id="vehicle" name="vehicle" class="form-control input-lg">
		<option value=""><?php if(null!=form_error("vehicle"))echo form_error('vehicle',' ',' '); ?></option>
		<?php if ($vehicleList != null): ?>
		<?php foreach ( $vehicleList as $row ): ?>
		<option value="<?php echo $row ['vehicle_id'] ?> "> <?php echo $row ['vehicle_regnumber'] ?> </option>
		<?php endforeach;endif;?>
	</select>
</div>

<div class="field">
<label>From Date:<span style="color: red;"> *</span></label> 
<input style="font-size: 13px;" maxlength="10" id="TrackStartDate" name="TrackStartDate" class="form-control input-lg"
value="<?php //if(!isset($outcome))echo $track_strt_date?>" placeholder="<?php if(null!=form_error('TrackStartDate')) echo form_error('TrackStartDate',' ',' ');?>" />
</div>

						<div class="field">
							<label>To Date:<span style="color: red;"> *</span></label> <input
								style="font-size: 13px;" maxlength="10" id="TrackEndDate"
								name="TrackEndDate" class="form-control input-lg"
								value="<?php //if(!isset($outcome))echo $track_end_date?>"
								placeholder="<?php if(null!=form_error('TrackEndDate')) echo form_error('TrackEndDate',' ',' ');?>" />
						</div>

						<input style="display: none;" id="hiddenDriver"
							name="hiddenDriver" value=""></input>
							<!--
						<div class="login-actions">
							<span class="login-checkbox"> <input id="Spot" name="Spot"
								type="checkbox" class="field login-checkbox" value="1"
								tabindex="4" <?php //echo(($active==1)? 'checked':'')?> /> <label
								Style="width: 180px; cursor: auto;">Show Spot Name</label>
							</span>
						</div>
						-->
					</div>
					<!-- /login-fields-->

					<div class="field">
						<button class="btn btn-primary" id="go" value="go" name="go"
							type="submit" onClick="progressBar();"><?php  echo "GO"; ?> </button>
						<input style="display: none;" hiddenGo"
							name="hiddenGo"
							value=""></input>
						<?php $reloadURL=base_url("index.php/tracking_data_ctrl/")?>	
						<button class="btn btn-primary" type="reset"
							onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>

					</div>

				</form>
			</div>
			<!-- /content -->
		</div>

		<!-- 		to get the details in table -->

		<!-- Login -->
		<?php if (! empty ( $get_gridDetails )) { ?>
		<form id="myform1"
			action="<?php echo(base_url("index.php/tracking_data_ctrl/report_generation/"))?>"
			method="post" class="form-horizontal">
			</br>

			<!--  <textarea style="display:none;" type="text" id="Query" name="Query" class="form-control input-lg" ><?php //echo $output?></textarea>-->
			<input type="text" id="temp" style="display: none;" name="temp"
				value="-1" />
			<div class="container">
				</br>
				<div class="table-responsive" style="height: 250px;">
					<table width="100%" class="user-dts" border=1>
						<tr>
							<th>Vehicle Name(Car)</th>
							<th>GPS Running No</th>
							<th>IMEI No</th>
							<th>Date/Time</th>
							<th>Speed(km/h)(Speed Limit)</th>
							<th>status</th>
							<th>Driver Name</th>
							<th>Spot</th>
						</tr>
              <?php foreach ($get_gridDetails as $row):?>
              <tr>
							<td style="text-align: center;"><?php echo $row['temp_vehicle_name']?></td>
							<td style="text-align: center;"><?php echo $row['gps_runningno'] ?></td>
							<td style="text-align: center;"><?php echo $row['gps_imeino'] ?></td>
							<td style="text-align: center;"><?php echo $row['gps_datetime']?></td>
							<td style="text-align: center;"><?php echo $row['gps_speed']?></td>
							<td style="text-align: center;"><?php echo $row['gps_digitalinputstatus']?></td>
							<td style="text-align: center;"><?php echo $row['driver_name']?></td>
							<td style="width: 24em; word-wrap: break-word;"><?php echo $row['spot'] //echo getAddress($row['lat'],$row['long']); ?></td>
						</tr>        
             <?php endforeach; ?>
     </table>
				</div>
				<div class="field">
					<button id="csv" name="csv" class="btn btn-primary" type="submit"
						value="csv" onClick="goCSVButton();"><?php echo "Export to CSV"; ?> </button>
					<button id="pdf" name="pdf" class="btn btn-primary" type="submit"
						value="pdf" onClick="goPDFButton();"><?php echo "Export to PDF"; ?> </button>
				<?php //$reloadURL=base_url("index.php/speed_violations_ctrl/map_view/");?>
<!-- 				<button class="btn btn-primary" type="button" onclick="window.open('<?php //echo $reloadURL; ?>')">View Map</button>-->

					<input id="clicked_btn" name="clicked_btn" style="display: none;"></input>
				</div>
			</div>

		</form>
          <?php } else if(empty($get_gridDetails) && $no_data) { ?>
          	<div class="container">
			</br>
			<table width="100%" class="user-dts" border=0>
				<tr>
					<th>Vehicle Name(Car)</th>
					<th>Gps Running No</th>
					<th>IMEI No</th>
					<th>Date/Time</th>
					<th>Speed(km/h)(Speed Limit)</th>
					<th>status</th>
					<th>Driver Name</th>
					<th>Spot</th>
				</tr>
				<tr>

					<td></td>
					<td></td>
					<td></td>
					<td style="color: red; padding: 20px; font-size: 18px;">No Records
						found</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>

				</tr>
			</table>
		</div>
          	
          	
          	
		<?php } ?>
          <div class="clearfix"></div>
		<!--  </div> -->
		<!-- /content -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- 		table ends here.... -->
	</div>
</div>
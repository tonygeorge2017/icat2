<div id="wrapper"> 
  <!-- style="display : none;" -->  
  <input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/geofence_ctrl/")?>"/>  
  <input type="Text" style="display : none;" id="wayPntURL" name="wayPntURL" value="<?php echo base_url('assets/images/')?>"/>
  <input type="Text" style="display : none;" id="SessClient" name="SessClient" value="<?php echo $sessClientID ?>"/>
  <input type="Text" style="display : none;" id="SessUser" name="SessUser" value="<?php echo $sessUserID ?>"/>
  
  
  <div id="sidebar-wrapper">
    <div class="nav">
      <div class="field">
        <input id="Replay" name="TraceType" type="radio" class="field login-checkbox" value="1" tabindex="1" onchange="radio_btn_click()" />
        <label>Trace History</label>
        &nbsp;
        <input id="Live" name="TraceType" type="radio" class="field login-checkbox" value="1" tabindex="2" checked onchange="radio_btn_click()"/>
        <label>Live Track</label>
      </div>
      <div class="field" id="PlottingType" style="display:none;">
        <input id="StepByStepPlotting" name="Plotting" type="radio" class="field login-checkbox" value="1" tabindex="1" onchange="plotting_radio_btn_click()" />
        <label>Step-by-step plotting</label>
        &nbsp;
        <input id="BulkPlotting" name="Plotting" type="radio" class="field login-checkbox" value="1" tabindex="2" checked onchange="plotting_radio_btn_click()"/>
        <label>Bulk plotting</label>
      </div>
      <div class="field">
        <label <?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER) ?>>Client :</label>
          <select style="font-size: 15px;<?php if($sessClientID!=AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER) ?>" id="ClientID"  name="ClientID" class="form-control input-lg" onchange="get_client()" >                     
           <?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER){echo'<option value=""></option>';} foreach ($clientList as $row):?>
           <?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
           <option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
            <?php elseif($sessClientID==$row['client_id']): ?>
           <option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
           <?php endif;?>
          <?php endforeach; endif;?>
         </select>
       </div>
      <div class="field">
	  <label>vehicle Group :</label>
      <select style="font-size: 15px;" id="vehicleGroup" name="vehicleGroup" class="form-control input-lg" onchange="get_vhl_gp_vehicle()">
      <?php
      	if(!empty($vehicleGroupList)) 
			foreach ($vehicleGroupList as $row)
			{
				echo "<option value='".$row['vh_gp_id']."'>".$row['vh_gp_name']."</option>";
			}
	 ?>
	  </select>
      </div>
      <div class="field">
        <label>Vehicle :</label>
        <select style="font-size: 15px;" id="vehicle" name="vehicle" class="form-control input-lg" onchange="clearAllMarker()">
          <?php 
			if($vehicleList!=null)
			{
			 	echo "<option value='0'>All Vehicles</option>";
	         	foreach ($vehicleList as $row)
	         	{
		     		echo "<option value='".$row['vh_id']."'>".$row['vh_name']."</option>";
			 	}
			}
		 ?>
        </select>
      </div>
      
      <div class="field">
        <label>Time Selection :</label>
        <select style="font-size: 15px;" id="TimeSelection" name="TimeSelection" class="form-control input-lg" onchange="time_selection_change()">
         <option value="today">Today</option>
         <option value="yesterday">Yesterday</option>
         <option value="custom">Custom Date&Time</option>		
        </select>
      </div>
      <?php
        
         $start_date=$startDate;
         $end_date=$endDate;
  		?>
      
      <div class="field">
        <label>Start date&amp;time :</label>
        <input style="font-size: 15px;" type="datetime-local" id="Fdaytime" name="Fdaytime" class="form-control input-lg" value="<?php echo $start_date->format('Y-m-d').'T'.$start_date->format('H:i')?>"/>
      </div>
      <div class="field">
        <label>End date&amp;time :</label>
        <input style="font-size: 15px;" type="datetime-local" id="Tdaytime" name="Tdaytime" class="form-control input-lg" value="<?php echo $end_date->format('Y-m-d').'T'.$end_date->format('H:i')?>"/>
      </div>
      <div class="field">
        <button id="startTimer" class="btn btn-primary" onclick="timerFunction()">Go</button>
        &nbsp;&nbsp;
        <button id="stopTimer" class="btn btn-primary" onclick="stopTimerFunction()">Stop</button>
      </div>
      <div class="field">
        <label>Info : </label>
        <textarea style="font-size: 15px;" id="info" name="info" cols="" rows="6" class="form-control input-lg" readonly></textarea>
      </div>
      <p id="demo"></p>
    </div>
  </div>
  <div id="page-content-wrapper">
    <div class="divx" id="googleMap"></div>
  </div>
</div>
</body>
</html>
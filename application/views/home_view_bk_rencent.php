<script src="http://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>"></script>
<script type="text/javascript">
var map;
var startMarke;
var zoomLevel=15;
var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:2,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
		  };

$(function(){				  
	map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	google.maps.event.addListener(map, 'zoom_changed', function() {
				 zoomLevel=map.getZoom();				 
			});
	startMarke=new google.maps.Marker({map:map});
	var timers;
	var is_lost = 10;//10 minutes
	$('#move').html('0');
	$('#stope').html('0');
	$('#halt').html('0');
	$('#lost').html('0');
	
	dashboard_update();
	timers=setInterval(dashboard_update,10000);
	
	$("#ClientID").on('change', function(){
		dashboard_update();
	});

	function dashboard_update()
	{
		if($("#ClientID").val())
		{	
			var move=0,halt=0,stop=0,lost=0;
			var clr='#333';
			$.ajax({
				type:'GET',
				url: 'home_ctrl/dashboard/'+$("#ClientID").val(),
				dataType: 'json',				
				success: function(result) {						
					$('#Grid tbody').html('');
					if(result.length>0)
					{
						$.each(result, function(k,v){
							if(parseInt(v["diff"]) >= is_lost)
							{
								lost+=1;
								clr='#286090';
							}
							else if(v["ig"]=='N')
							{
								stop+=1;
								clr='#c9302c';
							}
							else if(v["ig"]=='Y' && (parseFloat(v['sp'])*1.852) > parseFloat(2))
							{
								move+=1;
								clr='#5cb85c';
							}
							else if(v["ig"]=='Y' && (parseFloat(v['sp'])*1.852) < parseFloat(2))
							{
								halt+=1;
								clr='#f0ad4e';
							}
							$('#Grid tbody').append('<tr><td>'+v['imei']+'</td><td>'+v['vh']+'</td><td>'+v['rec']+'</td><td><i style="color:'+clr+'" title="'+v['vh']+'" class="fa fa-map-marker fa-2x cur" aria-hidden="true" onclick="show_marker({lat:'+parseFloat(v['lat'])+',lng:'+parseFloat(v['lng'])+'})"></i></td></tr>');
						});
						$('#move').html(move);
						$('#stope').html(stop);
						$('#halt').html(halt);
						$('#lost').html(lost);

					}else{
						$('#Grid tbody').append('<tr><td colspan="4" style="text-align:center;">-- No Data --</td></tr>');
					}				
				}
			});
		}
	}
	
	$('#go-to-top').click(function(){ $("body").animate({ scrollTop: 100}, 600);}); 
	

});
function show_marker(crt)
{	
	startMarke.setPosition(crt);
	//startMarke.setAnimation(google.maps.Animation.DROP);
	startMarke.setAnimation(google.maps.Animation.BOUNCE);
	if(zoomLevel<12)
		zoomLevel=12;
	map.setZoom(zoomLevel);
	map.setCenter(crt);
	
}
</script>

<style type="text/css">
	.cur{
		cursor: pointer
	}
	td{		
		padding-left: 5px;
		padding-right: 5px
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-lg-12 dashboard-main">				
			<nav class="navbar navbar-inverse" <?php echo(($sessClientID != AUTOGRADE_USER)?'style="display: none;"' :'' );?>>
				<div class="container-fluid">
					<ul class="nav navbar-nav">      
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Distributor &amp; Dealer
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/distributor_ctrl")?>">Distributor</a></li>
					<li><a href="<?php echo base_url("index.php/dealer_ctrl")?>">Dealer</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Device
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/device_type_ctrl")?>">Device Type</a></li>
					<li><a href="<?php echo base_url("index.php/device_ctrl")?>">Device</a></li>
					<li><a href="<?php echo base_url("index.php/device_allocation_dealer_ctrl")?>">Device Allocation to Dealer</a></li>
					<li><a href="<?php echo base_url("index.php/device_allocation_ctrl")?>">Device Allocation to Client</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Client
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/client_ctrl")?>">Client</a></li>
					<li><a href="<?php echo base_url("index.php/client_license_ctrl")?>">Client License</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Tester
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/device_monitoring_ctrl")?>">Device Monitoring</a></li>
					<li><a href="<?php echo base_url("index.php/vehicle_admin_tracking_ctrl")?>">Tester Tracking</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/service_ctrl")?>">Service</a></li>
					<li><a href="<?php echo base_url("index.php/settings_ctrl")?>">Settings</a></li>
					<li><a href="<?php echo base_url("index.php/vehicle_make_model_ctrl")?>"> Vehicle Make &amp; Model</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Others
					<span class="caret"></span></a>
					<ul class="dropdown-menu">	
					<!--
					<li><a href="<?php //echo base_url("index.php/fuel_coupon_ctrl")?>">Fuel Coupon</a></li>
					<li><a href="<?php //echo base_url("index.php/fuel_mis_i")?>">Fuel Report-I</a></li>
					<li><a href="<?php //echo base_url("index.php/fuel_mis_ii")?>">Fuel Report-II</a></li>
					<li><a href="<?php //echo base_url("index.php/daily_activity_rpt_ctrl")?>">Daily Activity Report</a></li>
					<li><a href="<?php //echo base_url("index.php/vehicle_summary_rpt_ctrl")?>">Vehicle Summery Report</a></li>
					-->
					<li><a href="<?php echo base_url("index.php/insurance_agency_ctrl")?>">Insurance Agency</a></li>
					<li><a href="<?php echo base_url("index.php/vehicle_service_ctrl")?>">Vehicle Service</a></li>
					<li><a href="<?php echo base_url("index.php/individual_user_ctrl")?>">Individual User</a></li>
					<li><a href="<?php echo base_url("index.php/individual_vt_ctrl")?>">Personal Tracking</a></li>
					<li><a href="<?php echo base_url("index.php/distributor_dashboard_ctrl")?>">Distributor Dashboard</a></li>
					<li><a href="<?php echo base_url("index.php/dealer_dashboard_ctrl")?>">Dealer Dashboard</a></li>
					</ul>
					</li>    
					<li>
						<form id="myform" action="<?php echo(base_url("index.php/home_ctrl/view"))?>" method="post">
							<select id="ClientID" name="ClientID" style=" padding: 15px;">           
							<?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER){echo'<option value="">-- Select Client --</option>';} foreach ($clientList as $row):?>
							<?php if($sessClientID==AUTOGRADE_USER):?>								
							<option value="<?php echo $row['client_id']?>"	<?php echo(($clientID==$row['client_id'])?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php elseif($sessClientID==$row['client_id']): ?>
							<option value="<?php echo $row['client_id']?>"><?php echo $row['client_name']?> </option>
							<?php endif;?>
							<?php endforeach; endif;?>
							</select>
						</form>
					</li>  
					</ul>
				</div>
			</nav>	
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#5cb85c; color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">							
							<div>Moving</div>
							<div id="move" class="huge" style="font-size:25px;">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#c9302c; color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">							
							<div>Stopped</div>
							<div id="stope" class="huge" style="font-size:25px;">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#f0ad4e; color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>Halt</div>
							<div id="halt" class="huge" style="font-size: 25px;">0</div>						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#286090;color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>Lost Connection</div>
							<div id="lost" class="huge" style="font-size:25px;">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row" >
		<!-- For Vehicle detail-->
		<div class="col-md-6" style=" height: 15em; overflow: auto; padding: 15px; border-radius:10px; height:400px; background-color:#ECECEC;">
			<div class="table-responsive" > 
				<table id="Grid" border="1" width="100%">
					<thead>
						<tr>
							<th style="text-align: center;">IMEI no.</th>
							<th style="text-align: center;">Vehicle</th>
							<th style="text-align: center;">Date &amp; Time</th>
							<th style="text-align: center;">Map</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="4" style="text-align:center;">-- No Data --</td>
						</tr>
    				</tbody>										
				</table>	 
			</div>
		</div>
		<!-- For Map View-->
		<div class="col-md-6 dashboard-main" style="padding: 5px; border-radius:10px; height:400px; background-color: lightblue;">
			<div id="googleMap" style=" border-style: inset; border-radius:10px; height: 100%">
			</div>
			<button id="go-to-top"  style=" border-style: solid; border-width: 2px; border-color:#758C30; background-color:#90C551; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1.428571429; border-radius: 15px; position: absolute;right: 0px; top:200px;">^</button>			
		</div>
	</div>

</div>
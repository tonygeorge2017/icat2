<script>
function clientName()
{
	document.getElementById("temp").value=document.getElementById("ClientName").value;//ClientName
	document.getElementById("myform").submit();
}
</script>
<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
#InsuranceAgencyIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/insurance_agency_ctrl/vts_insurance_agency_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="InsuranceAgencyId" style="display:none;"  name="InsuranceAgencyId" value="<?php echo((isset($insr_agcy_id))? $insr_agcy_id:null) ?>" />
					<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
					<h1>Insurance Agency Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>
					<div class="user-fields">
					
					<?php if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
							<select name="ClientName" id="ClientName" onchange="clientName()">
							<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
							<?php foreach ($clientList as $row):?>
							<option value="<?php echo $row['client_id']?>" <?php echo(($insr_agcy_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php endforeach;?>
							</select>
						</div>
					<?php endif; ?>
						<div class="field">
							<label>Name:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="40" id="InsuranceAgency" name="InsuranceAgency" value="<?php if(!isset($outcome))echo $insr_agcy?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgency'))echo form_error('InsuranceAgency',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Address: </label>
							<textarea type="text" maxlength="500" id="InsuranceAgencyAddress" name="InsuranceAgencyAddress" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyAddress'))echo form_error('InsuranceAgencyAddress',' ',' ');?>" ><?php if(!isset($outcome))echo $insr_agcy_address?></textarea>
						</div>
						<div class="field">
							<label>Postal Code: </label>
							<input type="text" maxlength="10" id="InsuranceAgencyPostCode" name="InsuranceAgencyPostCode" value="<?php if(!isset($outcome))echo $insr_agcy_post_code?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyPostCode'))echo form_error('InsuranceAgencyPostCode',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Branch Phone:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="10" id="InsuranceAgencyBranchPhone" name="InsuranceAgencyBranchPhone" value="<?php if(!isset($outcome))echo $insr_agcy_branch_phone?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyBranchPhone'))echo form_error('InsuranceAgencyBranchPhone',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Branch Email: </label>
							<input type="text" maxlength="40" id="InsuranceAgencyBranchEmail" name="InsuranceAgencyBranchEmail" value="<?php if(!isset($outcome))echo $insr_agcy_branch_email?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyBranchEmail'))echo form_error('InsuranceAgencyBranchEmail',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Person: </label>
							<input type="text" maxlength="40" id="InsuranceAgencyContactPerson" name="InsuranceAgencyContactPerson" value="<?php if(!isset($outcome))echo $insr_agcy_contact_person?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyContactPerson'))echo form_error('InsuranceAgencyContactPerson',' ',' ');?>" />						
						</div>
						<div class="field">
							<label>Contact Designation: </label>
							<input type="text" maxlength="40" id="InsuranceAgencyContactDesignation" name="InsuranceAgencyContactDesignation" value="<?php if(!isset($outcome))echo $insr_agcy_contact_designation?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyContactDesignation'))echo form_error('InsuranceAgencyContactDesignation',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Email: </label>
							<input type="text" maxlength="40" id="InsuranceAgencyContactEmail" name="InsuranceAgencyContactEmail" value="<?php if(!isset($outcome))echo $insr_agcy_contact_email?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyContactEmail'))echo form_error('InsuranceAgencyContactEmail',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Phone:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="10" id="InsuranceAgencyContactPhone" name="InsuranceAgencyContactPhone" value="<?php if(!isset($outcome))echo $insr_agcy_contact_phone?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyContactPhone'))echo form_error('InsuranceAgencyContactPhone',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Remarks:</label>
							<textarea type="text" maxlength="500" id="InsuranceAgencyRemarks" name="InsuranceAgencyRemarks" class="form-control input-lg" placeholder="<?php if(null!=form_error('InsuranceAgencyRemarks'))echo form_error('InsuranceAgencyRemarks',' ',' ');?>"><?php if(!isset($outcome))echo $insr_agcy_remarks?></textarea>
						</div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="InsuranceAgencyIsActive" name="InsuranceAgencyIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>> 
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br/>
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($insr_agcy_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/insurance_agency_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/insurance_agency_ctrl/editOrDelete_vts_insurance_agency/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						<tr>
							<th>Agency</th>
							<th>Contact Person</th>
							<th>Contact Mobile</th>
							<th>Actions</th>
						</tr>
						<?php if($insuranceAgencyList!=null): foreach ($insuranceAgencyList as $row): ?> 
						<tr>
							<td><?php echo trim($row['insurance_agency'])?></td>
							<td><?php echo trim($row['insurance_agency_contactperson'])?></td>
							<td><?php echo trim($row['insurance_agency_contactphone'])?></td>
							<td>
								<?php $md_id = md5(trim($row['insurance_agency_client_id']));?>
								<a href="<?php echo(base_url("index.php/insurance_agency_ctrl/editOrDelete_vts_insurance_agency/".$md_id."/".md5('edit')."/".md5(trim($row['insurance_agency_id']))))?>" title="Edit the Insurance Agency"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
								<a href="<?php echo(base_url("index.php/insurance_agency_ctrl/editOrDelete_vts_insurance_agency/".$md_id."/".md5('delete')."/".md5(trim($row['insurance_agency_id']))))?>" title="Delete the Insurance Agency" onclick="return confirm('Do you want to delete the selected Insurance Agency?')"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php endforeach; endif;?>
						</table>
				</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
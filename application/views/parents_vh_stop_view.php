<script type="text/javascript">
/*
 * This function is used to get all vehicle group which is belongs
 * particular client.
 * Return type - void
 */
function pt_vh_stop_get_client()
{
	var baseUrl=document.getElementById('URL').value;
	var clientID=document.getElementById('ClientID').value;
	document.getElementById("VehicleGroupID").innerHTML=null;
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("StopID").innerHTML=null;
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Student</th><th>Class</th><th>Parent</th><th>Select</th></tr></table>";
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_client_vhgp/"+clientID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	        pt_vh_stop_client_vhl_gp(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
/*
 * This function process the json string which is get by pt_vh_stop_get_client 
 * function and call pt_vh_stop_adds_vh_gp function to refresh the Vehicle Group dropdown
 * @param
 *  response - json string which contain list of vehicle group id and name
 * Return type - void
 */
function pt_vh_stop_client_vhl_gp(response)
{		
	var x=document.getElementById("VehicleGroupID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleGp="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vehicle_group_id;
      vehicleGp=arrs[j].vehicle_group;
      pt_vh_stop_adds_vh_gp(id,vehicleGp);    
    }
    pt_vh_stop_vh_gp_change();
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - vehicle group id
 *  vehicleGp - vehicle group name
 * Return type - void
 */
function pt_vh_stop_adds_vh_gp(id,vehicleGp)
{
	var x=document.getElementById("VehicleGroupID");
	var option=document.createElement("option");
	option.text=vehicleGp;
    option.value=id;
    x.add(option);
}
 /*
  * This function is used to get all vehicle which is related to
  * selected vehicle group.
  * Return type - void
  */
function pt_vh_stop_vh_gp_change()
{	
	var baseUrl=document.getElementById('URL').value;
	var vehiclegp=document.getElementById('VehicleGroupID').value;	
	var xmlhttp = new XMLHttpRequest();
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("StopID").innerHTML=null;
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Student</th><th>Class</th><th>Parent</th><th>Select</th></tr></table>";
	var url = baseUrl+"/get_vhl_grp_vehicle/"+vehiclegp;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	    	pt_vh_stop_vh_for_vhgp(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
/*
 * This function process the json string which is get by pt_vh_stop_vh_gp_change 
 * function and call pt_vh_stop_adds_vehicle function to refresh the Vehicle dropdown
 * @param
 *  response - json string which contain list of vehicle id and name
 * Return type - void
 */
function pt_vh_stop_vh_for_vhgp(response)
{
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleName="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vehicle_id;
      vehicleName=arrs[j].vehicle_regnumber;
      pt_vh_stop_adds_vehicle(id,vehicleName);    
    }
    pt_vh_stop_vehicle_change();
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - vehicle group id
 *  vehicleName - vehicle name
 * Return type - void
 */
function pt_vh_stop_adds_vehicle(id,vehicleName)
{
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}
/*
 * This function call when onchange event of vehicle
 * to refres the stop dropdown
 * Return type - void
 */
 function pt_vh_stop_vehicle_change()
 {	
 	var baseUrl=document.getElementById('URL').value;
 	var vehicleID=document.getElementById('VehicleID').value;
	document.getElementById("StopID").innerHTML=null;	
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Student</th><th>Class</th><th>Parent</th><th>Select</th></tr></table>";	
 	var xmlhttp = new XMLHttpRequest();
 	var url = baseUrl+"/get_vehicle_stop/"+vehicleID;	
 	xmlhttp.onreadystatechange=function() {
 	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { 	 	   	    	
 	    	pt_vh_stop_for_vehicle(xmlhttp.responseText); 	    	
 	    	}
 	}
 	xmlhttp.open("GET", url, true);
 	xmlhttp.send();
 }
 /*
  * This function process the json string which is get by pt_vh_stop_vehicle_change 
  * function and call pt_vh_stop_adds_stop function to refresh the stop dropdown list
  * @param
  *  response - json string which contain list of stop id and name
  * Return type - void
  */
 function pt_vh_stop_for_vehicle(response)
 {
 	var x=document.getElementById("StopID");
 	var option=document.createElement("option");    
     var arrs = JSON.parse(response);
     var id="";var stopName="";    
     for(j=0;j<arrs.length;j++)
     {
       id=arrs[j].stop_id;
       stopName=arrs[j].stop_name;
       pt_vh_stop_adds_stop(id,stopName);    
     }
     pt_vh_stop_change();
 }
 /*
  * This function is use to add new <option> to <select>
  * @param
  *  id - stop id
  *  stopName - stop name
  * Return type - void
  */
 function pt_vh_stop_adds_stop(id,stopName)
 {
 	var x=document.getElementById("StopID");
 	var option=document.createElement("option");
 	option.text=stopName;
    option.value=id;
    x.add(option);
 }
  /*
  * This function is used to refresh the table when 'onchange'
  * of client or vehcile group or vehicle or stop dropdown list
  * selection change.
  * Return type - void
  */
 function pt_vh_stop_change()
 {	
	 var baseUrl=document.getElementById('URL').value;
	 var clientID=document.getElementById('ClientID').value;
	 var stopID=(document.getElementById('StopID').value != "")?document.getElementById('StopID').value:"0";
	 var search=document.getElementById('SearchBox').value;
	 document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Student</th><th>Class</th><th>Parent</th><th>Select</th></tr></table>";
	 var xmlhttp = new XMLHttpRequest();
	 var url = baseUrl+"/make_table/"+clientID+"/"+stopID+"/"+search;	
	 xmlhttp.onreadystatechange=function() {
	     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { 	 	   	    	
	     	document.getElementById('Grid').innerHTML=xmlhttp.responseText; 	    	
	     	}
	 }
	 xmlhttp.open("GET", url, true);
	 xmlhttp.send();
 }
</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <form id="myform" action="<?php echo(base_url("index.php/parents_vh_stop_ctrl/validate_parent_stop/"))?>" method="post" class="form-horizontal">
           <!-- style="display:none;" -->
          <input type="text" id="OnClientID" name="OnClientID" style="display: none" value="-1"/>         
          <input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/parents_vh_stop_ctrl/"))?>"/>
          <h1>Parents Vehicle Stop Management</h1>
          <?php if(isset($outcome)) echo $outcome;?>
          <div class="user-fields">
          <div class="field">
              <label for="useremail" <?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?>>Client:<span <?php echo(($sessClientID!=AUTOGRADE_USER)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <select id="ClientID"  name="ClientID"  onchange="pt_vh_stop_get_client()" <?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?>>  
               <?php if(null!=form_error('ClientID'))echo '<option value="">'.form_error('ClientID',' ',' ').'</option>'; ?>            
               <?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER & null==form_error('ClientID')){echo'<option value=""></option>';} foreach ($clientList as $row):?>
               	<?php if($sessClientID==AUTOGRADE_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
              		<option value="<?php echo $row['client_id']?>" <?php echo(($clientID==$row['client_id'])?'selected':'')?>><?php echo $row['client_name']?> </option>
                <?php elseif($sessClientID==$row['client_id']): ?>
              		<option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
              	<?php endif;?>
               <?php endforeach; endif;?>
              </select>
            </div>            
            <div class="field">
              <label for="useremail">Vehicle Group:<span style="color:red;"> *</span></label>
              <select id="VehicleGroupID"  name="VehicleGroupID" onchange="pt_vh_stop_vh_gp_change()">              
              <option value=""><?php if(null!=form_error('VehicleGroupID'))echo form_error('VehicleGroupID',' ',' '); ?></option>
               <?php if($vehicleGpList!=null):  foreach ($vehicleGpList as $row):?>
               		<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vehicleGpID==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>                       
            
            <div class="field">
              <label for="useremail">Vehicle:<span style="color:red;"> *</span></label>
              <select id="VehicleID"  name="VehicleID" onchange="pt_vh_stop_vehicle_change()">   
              <option value=""><?php if(null!=form_error('VehicleID'))echo form_error('VehicleID',' ',' '); ?></option>           
               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
               		<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vehicleID==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div class="field">
              <label for="useremail">Stop:<span style="color:red;"> *</span></label>
              <select id="StopID"  name="StopID" onchange="pt_vh_stop_change()">
              <option value=""><?php if(null!=form_error('StopID'))echo form_error('StopID',' ',' '); ?></option>           
               <?php if($stopList!=null): foreach ($stopList as $row):?>
               		<option value="<?php echo $row['stop_id']?>" <?php echo(($stopID==$row['stop_id'])?'selected':'')?>><?php echo $row['stop_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
          </div>
          <!-- /login-fields -->
		 <div class="field">
            <button class="btn btn-primary" type="submit">Save</button>
            <?php $reloadURL=base_url("index.php/parents_vh_stop_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->																	
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          </form>
          <form action="">  
          <div class="user-fields">
          <div class="field">       
           <a style=" float: right;" href="#Grid" title="Edit Device Details" onclick="pt_vh_stop_change()"><img style="padding-top:5px; float: right;width: 25px; height: 25px;" alt="Search" src="<?php echo base_url("assets/images/search.png")?>"/></a>
          <input style=" float: right; margin-right:0px; width: 140px; height: 30px; font-size: 15px" type="text" id="SearchBox" class="form-control input-lg" value="" placeholder="Search.."  maxlength="50"/>
          </div></div><br><br>
          <div class="table-responsive" id="Grid">                            
            <table style="width:100%;" class="user-dts">              
              <tr>
                <th>Student</th>
                <th>Class</th>
                <th>Parent</th>
                <th>Select</th>
              </tr>
              <?php if($parentList!=null): foreach ($parentList as $row):?>
              <?php              
              		$checked=false;
              		if($selectedParentIDList!=null)
              		{
              			$checked=false;
              			foreach ($selectedParentIDList as $selected)
              			{
              				if($row['parent_id']==$selected)
              				{
              					$checked=true;
              					break; 
              				}
              			}
              		}
                ?>  
               	<tr>
              		<td><?php echo $row['parent_student_name']?></td>
               		<td><?php echo $row['parent_student_class']?></td>                	
               		<td><?php echo $row['parent_name']?></td>
               		<td><input name="SelectedParentID[]" type="checkbox" value="<?php echo $row['parent_id']?>" <?php echo($checked?'checked':'')?>/></td>
               	</tr>
               <?php endforeach; endif;?>
            </table>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
      <!-- /content -->
    </div>
  </div>
</div>
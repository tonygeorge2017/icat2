<html>
<style>
.block{
	min-height: 246px;
	background-color: #4CA7F3;
}
.link_block{
	text-align: right;
	margin-top: 10px;
}
a{
	color: #333;
}

</style>

<div class="container">
	<div class="row">
		<div class="col-lg-12 dashboard-main" style="margin-top:80px;">
				<div class="dash-tiles row">
					<div class="col-sm-6">
						<div class="dash-tile dash-tile-ocean clearfix animation-pullDown block" style="background-color:red;">
							<h3 style="color:black">Autograde TANK</h2>
							<p style="color:white">
								Autograde TANK integrates vehicle tracking, transport and safety solutions.
								The system uses the GPS technology for transmitting key vehicle information to our web server.
								The web software, with its easy to understand features like custom reporting, real time alert, 
								geo-fencing and much more, help to get all the information at a single glance and leads to efficient 
								vehicle management and cost efficiency.
							</p>
							<div class="link_block">
								<a href="Solutions_ctrl/downloadPDF/Autograde_TANK" style="color:white">Download PDF</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="dash-tile dash-tile-ocean clearfix animation-pullDown block" style="background-color:red;">
							<h3 style="color:black">TANK for Schools</h2>
							<p style="color:white">
								We understand the importance of kid's safety. Rising statistics of school bus accidents is really  a stress to parents. 
								With a real-time gps system / like that of a child safety assurance system / school bus operations can keep parents happy and reduce it's own overhead costs by being more efficient. 
								Live information allows the system operator to provide accurate information to parents.
							</p>
							<div class="link_block">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="dash-tile dash-tile-ocean clearfix animation-pullDown block" style="background-color:red;">
							<h3 style="color:black">TANK for Fleet Management</h2>
							<p style="color:white">
								In today's challenging business environment, company's competitive edge depends on its ability to maximize efficiency and reduce costs while keeping customers happy. 
								Autograde TANK's can help improve mobile worker productivity and increase customer satisfaction while delivering significant savings on labor, fuel and other expenses.
							</p>
							<div class="link_block">
							</div>
						</div>
					</div>
					<!-- <div class="col-sm-6">
						<div class="dash-tile dash-tile-ocean clearfix animation-pullDown block">
							<div class="link_block">
							</div>
						</div> 
					</div>-->
				</div>
		</div>
	</div>
</div>

</html>

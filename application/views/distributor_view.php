<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}

#DistributorIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/distributor_ctrl/vts_distributor_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<!--  <input type="text" id="DistributorId"  name="DistributorId" value="<?php //echo((isset($distr_id))? $distr_id:null) ?>" />-->
					<input type="text" style="display:none;" id="DistributorId"  name="DistributorId" value="<?php echo((isset($distr_id))? $distr_id:null) ?>" />
					
					<h1>Distributor Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>
					<div class="user-fields">
						<div class="field">
							<label>Name:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="100" id="DistributorName" name="DistributorName" value="<?php if(!isset($outcome))echo $distr_name?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorName'))echo form_error('DistributorName',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Location: </label>
							<input type="text" maxlength="40" id="DistributorLocation" name="DistributorLocation" value="<?php if(!isset($outcome))echo $distr_location?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorLocation'))echo form_error('DistributorLocation',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Address: </label>
							<textarea type="text" maxlength="500" id="DistributorAddress" name="DistributorAddress" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorAddress'))echo form_error('DistributorAddress',' ',' ');?>" ><?php if(!isset($outcome))echo $distr_address?></textarea>
						</div>
						<div class="field">
							<label>Postal Code: </label>
							<input type="text" maxlength="20" id="DistributorPostCode" name="DistributorPostCode" value="<?php if(!isset($outcome))echo $distr_post_code?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorPostCode'))echo form_error('DistributorPostCode',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Person:<span style="color:red;"> *</span></label>
							<input type="text" id="DistributorContactPerson" name="DistributorContactPerson" value="<?php if(!isset($outcome))echo $distr_contact_person?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorContactPerson'))echo form_error('DistributorContactPerson',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Designation: </label>
							<input type="text" maxlength="40" id="DistributorContactDesignation" name="DistributorContactDesignation" value="<?php if(!isset($outcome))echo $distr_contact_designation?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorContactDesignation'))echo form_error('DistributorContactDesignation',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Email:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="40" id="DistributorContactEmail" name="DistributorContactEmail" value="<?php if(!isset($outcome))echo $distr_contact_email?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorContactEmail'))echo form_error('DistributorContactEmail',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Mobile Number:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="10" id="DistributorContactPhone" name="DistributorContactPhone" value="<?php if(!isset($outcome))echo $distr_contact_phone?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorContactPhone'))echo form_error('DistributorContactPhone',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Remarks:</label>
							<textarea type="text" maxlength="500" id="DistributorRemarks" name="DistributorRemarks" class="form-control input-lg" placeholder="<?php if(null!=form_error('DistributorRemarks'))echo form_error('DistributorRemarks',' ',' ');?>"><?php if(!isset($outcome))echo $distr_remarks?></textarea>
						</div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="DistributorIsActive" name="DistributorIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>>
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br/>
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($distr_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/distributor_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/distributor_ctrl/editOrDelete_vts_distributor/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						<tr>
							<th>Distributor</th>
							<th>Location</th>
							<th>Mobile</th>
							<th>Action</th>
						</tr>
						<?php foreach ($DistributorList as $row): ?>
						<tr>
							<td><?php echo trim($row['dist_name'])?></td>
							<td><?php echo trim($row['dist_location'])?></td>
							<td><?php echo trim($row['dist_contact_phone'])?></td>
							<td>
								<a href="<?php echo(base_url("index.php/distributor_ctrl/editOrDelete_vts_distributor/?dist_id=edit-".trim($row['dist_id'])))?>" title="Edit Distributor details"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
								<a href="<?php echo(base_url("index.php/distributor_ctrl/editOrDelete_vts_distributor/?dist_id=delete-".trim($row['dist_id'])))?>" title="Delete Distributor details" onclick="return confirm('Do you want to delete the Distributor')"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php endforeach; ?>
						</table>
				</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>"></script>
<script type="text/javascript">
var map;
var startMarke;
var zoomLevel=15;
var monitor_vehicle_id=0;
var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:2,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
/*
var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
*/

var car ="M17.402,0H5.643C2.526,0,0,3.467,0,6.584v46.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";

car_marker = {
		path: car,
		scale: 0.6,
		strokeColor: '#807776',
		strokeWeight: .9,
		fillOpacity: 1,
		fillColor: '#3D9B55',
		offset: '5%',
		rotation : parseInt(0),
		//fixedRotation : true,
		anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
	};
	
var car_rotate_adj=	parseInt(0) * -1;

$(function(){
	var urls=$('#URL').val();
	var insuranceAlertDateDiff=15;
	var shiftAlertDateDiff=15;	
	var insuranceAlertArr=[];
	var shiftAlertArr=[];
	var powerAlertArr=[];
	var tamperAlertArr=[];
	var alertSount = new Audio(urls+'/sounds/alertS1.mp3');					  
	map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	google.maps.event.addListener(map, 'zoom_changed', function() {
				 zoomLevel=map.getZoom();				 
			});
	startMarke=new google.maps.Marker({map:map});
	var timers;
	var is_lost = 7200;//60minutes * 24Hr (i.e One day) * 5day
	$('#move').html('0');
	$('#stope').html('0');
	$('#halt').html('0');
	$('#lost').html('0');
	
	timers=setInterval(dashboard_update,10000);
	dashboard_update();
	$("#ClientID").on('change', function(){		
		dashboard_update();
	});

	function dashboard_update()
	{
		if($("#ClientID").val())
		{				
			vehicle_status();
			insurence_alerts();
		}
	}
	
	function insurence_alerts()
	{
		var insuranceAlertCount=0;
		var shiftAlertCount=0;
		var powerAlertCount=0;
		var tamperAlertCount=0;
		var alertCount=0;
		$.ajax({
			type:'GET',
			dataType:'json',
			url: 'home_ctrl/insurance/'+$("#ClientID").val()+'/'+insuranceAlertDateDiff,
			success: function(result){
				$('#GridInsurance tbody').html('');
				if(result.length > 0)
				{					
					$.each(result, function(key, v){
						insuranceAlertCount+=1;
						$('#GridInsurance tbody').append('<tr><td><a target="_blank" href="vehicle_ctrl/editOrDelete_vts_vehicle/'+v['clid']+'/'+v['edit']+'/'+v['vhid']+'/'+v['mod_id']+'" title="Edit the Vehicle Details"><i class="fa fa-pencil-square-o"></i></a>&nbsp;'+v['vhno']+'</td><td>'+v['insag']+'</td><td>'+v['plcno']+'</td><td>'+v['exdate']+'</td></tr>');

						if(jQuery.inArray(v['id'], insuranceAlertArr) == -1)
						{
							insuranceAlertArr.push(v['id']);
							$("#bell").effect( "shake", {times:4, distance:5 ,direction:'down'}, 1000 );							
    						alertSount.play();
    					}
					});
					/*
					insuranceAlertArr=[];
					$.each(result, function(key, v){
						insuranceAlertArr.push(v['id']);
					});
					*/
				}else{					
					$('#GridInsurance tbody').append('<tr><td colspan="4" style="text-align:center;">-- No Data --</td></tr>');
					insuranceAlertArr=[];
				}
				$("#insuranceAlert").text(insuranceAlertCount);
				//$("#alerts").text(insuranceAlertCount);				
			}, /* Below part use for Driver shift alert*/
			complete: function(){											
				$.ajax({
					type:'GET',
					dataType:'json',
					url: 'home_ctrl/shift/'+$("#ClientID").val()+'/'+shiftAlertDateDiff,
					success: function(result){
						$('#GridShift tbody').html('');
						if(result.length > 0)
						{					
							$.each(result, function(key, v){
								shiftAlertCount+=1;
								var sht=(v['sht'])?v['sht']:"--";
								$('#GridShift tbody').append('<tr><td>'+v['dr']+'</td><td>'+v['vh']+'</td><td>'+sht+'</td><td>'+v['sdt']+'</td><td>'+v['edt']+'</td></tr>');

								if(jQuery.inArray(v['id'], shiftAlertArr) == -1)
								{
									shiftAlertArr.push(v['id']);
									$("#bell").effect( "shake", {times:4, distance:2 ,direction:'down'}, 1000 );							
		    						alertSount.play();
		    					}
							});
							/*
							shiftAlertArr=[];
							$.each(result, function(key, v){
								shiftAlertArr.push(v['id']);
							});
							*/
							
						}else{							
							$('#GridShift tbody').append('<tr><td colspan="5" style="text-align:center;">-- No Data --</td></tr>');
							shiftAlertArr=[];
						}
						$("#shiftAlert").text(shiftAlertCount);
						//$("#alerts").text(insuranceAlertCount+shiftAlertCount);				
					},  /* Below part use for Power & Tamper alert*/
					complete: function(){
						$.ajax({
							type:'GET',
							dataType:'json',
							url: 'home_ctrl/digitalIO/'+$("#ClientID").val(),
							success: function(result){
								$('#GridPower tbody').html('');
								$('#GridTamper tbody').html('');
								if(result.length > 0)
								{					
									$.each(result, function(key, v){
										if(v['pw']=='0')
										{
											powerAlertCount+=1;
											$('#GridPower tbody').append('<tr><td>'+v['vh']+'</td><td>'+v['imei']+'</td><td>'+v['dt']+'</td></tr>');
										}
										if(jQuery.inArray(v['id'], powerAlertArr) == -1)
										{
											powerAlertArr.push(v['id']);
											$("#bell").effect( "shake", {times:4, distance:2 ,direction:'down'}, 1000 );							
				    						alertSount.play();
				    					}

										if(v['tp']=='1')
										{
											tamperAlertCount+=1;
											$('#GridTamper tbody').append('<tr><td>'+v['vh']+'</td><td>'+v['imei']+'</td><td>'+v['dt']+'</td></tr>');
										}
				    					if(jQuery.inArray(v['id'], tamperAlertArr) == -1)
										{
											tamperAlertArr.push(v['id']);
											$("#bell").effect( "shake", {times:4, distance:2 ,direction:'down'}, 1000 );							
				    						alertSount.play();
				    					}
									});
									/*
									powerAlertArr=[];
									tamperAlertArr=[];
									$.each(result, function(key, v){
										if(v['pw']=='0')										
											powerAlertArr.push(v['id']);
										if(v['tp']=='1')										
											tamperAlertArr.push(v['id']);
									});
									*/
									
								}else{							
									$('#GridPower tbody').append('<tr><td colspan="3" style="text-align:center;">-- No Data --</td></tr>');
									$('#GridTamper tbody').append('<tr><td colspan="3" style="text-align:center;">-- No Data --</td></tr>');
									powerAlertArr=[];
									tamperAlertArr=[];
								}
								$("#mainpowerAlert").text(powerAlertCount);
								$("#TamperAlert").text(tamperAlertCount);
								$("#alerts").text(insuranceAlertCount+shiftAlertCount+tamperAlertCount+powerAlertCount);				
							}								
						});
					}
				});
			}
		});
	}

	function vehicle_status()
	{
		var move=0,halt=0,stop=0,lost=0;
			var clr='#333';
			$.ajax({
				type:'GET',
				url: 'home_ctrl/dashboard/'+$("#ClientID").val(),
				dataType: 'json',				
				success: function(result) {						
					$('#Grid tbody').html('');
					if(result.length>0)
					{
						$.each(result, function(k,v){
							if(parseInt(v["diff"]) >= v["lost"])
							{
								lost+=1;
								clr='#286090';
							}
							else if(v["ig"]=='N')
							{
								stop+=1;
								clr='#c9302c';
							}
							else if(v["ig"]=='Y' && (parseFloat(v['sp'])*1.852) > parseFloat(v['haltspeed']))
							{
								move+=1;
								clr='#5cb85c';
							}
							else if(v["ig"]=='Y' && (parseFloat(v['sp'])*1.852) <= parseFloat(v['haltspeed']))
							{
								halt+=1;
								clr='#f0ad4e';
							}
							$('#Grid tbody').append('<tr><td>'+v['vhgp']+'</td><td>'+v['vh']+'</td><td>'+v['rec']+'</td><td><i style="color:'+clr+'" title="'+v['vh']+'" class="fa fa-map-marker fa-2x cur" aria-hidden="true" onclick="show_marker({lat:'+parseFloat(v['lat'])+',lng:'+parseFloat(v['lng'])+', vhid:'+v['vhid']+', vh:\''+v["vh"]+'\', clr:\''+clr+'\', ang:'+parseFloat(v['ang'])+'})"></i></td></tr>');
							if(monitor_vehicle_id==v['vhid'])
								move_marker({lat:parseFloat(v['lat']),lng:parseFloat(v['lng'])}, clr, parseFloat(v['ang']));
						});
						$('#move').html(move);
						$('#stope').html(stop);
						$('#halt').html(halt);
						$('#lost').html(lost);

					}else{
						$('#Grid tbody').append('<tr><td colspan="4" style="text-align:center;">-- No Data --</td></tr>');
					}				
				}
			});
	}

	$('#go-to-top').click(function(){ $("body").animate({ scrollTop: 100}, 600);}); 
	

});
function show_marker(crt)
{	
	car_marker.rotation=crt.ang + car_rotate_adj;
	car_marker.fillColor=crt.clr;
	startMarke.setIcon(car_marker);
	startMarke.setPosition({lat:crt.lat, lng:crt.lng});
	startMarke.setTitle(crt.vh);
	monitor_vehicle_id=crt.vhid;
	startMarke.setAnimation(google.maps.Animation.DROP);
	//startMarke.setAnimation(google.maps.Animation.BOUNCE);
	if(zoomLevel<12)
		zoomLevel=12;
	map.setZoom(zoomLevel);
	map.panTo(crt);
	
}

function move_marker(loc, clr, ang)
{
	car_marker.rotation=ang + car_rotate_adj;
	car_marker.fillColor=clr;
	startMarke.setIcon(car_marker);
	startMarke.setPosition(loc);
	map.panTo(loc);
}
</script>

<style type="text/css">
	.cur{
		cursor: pointer
	}
	table {
		width:100%;
	}
	td{		
		padding-left: 5px;
		padding-right: 5px
	}
</style>
<input type="Text" style="display : none;" id="URL" value="<?php echo base_url('assets/')?>"/>
<div class="container">
	<!-- Insurance Modal -->
	<div class="modal fade" id="vehcileInsuranceModel" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Vehicle Insurance Alert Details</h4>
				</div>
				<div class="modal-body">
					<table id="GridInsurance" border="1">
						<thead>
							<tr>
								<th style="text-align: center;">Vehcile</th>
								<th style="text-align: center;">Insurance Agency</th>
								<th style="text-align: center;">Policy No.</th>
								<th style="text-align: center;">Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" style="text-align:center;">-- No Data --</td>
							</tr>
	    				</tbody>										
					</table>
				</div>				
			</div>
		</div>
	</div>
	<!-- Driver shift Modal -->
	<div class="modal fade" id="DriverShiftModel" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Driver Shift/Trip Alert Details</h4>
				</div>
				<div class="modal-body">
					<table id="GridShift" border="1" >
						<thead>
							<tr>
								<th style="text-align: center;">Driver</th>
								<th style="text-align: center;">Vehicle</th>
								<th style="text-align: center;">Shift/Trip</th>
								<th style="text-align: center;">Start Date</th>	
								<th style="text-align: center;">End Date</th>							
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" style="text-align:center;">-- No Data --</td>
							</tr>
	    				</tbody>										
					</table>
				</div>				
			</div>
		</div>
	</div>
		<!-- Main-Power Model -->
	<div class="modal fade" id="mainPowerModel" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Main-Power Removal Alert Details</h4>
				</div>
				<div class="modal-body">
					<table id="GridPower" border="1" >
						<thead>
							<tr>
								<th style="text-align: center;">Vehicle</th>
								<th style="text-align: center;">IMEI No.</th>
								<th style="text-align: center;">Date &amp; Time</th>							
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" style="text-align:center;">-- No Data --</td>
							</tr>
	    				</tbody>										
					</table>
				</div>				
			</div>
		</div>
	</div>
		<!-- Tamper Model -->
	<div class="modal fade" id="TamperModel" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tamper Alert Details</h4>
				</div>
				<div class="modal-body">
					<table id="GridTamper" border="1" >
						<thead>
							<tr>
								<th style="text-align: center;">Vehicle</th>
								<th style="text-align: center;">IMEI No.</th>
								<th style="text-align: center;">Date &amp; Time</th>					
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" style="text-align:center;">-- No Data --</td>
							</tr>
	    				</tbody>										
					</table>
				</div>				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 dashboard-main">				
			<nav class="navbar navbar-inverse" <?php echo(($sessClientID != AUTOGRADE_USER)?'style="display: none;"' :'' );?>>
				<div class="container-fluid">
					<ul class="nav navbar-nav">      
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Distributor &amp; Dealer
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/distributor_ctrl")?>">Distributor</a></li>
					<li><a href="<?php echo base_url("index.php/dealer_ctrl")?>">Dealer</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Device
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/device_type_ctrl")?>">Device Type</a></li>
					<li><a href="<?php echo base_url("index.php/device_ctrl")?>">Device</a></li>
					<li><a href="<?php echo base_url("index.php/device_allocation_dealer_ctrl")?>">Device Allocation to Dealer</a></li>
					<li><a href="<?php echo base_url("index.php/device_allocation_ctrl")?>">Device Allocation to Client</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Client
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/client_ctrl")?>">Client</a></li>
					<li><a href="<?php echo base_url("index.php/client_license_ctrl")?>">Client License</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Tester
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<!-- <li><a target="_blank"  href="http://vtstest.hkaagencies.com:1234/vtstest_app/index.php/fota_ctrl">FOTA Monitoring</a></li> -->
					<li><a href="<?php echo base_url("index.php/device_monitoring_ctrl")?>">Device Monitoring</a></li>
					<li><a href="<?php echo base_url("index.php/vehicle_admin_tracking_ctrl")?>">Tester Tracking</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/service_ctrl")?>">Service</a></li>
					<li><a href="<?php echo base_url("index.php/settings_ctrl")?>">Settings</a></li>
					<li><a href="<?php echo base_url("index.php/vehicle_make_model_ctrl")?>"> Vehicle Make &amp; Model</a></li>
					</ul>
					</li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Others
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("index.php/insurance_agency_ctrl")?>">Insurance Agency</a></li>
					<li><a href="<?php echo base_url("index.php/vehicle_service_ctrl")?>">Vehicle Service</a></li>
					<li><a href="<?php echo base_url("index.php/individual_user_ctrl")?>">Individual User</a></li>
					<li><a href="<?php echo base_url("index.php/individual_vt_ctrl")?>">Personal Tracking</a></li>
					<li><a href="<?php echo base_url("index.php/distributor_dashboard_ctrl")?>">Distributor Dashboard</a></li>
					<li><a href="<?php echo base_url("index.php/dealer_dashboard_ctrl")?>">Dealer Dashboard</a></li>
					</ul>
					</li>    
					<li>
						<form id="myform" action="<?php echo(base_url("index.php/home_ctrl/view"))?>" method="post">
							<select id="ClientID" name="ClientID" style=" padding: 15px;">           
							<?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER){echo'<option value="">-- Select Client --</option>';} foreach ($clientList as $row):?>
							<?php if($sessClientID==AUTOGRADE_USER):?>								
							<option value="<?php echo $row['client_id']?>"	<?php echo(($clientID==$row['client_id'])?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php elseif($sessClientID==$row['client_id']): ?>
							<option value="<?php echo $row['client_id']?>"><?php echo $row['client_name']?> </option>
							<?php endif;?>
							<?php endforeach; endif;?>
							</select>
						</form>
					</li>  
					</ul>
				</div>
			</nav>	
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#5cb85c; color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">							
							<div>Moving</div>
							<div id="move" class="huge" style="font-size:25px;">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#c9302c; color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">							
							<div>Stopped</div>
							<div id="stope" class="huge" style="font-size:25px;">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#f0ad4e; color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>Halt</div>
							<div id="halt" class="huge" style="font-size: 25px;">0</div>						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel" style="background-color:#286090;color:#fff;">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-truck fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>Lost Connection</div>
							<div id="lost" class="huge" style="font-size:25px;">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<!-- For Alert detail-->
		<div class="col-md-6 col-xs-12" style=" overflow: auto; padding: 15px; border-radius:10px; height:400px; background-color:#ECECEC">
			<div class="row" style="margin: 0px; background-color: #3b318f;">				
				<div class="col-md-2 col-md-offset-10 col-xs-3 col-xs-offset-9">					
					<ul class="nav navbar-top-links navbar-right nav-to-right">
						<li class="dropdown">
				            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="javascript:void(0);">
				                <i  id="bell"  class="fa fa-bell"></i> <span id="alerts" class="label label-primary">0</span>
				            </a>
				            <ul class="dropdown-menu dropdown-alerts">					            
				                <li>
				                    <a class="cur" data-toggle="modal" data-target="#vehcileInsuranceModel">
				                        <div>
				                            <i Title="Vehicle Insurance Expire Alert" class="fa fa-truck fa-fw"></i> You have <span id="insuranceAlert">0</span> vehicle Insuarance Expire Alerts
				                        </div>
				                    </a>
				                    <a class="cur" data-toggle="modal" data-target="#DriverShiftModel">
				                        <div>
				                            <i Title="Shift/Trip Expire Alert" class="fa fa-clock-o fa-fw"></i> You have <span id="shiftAlert">0</span> Driver Shift Expire Alerts
				                        </div>
				                    </a>
				                    <a class="cur" data-toggle="modal" data-target="#mainPowerModel">
				                        <div>
				                            <i title="Main Power Removal Alert" class="fa fa-plug fa-fw"></i> You have <span id="mainpowerAlert">0</span> Main Power Removal Alerts
				                        </div>
				                    </a>
				                    <a class="cur" data-toggle="modal" data-target="#TamperModel">
				                        <div>
				                            <i title="Tamper Alert" class="fa fa-exclamation-triangle fa-fw"></i> You have <span id="TamperAlert">0</span> Device Tamper Alerts
				                        </div>
				                    </a>
				                </li>					                				               
				            </ul>
				        </li>
			        </ul>
		        </div>			        
	        </div>	
	        <!-- For Vehicle detail-->
	        <div class="row">
	        	<div class="col-md-12 col-xs-12">
					<div class="table-responsive" > 
						<table id="Grid" border="1" >
							<thead>
								<tr>
									<th style="text-align: center;">Vehicle Group</th>
									<th style="text-align: center;">Vehicle</th>
									<th style="text-align: center;">Date &amp; Time</th>
									<th style="text-align: center;">Map</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4" style="text-align:center;">-- No Data --</td>
								</tr>
		    				</tbody>										
						</table>	 
					</div>
				</div>
			</div>
		</div>
		<!-- For Map View-->
		<div class="col-md-6 col-xs-12 dashboard-main" style="padding: 5px; border-radius:10px; height:400px; background-color: lightblue;">
			<div id="googleMap" style=" border-style: inset; border-radius:10px; height: 100%">
			</div>
			<button id="go-to-top"  style=" border-style: solid; border-width: 2px; border-color:#758C30; background-color:#90C551; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1.428571429; border-radius: 15px; position: absolute;right: 0px; top:200px;">^</button>			
		</div>
	</div>

</div>
<script>
$(function(){
    $("#CompanyName").autocomplete({
    	source:function(request, response) 
    	{  
        	$('#CompanyNameID').val("");
    	    $.getJSON("<?php echo base_url('index.php/vehicle_make_model_ctrl/get_autoCompletionModelName')?>", { parent : $('#CompanyName').val(), isparent:'parent' }, response);
    	 },
         select:function (event, ui) 
         {
              $('#CompanyNameID').val(ui.item.id);              
         }
    });

    $("#ModelName").autocomplete({
    	source:function(request, response) 
    	{  
        	$('#ModelNameID').val("");
    	    $.getJSON("<?php echo base_url('index.php/vehicle_make_model_ctrl/get_autoCompletionModelName')?>", { parent : $('#ModelName').val(), isparent:'child' }, response);
    	 },
         select:function (event, ui) 
         {
              $('#ModelNameID').val(ui.item.id);              
         }
    });
 });
</script>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/vehicle_make_model_ctrl/validate_model_data/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/vehicle_make_model_ctrl/")?>"/>					
					
					<h1>Vehicle Make &amp; Model Details</h1>				
					<?php if(isset($modeloutcome)) echo $modeloutcome;?>
					<div class="user-fields">
						<div class="field" > 
							<label>Company Name:<span style="color:red;"> *</span></label>
							<input type="text" id="CompanyName" name="CompanyName" maxlength="100" value="<?php echo $companyName ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('CompanyName'))echo form_error('CompanyName',' ',' '); ?>" <?php echo(($modleID==null)?" ":"readonly")?>/>
							<input type="text" style="display:none" id="CompanyNameID" name="CompanyNameID" value="<?php echo $companyNameID ?>"  class="form-control input-lg" />
					 	</div>					 
					 	<div class="field" > 
					 		<label>Model Name:<span style="color:red;"> *</span></label>
					 		<input type="text" id="ModelName" name="ModelName" maxlength="100" value="<?php echo $modleName ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ModelName'))echo form_error('ModelName',' ',' '); ?>" <?php echo(($modleID==null)?" ":"readonly")?>/>
					 		<input type="text" style="display:none" id="ModelNameID" name="ModelNameID" value="<?php echo $modleID ?>"  class="form-control input-lg" />
					    </div>						
						<div class="field">
							<label>Sensor Values:</label>
							<textarea type="text" id="SensorValues" name="SensorValues" class="form-control input-lg" placeholder="Eg: ltr-volt; ltr-volt; ltr-volt;....ltr-volt;"><?php echo $sensorValues?></textarea>
						</div><br/>						
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($modleID==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/vehicle_make_model_ctrl/")?>
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						<tr>
							<th>Company</th>
							<th>Model</th>
							<th>Total value</th>
							<th>Actions</th>
						</tr>
						<?php if($makeModelList!=null): foreach ($makeModelList as $row): ?>
						<tr>
							<td><?php echo trim($row['parent_name'])?></td>
							<td><?php echo trim($row['fsv_model'])?></td>
							<td><?php echo trim($row['sensor_value_count'])?></td>
							<td>
							<?php 
							if(empty($row['parent_id'])) $md_parentid ='0'; else $md_parentid = md5(trim($row['parent_id']));
							if(empty($row['fsv_id'])) $md_childid ='0'; else $md_childid = md5(trim($row['fsv_id']));							
							//echo ("$md_parentid---$md_childid");						
							?>
								<a href="<?php echo(base_url("index.php/vehicle_make_model_ctrl/edit_vehicle_make_model/".$md_parentid."/".$md_childid))?>" title="Edit the Vehicle Make &amp; Model Details."><i class="fa fa-pencil-square-o"></i></a> &nbsp;								
							</td>
						</tr>
						<?php endforeach; endif;?>
						</table>
				</div>
			
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
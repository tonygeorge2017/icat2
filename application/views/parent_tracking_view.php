<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/jquery-ui.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/simple-sidebar.css')?>" rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY; ?>"></script>
<!-- <script src="<?php //echo base_url('assets/js/markerclusterer.js')?>"></script> -->
<!-- jQuery -->
<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.js')?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<style type="text/css">
.nav {
  line-height: 14px;/*30px;*/
  background-color: #eeeeee;
  float: left;
  padding: 3px;
}
.divx {
  /*position: absolute;
  width: 100%;*/
  height: 637px;/*574px;508px;*/
  /*float: left;
  margin-right: 5px;*/
}
select{
  width: 100%;
}
@CHARSET "ISO-8859-1";
</style>
</head>

<body id="bodyTag" >
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
      <?php
      $home_link="";
      if($this->session->userdata('login')!=null)
      {
        $sess_details=$this->session->userdata('login');
        $user_type=$sess_details['sess_user_type'];
        $is_admin=$sess_details['sess_is_admin'];
      if($user_type == AUTOGRADE_USER)
        $home_link="home_ctrl";
      else if($user_type == OHTER_CLIENT_USER)
      {
        if($is_admin == '1')
          $home_link="home_ctrl";
        else
          $home_link="vehicle_tracking_ctrl";
      }
      else if($user_type == INDIVIDUAL_CLIENT)
        $home_link="individual_vt_ctrl";
      else if($user_type==DISTRIBUTOR_USER)
        $home_link="distributor_dashboard_ctrl";
      else if($user_type==DEALER_USER)
        $home_link="dealer_dashboard_ctrl";
      else if($user_type==PARENT_USER)
        $home_link="parent_dashboard_ctrl";
      }else{$home_link="login_ctrl";}
      ?>
        <h3><a href="<?php echo base_url('index.php/'.$home_link)?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
         <h5><?php if ($this->session->userdata ( 'login' )){ echo $ID['sess_client_name']; }?></h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
      <?php if ($this->session->userdata ( 'login' )): ?>
       <h6><?php echo $ID['sess_user_name']; ?></h6>
       <h6>
    <?php if ($ID['sess_user_name']=='1'): ?>
      <a href= "<?php echo base_url('index.php/change_password_ctrl')?>">Change Password</a> | 
      <?php endif; ?>
     <a href= "<?php echo base_url('index.php/home_ctrl/logout')?>">Logout</a></h6>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>         
 <!-- --------------------------------------------------------------- -->
            
<?php if ($this->session->userdata ( 'login' )):?>
<?php echo $GLOBALS ['main_menu']; ?>
<?php endif; ?>

          </div>
        </div>
        <div>
          <h5>Autograde T.A.N.K. Service</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank">by Autograde</a> </em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right">
      <em> <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?>
        <?php // echo date('d - F Y')?></em></div>
    </div>
  </div>
</div>
<script>
var map;
var student_details=[];
var stopMarkerList=[];
var infowindowStopDetails=[], infowindowResultDetails = [];
var animInfowindow = null;
var zoomLevel=0;
var liveUpdate = null;
var animationUpdate = null;
var startMarker = null, endMarker = null;
var route = null;
var arrayWayPoint = [];
var resultHistory = [];
var queryResult = [];
var latLngPoint = [];
var latlngbounds = null;
var setBoundCount=0;
var i = 0;
var idle_speed = 0;
var distance = 0;
var is_AnimationComplete=true;
var arrow = "M 50,5 95,97.5 5,97.5 z";

var slow_icon = {
		path: arrow,
		scale: 0.09,
		strokeColor: '#05913b',
		strokeWeight: .9,
		fillOpacity: .9,
		fillColor: '#000000',
		offset: '5%',
		rotation : parseInt(0),
		anchor: new google.maps.Point(50,50)
	};
var speed_icon = {
		path: arrow,		
		scale: 0.09,
		strokeColor: '#000000',
		strokeWeight: .9,	
		fillOpacity: .9,
		fillColor: '#ff0000',
		offset: '5%',
		rotation : parseInt(0),	
		anchor: new google.maps.Point(50,50)
	};
	
var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v46.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";

var icon_ON = {
  path: car,
  scale: 0.6,
  strokeColor: '#807776',
  strokeWeight: 0.9,
  fillOpacity: 1,
  fillColor: '#3D9B55',
  offset: '5%',
  rotation : parseInt(0),
  //fixedRotation : true,
  anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};  
var icon_OFF = {
  path: car,
  scale: 0.6,
  strokeColor: '#807776',
  strokeWeight: 0.9,
  fillOpacity: 1,
  fillColor: '#E72B2D',
  offset: '5%',
  rotation : parseInt(0),
  //fixedRotation : true,
  anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};
var icon_Speed = {
  path: car,
  scale: 0.6,
  strokeColor: '#FF0000',
  strokeWeight: 0.6,
  fillOpacity: 1,
  fillColor: '#FD8D05',
  offset: '5%',
  rotation : parseInt(0),
  //fixedRotation : true,
  anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};
var stop_icon = {
          url: "<?php echo base_url('assets/images')?>/label2.png",
          // This marker is 20 pixels wide by 32 pixels high.
          size: new google.maps.Size(60, 45),
          // The origin for this image is (0, 0).
          origin: new google.maps.Point(0, -5),
          // The anchor for this image is the base of the flagpole at (0, 32).
          anchor: new google.maps.Point(0, 40)
        };
$(document).ready(function(){
  $('.dropdown-submenu a.not_close_onclick_menu').on("click", function(e){   
    e.stopPropagation();
    e.preventDefault();
  });
  
   var mapProp = {
      center:new google.maps.LatLng(0,0),
    zoom:2,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
   map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
   latlngbounds = new google.maps.LatLngBounds();
   startMarker = new google.maps.Marker();
   endMarker = new google.maps.Marker();

   google.maps.event.addListener(startMarker, 'click', function() {
    document.getElementById('info').value=startMarker.getTitle(); 
  }); 
	
	animInfowindow = new google.maps.InfoWindow();
   google.maps.event.addListener(endMarker, 'click', function() {
    document.getElementById('info').value=endMarker.getTitle();
    animInfowindow.open(map, endMarker);     
  });

   route = new google.maps.Polyline({
      strokeColor:"#0000FF",
      strokeOpacity:0.8,
      strokeWeight:3,
      editable:false
      });

   $("#stopTimer").attr("disabled", "disabled");   
   $("#selectDate").datepicker({dateFormat:'yy-mm-dd'});
   $("#selectDate").datepicker().on('change', function(){update_vehicle_list()});
 
   $('input[type=radio]').on('change', function(){clear_all()}); 

  function clear_all()
  {
	console.log('**Clearing ALL**');
	if(liveUpdate)
      clearInterval(liveUpdate);
    if(animationUpdate)
      clearInterval(animationUpdate);
	
	while(resultHistory.length) { resultHistory.pop().setMap(null); }	
	resultHistory = [];	
	while(arrayWayPoint.length) { arrayWayPoint.pop().setMap(null); }	
	arrayWayPoint = [];	
	
	/*
	while(infowindowStopDetails.length) { infowindowStopDetails.pop().setMap(null); }
    infowindowStopDetails = [];
	*/
	while(infowindowResultDetails.length) { infowindowResultDetails.pop().setMap(null); }
    infowindowResultDetails = [];	
	
    queryResult = [];	
	
    latLngPoint = [];

    setBoundCount = 0;
    i = 0;
    distance = 0;
    latlngbounds = new google.maps.LatLngBounds();    
    $("#startTimer").prop( "disabled", false );
    $("#stopTimer").prop( "disabled", true ); 
    startMarker.setMap(null);
	endMarker.setMap(null);
	route.setMap(null);
    $("#info").val(''); 
	is_AnimationComplete=true;
  }

   $.getJSON('parent_tracking_ctrl/get_student', function(data){
     /*
     [{"st_id":"1","st_pid":"1","st_name":"Ramesh G","st_class":"X - B","st_rfid":"2","stp_id":"341","stp_name":"Mudaliarpet police station","stp_lat":"11.921439072343","stp_lng":"79.815555810928"}]
     */
     student_details = [];
     stopMarkerList = [];
     $("#studentID").append('<option value="All">-- Select All--</option>');
     $.each(data, function(k,v){
       student_details.push(v);      
       $("#studentID").append('<option value="'+v['st_id']+'">'+v['st_name']+'</option>');
       
      if(stopMarkerList.length!=data.length)
      {             
        stopMarkerList[k]=new google.maps.Marker({ 
			position: {lat: parseFloat(v['stp_lat']), 
			lng: parseFloat(v['stp_lng'])}, 
			icon: stop_icon, 
			label: {
				text: v['st_name'],
				color: 'blue',
				fontSize: "10px" 
				}, 
			title: 'Stop Name : '+v['stp_name'], 
			map: map
			});

        infowindowStopDetails[k] = new google.maps.InfoWindow({ content:'<div id="content"><h3 id="firstHeading" class="firstHeading">Stop Detail of '+v['st_name']+'</h3><div id="bodyContent"><p><b>Stop Name : </b>'+v['stp_name']+'</p></div></div>'});
        
        stopMarkerList[k].addListener('click', function() {
          infowindowStopDetails[k].open(map, stopMarkerList[k]);
          zoom_check(15);
          map.panTo(stopMarkerList[k].getPosition());
          $("#info").val(stopMarkerList[k].getTitle());
        });
		latlngbounds.extend(stopMarkerList[k].getPosition());
      }
      else
      {         
        stopMarkerList[k].setPosition({lat: parseFloat(v['stp_lat']), lng: parseFloat(v['stp_lng'])});
        stopMarkerList[k].setTitle('Stop Name : '+v['stp_name']);
        infowindowStopDetails[k].setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">Stop Detail of '+v['st_name']+'</h3><div id="bodyContent"><p><b>Stop Name : </b>'+v['stp_name']+'</p></div></div>');
      }     

     });
	 setbound_check();
     console.log(student_details);
   });
   
   $("#studentID").on('change', function(){
	  clear_all(); 
	  update_student_stop();
	  if($("#Replay").is(':checked')){
		 update_vehicle_list(); 
	  }
   });
   
   function update_student_stop()
   {
	   $.each(student_details, function(k,v){      
       if(v['st_id'] == $("#studentID").val() || $("#studentID").val() == 'All')
       {
			stopMarkerList[k].setPosition({lat: parseFloat(v['stp_lat']), lng: parseFloat(v['stp_lng'])});
			stopMarkerList[k].setTitle('Stop Name : '+v['stp_name']);
			infowindowStopDetails[k].setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">Stop Detail of '+v['st_name']+'</h3><div id="bodyContent"><p><b>Stop Name : </b>'+v['stp_name']+'</p></div></div>');
			stopMarkerList[k].setMap(map);
       }else{        
         stopMarkerList[k].setMap(null);
       }
     });
   }
   
   $("#Live").on('click', function(){
     $("#studentID").html('<option value="All">-- Select All--</option>');
    $.each(student_details, function(k,v){
      $("#studentID").append('<option value="'+v['st_id']+'">'+v['st_name']+'</option>');
    });
     $("#PlottingType").hide();
	 update_student_stop();
   });
   
   $("#Replay").on('click', function(){
     $("#studentID").html('');
    $.each(student_details, function(k,v){
      $("#studentID").append('<option value="'+v['st_id']+'">'+v['st_name']+'</option>');
    });
    update_vehicle_list();
	update_student_stop();
     $("#PlottingType").show();
   });
   
   $("#startTimer").on("click", function(){
     if($("#Live").is(':checked')){
       console.log("Live");
       $("#startTimer").prop( "disabled", true );
       $("#stopTimer").prop( "disabled", false );
	   $("#info").val("Please wait..");
       get_live();
       liveUpdate = setInterval(function(){get_live()},10000);
     }
     else
     {
       console.log("History");           
       $("#startTimer").prop( "disabled", true );
       $("#stopTimer").prop( "disabled", false );
	   if(is_AnimationComplete)
	   {
		get_hist();    
	   }
		else{
			animationUpdate = setInterval(function(){animation_function()},100);
		}
     }
   });
   
   $("#stopTimer").on("click", function(){
	   $("#startTimer").prop( "disabled", false );
       $("#stopTimer").prop( "disabled", true );
     if(liveUpdate)
      clearInterval(liveUpdate);
     if(animationUpdate)
      clearInterval(animationUpdate);     
   })
  
   
    function update_vehicle_list(){
	  $("#vehicleID").html('');
      $.getJSON('parent_tracking_ctrl/get_swiped_vehicles',{ dt: $("#selectDate").val(), id: $("#studentID").val() },function(data){
        if(data.length > 0)
        {
			var info = "On "+$("#selectDate").val()+", \n";
          $("#info").val();
          $.each(data, function(k,v){
			info += " "+ (k+1) +") "+v['vh']+"  "+v['swap_cnt']+" - time(s) RF ID has been entered\n";
			info +=v['swap_time']+"\n";
            $("#vehicleID").append('<option value="'+v['vhid']+'">'+ v['vh'] +'</option>');
          });
		  $("#info").val(info);
        }
        else{
          $("#info").val("No card swapping found");
        }
      })
    }  

   function get_live(){	   
     $.getJSON('parent_tracking_ctrl/fetch_track_data', {id: $("#studentID").val()}, function(data){
        $.each(data, function(k,v){		
         if(resultHistory.length!=data.length)
         {
			 $("#info").val('');
			title = v['tooltip_msg'].replace(/<br>/g, "\n");			
           resultHistory[k]=new google.maps.Marker({ position: {lat: parseFloat(v['lat']), lng: parseFloat(v['lng'])}, title: v['tooltip_msg'].replace(/<br>/g, "\n") , map: map});
          infowindowResultDetails[k] = new google.maps.InfoWindow({ content: v['notify_msg']});
          
          resultHistory[k].addListener('click', function() {
            infowindowResultDetails[k].open(map, resultHistory[k]);
            zoom_check(15);
            map.panTo(resultHistory[k].getPosition());
            $("#info").val(resultHistory[k].getTitle());
          });
		  latlngbounds.extend(resultHistory[k].getPosition());
		  setBoundCount = 0;
         }
         else{
           resultHistory[k].setPosition({lat: parseFloat(v['lat']), lng: parseFloat(v['lng'])});
		    title = v['tooltip_msg'].replace(/<br>/g, "\n");
           resultHistory[k].setTitle( v['tooltip_msg'].replace(/<br>/g, "\n") );
		   infowindowResultDetails[k].setContent( v['notify_msg'] );
           //infowindowResultDetails[k].setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">Tracking Detail of '+v['stud_name']+'</h3><div id="bodyContent"><p><b>Vehicle : </b>'+v['vh']+'<br><b>Driver : </b>'+v['dr']+'<br><b>Mobile : </b>'+v['dr_mob']+'<br><b>Speed(km) : </b>'+v['sp']+'<br><b>Speed Limit(km) : </b>'+v['splmt']+'<br><b>RF ID : </b>'+v['rfid']+'<br><b>Swipe at : </b>'+v['rfid_tm']+'<br><b>Tracking : </b>'+v['tm']+'</p></div></div>');
           infowindowResultDetails[k].setPosition({lat: parseFloat(v['lat']), lng: parseFloat(v['lng'])});
         }
		 //console.log(resultHistory[k].getTitle());
		 if($("#studentID").val() != 'All')
			 $("#info").val(resultHistory[k].getTitle());
		 
          if(v['ig'] == 'N')
            resultHistory[k].setIcon(icon_OFF);
          else if((parseFloat(v['speed'])) <= (parseFloat(v['splmt'])) && v['ig'] == 'N')
            resultHistory[k].setIcon(icon_ON);
          else
            resultHistory[k].setIcon(icon_Speed);
      });
	  setbound_check();
     });
   }
   
   function get_hist(){
	   //$("#vehicleID").val()
    if($("#vehicleID").val())
    {
      latLngPoint = [];
      $.getJSON('parent_tracking_ctrl/fetch_hist_track',{id: $("#vehicleID").val(), dt : $("#selectDate").val()}, function(data){
		while(arrayWayPoint.length) { arrayWayPoint.pop().setMap(null); }	
		arrayWayPoint = [];
        if(data.length > 0)
        {
          var cnt = 0; 
          /*
        {"ang":"306.72","ign":"Y","driver":"Driver003","vehicle":"KA42-50","splt":"80",
        "lat":"11.914389953927","lng":"79.805637001991","speed":"29","dtime":"2017-11-13 10:00:16"}
        */        
          queryResult = data;           
          if($("#StepByStepPlotting").is(':checked'))
          {
            i = 0; 
			is_AnimationComplete=false;
            latLngPoint[i]=new google.maps.LatLng(data[i].lat, data[i].lng); 
            startMarker.setPosition(latLngPoint[i]);
            startMarker.setTitle('*Start*\nVehicle: '+data[i].vehicle+'\nDate & Time: '+data[i].dtime+'\nSpeed: '+data[i].speed+'\nSpeed limit: '+data[i].splt);
            endMarker.setPosition(latLngPoint[i]);
            endMarker.setTitle('*End*\nVehicle: '+data[i].vehicle+'\nDate & Time: '+data[i].dtime+'\nSpeed: '+data[i].speed+'\nSpeed Limit: '+data[i].splt);
			animInfowindow.setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">'+data[i].vehicle+'</h3><div id="bodyContent"><p><b>Date & Time :</b> '+data[i].dtime+'<br><b>Speed : </b>'+data[i].speed+'<br><b>Speed Limit :</b> '+data[i].splt+'</p></div></div>');
			animInfowindow.setPosition(latLngPoint[i]);
            startMarker.setMap(map);
            endMarker.setMap(map);
            route.setPath(latLngPoint);
            route.setMap(map);
			$.each(data, function(k,v){              	
			  latlngbounds.extend(new google.maps.LatLng(v['lat'],v['lng'])); 		  
            });			
			setbound_check();
            i++;   
            animationUpdate = setInterval(function(){animation_function()},100);
          }
          else
          {
			distance = 0;
            $.each(data, function(k,v){
              latLngPoint[k]=new google.maps.LatLng(v['lat'],v['lng']); 
              cnt = k; 		
			  latlngbounds.extend(latLngPoint[k]); 		  
            });
			setbound_check();
            startMarker.setPosition(latLngPoint[0]);
            startMarker.setTitle('*Start*\nVehicle: '+data[0].vehicle+'\nDate & Time: '+data[0].dtime+'\nSpeed: '+data[0].speed+'\nSpeed limit: '+data[0].splt);
			
			distance=(google.maps.geometry.spherical.computeLength(latLngPoint)).toFixed(3);		  
			if(distance>=1000)
			{			  
				distance=(distance/1000).toFixed(3);
				distance+="km";
			}
			else
				distance+="m";			  

            endMarker.setPosition(latLngPoint[cnt]);
            endMarker.setTitle('*End*\nVehicle: '+data[cnt].vehicle+'\nDate & Time: '+data[cnt].dtime+'\nSpeed: '+data[cnt].speed+'\nSpeed Limit: '+data[cnt].splt+'\nDistance: '+distance); 
            animInfowindow.setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">'+data[cnt].vehicle+'</h3><div id="bodyContent"><p><b>Date & Time :</b> '+data[cnt].dtime+'<br><b>Speed : </b>'+data[cnt].speed+'<br><b>Speed Limit :</b> '+data[cnt].splt+'<br><b>Distance : </b>'+distance+'</p></div></div>');
			animInfowindow.setPosition(latLngPoint[cnt]);

            startMarker.setMap(map);
            endMarker.setMap(map);
            route.setPath(latLngPoint);
            route.setMap(map);
			$("#startTimer").prop( "disabled", false );
			$("#stopTimer").prop( "disabled", true );
          }
        }
        else{
		  $("#startTimer").prop( "disabled", false );
		  $("#stopTimer").prop( "disabled", true );
          alert("Sorry..! No data to show.");   
        }
      });
    }
    else{
		$("#startTimer").prop( "disabled", false );
		$("#stopTimer").prop( "disabled", true );
		alert("Sorry..! No vehicle details to show");
    }
   }
   
   function animation_function()
   {     
      //console.log(i +","+ queryResult[i]) ; 
     if((i + 1)  >= queryResult.length)
     {
      console.log('clear animationUpdate');
	  is_AnimationComplete=true;
      clearInterval(animationUpdate);
      $("#info").val('Completed...\n' + endMarker.getTitle()); 
	  $("#startTimer").prop( "disabled", false );
      $("#stopTimer").prop( "disabled", true );	  
     }
     else
     {
      /*
      {"ang":"306.72","ign":"Y","driver":"Driver003","vehicle":"KA42-50","splt":"80",
      "lat":"11.914389953927","lng":"79.805637001991","speed":"29","dtime":"2017-11-13 10:00:16"}
      *///arrayWayPoint 
		  
        endMarker.setIcon( get_icon_type( queryResult[i].ign, queryResult[i].speed, queryResult[i].splt, queryResult[i].ang, 'Main') );
        if(queryResult[i].ign == 'Y' && queryResult[i].speed > idle_speed)
        {
          latLngPoint[latLngPoint.length] = new google.maps.LatLng( parseFloat (queryResult[i].lat), parseFloat(queryResult[i].lng));
          endMarker.setPosition(latLngPoint[latLngPoint.length - 1]);
		  
          distance=(google.maps.geometry.spherical.computeLength(latLngPoint)).toFixed(3);		  
          if(distance>=1000)
          {			  
            distance=(distance/1000).toFixed(3);
            distance+="km";
          }
          else
            distance+="m";			
			
          endMarker.setTitle('Vehicle: '+queryResult[i].vehicle+'\nDate & Time: '+queryResult[i].dtime+'\nSpeed: '+queryResult[i].speed+'\nSpeed Limit: '+queryResult[i].splt+'\nDistance: '+ distance);		

			arrayWayPoint[arrayWayPoint.length]=new google.maps.Marker({ 
			position: {lat: parseFloat(queryResult[i].lat), lng: parseFloat(queryResult[i].lng)}, 
			title: endMarker.getTitle(), 
			map: map,
			icon: get_icon_type( queryResult[i].ign, queryResult[i].speed, queryResult[i].splt, queryResult[i].ang )
			});
			arrayWayPoint[arrayWayPoint.length - 1].addListener('click', function() {
				$("#info").val(this.getTitle());
			});
		  
          map.panTo(endMarker.getPosition());		  
		  animInfowindow.setPosition(endMarker.getPosition());
          route.setPath(latLngPoint);		  
          route.setMap(map);		  
        }
        else
        {
          endMarker.setTitle('*End*\nVehicle: '+queryResult[i].vehicle+'\nDate & Time: '+queryResult[i].dtime+'\nSpeed: '+queryResult[i].speed+'\nSpeed Limit: '+queryResult[i].splt+'\nDistance: '+distance);
        }
		animInfowindow.setContent('<div id="content"><h3 id="firstHeading" class="firstHeading">'+queryResult[i].vehicle+'</h3><div id="bodyContent"><p><b>Date & Time :</b> '+queryResult[i].dtime+'<br><b>Speed : </b>'+queryResult[i].speed+'<br><b>Speed Limit :</b> '+queryResult[i].splt+'<br><b>Distance : </b>'+distance+'</p></div></div>');
        $("#info").val(i+' / '+queryResult.length+' Playing...\n' + endMarker.getTitle());
     }
     i++;
   }

   function get_icon_type(ignition, speed, overSpeed, angle = 0, markerType = 'Way')
   {
	   var result_icon = null;
      icon_ON.rotation= parseFloat(angle);
      icon_OFF.rotation= parseFloat(angle);
      icon_Speed.rotation= parseFloat(angle);
	  speed_icon.rotation= parseFloat(angle);
	  slow_icon.rotation= parseFloat(angle);

      if( ignition == 'N' )    
        result_icon = (markerType == 'Way')? slow_icon : icon_OFF;
      else if( ignition == 'Y' && parseFloat(speed) <= parseFloat(overSpeed))
        result_icon = (markerType == 'Way')? slow_icon : icon_ON;
      else if( ignition == 'Y' && parseFloat(speed) > parseFloat(overSpeed))
        result_icon = (markerType == 'Way')? speed_icon : icon_Speed;
      else
        result_icon = (markerType == 'Way')? slow_icon : icon_ON;
	
		return result_icon;
   }

  function zoom_check(zoom_level=12)
  {
    if(zoomLevel < zoom_level)
      zoomLevel=zoom_level;

    map.setZoom(zoomLevel);
  }
  
  function setbound_check()
  {
	if(setBoundCount == 0)
	{
		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);
		setBoundCount = 1;		
	}
  }

});
</script>
<div id="wrapper"> 
  <!-- style="display : none;" -->  
  <input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/parent_tracking_ctrl/")?>"/>  
  <input type="Text" style="display : none;" id="wayPntURL" name="ReportURL" value="<?php echo base_url('assets/images/')?>"/>
  <input type="Text" style="display : none;" id="SessClient" name="SessClient" value="<?php echo $sessClientID ?>"/>
  <input type="Text" style="display : none;" id="SessParent" name="SessParent" value="<?php echo $sessParentID ?>"/>
  <input type="Text" style="display : none;" id="SessUser" name="SessUser" value="<?php echo $sessUserID ?>"/>
  
  <div id="sidebar-wrapper">
    <div class="nav">
	<div class="field">
    <label>Student :</label>
    <select id="studentID" class="form-control input-sm">   
    </select>
    </div>
      <div class="field">
        <input id="Replay" name="TraceType" type="radio" class="field login-checkbox" value="1" tabindex="1" />
        <label>Trace History</label>
        &nbsp;
        <input id="Live" name="TraceType" type="radio" class="field login-checkbox" value="1" tabindex="2" checked />
        <label>Live Track</label>
      </div>
      <div class="field" id="PlottingType" style="display:none;">
        <input id="StepByStepPlotting" name="Plotting" type="radio" class="field login-checkbox" value="1" tabindex="1" />
        <label>Step-by-step plotting</label>
        <br>
        <input id="BulkPlotting" name="Plotting" type="radio" class="field login-checkbox" value="1" tabindex="2" checked />
        <label>Bulk plotting</label>
    <br><br>
    <label>Select Date :</label>
    <input type="text" id="selectDate" class="form-control input-sm" value="<?php echo date('Y-m-d')?>"/>
    <br>
    <label>Vehicle :</label>
    <select id="vehicleID" class="form-control input-sm">   
    </select>
      </div>
      
      <div class="field">   
        <button id="startTimer" class="btn btn-primary">Go</button>
        &nbsp;&nbsp;
        <button id="stopTimer" class="btn btn-primary">Stop</button>
      </div>
      <div class="field">
        <label>Info : </label>
        <textarea style="font-size: 15px;" id="info" name="info" cols="" rows="6" class="form-control input-lg" readonly></textarea>
      </div>     
    </div>
  </div>
  <div id="page-content-wrapper">
    <div class="divx" id="googleMap"></div>
  </div>
</div>
</body>
</html>
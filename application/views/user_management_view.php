<script type="text/javascript">
function onchange_userType(vale)
{		
	document.getElementById("CheckIDLogin").value=vale.value;
	document.getElementById("myform").submit();		
}
function submit_form(vale)
{
	document.getElementById("CheckID").value=vale.value;	
	document.getElementById("myform").submit();		
}
</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <!-- style="display: none;" -->        
        <form id="myform" action="<?php echo(base_url("index.php/user_management_ctrl/user_validation/"))?>"	method="post" class="form-horizontal">
        <input type="text"  id="UserID" style="display: none;" name="UserID" value="<?php echo((isset($userID))? $userID:null) ?>"/>
        <input type="text"  id="CheckID" style="display: none;" name="CheckID" value=""/>
        <input type="text"  id="CheckIDLogin" style="display: none;" name="CheckIDLogin" value=""/>
        
          <h1>User Management</h1>
          <?php if(isset($outcome)) echo $outcome;?>
          <br>
          <div class="user-fields">
            <div class="field" <?php echo(($sessUserLoginType!=AUTOGRADE_USER)?'style="display: none;"':'');?>>
              <label for="password">User Type:<span style="color:red;"> *</span></label>
              <select name="LoginUserTypeID" id="LoginUserTypeID" onchange="onchange_userType(this);"> 
              <?php if($loginUserTypeList!=null): foreach ($loginUserTypeList as $typeList):?>             
              	<option value="<?php echo $typeList['type_id'] ?>" <?php echo(($loginUserTypeID==md5($typeList['type_id']))?'selected':'')?>><?php echo $typeList['type_name'] ?></option>              
              <?php endforeach; endif;?>           
             </select>
            </div>
            <div class="field">
              <label for="useremail"><?php echo $lableName?>:<span style="color:red;"> *</span></label>
              <select id="ClientID"  name="ClientID" onchange="submit_form(this)">
              <option value=""><?php if(null!=form_error('ClientID'))echo form_error('ClientID',' ',' '); ?></option>
              <?php if($clientList!=null): foreach ($clientList as $client):?>             
              <option value="<?php echo $client['value'] ?>" <?php echo(($anyID==md5($client['value']))?'selected':'')?>><?php echo $client['name'] ?></option>              
              <?php endforeach; endif;?> 
              </select>
            </div>
            <div class="field">
              <label for="employee">User Name(email):<span style="color:red;"> *</span></label>
              <input type="text" id="UserEmail" name="UserEmail" value="<?php echo $userEmail?>" class="form-control input-lg"  placeholder="<?php if(null!=form_error('UserEmail'))echo form_error('UserEmail',' ',' '); ?>" maxlength="100"/>
            </div>
            <div class="field">
              <label for="primaryrole">User Full Name:<span style="color:red;"> *</span></label>
              <input type="text" id="UserFullName" name="UserFullName" value="<?php echo $userFullName?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('UserFullName'))echo form_error('UserFullName',' ',' '); ?>" maxlength="100"/>
            </div>
            <div class="field">
              <label for="employee">Mobile No.:<span style="color:red;"> *</span></label>
              <input type="text" id="MobileNo" name="MobileNo" maxlength="10" value="<?php echo $mobileNo?>" class="form-control input-lg"  placeholder="<?php if(null!=form_error('MobileNo'))echo form_error('MobileNo',' ',' '); ?>" maxlength="20"/>
            </div>
            <div class="field">
              <label for="password">Role:<span style="color:red;"> *</span></label>
              <select name="UserRole">              
              <option value="0" <?php echo(($userRoleID=='0')?'selected':'')?>>Normal</option>
              <option value="1" <?php echo(($userRoleID=='1')?'selected':'')?>>Admin</option>              
             </select>
            </div>
            <div class="field" <?php echo(($loginUserTypeID==md5(DISTRIBUTOR_USER) || $loginUserTypeID==md5(DEALER_USER) || $loginUserTypeID==md5(PARENT_USER))?'style="display: none;"':'');?>>
              <label for="confirm password">Vehicle Groups Management:<span <?php echo(($vehicleGroupList==null)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <fieldset>
              <?php if($vehicleGroupList!=null): foreach ($vehicleGroupList as $row):?>
              
              <?php 
              //log_message('debug','**********'.count($selectedVehicleGroup));
              $checked=false;
              		if($selectedVehicleGroup!=null)
              		{
              			$checked=false;
              			foreach ($selectedVehicleGroup as $selected)
              			{
              				if($row['vh_gp_id']==$selected['vh_gp_link_vehicle_group_id'])
              				{
              					$checked=true;//$accessCount++;
              					break; 
              				}
              			}
              		}
                ?>         
              
              <input name="VehicleGroupID[]" type="checkbox" value="<?php echo $row['vh_gp_id']?>" <?php echo($checked?'checked':'')?>/>
              <label><?php echo $row['vh_gp_name']?></label>   
              <?php endforeach; endif;?>           
              </fieldset>              
            </div>
           <!--    <div class="field" <?php echo((($GLOBALS['ID']['sess_user_type']) != DEALER_USER)?'style="display: none;"':'');?>>
              <label for="confirm password">Vehicle Groups Management:<span <?php echo(($vehicleGroupList==null)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <fieldset>
              <?php if($vehicleGroupList!=null): foreach ($vehicleGroupList as $row):?>
              
              <?php 
              //log_message('debug','**********'.count($selectedVehicleGroup));
              $checked=false;
              		if($selectedVehicleGroup!=null)
              		{
              			$checked=false;
              			foreach ($selectedVehicleGroup as $selected)
              			{
              				if($row['vh_gp_id']==$selected['vh_gp_link_vehicle_group_id'])
              				{
              					$checked=true;//$accessCount++;
              					break; 
              				}
              			}
              		}
                ?>         
              
              <input name="VehicleGroupID[]" type="checkbox" value="<?php echo $row['vh_gp_id']?>" <?php echo($checked?'checked':'')?>/>
              <label><?php echo $row['vh_gp_name']?></label>   
              <?php endforeach; endif;?>           
              </fieldset>              
            </div>  -->
            <!-- User and driver group link -->
            <div class="field" <?php echo(($loginUserTypeID==md5(DISTRIBUTOR_USER) || $loginUserTypeID==md5(DEALER_USER) || $loginUserTypeID==md5(PARENT_USER))?'style="display: none;"':'');?>>
              <label for="confirm password">Driver Groups Management:<span <?php echo(($driverGroupList==null)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <fieldset>
              <?php if($driverGroupList!=null): foreach ($driverGroupList as $row):?>
              
              <?php 
              $checked=false;
              		if(!empty($selectedDriverGroup))
              		{
              			//log_message('debug','**********'.count($selectedDriverGroup));
              			$checked=false;
              			foreach ($selectedDriverGroup as $selected)
              			{
              				if($row['dr_gp_id']==$selected['dr_gp_link_driver_group_id'])
              				{
              					$checked=true;//$accessCount++;
              					break; 
              				}
              			}
              		}
                ?>         
              
              <input name="DriverGroupID[]" type="checkbox" value="<?php echo $row['dr_gp_id']?>" <?php echo($checked?'checked':'')?>/>
              <label><?php echo $row['dr_gp_name']?></label>   
              <?php endforeach; endif;?>           
              </fieldset>              
            </div>
         <!--     <div class="field" <?php echo((($GLOBALS['ID']['sess_user_type']) != DEALER_USER)?'style="display: none;"':'');?>>
              <label for="confirm password">Driver Groups Management:<span <?php echo(($driverGroupList==null)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <fieldset>
              <?php if($driverGroupList!=null): foreach ($driverGroupList as $row):?>
              
              <?php 
              $checked=false;
              		if(!empty($selectedDriverGroup))
              		{
              			//log_message('debug','**********'.count($selectedDriverGroup));
              			$checked=false;
              			foreach ($selectedDriverGroup as $selected)
              			{
              				if($row['dr_gp_id']==$selected['dr_gp_link_driver_group_id'])
              				{
              					$checked=true;//$accessCount++;
              					break; 
              				}
              			}
              		}
                ?>         
              
              <input name="DriverGroupID[]" type="checkbox" value="<?php echo $row['dr_gp_id']?>" <?php echo($checked?'checked':'')?>/>
              <label><?php echo $row['dr_gp_name']?></label>   
              <?php endforeach; endif;?>           
              </fieldset>              
            </div> -->
            <div class="field">
              <label for="useremail">Time Zone:<span style="color:red;"> *</span></label>
              <select id="TimeZoneID"  name="TimeZoneID">
               <?php if($timeZoneList!=null):  foreach ($timeZoneList as $row): if( $timeZoneID!=null):if($timeZoneID==$row['time_zone_id']):?>
               		<option value="<?php echo $row['time_zone_id']?>" <?php echo 'selected'; ?>><?php echo $row['time_zone_name']?> </option>
               		<?php endif; else: ?>
               		<option value="<?php echo $row['time_zone_id']?>" ><?php echo $row['time_zone_name']?> </option>
               <?php  endif; endforeach; endif;?>
              </select>
            </div>
          </div>
          <!-- /login-fields -->
           <div class="login-actions"> <span class="login-checkbox">
            <input id="Active" name="Active" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>>
            <label class="choice" for="Field">Active</label>
            </span> </div>
          <div class="field">
            <button class="btn btn-primary" type="submit"><?php echo(($userID==null)?"Save":"Update")?></button>
            <?php $reloadURL=base_url("index.php/user_management_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->																	
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>	
          </form>
 		<form method="post" action="<?php echo(base_url("index.php/user_management_ctrl/edit_userManagement/"))?>">
          <div class="table-responsive">
            <h6 class="linkred">Existing Users</h6>
            <table width="100%" class="user-dts">
              <tr>
                <th>User</th>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
              </tr>
              <?php if($userList!=null): foreach ($userList as $row):?>                
              <tr>
              	<td><?php echo $row['user_user_name']?></td>
                <td><?php echo $row['user_email']?></td>
                <td><?php echo (($row['user_is_admin']=='1')?'Admin':'Normal')?></td>
               <td><a href="<?php echo(base_url("index.php/user_management_ctrl/edit_userManagement/".md5($row['user_id'])."/".md5($row['user_type_id'])))?>" title = "Edit Users"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php endforeach; endif; ?>              
            </table>
          </div>
          </form>
          <nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $pageLink;?>
			</ul>
		 </nav>
          <div class="clearfix"></div>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
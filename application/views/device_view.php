<script type="text/javascript">

		$(function() {
			$( "#DevicePrepareDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });
			$( "#DeviceReleaseDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
			$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
		});

</script>

<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
#DeviceIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/device_ctrl/vts_device_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" style="display: none;" id="DeviceId"  name="DeviceId" value="<?php echo((isset($dvice_id))? $dvice_id:null) ?>" />
					
					<h1>Device Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					<br>
					<div class="user-fields">
						<div class="field">
							<label>Device Type:<span style="color:red;"> *</span></label>
							<select name="DeviceTypeId">
								<option value=""><?php if(null!=form_error('DeviceTypeId'))echo form_error('DeviceTypeId',' ',' '); ?></option>
								<?php foreach ($deviceTypeList as $row):?>
								<option value="<?php echo $row['device_type_id']?>" <?php echo(($dvice_device_type_id==$row['device_type_id'])?'selected':'')?>><?php echo $row['device_type_name']?> </option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>IMEI:<span style="color:red;"> *</span></label>
							<?php if($dvice_id==null){?>
							<input type="text" id="DeviceIMEI" name="DeviceIMEI" value="<?php if(!isset($outcome))echo $dvice_imei?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DeviceIMEI'))echo form_error('DeviceIMEI',' ',' ');?>" />
							<span style="font-size:11px; padding-left:122px;color:blue;">(For multiple IMEI numbers, please enter comma separated)</span>
							<span style="font-size:11px; padding-left:122px;color:blue;">(Ex: 865733022623516,865733022629240)</span>
							<?php } else{?>
							<input type="text" id="DeviceIMEI" maxlength="15" name="DeviceIMEI" value="<?php if(!isset($outcome))echo $dvice_imei?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DeviceIMEI'))echo form_error('DeviceIMEI',' ',' ');?>" />
							<?php }?>
						</div>
						<div class="field">
							<label>Prepared Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="DevicePrepareDate" name="DevicePrepareDate" class="form-control input-lg" value="<?php if(!isset($outcome))echo substr_replace($dvice_prepare_date, '', 10); ?>" placeholder="<?php if(null!=form_error('DevicePrepareDate'))echo form_error('DevicePrepareDate',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Device_SLNo:<span style="color:red;"> *</span></label>
							<?php if($dvice_id==null){?>
							<input style="font-size: 12px;" type="text" id="DeviceSlno" name="DeviceSlno" class="form-control input-lg" value="<?php echo $dvice_slno ?>" placeholder="<?php if(null!=form_error('DeviceSlno'))echo form_error('DeviceSlno',' ',' ');?>" />
							<?php } else {?>
							<input style="font-size: 12px;" maxlength="6" type="text" id="DeviceSlno" name="DeviceSlno" class="form-control input-lg" value="<?php echo $dvice_slno ?>" placeholder="<?php if(null!=form_error('DeviceSlno'))echo form_error('DeviceSlno',' ',' ');?>" />
							<?php } ?>
						</div>
						<div class="field">
							<label>Distributor:</label>
							<select name="DeviceDistributorId">
							<?php if($dvice_dealer_id == null): ?>
										<option value=""><?php if(null!=form_error('DeviceDistributorId'))echo form_error('DeviceDistributorId',' ',' '); ?></option>
							<?php endif; ?>
								<?php foreach ($distributorList as $row): //log_message("debug", "dataaa suni--".print_r($row, true));
									if($dvice_dealer_id == null): ?>
										<option value="<?php echo $row['dist_id']?>" <?php echo(($dvice_distributor_id==$row['dist_id'])?'selected':'')?>><?php echo $row['dist_name']?> </option>
								<?php elseif($dvice_distributor_id==$row['dist_id']): ?>
									<option value="<?php echo $row['dist_id']?>" ><?php echo $row['dist_name']?> </option>
								<?php endif; endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>Release Date: </label>
							<input style="font-size:13px;" maxlength="10" id="DeviceReleaseDate" name="DeviceReleaseDate" class="form-control input-lg" value="<?php if(!isset($outcome)) echo substr_replace($dvice_release_distributor_date, '', 10); ?>" />
						</div>
						<div class="field">
							<label>Remarks:</label>
							<textarea maxlength="500" id="DeviceRemarks" name="DeviceRemarks" class="form-control input-lg" placeholder="<?php if(null!=form_error('DeviceRemarks'))echo form_error('DeviceRemarks',' ',' ');?>"><?php if(!isset($outcome))echo $dvice_remarks?></textarea>
						</div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="DeviceIsActive" name="DeviceIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>> 
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br />
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($dvice_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/device_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/device_ctrl/editOrDelete_vts_device/"))?>">
					<div class="table-responsive">
						<table style="width:100%;" class="user-dts">
							<tr>
								<th>IMEI</th>
								<th>Slno</th>
								<th>Distributor</th>
								<th>Released</th>
								<th>Action</th>
							</tr>
							<?php foreach ($DeviceList as $row): ?> 
							<tr>
								<td><?php echo trim($row['device_imei'])?></td>
								<td><?php echo trim($row['device_slno'])?></td>
								<td><?php echo trim($row['distributor_name'])?></td>
								<td><?php echo trim($row['device_release_distributor_date'])?></td>
								<td>
									<a href="<?php echo(base_url("index.php/device_ctrl/editOrDelete_vts_device/?device_id=edit-".trim($row['device_id'])))?>" title="Edit Device Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
									<a href="<?php echo(base_url("index.php/device_ctrl/editOrDelete_vts_device/?device_id=delete-".trim($row['device_id'])))?>" title="Delete Device Details" onclick="return confirm('Do you want to delete the Device')"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
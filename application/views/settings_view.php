<!-- Login -->
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form method="post" class="form-horizontal"
					action="<?php echo base_url('index.php/settings_ctrl/save');?>">
					<input type="text" style="display: none"
						value="<?php echo(isset($row_edit)?$row_edit->setting_id:'')?>"
						name="id" /> <input type="text" style="display: none"
						value="<?php echo(isset($row_edit)?$row_edit->setting_key_name:'')?>"
						name="SetName" />
					<h1>Settings Details</h1>
					<font color="green"><?php echo (isset($output)?$output:'')?></font>
					<br />
					<br />
					<div class="user-fields">
						<div class="field">
							<label>Settings Name:</label> <input type="text" id="ItemGroup"
								name="settingname" disabled="disabled"
								value="<?php if(isset($row_edit)){echo $row_edit->setting_key_name;}?>"
								class="form-control input-lg" required />
						</div>
						<div class="field">
							<label>Description:</label> <input type="text" id="ItemGroup"
								name="description" disabled="disabled"
								value="<?php if(isset($row_edit)){echo $row_edit->setting_description;}?>"
								class="form-control input-lg" required />
						</div>
						<div class="field">
							<label>Key Value:</label> <input type="text" maxlength="100"
								id="ItemGroup" name="setting_key_value"
								value="<?php if(isset($row_edit))echo trim($row_edit->setting_key_value)?>"
								placeholder="<?php echo((form_error('setting_key_value')!=null)? form_error('setting_key_value',' ',' '):null)?>"
								class="form-control input-lg" />
						</div>
					</div>
					<!-- /login-fields -->
					<div class="field">
						<input type="submit" class="btn btn-primary" name="save"
							value="Update" />
			<?php $reloadURL=base_url("index.php/settings_ctrl/")?>
			<button type="reset" class="btn btn-primary" id="cancel"
							onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
					<div class="table-responsive">
			<?php if(count($row)>=1){?>
			<table width="100%" class="user-dts">
							<tr>
								<th>Settings Name</th>
								<th>Description</th>
								<th>Setting Value</th>
								<th>Action</th>
							</tr>
				<?php foreach($row as $settings => $setting){ ?>
				<tr>
								<td><?php echo $setting->setting_key_name; ?></</td>
								<td><?php echo $setting->setting_description; ?></td>
								<td><?php echo $setting->setting_key_value; ?></td>
								<td><a class="edit"
									href="<?php echo base_url()?>index.php/settings_ctrl/edit/?setting_id=<?php echo $setting->setting_id; ?>" title="Edit Setting Details"><i
										class="fa fa-pencil-square-o"></i></a></td>
							</tr>
				<?php } ?>
			</table><?php }?>
		</div>
					<div class="clearfix"></div>
				</form>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>

  <nav class="navbar navbar-inverse" <?php echo(($sessClientID != AUTOGRADE_USER)?'style="display: none;"' :'' );?>>
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-brand">Autograde Admin</span>
    </div>
    <ul class="nav navbar-nav">      
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Distributor &amp; Dealer
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/distributor_ctrl")?>">Distributor</a></li>
         <li><a href="<?php echo base_url("index.php/dealer_ctrl")?>">Dealer</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Device
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/device_type_ctrl")?>">Device Type</a></li>
         <li><a href="<?php echo base_url("index.php/device_ctrl")?>">Device</a></li>
         <li><a href="<?php echo base_url("index.php/device_allocation_dealer_ctrl")?>">Device Allocation to Dealer</a></li>
         <li><a href="<?php echo base_url("index.php/device_allocation_ctrl")?>">Device Allocation to Client</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Client
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/client_ctrl")?>">Client</a></li>
         <li><a href="<?php echo base_url("index.php/client_license_ctrl")?>">Client License</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tester
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url("index.php/device_monitoring_ctrl")?>">Device Monitoring</a></li>
          <li><a href="<?php echo base_url("index.php/vehicle_admin_tracking_ctrl")?>">Tester Tracking</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/service_ctrl")?>">Service</a></li>
         <li><a href="<?php echo base_url("index.php/settings_ctrl")?>">Settings</a></li>
		 <li><a href="<?php echo base_url("index.php/vehicle_make_model_ctrl")?>"> Vehicle Make &amp; Model</a></li>
        </ul>
      </li>
       <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Others
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/individual_user_ctrl")?>">Individual User</a></li>
         <li><a href="<?php echo base_url("index.php/individual_vt_ctrl")?>">Personal Tracking</a></li>
         <li><a href="<?php echo base_url("index.php/distributor_dashboard_ctrl")?>">Distributor Dashboard</a></li>
         <li><a href="<?php echo base_url("index.php/dealer_dashboard_ctrl")?>">Dealer Dashboard</a></li>
        </ul>
      </li>      
    </ul>
  </div>
</nav>
  <div class="container-fluid">
  <div class="row">
		<div class="col-sm-12" style="padding: 5px; border-radius:10px; background-color: lightblue;">			
			<p><i><b>Note: </b>Please check the IMEI no. first in device monitoring screen. If the 'Last Date' in 'Received Date &amp; Time' is older than 1 month, then please don't give that IMEI no.</i></p>
		</div>
	</div>
    <div class="row"> 
  <input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/vehicle_admin_tracking_ctrl/")?>"/>
  <input type="Text" style="display : none;" id="ReportURL" name="ReportURL" value="<?php echo base_url("index.php/report_ctrl/report_generation/")?>"/>
  <input type="Text" style="display : none;" id="wayPntURL" name="ReportURL" value="<?php echo base_url('assets/images/')?>"/>
  <input type="Text" style="display : none;" id="SessClient" name="SessClient" value="<?php echo $sessClientID ?>"/>
  <input type="Text" style="display : none;" id="ClientID" name="ClientID" value="<?php echo $sessClientID ?>"/>
  <input type="Text" style="display : none;" id="SessUser" name="SessUser" value="<?php echo $sessUserID ?>"/>
  
  
  <div class="col-sm-6">
   <!-- <div class="nav">-->
      <div class="field">
        <input id="Replay" name="TraceType" type="radio" class="field login-checkbox" value="1" tabindex="1" onchange="radio_btn_click()" />
        <label>Trace History</label>
        &nbsp;
        <input id="Live" name="TraceType" type="radio" class="field login-checkbox" value="1" tabindex="2" checked onchange="radio_btn_click()"/>
        <label>Live Track</label>
      </div>
      <div class="field" id="PlottingType" style="display:none;">
        <input id="StepByStepPlotting" name="Plotting" type="radio" class="field login-checkbox" value="1" tabindex="1" onchange="plotting_radio_btn_click()" />
        <label>Step-by-step plotting</label>
        &nbsp;
        <input id="BulkPlotting" name="Plotting" type="radio" class="field login-checkbox" value="1" tabindex="2" checked onchange="plotting_radio_btn_click()"/>
        <label>Bulk plotting</label>
      </div>
      <label>Speed limit:</label>
		<input type="number" maxlength="3" id="speedlimit" name="speedlimit" value="80" size="1" style="border-radius: 5px;"></input>
      <div class="field">		
		<label>IMEI No.:</label>
		<input type="text" maxlength="15" id="imeino" name="imeino" value=""  class="form-control input-lg"></input>
	  </div>
      <div class="field">
        <label>Info : </label>
        <textarea style="font-size: 15px;" id="info" name="info" cols="" rows="6" class="form-control input-lg" readonly></textarea>
      </div>
      <p id="demo"></p>	
  </div>
   <div  class="col-sm-6">   
      <div class="field">
        <label>Time Selection :</label>
        <select style="font-size: 15px;" id="TimeSelection" name="TimeSelection" class="form-control input-lg" onchange="time_selection_change()">
         <option value="today">Today</option>
         <option value="yesterday">Yesterday</option>
         <option value="custom">Custom Date&Time</option>		
        </select>
      </div>
      <?php        
         $start_date=$startDate;
         $end_date=$endDate;
  		?>
      
      <div class="field">
        <label>Start date&amp;time :</label>
        <input style="font-size: 15px;" type="datetime-local" id="Fdaytime" name="Fdaytime" class="form-control input-lg" value="<?php echo $start_date->format('Y-m-d').'T'.$start_date->format('H:i')?>"/>
      </div>
      <div class="field">
        <label>End date&amp;time :</label>
        <input style="font-size: 15px;" type="datetime-local" id="Tdaytime" name="Tdaytime" class="form-control input-lg" value="<?php echo $end_date->format('Y-m-d').'T'.$end_date->format('H:i')?>"/>
      </div>
      <div class="field">
        <button id="report" class="btn btn-primary" onclick="reportFunction()">Report</button>
        &nbsp;&nbsp;
        <button id="startTimer" class="btn btn-primary" onclick="timerFunction()">Go</button>
        &nbsp;&nbsp;
        <button id="stopTimer" class="btn btn-primary" onclick="stopTimerFunction()">Stop</button>
      </div>
	  </div>	  
  </div>
</div>
</div>

<!--  <div id="page-content-wrapper">-->
	<div class="container-fluid">	
		<div class="row" >			
			<div class="col-sm-12" style="padding: 5px; border-radius:10px; height:500px; background-color: lightblue;">			
			<div id="googleMap" style=" border-style: inset; border-radius:10px; height: 490px">
			</div>
			<button id="go-to-top"  style=" border-style: solid; border-width: 2px; border-color:#758C30; background-color:#90C551; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1.428571429; border-radius: 15px; position: absolute;right: 0px; top:200px;">^</button>			
			</div>
		</div>
	</div>
</body>
</html>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="CloseModelBtn" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Stop Detail</h4>
        <?php if(isset($modeloutcome)) echo $modeloutcome;?>
      </div>      
      <div class="modal-body">   
      <input type="Text" style="display : none;" id="ArrayIndex" name="ArrayIndex" value=""/>    
          <div class="user-fields">          
            <div class="field">
              <label for="Stop">Stop Name:<span style="color:red;"> *</span></label>
              <input type="text" id="StopName" name="StopName" value="" class="form-control input-lg" placeholder="Stop Name" maxlength="100"/>
            </div>      
          </div>
          <div class="clearfix"></div>       
      <div class="modal-footer">
        <input type="button" class="btn btn-primary" name="submit" id="submit" value="Change" onclick="change_stop_name()" />       
      </div>       
      </div>
    </div>
  </div>
</div>

<div id="wrapper"> 
  <!-- style="display : none;" -->  
  <input type="Text" style="display : none;" id="URL" name="URL" value="<?php echo base_url("index.php/vehicle_geofence_ctrl/")?>"/>  
  <input type="Text" style="display : none;" id="MarkerPntURL" name="MarkerPntURL" value="<?php echo base_url('assets/images/')?>"/>
  
  <input type="Text" style="display : none;" id="RouteID" name="RouteID" value="<?php echo $routeID ?>"/>
  <input type="Text" style="display : none;" id="IsGeoFence" name="IsGeoFence" value="<?php echo $geofence ?>"/>
  
  <div id="sidebar-wrapper">
    <div class="nav"> 
    	<div class="field"> 
    		<a href="<?php echo(base_url("index.php/vehicle_geofence_ctrl/"))?>">
    			<img alt="Home" title="Back to Vehicle Route" src="<?php echo base_url("assets/images/home_icon.png")?>"/>
        	</a>
        	<!--  <a id="UndoBtn" href="#" onclick="undo_fun()">
        	<img alt="Undo" title="Undo the Marker Points." src="<?php //echo base_url("assets/images/back-icon.png")?>" />
        	</a>- -->
        	<!-- <input id="UndoBtn" type="button" title="Undo the Marker Points." onclick="undo_fun()" value="Undo"/> -->
        </div>
        <div class="field">
              <label for="employee">Route:</label>
              <input type="text" id="Route" name="Route" value="<?php echo $routeName ?>" class="form-control input-lg"  placeholder="Route" readonly/>              
        </div> 
        <div class="field">
              <label for="employee">Search Location:</label>
              <input type="text" id="SearchLocation" name="SearchLocation" value=" " class="form-control input-lg"  placeholder="Search Location"/>
              <button style="display: none;" id="AddBtn" type="button" title="Add device" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Edit Stop Name</button>              
        </div>     
      <div class="field">
        <button id="Save" class="btn btn-primary" onclick="saveFunction()">Save</button>
        &nbsp;
        <button id="RemoveAllMarkers" class="btn btn-primary" onclick="deleteFunction()">Remove All</button>
        &nbsp;
        <button id="ClearAll" class="btn btn-primary" onclick="clearAllMarker()">Clear</button>
        <button id="UndoBtn" class="btn btn-primary" onclick="undo_fun()">Undo</button>
      </div>
      <div class="field">
        <label>Info : </label>
        <textarea style="font-size: 15px;" id="info" name="info" cols="" rows="6" class="form-control input-lg" readonly></textarea>
      </div>
    </div>
  </div>
  <div id="page-content-wrapper">
    <div class="divx" id="googleMap"></div>
  </div>
</div>
</body>
</html>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="account-container stacked"><br>
      <div class="content clearfix">
      <form action="<?php echo(base_url("index.php/forget_password_ctrl/validate_user/"))?>"	method="post" class="form-horizontal">
          <h1>Forgot Password</h1>
          <div class="login-fields">
            <div class="field control-group">
              <label for="username">Username:</label>
              <input type="text" id="UserName" name="UserName" value="<?php echo $UserName?>" placeholder="<?php echo((null!=form_error('User Name'))?form_error('User Name',' ',' '):'User Name'); ?>" class="form-control input-lg username-field" />
            </div>            
          <div class="field">
            <button class="btn btn-primary">Ok</button>
            <?php $reloadURL=base_url("index.php/forget_password_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>          
          <div class="clearfix"></div>
          <!-- .actions -->
        </form>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/simple-sidebar.css')?>" rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js"></script>
<link href="<?php echo base_url('assets/src/css/bootstrapValidator.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/src/js/bootstrapValidator.js')?>"></script>
<!-- jQuery -->
<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script type="text/javascript">
var map; var latLngPoint=[];
var i;
var arr=[];
var arrayMarker=[];  
var msg=[];



function initMap()
{	
	var mapProp = {
			  center:new google.maps.LatLng(0,0),
			  zoom:2,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			  };			  
	map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	google.maps.event.addListener(map, 'zoom_changed', function() {
				var zoomLevel=map.getZoom();			
				if(zoomLevel < 2)
				{	
					map.setZoom(2);			
				}						
			});
	//alert(document.getElementById("demos").innerHTML);
	place_markers();
}

function place_markers()
{
	var test_loop=0;
	var test_point=[];
	var point1;var point2;var distance_btw_pnts=0, temp_distance=0;
	var mid_point;
	
	latLngPoint=[];
    arr = JSON.parse(document.getElementById("demos").innerHTML);
    if(arr.length!=0)
    {    	
    	for(i = 0; i < arr.length; i++)
		{    		  
    		latLngPoint[i]=new google.maps.LatLng(arr[i].latitude,arr[i].longitude);    		   		
    		arrayMarker[i]=new google.maps.Marker({ position: latLngPoint[i], map: map, animation: google.maps.Animation.DROP});    		
    		arrayMarker[i].setTitle(arr[i].vehicle+"["+arr[i].driver+"]\nSpeed(km/h) : "+ arr[i].maxspeed +"("+arr[i].speedlimit+") \nDateTime : "+arr[i].from_date_time);    		
    		google.maps.event.addListener(arrayMarker[i], 'click', function() {        		        		
        		var infoWindow=new google.maps.InfoWindow({content:this.getTitle()});
        		infoWindow.open(map,this);
    	    });
			
    		for(test_loop = 0; test_loop < arr.length ; test_loop++)
		 	{				
				distance_btw_pnts =distance(arr[i].latitude,arr[i].longitude,arr[test_loop].latitude,arr[test_loop].longitude);				
				if(parseFloat(distance_btw_pnts) >= parseFloat(temp_distance))
				{
					temp_distance=distance_btw_pnts;
					point1=new google.maps.LatLng(arr[i].latitude,arr[i].longitude);
					point2=new google.maps.LatLng(arr[test_loop].latitude,arr[test_loop].longitude);					
				}	
				//alert("dist:"+distance_btw_pnts+" temp:"+temp_distance+"i="+i+" loop="+test_loop);
		 	}     		 		    		
		} 		
		mid_point=new google.maps.LatLng(((point1.lat() + point2.lat())/2),((point1.lng() + point2.lng())/2));	
		map.setCenter(mid_point);
		//alert(temp_distance);
		if(temp_distance <= (100/1000))					
			map.setZoom(20);					
		else if(temp_distance <= (200/1000))
			map.setZoom(19);
		else if(temp_distance <= (400/1000))
			map.setZoom(18);
		else if(temp_distance <= (800/1000))
			map.setZoom(17);
		else if(temp_distance <= 1.5)
			map.setZoom(16);
		else if(temp_distance <= 3)
			map.setZoom(15);
		else if(temp_distance <= 6)
			map.setZoom(14);
		else if(temp_distance <= 12)
			map.setZoom(13);
		else if(temp_distance <= 25)
			map.setZoom(12);
		else if(temp_distance <= 50)
			map.setZoom(11);
		else if(temp_distance <= 100)
			map.setZoom(10);
		else if(temp_distance <= 200)
			map.setZoom(9);
		else if(temp_distance <= 400)
			map.setZoom(8);
		else if(temp_distance <= 800)
			map.setZoom(7);
		else if(temp_distance <= 1600)
			map.setZoom(6);
		else if(temp_distance <= 3165)
			map.setZoom(5);
		else if(temp_distance <= 6320)
			map.setZoom(4);
		else 
			map.setZoom(3);
    }  
/*
 * This function is used to return the distance between two points in km
 */
	function distance(lat1, lon1, lat2, lon2) 
	{
		    var radlat1 = Math.PI * lat1/180;
		    var radlat2 = Math.PI * lat2/180;
		    var radlon1 = Math.PI * lon1/180;
		    var radlon2 = Math.PI * lon2/180;
		    var theta = lon1-lon2;
		    var radtheta = Math.PI * theta/180;
		    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		    dist = Math.acos(dist);
		    dist = dist * 180/Math.PI;
		    dist = dist * 60 * 1.1515;
		    dist = dist * 1.609344;
		    return dist;
	}
		      
}
</script>
<style type="text/css">
.nav {
	line-height: 14px;/*30px;*/
	background-color: #eeeeee;
	float: left;
	padding: 3px;
}
.divx {
	/*position: absolute;
	width: 100%;*/
	height: 637px;//574px;508px;*/
	/*float: left;
	margin-right: 5px;*/
}
@CHARSET "ISO-8859-1";
</style>
</head>

<body id="bodyTag" onload="initMap()"><!--initMap  function_onload_page-->
<div class="topdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
      <?php
      $home_link="";
      if($this->session->userdata('login')!=null)
      {
      	$sess_details=$this->session->userdata('login');
      	$user_type=$sess_details['sess_user_type'];
      	$is_admin=$sess_details['sess_is_admin'];
      if($user_type == AUTOGRADE_USER)
      	$home_link="home_ctrl";
      else if($user_type == OHTER_CLIENT_USER)
      {
      	if($is_admin == '1')
      		$home_link="home_ctrl";
      	else
      		$home_link="vehicle_tracking_ctrl";
      }
      else if($user_type == INDIVIDUAL_CLIENT)
      	$home_link="individual_vt_ctrl";
      else if($user_type==DISTRIBUTOR_USER)
      	$home_link="distributor_dashboard_ctrl";
      else if($user_type==DEALER_USER)
      	$home_link="dealer_dashboard_ctrl";
      else if($user_type==PARENT_USER)
      	$home_link="individual_vt_ctrl";
      }else{$home_link="login_ctrl";}
      ?>
       <h3><a href="<?php echo base_url('index.php/'.$home_link)?>"><img alt="" src="<?php echo base_url('assets/images/logo.png')?>" /></a></h3>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-center">
         <h5><?php if ($this->session->userdata ( 'login' )){ echo $ID['sess_client_name']; }?></h5>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 text-right">
      <?php if ($this->session->userdata ( 'login' )){ ?>
       <h6><?php echo $ID['sess_user_name']; ?></h6>
       <h6><a href= "<?php echo base_url('index.php/change_password_ctrl')?>">Change Password</a> | <a href= "<?php echo base_url('index.php/home_ctrl/logout')?>">Logout</a></h6>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="secondtopdiv">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="pull-left">
          <div class="dropdown menubtn">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i> </button>    	    
 <!-- --------------------------------------------------------------- -->   	    
<?php  
if ($this->session->userdata ( 'login' )){	
     	$entered_once=0; $menu_name=""; $sub=false;
     	if($string !=NULL )
     	echo('<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">');  
     	if($string) 
     	{ 		
     		foreach ($string as $sub_menu_list)
     		{    			
     				if($sub_menu_list[3]!=' ' & $menu_name!=$sub_menu_list[2])
     				{
     					if($sub)
     					{
     						echo('</ul></li>');
     						$sub=false;
     					}
     					echo('<li class="dropdown-submenu"> <a tabindex="-1" href="#">'. $sub_menu_list[2].'</a>');     					
     					echo('<ul class="dropdown-menu">');
     					$menu_name=$sub_menu_list[2];
     					
     				}
     				else if($sub_menu_list[3]!=' ' & $menu_name==$sub_menu_list[2])
     				{
     					echo('<li><a href="'.base_url("index.php/".$sub_menu_list[4]).'">'.$sub_menu_list[3].'</a></li>');
     					$sub=true;
     					continue;
     				}    
     				else if($sub_menu_list[3]==' ' & $menu_name!=$sub_menu_list[2] & ($sub_menu_list[4]!='forget_password_ctrl' & $sub_menu_list[4]!='change_password_ctrl' & $sub_menu_list[4]!='login_ctrl')) 
     				{
     					echo('<li> <a href="'.base_url("index.php/".$sub_menu_list[4]).'">'. $sub_menu_list[2].'</a></li>');
     					$menu_name=$sub_menu_list[2];
     					$sub=false;
     					continue;
     				}									
     			  		
     			if($menu_name==$sub_menu_list[2])
     				echo('<li><a href="'.base_url("index.php/".$sub_menu_list[4]).'">'.$sub_menu_list[3].'</a></li>');
     		}
     	if($sub)
     	{
     		echo('</ul></li>');
     		$sub=false;
     	}
     	}
     	echo('</ul>');
}   
     ?>

          </div>
        </div>
        <div>
          <h5>Vehicle Tracking Software</h5>
          <em> Version 1.0.01</em><br>
          <em><a style="text-decoration:none; color:white;" href = "http://www.autograde.in" target="_blank">by Autograde</a> </em></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 text-right">
      <em> <?php if(isset($GLOBALS['timezone'])) { 
       date_default_timezone_set($timezone);
       echo $utc_time = date(' jS \of F Y h:i:s A'); }
       ?>
        <?php // echo date('d - F Y')?></em></div>
    </div>
  </div>
</div>
<div id="wrapper"> 
<p style="display:none ;" id="demos"><?php echo $gpsdata ?></p>
  <div id="page-content-wrapper">
    <div class="divx" id="googleMap"></div>
  </div>
</div>
</body>
</html>
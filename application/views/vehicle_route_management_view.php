<script type="text/javascript">
var count=0;
function check_checkbox(route_id)
{		
	var selectionLimit=document.getElementById("MaxRoutePerVehicle").value;	
	var status=true;
	var selectedCount=parseInt(document.getElementById("selectedCount").value);
	//selectedCount=(selectedCount==0)?0:selectedCount-1;
	
	count=(route_id.checked)?++count:--count;
	//alert(selectedCount+count);	
	if((selectedCount + count)<=selectionLimit)
	{
		status=!route_id.checked;		
	}
	else{
		count=--count;
		route_id.checked=false;		
		alert("Max "+selectionLimit+" Route can assign per vehicle.");
	}	
}

function submit_form(clientid)
{
	//alert(clientid.value);
	document.getElementById("OnClientID").value=clientid.value;	
	document.getElementById("mainForm").submit();		
}

function get_vhl_gp_vehicle()
{	
	refresh_arrayMarker()
	var baseUrl = document.getElementById('URL').value;
	var vehicleGp = document.getElementById('VehicleGroup').value;
	var vhID = (document.getElementById('VhID').value!="")?document.getElementById('EditVhID').value:0;	
	if(vehicleGp!=null)
	{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_vhl_grp_vehicle/"+vehicleGp+"/"+vhID;	
		xmlhttp.onreadystatechange=function() {
	    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {	    	
	    		vhl_gp_vehicle(xmlhttp.responseText);
	    	}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
}

function vhl_gp_vehicle(response)
{
	refresh_arrayMarker()
	document.getElementById("Vehicle").innerHTML=null;
	var x=document.getElementById("Vehicle");
	var option=document.createElement("option");	
	if(response!="null")
	{    
	    var arrs = JSON.parse(response);   
	    var id="";var vehicleName="";    
	    for(j=0;j<arrs.length;j++)
	    {
	      id=arrs[j].vh_id;
	      vehicleName=arrs[j].vh_name;
	      adds(id,vehicleName);    
	    }
	}else{		
		option.text="Empty";
		option.value="";
		x.add(option);
    }
}
function adds(id,vehicleName)
{
	var x=document.getElementById("Vehicle");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}
function refresh_arrayMarker()
{
	count=0;
	var chk_arr =  document.getElementsByName("RouteArr[]");
	var chklength = chk_arr.length;
	for(k=0;k< chklength;k++)
	{
	    chk_arr[k].checked = false;
	} 
}
</script>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<input type="text" id="MaxRoutePerVehicle" name="MaxRoutePerVehicle" style="display: none;" value="<?php echo MAX_NO_OF_ROUTE_PER_VEHICLE ?>" />
				<input type="text" id="URL" name="URL" style="display: none;" value="<?php echo(base_url("index.php/vehicle_route_management_ctrl/"))?>" />
				<input type="text" id="selectedCount" style="display: none;" value="<?php echo count($editrouteList)?>" />
				<form id="mainForm" action="<?php echo(base_url("index.php/vehicle_route_management_ctrl/vh_route_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->					
					<input type="text" id="OnClientID" name="OnClientID" style="display: none" value="-1"/>
					<input type="text" id="VhID" style="display:none;"  name="VhID" value="<?php echo((isset($vhID))? $vhID:null) ?>" />
					<h1>Route Management</h1>					
          			<div><?php if(isset($outcome)) echo $outcome;?></div>         			
          			</br>          			
					<div class="user-fields">
						<div class="field" <?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)?>>
	          				<label for="useremail" >Client:<span style="color:red;"> *</span></label>
	          				<select id="ClientID"  name="ClientID" onchange="submit_form(this)">          				
	          				<?php if($clientList!=null):foreach ($clientList as $row):?>
	          				<?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?><!-- if client ID = 1 and if loged in is a Dealer(i.e. Autograde Client then dropdown allow to select different client)(If it is Dealer the client dropdown should have clients realted to that particular dealer) -->
	          				<option value="<?php echo $row['client_id']?>" <?php echo(($clientID==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
	          				<?php elseif($sessClientID==$row['client_id']): ?>
	          				<option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
	          				<?php endif;?>
	          				<?php endforeach; endif;?>
	          				</select>
	          			</div>
						<div class="field">
							<input type="text" id="EditVhID" name="EditVhID" style="display: none" value="<?php echo $editVhID ?>"/>  
							<label>vehicle Group:<span style="color:red;"> *</span></label>
							<?php echo '<span style="color:red;">'.form_error('VehicleGroup',' ',' ').'</span>'?>
							<select  id="VehicleGroup" name="VehicleGroup" class="form-control input-lg" onchange="get_vhl_gp_vehicle()">
						      <?php
						      	if(!empty($vehicleGroupList))
						      	{						      		
									foreach ($vehicleGroupList as $row)
									{
										$select=($vehicleGroupID==$row['vh_gp_id'])?'selected':'';
										echo '<option value="'.$row['vh_gp_id'].'" '.$select.'>'.$row['vh_gp_name'].'</option>';
									}
						      	}
						      	else{echo'<option value="">Empty</option>';}
							 ?>
						   </select>
					     </div>
				      <div class="field">
				        <label>Vehicle:<span style="color:red;"> *</span></label>
				        <?php echo '<span style="color:red;">'.form_error('Vehicle',' ',' ').'</span>'?>
				        <select  id="Vehicle" name="Vehicle" class="form-control input-lg" onchange="refresh_arrayMarker()">				          
				          <?php 
							if($vehicleList!=null)
							{			 	
					         	foreach ($vehicleList as $row)
					         	{
					         		$select=($vehicleID==$row['vh_id'])?'selected':'';
						     		echo '<option value="'.$row['vh_id'].'" '.$select.'>'.$row['vh_name'].'</option>';
							 	}
							}else{echo'<option value="">Empty</option>';}
						 ?>
				        </select>
				      </div>
				      <div class="field">
				      	<label>Route List:<span style="color:red;"> *</span></label>
				      	<fieldset>
				      	<?php if($routeList!=null): foreach ($routeList as $row):?>
				          <?php 
				            $routeCheck="";
				          	if(isset($editrouteList[$row['route_id']]))
				          	{	
				          		$routeCheck="checked";				          		
				          	}							
				          	?>				          
				          	<input type="checkbox" name="RouteArr[]" style ="height:15px;width: 15px;" value="<?php echo $row['route_id']?>" <?php echo $routeCheck; ?> onchange="check_checkbox(this)"/>
				          	<label style="width: 90%;"><?php echo '('.$row['route_user_define_id'].') '.$row['route_name']?></label>
				          <?php endforeach; endif;?>
				      	</fieldset>			      
				      </div>
					</div>					
					<!-- /login-fields -->
					<div class="field" >
						<button class="btn btn-primary" type="submit"><?php echo(($vhID==null)?"Save":"Update")?> </button>
            			<?php $reloadURL=base_url("index.php/vehicle_route_management_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>				
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						  <tr>
						     <th>Vehicle</th>
							 <th>Route</th>							 
							 <th>Action</th>
						  </tr>
			              <?php if($vhRouteList!=null):foreach ($vhRouteList as $row): ?> 
			              <tr>			              	 
			                 <td><?php echo trim($row['vehicle_regnumber']); ?></td>
							 <td><?php echo trim(htmlentities($row['route_name']));?></td>							 
							 <td>
							    <a href="<?php echo(base_url("index.php/vehicle_route_management_ctrl/edit_routedetails/".md5(trim($row['vh_rt_vehicle_id']))))?>" title="Edit the vehicle route"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
							    <a href="<?php echo(base_url("index.php/vehicle_route_management_ctrl/edit_routedetails/".md5(trim($row['vh_rt_id']))."/".md5('delete')))?>" title="Remove Route" onclick="return confirm('Do you want to remove this route?')"><i class="fa fa-trash-o"></i></a>							    							    
							 </td>
						  </tr>
			              <?php endforeach; endif;?>
            		  </table>
				 </div>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
<script type="text/javascript">
function submit_form(clientid)
{
	//alert(clientid.value);
	document.getElementById("OnClientID").value=clientid.value;	
	document.getElementById("mainForm").submit();		
}
</script>
	<style>
	#Active{
	width:15px;
	height:15px;
	margin-right: -14px;
	}
	#GeoFence{
	width:15px;
	height:15px;
	margin-right: -14px;
	}
	</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="mainForm" action="<?php echo(base_url("index.php/vehicle_route_ctrl/route_name_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="OnClientID" name="OnClientID" style="display: none" value="-1"/>
					<input type="text" id="RouteID" style="display:none;"  name="RouteID" value="<?php echo((isset($routeID))? $routeID:null) ?>" />
					<h1>Route Details</h1>					
          			<div><?php if(isset($outcome)) echo $outcome;?></div>         			
          			</br>          			
					<div class="user-fields">
						<div class="field" <?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)?>>
	          				<label for="useremail" >Client:</label>
	          				<select id="ClientID"  name="ClientID" onchange="submit_form(this)">          				
	          				<?php if($clientList!=null):foreach ($clientList as $row):?>
	          				<?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?><!-- if client ID = 1 and if loged in is a Dealer(i.e. Autograde Client then dropdown allow to select different client)(If it is Dealer the client dropdown should have clients realted to that particular dealer) -->
	          				<option value="<?php echo $row['client_id']?>" <?php echo(($clientID==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
	          				<?php elseif($sessClientID==$row['client_id']): ?>
	          				<option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
	          				<?php endif;?>
	          				<?php endforeach; endif;?>
	          				</select>
	          			</div>
						<div class="field">
							<label>Route ID:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="10" id="RouteUserDefineID" name="RouteUserDefineID" value="<?php echo htmlentities($routeUserDefineID) ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('RouteUserDefineID'))echo form_error('RouteUserDefineID',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Route:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="50" id="RouteName" name="RouteName" value="<?php echo htmlentities($routeName) ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('RouteName'))echo form_error('RouteName',' ',' ');?>" />
						</div>
						<div class="login-actions" style="display:none;"> 
							<span class="login-checkbox">
								<input id="GeoFence"  name="GeoFence" type="checkbox" class="field login-checkbox" value="0" tabindex="2"  />
								<label class="choice" for="Field">GeoFence</label>
							</span>
						</div>
						<div class="login-actions"> 
							<span class="login-checkbox">
								<input id="Active" name="Active" type="checkbox" class="field login-checkbox" value="1" tabindex="2" <?php echo(($active==ACTIVE)? 'checked':'')?>>
								<label class="choice" for="Field">Active</label>
							</span>
						</div>
					</div>					
					<!-- /login-fields -->
					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($routeID==null)?"Save":"Update")?> </button>
            			<?php $reloadURL=base_url("index.php/vehicle_route_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>				
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						  <tr>
							 <th>Route</th>
							 <th>Total No. of stops</th>
							 <th>Action</th>
						  </tr>
			              <?php if($VhRouteList!=null):foreach ($VhRouteList as $row): ?> 
			              <tr>
							 <td><?php echo trim(htmlentities($row['route_name']))?></td>
							 <td><?php echo trim($row['stop_count'])?></td>
							 <td>
							    <a href="<?php echo(base_url("index.php/vehicle_route_ctrl/edit_routedetails/".md5(trim($row['route_id']))))?>" title="Edit the Route"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
							    <a href="<?php echo(base_url("index.php/vehicle_route_ctrl/map_view/".md5(trim($row['route_id']))))?>" title="Go to map"><i class="glyphicon glyphicon-globe"></i></a>							    
							 </td>
						  </tr>
			              <?php endforeach; endif;?>
            		  </table>
				 </div>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
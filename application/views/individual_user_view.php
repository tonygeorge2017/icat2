<script type="text/javascript">
$(function() {
	$( "#InstallDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });	
	$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
function onchange_editOrError()
{	
	if(document.getElementById('EditOrError').checked==true)
		document.getElementById('AddBtn').click();	
}
function dealer_change(dealerid)
{	
	var baseUrl=document.getElementById('URL').value;
	var deviceID=document.getElementById('PreDeviceID').value;
	var dealerID=dealerid.value;
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_deviceByDealer/"+dealerID+"/"+deviceID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	    	device_for_dealer(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function device_for_dealer(response)
{
	document.getElementById("DeviceID").innerHTML=null;
	var x=document.getElementById("DeviceID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var deviceName="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].device_id;
      deviceName=arrs[j].device_imei;
      adds_device(id,deviceName);    
    }
}
function adds_device(id,deviceName)
{
	var x=document.getElementById("DeviceID");
	var option=document.createElement("option");
	option.text=deviceName;
    option.value=id;
    x.add(option);
}
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Device Installation</h4>
        <?php if(isset($modeloutcome)) echo $modeloutcome;?><br />
      </div>      
      <div class="modal-body">      
        <form method="post" id="upload_file" action="<?php echo(base_url("index.php/individual_user_ctrl/device_installation"))?>" class="form-horizontal" >
         <!-- style="display: none;" -->
         <input type="text"  id="UserID" style="display: none;" name="UserID" value="<?php echo $userID ?>"/>         
         <input type="text" id="UserFullName" name="UserFullName" style="display: none;" value="<?php echo $userFullName?>"/>
         <input type="text" id="UserEmail" name="UserEmail" style="display: none;" value="<?php echo $userEmail?>"/>
         <input type="text" id="MobileNo" name="MobileNo" style="display: none;" value="<?php echo $mobileNo?>" />         
         <input type="text" id="UserDeviceID" name="UserDeviceID" style="display: none;" value="<?php echo $userDeviceID?>" />
        
         <input type="text" id="PreDeviceID" name="PreDeviceID" style="display: none;" value="<?php echo $deviceID?>" />          
         <input type="text"  id="VehicleID" name="VehicleID" style="display: none;" value="<?php echo $vehicleID ?>"/>
         <input type="text"  id="PreInstallDate" name="PreInstallDate" style="display: none;" value="<?php echo $installDate ?>"/>
        
          <div class="user-fields">
          <div class="field">
              <label for="password">Dealer:<span style="color:red;"> *</span></label>
              <select id="DealerID" name="DealerID" onchange="dealer_change(this)" >
              <option value=""><?php if(null!=form_error('DealerID'))echo form_error('DealerID',' ',' '); ?></option> 
               <?php if($dealerList!=null): foreach ($dealerList as $row):?>                
               	<option value="<?php echo $row['dealer_id']?>" <?php echo(($dealerID==$row['dealer_id'])?'selected':'')?>><?php echo $row['dealer_name']?> </option>
               <?php endforeach; endif;?> 
             </select>
            </div>  
            <div class="field">
              <label for="useremail">Device(IMEI No):<span style="color:red;"> *</span></label>
              <select id="DeviceID"  name="DeviceID">  
              <option value=""><?php if(null!=form_error('DeviceID'))echo form_error('DeviceID',' ',' '); ?></option>            
               <?php if($deviceList!=null): foreach ($deviceList as $row):?>
               		<option value="<?php echo $row['device_id']?>" <?php echo(($deviceID==$row['device_id'])?'selected':'')?>><?php echo $row['device_imei']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div class="field">
              <label for="Vehicle">Vehicle Reg No :<span style="color:red;"> *</span></label>
              <input type="text" id="VehicleRegNo" name="VehicleRegNo" value="<?php echo $vehicleRegNo?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('VehicleRegNo'))echo form_error('VehicleRegNo',' ',' '); ?>" maxlength="100"/>
            </div>
            <div class="field">
              <label for="Vehicle">Speed Limit(kmph):<span style="color:red;"> *</span></label>
              <input type="text" id="SpeedLimit" name="SpeedLimit" value="<?php echo $speedLimit?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('SpeedLimit'))echo form_error('SpeedLimit',' ',' '); ?>" maxlength="3"/>
            </div>  
            <?php            	
        	    $diff=0;
          		$time=gmdate('Y-m-d');
            	if($clientTimeDiff!=null)
            		$diff=$clientTimeDiff*60;
            	$current_date=new DateTime($time);
            	$current_date=$current_date->modify($diff." minutes");
         	   if($installDate==null)
         	   {             	 
         		   	$install_Date=new DateTime($time);            	
         		   	$install_Date=$install_Date->modify($diff." minutes");
         	   }   
         	   else
         	   {           	
           		 	$sdates=date_create_from_format("Y-m-d",$installDate);
         		   	$sdate_now=date_format($sdates,"Y-m-d");
            		$install_Date=new DateTime($sdate_now);
          	    }        
			?>
		<div class="field">
			<label>Date:<span style="color:red;"> *</span></label>
			<input style="font-size:13px;" maxlength="10" id="InstallDate" name="InstallDate" class="form-control input-lg" value="<?php echo $install_Date->format('Y-m-d'); ?>" placeholder="<?php echo((null!=form_error('InstallDate'))?form_error('InstallDate',' ',' '):'yyyy-mm-dd');?>" />
		</div>                 
          </div>
          <div class="clearfix"></div>
       
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="<?php echo(($userDeviceID==null)?"ADD":"Update")?>" />
       <!--  <button type="reset" class="btn btn-primary">Reset</button> -->
      </div>
       </form>
      </div>
    </div>
  </div>
</div>

<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <!-- style="display: none;" -->        
       <form id="myform" action="<?php echo(base_url("index.php/individual_user_ctrl/user_validation/"))?>"	method="post" class="form-horizontal">
       <input id="EditOrError"  name="EditOrError" style="display: none;" type="checkbox" class="field login-checkbox" value="1" <?php echo(($editOrError == ACTIVE)? 'checked':'')?> onchange="onchange_editOrError()" />
        <input type="text"  id="UserID" style="display: none;" name="UserID" value="<?php echo $userID ?>"/>
        <input type="text"  id="URL" style="display: none;" name="URL" value="<?php echo base_url("index.php/individual_user_ctrl/") ?>"/>        
          <h1>Individual User Management</h1>
          <?php if(isset($outcome)) echo $outcome;?><br />
          <div class="user-fields">
          <div class="field">
              <label for="primaryrole">User Name :<span style="color:red;"> *</span></label>
              <input type="text" id="UserFullName" name="UserFullName" value="<?php echo $userFullName?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('UserFullName'))echo form_error('UserFullName',' ',' '); ?>" maxlength="100"/>
            </div>  
            <div class="field">
              <label for="employee">Login E-mail :<span style="color:red;"> *</span></label>
              <input type="text" id="UserEmail" name="UserEmail" value="<?php echo $userEmail?>" class="form-control input-lg"  placeholder="<?php if(null!=form_error('UserEmail'))echo form_error('UserEmail',' ',' '); ?>" maxlength="50"/>
            </div>
             <div class="field">
              <label for="employee">Mobile No :<span style="color:red;"> *</span></label>
              <input type="text" id="MobileNo" name="MobileNo" value="<?php echo $mobileNo?>" class="form-control input-lg"  placeholder="<?php if(null!=form_error('MobileNo'))echo form_error('MobileNo',' ',' '); ?>" maxlength="10"/>
            </div>
            <div class="field">
              <label for="useremail">Time Zone:<span style="color:red;"> *</span></label>
              <select id="TimeZoneID"  name="TimeZoneID">              
              <option value=""><?php if(null!=form_error('TimeZoneID'))echo form_error('TimeZoneID',' ',' '); ?></option>
               <?php if($timeZoneList!=null):  foreach ($timeZoneList as $row):?>
               		<option value="<?php echo $row['time_zone_id']?>" <?php echo(($timeZoneID==$row['time_zone_id'])?'selected':'')?>><?php echo $row['time_zone_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
          </div>
          <!-- /login-fields -->
           <div class="login-actions"> <span class="login-checkbox">
            <input id="Active" name="Active" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==ACTIVE)? 'checked':'')?>>
            <label class="choice" for="Field">Active</label>
            </span> </div>
          <div class="field">
            <button name="submitBtn" class="btn btn-primary" type="submit" value="1"><?php echo(($userID==null)?"Save":"Update")?></button>
            <?php $reloadURL=base_url("index.php/individual_user_ctrl/")?>	<!-- '$reloadURL' created for onclick of cancel btn-->																	
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>	
          </form>
 		
 		 <?php if($userID!=null):?>
 		 <?php $reloadURL=base_url("index.php/individual_user_ctrl/add_device/".md5($userID))?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
         <div class="pull-right">
         	<button class="btn btn-primary" type="button" onclick="self.location='<?php echo $reloadURL?>'">ADD Device</button>
           	 <button style="display: none;" id="AddBtn" type="button" title="Add device" class="btn btn-primary" data-toggle="modal" data-target="#myModal">ADD Device</button>
         </div>
          <?php echo "<br><br>"?>
         <h6 class="linkred">Device Details</h6>
          <div class="table-responsive">          	            
            <table width="100%" class="user-dts">
              <tr>
                <th>Device</th>
                <th>Vh. Reg. No.</th>
                <th>Action</th>
              </tr>
              <?php if($userDeviceList!=null): foreach ($userDeviceList as $row):?>                
              <tr>
              	<td><?php echo $row['device_imeino']?></td>
                <td><?php echo $row['vehicle_name']?></td>
               <td><a href="<?php echo(base_url("index.php/individual_user_ctrl/edit_deviceManagement/".md5($userID)."/".md5($row['user_dev_id'])))?>" title="Edit Device Details"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php endforeach; endif; ?>                       
            </table>  
                     
          </div>
            
         <nav class="pull-right">
			<ul class="pagination pagination-sm">
					<?php echo $devicePageLink;?>
		   </ul>	
		 </nav>
		 <?php echo "<br><br><br><br>"?>
		 <?php endif;?>
	<!-- --------------------------------------------------------------------------------- -->	 
		    <div class="table-responsive">
            <h6 class="linkred">Existing Users</h6>
            <table width="100%" class="user-dts">
              <tr>
                <th>User</th>
                <th>Email</th>                
                <th>Actions</th>
              </tr>
              <?php if($userList!=null): foreach ($userList as $row):?>                
              <tr>
              	<td><?php echo $row['user_user_name']?></td>
                <td><?php echo $row['user_email']?></td>                
               <td><a href="<?php echo(base_url("index.php/individual_user_ctrl/edit_userManagement/".md5($row['user_id'])))?>" title = "Edit User"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php endforeach; endif; ?>              
            </table>
          </div>
          <nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $userPageLink;?>
			</ul>
		 </nav>		 
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {	
 	$("#givenDate").datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd'});
 	$("#givenDate").on("change",function(){ 	 	
 	 	$("#givenDateTime").val($("#givenDate").val());
 	 	});	
});
function submit_form(vale)
{
	document.getElementById("CheckID").value=vale.value;
	document.getElementById("myform").submit();
}
</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <!-- style="display: none;" -->
        <form id="myform" action="<?php echo(base_url("index.php/device_allocation_ctrl/device_allocation_validation/"))?>"	method="post" class="form-horizontal">
        <input type="text"  id="IsEditing" style="display: none;" name="IsEditing" value="<?php echo $editing ?>"/>
        <input type="text"  id="CheckID" style="display: none;" name="CheckID" value=""/>        
        <input type="text"  id="DeviceCount" style="display: none;" name="DeviceCount" value="<?php echo count($availableDeviceList)?>>"/>
          <h1>Device Allocation To Client</h1>
          <?php if(isset($outcome)) echo $outcome;?>
          <br/>
          <div class="user-fields">
           <div class="field">
              <label for="password">Dealer:<span style="color:red;"> *</span></label>
              <select id="DealerID" name="DealerID" onchange="submit_form(this)">
               <?php if($dealerList!=null): foreach ($dealerList as $row):?>
                <?php if($editing==ACTIVE && $dealerID==md5($row['dealer_id'])):?>
               	<option value="<?php echo $row['dealer_id']?>"><?php echo $row['dealer_name']?> </option>
               	<?php elseif($editing==NOT_ACTIVE):?>
               	<option value="<?php echo $row['dealer_id']?>" <?php echo(($dealerID==md5($row['dealer_id']))?'selected':'')?>><?php echo $row['dealer_name']?> </option>
               <?php endif; endforeach; endif;?> 
             </select>
            </div>
            <?php $isDeviceInstalled=false?>
            <div class="field">
              <label for="confirm password">Available Device(s):<span <?php echo(($availableDeviceList==null)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <fieldset>
              <?php if($availableDeviceList!=null): foreach ($availableDeviceList as $row):?>
              <?php 
              $checked=false;
              $function=null;
              		if($selectedDevice!=null)
              		{
              			$checked=false;
              			foreach ($selectedDevice as $selected)
              			{
              				if($row['device_id']==$selected['device_id'])
              				{
              					$checked=true;
              					if(isset($selected['dev_install_vehicle_id']))
              					{
              						$isDeviceInstalled=true;
              						$function=($selected['dev_install_vehicle_id']!=null)?'onclick="return false"':"";
              					}
              					break; 
              				}
              			}
              		}
                ?>              
              <input name="deviceID[]" type="checkbox" value="<?php echo $row['device_id']?>" <?php echo($checked?'checked ':''); echo $function ?>/>
              <label><?php echo $row['device_imei']." (".$row['device_slno'].")"; echo(($function!=null)?'<style="color:red;">* (assigned)</style>':'');?></label>
              <?php endforeach; endif;?>
              </fieldset>
            </div>
            
            <?php
            if($givenDate!=null)
            {
            	$dates=date_create_from_format("Y-m-d",$givenDate);
            	$date_now=date_format($dates,"Y-m-d");
            	$date_value=new DateTime($date_now);
            	$client_time=$date_value->format('Y-m-d');
            }
            else {            	
            	
            	$client_time=$currDate->format('Y-m-d');
            }
			?> 
			<div class="field">
				<label>Date:<span style="color:red;"> *</span></label>
				<input style="font-size:13px;" maxlength="10" id="givenDate" name="givenDate" class="form-control input-lg" value="<?php echo $givenDate; ?>" placeholder="<?php echo((null!=form_error('InstallDate'))?form_error('InstallDate',' ',' '):'yyyy-mm-dd');?>" <?php echo(($isDeviceInstalled)?'disabled':'');?>/>
				<input style="display: none;"  id="givenDateTime" name="givenDateTime" class="form-control input-lg" value="<?php echo $givenDate; ?>"/>
			</div>            
            <div class="field">
            <?php //echo $editing.','.$clientID ?>
              <label for="useremail">Client:<span style="color:red;"> *</span></label>
              <select id="ClientID"  name="ClientID" >   
               <?php if($editing==NOT_ACTIVE) if(null!=form_error('ClientID'))echo '<option value="">'.form_error('ClientID',' ',' ').'</option>'; else echo '<option value=""></option>'; ?>
               <?php if($clientList!=null): foreach ($clientList as $row):?>
                <?php if($editing==ACTIVE && $clientID==$row['client_id']):?>
               	<option value="<?php echo $row['client_id']?>"><?php echo $row['client_name']?> </option>
               	<?php elseif($editing==NOT_ACTIVE):?>
               	<option value="<?php echo $row['client_id']?>" <?php echo(($row['client_id']==$clientID)?'selected':'')?>><?php echo $row['client_name']?> </option>
               <?php endif; endforeach; endif;?>
              </select>
            </div>             
          </div> 
          <div class="field">
            <button class="btn btn-primary" type="submit"><?php echo(($editing==NOT_ACTIVE)?"Save":"Update")?></button>
            <?php $reloadURL=base_url("index.php/device_allocation_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>	
          </form>
 		<form method="post" action="<?php echo(base_url("index.php/device_allocation_ctrl/edit_deviceAllocation/"))?>">
          <div class="table-responsive">
            <h6 class="linkred">Allocated Device Details</h6>
            <table width="100%" class="user-dts">
              <tr>
                <th>Client</th>
                <th>Date</th>
                <th>IMEI No</th>
                <th>Actions</th>
              </tr>
              <?php if($deviceDetailList!=null): foreach ($deviceDetailList as $row):?>                
              <tr>
              	<td><?php echo $row['client_name']?></td>
                <td><?php echo $row['device_release_client_date']?></td>
                <td><?php echo $row['device_imei']?></td>
               <td><a href="<?php echo(base_url("index.php/device_allocation_ctrl/edit_deviceAllocation/".md5($row['device_dealer_id'])."/".md5($row['device_client_id'])."/".md5($row['device_release_client_date'])."/".md5('edit')))?>" ><i title="Edit device(s)"  class="fa fa-pencil-square-o"></i></a>&nbsp;
               		<!-- <a href="<?php //echo(base_url("index.php/device_allocation_ctrl/edit_deviceAllocation/".md5($row['device_dealer_id'])."/".md5($row['device_client_id'])."/".md5($row['device_release_client_date'])."/".md5('delete')))?>" onclick="return confirm('Do you want to remove all the device from the client?')"><i title="Remove all device(s)" class="fa fa-trash-o"></i></a></td> --> 
              </tr>
              <?php endforeach; endif; ?>              
            </table>
          </div>
          </form>
          <nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $pageLink;?>
			</ul>
		 </nav>
          <div class="clearfix"></div>
      </div>
      <!-- /content --> 
    </div>
  </div>
</div>
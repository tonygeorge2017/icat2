<script src="//maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&key=<?php echo MAP_API_KEY; ?>"></script>
<script src="<?php echo base_url('assets/js/pdfmake.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/vfs_fonts.js')?>"></script>
<script type="text/javascript">
$(function(){
	//var d = new Date(1234567890000);
	//alert(d);
	var clientID=0;
	var vehicleGp=0;
	var vehicleList=[];	
	var vhnmae='';
	$( "#f_date" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });	
	
	
	var reportPDF = { content:[ 
	{		
		table: {
			widths:['80%','20%'],
			body: [
				[{text:'',style:'client'},{rowSpan: 4, image: 'logo',width: 150, alignment: 'right'}],
				[{text:'Date : '},''],
				[{text:'Vehicle Group : '},''],
				[{text:'Vehicle : --All--'},'']
			]
		},layout: 'noBorders'
	},
	{
		style: 'tableExample',
		table: {
				headerRows: 1,
				widths: ['10%', '30%', '30%','30%'],								
				body: [
						[{ text: 'Sl. No.', style: 'tableHeader' }, { text: 'Vehicle', style: 'tableHeader'}, { text: 'Working Hrs', style: 'tableHeader' },{ text: 'Total Distance', style: 'tableHeader' }]										
				]
		},
		layout: 'lightHorizontalLines'
	}
	],
	pageSize: 'A4',	
	images: {
		logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAABcCAYAAAAF6ttZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2xpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo5M2VkMjQ2Ny0yOWIyLWYxNDYtOWY3Ny01NDI0NmYyYWVmMDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDlFMDA3NUUwQkRFMTFFNDgxNEQ5REM1NjQ5QTgzMjQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDlFMDA3NUQwQkRFMTFFNDgxNEQ5REM1NjQ5QTgzMjQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MUQyMzIyOERCNTA4RTQxMUE5NTlCNEM0QzEwNERDMzUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTNlZDI0NjctMjliMi1mMTQ2LTlmNzctNTQyNDZmMmFlZjA4Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+4amNgwAAXPpJREFUeNrsXQdYk2fXvrMTwp4iioILF+LCPXBvrbPu1s9Wq7ZqtdVOrW3tsLt1dLda994bN+6tKAoKKLL3DiT5z3kSLCoksV+1fv19r+u5xLzJ+z7rnHOf+chzcvLw//2SyWTIzs7G6OGDkJOTi1XrNsPFxQXFxcV/6XlKpQqR1yMwesQQGAwGyOVyLFm2GlX9/KHT6Z648dvZaTDl5QkI3bub/raz+n0eg3+16li8dBUkUimMNMYn7VIqlYiKikT/Pt0x8eUp1F5Bbm7+Qz9HIpFIVWqV6/Ah/TOvXrlSpNFokJObh0tXrpX+Wktq46m1olaJWgG1q9RWUvuVWnrJF6V4ej29nl6lLwdqralNobZUo1GfPnrk8KGE+Pg3iHHaSSUS5OXdJVwltfnUjlAbQO00tc+pLaGmNf99hlrbkh/In87v0+vpBXeSZT0UCnlvpVLeqqiouEJBQaFEa6/F+fPn8MbrrxpycnJukPTPS8/Iwo2Y2yW/+4naSGqrqL1ILfO+5w6h9jO1rdS6UDv6lOCeXv+fr/pSqXS8Wq0apNfrPW5G38LRY8cReyMCL7w4HgUF+Zj99hvIysx8TWNn90dBQQFib8fRz4z82xFmYttCbRS1iWZoyZKtiNpuat9Sy6W2mdo3LDmfEtzT6//jVZkI7V2NRjUqIzNbuW3nbuzcvg1pMedQSZWAPs+/Da8Knpj52jRcvx7xi729wxcymRQ3YxOR+yecfMX87+tm1exVavbU/qDW1ExgValNM+tyLO26PCW4pxdILWHjQAnn/jdcrFuFKOTyMBpW9n33hpFE+0qnK/L4Y9kqrF6xFA6F19GrgQohg6Twqkd00WwMVq5Yga1bNl3Sau2n89wUFuqQmZlV8oxq1BpTO07tCjUnagpq4dQmUatlNprUMn9/vZngQp4S3L/34k1Xlzh5kJnzlmlKNBgMEqMRhKj024jsYmx5sJF+wPCK/5U+eWTKG/8TmVw2JTY2ZmteXt6YoqKiJPpMRoTzuZ2devL585fw6aefIj/uBF7q4ICO9StAISuGXuoCfd0puHnjFhbN/8agUCim0m/SZVIpkjIyUagrKqXziaFfLvXeQp5vs8EkkFoitTnme1fN/1Z5SnD/vktGbbBKpZwmk8ka5eYXSHQGfdnfNBhgr9IgKjISFy+eP6pSqTrRpxb9ROzmKC4uwjP9B0KlVkNXWPgkjb0iGymISLoVF+mxdMnvPT08vEI6duqyUq83rtDYqQeuWr0e33zxEQYGFmD8pIpQqyUo0hlQmKeDsdFzkGq98PVn45GcnPSlVqvdI8ZMjIWNJaWuEt+O0/3TQ01jhpI8j6nmzx3M/+Y+Jbh/11WRiOwXpVLZ9fyli9i0cxtunDkNOUEhqYCMf17FOh18/P3x3sIfceDAPiQnJV2yt7e3Smz5eXmY/OrrGPviOOTlFTxJYx9M4/5Cp9P5aDRqHD50CEcOHwyf8cbbe+sH1l9NBDfw66/nY/0f3+GD/o5oH+hJc0CEVkjyuTgPRp8OUNd8BiuWL8f+fXsu2NnZzTbBbQny8wuQnZNb+l2x1JKpNaOmoqY3/8uSbCC1vtQ2UBtD7S2zTsfXhacE9y8yBBAE2lpUpKv/0RdfIXTFMrROTMMogxSVjCaxVxr6GbNz4NC6A3QEjPaH7gHpO0utERvBM0x59TW8MM5EbAwpn4Crvlwu/0ClUvQ5eeIkiGmgeo2aWPDtV2jSNHjeqOfGvEXdHPjFl19j15qF+H6MJwJ8lSgsMCNsQxGMGi8oGk1F5I1YfL/gW71CoWQomcO3mVEVEnMiyF36nSy52Dr5PLVnzYaSEnTB1xGzhGsDk9XyBZgslxufEty/41KQZPuVuHv9Ge++iTvr1uFjowI1JGoU0RYokjwIJeHqCvcB/XH+4iVcvRJ+QalShlkiNjaRT532p2R7AoitNo15slqtfC41NV311ZeLUETw9q1Zs/Hzjz/gSvilves276gtlUqmLCApvmP1QvzwH09U82ZiM/fdaDCxofovQ6f0xOcfj0VKStI8OzttqA3v/4Raf5iskbdgcm6XEFwKtSCzrvcd95XaZ9SuP400+Xdc4whOdfzs2y+RSMT2hVENP6MMOURohRKTYlG66WljShsEQlGvNg7s2s4wcZNEIi0q7+EcytW33wCMGfsiEVvhP0lsvKHbkURbqdVqzup0ReOWrV2rGjmwD2IvXMD0mW/izKlTbPBImjVn7i1/f78pq9dswPJfvsLnI9xRrSIRm65U34vzYfTrD7VfZ/zx6/c4fHD/cSK2923sSwS156ipqW03E1+O2UDlZia2d8zf2UTtXf7RUwn3v3+p6Xrp+KmTOLh8Gb40qmBPe6pAYuEXBI/UPbohXVeMw/v36RVK5bp79LviYmEYUas1pOMUolmzFnhn9gdEeAS/jP9I3GQ1qVTan/o5VC6TBN1JSJJsD92FbevWQHLwCMZ16Iy+P/6MmwnxmDxpfGG//gPPPzNgwDP7DxxSfvnpe5g7wAFB/po/YaQYJOltHk2gbjwZYWHH8NP3C3JUavVEa0aj+y7W01qapdhcc8uAyUJsZ34WE9rHZkj5lOD+ly5W4MuQLq0JNtXZtGMrOqZnowrByBwrxCZxd4N9j+44dOIYoqNvHCWd5WzJ7fz8PFSs6INGjYOxZ/cO8c7xE18G6Yd0L/9xDZWRVw0isi703mfkcmmLvPxC9clzZ7Fjz06c2bMbnhHXMTynAO1GjILbpx/hRmI8xj03sqhxk+Dzb787u8mli5ed3n5zJqZ2lKF9fe19xEaQWFsZiubv4tadZMyd8w4zlmkqler0/R3h2Want4WLf8Oxl03M/7KPjk2a56gdoBZf+stPCe5/6CoiyaOQy+8hOplMPigxORlXjhzG67Sc1nIRjCSxlCHtIPP1wb4f5qOoqHglZzcIYsvLQ7XqNfAN6TwZaanYumUDP5+ITipM44/4cmFrnlQma6dUKDrLZJLA3LwC1cWr4Th4NAwnDu5HPsHGQGIqr+cUIsCFmMZH70EzbgyOE4ycPml8foNGjU98/d2CmlFRN12mTJ2KoUG5GNTaHbrCUsSmJ2LTeEDW4n1kG50x+60XEBNz8zuCkj+Up7/aa7VgyydbK8tjYzA5wY9bG+RTgvsfuTgtZPPG9ahesyZq1KhFhCIQiqtKpegRRnDS6UY0qklkDxpI7mfXjHX69EZCVg6OHw3LIK6+hZ/F0mvY8FF49bUZ0GrtcCuGfeCmhzGBS/7+IXlQqyuTydqSTtaCpFhjvQEeqenpCL8egRMnj+PSiRPICb+MainpGGiQokGxEU4FxFLat4P9B+9BUTcAfyz9A19+MjetZ+9+J9/7YG7DyOtRni+Ofwnd/RIwsZsHiojY7vIK0tmg8YS0xcfQOwXgw9en4vixI1u0WvtXLTIpfsDfxHCeEtz/yCUlVHP7ViySkpJQt27dEoLraDAYK4Xu3YWW+UVQSFUosvQQ0suklSpB26kj9u4PRXx83D4iqmh3d3e8P/dTdO7STdAkbXxI/l4KYxHqS/C0IUmwpkRgwaSLBej1Bs/M7BzcpHFdCL+I8ySp4onAVDdjUCMnD0P0EgSQhHU2KmAg6Vvs4QHpu5PgMn4s7qSm49NXJiF0984Lsz/4+HL/AQO6X716zfnlSZPQvlI8pvX2JD3USJK5ZOy5MDr4QdbsPRQ71sKH78zE9m2bDhOxjS7Rrx7H9ZTg/ocuuVKJg0QoY14YJ/5P+s2QmLjbuEWS4AUJwUmJdTipatMKBhdH7Nm+FZkZGSu69+iN2R98BK8KXijILxQQimHrf2PEoeZNxFWHw8pIggXJ5bIG9FnlomK9Ojk1FTeIwMKvXMbVyxeRdPUqpLG34ZOegYZEIMNJffORyukhSuhlEsFY8ohRKHv1gOu7b0NWrSo2bNyIeXPn5Lp7eGxevmaDjBjQgIMHw5RvvTUTA+tmYXJPE7HphVm2mM2yMFZoBWXwW8gs0uD96S9j987tRwhG9qNvpD3WNXzSGbt5AR/lVWjG4IK7s5FAylnMJs4owV9K0jUSMcgN9KzycIjsr3TUzs7OcOHCOWPk9Wvw96/mq1DIOh44cgg+cfHwkShQYBkXQaJQwK5fH9y8HYcr4ZfvvPXOnMPDRo5SqNUaRd692dASkkI8L9ZS3jlu0ZdaABFWQyYuanVp+nx1RXq7zJwcxMXfQWT0DVwlyXUrIgI50TehvJOASvS+pjo9/KUy+NB02EkVkEglAhLrzeY9I0k1KUk1hzdeh/PIoYiMvYV5Y5/HoYP7T/5n7Pj94yZMam9vr23640+/4/cfv8LLITIMae2GIp1RMA7oaUwqVxjrjIe67jBci7yJ99+dgLNnTq0nYhtN65P92JnmE05wPWjzL1GrVY8mhohoiyBZor5Yv81oNHxJi5TI5vCc7Gw4OzuzQa8NvX+RRqMqgo0xumw6p02njIyMvKDT6ebSxr3Ai2+4i20QSAu9mJRwDmKwycZeUCBMIYqkxIS91LdPSDLFyeSyXgWFRc4HQ/egq85Am9UKC9DpIK9TG5qWzeFLEmzFmo1ubm6uJ6VSiZSZC5dZENynUMfPUhJRX6A5mVZYWHxGf28sJkPDXiRdO5gll0+xwahJy0hHQnIyomNjcO3aVdyKjETqtQhIkpLhmZaJKjQvfYwSVCE905WITE1bzyBXCOIqJiK73/5pJGKVN2kCl68/hyygBn759Rcs+varjOo1au5YvnqDvkGDBuNux91xnP76m7h1YSe+etYVTWqooSMpbdTTcqmcYKzUGcq6z0OvrYi1a9dh/tef6VNSUj4mGDmrhMk+JbjSYkAmG5GRkeG8au1OFNKmlf7NigUrw75V/TybNWtRX6PVDnJ2dhlBG+3oKVLY/av5cTjPyPy8vNqhB/cjR19s9f3FBH+CagagZvWaWLN6RW36/Woitgv16geiUuXKwr9Fm/XZ4iJdg10kmTKKdFafaWSqb9RUmOV37tjmV8G74nw30rlIGAyMiLyO9LPn0IikmzU4KdJvNBqsX78OqTQWpUymov5UvLf/OoS0bAPfKlWxcf3a9vn5+e4dO3VFTRoTMQ9/Yj5vqTWqIYUFOu21mzcQTgR1IyoSsRFXkUkwURJ3B27ZufClTR9CrMSXCMuDpJeGRJ6c9EsavSAuAyw7uzjsTNm7B9znf4O43Bx8OHYMIq6GJ7zx9uxzz/Tv30KnK67y++JlWPzLQrSokIiPxrnCxd6AwgI9jPZVIfFuCYV/b0gcK+P8hXD88v0cHNgfek0mk0/SaDS7/1G14Ammt4pqtbLzTuLgq6dOIe7IOoqkvN1EI5GbLAsPR3G4Lpcio0dP9P/6a/+hw0dtPH3yRLu0tDTOcXIjydZ91749WDh+HG0gKfSWiIOeVUzEEbh6NSJlUQi/dPGGSqXamZeXi9Zt2kKr1XARGxUteL9DZ09j3vgX0bGgWPS5PNFpJAKVubmi4569OHHqBG5GRYV279k7wt/fvz7dbh166ABqJafCXaKy6q2VkP5XeP4CcujdOqLWYpgtb8Y/CVviYA/HnR1x+sxphIdfOuHlVWH35KnT4ODgMIaY0xc5OTlOa7dswq5tm5EXHg6f5DT4F+nRTW8kSCuDGxGYPaNwIi6j1ERcLEYK6T3MMMX72KdlQRwbc3KJ2HrC84f5uE766beffYpatQIw58OPHVzd3Lps37ZN+tsvP8GQFo53e3mjXcMmKFZXRqFjTcgrNIHMvR7pfDKcOXMGm9Z9w3GiKTk52V9qNHbzielk/uN6+JNKbTQ53Qt1xa6RkdcwpnYgepK6UGb0BDuDSbIYSC8xEjeUEBd/KBMbadZZy1Yjt10IGnUK8fD08ppC3HRccbGhs0Qq9dm1ZzcGGmQYJVEizxK9FRZAHhgEh5YtMX/RfE7L32yn1eZwtEaLlq1JWoqd3Ya6VnvHXoaBekyWqq08UwdV+87QVPDEjm1bWOKubN+hIw+vf3ZOnuLYvr0iONlgw3D57XKZDL2YKRUQQi8mAmMYqVCY7ufmQdWxK+xJsn/z40KuYrb01ekzjbXr1HmT6OTDE6dO4pPPPobi9Fk8U2hEE4kcrkRkUmrFcokgLMP90JBgLENZiUoFEDFLHRygJulVmJ9fJvMyUr/kDQLh/t2XuEwS9Kf53+KNt2ehYkVvvq1NT88U7pEJEychuH5FaFzciLCp0Rzk5RURQ7qOU+t+xuGDoQi/fCm8oKCAoLvmd9LXEp4Yw9eTSnAqtWoU6Rd4c/obUL/2pvhMWZ50MxKcuBmNzEU/oGjVWkgVpB/YKu1oE/JGNN64AbUdkbVCWb9R46YkMKUDI6NjcPv4MYyXypEmkVi0ILA0su/RDdnUHdarCAKuZkdyl249ENigoXCakj43OD4pCVcPH8QMmvp0iaR8vxlLA5JKqv79cDM+AQRz71T0qbS1aXBzvtv/zKULMFwOR30brJOlCSCPJJm8cWNoenaHpkUzyL28TO8iCSTzdEfcnXjs27MzrUOHzktHjHqe00w+3L0vFO/NfA1DElIxiBifitalsAy9608mRtCOxiur5g91396wC2kHAxGNUWOHtdNfRa09B1BFqbp3Pun9TJiOc+cgk9bvzVcno5Kv7wKaopbHjx8PkpH0ZGOWp6eXgOZHziciNeUS4u/cQmz0DWNUVGRGQvyd67m5OftpnrcrlaqjWq228Enb108kwclJmd68cUP8eYI/LCGMkvItFmyQcHBwRMcevRDwzee45OkBzTfzYU+c0NaoPyMRnKJaNaRnZdJeKTa4u3tweE7IftKzKt9JhDesWAB5szg7w75XT5w8fQqkB55UKJXHGGK2btvOjHSNbkp2UpN+6HQzBn5WnNRGNr7UqAG71q1wcPkftLHitg8Y9Gyad0UfjkoPZD9aUGYu7G2AkyUSTFrFV1j8HPv3RSb17eylS7hz9qSYQ9bxlDI5Th0NY+m2ZfR/XkgnpvNB+NUIzH3nDbyUlI7exPJyJRb0L0YbhYUma+i0KXCeOB46JwdcDr8Cf9I7k1NTsOzGNXxslqr39I+Yk2r4UGibN8WCTz7ChQvnNjZo2Gji+7PfaXHk0MHnVGo1+/IMZt1bQtI+iwgvidYrhnT9GCKy6zKZIkGrtX+yXTtPIJQUG+C3n394KTz84nvEqaxZk+jrBsfff/6hwedfL3it3ozptbccPoAu50gNUymtv5C4JZueNW3b4Mr5MwzbIpoEB3coKja4Htq7G72KDFYdA8aCQihbNIe8pj9C5yxBYWHBWmIUeg96bnCzFvT/IgGR9cVGn70EUVvZ4qQmaaTq3AH5Shn27d4BpUK5ctDgZ0kaSwYkpaTh0sEDmAoZim0kNnnzYLgu+AYG30pYumwp1q1cro+JvnmssLBwL/UtpcT9Qf9XkM60pnHjpt3pv/V+XvIb2sbEow/B32yJZVOtIDZHBzh99QXsu3fGgcOHseDLzwxGgyFhyfI13rtD90jqxCXAlxhY3v0MS6uFw3OjEBETiw3r1qQ72DvMSE9Lw63Y2KMyuexo6TebVHY5u16ARxED8/8OUrKyrdenk2hLJ2XXuhpG3LpyZd8zdhrNFXeFPCytVQtJ9NmL8DcqheIOK5tE2bIFJN6kJ72/ATVq1jrq4eHe/+yly8g6dx4NpdYtgLxhNKTop+Tl41jYoTxiEhs4f6xN2/akf1QUhUMJqg6+ERuNuBPHMdHaM3kD2mth36c3wkk6nD939nKtgNqhtQLq8GT0On72FDRRN1DdWiiXWS+S1akNt19+QKpMijkvjsWhg/uOE4p4TalUHiLYdfe7DIG9vCoIvUmr1XwUQ3rxtUMH8Z5EgXwrxCaCoklyOX75GdREbN998xUWfvd1PKGPSb8uWVFXrlDMObxnF4bSghikkgddFo0aQtOoAXZ8/RUyMjIW29vbR5w+dUJYkmnu8G+5nkiC49LZvfo+gx69+6J9SMf7s23LhJW+vlU42zfGYCTVwtHRLoYWtSaseG5ZdyG8x87guNQ0nDx+LHbCy5M5db797v17UTslHa6wAtlYQlasAG3XLjh8+BBu3bp1QKVSRXAaS7cevUsc6NWVSllIKOlufgKiKpFvhQkomgVDFVQf+z7/DBnp6RuGDhuhd3Jy6KI3wD80dC9a5JEEtAYnmXDVajh9Ohc5jvaY8Z/nWBf8mYkA+BMl8/xlZmagU5fueOOtWfDx8WErRadDx8LgHRePykTY1pQhJmy7VybBoUdXfEdE8+1Xn593cXUd8N4Hn0TVqVN7ZtjJUyi6eAmBpIs9wGxoDlXNmyGH5urk0SMsuZaWMFKJRIJ/0/XEEVxJJDwX4uTJ1utt08RYkSY41Jiwvt2tmGhUktlgNCkqhqxqFWjbtcWW7VuQX5C3g3SuenkFOs3JfaEYa2RXgA1wslVLwNMNoTu28eZdyeFIfn7VSMK1E/lkNI4++QU6+7B9e9GXOIDRWteK9VD36oH0Ij0OHdinV2s0a1u1EdWyB92+cwdRpGeNhA2hXCRZ1UOHwI70oq8+mINTJ45vcHB0HFsavnPQMhunXp0+UwQva+3t+bMepIO67yNI3U6nh1RiZZvQeFk/dJo0AYePHsWvP/+QoFAqhrw4fmJUt+7dOGWl6a7Q3QjMyIYTMbDcMh6hrFNHxInGxsZcJel74a7v8F92PbFWSnMqBOOdllKptBE1R5RR6s1oqvEmpY2eRMpzm4TkJMRcOI/htEms6TemVJX2KHaww9YN69Cte+/d/v7+r+0l3UN65Srq2mIBJMLWkoS8nZSM0yePJ5N028G1P1q2bgM7OzX73iQqlXrw2UsXkMMQ1dozmat7uIt8tYMkYa6EXz7Wuk27s02DW7DU6Xrg2BFUiL1NUkdhORWHQ7lIh7Uf/qzQi7Zu2pBip9VOK01subm5qFK1Kia+PBXde/QQES1c/o6g5uBrN6KQfPoMgm1wqjPTUXfpDLg6YemvPyE7O2tWs+YtI0aOfp5U0eLBmdnZOE865xRjGRCYIAm7chQ+FZF4J47fH0F9K8S/9HpSCU5Fk/4ibd4p9K9/GsEdjsu737/GspDtXRU8PHHlSjhDIRw/cwrO0bcEDLKov/GGVKugJd0rKjoGVy5fvDz6+TGMXYP37tuDJtl5sJOoLUM20j1k/n7Qtm2LzZs3ICUleQfpnIl8Ak2btiEwI+FmcrmkGUPUOqkZcLYCAxmaqTp3hKxyRYQu/AaFBQUr27ZrD5VK0UtXpHc7KKSOQVg+LRswSC8KrAdN82Ac+GER+wWXkfS6UXKfnovq1Wvg24U/oFKlSqVPlglQKGTCqV49KQUeVuCvmEciGPt+fRFBeuX5s2eiSEItb0UMR6lUOBNa7c1ror0ehZpl6ZyMaFQqyJwckZEQhyKd7jpJVzwluMd3+ctk8sWE41sdP30KazeuRdyFC3DOyYUakns2mY7gUL2QEIx57wPSc9JQp05d7N21Ey0KrFsBhaJeOwCaFs2xd9F8aB0cNgU3a9mGLYDhhw5hJk2NtZwN3tTKjh2gs1MhdNd2TtRcxRCNJBKaBjcT0kIqlQ1Iy8jC6f378JI1iMqbj5iKpk9v3EnPxLEjhzM9PD03tmwl4KSQOilnztokdWjnQk1SkrO/9+/ZBblCsbI0KuCE0kmvvHo/sbGvq09uXoHdMYLUg22Av4wS2OChbtYY++d/x4S9gRhONjOc4mJjR4KrVXfT+1vkFUIlLYOBSXA314z7ZXzwQIynBPcoiU2hUOygjVrjw88/xpFVK9A5LRuDDRyTJ3nAAMy+G7dpzZBIxObk5IyElGTcPnkCL9kCBYXZvRPyZBLs2r4VLVu2PuHoaP/Jnq1b4Wz2kxVbsyTaEWfv2wfXo6Jw8cIFDuXaz6Fcbcy+N9pAWrVa2efQiaNQRlxDHWv94lAuX1/YdwjBrtBdXP5gb/sOnWOr+vlxyezW+0jq+Ccmw52thtYshi4usCfpffLkKVy7FnGGYOKxu7fpPRUJDQQRoRQU3MNWpPS9AVzKIP8SGzhsIewi4UTPIrB/kHRUgverOLKmRq0Arn8yJOb2HUQfO4rnyoPAZiaDP8tHSP7NBPckVe1SyuXy30hC1Hj1zddx7Ycf8WWGjohHjapyBdRyOVSlGy2OXbVqsOvQXkSms5WSYZAvWwElUsv6GxOLoyO0fXrh0vkLuBUbHdajVx/WF2vu3bMTrdlPdp80fVBCFglzu7pJI+zfsR3ZWZlcUCbHxdUNwc1b0D4UeLId7aOaewgGBufkPyChy7RO0niKneyxl54pkUqXd+nWnaNe+pHUUR+lDd1Oz8+QWJU6imZNoajuh9AdW9kvuJqg+V39N58kb5OmzeDm5saO/tI/bSaTSYI5frR+WhacYCWdgQmbnmHfqwfOnziG69eunpBIJSc5ukallFclaNqF9dCKt+JQideknNA8fo6B9UCCpnw45lOCewwXbYhJRHBtPvpyHnK3bME82p4VjRIBiYpQRqk3WiB5SHvkOjogLzsL9o5OOEAbu3WRsQxZWAYMql8Pqnq1sXvzRvj5V9/apm3brpE3o3H7xAk0t5GzK9u0Rg7N4OH9+4wE2dawXhRQu46wUHJpOQ7lupOQiIgjR9DaGkQVRg7SKfv2Fjrl6VMnYqr6+e/q3KU7E+mAi1evQEdSp4Et0puependC0k5eThy6EC+SqnaUBpOsu+tZ6++JfGdpeCkrD/D3/OHDqINZFbzV9hYoiAdUVbVF3u3biE4mbXKz7+asUXLVmwsebFQV+y0d+d2tC3iMwjKDzznMDB9Whpc3N3ZYOPyhBSY/VcTnIbg2AvHT5/E6XVr8aZRBY3RVFOxfEVdDTvanLdjYlChgjfYqZx++gwaS23YkMRR2VGdXlTMcYPZHTt14RqDXfYdOUQSMgHekFqP4JBKoKjghdTUNCQnJV4khnGKK/QOGjxMmNnp8lKpFN3ZquhOBFTVaiiXDrKAANiRTnlg1w4kJMRvJmiW5ezsFEw/a7KbpU56FhytSZ0iUxkF+65dcOzAPsTdvr2HmMHVP5F0IQIC6qBxk6bCZVHq0qpUyj6nzp+FIuI6aktklufRDAXt+vVFfGYWDh7Yl+3m4bF+7sefw8XFrbNSKZ+2iyRy6tFjaEYMrNy1ZAlH+mZxQgLcPb1A+6DyU4J79FcH2qQB6zZtQBfS2SobJeUvkNlYIasdAHnjRrgdFUkSqhoYBtVMSYMbDUlvhdikBIO03bri7NEwpKWmHmgX0oGrgXsdCt1D3NjGTFODEfrsbDg6O8POXptepNMVTXx5Mlq3aUOQTfjeehCs9GRfVutCPckLiVWJqerWBbmkU+7fs5uj4oV1kvbjoNT0DMnZA/vRxmijq6NtaxhdnYRfkDbvqntVVx1a03MJ7t1fcq+1gL80B01zC2BnBf4Kh3+VytB27ogw+s3NG1Hbx74w7kZQw6AuUplkWWTUDeWXhFaG5hXByWiFSZDCW0TowtHeAR6eXlWsBTrYeokYUeG5kT4luHssNwS9bscn4FrYEbQnyGTVCUMcUdW9K9L1RSJKX0lQ7NheIha9xCYYJG8WLMrEbd+4Hv7Vq2+vU6du19MXLiBbJHPaGH2vkKMgdD+caDH/M25ig/4Dh9R4ZcqrpvIMXBdEoRh0Pfomkk6eRHOJwiIDETqlkxPsSae8fO48uyjOVq7se7hBUEMuL9GbpY76eiQCrFblMoq8Ny1nGMTdwZnTJ+PYL1h6A9o7OKJVqzZEL4YH1uBOIsPfwwR/ZdbL7dE8qkJCoHO0x4aVyzHyuTH7Xhj3Eqd1bI2MinKfOG0yWkXcQDeDzHJR2hKCu3wZWqWcdfGKxcXFbn9lH5mO0cpHVlYWcnJyhD6oK9YjPSsbJYW3eH3484dtf1dVpSfBSsnQq+shgl6eMbdECr7VOEMXF2h79cK569c4YxuXIq6gOPwK6hN0KbJhXuwITibSIoQdPpQ8YvRzF2guPzgUdhh1U9LhYs33VoKE1GroT5zC7bmfoNNbM5x79+6zm/SWjRKJLJHWx4kkSDs24lRLSIanRGU5lIt1oeCmUAbUwL6P5yI9I30dF1/18PDoSMOttZuYSZOcAqit+fBYb6xV05RhsOQ3pKWlbtZq7VNKSzciYlF70lz1q+RyVyoV3cNOnoArrYGfDXCSIb26T0/k5hVg1PMvGEgH/qi4WO+8ZddOfEOSrU1kDF4ymJiXVcRAaIEDrNmnqtFqKxqNBs5ET31YaVakKyJm0hYNGjZigxMaNmxMjOA5RFy7Aa2DvSBIR3s7qIgpPSxstTXi6YknOK6PUVRk8LoLvaRyK5y1AIqWLWCsXg1xG9aiW/ee+GLRdyJsyAFKy8RCOpu0UkVzmbh9xAkzQ9u178gL65h8KxaNuaya4iH6rlKi+NsFCL0dC83I4VUqVfZ9xc7stDVkAuykHqGXWg/lMuiFkSMlvxAH94UWuLq6rWvZig9eweC4BJb8hzHYBr+gcHWQ7pYnl5p8bzL5A3CSDRoajeoe3xutQZdivcG7pNwel0PQWSFsRYNAGIOCBESqWb++dNPuXc5bNq1HelgYxuUWo6PRRGxWtykTvtEAu2HPIpuk7vWIqzqFQql7iP0j/J3MQGa88Q6IgaIkNjot/U+XHtdq4Ss5NeMvnY0gJS7KzfBfwt1/nOAIeg2+diMSySdPEfTiysFGyyyRuKEdQa/41GRWsMWCnt0fiomGErOz0QKx5kPVxqTf7NvFSMu4miAGl6LOD6gZYC+lRTPKVKYyALZAGOKiaiKwOms24uSOnTjq7YUUtVJkMxfqi1GBdMr6MishWKwLeVcQyau7jxxCVFTkocZNmobXDKjNhVK7HT5xFB7RsagqCM5oKTEQEgcH2Pfrg3PnL3BI2HmC2of/fI3J99an3wCRLnTPJlAohtyI4UyGE5hoA6TmrADj7Th8N3QwwgnEG1PT4JCYjBAimHYEn52JwxRYkmy84ak/bJ2UuLnAfvY7cBw6CIt//w0RV8KPKdWqCNukjp6gY7bQ4Tk8rWfv3ndL/XH6DunV9xBm6X//yiWTK8z8Uf8/S3B15ApZm227d8CfoIyHUWWx5ICwwHl7Q9O5EyIvXUDNmrVw5ORxyC9cRoDOQBtFb3mRabK1ffsgmvTFs2dOJSiVqv0KuYIPX9jZbcCgAZeOHEPRhm2mAyts5IIlJ/F1zMxD27RI2mii2J6JmRBHFAHYlog2N1ccrCExBz8X5Oev4bQetUrJJSY892zbgtb0bClJHUulFIz5+SIuVFmnFvZ/8jGfEbCO4GRRaekWRFCrJF2o1OVP8DeEa8f4RN5ABb1SpOJYFUzU7/63buEZ+q5SKoOWGm+mAkmRZZRB8yEhYpBSPxQdQ+A0dgwUNath5cqV+O7rL9KJAc+05tZh3yGPwdXVFQMGPYsRI0ejsm/le6T2o7r+W6L7RwmOpMtghu2tmzaH54JFsJfILBehJAVYRlJE7+yI9OQktG/TFjoijJlffC2sk1ZTcTgTuVN7HFm8GKkpKdvtHRxSI0j/y8hIm+Hu4dkyZNFCb90LZ6G7GiHKtD2sosyE53D/a61JSSIETu25dScBJ46FJbu6uW1q1VqEcg3XE6ceMXgo6nftAy2HfFmRlMp6dZGek4tD+0N1yvtOxOGetA/p9MCQaA360S2HAJISbWd/QO+RQWnjsF3MrMVYapwKWNP9NFDWqkWMIQBGJwdcjYzCktdfw45tm2NkctkImUx+zJJE48NGOLiA3S+Dhg6Hv19VUZowJycfjyu54L8hukdGcByAqlRYjhpIT89odv1aBNyJU+k9PRFlS4cJoMeQ/pWXm8MZyxK50ShVNgyy6bcigoiIaQ9JEoVSsYprUG7ftpmrQkUdOrB/RNuQjn+0COno7dQpRFg+H4s/iOGnRIrdq5bjzp24na1at0uoVbu2x524O3VSU1OFUSiZJGWSDc9REcQ9tn4d4m7fOkww8VJpOFmhQkW0a9de/J8riJVcOTm5IdE34xFYpx7pZA3FeyTlbHaWklyuj4jZVGjVhsucnsRnapkyO4hpZqSm4OrObTh68ICo1UK69GKNxu4L+l5yeYTGIXOuru4YSBJtCBGan78fdEX6u1LtcWfy/FWieyQEx/j57OmTuHDuHOsHQtcqQ3fjOotfXTh/tpj+fqhCr7QAMnpH3E8/LHSgRWShYrMJib4vJwU7WaVSH2FF+07cbdSvH4TUtNRQgjSNfv3p+6EODg5NaVMpjY/JAyvh821zcpQEb3/wqeRDSr80+fXpUz6LuHqlE3XjoeaGiEtDc/5DaV2FTeFFRTqsW7tGbPwSYuG12bd3968RV8O9aD7LwWMSJhqjg6OjvG7dek2TkpOV4eGXxdG+tvi3AgJqi/KBZ86chlajuSSVSlIyMjLyTGk42E9745CdnTa9PEIrLCyAmxsR2mAitGdHEKFV5RN/Hgt8fBRE90gIjhe0SpWquHrlClYsW4Lw8IuiMJBapb7HZ0JSZCdtqJ06ne6h3/HfOEel5o3C/pWU5GSxqMwAtFptAt37Mjs7+7Gf8sl9YlM26y8k6bjM29f02dcPOzdMaPcbBniukmmcc2a9KSQ3V8AqxfjWEfdaVw5zIiSRiwqkN89678N2TYOb7p846WWcOHWW9MEGbAUtd564334knT/97Ct8t2A+Fn7/Y2YFL48+vj4VbnL/OIXJ0tpyTqEnoZ427UIweMgwtGjZgsPFnghCu5/o+DgvfXHRP0twTs4uGP3884Iz7SIIt2zpYly5fKlMifdPZvaKlBBjSfylUfTlnwygNQrikwnYxhtP+l9ESbAE5/hOP//qGDjkWY4GwfYtm5GXn0sbXmtx3vnd/Pt+/QfhlanTuDbk4MTEFGzZuhXuRAh2pItZ8k0xE+vUuQvUGhVJVkHPu1LSs266ubrAUWsHfRmQVOhonAFCEm3QkKEYOep5ZJFOWpCfL4LB7/MdPjEXuwuYWTLRGa1A7UemwzHRMTfiDfPMgAHo3LUrdu/cieUs8S5dFLBTpVb/Y5PEek1ubg5x74q0eeyQm5NDn+kFhPqnCM7kvNUJshNM4C8yIv4tj6eKn5+AYb369BGZAZwDN2DgEMH89u7eKaQXR+iXJZ34kMa5n3yO7j168rkIrvRx3337QhEbfRP16tezaMQVES32DujWrRuOHz+Jk6ReyBTK5dyv2LgEBFSrcvd7gjiJKTCxuXl4EKENw+Bnh8HPr6q4F3/6LP4XLl4ruUIpKljzWMo7mln+ODYRE55MphCEx6kbu3duF4t+mQivpCbiw0Av1h+k5nJ6Za07Y2omKB371crYGTwZXl7eGDZiNJ4dNkLUvkxOSRbVjfmZaWmpFiEl3yM9zzy23L9FQvMzmdCr16zFOpzY9AyrBGMiRCCMDnwoiIV+8dkGbKRgjtu3/0BMnjqdxukpnL4mKCZBnbp18dEnn+ESjf2PJb9xtM09UpTfW69+A7z+xluoXbuOKHVBfesml2t8Vq5aJTYVnwhqyWjCz2jSpClq1KiOqdOmo1ini1ao1HuFkSYvX1hk/atUFtW4eNxslfWtWhUDBgxGVb8/dTSG+eUcs/zkEh7BbFFYWBzgYhB7rbTUI2U977F2iBeXIx1ysnOwZ89upKam2CxRTDpXCn76+ScUEWRxcnQQtQkNBr3guLzQOtpwHh6eqFChAlrQQrqKnC/9PRubF7Et6Qb+bOkivUCciaaQISkxWUQthIUdEs8qj5B48y9atAhOTk4YNmzY33L2NcOlOnXqoWlwsIBqe/fsQlJSoujHkSNHcPDAAbg4O0Ij6qEa757GY4ofLBBMxsenEhoHN0O79h3QpWs3YS5nxlOmC0PFVkM9EhMSH1QHOK7TwVHEJYrvKlVbIm9E9WzWLJjuOaJyJR+LOjQzobfffhe9e/dBYINAduZ/o1CqJvOz9UVEjE2b4sfvfxCWU7YUV6jgRYQsp/7qhXGn5FISQV4kpsxjDAwMxMPos8yoMjIy0C6kPSKuXIGiDMPdY9RbHp+EK1fiyZXo07dfSSqLzVd0dCy+XbiI9JFoUWHKw80F7i7OYuH8/P0JQg0HlwOvVNmXJl1W7vMLC+9VwHkDuZLuwDQ2fMRIq/34bfEf8KjgTe8b+jfCXIM5BMmIHj17ib5nZWUjKTUDfyxfiTtJKXAhJuPl4QoHe+1d6crMox9JtMAGQcRs3MWz8vIKYMkLyDoW/9bD06vMNSohNrqC5Appp/Xr1iErMwPViElZkm48j1wAt1OnztixcweiIq8bSbotF1CZuP1/xr6ABfPni7PDTRCeGCVJtEJdEf61VynG/Y85vlkq5ec/nKWRIQbDLCF5CHbxIsXGJSL6Vhzee/cdzJzJVjjlXQX7YZXsYrOlydrv2I8lyvGbEzgfheXMfBYBPTtPQMUS2JeakSWi37WEEvr26okJE18REkNGzIWrbpkIzXYYW2zFukZ9mJKVma3is7SdnJ3BFcctERxDWiY2N2KEHD1C13Hq/3GfSpXwxeefY+CA/mJ9WCr/f7zk/9uMgw0cpMuRZKhdt74gtifNbPy3m6FlUkEomZlZGDtuApq1aC6IzFio+8tzWEJ8ZVwjVSrF6AULFrCbgnS7+jbpod2798Tt23ewZ69Q21ZVrVrVGHYkjKCjJzGSQpud5k8J7s/fuPwTQpH9w3qDvlBXWFimDmSlzyrYmFtqAxw3yuXSfL2++JHPBdMW6S46Ax8IKVXeQySs65nyvMoclhM1H/qus4VxS2jemEPJFKZ64vw9pgYtSdSBarVybFjYMcyaPQsVvCvA0UFr0RXAa8D+1xbNm2PR998jLSU5WyKVbfhs3mdMbO5co/Ohxg6jhAi4gJ4redj14wM/SKXQ0zhKnOqG/1WCG0KTsIgWI//v2MC2MmKYqpanurm6RdeuU/fM7Vuxy2hSIyxYCB254jFtpN4Khaw6/d/uv+0vbzaDwSiJirqpDzt67PqLL7zIgc+9CGIaH9VcVPDyvNOoUcMb7h4eu1KSkpeR8p9dakOW9ZN2NOYJSqW8Nc2PN+mFkrItiUXC4HTq9OmiGtWrS729vWSsQ5qIV8In6GAFQcIZM16HguBqlSq+pY9NLofgdGjbth0USjlWrRaZQbu6det2s3//fi0I7m6meTLYOk9sqZRK5ZKIaxG5NWvUZKOWVqHQ2JaML6yDEkl0TGzx7du3E6Ry+WT6+ND/JMERph91Jz7efsmSxfYMH6SPIX1dRKUQA65cubJ3hw4h9TZtWN/ruTFjXlu5fNlUUsi/F5N8L+cdpFar59Kn1a9GXMfFixeRnp4mIiP+ukGjWFg+n3mmL1bTZspIS604fNiwrLi4eHe2mpaYsP/OMfPcNmrYyHPwoIFBVatU7d+7b5+XkpKS+tO7bpgprvTlTuvxFTHCYdHRsZKdu3bi3Lnz4swA1X2FVdmqOnDQYHTt0oWDgRXsZpn48isi6oZN/qyHhV+5ihs3IkVEfgWvqncDBCz1l53pPXr0wNmz53HixAn+ePmEl14STDonJ8ftu+9+ExXD2Jhl6eL3t27VGi1atEBCQoJHzx49seSPpYiKiiozTPB+Kevn54+RI4ZhzZrVyM7MkNAeufaX10G4nowiGOGfILgAwvRtVixfjlnvvkMEUElERFhaCIPZH1H6Esl84thZ24UCh8/wQv3+ezVSvr/EwvnzNbSoC25GRV5xdfc42LhxY1bGZbTpP7OzU09hh+uHc+fi6LGjkEkl0KjVVhda9JeLpBoMD4yJLYITJkwUBMf+qKbBzeMDA+t5v/3Ou/jwg/cFMbLEKHMu2ClKxG5iTg8nCDk44Jl+z+DDDz/EzBkzGrw6dcoCo1He7T7YWU+lUq7S6Qprz3l/HhYuWijyxFycnYU1szQj4LFxP2bOnEkEdQOkXyEsLAwLvvsWXl5ecHZ2uptPVrNGdbG+5bkW7ieSRo0aIyCgFmbMfAO6woJYP/9qO4KDm3F0Q5+Dhw7htdemiXc4ONiXq8fx/IlEWYKlN27eFOFhWSRtJ0+eTOtbSMTvZVEH5NIKM2a8Idw9mzdv5o82UUv8K8TBllV7PsXIwVGce/B3BEQ8FMHRQg3IycnTrFm7Bg5OTixxLA6euQNvGHYSG0tV101PTxcbmMu1PYxjk78XExONz7/4DD/++CO6dO4s/T4qcryjg8NBH5+KDPk+YWKbv2Ahpr/2GklFOapX8xM+JwuGgbuckReaj7Tl/vJGK21Q4DZs6DCcOXMO586eKfrqm2/ZxOrCXLdZ8xaoUb1amdEFJQdmELQR/7I0eRijAZvZN27cQJKjJ3r16o133n23c252VlCdunXPNWrUCAUFhRWY2EiC1x45ejR27diOmjVrwr9q5bv6Xelxs3WwOW3munXrYfuOHULKrVmzFnXr1Udg/bqCk/M4eHOxS+LWrVhBfNakN1ud2TrJltKNGzfyR5vbt2uX6+np3ov+9luzZg0xWvndCJLyLhGDSd9p164d1q1bjw4dOmATEU56WiqCghqIGMzy5k8vfJE+ePbZodi3bx8uX7oEuVK18i8RW2EBqhOU/fabb8V5en8sWfzYCU6p0agG7t27D2fOnhXczxrMYPgwecpUDB48VCxICYe9cSMKy0lKrli5ghRxewHHbCU6JohY2gSsg1T05tIX8Atu1oyI136ATCaZNn/BIkyaOEEcUlGporeI2bPmN+JN6OfnJzZ1K4IyXP67rFhD0ovw+uszeBGz+vTqLQrd/Pbrb2JDWuo/SxSGtd/N/w67d++EN0lDU/iWTUzOzGhiRM1LYgTSXMC3WvUa5xwdHdhv9zlt0tqjn3tOEBsTIZvuy3NM81x06dJVMD2Gj4RYMH3aNMx4/fW7h2HyfDBnf+PNt7B123YENQjkYrTljlGEZbm5C4LbvWc3Iq5e4Y9X9OjZg/8dHBt7G3v37oFXBS8xHktOcy7jZypyZFoXTw8PLFu+DGoiNI2I39RblLLNiflVrFgB77zzDs/xJRrT4YfS00mas4HqPy+8iFnvmFDcT7/8/I/ocJwVGbSK4BSn8zs7OVrcZCU6T7du3XHi5HGsXbvOSISVThxK37dPb/mcOe85V/Xzl7z++nTa7FUsRp7fv7i8CEqlAolJAino6tWtqyBie+fc+YsElWaI6PZKPt53HauWpBoT+/jxL+G5554XIV4nT50iRrAK8fHxhTSGHFowY4mkMhDn/33xYrtGDRuuy8vLPfXcmP9MkEmlfPBIueYE7m8lHx/DxAkTXX/84fuKY18YRxJgfbkSsSypznoL6yV34uIEE6Mrr1vXroxUW5DONnTevM+xY/s21KtXjzPFy92UosxCxYro3LkLEdI2gueLC5etWB5JSIQzhEzhcDqdfsrkKVUCAwMd169fL6S9KZrHsu8tJKQDSJqV+N5ON2vW/HCP7j3Zq951165dSEpMEFEn1vRAjUaLzp26kA4ZTvA0AFeuXhWQl6NbrOvIRnTt2g1paRlgHZZEKkdN6x5Gqrm7ewhIPO3VqTZB6UdGcMTFh8THJ2LLtq2CUzFXt8ZtWrRoCRcXZ8wl/WPLls1h9DHrHkYBCz/7fNaY50e/tnnLZhzcvx+1atWwieDYUdu2bXtRNfjwkSNicXv26NGM/m3wHekhOdlZqFO7ltUqS2xR47PB33//Q7Rp0xqbNm3B3I/mstUuTF+kY5Z2nFocTFUUSq+0JLBBgwKSOIW///rLr7CtFr6edBmHPr17rp89e1b7nbQBU1JT4c4BxVbgJS86Q/eGDRvix59+REFuTqZSrb7eILAB3x6VnJwq4fQXd3d3sMSztiYsPViHWrJkCRHp1qP0cecSYcqvm/3eHIfg4CanN2/e6ngt4grq1KljkxTmjZ6QmAQmLrrWdiGGQPC+B62T51oiXOqzyDCwFoNZv3591KxVC6y29OnTF59++okgBBezbmlpnrwJ8bCVdP2G9ZyEW6RQqdfZZNXkxFgukEsIYuHCRWjfro3JtylOX5X/IwTHpex6M1y4c/sW6gfWt8qpWAdiiHY9MgoHDh6AVK5Yxn40TloMahDEeHguWxP79ulTdRNhfpY21qAZbyYXF1d0794dBw4cwIVzZxHSsdP6oKCGzzIzYK7N0s0aM+B77HZ6//25RGytMG366/ji83k5dGsa9fMHhUptkQsW5OeVBBEXuRJHtJbmwlCIIGtGcbFhXiWfiu0bNWqI0L174eHubnXieRMykZAkI51G7J8DgfXrxwQFBXnS3/04fMoUwW99TVhS9urVC1evXsN+YnI01sW0DsVsHOCThxYQ4bblCQGqsWFIQvqcg7m8nCWUULmyr4Div/72G1ehzpfIZGv79O4jrMUsoQ4dOihgtDU4yUTTvn0HoTtyXzlAfe3atXB2cRH/t8xMCgScdHDQkpQVLgnmxueteZuKidAYaTwzYCD+WLxYxMk+yuAJmwiOqwgTc/Fi5Zr0F2g1dha5DUuPGqRwBgc3xUcffYzsrKx02sSbebOOIUg1adIEjjjIoHW87enhWZWXs4gmmwnFmiXMpGP54N1Zs/ijsy/85z/RnAG0e88eJNyJQ6AVZlDynPHjJwhim/6aILZMmVzRSyqTHbZEOIZiEwRt1z4E9erWA+uLPYj4remgTJAcFkXEkyyXq6El/bConEyG+4mEN0BXkhaXLoXj2DFR7mP1xImTOLysLxFwheXLV4jAXHutnVXpwYaShg2DMGvWe1yiIp7WZAuvyaDBQ0gX/VUwBoLhw+MIuu7avVsEFbOhwLLOxb63toIhmH1voSTtrhEkZd9n+61btyCXUEdNgtDWdGkOmmZoGnEtAnWJAYSFHRERLrVr17bKiFkdGDhwEK5fjyxh8CuszW2xroDQWgXMnfsxRo8aKVQQ80Gg/yzBkbQacuXKFYTu24eK3sSpZJY5FTstO3ToxKoeQYO1/NFOGuAt5ih9+/YpUd7daR/6xd66BSNHUUis+/OYQ3IEOsEogoCb4O7huZgWO4huVVy7jpmB0qIVqzRHHjVqNDZv2UrQdh4vzsvlERsbe/T0GzutPQYOH4GpU6agQQNT5PqEl8YJ87Otfjyag0AuUXeLxsw6qC3SjTcbv++9Oe+zZE2gMW8nKcS3h126dAn79u+DD+ll1qQHV7ri/LRC6i/PFV1bqD+ihki/vn1p3jTM2b2JkLswLExOjBfvtZRQaWIIGhHKdfHi5RKGsGzY0KFsYHqGmKpm/YYNwqxuLQazJKWHrYxnzp5Bs+BgzKO14arMDo4O5eq7Iis9Lxdjx75IOmxdvDRhIhN4LDGTteVJNX2xTmRX9B8wCJ/NmydsCI8r5MwWgguQy6XtNm3eRAPJRM0a/hY7xvccHZ0Epj98+DDOnjsnkg85ANefuFzzFi2E6Zi4fgh93Sd0XyjHFYkNaA26VKlShaRSGwFdUlOSc4c8O3SDj0/FTyIirguIaQts4YVlU7O9vR2++OILEQ1BXHxJ2RywEBoi4EHDRmDK5MkkHRoI3bGkzFxxcb4rjaMNH4lMz+AQKkPZUQ9GSVJycra+uLgnE9658+fg7upm1QjA+mr79iHinRs2iANwdjVv3jzV39+vCf3dhplZAW02N1dXq9KDjQHdunbH7t27RB4iIZU/eHyNGjchJthXbDguyktS0331mlVQkbSzxrzYohhIumS9enXw1jvvcl9ue3h6bW/frh3ffub8+fM4deoU/KpWsTpWJii2cqakpMCJ9g9bUbdu2yrWVF6OlOX5YYnEeXRTp04VFtWff/4JUoXyI7qdUh5K8SYG9QGpE88/P0pItccZfyu3QaoMoA6pV69ZQ5zGCWqV9Wjxpk2DUbWqLz6d9ylLr+tSuXqvnia0M0eRu7qIAZLUHBkZeYM2wG7hzLQFujC+Z8Jctmwpf7SddDkOquy6fcd2ZKan0cL6WvYLCiuYBl27dMOFC5dwlDgyMYPfypNqnbp0xZeffyE21H0L40QbaLpGox5Lfa6QkpIqcq+M5eiyVXx9OScMDYMa4pdffkFOFjGu6tYZFxt12ITP+XDnzrM6Ilk54aUJfPvZjIws2eo1q0XJOJUFy2SJfsOb2d3DDX/88Qd/dKrEXN65UyeCp3ZibARfh7DUPHIkjJCMtw0MQS+QDEvNDesFQ9jasUOH9MqVKzFDaMZWTs5/Y4u29ZQeT7Ru3Yb0y6sEfesKw0difDxq1aop/Jelf28+11040SdPHoORI0fQWh4nKfcfLne+iCD2onslWpEw9Ts6u2DEcBPzrFGj2l3DyOO8rBGcnKDAwH2kYJ89exa1alS3yYzNOkdWVo6of0Ga9zqarFwlQY/hw4bDPG+1FQpZp42bNnKIFKpWCbK6+dhJzkaY06fPiGTM+oGBPw/oP6ArcWQnNibY0X1NqYpU5REtm87r1a8LjhApzM+7TdBj5/39NxqM+OLLrzBxwgShn93DAY0IUqqUS2kz1tm4aTN++/03XOZaLTK52Pj3R6c8O+RZvPHGm0Iq8hjWEJyzd3AwVY22Ij2CgoKIcVXBZyyJDfrrtWrX3tuyZUuuUjaI4f21qyYrojVjCeee8dzduhUnJJxEJl9BktbgRlJv7NixJYdHBspkktYsSfNJarJ12RqRcNgXuxjYyRx++SJ/vJxDu1i6EUOQsgXahb7DTNJy/ZNCNCZJy8YRTr/ifLqY6BiRSBtQq8Y90Tkl5Rt4bnhM7u5u+H3xErz66qtIS035jtZz8t3vcrkDIjbWn4cPH4mXJ01CQEBNwSgeZx3LhyG4VtSpoNWrV4v69442+N7YNBsS0pH0oy24FRPNptlVRcKC1JY4WCux8bgALOP7FStWwt7R0SqhlDg02XXw6qvT+aNoWugjBAvXnTp1GsePH0elyj42cORidOzYWSR6rjdBNOIISLvnO7TR2bk9dcpkc35ZfqnNi2A7O/WO+Ph4l5dfmYKNm9YLS6N3Ba8HYinFRrfXonev3oiOjmZfHBHmZWIYp4jB+FrtK29Q7itz4W3btonIjW5duxU6OTk8Q3/7cvQD6Z4Ww6RKoHi1atVp7lsThP6Sj+fKZHM5r0kw6Un+/tXMZ5FLn8nJyVOxZOENqrJSe1LEO5JEYifzW2+9JQxYgQ2CDvTv35+5Tr9jx46C9X4O9bIkREpiRlkCJyclCWJjJ/vs2bNFFn5ZXeAIOYbArEZ89fXX2Llzx1WJVPIujWt1aee1g5MzSb+ReGXSK7R3qt+DUv6pulVyK763ofEJidhKksqTxLfCBt8bcWBhml2+YnmJafaMYHmkJ5gHSchFNWTv3lCB72vWrA6JDVKTFf4cmqz1GwWhLO/duxfX3m+ziaSMTvhprHNkZ/oOLyybxMNNYT+r7ydIhs19+/YTm+S+sXpQv3+/feu2S59+fQmSXkBQg/rC2GBKkzE+IE1r1QoQzt6dO3ehI0G3j+Z+yPU9CGI52aRzscWOXTE3oyL54av79ukrfG9XrkYIKO5T0dsqFGeC69TJVHF51SrhlN5J777Jf7zy8sslZ5GriZGQ1NwP1rsYxtlguRbGkpSUNJHZLXxvnTszPGXdvI7JWGYUkUTWmDRH9rD1mV0Rvy/+vdDVxeUcvaHY5OM0PvBeYhCSiGvX9DE3b0TQ2LfTOm6jG/klOhpXjBsxwiTRmEk/bj3trxKcm0ql6LVnz17cio0xV2qyzfcWGXVTcB/iwCt5QtlKxUYU85lkHWjOai9fuUKk3At8b1Vqegtuz8Go0TeiDB06dvq9das2g2gSVQxLeYJVJGH0VuBks2bNaXErYvZ7s1FW2A9bS6cRNGnZsvkDC0RceEaxXh8w6ZWXcf7cWTRu0hhKemd50Szc75CQEI51FNHm7H/cuGkTnAlisTS0Jj24SjJHbjAKoOtkg6CGxxo3blyb/g7ZuGEjR8Gjug3lDtjUzmty8OBhdupDplQtZifv6NHP0Zx2EhKU5mEsfb3uL7/+alJQHR2sGrAqVaqMtm3bYOnSZUiIv1PAUR3MFOkayD7RHTt2CB3Lmk+Un8XuCs6iZ4ZwcP++UPq4h23b15T5z6iEOYojSbRRJNEmTXzZLNEMT1xCstyK782HUxxsrdRUq1YtNG3aBB99/AlviBTiPBt5MkaNGiUMD+ZKTCM5to6lpi1O6hLo4uhoTwq/MCYeJNhyk2DHsP0HjuHCxYuoVbOG9Rr+RlPYT0Zmtinsh3RLfnxp3YD1TLbY3X/2NTv+NRrVyD+WLsdmIvDadWoLyGUpXtGRoHLnzl1xOfwyqhOkO37iuICUrEPYIj1YN0pMTEao2H9Y++ILL/AcDM3OyVWvXL1KbC7rpnZTtI+vb2XM+eADhllXCW+F+vpWwfvvzymJnWxP0Pyj0ND9RNwrhLGLsyos6VziFFWCqPz+1Sbf24HatWtfCQ5uxuX0erB+GWdDgEQJU2CGysQRHc2CVxLGtUu5nktFn0qWI0Ro7CYjWBdRZKhz507wrVz5ianM/FAEx763iIgIoRBXJOhiS5QA6248v2tNERE7aYHjZ8+eg0kk2s1HJFVRKuXdWSdJjL9j8vMYDVajI3r27IWIa5HCD6jS2P3cpXMXzvUPXMf+JNItnWyALXwOOEuNLVu34HZsbDExg3vCfkQ1qcYtBLe97+xr3pg9aUN4csS4UqUWlausMQlOVfH19RVxgAMHDhTVh3lyHB0crPaVfVFt2rQVAd5JCfEFjs7O6wleMvIeFBZ2FOfOnBZxhrZIAJZuqanpHFonCLdZ85b5CxcsEBIqNzevDxHbbzdvRtu/9NJ42GnUwmJsidhK1qR79x6ko10rCa9bTvolqxKd6bbwibKrR2unserb4pIR7Apgva1qVT9EXL3qUEwE8/4H72P6tOlWK6KJoAInBzHWJ5nQ7iKlcj6vRRMQwnAtOysT7m6uVq2IDF26d+sO1gNOnzzBZ3b9/MeSpZg16x0Rec4biSZngE5X7LR02TJo7LRC/7GURcyclGvTcygUB8UW5OUm9e3TZ2ONGtVGJCWlYNv2HfDw9BTVnC1tYub0nJLCUpK5OF1h1JcHwn6GDx9R1tnXvMGeDQ8Px8FDh03Mx4qTvsRSm5CQILIOmAC3b99u6quV8DVTDGorjkMscVAfatY0+HqNGtW7s09UuETo/Y42wD72WzKTWbr0DzaxZ7dt2/63QwcPcDRO/cJC3RI2Ol28FO7Sm6R6zK1YBNigu7GBhZ3xnCqzavVK5OZkJ2q09tsGDxok4CRHejCTNvlEraezcMFZ1uWZKXz80ceo5FtlKKkyVdh5zlZflmCWGkfiMCxm49ajqsws/RstLPJyfG+taRB2bJCwxYRd4nvzrVIZHKj60ksTc6ZOndKCNgktbBEfkMwbrZgmcvzRo8cRdjQM/n5VRFIpYFlqMrRis7U5bGjd8889zyzsGU5ojLkZhbr16lrVLflcA2YGN2/G4sDBg2wWX33/5vTwqgDeNGVEjtSTySRtOCcrPy8Hbq41rQbRsu7CEur48RMi+mH/gf2meMd6tunBXQgiXb8ehUOHRFWA1S+//DLDvFFcIpBz2Ly9K1iF4sys2OjCNUA5suarr7/JGz1q9BsKhZyjkRvl5BQqf/jxR4KW74vEzgako7OeabASQsVIZ+zYceB6pouXCIi/qVWrVsnNmwdr6e+gw0cOC1dPlcqVrJb9NhvmEE9oZ968T6gvH+DMqVOV9+zZu1+jsdtAzLhQakNJgUdVdcCcLSDT6YoY6zKDPvRICE4mk1VNSEzETdokbNmzpXQAK8xxcQnEPeth6pRX7DMysj9k5bn0b9kPtZhgGVuSnJ1t8fO4iUBlrv50+eIFTgj8OTg4mK1gVVi35IRGa7qlKeXeTySJfvnVl8jKSGez+JZ7RbQeXTp1IkLxeKDMnDnpVs3R66w3KW0wl5MuQxLfGRyTyPPyzrvvQkobS6u1s6Gv/oJ5ff7F55z5kF7Vz38DjZkT/7qxXys5MQENSLrY4rcMCmqE+IQkId3btWvnRRJ3zOGwIzh29Cg4cijy+nURFubp6SeQhjXjFTvQJ016hZ7VBm++9TZuRF7nmi7fONiLU/G49oGWTfsmVKAsr+bKAxdLqh07tglEwCijffv2VQkeTuGwv3/qYubnRYiE7Q0nT53kVJ+5j4zgaLMnsJmdow1iY6Mt+ixKrIgtW7YGRykE1m+A339fIjaZj4/3A9yHM5+rVa8uXAzWc6xCaDN4YInJWHKaOP8pV1fnxczp2XpawdvLBrO4Tkgb7oY5rnMP+/H+9GObLs6mLpnoUpdCrVYNEObyc+dRs0Y1oYdZklLcn27depCEui7yAfnUGk6+ZN3IlrAzDgTmvpr14N0vvDA22cvL89WCAp0Tl7ZgKG7Nb1lygs6cObNEblixXsB5sVbsWGbC5lIKgSTVTJ9bzqxgGMlGoClTpmLEiBFYsnSZiCKSKZRvku57qf8z/fir2TSZ8fXq1fdm5sK6FzMYWyM5OCv/3LmzOH/+nPi7gNafdbl/4uKQMYb/WzZvRWhoKGfI6ORK9fJHBilpMUNJ78qfOfMNzUsTXhJ5Tlxiu6zJ4wXkgGIuHcALw7GH8xfMxy0iVI363gBd/g77jkq4sGUYKKfn9sXtuHjs2CmCQRZ379bVnv7tzin8qSlJqFbNH9nZmeXqgaYS5gqh4J8+fZbaad4k9xw0zzGe3hV90KpVywfgJG3GlrQf67PxAkZOs1GjkIjC0hFNXN6gadOmBIFXI6R9e1EeITUlWRg5WJe01FdTZkA34qhnRBEehVK1olPHTnx7KMelcvQ8V89igrEl0JbXhvUPpbmWi0KmhJbGIDVDR2ulw/l7Li4uJCFbYvTo0ahevRp++vlXknIT+f2zSUdbSPobIY8a7KAuovVf26lTp0ZvvvEW6Y1LBPN52HMjTKXbTVEgCrn0HyG4ItoHHYjZszHGrMocof5cepRWyiukhL47oH//eRwHyNZKB3u7MgECc81WLVuJCHiuK8ILPG7ceOFUZWMJh0npzLlui75fRHrUDat5YCUwsCU9l4uQpqckZ7Vt134ZbcaBdNudzc0LF34vIKA1szFnLLMjl9NwigoLou8P5WI4+Z8xY0Qq/f0WLtoso3lfjxgxXGQ5ODk6Wk0T4Yxq3sh87FLlyj4CHq5bt4HgsYvVvnKwMEdmvDZjJvsEYwYOGbIlOLhpR7rdhA0ga9asg5ub62M53ovHqRX9qQ0HR3tw3OvwEaOwfPnSJNK9p5JevKyIpFAlfz/Ur2ey7NJvvqBxNJ09a1a/kSQJ4+JuP5aqbn/3xevYoEEDGnOUCMCXyhWrHofj+zPiWLFBQQ2mV/TxaUITKSkPvhQU6nCcODJn6t4iyMgWurtwhKAFe/vv3LmDcS9NEMcQs4+lDF/XPZKiQ4eOoqryUlOg8s7OnTqlkMLf5tr1KGE+DqgVIE7WtMWndf78RZHOQ/9j23hm6U0uU6rQqnXru7CG9RTz79Q0/gocQ2pPXNyZdDJbDoFkRzfXasnMyjScOn2Gy/dJmXnobezrgYOHwEG/dG0haclmt0YXL10uyM7KVrM/j+Hh47i4LyzN165fxwjDuHPnzqiMtNQVhBAWEhHd+ZMwJWa9VlQ7K6A2hNZvELVxWnuHlrK/q77c47xo7Lfj7mDx4t/ZSp9GTHrr4yA4hmOrYmJi1rRq07o9YfIGBBHKxDGsHHNdyJIICrFYxPGo6d6YObPa+3Pem7Z8xUqkJiciqGGQRVdAidOY/Udc9ZdrjND1Rx4R7o0b0a8ENwvm+tne9A6bdx4/My83V6FQqdbfx8ZFCT1OPuT3TZs2XTjo2Z9DXLzwytWrz4Z07NiRD+zkuiU2v0+vl8jk8sLZ773Hpxar8TAVg6mvxEgYix8wS9PPR40efeTcuXPVCHIWP85Npy8ulhAq4HdGSmXyq7Tx7jlqSUKQ8U7cLXzyyad4440ZNJ1Cn9aRfr50xaqVK+bNmxdA81aXmhyPr2jw36fLFRXJaMxcOOfWYyE4sXloB2RmZoYW5ueHSiyUCbtb7k5E2+tRgaDVwgVL0ad3rxkcaLp06VIRp2hLoDLrQH5+VTFv3mds0YxgQ8fFi5dA/cjNzMhYxptBUhZUsQD37s0mZwlLehDBYRYYXHPwt99+FY7moUOHYcaMGRwRYlQpFVm6woL14ijgMo7ytTQX+vv0I1sOWCz5jjCu/KnnGkgXCyMqDivxM1l6VmnIa+07pcsAlvfdP8tNlArMhkHMHR8+mJdbjHfeeUsck8wlGu5apO3s9NTfy/Tn5fJgZVnvLf1ZWf20NraHfbalZz0K6G49AdVo0nOEHLMAZ0q6z2dI9+rdH+++/TZHkrC9eAzHVZ49e4ZgYC1beDxJm17IyMgSCYhEWWulMlne0aNhuHEzShhldCZ94QGOLC2DIZQ1wRxJbihjLAwnf/vtF+zbtxc///wLQbt1grA5vYUf8zC5U/cvVolBoLwD2Pk+9/9u6UGJVGzgjqS8c3RK6WeWPMuaT8ogTuI0lvme0nNi6Xkln5fMl4gMMhoe4G0LFy5AYmICqlevLhhFfEKC2AtcL6Usf5zUXLW7rP6V7tf9/SyLsVgywFh7ti3PemwEVxKr1rdff6FXlcepmPPWoIkm9GW2ajUTOktubsErWq265vc/fC+iDpysJCLyc3x8KiOkfQesWrMasTHROrlStYI3WmpaGhYt+p74rLHcA8wNJqqwSnDWLq4B2alTR9PvxSLpBWQ26PV/eaIFArAghZmn3V8UNy01FQMHDbwblC2CdO/2yVjms9gkLzHrz2XdF+8xpQiYCVtyDzr5by5zkaMHUU+Ze0tvOlX9/vXisZnHKxgDx3uay9WVnKUtGIkVK21Z7y49bxIzgxYZ+fysMvbO3fl81JEmpS2QfDb0yhXLHsLCYxBp7zSQnvb2drM2bNwEDhHz9/OzIau7UPih7LTqkqzugzTYi2Z9Env37C49iVzYo6kZ60RS20U38y1BrL9kreNTO/+GWhel+sGn2jA1y8zAgPU1ri2y11hGZ1OJ6MrqU7lM8t5aimwO5sh7bQkvYd8e/b6oNAx/VBz+vrWqV2JX4nWlm1HGcua7tIS+TzfmDfaw7y7z2f/H3pnGRlVFcfx2tnYKpS2rLLIKyB4iUTDyxYii0egHQlSMIItCIAQR1ICFsCgVo6DYGARBwDVxgw8sCQ0xIggoiwhBoYCkaC1IgZaWUjrj/b/+nr0dp0BM+aDhJSedmb5337nnnv85527nervGXVnVZxgaODdl6FoY/xcLQsdasC3e9+P+8Pjx471J1pYtml8RbH5CGi1UVoYqbzg2HFnzd5+rqs7819OWtPBPS7Rk9u+0NEqRjaV7Ld0NEAstKcekFGw2/z9JGUp//JSlhZaUjluJOMoBgvqNGtFUgqJ8h00lw33Eua/I1Jzqo2Xt6uyo43bC1EysC1QXFGVb0uEbSymjN/9bwzPibRhA1Mz8oxqZhP9DllbxDl058LXXUhNLz1G+ylCavy8wPgb+5gpglnZx/22m5ngtLQfRypBZlpZb0tIlTZBqt3Q1hkB/v0e2+qzwpoS6DcBQVAEg1fsjytF9ozXKinzuR2ZLKFdbjPpaKrCk3QVjMQx6xwYPjMbcRX32UpfOljQHtIPvqbSFVuD0RI+rqNc+yhafWuB5B2Ufpq4q5xlLedQnhfs+xzAMoW56/8c8d10A1woFiDu9ZF+giT5VCIwldOF0PNQg64kmhcPBIZvzt5hRo0Z6aQZ69bz1qla0oqLcm2Dt06e3mTFjprb5n7Qd9vXYGXdXQTsafIKpXYjpN4zAph3RLxutfDBmuKWZfFcj6siqafA+2VI/FOEWwHsY5ZmHkjdJAJw6oVoer6S2Ue6J8flFGvQQSiQl34J3cbcfBAHFd85vSkDUHrD1xwhUw+MUTdGYmiS6AmJj6tsSRV1AO2r7wHyM0Xl+y0Q5o3jRD51pEYG8l6XHkU8H2nkR76jEcEkpV1jKoJ0rkMF0ACmAdEXOAuwY6lzkj5/Ag2R5hnr70wp679eWlIOkGQZA93TCWPnt2oZ3+ICLoAMrkfEkS2sxlA/C8yDktRA9Hgt/6wBrHLmqngNNzUbpybT9CQxy2fXqw9mGS9kRjaZ2CARSLjmAu4jiJgLuHJVwf29tMdWs4Ogxk5eXZ9Rv08ktvXv1uGpop76bVtVrG09x8Smzas1qBdwCwGn/Wef5LDxXMnc5kEYo5PtyBN4V4BxGgStQ+l+xZgEseXvqJMPzO++qE2HgnTpjQd1F0KcoswTedF9bx1saJ6R6CAMXQOllkbVWb6qlN01txqkleKkoxmMCdA/APufU9TgerD9KXIlxGYxypuOxNTWyCUv+BErfm7JkUEbAfx58PIwRKk3wtqd45x9QKmXvAnz+9Rmy7I5c22AI3kNWfv5IGYkPaIfShPaNoW91umQOqE8hwzPwF0IWeXg9X5av48mVHnoPEcNswFeMXB7j/arPRnhvWMBZz3RfIBDo8NaSJeZIQUFEAyXeeV/R9HBmZmaGdxCHg5esrKzW4UjtrmUNpihz1a5dO739URdKS02XTh29LSR69krOTdMA6pROn/68t4lw9Jhx5rfCQiUpfaOeRyTYjjTgz/zWmFChGKXaye8dAU0JFvYTFLgt4dhUlD5GOHQMQc+jwfsmGYxV6LQ1STgeNHW3O+ldm/ndvdII83K5fyxKUIRS9XHC3m4o4gOEYhOduuckzG2F+f1L53sIo+DfNxRqhheRl7rZ0ngAICV8G68UB6T5hMNhB2y+wrv1zccQvJ9QXxmWA3hF/1qMd87GU/ug6I/RiiYxdJ2d7y3gzeXFNf5quz9pv2NOKF+J5/TrlQ3gQjy/GmNsCEWnwXe8QQEXiUSGHTh40MycMcNbOKtELu5IZXXCeWkawDCmNueHN++k9OGhsGneNMu0a3OT91xlkqOB/dEjDcjU5M1v7004a1Qw99WFZuWK5fFQJHXyFSYbK/ACUwDcJYSp0OJdwqq5jsX2LakfSs1HeXyrGqPBdziK7nvSAQAjyP/K6PcNpYyLhGBnHc9msJCj6BdGeG4RfyuxyH4/aSmh1CAs8BxCoRjh81uWRhICllB/5cq7HcORS1nZyOCgo6QjUOyjKFl3PM5gPNdlypuOsvuRw3l/oBSL39aJBJYBinMJobKuX1Bo9+pAmOs/0wV558P3HLx1S/j5Cm/+JINiUQxcGeHzWYyEewzVeUf25dT9Hcr2gaZuw2vU4ZzjfZvQFdH1Am1dDJ/rTQNP2KeUlZV3TE+P7l27bl2mMt326N7NW0Srg+20GLlmSDSlnonTGgBpkax/b+05cPWHj/KIWnOogxeGDx+uo6bMSzk5JnfBK1WBUGhcMBhcVfd9MS/5ThJP0QmFLjK1h+4FsIaNCBnP8lsajZFYRqUz8BBLYp39Eb4qrGYLx2tVA8TLKMZFGiiNZ/37Lpvag0FCkLsPKBU6z98u8FwAf+kJ/YkIlIbCpKBEp5OIvDU8VyOPi7zjQoIcwhivygSD3MrUPV+7CJBGkcnlhHIuJZFjFOD5dTzulNcUAJXijeLc18rxPCWAvz19yUIHNG47xpFLDL4iAM2XZQVtEnE8WcCRbxA+M5DlyYbuwwlwz6ampnrhm7a7u8P71zpcnOzE0PrmRrRtQ56wUWMvj73ZsHGTWZC7wGz/duv+YDgy0YLxm3+OYCYF3I3rxvWfu0I2nMzPmZWzbPfuPf2i0bRL/qmZ0fSoaZqdramhKJ3ceH1g02bLjIyMuP2cdqV7Va4Sd2qB75EjRwLbtm8rPXjgpx9i1dWfKuGQBWT5jSa5cf2fr78EGACH+JjRQ8ncZAAAAABJRU5ErkJggg=='
	},
	footer: function(currentPage, pageCount) { return { text: currentPage.toString() + ' of ' + pageCount,  alignment: 'right', margin: 20}; },
		styles:{
			header: {
			fontSize: 18,
			bold: true,
			margin: [0, 0, 0, 10]
			},
			subheader: {
				fontSize: 16,
				bold: true,
				margin: [0, 10, 0, 5]
			},
			tableExample: {
				margin: [0, 5, 0, 15]
			},
			tableHeader: {
				bold: true,
				fontSize: 13,
				color: 'black'
			},
			client: {
			fontSize: 18,
			bold: true,
			margin: [0, 0, 0, 0]
			},
		}
	};	
	
	//pdfMake.createPdf(reportPDF).open();
	//pdfMake.createPdf(reportPDF).print();
	//pdfMake.createPdf(reportPDF).download('VehicleRunReport.pdf');
	 //var clientlist=[];
	 var clid=$('#clientID').val();
  $.ajax({
       url: 'working_hr_rpt_ctrl/get_client',
       dataType: 'json',
       success: function(data) {
           $.each( data, function( key, value ) {
				if(clid=='1')
					$('#client').append($('<option></option>').val(value['client_id']).html(value['client_name'] ));
				else if(clid==value['client_id']){
					$('#client').html('');
					$('#client').append($('<option></option>').val(value['client_id']).html(value['client_name'] ));
					vh_gp(clid);					
				}
			   //clientlist[key]=value['client_name'];
        	 });
       },
       error: function(e) {
		   $('#client').append($('<option></option>').html("--- No Client Found ---"));
           console.log(e.responseText);
       }
    });
	
	$('#client').on('change',function(){
		$('#vehicleGp').html('');
		$('#vehicle').html('');
		$('#allVh').attr('checked', false);
		var id=$(this).val();		
		if(id)
		{
			$.ajax({
				type:'GET',
				url: 'working_hr_rpt_ctrl/get_vehiclegp/'+id,
				dataType: 'json',				
				success: function(data) {
					$('#vehicleGp').html('<option>--- Select Vehicle Group ---</option>');					
					$.each( data, function( key, value ) {        	  
						$('#vehicleGp').append($('<option></option>').val(value['id']).html(value['value'] ));
					});
				},
				error: function(e) {
					console.log(e.responseText);
				}
			});
		}
	});
	
	function vh_gp(id)
	{
		$('#vehicleGp').html('');
		$('#vehicle').html('');
		$('#allVh').attr('checked', false);				
		if(id)
		{
			$.ajax({
				type:'GET',
				url: 'working_hr_rpt_ctrl/get_vehiclegp/'+id,
				dataType: 'json',				
				success: function(data) {
					$('#vehicleGp').html('<option>--- Select Vehicle Group ---</option>');					
					$.each( data, function( key, value ) {        	  
						$('#vehicleGp').append($('<option></option>').val(value['id']).html(value['value'] ));
					});
				},
				error: function(e) {
					console.log(e.responseText);
				}
			});
		}
	}
	
	$('#vehicleGp').on('change',function(){	
		$('#vehicle').html('');
		$('#allVh').attr('checked', false);	
		var id=$(this).val();
		if(id)
		{
			$.ajax({
				type:'GET',
				url: 'working_hr_rpt_ctrl/get_vehicle/'+id,
				dataType: 'json',				
				success: function(data) {
					$('#vehicle').html('');
					$.each( data, function( key, value ) {        	  
						$('#vehicle').append('<input type="checkbox" name="Vehicle"  title="'+value['value']+'" value="'+value['id']+'" />' + value['value']+'<br>');
					});
				},
				error: function(e) {
					$('#vehicle').html('--- Select vehcile Group first ---');
					console.log(e.responseText);
				}
			});
		};
	});	 
	
	$('#allVh').on('click', function(){
		var con=false;
		 if ($(this).is(':checked')) {
			 con=true;
		 }else{
			 con=false;
		 }
		 $.each($("input[name='Vehicle']"), function(){
			 $(this).prop('checked', con);
		 });
	});	
	
	$("#submit").on('click', function(){		
		var fdate=$('#f_date').val();	
		var distance=0.0;
		var count=0;
		vhnmae='';
		var ClientFullName=$("#client :selected").text();
		reportPDF.content[1].table.body=[];
		reportPDF.content[1].table.body.push([{ text: 'Sl. No.', style: 'tableHeader' }, { text: 'Vehicle', style: 'tableHeader'}, { text: 'Working Hrs | Halt Hrs', style: 'tableHeader' },{ text: 'Total Distance', style: 'tableHeader' }]);				
		reportPDF.content[0].table.body[0][0].text='Working Hour Report ('+ClientFullName+')';
		reportPDF.content[0].table.body[1][0].text='For Date : '+$( "#f_date" ).val();
		reportPDF.content[0].table.body[2][0].text='Vehicle Group : '+$("#vehicleGp :selected").text();	
		if ($('#allVh').is(':checked'))
		{
			vhnmae='--All--';
		}
		else{
			$.each($("input[name='Vehicle']:checked"), function(){
				vhnmae+=$(this).attr('title')+', ';
			});
		}
		reportPDF.content[0].table.body[3][0].text='Vehicle(s) : '+vhnmae;	
		var slno=0;
		$.each($("input[name='Vehicle']:checked"), function(){
			var latLngPoint=[];
			var vhid=$(this).val();
			var vhnmae=$(this).attr('title');				
			var crnt_epo=0, crnt_epo_hlt=0;
			var pre_epo=0, pre_epo_hlt=0;
			var epo_diff=0, epo_diff_hlt=0;
			var actualRUNtime = 0, actualHALTtime = 0;
			var runHr=0, runMn=0, hltHr=0, hltMn=0;			
			count=0;			
			$.ajax({
				type:'GET',
				url: 'working_hr_rpt_ctrl/get_vehicle_run_data/'+vhid+'/'+fdate,
				dataType: 'json',	
				async: false,				
				success: function(data) {
					slno++;					
					count=0;
					distance=0;
					if( data.length >= 1 )
					{						
						data.sort(function(a, b){return a.epo-b.epo});
						$.each( data, function( key, value ) {	
							if((parseFloat(value['spd'])*1.852) > parseFloat(0.5))
							{
								crnt_epo=value['epo'];
								epo_diff=(crnt_epo-pre_epo)/60;
								if(epo_diff<=5)		
									actualRUNtime+=epo_diff;	
								latLngPoint[count]=new google.maps.LatLng(value['lat'],value['lng']);
								count++;
								
							}else{
								crnt_epo_hlt=value['epo'];
								epo_diff_hlt=(crnt_epo_hlt-pre_epo_hlt)/60;
								if(epo_diff_hlt<=5)		
									actualHALTtime+=epo_diff_hlt;								
							}
							pre_epo=value['epo'];
							pre_epo_hlt=value['epo'];
														
						});							
						distance=(google.maps.geometry.spherical.computeLength(latLngPoint)).toFixed(2);
						if(distance>=1000)
						{
							distance=(distance/1000).toFixed(2);
							distance+=" km";
						}
						else
							distance+=" m";						
						runHr=parseInt(actualRUNtime/60);
						runMn=parseInt(((actualRUNtime/60)-runHr)*60); 
						hltHr=parseInt(actualHALTtime/60); 
						hltMn=parseInt(((actualHALTtime/60)-hltHr)*60);	
						runHr=str_pad(runHr,2);
						runMn=str_pad(runMn,2);
						hltHr=str_pad(hltHr,2);
						hltMn=str_pad(hltMn,2);						
						reportPDF.content[1].table.body.push([''+slno+'',vhnmae,''+runHr+':'+runMn+' | '+hltHr+':'+hltMn+'',distance]);
					}else{
						reportPDF.content[1].table.body.push([''+slno+'',vhnmae,''+'00:00 | 00:00'+'','0m']);
					}
				},
				error: function(e) {								
					console.log(e.responseText);
				}
			});
			
		});		
		//console.log(reportPDF.content[1].table.body);
		pdfMake.createPdf(reportPDF).open();
		//console.log('test');
        pdfMake.createPdf(reportPDF).download('WorkingHrsReport['+fdate+'].pdf');		
	});
	function str_pad(value, length) {
    	return (value.toString().length < length) ? str_pad("0"+value, length):value;
	}
	/*
	$('#test').click(function(){
		var vhnmae='';
		var ClientFullName=$("#client :selected").text();		
		reportPDF.content[0].table.body[0][0].text='Vehicle Report ('+ClientFullName+')';
		reportPDF.content[0].table.body[1][0].text='Date : '+$( "#f_date" ).val();
		reportPDF.content[0].table.body[2][0].text='Vehicle Group : '+$("#vehicleGp :selected").text();	
		if ($('#allVh').is(':checked'))
		{
			vhnmae='--All--';
		}
		else{
			$.each($("input[name='Vehicle']:checked"), function(){
				vhnmae+=$(this).attr('title')+', ';
			});
		}
		reportPDF.content[0].table.body[3][0].text='Vehicle(s) : '+vhnmae;
		pdfMake.createPdf(reportPDF).open();
	});
	*/
});

/*
if (typeof someVar === 'undefined') {
  // Your variable is undefined
}
*/
</script>
<div class="container">
	<div class="row">
		<div class="user-container stacked"><br>
			<div class="content clearfix">
				<div class="user-fields">	
					<input type="hidden" id="clientID" name="clientID" value="<?php echo $sess_clientid; ?>"/>
					<h1>Working Hour Report</h1>
					<div class="field">
						<label for="client">Client</label>
						<select name="client" id="client" required>
							<option>--- Select Client --- </option>
						</select>
					</div>
					<div class="field">
						<label for="fromdate">Date</label>
						<input type="Text" name="fromdate" id="f_date" class="form-control input-lg" placeholder="YYYY-MM-DD" readonly /> 
					</div>
					<div class="field">
						<label for="vehicleGp">Vehicle Group</label>
						<select name="vehicleGp" id="vehicleGp" required>
							<option>-- Select Vehicle Group ---</option>
						</select>
					</div>	
					<div class="login-actions">
						<label for="vehicle" style="display:inline">Vehicle</label>
							<input type="checkbox" class="field login-checkbox" style="width:8%; height:16px; box-shadow:none; margin:0px; display:initial;" id="allVh" value="All" /><span class="field login-checkbox" style="width:3%; height:16px; margin:0px; display:initial;">All</span>
							<fieldset id="vehicle" >
								--- Select vehcile Group first ---
							</fieldset>
					</div>					
					<div class="field">
						<button class="btn btn-primary" id="submit" type="button">Submit</button>			
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo base_url("index.php/working_hr_rpt_ctrl/")?>'">Cancel</button>
					</div>
					<!-- <div id="info"></div> -->
				</div>
			</div>
		</div>
	</div>
</div>
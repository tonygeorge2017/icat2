<script type="text/javascript">
$(function() {
	$( "#InstallDate" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });	
	$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
function submit_form(clientid)
{
	document.getElementById("OnClientID").value=clientid.value;	
	document.getElementById("myform").submit();		
}

function device_type_change(typeid)
{	
	var baseUrl=document.getElementById('URL').value;
	var devicetype=typeid.value;
	var clientdevice=document.getElementById("ClientID").value;
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_device/"+devicetype+"/"+clientdevice;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	    	device_for_device_type(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function device_for_device_type(response)
{
	document.getElementById("DeviceID").innerHTML=null;
	var x=document.getElementById("DeviceID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var deviceName="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].device_id;
      deviceName=arrs[j].device_imei;
      adds_device(id,deviceName);    
    }
}
function adds_device(id,deviceName)
{
	var x=document.getElementById("DeviceID");
	var option=document.createElement("option");
	option.text=deviceName;
    option.value=id;
    x.add(option);
}

function vh_gp_change(vhgpid)
{	
	var baseUrl=document.getElementById('URL').value;
	var vehiclegp=vhgpid.value;	
	var clientid=document.getElementById("ClientID").value;	
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_vhehicle_gp/"+clientid+"/"+vehiclegp;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	    	vehicle_for_vehicle_gp(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function vehicle_for_vehicle_gp(response)
{
	document.getElementById("VehicleID").innerHTML=null;
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleName="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vehicle_id;
      vehicleName=arrs[j].vehicle_regnumber;
      adds_vehicle(id,vehicleName);    
    }
}
function adds_vehicle(id,vehicleName)
{
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}
</script>

<style>
#ByAutograde {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <form id="myform" action="<?php echo(base_url("index.php/device_installation_ctrl/validate_device_installation/"))?>" method="post" class="form-horizontal">
           <!-- style="display:none;" -->
          <input type="text" id="OnClientID" name="OnClientID" style="display: none" value="-1"/>         
          <input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/device_installation_ctrl/"))?>"/>
          <input type="text" id="InstallationID" style="display: none" name="InstallationID"  value="<?php echo $installationID ?>"/>
          <h1>Device Installation Details</h1>
          <?php if(isset($outcome)) echo $outcome;?><br>
          <div class="user-fields">
          <div class="field">
              <label for="useremail" <?php if($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER) echo 'style="display: none"'?>>Client:<span <?php echo(($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <select id="ClientID"  name="ClientID" onchange="submit_form(this)" <?php if($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER) echo 'style="display: none"'?>>  
                <?php if($isEdit!=ACTIVE):?>
               <?php if(null!=form_error('ClientID'))echo '<option value="">'.form_error('ClientID',' ',' ').'</option>'; ?>            
               <?php endif;?>
               <?php if($clientList!=null): if(($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER ) & null==form_error('ClientID') & $isEdit!=ACTIVE){echo'<option value=""></option>';} foreach ($clientList as $row):?>
               	<?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
              		<option value="<?php echo $row['client_id']?>" <?php echo(($clientID==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
                <?php elseif($sessClientID==$row['client_id']): ?>
              		<option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
              	<?php endif;?>
               <?php endforeach; endif;?>
              </select>
            </div>
            
         	<div class="field">
              <label for="useremail">Device Type:<span style="color:red;"> *</span></label>
              <select id="DeviceTypeID"  name="DeviceTypeID" onchange="device_type_change(this)"> 
               <?php if($isEdit!=ACTIVE):?>               
               <option value=""><?php if(null!=form_error('DeviceTypeID'))echo form_error('DeviceTypeID',' ',' '); ?></option>
               <?php endif;?>
               <?php if($deviceTypeList!=null): foreach ($deviceTypeList as $row):?>
               		<option value="<?php echo $row['device_type_id']?>" <?php echo(($deviceTypeID==$row['device_type_id'])?'selected':'')?>><?php echo $row['device_type_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            
            <div class="field">
              <label for="useremail">Device IMEI No.:<span style="color:red;"> *</span></label>
              <select id="DeviceID"  name="DeviceID">
              <?php if($isEdit!=ACTIVE):?>  
              <option value=""><?php if(null!=form_error('DeviceID'))echo form_error('DeviceID',' ',' '); ?></option>
               <?php endif;?>
               <?php if($deviceList!=null): foreach ($deviceList as $row):?>
               		<option value="<?php echo $row['device_id']?>" <?php echo(($deviceID==$row['device_id'])?'selected':'')?>><?php echo $row['device_imei']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            
            <div class="field">
              <label for="useremail">Vehicle Group:<span style="color:red;"> *</span></label>
              <select id="VehicleGroupID"  name="VehicleGroupID" onchange="vh_gp_change(this)">
              <?php if($isEdit!=ACTIVE):?>  
              <option value=""><?php if(null!=form_error('VehicleGroupID'))echo form_error('VehicleGroupID',' ',' '); ?></option>
              <?php endif;?>
               <?php if($vehicleGroupList!=null):  foreach ($vehicleGroupList as $row):?>
               		<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vehicleGroupID==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            
            <div class="field">
              <label for="useremail">Vehicle:<span style="color:red;"> *</span></label>
              <select id="VehicleID"  name="VehicleID">
               <?php if($isEdit!=ACTIVE):?>  
              <option value=""><?php if(null!=form_error('VehicleID'))echo form_error('VehicleID',' ',' '); ?></option>
              <?php endif;?>
               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
               		<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vehicleID==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            
            <?php 
            $date_value=new DateTime($installedDate);
            $client_time=$date_value->format('Y-m-d');
			?>
			<div class="field">
				<label>Installed Date:<span style="color:red;"> *</span></label>
				<input style="font-size:13px;" maxlength="10" id="InstallDate" name="InstallDate" class="form-control input-lg" value="<?php echo $client_time; ?>" placeholder="<?php echo((null!=form_error('InstallDate'))?form_error('InstallDate',' ',' '):'yyyy-mm-dd');?>" />
			</div>
      		<div class="field">
        		<label>Sim Number:</label>
        		<input style="font-size: 13px;" maxlength="10" type="text" id="SIMNumber" name="SIMNumber" class="form-control input-lg" value="<?php echo $sim_number ?>" placeholder="<?php if(null!=form_error('SIMNumber'))echo form_error('SIMNumber',' ',' ');?>" />
      		</div>
      		<div class="field">
        		<label>Provider:</label>
        		<input style="font-size: 13px;" maxlength="20" type="text" id="Provider" name="Provider" class="form-control input-lg" value="<?php echo $provider ?>" placeholder="<?php if(null!=form_error('Provider'))echo form_error('Provider',' ',' ');?>" />
      		</div>
      		<div class="login-actions">
				<span class="login-checkbox"> <input style="" id="ByAutograde" name="ByAutograde" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($isAtcSim==ACTIVE)? 'checked':'')?>> 
				<label class="choice" for="Field" style="width:100%" >Sim given by Autograde</style></label>
				</span>
			</div>
			<div class="field">
				<label>Remarks:</label>
				<textarea style="font-size: 13px; float: left;" id="Remarks" name="Remarks" cols="" rows="" class="form-control input-lg" ><?php echo $remarks ?></textarea>
			</div>
			</div>
          <!-- /login-fields -->
		 <div class="field">
            <button class="btn btn-primary" type="submit"><?php echo(($isEdit==ACTIVE)?'Update':'Attach')?></button>
            <?php $reloadURL=base_url("index.php/device_installation_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->																		
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          <div class="table-responsive">
            <table width="100%" class="user-dts">
              <tr>
                <th>Device(IMEI)</th>
                <th>Sl.No.</th>
                <th>SIM No.</th>
                <th>Vehicle</th>
                <th>Action</th>
              </tr>
              <?php if($deviceInstalledList!=null): foreach ($deviceInstalledList as $row):?>
               	<tr>
              		<td><?php echo $row['device_name']?></td>
              		<td><?php echo $row['device_slno'] ?></td>
              		<td align="center"><?php echo (($row['sim_no']!=null)?$row['sim_no']:'--') ?></td>
               		<td><?php echo $row['vehicle_name']?></td>
               		<td><a href="<?php echo(base_url("index.php/device_installation_ctrl/edit_remove_device_istallation/".md5(trim($row['dev_install_client_id']))."/".md5(trim($row['dev_install_id']))."/".md5('edit')))?>" title="Edit Installation Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp;
               			<a href="<?php echo(base_url("index.php/device_installation_ctrl/edit_remove_device_istallation/".md5(trim($row['dev_install_client_id']))."/".md5(trim($row['dev_install_id']))))?>" onclick="return confirm('Do you want to remove <?php echo $row['device_name'].' from the '.$row['vehicle_name']?> vehicle?')"><i title="Remove" class="fa fa-trash-o"></i></a>
               		</td>
              	</tr>
               <?php endforeach; endif;?>
            </table>
             <nav class="pull-right">
              <ul class="pagination pagination-sm"> 
              <?php echo $pageLink?>
              </ul>
            </nav>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
      <!-- /content -->
    </div>
  </div>
</div>
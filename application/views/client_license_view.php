<script type="text/javascript">

function monthsDropdown() {
	var x = document.getElementById("license_duration").value;
	var expiry_date = $("#ClientLicenseExpiryDate").val();

	var currentDate = new Date();
	var day = currentDate.getDate();
	if(parseInt(day)<10)
	{
		day="0"+day;
	}
	var month = currentDate.getMonth() + 1;
	if(parseInt(month)<10)
	{
		month="0"+month;
	}
	var year = currentDate.getFullYear();
	var today = year+"-"+month+"-"+day;

	if(expiry_date >= today)
	{
		var dateString = expiry_date;
		var myDate = new Date(dateString);
		//add a day to the date
		var mor = myDate.setDate(myDate.getDate() + 1);
		var dt = new Date(mor);
		var day = dt.getDate();
		var month = dt.getMonth() + 1;
		var year = dt.getFullYear();
		var new_start_date = year+"-"+month+"-"+day;
		
		var x_date = new Date(new_start_date);

		var dt1 = x_date.setDate(x_date.getDate() - 1);
		var dt1_new = new Date(dt1);
		var day = dt1_new.getDate();
		var month = dt1_new.getMonth() + 1; //x is the value from the dropdown
		var year = dt1_new.getFullYear();
		var new_expiry_date = year+"-"+month+"-"+day;
// 		alert(new_expiry_date + "is the new expiry date with decreasing - 1day" + x + "is the dropdown val");


		var xy_date = new Date(new_expiry_date);
		var y = x;
		var dt2 = xy_date.setMonth(xy_date.getMonth() + parseInt(x));
		var dt2_new = new Date(dt2);
		var day = dt2_new.getDate();
		var month = dt2_new.getMonth() + 1; //x is the value from the dropdown
		var year = dt2_new.getFullYear();
		var new_expiry_date_final = year+"-"+month+"-"+day;
		
		var elem_expiry = document.getElementById("NextExpiry");
		if(document.getElementById("license_duration").value != "0")
		{
			elem_expiry.value = new_expiry_date_final;
			var elem_start = document.getElementById("ClientLicenseStartDate");
			elem_start.value = new_start_date;
		}
		else
		{
			elem_expiry.value = "";
		}
		
	}
	else
	{
		var new_start_date = today;
		var x_date = new Date(new_start_date);
		var dt1 = x_date.setDate(x_date.getDate() - 1);
		var dt1_new = new Date(dt1);
		var day = dt1_new.getDate();
		var month = dt1_new.getMonth() + 1; //x is the value from the dropdown
		var year = dt1_new.getFullYear();
		var new_expiry_date = year+"-"+month+"-"+day;

		var xy_date = new Date(new_expiry_date);
		var y = x;
		var dt2 = xy_date.setMonth(xy_date.getMonth() + parseInt(x));
		var dt2_new = new Date(dt2);
		var day = dt2_new.getDate();
		var month = dt2_new.getMonth() + 1; //x is the value from the dropdown
		var year = dt2_new.getFullYear();
		var new_expiry_date_final = year+"-"+month+"-"+day;
		var elem_expiry = document.getElementById("NextExpiry");
		elem_expiry.value = new_expiry_date_final;

		var elem_start = document.getElementById("ClientLicenseStartDate");
		elem_start.value = new_start_date;
	}
}
</script>

<style>
.form-horizontal{
color:green;
}
#ClientIsActive{
width:15px;
height:15px;
margin-bottom: -14px;
margin-right: -14px;
}
</style>

<div class="container">
	<div class="row">
	<div class="user-container stacked"><br>
		<div class="content clearfix">
		<form id="myform" action="<?php echo(base_url("index.php/client_license_ctrl/vts_client_license_validation/"))?>" method="post" class="form-horizontal">
			<input style="display:none" type="text" id="ClientLicenseId"  name="ClientLicenseId" value="<?php echo((isset($client_license_id))? $client_license_id:null) ?>"/>
			<input style="display:none" type="text" id="Operation"  name="Operation" value="<?php echo $operation; ?>"/>
			<input style="display:none" type="text" id="ClientLicenseClientId"  name="ClientLicenseClientId" value="<?php echo((isset($client_license_client_id))? $client_license_client_id:null) ?>"/>
			<h1>Client License</h1>
			<div><?php if(isset($outcome)) echo $outcome; ?></div><br>
			<div class="user-fields">
			<div class="field">
				<label>Client:<span style="color:red;"> *</span></label>
				<input type="text" id="ClientName" name="ClientName" placeholder="<?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' ');?>" value="<?php echo $client_name; ?>" class="form-control input-lg" readonly/>
			</div>
			<div class="field">
				<label>Start Date:</label>
				<input type="text" id="ClientLicenseStartDate" name="ClientLicenseStartDate" placeholder="<?php if(null!=form_error('ClientLicenseStartDate'))echo form_error('ClientLicenseStartDate',' ',' ');?>" value="<?php if(!isset($outcome))echo $client_license_start_date?>" class="form-control input-lg" readonly/>
			</div>
			<div id="diff">
			<?php	$client_license_start_date." ".date('Y-m-d');
					$datetime1 = date_create($client_license_start_date);
					$datetime2 = date_create(date('Y-m-d'));

					$interval = date_diff($datetime1, $datetime2);
					$diff = $interval->format('%a');//format('%R%a days');
			?>
			</div>
			<div class="field">
				<label>Expiry Date:</label>
				<input type="text" id="ClientLicenseExpiryDate" name="ClientLicenseExpiryDate" placeholder="<?php if(null!=form_error('ClientLicenseExpiryDate'))echo form_error('ClientLicenseExpiryDate',' ',' ');?>" value="<?php if(!isset($outcome))echo $client_license_expiry_date?>" class="form-control input-lg" readonly/>
			</div>
			<div class="field">
				<label>Renewal Date:</label>
				<input type="text" id="ClientLicenseRenewalDate" name="ClientLicenseRenewalDate" placeholder="<?php if(null!=form_error('ClientLicenseRenewalDate'))echo form_error('ClientLicenseRenewalDate',' ',' ');?>" value="<?php echo((isset($client_license_id))? date('Y-m-d'):null) ?>" class="form-control input-lg" readonly/>
			</div>
			<?php if($operation=='renew'):?>
			<div class="field">
				<label>License Duration:<span style="color:red;"> *</span></label>
				<select id="license_duration" name="LicenseDuration" onchange="monthsDropdown()"/>
					<option value="0"></option>
					<option value="1">1 Month</option>
					<option value="3">3 Month(s)</option>
					<option value="6">6 Month(s)</option>
					<option value="12">12 Month(s)</option>
				</select>
			</div>
			<div class="field">
				<label>Next Expiry: </label>
				<input type="text" maxlength="4" id="NextExpiry" name="NextExpiry" placeholder="<?php if(null!=form_error('NextExpiry'))echo form_error('NextExpiry',' ',' ');?>" value="" class="form-control input-lg" readonly/>
			</div>
			<?php endif; ?>
			<div id="test">
				<div class="field">
					<label>User(s):<span style="color:red;"> *</span></label>
					<input type="text" maxlength="4" id="ClientLicenseUsers" name="ClientLicenseUsers"  placeholder="<?php if(null!=form_error('ClientLicenseUsers'))echo form_error('ClientLicenseUsers',' ',' ');?>" value="<?php if(!isset($outcome))echo $client_license_users?>" class="form-control input-lg" <?php echo(($operation=="cancel")?'readonly':'');?>/>
				</div>
				<div class="field">
					<label>Vehicle(s):<span style="color:red;"> *</span></label>
					<input type="text" maxlength="4" id="ClientLicenseVehicles" name="ClientLicenseVehicles" placeholder="<?php if(null!=form_error('ClientLicenseVehicles'))echo form_error('ClientLicenseVehicles',' ',' ');?>" value="<?php if(!isset($outcome))echo $client_license_vehicles?>" class="form-control input-lg" <?php echo(($operation=="cancel")?'readonly':'');?>/>
				</div>
				<div class="field">
					<label>Driver(s):<span style="color:red;"> *</span></label>
					<input type="text" maxlength="4" id="ClientLicenseDrivers" name="ClientLicenseDrivers" placeholder="<?php if(null!=form_error('ClientLicenseDrivers'))echo form_error('ClientLicenseDrivers',' ',' ');?>" value="<?php if(!isset($outcome))echo $client_license_drivers?>" class="form-control input-lg" <?php echo(($operation=="cancel")?'readonly':'');?>/>
				</div>
			</div>
			<div class="field">
				<label>Remarks: </label>
				<textarea type="text" id="ClientLicenseRemark" name="ClientLicenseRemark" class="form-control input-lg"><?php echo(($operation=='cancel')?$client_license_cancel_remarks:$client_license_remark) ?></textarea>
			</div>
		</div>
		<!-- /login-fields -->
		<?php
			$check='';
			if($operation =='cancel')
			{
				$check=($active=='0')?'checked':'';
			}
			else if($operation =='edit' || $operation =='renew')
			{
				$check=($active=='1')?'checked':'';
			}
			else {
				$check='';
			}
		?>
		<div class="login-actions"> <span class="login-checkbox">
			<input style="" id="ClientLicenseIsActive" name="ClientLicenseIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4"  <?php echo(($operation=='cancel')?'onclick="return false" '.$check:''.$check);?> >
			<label class="choice" for="Field">Active</label>
			</span>
		</div>

		<div class="field">
		<?php //if($operation=='cancel' || $operation=='edit' || $operation=='renew'):?>   
			<button class="btn btn-primary" type="submit"><?php echo(($operation=='renew')?'Renewal':'Update')?></button>
			<?php $reloadURL=base_url("index.php/client_license_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
			<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
		<?php //endif; ?>
		</div>

	</form>
<!--	for editing the details of the client license details.. -->
		<form method="post" action="<?php echo(base_url("index.php/client_license_ctrl/editOrRenewOrCancel_vts_client_license/"))?>">
			<div class="table-responsive">
				<table width="100%" class="user-dts" >
					<tr>
						<th>Client</th>
						<th>Expiry</th>
						<th>Contact</th>
						<th colspan="3">Action</th>
					</tr>
				<?php foreach ($clientLicenseList as $row): 
						$datetime1 = new DateTime($row['client_license_start_date']);
						$datetime2 = new DateTime(date('Y-m-d'));
						$interval = date_diff($datetime1, $datetime2);
						$day_diff = $interval->format('%a'); //output returns in days

						$datetime3 = new DateTime($row['client_license_expiry_date']);
						$interval = $datetime3->diff($datetime2);
						$months_diff = $interval->format('%m');//output returns in months
				?>
					<tr>
						<td><?php echo trim($row['client_name'])?></td>
						<td><?php echo trim($row['client_license_expiry_date'])?></td>
						<td><?php echo trim($row['client_contact_person'])?></td>
						<td style="padding:0px;">
							<?php if($row['client_license_is_active']=='1'):?>
							<?php if($day_diff <= 14):?><!-- here 14 is the "days" different between start date and todays date -->
							<a id="edit" href="<?php echo(base_url("index.php/client_license_ctrl/editOrRenewOrCancel_vts_client_license/?client_license_id=edit-".trim($row['client_license_id'])))?>" title="Edit the Details"><i class="fa fa-pencil-square-o"></i></a>
						</td><?php endif; ?><?php endif; ?>
						<td id="can" style="padding-left:12px;">
							<a href="<?php echo(base_url("index.php/client_license_ctrl/editOrRenewOrCancel_vts_client_license/?client_license_id=cancel-".trim($row['client_license_id'])))?>" <?php echo(($row['client_license_is_active']=='1')?'title="Click to Cancel"':'title="Click to Activate"')?><?php echo(($row['client_license_is_active']=='1')?'<i class="glyphicon glyphicon-remove"></i>':'<i class="glyphicon glyphicon-ok"></i>')?></a>
						</td>
						<td style="padding-left:0px;">
							<?php if($row['client_license_is_active']=='1'):?>
							<?php if($months_diff <= 1):?> <!-- here 1 is the "month" difference between expiry date and the current date -->
							<a href="<?php echo(base_url("index.php/client_license_ctrl/editOrRenewOrCancel_vts_client_license/?client_license_id=renew-".trim($row['client_license_id'])))?>" title="Renew the Details" ><i class="glyphicon glyphicon-bell"></i> </a>
							<?php endif; ?><?php endif; ?>
						</td>
					</tr>
					<?php endforeach; ?>
			</table>
			</div>
		</form>

		<nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $pageLink;?>
			</ul>
		</nav> 
	<div class="clearfix"></div>

	</div>
	<!-- /content --> 
	</div>
</div>
</div>
<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}

#DealerIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/dealer_ctrl/vts_dealer_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<!--  <input type="text" id="DealerId"  name="DealerId" value="<?php //echo((isset($dler_id))? $dler_id:null) ?>" />-->
					<input type="text" style="display:none;" id="DealerId"  name="DealerId" value="<?php echo((isset($dler_id))? $dler_id:null) ?>" />
					
					<h1>Dealer Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>
					
					<div class="user-fields">
						<div class="field">
							<label>Name:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="100" id="DealerName" name="DealerName" value="<?php if(!isset($outcome))echo $dler_name?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerName'))echo form_error('DealerName',' ',' ');?>" />
						</div>
						<div class="field">
				              <label>Distributor Name:<span style="color:red;"> *</span></label>
				              <select name="DistributorId">
				              <option value=""><?php if(null!=form_error('DistributorId'))echo form_error('DistributorId',' ',' '); ?></option>
				              <?php foreach ($distributorList as $row):?>
				              <option value="<?php echo $row['dist_id']?>" <?php echo(($dealer_distr_id==$row['dist_id'])?'selected':'')?>><?php echo $row['dist_name']?> </option>
				              <?php endforeach;?>
				              </select>
			            </div>
						<div class="field">
							<label>Location: </label>
							<input type="text" maxlength="40" id="DealerLocation" name="DealerLocation" value="<?php if(!isset($outcome))echo $dler_location?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerLocation'))echo form_error('DealerLocation',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Address: </label>
							<textarea type="text" maxlength="500" id="DealerAddress" name="DealerAddress" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerAddress'))echo form_error('DealerAddress',' ',' ');?>" ><?php if(!isset($outcome))echo $dler_address?></textarea>
						</div>
						<div class="field">
							<label>Postal Code: </label>
							<input type="text" maxlength="20" id="DealerPostCode" name="DealerPostCode" value="<?php if(!isset($outcome))echo $dler_post_code?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerPostCode'))echo form_error('DealerPostCode',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Person:<span style="color:red;"> *</span></label>
							<input type="text" id="DealerContactPerson" name="DealerContactPerson" value="<?php if(!isset($outcome))echo $dler_contact_person?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerContactPerson'))echo form_error('DealerContactPerson',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Contact Designation: </label>
							<input type="text" maxlength="40" id="DealerContactDesignation" name="DealerContactDesignation" value="<?php if(!isset($outcome))echo $dler_contact_designation?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerContactDesignation'))echo form_error('DealerContactDesignation',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Email:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="40" id="DealerContactEmail" name="DealerContactEmail" value="<?php if(!isset($outcome))echo $dler_contact_email?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerContactEmail'))echo form_error('DealerContactEmail',' ',' ');?>" />						</div>
						<div class="field">
							<label>Mobile Number:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="10" id="DealerContactPhone" name="DealerContactPhone" value="<?php if(!isset($outcome))echo $dler_contact_phone?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerContactPhone'))echo form_error('DealerContactPhone',' ',' ');?>" />
						</div>
						<div class="field">
			            	<label>Remarks:</label>
			            	<textarea type="text" maxlength="500" id="DealerRemarks" name="DealerRemarks" class="form-control input-lg" placeholder="<?php if(null!=form_error('DealerRemarks'))echo form_error('DealerRemarks',' ',' ');?>"><?php if(!isset($outcome))echo $dler_remarks?></textarea>
			            </div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="DealerIsActive" name="DealerIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>> 
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br/>
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($dler_id==null)?"Save":"Update")?> </button>
            			<?php $reloadURL=base_url("index.php/dealer_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/dealer_ctrl/editOrDelete_vts_dealer/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						  <tr>
							 <th>Dealer</th>
							 <th>Location</th>
							 <th>Mobile</th>
							 <th>Action</th>
						  </tr>
			              <?php foreach ($DealerList as $row): ?> 
			              <tr>
							 <td><?php echo trim($row['dealer_name'])?></td>
							 <td><?php echo trim($row['dealer_location'])?></td>
							 <td><?php echo trim($row['dealer_contact_phone'])?></td>
							 <td>
							    <a href="<?php echo(base_url("index.php/dealer_ctrl/editOrDelete_vts_dealer/?dealer_id=edit-".trim($row['dealer_id'])))?>" title="Edit Dealer Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
								<a href="<?php echo(base_url("index.php/dealer_ctrl/editOrDelete_vts_dealer/?dealer_id=delete-".trim($row['dealer_id'])))?>" title="Delete Dealer Details" onclick="return confirm('Do you want to delete the Dealer')"><i class="fa fa-trash-o"></i></a>
							 </td>
						  </tr>
			              <?php endforeach; ?>
            		  </table>
				 </div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
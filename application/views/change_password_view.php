 <script>
$(function(){
    $("#username").autocomplete({
    	source:function(request, response) 
    	{          	
    	    $.getJSON("<?php echo base_url('index.php/change_password_ctrl/get_user_list_name')?>", { userName : $('#username').val() }, response);
    	}
    });       
 });
</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="account-container stacked">
      <div class="content clearfix">
        <form action="<?php echo(($showCurrentPwd)? base_url("index.php/change_password_ctrl/field_validation/".md5('show')):base_url("index.php/change_password_ctrl/field_validation/".md5('dshow')))?>" method="post">
          <h1>Change Password</h1>
          <div class="login-fields">
            <div class="field">
              <?php if(isset($outcome)) echo $outcome;?>
              <label for="username">Username:</label>
              <!-- value="<?php //echo  set_value('username')?>" -->
              <input type="text" id="username" name="username"  value="<?php echo(isset($user_name)?$user_name:'') ?>" placeholder = "<?php if(null!=form_error('username'))echo form_error('username',' ',' ');else echo 'Username'; ?>" class="form-control input-lg username-field"  <?php echo $readOnly ?>>
            </div>
            <!-- /field -->
            
            <?php if($showCurrentPwd):?><!-- Check whether the Current password field want to show or not -->
            <div class="field">
              <label for="password">Current Password:</label>
              <input type="password" maxlength="20"  id="CurrentPassword" name="CurrentPassword" value="<?php echo $current_pwd;?>" placeholder="<?php if(null!=form_error('CurrentPassword'))echo form_error('CurrentPassword',' ',' ');else echo 'Current Password'; ?>" class="form-control input-lg password-field">
            </div>
            <?php endif;?>
            
			<div class="field">
              <label for="password">New Password:</label>
              <input type="password"  maxlength="20"  id="NewPassword" name="NewPassword" value="" placeholder="<?php if(null!=form_error('NewPassword'))echo form_error('NewPassword',' ',' ');else echo 'New Password'; ?>" class="form-control input-lg password-field">
            </div>
			<div class="field">
              <label for="password">Confirm Password:</label>
              <input type="password" maxlength="20" id="ConfirmPassword" name="ConfirmPassword" value="" placeholder="<?php if(null!=form_error('ConfirmPassword'))echo form_error('ConfirmPassword',' ',' ');else echo 'Confirm Password'; ?>" class="form-control input-lg password-field">
            </div>
            <div class="field" for="password">
            	<span style="font-size: 11px;color: blue;">Your Password must be at least 10 characters long. </br> Please use only English alphabets, Numbers and Special characters for your password.</span>
            </div>
            <!-- /password -->
          </div>
          <div class="field">
            <button class="btn btn-primary" type="submit">UPDATE</button>
            <?php $reloadURL=($showCurrentPwd)? base_url("index.php/change_password_ctrl/"):base_url("index.php/change_password_ctrl/")?>
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          <!--<div class="clearfix"></div> -->
          <!-- .actions -->
        </form>
      </div>
      <!-- /content -->
    </div>
  </div>
</div> 
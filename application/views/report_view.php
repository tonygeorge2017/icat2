<!-- Login -->
 <div class="container"> 
 <!-- <div class="row"> -->
    <!-- <div class="user-container stacked"> <br>-->
     <!--  <div class="content clearfix"> -->       
     <h6 class="linkred">Report&nbsp;:&nbsp;<?php echo $vehicle_name?></h6>
            <h6 class="linkred">From&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $from_date?>&nbsp;&nbsp;To&nbsp;:&nbsp;<?php echo $to_date?></h6> 
          <div class="table-responsive" style="height: 508px;">            
            <table width="100%" class="user-dts">
              <tr>      
              	<th>Running No</th> <th>Date &amp; Time</th> <th>Latitude</th> 
              	<th>Longitude</th> <th>Speed(km/h)</th>  <th>Ignition Status</th><th>Driver</th>              	                                      
              </tr>
              <?php if($gps_result!=null):
              	$diff=$ID['sess_time_zonediff'];
              	 foreach ($gps_result as $row): ?>
              <?php  
              	$dates=date_create_from_format("Y-m-d H:i:s",$row['gps_datetime']);
				$date_now=date_format($dates,"Y-m-d H:i:s");
				$diffs=$diff*60;
				//log_message("error","TimeDiff2--".$diffs);
				$date_value=new DateTime($date_now);
				$date_value=$date_value->modify($diffs." minutes");
				$client_time=$date_value->format('Y-m-d H:i:s');
			  ?>                
              <tr>                
                <td><?php echo $row['gps_runningno']?></td>               
                <td><?php echo $client_time?></td>
                <td><?php echo round($row['gps_latitude'],4)//$row['gps_latitude']?></td>
                <td><?php echo round($row['gps_longitude'],4)//$row['gps_longitude']?></td>
                <td><?php echo round($row['gps_speed']*1.852,2)?></td>   
                <td><?php echo (($row['gps_digitalinputstatus']=='Y')?'ON':'OFF')//." (".$row['gps_digitalinputstring'].")"?></td>
                <td><?php echo $row['driver_name']?></td>                                         
              </tr>
              <?php endforeach; endif; ?>              
            </table>
          </div>
          <div class="clearfix"></div>
     <!--  </div> -->
      <!-- /content --> 
    <!-- </div> -->
  <!-- </div> -->
</div>
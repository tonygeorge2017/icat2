<div class="container">
	<div class="row">
		<div class="user-container stacked" style="width: 100%">
			<br>
			<div class="content clearfix">
				<h1>Speed Violations Graph</h1>
				<div class="col-md-offset-2 col-md-10">
					<?php echo $bar_chart; ?>
				</div>
				
			</div>
			
		</div>
	</div>
</div>

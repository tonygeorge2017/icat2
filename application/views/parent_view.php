<script>

/*
 * This function is used to clear all student details
 * input field to add new student.
 * Return type - void
 */
function clear_all()
{
	document.getElementById('ModalStopID').value="";	
	document.getElementById('StudentID').value="";
	document.getElementById('StudentRFID').value="";
	document.getElementById('StudentRFValue').value="";
	document.getElementById('StudentName').value="";
	document.getElementById('StudentClass').value="";		
	document.getElementById('routeID').value="";
	document.getElementById('StopID').value="";
}
/*
 * This function triggered by onchange event of the hidden edit check box.
 * Return type - void
 */
function onchange_editOrError()
{	
	if(document.getElementById('EditOrError').checked==true)
	{		
		document.getElementById('AddBtn').click();
		document.getElementById('EditOrError').checked=false;
	}	
}

/*
 * This function triggered by onclick event of the ADD student btn.
 * Return type - void
 */
function Add_btn_clicked()
{
  if(document.getElementById('EditOrError').checked!=true)
    clear_all();
}

 $(function(){

    $( "#MobileNo" ).autocomplete({
      delay:500,
      source: function(request, response){
        $("#ParentName").val('');
        $("#ParentEmail").val('');
        if($("#MobileNo").val().length >= 5)
        {
          console.log($("#MobileNo").val().length);
          $.getJSON("<?php echo base_url("index.php/parent_ctrl/get_existing_parent") ?>",{mob:$("#MobileNo").val(), cid:$("#ClientID").val()},response);
        }
      },
      select: function(event,ui){
        window.location = "<?php echo base_url("index.php/parent_ctrl/edit_parentManagement") ?>/"+ui.item.pid;
      }
    });

    $( "#StudentRFValue" ).autocomplete({
      delay: 500,
      appendTo: "#autocomp",
      source: function(request, response){
        $("#StudentRFID").val('');
        var current_operation=($("#OldId").val())?$("#OldId").val():'0';
        if($("#StudentRFValue").val().length >=3 )
          $.getJSON("<?php echo base_url('index.php/parent_ctrl/get_rf_details')?>", {rf:$("#StudentRFValue").val(), opt:current_operation, cid:"<?php echo $clientID ?>"},response);
      },
      select:function (event, ui)
        {
          $("#StudentRFID").val(ui.item.id);
        }
    });

    $("#ClientID").on('change',function(){
      $("#temp").val($(this).val());
      $("#myform").submit();
    });

    $("#routeID").on('change', function(){
      $("#StopID").html('');
      $routeID=$(this).val();
      $.getJSON("<?php echo base_url('index.php/parent_ctrl/get_stop');?>/"+$routeID,function(result_data){
        $.each(result_data,function(key,value){
          $("#StopID").append("<option value="+value.stop_id+">"+value.stop_name+"</option>");
        });
      })
    });
 });
</script>
<style>
	#StudentActive, #ActiveRfNotify, #ActiveSpeedNotify, #ActiveRouteNotify{
		width:15px;
		height:15px;	
		margin-right: -14px;
	}
</style>
<!--Student Modal (sub modal) -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clear_all()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Student Details</h4>
        <?php if(isset($studentoutcome)) echo $studentoutcome;?>
      </div>
      <div class="modal-body">
        <form method="post" id="upload_file" action="<?php echo(base_url("index.php/parent_ctrl/vts_student_validation"))?>" class="form-horizontal" >
         <!-- style="display: none;" -->
         <input type="text"  id="ModalParentID" style="display: none;" name="ModalParentID" value="<?php echo $parentID ?>"/>
         <input type="text"  id="ModalStopID" style="display: none;" name="ModalStopID" value="<?php echo $parentStopID ?>"/>
         <input type="text"  id="ModalClientID" style="display: none;" name="ModalClientID" value="<?php echo $clientID ?>"/>
         <input type="text" id="OldId" style="display:none;" name="OldId" value="<?php echo((isset($studentOldRfid))? $studentOldRfid:null) ?>" />
          <div class="user-fields">

            <div class="field">
              <label for="StudentID">Student ID/Roll No.:<span style="color:red;"> *</span></label>
              <input type="text" id="StudentID" name="StudentID" value="<?php echo $studentID?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('StudentID'))echo form_error('StudentID',' ',' '); ?>" maxlength="20"/>
            </div>
            <div class="field" id="autocomp">
              <label for="StudentID">RF ID:</label>
              <input type="hidden"  id="StudentRFID" name="StudentRFID" value="<?php echo $studentRfid ?>" />
              <input type="text"  maxlength="40" id="StudentRFValue" name="StudentRFValue" value="<?php echo  $studentRfvalue ?>" class="form-control" />
            </div>
            <div class="field">
              <label for="StudentName">Student Name:<span style="color:red;"> *</span></label>
              <input type="text" id="StudentName" name="StudentName" value="<?php echo $studentName ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('StudentName'))echo form_error('StudentName',' ',' '); ?>" maxlength="100"/>
            </div>
            <div class="field">
              <label for="Vehicle">Class:<span style="color:red;"> *</span></label>
              <input type="text" id="StudentClass" name="StudentClass" value="<?php echo $studentClass ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('StudentClass'))echo form_error('StudentClass',' ',' '); ?>" maxlength="20"/>
            </div>
            <div class="field">
              <label for="routeID">Route:<span style="color:red;"> *</span></label>
              <select id="routeID" class="form-control input-lg" name="routeID">
              <option value=""><?php if(null!=form_error('routeID'))echo form_error('routeID',' ',' '); ?></option>
               <?php if($routeList!=null): foreach ($routeList as $row):?>
               		<option value="<?php echo $row['r_id']?>" <?php echo(($routeID==$row['r_id'])?'selected':'')?>><?php echo $row['r_val']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div class="field">
              <label for="useremail">Stop:<span style="color:red;"> *</span></label>
              <select id="StopID" class="form-control input-lg" name="StopID">
              <option value=""><?php if(null!=form_error('StopID'))echo form_error('StopID',' ',' '); ?></option>
               <?php if($stopList!=null): foreach ($stopList as $row):?>
               		<option value="<?php echo $row['stop_id']?>" <?php echo(($stopID==$row['stop_id'])?'selected':'')?>><?php echo $row['stop_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div class="login-actions">
            	<span class="login-checkbox">
            		<input id="ActiveRfNotify" name="ActiveRfNotify" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($activeRfNotify==ACTIVE)? 'checked':'')?>>
            		<label class="choice label_full_per" for="active">RF ID Notification</label>
            	</span>
            </div>
			<br><br><br>
			<div class="login-actions">
            	<span class="login-checkbox">
            		<input id="ActiveRouteNotify" name="ActiveRouteNotify" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($activeRouteNotify==ACTIVE)? 'checked':'')?>>
            		<label class="choice label_full_per" for="active">Route Violation Notification</label>
            	</span>
            </div>
			<br><br><br>
			<div class="login-actions">
            	<span class="login-checkbox">
            		<input id="ActiveSpeedNotify" name="ActiveSpeedNotify" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($activeSpeedNotify==ACTIVE)? 'checked':'')?>>
            		<label class="choice label_full_per" for="active">Over-Speed Notification</label>
            	</span>
            </div> 
			<br><br><br>
			<div class="login-actions">
            	<span class="login-checkbox">
            		<input id="StudentActive" name="Active" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($studentActive==ACTIVE)? 'checked':'')?>>
            		<label class="choice label_full_per" for="active">Active</label>
            	</span>
            </div>
          </div>
          <div class="clearfix"></div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="<?php echo(($parentStopID==null)?"ADD":"Update") ?>" />
       <!--  <button type="reset" class="btn btn-primary">Reset</button> -->
      </div>
       </form>
      </div>
    </div>
  </div>
</div>

<!-- Parent modal(main modal) -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <!-- style="display: none;" -->
       <form id="myform" action="<?php echo(base_url("index.php/parent_ctrl/vts_parent_validation/"))?>" method="post" class="form-horizontal">
       <input id="EditOrError" name="EditOrError" style="display: none;" type="checkbox" class="field login-checkbox" value="1" <?php echo(($editOrError == ACTIVE)? 'checked':'')?> onchange="onchange_editOrError()" />
        <input type="text" id="ParentID" style="display: none;" name="ParentID" value="<?php echo $parentID ?>"/>
        <input type="text" id="URL" style="display: none;" name="URL" value="<?php echo base_url("index.php/parent_ctrl/") ?>"/>
        <input type="text" id="temp" style="display: none;" name="temp" value="-1"/>
          <h1>Parent Management</h1>
          <?php if(isset($parentoutcome)) echo $parentoutcome;?><br />
          <div class="user-fields">
             <div class="field">
        <label <?php if($sessClientID!=AUTOGRADE_USER) echo 'style="display: none"'?>>Client:<span style="color:red;"> *</span></label>
          <select style="font-size: 15px;<?php if($sessClientID!=AUTOGRADE_USER) echo 'display: none;'?>" id="ClientID"  name="ClientID" class="form-control input-lg">
           <?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER){echo((null!=form_error('ClientID'))?'<option value="">'. form_error('ClientID',' ',' ').'</option>':'<option value=""></option>');} foreach ($clientList as $row):?>
           <?php if($sessClientID==AUTOGRADE_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
           <option value="<?php echo $row['client_id']?>" <?php echo(($clientID==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
            <?php elseif($sessClientID==$row['client_id']): ?>
           <option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
           <?php endif;?>
          <?php endforeach; endif;?>
         </select>
       </div>
            <div class="field">
              <label for="mobileno">Mobile No.:<span style="color:red;"> *</span></label>
              <input type="text" id="MobileNo" name="MobileNo" maxlength= "10" value="<?php echo $parentMobile?>" tabindex="3" class="form-control input-lg"  placeholder="<?php if(null!=form_error('MobileNo'))echo form_error('MobileNo',' ',' '); ?>" maxlength="20"/>
            </div>
          	<div class="field">
              <label for="parentname">Parent Name:<span style="color:red;"> *</span></label>
              <input type="text" id="ParentName" name="ParentName" value="<?php echo $parentName?>" tabindex="1" class="form-control input-lg" placeholder="<?php if(null!=form_error('ParentName'))echo form_error('ParentName',' ',' '); ?>" maxlength="100"/>
            </div>
            <div class="field">
              <label for="email">Login E-mail:<span style="color:red;"> *</span></label>
              <input type="text" id="ParentEmail" name="ParentEmail" value="<?php echo $parentEmail?>" tabindex="2" class="form-control input-lg"  placeholder="<?php if(null!=form_error('ParentEmail'))echo form_error('ParentEmail',' ',' '); ?>" maxlength="50"/>
            </div>
          </div>
          <!-- /login-fields -->
           <div class="login-actions"> <span class="login-checkbox">
            <input id="Active" name="Active" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($parentActive==ACTIVE)? 'checked':'')?>>
            <label class="choice" for="active">Active</label>
            </span> </div>
          <div class="field">
            <button name="submitBtn" class="btn btn-primary" type="submit" value="submit"><?php echo(($parentID==null)?"Save":"Update") ?></button>
            <?php $reloadURL=base_url("index.php/parent_ctrl/")?>	<!-- '$reloadURL' created for onclick of cancel btn-->																	
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>	
          </form> 		
 		 <?php if($parentID!=null):?> 		
         <div class="pull-right">         	
           	 <button id="AddBtn" type="button" title="Add Student" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="Add_btn_clicked()">Add Children</button>
         </div>
          <?php echo "<br><br>"?>
         <h6 class="linkred">Student Details</h6>
          <div class="table-responsive">          	
            <table width="100%" class="user-dts">
              <tr>
                <th>Student Name</th>
                <th>Class</th>
                <th>Stop</th>
                <th>Action</th>
              </tr>
              <?php if($studentList!=null): foreach ($studentList as $row):?>
              <tr>
              	<td><?php echo $row['pt_stop_student_name']?></td>
                <td><?php echo $row['pt_stop_student_class']?></td>
                <td><?php echo $row['stop_name']?></td>
               <td><a title="Edit Student details" href="<?php echo(base_url("index.php/parent_ctrl/edit_parentManagement/".md5($row['pt_stop_parent_id'])."/".md5($row['pt_stop_id'])))?>"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php endforeach; else: ?>
              <tr><td colspan="4"> No record.</td></tr>
              <?php endif; ?>
            </table>
          </div>
		 <?php endif;?>
	<!-- --------------------------------------------------------------------------------- -->	
		    <div class="table-responsive">
            <h6 class="linkred">Existing Parents</h6>
            <table width="100%" class="user-dts">
              <tr>
                <th>Parent</th>
                <th>Stud. Count</th>
                <th>Email</th>
                <th>Mobile No.</th>
                <th>Action</th>
              </tr>
              <?php if($parentList!=null): foreach ($parentList as $row):?>
              <tr>
              	<td><?php echo $row['parent_name']?></td>
              	<td><?php echo $row['child_count']?></td>
                <td><?php echo $row['parent_email']?></td>
                <td><?php echo $row['parent_mobile']?></td>
               <td><a href="<?php echo(base_url("index.php/parent_ctrl/edit_parentManagement/".md5($row['parent_id'])))?>" title="Edit Parent Details"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php endforeach; else: ?>
              <tr><td colspan="5"> No record.</td></tr>
              <?php endif; ?>
            </table>
          </div>
          <nav class="pull-right">
			<ul class="pagination pagination-sm">
				<?php echo $pageLink;?>
			</ul>
		 </nav>		
      </div>
      <!-- /content -->
    </div>
  </div>
</div>
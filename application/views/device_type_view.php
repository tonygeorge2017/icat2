<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
</style>

<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/device_type_ctrl/vts_device_type_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="DeviceTypeId" style="display:none;"  name="DeviceTypeId" value="<?php echo((isset($dvice_type_id))? $dvice_type_id:null) ?>" />
					<h1>Device Type Details</h1>
					
          			<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
          			<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
          			
          			</br>
					<div class="user-fields">
						<div class="field">
							<label>Device Type:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="20" id="DeviceTypeName" name="DeviceTypeName" value="<?php if(!isset($outcome))echo $dvice_type_name?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DeviceTypeName'))echo form_error('DeviceTypeName',' ',' ');?>" />
						</div>
					</div>
					<!-- /login-fields -->

					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($dvice_type_id==null)?"Save":"Update")?> </button>
            			<?php $reloadURL=base_url("index.php/device_type_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/device_type_ctrl/editOrDelete_vts_device_type/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						  <tr>
							 <th>Device Type</th>
							 <th>Action</th>
						  </tr>
			              <?php foreach ($DeviceTypeList as $row): ?> 
			              <tr>
							 <td><?php echo trim($row['device_type_name'])?></td>
							 <td>
							    <a href="<?php echo(base_url("index.php/device_type_ctrl/editOrDelete_vts_device_type/?device_type_id=edit-".trim($row['device_type_id'])))?>" title="Edit the Device Type"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
								<a href="<?php echo(base_url("index.php/device_type_ctrl/editOrDelete_vts_device_type/?device_type_id=delete-".trim($row['device_type_id'])))?>" title="Delete the Device Type" onclick="return confirm('Do you want to delete the selected Device Type?')"><i class="fa fa-trash-o"></i></a>
							 </td>
						  </tr>
			              <?php endforeach; ?>
            		  </table>
				 </div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
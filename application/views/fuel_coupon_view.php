<script src="//maps.googleapis.com/maps/api/js?libraries=geometry&key=<?php echo MAP_API_KEY; ?>"></script>
<script src="<?php echo base_url('assets/js/pdfmake.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/vfs_fonts.js')?>"></script>
<script type="text/javascript">
	$(function(){
		var fuelCouponCopy=['Department Copy','Driver Copy','Fuel Bunk Copy'];
		var CONST_time_diff_tolerance=30;
		var userType=$('#userType').val();
		var clid=$('#clientID').val();
		var urls=$('#URL').val();
		var vehicleDetails=[];		
		var bunkDetails=[];
		var remainingFuel=0;
		$( "#f_date" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });
		$('#rcorners').hide();
		$('#info').html('');
		$('#infoDisance').html('');
		$('#infoVehicle').html('');
		$('[data-toggle="tooltip"]').tooltip();		

		$.ajax({
			url: urls+'/get_client',
			dataType: 'json',
			success: function(data){
				$.each( data, function( key, value ) {
					if(userType=='1')
						$('#client').append($('<option></option>').val(value['client_id']).html(value['client_name'] ));
					else if(clid==value['client_id']){
						$('#client').html('');
						$('#client').append($('<option></option>').val(value['client_id']).html(value['client_name'] ));
					}							   
        		});					
				if(clid)
				{
					$('#client').val(clid);
					get_vhGp(clid);
				}
			},
			error:function(e)
			{
				$('#client').append($('<option></option>').html("--- No Client Found ---"));
          		 ////console.log(e.responseText);
			}
		});

		$('#client').on('change',function(){
			remainingFuel=0;
			$('#clientChangeID').val($(this).val());
			$( "#fuelCouponForm" ).submit();
		});

		$('#vehicleGp').on('change',function(){
			$('#rcorners').fadeOut(300);
			remainingFuel=0;
			vehicleDetails=[];
			$('#vehicle').html('<option value="">-- Select --</option>');
			if($(this).val())		
				get_vh($(this).val());
		});		

		$('#vehicle').on('change', function(){	
			remainingFuel=0;					
			if($(this).val())
			{
				$('#infoDisance').html('');
				$('#fuel').val('0');
				//$('#fuelBunk').val('0');
				var id=$(this).val();
				$('#infoVehicle').html("<p><b>Mileage :</b> "+vehicleDetails[id].mil+" <b>km per liter</b></p>");
				$('#infoVehicle').append($("<p></p>").html("<b>Tolerance (Fuel Consumption) :</b> "+vehicleDetails[id].con+" liter"));
				$.ajax({
					url:urls+'/get_last/'+id,
					dataType: 'json',
					success: function(data){						
						if(data.length==0)
						{
							$('#rcorners').fadeOut(300);
							$('#info').html("<p><b>Date :</b> -- </p>");
							$('#info').append($("<p></p>").html("<b>Fuel Bunk :</b> --"));
							$('#info').append($("<p></p>").html("<b>Fuel (ltr) :</b> --"));
						}
						else
						{
							$('#rcorners').fadeIn(300);
							$('#info').html("<p><b>Date :</b> "+data[0]['pdate']+"</p>");
							$('#info').append($("<p></p>").html("<b>Fuel Bunk :</b> "+data[0]['pbnk']));
							$('#info').append($("<p></p>").html("<b>Fuel (ltr) :</b> "+data[0]['pfuel']));
							$('#info').append($("<p></p>").html("<b>Excess Fuel (ltr) :</b> "+data[0]['pexfuel']));
							$('#info').append($("<p></p>").html("<b>Total Fuel (ltr) :</b> <span style=\"color:green\"><b>"+(parseFloat(data[0]['pfuel']) + parseFloat(data[0]['pexfuel'])).toFixed(2)+"</b></span>"));
							vehicleDetails[id].pdate = data[0]['pdate'];
							vehicleDetails[id].pfuel = data[0]['pfuel'];
							vehicleDetails[id].pexfuel = data[0]['pexfuel'];			
						}
					}
				});
			}
			else
			{				
				$('#rcorners').fadeOut(300);								
			}
		});

		$("#calculateDist").on('click',function(){
			var id=$("#vehicle").val();
			if(id)
			{
				if(vehicleDetails[id].mil!=null & vehicleDetails[id].con!=null)
				{
					if(vehicleDetails[id].pdate && vehicleDetails[id].pfuel && vehicleDetails[id].pfuel>=1)
						get_distance_info_new(vehicleDetails[id].pdate, vehicleDetails[id].pfuel, vehicleDetails[id].pexfuel);
					else
						alert('No previous fuel details to process');
				}else{
					alert('Please set the Mileage and Fuel Tolerance first.');
				}
			}
			else{
				alert('Please select the vehicle first');
			}
		});
		
		
		function get_distance_info_new(predate, prefuel, pexfuel)
		{	
			//console.log('predate:'+predate);
			remainingFuel=0;			
			var misA_reminFuel=0;		
			var misB_reminFuel=0;		
			var tdate=$('#t_date').val();
			//console.log('tdate:'+tdate);			
			var fd = new Date(predate);			
			var td = new Date(tdate);
			//console.log('fd & td:'+fd+' & '+td);
			var n1 = fd.getTime();
			var n2 = td.getTime();
			//console.log('n1 & n2:'+n1+' & '+n2);
			var noOfdays=parseInt(((n2-n1)/(3600*1000))/24);			
			var latLngPoint_move=[];			
			var i=0, distance_move=0;
			var distance="";
			var vhid=$('#vehicle').val();
			var crnt_epo=0, pre_epo=0, epo_diff=0, actualRUNtime=0, runHr=0, runMn=0;
			var startBit="F";
			
			do
			{
				var d1=new Date(predate);		
				d1.setDate(fd.getDate()+i);				
				if(startBit == "F"){
					var searchDate=predate.replace(" ", "T");
					//var searchDate=predate.substring(0, 10)+'T'+predate.substring(11, 8);
				}else{					
					d1.setMinutes(d1.getMinutes()+330);
					var searchDate=d1.toISOString().substring(0, 19);
				}
				console.log('searchDate:'+searchDate);				
				$.ajax({
					url:urls+'/get_rundata/'+vhid+'/'+searchDate+'/'+startBit,
					dataType: 'json',
					async: false,	
					timeout:0,
					success: function(data){
						if(data.length > 0)
						{
							data.sort(function(a, b){return a.epo-b.epo});
							$.each(data, function(key, value){	
								crnt_epo=value['epo'];
								epo_diff=(crnt_epo-pre_epo)/60;
								/* Vehicle in moving status */
								if(parseFloat(value['spd'])*1.852 > parseFloat(vehicleDetails[vhid].ideal) && epo_diff <= CONST_time_diff_tolerance)
									latLngPoint_move[latLngPoint_move.length]=new google.maps.LatLng(value['lat'],value['lng']);
								else if(epo_diff <= CONST_time_diff_tolerance){		/* Vehicle in idle state */	
										actualRUNtime+=epo_diff;
								}
								pre_epo=value['epo'];
							});
						}else
						{

						}
					}
				});
				startBit="N";
				i+=1;
			}while(i <= (noOfdays))

			distance_move=(google.maps.geometry.spherical.computeLength(latLngPoint_move)).toFixed(2);
			if(distance_move>=1000)
			{
				distance_move=(distance_move/1000).toFixed(2);
				distance=distance_move+" km";
			}
			else
			{
				distance=distance_move+"  m";
				distance_move=(distance_move/1000).toFixed(2);
			}
			/*here runHr, RunMn is actually a halt time of the vehicle*/
			runHr=parseInt(actualRUNtime/60);
			runMn=parseInt(((actualRUNtime/60)-runHr)*60);
			runHr=str_pad(runHr,2);
			runMn=str_pad(runMn,2);
			////console.log('Distance:'+distance);
			var consumed_fuel_by_mileage=0;
			var consumed_fuel_by_ideal=0;
			if(distance_move > 0 && parseFloat(vehicleDetails[vhid].mil) > 0)
			{
				consumed_fuel_by_mileage=(distance_move/vehicleDetails[vhid].mil).toFixed(2);
			}
			if(actualRUNtime > 0 && parseFloat(vehicleDetails[vhid].con) > 0)
			{
				consumed_fuel_by_ideal=(parseFloat(actualRUNtime/60) * vehicleDetails[vhid].con).toFixed(2);
			}
			
			$('#infoDisance').html("<p><b>Total Distance (Till Now) :</b> "+distance+" (<span style=\"color:green\"><b>"+consumed_fuel_by_mileage+"</b></span> ltr)</p>");
			$('#infoDisance').append($("<p></p>").html("<b>Total Ideal Time (HH:MM):</b> "+runHr+":"+runMn+" (<span style=\"color:green\"><b>"+consumed_fuel_by_ideal+"</b></span> ltr)"));
			$('#infoDisance').append($("<p></p>").html("<b>Total Fuel Consumed:</b> "+((parseFloat(consumed_fuel_by_mileage) + parseFloat(consumed_fuel_by_ideal)).toFixed(2))+" ltr"));
			
			if(distance_move > 0 )
			{
				if(vehicleDetails[vhid].mil)
				{
					var min_dist=parseFloat(vehicleDetails[vhid].mil)*(parseFloat(prefuel)+parseFloat(pexfuel));
					//console.log("min_dist:"+min_dist);
					if(distance_move < min_dist)
					{			
						misA_reminFuel=	((min_dist-distance_move)*(1/parseFloat(vehicleDetails[vhid].mil)));							
					}	
				}
				if(vehicleDetails[vhid].con && (parseFloat(actualRUNtime/60))>0)
				{					
					misB_reminFuel=(parseFloat(actualRUNtime/60))*vehicleDetails[vhid].con;					
				}
				
				if(misA_reminFuel > 0 && misB_reminFuel > 0 && misA_reminFuel >= misB_reminFuel && (misA_reminFuel - misB_reminFuel)>0 )
				{
					remainingFuel=(misA_reminFuel - misB_reminFuel);
					$('#infoDisance').append($("<p></p>").html("<b>Total Remaining Fuel(Include Ideal Time):</b> <span style=\"color:green\"><b>"+ remainingFuel.toFixed(2)+"</b></span> Liter(s)"));					
				}				
				else
				{
					remainingFuel=0;
					$('#infoDisance').append($("<p></p>").html("<b>Total Remaining Fuel:</b> <span style=\"color:green\"><b>"+ remainingFuel+"</b></span> Liter"));					
				}				
			}
			else			
			{
				remainingFuel=(parseFloat(prefuel)+ parseFloat(pexfuel));
				$('#infoDisance').append($("<p></p>").html("<b>Total Remaining Fuel:</b> <span style=\"color:green\"><b>"+ remainingFuel+"</b></span> Liter"));
			}
		}	

		function str_pad(value, length) 
		{
    		return (value.toString().length < length) ? str_pad("0"+value, length):value;
		}

		function get_vhGp(id)
		{
			$('#vehicleGp').html('<option value="">-- Select --</option>');
			$.ajax({
				url:urls+'/get_vehicle_gp/'+id,
				dataType: 'json',
				success: function(data){					
					$.each( data, function( key, value ) {
						$("#vehicleGp").append($("<option></option>").val(value['id']).html(value['gpname']));
					});
				}
			});
		}

		function get_vh(id)
		{
			$('#vehicle').html('<option value="">-- Select --</option>');
			$.ajax({
				url:urls+'/get_vehicle/'+id,
				dataType: 'json',
				success: function(data){					
					$.each( data, function( key, value ) {
						$("#vehicle").append($("<option></option>").val(value['id']).html(value['vehicle']));
						var ids='v'+value['id'];
						vehicleDetails[value['id']]={'name':value['vehicle'],'mil':value['milage'], 'con':value['consumption'], 'ideal':value['idealspeed'],'pdate':null, 'pfuel':null, 'pexfuel':null};
					});
					//console.log(JSON.stringify(vehicleDetails));
				}
			});
		}
		
		$("#submit").on("click",function(){			
				insert_coupon("later");									
		});
		
		function insert_coupon(print)
		{
			if($('#fuelBunk').val() && $('#fuel').val() && $('#driver').val() && $('#fuelBunkplace').val())
			{
				var fuelLtr=$('#fuel').val();
				if(parseFloat(fuelLtr)>=1)
				{
					$.ajax({
						url:urls+'/save_coupon/'+$('#vehicleGp').val()+'/'+$('#vehicle').val()+'/'+$('#driver').val()+'/'+$('#fuelBunk').val()+'/'+fuelLtr+'/'+$('#sessUserID').val()+"/"+remainingFuel+"/"+print+"/"+$('#fuelBunkplace').val(),
						dataType: 'json',
						success: function(data){
							//console.log(data);							
							if(data['id'] > 0)
							{
								$('#Grid tbody').html('');
								$.each(data['list'],function(key,row){
									$('#Grid tbody').append("<tr><td>"+row['fdate']+"</td><td>"+row['fvhreg']+"</td><td>"+row['fbunk']+"</td><td>"+row['ffuel']+"</td><td>"+row['fex']+"</td></tr>");
								});
								$('#pagelink').html(data['pages']);								
								alert('Fuel details saved');
							}
							else
								alert('Fuel details not saved');
						}
					});
				}
				else
				{
					alert("Minimun 1-ltr required to generate fuel coupon.");
				}
			}
			else
				alert("Please select driver, fuel bunk & enter fuel values in ltr");
		}	
	});
</script>
<style type="text/css">
	.cur
	{
		color:#8a6d3b;
	}	
	.cur:hover 
	{
		cursor:pointer;
		color:#83B74C;
		font-size: 40px;
	}
	#rcorners 
	{
	    border-radius: 25px;
	    border: 2px solid #73AD21;
	    padding: 20px;     
	}
	#printdiv
	{
		border-radius: 5px;
	    border: 2px solid #73AD21;
	    padding: 5px;
	    background-color: #73AD21;
	}
	h4{
		color: #428bca;
	}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<div class="content clearfix">
				<?php $reloadURL=base_url("index.php/fuel_coupon_ctrl/")?>
				<input type="hidden" id="t_date" value="<?php echo date("Y-m-d") ?>"/>
				<input type="hidden" id="userType" value="<?php echo $sess_user_type; ?>"/>
				<input type="hidden" id="sessUserID" value="<?php echo $sess_user_id; ?>"/>
				<input type="hidden" id="URL" value="<?php echo $reloadURL ?>" />
				<form id="fuelCouponForm" action="<?php echo(base_url("index.php/fuel_coupon_ctrl/client_change/"))?>" method="post" class="form-horizontal">
					<input type="hidden" id="clientChangeID" name="clientChangeID" />
				</form>
				<form  action="<?php echo(base_url("index.php/fuel_coupon_ctrl/input_validation/"))?>" method="post" class="form-horizontal">
				<input type="hidden" name="driverGp" id="driverGp" value="128"/>
				<input type="hidden" name="driver" id="driver" value="444"/>
				<input type="hidden" name="fuelBunk" id="fuelBunk" value="5"/>
				<input type="hidden" id="clientID" name="clientID" value="<?php echo $clientID ?>" />
				<input style="display: none" name="print" type="radio" value="later" checked/>
					<h1>Fuel Management</h1>
					<div class="user-fields">
						<div class="field">
							<label for="client">Client: </label>							
							<select name="client" id="client" required >
								<option value=''>--- Select --- </option>
							</select>
						</div>						
						<!--
						<div class="field">
							<label for="driverGp">Driver Group: <span style="color:red;"> *</span></label>							
							<select name="driverGp" id="driverGp" required >
								<option value=''>--- Select --- </option>
							</select>
						</div>						
						<div class="field">
							<label for="driver">Driver: <span style="color:red;"> *</span></label>							
							<select name="driver" id="driver" required >
								<option value=''>--- Select --- </option>
							</select> 
						</div>
						-->
						<div class="field">
							<label for="vehicleGp">Vehicle Group: <span style="color:red;"> *</span></label>
							<select name="vehicleGp" id="vehicleGp" required >
								<option value=''>--- Select --- </option>
							</select>
						</div>
						<div class="field">
							<label for="vehicle">Vehicle: <span style="color:red;"> *</span></label>
							<select name="vehicle" id="vehicle" required >
								<option value=''>--- Select --- </option>
							</select>
						</div>
						<div id="rcorners">	
							<h4>Previous Fuel Info.:</h4>	
							<div id="info" class="field"></div>	
							<h4>Vehicle info. :</h4>
							<div id="infoVehicle" class="field"></div>	
							<h4>Calculation : <i id="calculateDist" data-toggle="tooltip" data-placement="right" title="Click this to start mileage calculation"  class=" fa fa-youtube-play cur"></i></h4>
							<div id="infoDisance" class="field"></div>						
						</div>							
						<div>
							<h4>Add Fuel :</h4>								
							<div class="field">
								<label>Fuel Bunk Location:<span style="color:red;"> *</span></label>
								<input type="text" name="fuelBunkplace" id="fuelBunkplace" maxlength="100" class="form-control input-lg"/>
								<!--<select id="fuelBunk"></select>-->
							</div>
							<div class="field">
								<label>Fuel (ltr):<span style="color:red;"> *</span></label>
								<input type="number" min="1" max="50" id="fuel" value="" class="form-control input-lg" />
							</div>
							<div  class="field">								
								<!--
								<b>Advance Print Option:</b><br>
								<input style="display: hidden" name="print" type="radio" value="later" checked/>
								Save Coupon									
								<br>
								-->
								<button class="btn btn-primary" id="submit" type="button">Save</button>
							</div>							
						</div>				
						
						<div class="table-responsive">
						<table id="Grid" width="100%" class="user-dts">
							<thead>
								<tr>
									<th>Date</th>
									<th>Vehicle Reg. No.</th>
									<th>Place</th>
									<th>Fuel (L)</th>
									<th>Excess Fuel (L)</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($fuel_list as $row): ?> 
									<tr>								
										<td><?php echo trim($row['fdate'])?></td>
										<td><?php echo trim($row['fvhreg'])?></td>
										<td><?php echo trim($row['fbunk'])?></td>
										<td><?php echo trim($row['ffuel'])?></td>
										<td><?php echo trim($row['fex'])?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						</div>	
						<nav class="pull-right">
							<ul id="pagelink" class="pagination pagination-sm">
								<?php echo $pageLink;?>
							</ul>
						</nav>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
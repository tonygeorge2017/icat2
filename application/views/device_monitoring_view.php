 <script>
$(function(){
    $("#AutoClinetName").autocomplete({
    	source:function(request, response) 
    	{  
        	$('#AutoClinetID').val("");
    	    $.getJSON("<?php echo base_url('index.php/device_list_ctrl/get_client_list_name')?>", { client : $('#AutoClinetName').val(), dist: $('#DistributorID').val(), dealer : $('#DealerID').val() }, response);
    	 },
         select:function (event, ui) 
         {
              $('#AutoClinetID').val(ui.item.id);
              client_change();
         }
    });
    $('#DealerID').change(function(){
    	$('#AutoClinetID').val("");
    	$('#AutoClinetName').val("");
        });
    $('#DistributorID').change(function(){
    	$('#AutoClinetID').val("");
    	$('#AutoClinetName').val("");    	
        });   
 });
</script>
<script type="text/javascript">
var fotaObj=new Object();
var tableHeader="";
tableHeader="<table border=\"1\" class=\"user-dts\" style=\"width: 100%; margin: auto;left: 0;right: 0;\">";
tableHeader +="<tr><th style=\"text-align:center;\" rowspan=\"2\">Client</th><th style=\"text-align:center;\" rowspan=\"2\">IMEI No.</th><th style=\"text-align:center;\" rowspan=\"2\">Vehicle</th><th style=\"text-align:center;\" rowspan=\"2\">FOTA</th>";
tableHeader +="<th style=\"text-align:center;\" colspan=\"2\">Received Data &amp; Time</th><th style=\"text-align:center;\" colspan=\"3\">Device Status</th>";
tableHeader +="<th style=\"text-align:center;\" colspan=\"2\">GPS Data Count</th><th style=\"text-align:center;\" rowspan=\"2\">Get Detail</th>";
tableHeader +="</tr><tr><th style=\"text-align:center;\"><i>First Data</i></th><th style=\"text-align:center;\"><i>Last Data</i></th>";
tableHeader +="<th style=\"text-align:center;\"><i>Valid</i></th><th style=\"text-align:center;\"><i>Ignition</i></th>";
tableHeader +="<th style=\"text-align:center;\"><i>Power</i></th><th style=\"text-align:center;\"><i>Valid</i></th><th style=\"text-align:center;\"><i>Invalid</i></th></tr>";
tableHeader +="<tr><td colspan=\"10\">Please wait...</td></tr></table>";
function fota(data)
{
	fotaObj=data;		
}
var myVar=setInterval(function(){get_allDeviceDetails()},3000);

function get_deviceDetails(imeino)
{
	var baseUrl=document.getElementById('URL').value;
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_device_list/"+imeino;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		   
	    	document.getElementById('Details').value=xmlhttp.responseText;
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send()
}
function get_allDeviceDetails()
{
	var query={'imei':'', 'dist':'', 'dealer':'', 'slno':'', 'client':'', 'vhgp':'', 'vh':'', 'alldevice':''};
	var baseUrl=document.getElementById('URL').value;
	
	query.imei=document.getElementById('ImeiNo').value;
	query.dist=document.getElementById('DistributorID').value;
	query.dealer=document.getElementById('DealerID').value;
	query.slno=document.getElementById('SlNo').value;
	query.client=document.getElementById('AutoClinetID').value;
	query.vhgp=document.getElementById('VehicleGroupID').value;
	query.vh=document.getElementById('VehicleID').value;
	if(document.getElementById("AllDevice").checked == true)
		query.alldevice=false;
	else
		query.alldevice=true;
	$.getScript("http://vtstest.hkaagencies.com:1233/vts_rmc_tank/index.php/fota_ctrl/fota_desc_jsonp");
	//$.getScript("http://vtstest.hkaagencies.com:1234/vtstest_app/index.php/fota_ctrl/fota_desc_jsonp");Old
	var json_query=JSON.stringify(query);
	
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/auto_refresh/";	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		    	   
	    	document.getElementById("Grid").innerHTML=xmlhttp.responseText;
			$('#Grid tr').each(function(){ 	    		 
	    		 var scp=$(this).find('td').eq(1).text(); 	    		      		
           		 $(this).find('td').eq(3).text(fotaObj[scp]);
   			 });
	    }
	}
	xmlhttp.open("POST", url, true);	
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("query="+json_query);
}
/*
 * This function is used to get all dealer which is belongs
 * particular distributor.
 * Return type - void
 */
function distributor_change()
{	
	var baseUrl=document.getElementById('URL').value;
	var distributorID=document.getElementById('DistributorID').value;
	document.getElementById("DealerID").innerHTML=null;	
	document.getElementById("VehicleGroupID").innerHTML=null;
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("Grid").innerHTML=tableHeader;	
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_dist_dealer/"+distributorID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    var x=document.getElementById("DealerID");
	    	var option=document.createElement("option");
	    	option.text="";	option.value=""; x.add(option);
	        var arrs = JSON.parse(xmlhttp.responseText);
	        var id="";var dealer="";    
	        for(j=0;j<arrs.length;j++)
	        {
	          id=arrs[j].dealer_id;
	          dealer=arrs[j].dealer_name;
	          add_dealer(id,dealer);    
	        }
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send()
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - dealer id
 *  dealer - dealer name
 * Return type - void
 */
function add_dealer(id,dealer)
{
	var x=document.getElementById("DealerID");
	var option=document.createElement("option");
	option.text=dealer;
    option.value=id;
    x.add(option);
}
/*
 * This function is used to get all vehicle group which is belongs
 * particular client.
 * Return type - void
 */
function client_change()
{	
	//alert("Test"+document.getElementById('AutoClinetID').value);
	var clientID=document.getElementById('AutoClinetID').value;;
	var baseUrl=document.getElementById('URL').value;	
	document.getElementById("VehicleGroupID").innerHTML=null;
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("Grid").innerHTML=tableHeader;
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_client_vhgp/"+clientID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
		{
			var x=document.getElementById("VehicleGroupID");
	    	var option=document.createElement("option");
	    	option.text="";	option.value=""; x.add(option);
	        var arrs = JSON.parse(xmlhttp.responseText);
	        var id="";var vehicleGp="";    
	        for(j=0;j<arrs.length;j++)
	        {
	          id=arrs[j].vehicle_group_id;
	          vehicleGp=arrs[j].vehicle_group;
	          adds_vh_gp(id,vehicleGp);    
	        }
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - vehicle group id
 *  vehicleGp - vehicle group name
 * Return type - void
 */
function adds_vh_gp(id,vehicleGp)
{
	var x=document.getElementById("VehicleGroupID");
	var option=document.createElement("option");
	option.text=vehicleGp;
    option.value=id;
    x.add(option);
}
 /*
  * This function is used to get all vehicle which is related to
  * selected vehicle group.
  * Return type - void
  */
function vehicleGroup_change()
{	
	var baseUrl=document.getElementById('URL').value;
	var vehiclegp=document.getElementById('VehicleGroupID').value;
	var xmlhttp = new XMLHttpRequest();
	document.getElementById("VehicleID").innerHTML=null;	
	document.getElementById("Grid").innerHTML =tableHeader;
	var url = baseUrl+"/get_vhl_grp_vehicle/"+vehiclegp;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	var x=document.getElementById("VehicleID");
	    	var option=document.createElement("option"); 
	    	option.text="";	option.value=""; x.add(option);
	        var arrs = JSON.parse(xmlhttp.responseText);
	        var id="";var vehicleName="";    
	        for(j=0;j<arrs.length;j++)
	        {
	          id=arrs[j].vehicle_id;
	          vehicleName=arrs[j].vehicle_regnumber;
	          adds_vehicle(id,vehicleName);    
	        }	        
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - vehicle group id
 *  vehicleName - vehicle name
 * Return type - void
 */
function adds_vehicle(id,vehicleName)
{
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}
function vehicle_change()
{
	document.getElementById("Grid").innerHTML=tableHeader;	
}
</script>
 	<style>
	#AllDevice{
	width:15px;
	height:15px;	
	margin-right: -14px;
	}
	</style>
<div class="container">
	<div class="row">
	<div class="col-lg-12 dashboard-main">
	<nav class="navbar navbar-inverse" <?php echo(($sessClientID != AUTOGRADE_USER)?'style="display: none;"' :'' );?>>
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-brand">Autograde Admin</span>
    </div>
    <ul class="nav navbar-nav">      
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Distributor &amp; Dealer
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/distributor_ctrl")?>">Distributor</a></li>
         <li><a href="<?php echo base_url("index.php/dealer_ctrl")?>">Dealer</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Device
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/device_type_ctrl")?>">Device Type</a></li>
         <li><a href="<?php echo base_url("index.php/device_ctrl")?>">Device</a></li>
         <li><a href="<?php echo base_url("index.php/device_allocation_dealer_ctrl")?>">Device Allocation to Dealer</a></li>
         <li><a href="<?php echo base_url("index.php/device_allocation_ctrl")?>">Device Allocation to Client</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Client
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/client_ctrl")?>">Client</a></li>
         <li><a href="<?php echo base_url("index.php/client_license_ctrl")?>">Client License</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tester
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url("index.php/device_monitoring_ctrl")?>">Device Monitoring</a></li>
          <li><a href="<?php echo base_url("index.php/vehicle_admin_tracking_ctrl")?>">Tester Tracking</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/service_ctrl")?>">Service</a></li>
         <li><a href="<?php echo base_url("index.php/settings_ctrl")?>">Settings</a></li>
		 <li><a href="<?php echo base_url("index.php/vehicle_make_model_ctrl")?>"> Vehicle Make &amp; Model</a></li>
        </ul>
      </li>
       <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Others
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/individual_user_ctrl")?>">Individual User</a></li>
         <li><a href="<?php echo base_url("index.php/individual_vt_ctrl")?>">Personal Tracking</a></li>
         <li><a href="<?php echo base_url("index.php/distributor_dashboard_ctrl")?>">Distributor Dashboard</a></li>
         <li><a href="<?php echo base_url("index.php/dealer_dashboard_ctrl")?>">Dealer Dashboard</a></li>
        </ul>
      </li>      
    </ul>
  </div>
</nav>
	<h1>Device Monitoring</h1>
	<div class="col-sm-6">
		 <div class="user-container stacked">
			<div class="content clearfix">
			<input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/device_monitoring_ctrl/"))?>"/>
			<form id="QueryForm" action="<?php echo(base_url("index.php/device_monitoring_ctrl/fetch_device_list/"))?>" method="post" class="form-horizontal">
			<div class="user-fields">
		    <div class="field">
				<label>IMEI No.:</label>
				<input type="text" maxlength="15" id="ImeiNo" name="ImeiNo" value="<?php echo $imeiNo ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ImeiNo'))echo form_error('ImeiNo',' ',' ');?>" />
			</div>
			<div class="field">
				<label>SlNo.:</label>
				<input type="text" maxlength="6" id="SlNo" name="SlNo" value="<?php echo $slNo ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('SlNo'))echo form_error('SlNo',' ',' ');?>" />
			</div>
		    <div class="field">
              <label for="useremail">Distributor:</label>
              <select id="DistributorID"  name="DistributorID" onchange="distributor_change()">
              <option value=""></option>
               <?php if($distributorList!=null):  foreach ($distributorList as $row):?>
               		<option value="<?php echo $row['dist_id']?>" <?php echo(($distributorID==$row['dist_id'])?'selected':'')?>><?php echo $row['dist_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div> 
            <div class="field">
              <label for="useremail">Dealer:</label>
              <select id="DealerID"  name="DealerID" onchange="dealer_change()">
              <option value=""></option>
               <?php if($dealerList!=null):  foreach ($dealerList as $row):?>
               		<option value="<?php echo $row['dealer_id']?>" <?php echo(($dealerID==$row['dealer_id'])?'selected':'')?>><?php echo $row['dealer_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>	
            <div class="field" > 
				<label>Client:</label><!-- style="display:none" -->
                <input type="text" id="AutoClinetName" name="AutoClinetName" maxlength="100" value="<?php echo $clientName ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('AutoClinetName'))echo form_error('AutoClinetName',' ',' '); ?>"/>
                <input type="text" style="display:none" id="AutoClinetID" name="ClinetID" value="<?php echo $clientID ?>"  class="form-control input-lg" />
            </div>	  
            <div class="field">
              <label for="useremail">Vehicle Group:</label>
              <select id="VehicleGroupID"  name="VehicleGroupID" onchange="vehicleGroup_change()">
              <option value=""></option>
               <?php if($vehicleGroupList!=null):  foreach ($vehicleGroupList as $row):?>
               		<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vehicleGroupID==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div> 
            <div class="field">
              <label for="useremail">Vehicle:</label>
              <select id="VehicleID"  name="VehicleID" onchange="vehicle_change()">
              <option value=""></option>           
               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
               		<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vehicleID==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div class="login-actions"> <span class="login-checkbox">
            <input id="AllDevice" name="AllDevice" type="checkbox" class="field login-checkbox" value="1"/>
            <label class="choice" for="Field">All</label>
            </span> </div>
		  <div class="clearfix" style="margin-bottom: 6px"></div>
			</div>
			<div class="field" style="display:none">
            <button class="btn btn-primary" type="submit">Show</button>
            <?php $reloadURL=base_url("index.php/device_monitoring_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          </form>
			</div>
			<!-- /content -->
			</div>
			</div>
			
			<div class="col-sm-6">
		<div id="DeviceDetails" class="user-container stacked"> 
			<div class="content clearfix">
				
				<div class="field">
        		<label>Details:</label>
       			<textarea style="font-size: 15px;" id="Details" name="Details" cols="5" rows="16" class="form-control input-lg" readonly><?php echo $deviceDetail ?></textarea>
     	 		</div>
				<div class="field">					
            			<?php $reloadURL=base_url("index.php/device_monitoring_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
					<button class="btn btn-primary" type="button"
						onclick="self.location='<?php echo $reloadURL?>'">Refresh</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
			</div>
			</div>
		</div>
	</div>
</div>
				<div id="Grid" class="table-responsive" style="padding-top: 5px;padding-left:15px; padding-right: 15px;  ">
					<table border="1" class="user-dts" style="width: 100%; margin: auto;left: 0;right: 0;">
						<tr>
							<th style="text-align:center;" rowspan="2">Client</th>
							<th style="text-align:center;" rowspan="2">IMEI No.</th>
							<th style="text-align:center;" rowspan="2">Vehicle</th>
							<th style="text-align:center;" colspan="2">Received Data &amp; Time</th>
							<th style="text-align:center;" colspan="3">Device Status</th>
							<th style="text-align:center;" colspan="2">GPS Data Count</th>
							<th style="text-align:center;" rowspan="2">Get Detail</th>
						</tr>
						<tr>							
							<th style="text-align:center;"><i>First Data</i></th>
							<th style="text-align:center;"><i>Last Data</i></th>
							<th style="text-align:center;"><i>Valid</i></th>
							<th style="text-align:center;"><i>Ignition</i></th>
							<th style="text-align:center;"><i>Power</i></th>
							<th style="text-align:center;"><i>Valid</i></th>
							<th style="text-align:center;"><i>Invalid</i></th>							
						</tr>
			              <?php if(isset($deviceList))foreach ($deviceList as $row):
				              $l_isvalid=($row['chk_new_device_valid']=='A')?'YES':'NO';
				              $l_ispowerup=($row['chk_new_device_power']=='Y')?'YES':'NO';
				              $l_ignition=($row['chk_new_device_ingition']=='Y')?'YES':'NO';
				              $l_clientname=(isset($row['client_name']))?$row['client_name']:'< UNKNOWN >';
				              $l_vhregno=(isset($row['vehicle_name']))?$row['vehicle_name']:'< UNKNOWN >';
			             		 $color=null; 
			             		 if($row['timediff_btw_curnt_lat']==null || $row['timediff_btw_fst_lat']==null)
			             		 {
			             		 	if($row['timediff_btw_curnt_fst'] < DEVICE_ALERT_TIME_DIFF)// the difference betweent the first date and last date is less than the interval mentioned in the constant, then green else red.
			             		 		$color='#CDFF88'; // change the color to green for new device
			             		 	else 
			             		 		$color='#FE9997'; //alert the user by showing the row in red color.
			             		 }
			             		 else if( $row['timediff_btw_fst_lat'] < NEW_DEVICE_UPTO_TIME_DIFF && $row['timediff_btw_curnt_lat'] < DEVICE_ALERT_TIME_DIFF)
			             		 	$color='#CDFF88'; // change the color to green for new device
			             		 else if($row['timediff_btw_curnt_lat'] >= DEVICE_ALERT_TIME_DIFF)//alert the user by showing the row in red color.
			             		 	$color='#FE9997';
			             		 else 
			             		 	$color=null;
			              	?> 
			             
			              <tr <?php echo ((isset($color))?'bgcolor="'.$color.'"':'') ?>><!-- green #7CB000 red #F05133-->
							<td><?php echo $l_clientname ?></td>
							<td><?php echo trim($row['chk_new_device_imei'])?></td>
							<td><?php echo $l_vhregno ?></td>
							<td><?php echo trim($row['first_datetime'])?></td>
							<td><?php echo trim($row['last_datetime'])?></td>
							<td><?php echo $l_isvalid?></td>
							<td><?php echo $l_ignition?></td>
							<td><?php echo $l_ispowerup?></td>
							<td><?php echo trim($row['chk_new_device_valid_count'])?></td>
							<td><?php echo trim($row['chk_new_device_invalid_count'])?></td>
							<td><a href="#DeviceDetails" title="Click this to get the details" onclick="get_deviceDetails('<?php echo md5($row['chk_new_device_imei'])?>')"><i class="glyphicon glyphicon-file"></i></a></td>
							<?php //echo base_url("index.php/device_monitoring_ctrl/get_device_list/".md5($row['chk_new_device_imei'])) ?>
						</tr>
			              <?php endforeach; ?>
            		  </table>
				</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main-content">
				<div class="row">
					<div class="col-md-12">
						<h1 class="page_header"><?= $page_title ?> </h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-2 col-md-8">
						<form class="form-inline">
						  <div class="form-group">
							<label for="exampleInputName2">Date</label>
							<input type="text" class="form-control from_datepicker" id="date_range" value="<?= date("Y-m-d") ?>" placeholder="YYYY-MM-DD">
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail2">Vehicle</label>
							<select class="form-control">
								<option>-- Select Vehicle --</option>
							</select>
						  </div>
						  <button type="button" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i></button>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="toolbar">
							<select class="form-control">
								<option value="">Export Basic</option>
								<option value="all">Export All</option>
								<option value="selected">Export Selected</option>
							</select>
						</div>
						<table id="table" data-show-export="true" data-pagination="true" data-search="true" data-toolbar="#toolbar">
							<thead>
								<tr>
									<th data-field="gps_latitude">Latitude</th>
									<th data-field="gps_longitude">Longitude</th>
									<th data-field="dt">Date</th>
									<th data-field="driver_name">Driver</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
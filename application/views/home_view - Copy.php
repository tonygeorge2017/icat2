<script type="text/javascript">
/*
*function to get dynamic values depend on client selection
*/
function get_values_client(val) {
		var baseUrl = document.getElementById('URL').value;
		var clientid =document.getElementById('ClientID').value;
		document.forms["myform"].submit();
		xmlhttp.open("POST",baseUrl+"/get_drivers/"+drvgpid,true);
	}

/*	
*function to get dynamic values for drivers  function get_no_of_drivers()
*/
	function get_no_of_drivers() {
		var baseUrl = document.getElementById('URL').value;
		var drvgpid =document.getElementById('DriverGroupID').value;
		var clientid =document.getElementById('ClientID').value;

		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("NoDrivers").innerHTML = xmlhttp.responseText;
			grid_data();
			}
		}
		xmlhttp.open("POST",baseUrl+"/get_drivers/"+clientid+"/"+drvgpid,true);
		xmlhttp.send();
	}

/*
*function to get dynamic vehicles
*parametrs are (clientid,vehiclegroupid)
*/
	function get_no_of_vehicles(val1,val2) {
		var baseUrl = document.getElementById('URL').value;
		var vhgpid =document.getElementById('VehicleGroupID').value;
		var clientid =document.getElementById('ClientID').value;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("NoVehicles").innerHTML = xmlhttp.responseText;
			grid_data();
			}
		}
		xmlhttp.open("POST",baseUrl+"/get_vehicles/"+clientid+"/"+vhgpid,true);
		xmlhttp.send();
	}

/*
* function to get middle grid data on passing the parameters(clientid, vehiclegroupid, drivergroupid).
*/
	function grid_data() {
		var baseUrl = document.getElementById('URL').value;
		var drvgpid =(document.getElementById('DriverGroupID').value!="")?document.getElementById('DriverGroupID').value:"0";
		var vhgpid =(document.getElementById('VehicleGroupID').value!="")?document.getElementById('VehicleGroupID').value:"0";
		var clientid =document.getElementById('ClientID').value;
				
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("grid").innerHTML = xmlhttp.responseText;
			grid_speed();
			}
		}

		xmlhttp.open("POST",baseUrl+"/get_grid/"+clientid+"/"+drvgpid+"/"+vhgpid,true);
		xmlhttp.send();
	}

/*
* function to get speed violations grid data on passing the parameters(clientid, vehiclegroupid, drivergroupid).
*/
	function grid_speed() {
		var baseUrl = document.getElementById('URL').value;
		var drvgpid =(document.getElementById('DriverGroupID').value!="")?document.getElementById('DriverGroupID').value:"0";
		var vhgpid =(document.getElementById('VehicleGroupID').value!="")?document.getElementById('VehicleGroupID').value:"0";
		var clientid =document.getElementById('ClientID').value;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("speed").innerHTML = xmlhttp.responseText;
			grid_service();//fucntion to fill data for service status grid
			}
		}
		xmlhttp.open("POST",baseUrl+"/get_speed_grid/"+clientid+"/"+vhgpid+"/"+drvgpid,true);
		xmlhttp.send();
	}

	/*
	* function to get speed violations grid data on passing the parameters(clientid, vehiclegroupid, drivergroupid).
	*/
	function grid_service() {
		var baseUrl = document.getElementById('URL').value;
		var clientid =document.getElementById('ClientID').value;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("service").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("POST",baseUrl+"/get_service_grid/"+clientid,true);
		xmlhttp.send();
	}
	
</script>

<div class="container">
	<div class="row">
		<div class="col-lg-12 dashboard-main">		
		<form id="myform" action="<?php echo(base_url("index.php/home_ctrl/view"))?>" method="post">		
		<nav class="navbar navbar-inverse" <?php echo(($sessClientID != AUTOGRADE_USER)?'style="display: none;"' :'' );?>>
  <div class="container-fluid">
    <ul class="nav navbar-nav">      
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Distributor &amp; Dealer
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/distributor_ctrl")?>">Distributor</a></li>
         <li><a href="<?php echo base_url("index.php/dealer_ctrl")?>">Dealer</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Device
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/device_type_ctrl")?>">Device Type</a></li>
         <li><a href="<?php echo base_url("index.php/device_ctrl")?>">Device</a></li>
         <li><a href="<?php echo base_url("index.php/device_allocation_dealer_ctrl")?>">Device Allocation to Dealer</a></li>
         <li><a href="<?php echo base_url("index.php/device_allocation_ctrl")?>">Device Allocation to Client</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Client
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/client_ctrl")?>">Client</a></li>
         <li><a href="<?php echo base_url("index.php/client_license_ctrl")?>">Client License</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tester
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url("index.php/device_monitoring_ctrl")?>">Device Monitoring</a></li>
          <li><a href="<?php echo base_url("index.php/vehicle_admin_tracking_ctrl")?>">Tester Tracking</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/service_ctrl")?>">Service</a></li>
         <li><a href="<?php echo base_url("index.php/settings_ctrl")?>">Settings</a></li>
		 <li><a href="<?php echo base_url("index.php/vehicle_make_model_ctrl")?>"> Vehicle Make &amp; Model</a></li>
        </ul>
      </li>
       <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Others
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo base_url("index.php/insurance_agency_ctrl")?>">Insurance Agency</a></li>
		 <li><a href="<?php echo base_url("index.php/vehicle_service_ctrl")?>">Vehicle Service</a></li>
         <li><a href="<?php echo base_url("index.php/individual_user_ctrl")?>">Individual User</a></li>
         <li><a href="<?php echo base_url("index.php/individual_vt_ctrl")?>">Personal Tracking</a></li>
         <li><a href="<?php echo base_url("index.php/distributor_dashboard_ctrl")?>">Distributor Dashboard</a></li>
         <li><a href="<?php echo base_url("index.php/dealer_dashboard_ctrl")?>">Dealer Dashboard</a></li>
        </ul>
      </li>    
      <li>
       <select id="ClientID" name="ClientID"  onChange="get_values_client(this.val);" style=" padding: 15px;">   
		<?php if(null!=form_error('ClientID'))echo '<option value="">-- Select Client --</option>'; ?>            
		<?php if($clientList!=null): if($sessClientID==AUTOGRADE_USER & null==form_error('ClientID')){echo'<option value="">-- Select Client --</option>';} foreach ($clientList as $row):?>
		<?php if($sessClientID==AUTOGRADE_USER):?>								
		<option value="<?php echo $row['client_id']?>"	<?php echo(($clientID==$row['client_id'])?'selected':'')?>><?php echo $row['client_name']?> </option>
		<?php elseif($sessClientID==$row['client_id']): ?>
		<option value="<?php echo $row['client_id']?>"><?php echo $row['client_name']?> </option>
		<?php endif;?>
		<?php endforeach; endif;?>
		 </select>
      </li>  
    </ul>
  </div>
</nav>	


				<div class="dash-tiles row">
					<div class="col-sm-3">
						<div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
							<div class="dash-tile-header">

								<i class="fa fa-car"></i> Vehicles In
							</div>
							<input type="Text" style="display: none;" id="URL" name="URL"
								value="<?php echo base_url("index.php/home_ctrl/")?>" />
							<ul class="dash-tile-ul">
								<li><select id="VehicleGroupID" name="VehicleGroupID"
									onChange="get_no_of_vehicles(this.val);">
									<option value="">All</option>
									<?php foreach ($vhgp as $row ){?>
									<option value="<?php echo $row['vehiclegroupid'];?>"><?php echo $row['vehiclegroupname'];?></option>
									<?php } ;?>
								</select></li>
								<li>Number of Vehicles :
									<div class="dash-tile-text1" id="NoVehicles"><?php echo $vehicles ;?></div>
								</li>
								<li>Moving Vehicles :
									<div class="dash-tile-text1"><?php  echo $moving_vehicles ;?></div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="dash-img clearfix">
							<img
								src="<?php echo base_url('assets/images/Dashboard_img1.jpg')?>"
								alt="" />
						</div>
					</div>
					<div class="col-sm-3">
						<div
							class="dash-tile dash-tile-flower clearfix animation-pullDown">
							<div class="dash-tile-header">
								<i class="fa fa-male"></i> Drivers
							</div>
							<ul class="dash-tile-ul">
								<li><select id="DriverGroupID" name="DriverGroupID"
									onChange='get_no_of_drivers();'>
										<option value="">All</option>
										<?php foreach ($drgp as $row ){?>
										<option value="<?php echo $row['drivergroupid'];?>"><?php echo $row['drivergroupname'];?></option>
										<?php };?>
								</select></li>
								<li>Number of Drivers :
									<div class="dash-tile-text1" id="NoDrivers"><?php echo $drivers ; ?></div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="dash-img clearfix">
							<img src="<?php echo base_url('assets/images/Dashboard_img2.jpg')?>" alt="" />
						</div>
					</div>
					<div class="col-sm-12">
						<div style="background-color: Lavender;"
							class="dash-tile dash-tile-oil clearfix animation-pullDown">
							<div class="table-responsive table-thing" id="grid">
								<table>
									<tr>
										<th>Vehicle Running Details</th>
										<th>Today</th>
										<th>This Month</th>
										<th>Last Month</th>
										<th>This Year</th>
									</tr>
								<?php foreach ($grid_data as $row){?>
								<tr>
										<td><?php echo $row['title']; ?></td>
										<td><?php echo $row['today'];?></td>
										<td><?php echo $row['thismonth'];?></td>
										<td><?php echo $row['lastmonth'];?></td>
										<td><?php echo $row['thisyear'];?></td>
									</tr>
								<?php } ?>
							</table>
							</div>
						</div>
					</div>
			
						<div class="col-sm-6" 
						onclick="location.href='<?php echo base_url();?>index.php/speed_violations_ctrl/';">
						<div>
							<a href="<?php echo base_url('index.php/speed_violations_ctrl/');?>">
							<strong>Speed Violations</strong>
						</div>
						</a>
						<div style="background-color: Thistle;"
							class="dash-tile dash-tile-balloon clearfix animation-pullDown">
							<div class="table-responsive table-thing" id="speed">
								<table>
									<tr>
										<th>Car</th>
										<th>From Date</th>
										<th>To Date</th>
										<th>Speed</th>
										<th>Driver</th>
										<th>Spot</th>
									</tr>
									<?php $i = 0; $getIn = false;
											if ($speed_data != null) :
											if (isset($speed_data [0] ['vehicle'])) :
											foreach ( $speed_data as $row ) :
									?>
									<tr>
										<td><?php echo $row['vehicle'];?></td>
										<td><?php echo $row['from_datetime'];?></td>
										<td><?php echo $row['to_datetime'];?></td>
										<td><?php echo $row['maxspeed'];?></td>
										<td><?php echo $row['driver'];?></td>
										<td><?php echo $spot[$i];$i++;?></td>
									</tr>
									<?php endforeach ;?>
									<?php  else: ?>
									<tr>
										<td style="padding-top: 10px;" colspan="4"><?php echo "No records found." ?></td>
									</tr>
									<?php endif; ?>
									<?php  else: ?>
									<tr>
										<td style="padding-top: 10px;" colspan="4"><?php echo "No records found." ?></td>
									</tr>
									<?php endif;?>
								</table>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div>
							<strong>Service Status</strong>
						</div>
						<div style="background-color: Wheat;"
							class="dash-tile dash-tile-doll clearfix animation-pullDown">
							<div class="table-responsive table-thing" id = "service" >
								<table>
									<tr>
										<th>Car</th>
										<th>Date</th>
										<th>Service</th>
										<th>Status</th>
									</tr>
									<?php //if($string2!=null):?>
									<?php  //foreach ($string2 as $row): ?>
									<?php if ($service_data != null) : foreach ( $service_data as $row ) :?>
									<tr>
										<td><?php echo $row ['vehicle'] ; //$row[1]; ?></td>
										<td><?php echo $row ['servicedate'] ;//$row[2];?></td>
										<td><?php echo $row ['service'] ;//$row[4]; ?></td>
										<td><?php echo $row ['status'] ;//$row[5];?></td>
									</tr>
									<?php endforeach; ?>
									<?php else: ?>
									<tr>
										<td style="padding-top: 10px;" colspan="4"><?php echo "No records found." ?></td>
									</tr>
									<?php endif;?>
								</table>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Footer -->

<link href="src/css/bootstrapValidator.css" rel="stylesheet">
<script src="src/js/bootstrapValidator.js"></script>
<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- Script to Activate the Carousel -->
</body>
</html>

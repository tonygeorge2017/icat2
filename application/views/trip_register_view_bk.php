<script type="text/javascript">
function submit_form(clientid)
{
	document.getElementById("OnClientID").value=clientid.value;	
	document.getElementById("myform").submit();		
}

function driver_gp_change()
{		
	var baseUrl=document.getElementById('URL').value;
	var tripid=document.getElementById('TripRegID').value;
	tripid=(tripid!='')?tripid:'-1';
	var drivergpid=document.getElementById('DriverGroupID').value;
	var startdate=document.getElementById("StartDateTime").value;
	var enddate=document.getElementById("EndDateTime").value;
	//alert(drivergpid+","+startdate+", "+enddate);
	if(drivergpid!='' && startdate!='')
	{
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_driver/"+tripid+"/"+drivergpid+"/"+startdate+"/"+enddate;
		//alert(url);		
		xmlhttp.onreadystatechange=function() {
		    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    	
		    	driver_for_driver_gp(xmlhttp.responseText);
		    }
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	else
	{
		document.getElementById("DriverID").innerHTML=null;		
	}
}

function driver_for_driver_gp(response)
{
	document.getElementById("DriverID").innerHTML=null;
	var x=document.getElementById("DriverID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var deviceName="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].driver_id;
      driverName=arrs[j].driver_name;
      adds_driver(id,driverName);    
    }
}

function adds_driver(id,driverName)
{
	var x=document.getElementById("DriverID");
	var option=document.createElement("option");
	option.text=driverName;
    option.value=id;
    x.add(option);
}

function dateTime_change()
{
	document.getElementById('ErrorMsg').innerHTML=null;
	var clientId=document.getElementById('ClientID').value;	
	if(clientId!='')
	{
		vh_gp_change();
		driver_gp_change();
	}
}

function vh_gp_change()
{	
	var baseUrl=document.getElementById('URL').value;
	var tripid=document.getElementById('TripRegID').value;
	tripid=(tripid!='')?tripid:'-1';
	var vehiclegp=document.getElementById('VehicleGroupID').value;	
	var startdate=document.getElementById("StartDateTime").value;
	var enddate=document.getElementById("EndDateTime").value;
	//alert(vehiclegp+","+startdate+", "+enddate);
	if(vehiclegp!='' && startdate!='')
	{	
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_vhehicle_gp/"+tripid+"/"+vehiclegp+"/"+startdate+"/"+enddate;
	//alert(url);	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	
	    	vehicle_for_vehicle_gp(xmlhttp.responseText);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
	}
	else
	{
		document.getElementById("VehicleID").innerHTML=null;
		//alert("Please check the selection of vehicle group and start date & end date..");
	}
}

function vehicle_for_vehicle_gp(response)
{
	document.getElementById("VehicleID").innerHTML=null;
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");    
    var arrs = JSON.parse(response);
    var id="";var vehicleName="";    
    for(j=0;j<arrs.length;j++)
    {
      id=arrs[j].vehicle_id;
      vehicleName=arrs[j].vehicle_regnumber;
      adds_vehicle(id,vehicleName);    
    }
}
function adds_vehicle(id,vehicleName)
{
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}


</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <form id="myform" action="<?php echo(base_url("index.php/trip_register_ctrl/validate_trip_list/"))?>" method="post" class="form-horizontal">
           <!-- style="display:none;" -->
          <input type="text" id="OnClientID" name="OnClientID" style="display: none" value="-1" />  
          <input type="text" id="TripRegID" name="TripRegID" style="display: none" value="<?php echo $tripRegID ?>" />         
          <input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/trip_register_ctrl/"))?>" />
          <h1>Trip Register</h1>
          <?php if(isset($outcome)) echo $outcome;?>
          <div id="ErrorMsg" style="color: red;"></div>
          <div class="user-fields">
          <div class="field">
              <label for="useremail" <?php if($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER) echo 'style="display: none"'?>>Client:<span <?php echo(($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER)?'style="display: none"':'style="color:red"')?>> *</span></label>
              <select id="ClientID"  name="ClientID" onchange="submit_form(this)" <?php if($sessClientID!=AUTOGRADE_USER & $GLOBALS['ID']['sess_user_type'] != DEALER_USER) echo 'style="display: none"'?>>  
               <?php if(null!=form_error('ClientID'))echo '<option value="">'.form_error('ClientID',' ',' ').'</option>'; ?>            
               <?php if($clientList!=null): if(($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER ) & null==form_error('ClientID')){echo'<option value=""></option>';} foreach ($clientList as $row):?>
               	<?php if($sessClientID==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?><!-- if client ID = 1 (i.e. Autograde Client) then only dropdown allow to select different client -->
              		<option value="<?php echo $row['client_id']?>" <?php echo(($clientID==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
                <?php elseif($sessClientID==$row['client_id']): ?>
              		<option value="<?php echo $row['client_id']?>" ><?php echo $row['client_name']?> </option>
              	<?php endif;?>
               <?php endforeach; endif;?>
              </select>
            </div>
            <?php            	
        	    $diff=0;
          		$time=gmdate('Y-m-d H:i:s');
            	if($clientTimeDiff!=null)
            		$diff=$clientTimeDiff*60;
            	$current_date=new DateTime($time);
            	$current_date=$current_date->modify($diff." minutes");
            	$start_date=null;
            	$end_date=null;
            if($startDate==null && $endDate==null)
            {              	
            	$start_date=new DateTime($time);
            	$end_date=new DateTime($time);
            	$start_date=$start_date->modify($diff." minutes");            	
            	$end_date=$end_date->modify(($diff+60)." minutes");            	
            }   
            else
            {	
            	if($startDate!=null)
            	{
	            	$sdates=date_create_from_format("Y-m-d H:i:s",$startDate);
	            	$sdate_now=date_format($sdates,"Y-m-d H:i:s");
	            	$start_date=new DateTime($sdate_now);
            	}            	
            	if($endDate!=null)
            	{
	            	$edates=date_create_from_format("Y-m-d H:i:s",$endDate);
	            	$edate_now=date_format($edates,"Y-m-d H:i:s");
	            	$end_date=new DateTime($edate_now);
            	}            	
            }        
			?>
		<div class="field">
		 <label>Start Date &amp; time:<span style="color:red;"> *</span></label>
     	   <input style="font-size: 15px;" type="datetime-local" id="StartDateTime" name="StartDateTime" class="form-control input-lg" <?php echo(($start_date<=$current_date & !empty($tripRegID))?'readonly':'');?> value="<?php echo(($start_date!=null)?$start_date->format('Y-m-d').'T'.$start_date->format('H:i:s'):'') ?>" onchange="dateTime_change()"/>
    	  </div>
   		   <div class="field">
    	    <label>End Date &amp; time:</label>
   	  	   <input style="font-size: 15px;" type="datetime-local" id="EndDateTime" name="EndDateTime" class="form-control input-lg" value="<?php echo(($end_date!=null)? $end_date->format('Y-m-d').'T'.$end_date->format('H:i:s'):'') ?>" onchange="dateTime_change()"/>
   		   </div>
            <div class="field">
              <label for="useremail">Vehicle Group:<span style="color:red;"> *</span></label>
              <select id="VehicleGroupID"  name="VehicleGroupID" onchange="vh_gp_change(this)">              
              <option value=""><?php if(null!=form_error('VehicleGroupID'))echo form_error('VehicleGroupID',' ',' '); ?></option>
               <?php if($vehicleGroupList!=null):  foreach ($vehicleGroupList as $row):?>
               		<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vehicleGroupID==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>                       
            
            <div class="field">
              <label for="useremail">Vehicle:<span style="color:red;"> *</span></label>
              <select id="VehicleID"  name="VehicleID">   
              <option value=""><?php if(null!=form_error('VehicleID'))echo form_error('VehicleID',' ',' '); ?></option>           
               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
               		<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vehicleID==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            
            <div class="field">
              <label for="useremail">Driver Group:<span style="color:red;"> *</span></label>
              <select id="DriverGroupID"  name="DriverGroupID" onchange="driver_gp_change()">              
              <option value=""><?php if(null!=form_error('DriverGroupID'))echo form_error('DriverGroupID',' ',' '); ?></option>
               <?php if($driverGroupList!=null):  foreach ($driverGroupList as $row):?>
               		<option value="<?php echo $row['driver_group_id']?>" <?php echo(($driverGroupID==$row['driver_group_id'])?'selected':'')?>><?php echo $row['driver_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>                       
            
            <div class="field">
              <label for="useremail">Driver:<span style="color:red;"> *</span></label>
              <select id="DriverID"  name="DriverID">   
              <option value=""><?php if(null!=form_error('DriverID'))echo form_error('DriverID',' ',' '); ?></option>           
               <?php if($driverList!=null): foreach ($driverList as $row):?>
               		<option value="<?php echo $row['driver_id']?>" <?php echo(($driverID==$row['driver_id'])?'selected':'')?>><?php echo $row['driver_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>     
			
        	     		
			<div class="field">
        		<label>Remarks:</label>
       			<textarea style="font-size: 13px;" id="Remarks" name="Remarks" cols="" rows="" class="form-control input-lg" maxlength="500"><?php echo $remarks ?></textarea>
     	 	</div>
          </div>
          <!-- /login-fields -->
		 <div class="field">
            <button class="btn btn-primary" type="submit"><?php echo(($tripRegID==null)?"Save":"Update")?></button>
            <?php $reloadURL=base_url("index.php/trip_register_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          <div class="table-responsive">
            <table width="100%" class="user-dts">
              <tr>
                <th>Driver</th>
                <th>Vehicle</th>
                <th>Start</th>
                <th>End</th>
                <th>Action</th>
              </tr>
              <?php if($tripList!=null): foreach ($tripList as $row):?>
               	<tr>
              		<td><?php echo $row['driver_name']?></td>
               		<td><?php echo $row['vehicle_name']?></td>
               		<td><?php echo $row['trip_register_time_start']?></td>
               		<td><?php echo $row['trip_register_time_end']?></td>
               		<td><a href="<?php echo(base_url("index.php/trip_register_ctrl/manage_trip/".md5(trim($row['trip_register_client_id']))."/".md5(trim($row['trip_id'])))."/".md5('edit'))?>" ><i title="Edit Trip Register" class="fa fa-pencil-square-o"></i></a> &nbsp;
               		<a href="<?php echo(base_url("index.php/trip_register_ctrl/manage_trip/".md5(trim($row['trip_register_client_id']))."/".md5(trim($row['trip_id'])))."/".md5('delete'))?>" onclick="return confirm('Do you want to cancel this trip?')"><i title="Delete Trip Register" class="fa fa-trash-o"></i></a></td>
              	</tr>
               <?php endforeach; endif;?>
            </table>
             <nav class="pull-right">
              <ul class="pagination pagination-sm">
              <?php echo $pageLink?>
              </ul>
            </nav>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
      <!-- /content -->
    </div>
  </div>
</div>
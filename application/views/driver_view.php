<script type="text/javascript">

	window.onload = function (){
		if($("#dobmonth").val() == "mm")
		{
			document.getElementById("dobmonth").disabled=true;
		}
		if($("#dobday").val() == "dd")
		{
			document.getElementById("dobday").disabled=true;
		}
	}

	$(function(){		

		$( "#DriverRFValue" ).autocomplete({
		  delay: 500,		 		  
		  source: function(request, response){	
		  	$("#DriverRFID").val('');	
		  	var current_operation=($("#OldId").val())?$("#OldId").val():'0';	  	
		  	if($("#DriverRFValue").val().length >=3 )		  	
		  		$.getJSON("<?php echo base_url('index.php/driver_ctrl/get_rf_details')?>", {rf:$("#DriverRFValue").val(), opt:current_operation},response);		  	
		  },
		  select:function (event, ui) 
     	  {
     	  	$("#DriverRFID").val(ui.item.id);
     	  } 
		});
		
	});
//This function is used on change of year dropdown 
	function changeYear()
	{
		$("#dobmonth").val("mm");
		$("#dobday").val("dd");
		document.getElementById("dobmonth").disabled=false;
		document.getElementById("dobday").disabled=true;
		
		$yearVal = $("#dobyear").val();
		$monthVal = $("#dobmonth").val();
		
		if($yearVal == "yyyy")
		{
			$("#dobmonth").val("mm");
			$("#dobday").val("dd");
			document.getElementById("dobmonth").disabled=true;
			document.getElementById("dobday").disabled=true;
		}
		
	}

//This function is used on change of month dropdown
	function changeMonth()
	{
		$("#dobday").val("dd");
		document.getElementById("dobday").disabled=false;
		$monthVal = $("#dobmonth").val();
		$yearVal1 = $("#dobyear").val();
		if($monthVal == "mm")
		{
			document.getElementById("dobday").disabled=true;
		}
		else if($yearVal1 % 4 == 0 && $monthVal == '2')
		{
			j = 29;
		}
		else if(($monthVal % 2 == 0 && $monthVal != '2')|| $monthVal == '1')
		{
			j = 31;
		}
		else if($monthVal == '2')
		{	
			j = 28;
		}
		else
		{
			j = 30;
		}

		document.getElementById("dobday").innerHTML=null;
		
		var x=document.getElementById("dobday");
		var option=document.createElement("option");
		option.text="dd";
		option.value="dd";
		x.add(option);
		
		for(i=1;i<=j;i++)
		{
			id=i;
			date=i;
			adds_date(id,date);    
		}
	}

	function adds_date(id,date)
	{
		var x=document.getElementById("dobday");
		var option=document.createElement("option");
		option.text=date;
		option.value=id;
		x.add(option);
	}
	
	$(function() {
		$( "#DriverDateOfBirth" ).datepicker({  maxDate: new Date(), dateFormat: 'yy-mm-dd' });
		$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
	});

	function clientName()
	{
		document.getElementById("temp").value=document.getElementById("ClientName").value;//ClientName
		document.getElementById("myform").submit();
	}
	function vh_gp_change()
	{	
		var baseUrl=document.getElementById('URL').value;
		var vehiclegp=document.getElementById('VehicleGroupID').value;		
		
		if(vehiclegp!='')
		{	
		var xmlhttp = new XMLHttpRequest();
		var url = baseUrl+"/get_vhl_grp_vehicle/"+vehiclegp;	
		xmlhttp.onreadystatechange=function() {
		    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    	
		    	vehicle_for_vehicle_gp(xmlhttp.responseText);
		    }
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
		}
		else
		{
			document.getElementById("VehicleID").innerHTML=null;			
		}
	}

	function vehicle_for_vehicle_gp(response)
	{
		document.getElementById("VehicleID").innerHTML=null;
		var x=document.getElementById("VehicleID");
		var option=document.createElement("option");  
		adds_vehicle("","");
	    var arrs = JSON.parse(response);
	    var id="";var vehicleName="";    
	    for(j=0;j<arrs.length;j++)
	    {
	      id=arrs[j].vh_id;
	      vehicleName=arrs[j].vh_name;
	      adds_vehicle(id,vehicleName);    
	    }
	}
	function adds_vehicle(id,vehicleName)
	{
		var x=document.getElementById("VehicleID");
		var option=document.createElement("option");
		option.text=vehicleName;
	    option.value=id;
	    x.add(option);
	}
</script>

<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}

#DriverIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
					<input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/driver_ctrl/"))?>"/>
				<form id="myform" action="<?php echo(base_url("index.php/driver_ctrl/vts_driver_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" style="display:none;" id="DriverId"  name="DriverId" value="<?php echo((isset($dvr_id))? $dvr_id:null) ?>" />
					<input type="text" style="display:none;" id="OldId"  name="OldId" value="<?php echo((isset($dvr_oldid))? $dvr_oldid:null) ?>" />
					<input type="text" id="temp" style="display: none;"  name="temp" value="-1" />
					<input type="text" id="TripID" name="TripID" style="display: none;" value="<?php echo $dvr_trip_id ?>" />
					<h1>Driver Details</h1>
					
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					</br>
					<div class="user-fields">
						<?php if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
								<select name="ClientName" id="ClientName" onchange="clientName()">
									<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
									<?php foreach ($clientList as $row):?>
									<option value="<?php echo $row['client_id']?>" <?php echo(($dvr_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
									<?php endforeach;?>
								</select>
						</div>
						<?php endif; ?>
				 		<div class="field">
							<label>Name:<span style="color:red;"> *</span></label>
							<input type="text"  maxlength="40" id="DriverName" name="DriverName" value="<?php if(!isset($outcome))echo $dvr_name?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverName'))echo form_error('DriverName',' ',' ');?>" />
						</div>
						<div class="field">
							<label>RF ID:</label>
							<input type="hidden"  id="DriverRFID" name="DriverRFID" value="<?php echo $dvr_rfid ?>" />
							<input type="text"  maxlength="40" id="DriverRFValue" name="DriverRFValue" value="<?php if(!isset($outcome))echo  $dvr_rfvalue ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverRFValue'))echo form_error('DriverRFValue',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Driver Group:<span style="color:red;"> *</span></label>
							<select name="DriverGroup">
								<option value=""><?php if(null!=form_error('DriverGroup'))echo form_error('DriverGroup',' ',' '); ?></option>
								<?php foreach ($driverGroupList as $row):?>
								<option value="<?php echo $row['driver_group_id']?>" <?php echo(($dvr_drivergroup_id==$row['driver_group_id'])?'selected':'')?>><?php echo $row['driver_group']?> </option>
								<?php endforeach;?>
							</select>
						</div>
						<!--  Date Of Birth Starts from here -->
							<?php $splitDOB =explode("-", $dvr_dob); ?>
							<div class="field">
							<table width="100%" >
								<tr>
									<td><div class="field" style="margin-right:66px;">
										<label>DOB: <div id="dob" name="dob"></div></label>
										</div>
									</td>
									<td>
										<select name="dobyear" id="dobyear" onchange="changeYear()" style="width:82px;">
										<option value="yyyy">yyyy</option>
										<?php 
											$current_year = date("Y");
											$last_date = date("Y")-80;
											for($i=$last_date;$i<=$current_year;$i++)
											{
												if($i == $splitDOB[0])
													echo '<option value="'.$i.'" selected>'.$i.'</option>';
												else
													echo '<option value="'.$i.'" >'.$i.'</option>';
											}
										?>
										</select>
									</td>
									<td>
										<select name="dobmonth" id="dobmonth" onchange="changeMonth()" style="width:77px;">
										<option value="mm">mm</option>
										<?php
											for($i=1;$i<=12;$i++){
												$mnth=(isset($splitDOB[1]))?$splitDOB[1]:0;
													if($i == $mnth)
													{
														echo '<option value="'.$i.'" selected>'.$i.'</option>';
													}
													else
													{
														echo '<option value="'.$i.'" >'.$i.'</option>';
													}
												}
										?>
										</select>
									</td>
									<td>
										<select name="dobday" id="dobday" style="width:69px;">
										<option value="dd">dd</option>
										<?php 
											if(isset($splitDOB[2]))
											{
												if($splitDOB[2] == "")
													echo '<option value="dd" selected>dd</option>';
												else
													echo '<option value="'.$splitDOB[2].'"  selected>'.$splitDOB[2].'</option>';
											}
										?>
										</select>
									</td>
								</tr>
							</table>
						</div> 
							<!-- Date Of Birth Ends here -->
							
						<!-- </div> -->
						<div class="field">
							<label>Address: </label>
							<textarea type="text" maxlength="500" id="DriverAddress" name="DriverAddress" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverAddress'))echo form_error('DriverAddress',' ',' ');?>"><?php if(!isset($outcome))echo $dvr_address?></textarea>
						</div>
						<div class="field">
							<label>Postal Code: </label>
							<input type="text" maxlength="20" id="DriverPostCode" name="DriverPostCode" value="<?php if(!isset($outcome))echo $dvr_postcode?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverPostCode'))echo form_error('DriverPostCode',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Phone: </label>
							<input type="text" maxlength="15" id="DriverPhone" name="DriverPhone" value="<?php if(!isset($outcome))echo $dvr_phone?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverPhone'))echo form_error('DriverPhone',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Mobile Number:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="10" id="DriverMobileNumber" name="DriverMobileNumber" value="<?php if(!isset($outcome))echo $dvr_mobile_number?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverMobileNumber'))echo form_error('DriverMobileNumber',' ',' ');?>" />
						</div>
						<div class="field">
							<label>Driving License No: </label>
							<input type="text" maxlength="20" id="DriverDLNo" name="DriverDLNo" value="<?php if(!isset($outcome))echo $dvr_dl_no?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverDLNo'))echo form_error('DriverDLNo',' ',' ');?>" />
						</div>
						<div class="field">
							<label>DL Details:</label>
							<textarea type="text" maxlength="100" id="DriverDLDetails" name="DriverDLDetails" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverDLDetails'))echo form_error('DriverDLDetails',' ',' ');?>"><?php if(!isset($outcome))echo $dvr_dl_details?></textarea>
						</div>
						<div class="field">
							<label>ID Type:</label>
							<select name="DriverIDType">
							<option value=""><?php if(null!=form_error('DriverIDType'))echo form_error('DriverIDType',' ',' '); ?></option>
							<?php foreach ($driverIdTypeList as $row):?>
							<option value="<?php echo $row['config_id']?>" <?php echo(($dvr_id_type==$row['config_id'])?'selected':'')?>><?php echo $row['config_item']?> </option>				              	
							<?php endforeach;?>
							</select>
						</div>
						<div class="field">
							<label>ID Number:</label>
							<input type="text" maxlength="20" id="DriverIDNo" name="DriverIDNo" value="<?php if(!isset($outcome))echo $dvr_id_no?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverIDNo'))echo form_error('DriverIDNo',' ',' ');?>" />
						</div>
						
						<!-- VehicleGroup & Vehicle was added on 06 Oct 2015 by Ramesh G-->
						<div class="field">
			              <label for="useremail">Vehicle Group:</label>
			              <select id="VehicleGroupID"  name="VehicleGroupID" onchange="vh_gp_change()">  
			               <option value=""></option>
			               <?php if($vehicleGroupList!=null):  foreach ($vehicleGroupList as $row):?>
			               		<option value="<?php echo $row['vh_gp_id']?>" <?php echo(($vehicleGroupID==$row['vh_gp_id'])?'selected':'')?>><?php echo $row['vh_gp_name']?> </option>
			               <?php endforeach; endif;?>
			              </select>
			            </div>                       
			            
			            <div class="field">
			              <label for="useremail">Assign Vehicle:</label>
			              <select id="VehicleID"  name="VehicleID">   
			               <option value=""></option>         
			               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
			               		<option value="<?php echo $row['vh_id']?>" <?php echo(($vehicleID==$row['vh_id'])?'selected':'')?>><?php echo $row['vh_name']?> </option>
			               <?php endforeach; endif;?>
			              </select>
			            </div>
            
            
						<div class="field">
							<label>Remarks:</label>
							<textarea type="text" maxlength="500" id="DriverRemarks" name="DriverRemarks" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverRemarks'))echo form_error('DriverRemarks',' ',' ');?>"><?php if(!isset($outcome))echo $dvr_remarks?></textarea>
						</div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="DriverIsActive" name="DriverIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>>
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br />
					</div>
					<!-- /login-fields -->
					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($dvr_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/driver_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post" action="<?php echo(base_url("index.php/driver_ctrl/editOrDelete_vts_driver/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
						<tr>
							<th>Driver Name</th>
							<th>Group</th>
							<th>Mobile</th>
							<th>Actions</th>
						</tr>
						<?php if($driverList!=null): foreach ($driverList as $row): ?> 
						<tr>
							<td><?php echo trim($row['driver_name'])?></td>
							<td><?php echo trim($row['driver_group'])?></td>
							<td><?php echo trim($row['driver_mobile'])?></td>
							<td>
								<?php $md_id = md5(trim($row['driver_client_id']));?>
								<a href="<?php echo(base_url("index.php/driver_ctrl/editOrDelete_vts_driver/".$md_id."/".md5('edit')."/".md5(trim($row['driver_id']))))?>" title="Edit Driver Details"><i class="fa fa-pencil-square-o"></i></a> &nbsp;
								<a href="<?php echo(base_url("index.php/driver_ctrl/editOrDelete_vts_driver/".$md_id."/".md5('delete')."/".md5(trim($row['driver_id']))))?>" title="Delete Driver Details" onclick="return confirm('Do you want to delete the Driver')"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php endforeach; endif;?>
				</table>
				</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
<script>
$(function(){
    $("#AutoClinetName").autocomplete({
    	source:function(request, response) 
    	{  
        	$('#AutoClinetID').val("");
    	    $.getJSON("<?php echo base_url('index.php/device_list_ctrl/get_client_list_name')?>", { client : $('#AutoClinetName').val(), dist: $('#DistributorID').val(), dealer : $('#DealerID').val() }, response);
    	 },
         select:function (event, ui) 
         {
              $('#AutoClinetID').val(ui.item.id);
              client_change();
         }
    });
    $('#DealerID').change(function(){
    	$('#AutoClinetID').val("");
    	$('#AutoClinetName').val("");
        });
    $('#DistributorID').change(function(){
    	$('#AutoClinetID').val("");
    	$('#AutoClinetName').val("");    	
        });   
 });
</script>
<script type="text/javascript">
function goCSVButton(val)
{
	document.getElementById('ClickedBtn').value=val.value;
	document.getElementById("QueryForm").submit();
	document.getElementById('ClickedBtn').value="";
}
function goPDFButton(val)
{
	document.getElementById('ClickedBtn').value=val.value;
	document.getElementById("QueryForm").submit();
	document.getElementById('ClickedBtn').value="";	
}
/*
 * This function is used to get all dealer which is belongs
 * particular distributor.
 * Return type - void
 */
function distributor_change()
{
	var baseUrl=document.getElementById('URL').value;
	var distributorID=document.getElementById('DistributorID').value;
	document.getElementById("DealerID").innerHTML=null;	
	document.getElementById("VehicleGroupID").innerHTML=null;
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Slno</th><th>IMEI No.</th><th>Client</th><th>Vehicle</th><th>Installed Date &amp; Time</th><th>Last Date &amp; Time</th></tr><tr><td colspan=\"6\">Please click the show button to get the result.</td></tr></table>";	
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_dist_dealer/"+distributorID;
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    var x=document.getElementById("DealerID");
	    	var option=document.createElement("option");
	    	option.text="";	option.value=""; x.add(option);
	        var arrs = JSON.parse(xmlhttp.responseText);
	        var id="";var dealer="";    
	        for(j=0;j<arrs.length;j++)
	        {
	          id=arrs[j].dealer_id;
	          dealer=arrs[j].dealer_name;
	          add_dealer(id,dealer);    
	        }
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send()
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - dealer id
 *  dealer - dealer name
 * Return type - void
 */
function add_dealer(id,dealer)
{
	var x=document.getElementById("DealerID");
	var option=document.createElement("option");
	option.text=dealer;
    option.value=id;
    x.add(option);
}
/*
 * This function is used to get all vehicle group which is belongs
 * particular client.
 * Return type - void
 */
function client_change()
{	
	//alert("Test"+document.getElementById('AutoClinetID').value);
	var clientID=document.getElementById('AutoClinetID').value;;
	var baseUrl=document.getElementById('URL').value;	
	document.getElementById("VehicleGroupID").innerHTML=null;
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Slno</th><th>IMEI No.</th><th>Client</th><th>Vehicle</th><th>Installed Date &amp; Time</th><th>Last Date &amp; Time</th></tr><tr><td colspan=\"6\">Please click the show button to get the result.</td></tr></table>";
	var xmlhttp = new XMLHttpRequest();
	var url = baseUrl+"/get_client_vhgp/"+clientID;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
		{
			var x=document.getElementById("VehicleGroupID");
	    	var option=document.createElement("option");
	    	option.text="";	option.value=""; x.add(option);
	        var arrs = JSON.parse(xmlhttp.responseText);
	        var id="";var vehicleGp="";    
	        for(j=0;j<arrs.length;j++)
	        {
	          id=arrs[j].vehicle_group_id;
	          vehicleGp=arrs[j].vehicle_group;
	          adds_vh_gp(id,vehicleGp);    
	        }
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - vehicle group id
 *  vehicleGp - vehicle group name
 * Return type - void
 */
function adds_vh_gp(id,vehicleGp)
{
	var x=document.getElementById("VehicleGroupID");
	var option=document.createElement("option");
	option.text=vehicleGp;
    option.value=id;
    x.add(option);
}
 /*
  * This function is used to get all vehicle which is related to
  * selected vehicle group.
  * Return type - void
  */
function vehicleGroup_change()
{	
	var baseUrl=document.getElementById('URL').value;
	var vehiclegp=document.getElementById('VehicleGroupID').value;
	var xmlhttp = new XMLHttpRequest();
	document.getElementById("VehicleID").innerHTML=null;
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Slno</th><th>IMEI No.</th><th>Client</th><th>Vehicle</th><th>Installed Date &amp; Time</th><th>Last Date &amp; Time</th></tr><tr><td colspan=\"6\">Please click the show button to get the result.</td></tr></table>";
	var url = baseUrl+"/get_vhl_grp_vehicle/"+vehiclegp;	
	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	var x=document.getElementById("VehicleID");
	    	var option=document.createElement("option"); 
	    	option.text="";	option.value=""; x.add(option);
	        var arrs = JSON.parse(xmlhttp.responseText);
	        var id="";var vehicleName="";    
	        for(j=0;j<arrs.length;j++)
	        {
	          id=arrs[j].vehicle_id;
	          vehicleName=arrs[j].vehicle_regnumber;
	          adds_vehicle(id,vehicleName);
	        }
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
/*
 * This function is use to add new <option> to <select>
 * @param
 *  id - vehicle group id
 *  vehicleName - vehicle name
 * Return type - void
 */
function adds_vehicle(id,vehicleName)
{
	var x=document.getElementById("VehicleID");
	var option=document.createElement("option");
	option.text=vehicleName;
    option.value=id;
    x.add(option);
}
function vehicle_change()
{
	document.getElementById("Grid").innerHTML="<table width=\"100%\" class=\"user-dts\"><tr><th>Slno</th><th>IMEI No.</th><th>Client</th><th>Vehicle</th><th>Installed Date &amp; Time</th><th>Last Date &amp; Time</th></tr><tr><td colspan=\"6\">Please click the show button to get the result.</td></tr></table>";	
}
</script>
<!-- Login -->
<div class="container">
  <div class="row">
    <div class="user-container stacked"><br>
      <div class="content clearfix">
        <form id="QueryForm" action="<?php echo(base_url("index.php/device_list_ctrl/fetch_device_list/"))?>" method="post" class="form-horizontal">
           <!-- style="display:none;" -->
          <input type="text" id="URL" name="URL" style="display: none" value="<?php echo(base_url("index.php/device_list_ctrl/"))?>"/>
          <input type="text" id="ClickedBtn" name="ClickedBtn" style="display: none" value=""/>
          <h1>Device List</h1>
          <div class="user-fields">
			<div class="field">
				<label>IMEI No.:</label>
				<input type="text" maxlength="15" id="ImeiNo" name="ImeiNo" value="<?php echo $imeiNo ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('ImeiNo'))echo form_error('ImeiNo',' ',' ');?>" />
			</div>
			<div class="field">
				<label>SlNo.:</label>
				<input type="text" maxlength="6" id="SlNo" name="SlNo" value="<?php echo $slNo ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('SlNo'))echo form_error('SlNo',' ',' ');?>" />
			</div>
			
		    <div class="field" <?php echo ($GLOBALS['ID']['sess_user_type'] != DEALER_USER & $GLOBALS['ID']['sess_user_type'] != OHTER_CLIENT_USER)?"":'style="display:none;"'?>>
              <label for="useremail">Distributor:</label>
              <select id="DistributorID"  name="DistributorID" onchange="distributor_change()">
              <option value=""></option>
               <?php if($distributorList!=null):  foreach ($distributorList as $row):?>
               		<option value="<?php echo $row['dist_id']?>" <?php echo(($distributorID==$row['dist_id'])?'selected':'')?>><?php echo $row['dist_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <?php if($GLOBALS['ID']['sess_user_type'] != OHTER_CLIENT_USER)?>
            <div class="field" <?php echo ($GLOBALS['ID']['sess_user_type'] != OHTER_CLIENT_USER)?"":'style="display:none;"'?>>
              <label for="useremail">Dealer:</label>
              <select id="DealerID"  name="DealerID" onchange="dealer_change()">
              <option value=""></option>
               <?php if($dealerList!=null):  foreach ($dealerList as $row):?>
               		<option value="<?php echo $row['dealer_id']?>" <?php echo(($dealerID==$row['dealer_id'])?'selected':'')?>><?php echo $row['dealer_name']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
            <div class="field" > 
				<label>Client:</label><!-- style="display:none" -->
                <input type="text" id="AutoClinetName" name="AutoClinetName" maxlength="100" value="<?php echo $clientName ?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('AutoClinetName'))echo form_error('AutoClinetName',' ',' '); ?>"/>
                <input type="text" style="display:none" id="AutoClinetID" name="ClinetID" value="<?php echo $clientID ?>"  class="form-control input-lg" />
            </div>	  
            <div class="field">
              <label for="useremail">Vehicle Group:</label>
              <select id="VehicleGroupID"  name="VehicleGroupID" onchange="vehicleGroup_change()">
              <option value=""></option>
               <?php if($vehicleGroupList!=null):  foreach ($vehicleGroupList as $row):?>
               		<option value="<?php echo $row['vehicle_group_id']?>" <?php echo(($vehicleGroupID==$row['vehicle_group_id'])?'selected':'')?>><?php echo $row['vehicle_group']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div> 
            <div class="field">
              <label for="useremail">Vehicle:</label>
              <select id="VehicleID"  name="VehicleID" onchange="vehicle_change()">
              <option value=""></option>           
               <?php if($vehicleList!=null): foreach ($vehicleList as $row):?>
               		<option value="<?php echo $row['vehicle_id']?>" <?php echo(($vehicleID==$row['vehicle_id'])?'selected':'')?>><?php echo $row['vehicle_regnumber']?> </option>
               <?php endforeach; endif;?>
              </select>
            </div>
          </div>
          <!-- /login-fields -->
		 <div class="field">
            <button class="btn btn-primary" type="submit">Show</button>
            <?php $reloadURL=base_url("index.php/device_list_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
            <button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
      <!-- /content -->
    </div> 
    <br>   
    <div class="container">
    <?php if($showGrid):?>
           <div class="table-responsive" id="Grid" style="height: 250px;">
            <table style="width:100%;" class="user-dts" border="1">
              <tr>
              	<th>Slno</th>
                <th>IMEI No.</th>
                <th style="width:17%;">Client</th>
                <th>Vehicle</th>
                <th>Installed Date &amp; Time</th>
                <th>Last Date &amp; Time</th>
              </tr>
              <?php if($deviceDetailList!=null){ foreach ($deviceDetailList as $row):?>
              <?php
              $title= ' Sl.No. : '.$row['device_slno'].', ';
              $title.= ' Distributor : '.$row['dist_name'].', ';
              $title.= ' Dealer :'.$row['dealer_name'].', ';
              $title.= ' Client : '.$row['client_name'].', ';
              $title.= ' SIM Provider : '.$row['dev_install_sim_provider'].', ';
              $title.= ' SIM Provide By : ';
              $title.=($row['dev_install_is_atc_sim']== 1)?'Autograde':''.', ';
              $title.= ' Mob. No. : '.$row['dev_install_mobile_no'].', ';
              $title.= ' IMEI No. : '.$row['device_imei'];
              ?>
               	<tr title="<?php echo $title?>">
               		<td><?php echo $row['device_slno']?></td>
               		<td><?php echo $row['device_imei']?></td>
               		<td><?php echo $row['client_name']?></td>
               		<td><?php echo $row['vehicle_name']?></td>
               		<td><?php echo $row['first_data_date']?></td>
               		<td><?php echo $row['last_data_date']?></td>
              	</tr>
               <?php endforeach;} else if($GLOBALS['ID']['sess_user_type']==OHTER_CLIENT_USER){ ?>
               <tr>
               <td colspan="6" style="color:red;padding:20px;font-size:18px;">No records found</td>
               </tr>
               <?php  } else{ ?>
               <tr>
               <td colspan="6" style="color:red;padding:20px;font-size:18px;">No records found</td>
               </tr>
               <?php } ?>
            </table>
          </div>
        <?php if($deviceDetailList!=null):?>
        <div class="field" >
				<button id="csv" name="csv" class="btn btn-primary" type="button" value="csv" onClick="goCSVButton(this);"><?php echo "Export to CSV"; ?> </button>
				<button id="pdf" name="pdf" class="btn btn-primary" type="button" value="pdf" onClick="goPDFButton(this);"><?php echo "Export to PDF"; ?> </button>
		</div>
		<?php endif;//csv and pdg btn hide condition end?>
	<?php endif;//result grid hide condition end?>
    </div>
  </div>
</div>
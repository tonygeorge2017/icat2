<script>
function clientName() //This function is to get the clientname in the dropdown
{
	document.getElementById("temp").value=document.getElementById("ClientName").value;//ClientName
	document.getElementById("myform").submit();
}
</script>

<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
#DriverGroupIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
</style>
<div class="container">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="myform" action="<?php echo(base_url("index.php/driver_group_ctrl/vts_driver_group_validation/"))?>" method="post" class="form-horizontal">
					<!-- style="display: none;" -->
					<input type="text" id="DriverGroupId" style="display: none;" name="DriverGroupId" value="<?php echo((isset($dver_group_id))? $dver_group_id:null) ?>" />
					<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
					
					<h1>Driver Group Details</h1>
					<div id="outcome1"><?php if(isset($outcome)) echo $outcome;?></div>
					<div id="outcome2"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div>
					<br/>
					<div class="user-fields">
						<?php if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER):?>
							<div class="field">
								<label>Client:<span style="color:red;"> *</span></label>
								<select name="ClientName" id="ClientName" onchange="clientName()">
								<option value=""><?php if(null!=form_error('ClientName'))echo form_error('ClientName',' ',' '); ?></option>
								<?php foreach ($clientList as $row):?>
								<option value="<?php echo $row['client_id']?>" <?php echo(($dver_group_client_id==md5($row['client_id']))?'selected':'')?>><?php echo $row['client_name']?> </option>
								<?php endforeach;?>
								</select>
							</div>
						<?php endif; ?>
						<div class="field">
							<label>Driver Group:<span style="color:red;"> *</span></label>
							<input type="text" maxlength="20" id="DriverGroup" name="DriverGroup" value="<?php if(!isset($outcome))echo $dver_group?>" class="form-control input-lg" placeholder="<?php if(null!=form_error('DriverGroup'))echo form_error('DriverGroup',' ',' ');?>" />
						</div>
						<div class="login-actions">
							<span class="login-checkbox"> <input style="" id="DriverGroupIsActive" name="DriverGroupIsActive" type="checkbox" class="field login-checkbox" value="1" tabindex="4" <?php echo(($active==1)? 'checked':'')?>> 
							<label class="choice" for="Field">Active</label>
							</span>
						</div><br />
					</div>
					<!-- /login-fields -->
					<div class="field">
						<button class="btn btn-primary" type="submit"><?php echo(($dver_group_id==null)?"Save":"Update")?> </button>
						<?php $reloadURL=base_url("index.php/driver_group_ctrl/")?>	<!-- new php variable '$reloadURL' created for onclick of cancel btn-->
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>
					</div>
				</form>
				<form method="post"
					action="<?php echo(base_url("index.php/driver_group_ctrl/editOrDelete_vts_driver_group/"))?>">
					<div class="table-responsive">
						<table width="100%" class="user-dts">
							<tr>
								<th>Driver Group</th>
								<th>Action</th>
							</tr>
							<?php if($driverGroupList!=null): foreach ($driverGroupList as $row): ?> 
							<tr>
								<td><?php echo trim($row['driver_group'])?></td>
								<td>
									<?php $md_id = md5(trim($row['driver_group_client_id']));?>
									<a href="<?php echo(base_url("index.php/driver_group_ctrl/editOrDelete_vts_driver_group/".$md_id."/".md5('edit')."/".md5(trim($row['driver_group_id']))))?>" title="Edit the Driver Group"><i class="fa fa-pencil-square-o"></i></a> &nbsp; 
									<a href="<?php echo(base_url("index.php/driver_group_ctrl/editOrDelete_vts_driver_group/".$md_id."/".md5('delete')."/".md5(trim($row['driver_group_id']))))?>" title="Delete the Driver Group" onclick="return confirm('Do you want to delete the selected Driver Group?')"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<?php endforeach; endif;?>
						</table>
					</div>
				</form>
				<nav class="pull-right">
					<ul class="pagination pagination-sm"><?php echo $pageLink;?></ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- /content -->
		</div>
	</div>
</div>
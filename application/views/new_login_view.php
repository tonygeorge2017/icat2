<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Autograde</title>
<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico')?>" type="image/gif">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/css/new_bootstrap.css')?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/css/new_style.css')?>" rel="stylesheet">
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
</head>
<body>
<div class="topbar">
  <div class="container">
    <div class="col-lg-7"></div>
    <div class="col-lg-5">
      <div class="input-group">
      	<form action="<?php echo(base_url("index.php/login_ctrl/validate_user/"))?>" method="post" class="form-horizontal">
        		<input type="text" id="UserName" name="UserName" value="<?php echo $user_name?>" placeholder="<?php echo((null!=form_error('UserName'))?form_error('UserName',' ',' '):'Username'); ?>" class="form-control" aria-describedby="basic-addon1"/>
        		<input type="password" id="Password" name="Password" value="" placeholder="<?php echo((null!=form_error('Password'))?form_error('Password',' ',' '):'Password'); ?>" class="form-control" aria-describedby="basic-addon1"/>
        	<button class="btn btn-danger" > Login </button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Navigation -->
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a href=""><img alt="" src="<?php echo base_url('assets/images/new_logo.jpg')?>" /></a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">	
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#"><i class="fa fa-home"></i><br>
          <span>Home</span></a></li>
        <li><a href="http://www.autograde.in/" target="_blank"> <i class="fa fa-pencil"></i><br>
          <span>About us </span></a> </li>
        <!--  <li><a href="#"><i class="fa fa-car"></i><br>
          <span>Automotive</span></a> </li>
        <li><a href="#" ><i class="fa fa-tablet"></i><br>
          <span>Consumer </span></a> </li>-->
        <li><a href="<?php echo base_url('index.php/solutions_ctrl'); ?>" target="_blank"><i class="fa fa-map-marker"></i> <br>
          <span>Solutions</span> </a> </li>
        <!-- <li><a href="#"><i class="fa fa-shopping-cart"></i><br>
          <span>Shop</span> </a> </li>-->
        <!--<li><a href="#"><i class="fa fa-file-text"></i><br>
          <span>Blog </span></a> </li>-->
        <li><a href="http://support.autograde.in" target="_blank"><i class="fa fa-user"></i><br>
          <span>Support</span> </a> </li>
        <li><a href="<?php echo base_url('index.php/contact_us');?>"><i class="fa fa-envelope-o"></i><br>
          <span>Contact </span></a> </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container --> 
</nav>
<!-- Full Page Image Background Carousel Header -->
<header id="myCarousel" class="carousel slide"> 
  <!-- Indicators --> 
  <!-- <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>--> 
  <!-- Wrapper for Slides -->
  <div class="carousel-inner">
    <div class="item active"> 
      <!-- Set the first background image using inline CSS below. -->
      <div class="fill" style="background-image:url(<?php echo base_url();?>assets/images/banner-img1.png);"></div>
     <!-- <div class="carousel-caption">
          <h1>Taking a break from your car?</h1>
        <h1>Don't let them break your car!</h1>
        <p>(Enjoy the priceless music from your sound system in your car<br>
          staying safe with our anti-theft automobile solutions.) </p>
        <button class="btn btn-danger" type="button" > Read More </button>
      </div>-->
    </div>
    <div class="item"> 
      <!-- Set the first background image using inline CSS below. -->
      <!-- style="background-image:url('assets/images/banner-img1.png');" -->
      <div class="fill" style="background-image:url(<?php echo base_url();?>assets/images/banner-img2.png);"></div>
     <!-- <div class="carousel-caption">
        <h1>Taking a break from your car?</h1>
        <h1>Don't let them break your car!</h1>
        <p>(Enjoy the priceless music from your sound system in your car<br>
          staying safe with our anti-theft automobile solutions.) </p>
        <button class="btn btn-danger" type="button" > Read More </button>
      </div>-->
    </div>
    <div class="item"> 
      <!-- Set the first background image using inline CSS below. -->
      <!-- style="background-image:url('assets/images/banner-img1.png');" -->
     <div class="fill" style="background-image:url(<?php echo base_url();?>assets/images/banner-img3.png);"></div>
     <!-- <div class="carousel-caption">
        <h1>Taking a break from your car?</h1>
        <h1>Don't let them break your car!</h1>
        <p>(Enjoy the priceless music from your sound system in your car<br>
          staying safe with our anti-theft automobile solutions.) </p>
        <button class="btn btn-danger" type="button" > Read More </button>
      </div>-->
    </div>
  </div>
  <!-- Controls --> 
  <!--<a class="left carousel-control" href="#myCarousel" data-slide="prev"> <img src="images/btn-prev.png"></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <img src="images/btn-next.png"> </a>--> </header>
<!-- Page Content -->
<div class="footer-inner">
  <div class="container"> 
    <!-- Footer -->    
    <div class="row">
      <div class="col-lg-12">
        <ul>
          <li><a href="#">Home</a> </li>
          <li><a href="http://www.autograde.in/" target="_blank">About Us</a></li>
          <!-- <li><a href="#">Automotive</a> </li> -->
         <!--  <li><a href="#">Consumer</a> </li> -->
          <li><a href="<?php echo base_url('index.php/solutions_ctrl'); ?>">Solutions</a> </li>
          <!-- <li><a href="#">Shop</a></li>
          <li><a href="#">Blog</a></li> -->
          <li><a href="http://support.autograde.in" target="_blank">Support</a></li>
          <li><a href="<?php echo base_url('index.php/contact_us');?>">Contact</a></li>
        </ul>
        <p> <a href="https://www.facebook.com/AutogradeInternationalllc/?fref=ts" target="_blank" ><i class="fa fa-facebook"> </i></a><!--<a href="#"><i class="fa fa-twitter"></i></a>-->  Powered By Autograde &copy; <?php echo date("Y")?></p>
      </div>
    </div>
    <!-- /.row -->     
  </div>
</div>
<!-- Script to Activate the Carousel --> 
<script src="<?php echo base_url('assets/js/jquery.js');?>"></script> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script> 
<script>
  $('.carousel').carousel({
	  interval: 5000 //changes the speed
  })
</script>
</body>
</html>

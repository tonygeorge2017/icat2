<style>
#outcome1 {
	color: green;
}
#outcome2 {
	color: red;
}
#VehicleGroupIsActive {
	width: 15px;
	height: 15px;
	margin-right: -15px;
}
.modal_progress_bar
{
position: fixed;
	  top: 10%;
	  margin-left: 393px;
	  margin-top: 105px;
	  z-index: 1050;
	  width: 550px;
	  height:170px;
	  background-color: #fff;
	  border: 1px solid #999;
	  border: 1px solid rgba(0,0,0,0.3);
	  border-color: white;
	  -webkit-border-radius: 6px;
	  -moz-border-radius: 6px;
	  border-radius: 6px;
	  outline: 0;
	  -webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	  -moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	  box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	  -webkit-background-clip: padding-box;
	  -moz-background-clip: padding-box;
	  background-clip: padding-box; 
}
</style>
<script type="text/javascript">
$(function() {

	var controller = '<?=base_url("index.php/fuel_report_ctrl/")?>';
	
	$("#fuel_sense_StartDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#fuel_sense_EndDate").datepicker({ maxDate: new Date(), dateFormat: 'yy-mm-dd' });
	
	$("#VehicleGroup").on("change", function() {

		var group_id = $(this).val();
		$("#hiddenVehicleGroup").val( $("#VehicleGroup option:selected").text() );
		var htm = '<option value="">-- Select vehicle name --</option>';
		 $.get( controller+"/get_grp_vehicles", { requestData: group_id }, function(data) {

			var json = JSON.parse( data );

			$.each(json, function( index, vehicle ) {
				htm+= '<option value="'+vehicle.vcle_id+'">'+vehicle.vcle_regnumber+'</option>';
			});

			$("#vehicle_name").html(htm);
			
		});
		
	});	


	$("#vehicle_name").on("change", function() {

		var vehicle_name = $(this).val();
		$("#hiddenVehicle").val( $("#vehicle_name option:selected").text() );		
	});
			
});
	
function validate_client() {

	document.getElementById("is_client").value=document.getElementById("ClientName").value;//ClientName
	document.getElementById("fuel_report_voilation_form").submit();
	
}

		
</script>

<div class="">
	<div class="row">
		<div class="user-container stacked">
			<br>
			<div class="content clearfix">
				<form id="fuel_report_voilation_form" action="<?php echo(base_url("index.php/fuel_report_ctrl/get_fuel_report_table/"))?>" method="post" class="form-horizontal">
					<h1>Fuel Sensor</h1>
					<?php $vehicle = isset($selected_vehicle_name)?$selected_vehicle_name:"";?>
					<input type="hidden" id="is_client" name="is_client" value="-1" />
					<div class="user-fields">
						<?php if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER):?>
						<div id="outcome2" style="text-align: center"><?php if( isset( $error_client_name ) ) echo $error_client_name; ?></div></br>
						<div class="field">
							<label>Client:<span style="color:red;"> *</span></label>
							<select name="ClientName" id="ClientName" onchange="validate_client();">
							<option value="">-- Select Client --</option>
							<?php foreach ($clientList as $row):?>
							<option value="<?php echo $row['client_id']?>" <?php echo(($vcle_rept_client_id==$row['client_id'])?'selected':'')?>><?php echo $row['client_name']?> </option>
							<?php endforeach;?>
							</select>
						</div>
						<?php endif; ?>
						<div id="outcome2" style="text-align: center"><?php if( isset( $error_vehicle_grp ) ) echo $error_vehicle_grp; ?></div></br>
						<input type="hidden" id="hiddenVehicleGroup" name="hiddenVehicleGroup" value="<?php if( isset($vh_gp_name) ) echo $vh_gp_name;?>"/>
						<div class="field">
							<label>Vehicle Group:<span style="color:red;"> *</span></label>
							<select name="VehicleGroup" id="VehicleGroup">
								<option value="">-- Select vehicle group --</option>
								<?php if( isset( $vehicleGroupList ) ) : $vehicle_grp = json_decode( $vehicleGroupList ); foreach( $vehicle_grp AS $group ) : ?>
									<option value="<?=$group->vehiclegroupid;?>" <?=($vcle_rept_vclegroup_id==$group->vehiclegroupid)?'selected':'';?>><?=$group->vehiclegroupname;?></option>
								<?php endforeach; endif;?>
							</select>
						</div>
						<div id="outcome2" style="text-align: center"><?php if(isset($outcome_with_db_check)) echo $outcome_with_db_check;?></div></br>
						<input type="hidden" id="hiddenVehicle" name="hiddenVehicle" value="<?php if( isset($vh_name) ) echo $vh_name;?>"/>
						<div class="field">
							<label>Vehicle:<span style="color:red;"> *</span></label>
							<select name="vehicle_name" id="vehicle_name">
								<option value="">-- Select Vehicle Name --</option>
								<?php $vehicles = json_decode( $vehicle_list ); if( sizeof( $vehicles >= 1 ) ) { foreach ( $vehicles AS $vehicle ): ?>
								<option value="<?=$vehicle->vcle_id;?>" <?=isset( $selected_vehicle_name ) ? ( $selected_vehicle_name == $vehicle->vcle_id ? "selected" : "" ) : "";?>><?=$vehicle->vcle_regnumber;?></option>
								<?php endforeach; } ?>
							</select>
						</div>
						<div id="outcome2" style="text-align: center"><?php if(isset($error_start_date)) echo $error_start_date;?></div></br>
						<div class="field">
							<label>From Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="fuel_sense_StartDate" name="fuel_sense_StartDate" class="form-control" value="<?php if(isset($start_date)) echo $start_date;?>" placeholder="">
						</div>
						<div id="outcome2" style="text-align: center"><?php if(isset($error_end_date)) echo $error_end_date;?></div></br>
						<div class="field">
							<label>To Date:<span style="color:red;"> *</span></label>
							<input style="font-size:13px;" maxlength="10" id="fuel_sense_EndDate" name="fuel_sense_EndDate" class="form-control input-lg" value="<?php if(isset($end_date)) echo $end_date;?>" placeholder="" />
						</div>
					</div>
					
					<!-- /login-fields-->
					<div class="field">
						<button class="btn btn-primary" type="submit" name="btn_go" value="go"><?php  echo "GO"; ?> </button>
						<?php $reloadURL=base_url("index.php/fuel_report_ctrl/")?>	
						<button class="btn btn-primary" type="reset" onclick="self.location='<?php echo $reloadURL?>'">Cancel</button>	
					</div>
			</form>
		</div>
		<!-- /content -->
	</div>
		
<!-- 		to get the details in table -->
<?php

	if( isset( $fuel_sensor_data ) ) { 

		if( count( json_decode( $fuel_sensor_data ,1 ) ) > 1 ) { 
	
	?>
	<form id="myform1" action="<?php echo(base_url("index.php/fuel_report_ctrl/fuel_report_export/"))?>" method="post" class="form-horizontal">
			</br>
			<input type="text" id="temp" style="display:none;"  name="temp" value="-1" />
		<div class="container">
		</br>
		<div class="table-responsive" style="height: 250px;">  
			<table width="100%" class="user-dts" border=1>
				<tr>
					<th style="text-align: center;">Sl. No </th>
					<th style="text-align: center;">Date</th>
					<th style="text-align: center;">Time</th>
					<th style="text-align: center;">Distance (Kms)</th>
					<th style="text-align: center;">Fuel ltr </th>
					<th style="text-align: center;">Fuel Percentage</th>
					<!-- <th style="text-align: center;">Mileage (Kms/Ltr)</th> -->
				</tr>
				<?php $i=1; foreach( json_decode( $fuel_sensor_data ) AS $rec ): ?>
				
				<tr>
					<td style="text-align: center;"><?=$i;?></td>
					<td style="text-align: center;"><?=date("d/m/y", strtotime( $rec->time_value ) );?></td>
					<td style="text-align: center;"><?=date("H:i:s", strtotime( $rec->time_value ) );?></td>
					<td style="text-align: center;"><?=$rec->vehicle_run_data;?></td>
					<td style="text-align: center;"><?=$rec->liters;?></td>
					<td style="text-align: center;"><?=$rec->fuel_percentage .'%';?></td>
					<!-- <td style="text-align: center;"><?php //echo $rec->mileage; ?></td> -->
					
					
				</tr>
				
				<?php $i++; endforeach; ?>
			</table>
		</div>
		<div class="field" >
				<button id="btn_csv" name="btn_csv" class="btn btn-primary" type="submit" value="csv"><?php echo "Export to CSV"; ?> </button>
				<button id="btn_pdf" name="btn_pdf" class="btn btn-primary" type="submit" value="pdf"><?php echo "Export to PDF"; ?> </button>
				<?php $reloadURL=base_url("index.php/fuel_report_ctrl/fuel_report_graph/");?>
				<button class="btn btn-primary" type="button" onclick="window.open('<?=$reloadURL;?>')" >View Graph</button>
		</div>
	</div>

	</form>	
	<?php 
			} else if($fuel_sensor_data == "No_Data" ) {
				
	?>
		<div class="container">
		</br>
			<div class="table-responsive" style="height: 250px;">  
				<table width="100%" class="user-dts" border=1>
					<tr>
						<th style="text-align: center;">Sl. No </th>
						<th style="text-align: center;">Date</th>
						<th style="text-align: center;">Time</th>
						<th style="text-align: center;">Distance (Kms)</th>
						<th style="text-align: center;">Fuel ltr </th>
						<th style="text-align: center;">Fuel Percentage</th>
						<!-- <th style="text-align: center;">Mileage (Kms/Ltr)</th> -->
					</tr>
					<tr>
						<td style="color:red;padding:20px;font-size:18px;" colspan=6>No Records found</td>
					</tr>
				</table>
			</div>
		</div>
	<?php
				
			} else {
	?>
		<div class="container">
		<br>
		<div class="table-responsive" style="height: 250px;">  
				<table width="100%" class="user-dts" border=1>
					<tr>
						<th style="text-align: center;">Sl. No </th>
						<th style="text-align: center;">Date</th>
						<th style="text-align: center;">Time</th>
						<th style="text-align: center;">Distance (Kms)</th>
						<th style="text-align: center;">Fuel ltr </th>
						<th style="text-align: center;">Fuel Percentage</th>
						<!-- <th style="text-align: center;">Mileage (Kms/Ltr)</th> -->
					</tr>
					<tr>
						<td style="color:red;padding:20px;font-size:18px;" colspan=6><?=$fuel_sensor_data;?></td>
					</tr>
				</table>
			</div>
		</div>
			
	<?php
			}
	} 
	?>
		<div class="clearfix"></div>
<!-- table ends here....-->
	</div>
</div>

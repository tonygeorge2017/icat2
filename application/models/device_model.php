<?php

class Device_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	
	
	/*
	 * This function used to return the dealer details relate the give parameter(i.e. dealer_id)
	 * if any error, then return null.
	 */
	public function get_vts_device($rowPerPage=null,$startNo=null,$device_id=null)
	{
		try
		{
			if($rowPerPage==null & $device_id==null)
			{
				$command="select device_id, device_imei, device_distributor_id, device_dealer_id, get_distributor_name(device_distributor_id) distributor_name, device_release_distributor_date, device_slno from vts_device;";// cast(device_release_distributor_date as date) as 
				$query = $this->db->query($command);
// 				log_message('error','****2nd'.$command);
				return $query->result_array();
			}
			else if($device_id==null)
			{
				$command="select device_id, device_imei, device_distributor_id, device_dealer_id, get_distributor_name(device_distributor_id) distributor_name, device_release_distributor_date, device_slno from vts_device order by device_imei limit ".$rowPerPage." offset ".$startNo.";";
				$query = $this->db->query($command);
// 				log_message('error','****33nd'.$command);
				return $query->result_array();
			}
			else
			{
				$query=$this->db->get_where('vts_device',array('device_id'=>$device_id));
				return $query->row_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_device',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the device details from the vts_device table
	 */
	
	public function InsertOrUpdateOrdelete_vts_device($device_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_device', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('device_id', $device_id);
				$this->db->update('vts_device', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('device_id', $device_id);
				if($this->db->delete('vts_device'))
					return true;
				else
				{
					return FALSE;
				}
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_device',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is used to all the distributors from the
	 * distrinutor table to fill the drop down in Device view.
	 */
	public function get_allDistributor()
	{
		try{
			$this->db->order_by('dist_name','ASC');
			$this->db->select('dist_name, dist_id');
			$this->db->where('dist_isactive', ACTIVE);
			$query = $this->db->get('vts_distributor');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all dealer names - vts_dealer');
			return null;
		}
	}
	
	/*
	 * This function is used to get all the devices from the
	 * device table to fill the drop down in Device view.
	 */
	public function get_allDeviceTypes()
	{
		try{
			$this->db->order_by('device_type_name','ASC');
			$this->db->select('device_type_name, device_type_id');
			$query = $this->db->get('vts_device_type');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all device type names - vts_device_type');
			return null;
		}
	}
}
?>
<?php

class Driver_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
		$this->load->model('common_model');
		$GLOBALS['todayDateTime']=$this->common_model->get_usertime();
	}	
	
	
	/*
	 * This function used to return the driver details relate the give parameter(i.e. driver_id)
	 * if any error, then return null.
	 */
	public function get_vts_driver($rowPerPage=null,$startNo=null,$driver_id=null,$session_login_client_id=null)
	{
		try
		{
// 			$command="select d.driver_id, d.driver_name, d.driver_drivergroup_id, d.driver_mobile, dg.driver_group from vts_driver d, vts_driver_group dg where d.driver_drivergroup_id = dg.driver_group_id order by d.driver_name";
				
			if($rowPerPage==null & ($driver_id==null & $session_login_client_id!=md5(AUTOGRADE_USER)))
			{			
				$this->db->select('driver_name, driver_rf_id, get_driver_group_name(driver_drivergroup_id) driver_group, driver_mobile, driver_client_id, driver_id');
				
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(driver_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_driver');
// 				$query = $this->db->query($command);
				$query = $this->db->get();
				return $query->result_array();
			}
			else if($driver_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$this->db->limit($rowPerPage, $startNo);			
// 				$command="select d.driver_id, d.driver_name, d.driver_drivergroup_id, d.driver_mobile, dg.driver_group from vts_driver d, vts_driver_group dg where d.driver_drivergroup_id = dg.driver_group_id order by d.driver_name limit ".$rowPerPage." offset ".$startNo.";";
// 				$query = $this->db->query($command);
// 				return $query->result_array();

				$this->db->order_by('driver_name','ASC');
				$this->db->select('driver_name, driver_rf_id, get_driver_group_name(driver_drivergroup_id) driver_group, driver_mobile, driver_client_id, driver_id');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(driver_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_driver');
				$query = $this->db->get();
				return $query->result_array();
			}
			else
			{
				$command="SELECT tr.trip_id, d.driver_id, d.driver_client_id, d.driver_drivergroup_id, d.driver_name, d.driver_rf_id, d.driver_dob, d.driver_address, d.driver_postcode, d.driver_phone, d.driver_mobile, 
d.driver_dl_number, d.driver_dl_details, d.driver_id_type, d.driver_id_number, d.driver_isactive, d.driver_remarks, v.vehicle_group_id, v.vehicle_id
FROM vts_driver d left outer join vts_trip_register tr on d.driver_id=tr.trip_register_driver_id and (tr.trip_register_time_start=(select max(trip_register_time_start) 
from vts_trip_register ltr where ltr.trip_register_driver_id=tr.trip_register_driver_id and (trip_register_time_end > '".$GLOBALS['todayDateTime']."' or trip_register_time_end is null))) left outer join vts_vehicle v on v.vehicle_id=tr.trip_register_vehicle_id
where md5(d.driver_id::varchar)='".$driver_id."';";
				//log_message('debug','**get_vts_driver**'.$command);
				$query=$this->db->query($command);
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error', 'get all detail - vts_driver',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle group for the given client ID
	 */
	public function get_all_vhGp($clientID=null, $vhgp_id=null)
	{
		try
		{
			if($GLOBALS['sessClientID']!=null || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and md5(vehicle_group_client_id::varchar)='".$clientID."' order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and md5(vehicle_group_client_id::varchar)='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
				log_message('debug','**get_all_vhGp**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	/*
	 * This function is used to get all the vehicle select vehicleGpId from the
	 * vts_client table to fill the drop down.
	 */
	public function get_all_vhGp_vehicle( $vehicleGpID=null, $todayDateTime=null, $vhID=-1, $isEdit='0')
	{
		try
		{
			$command="";
			if($vehicleGpID!=null & $todayDateTime!=null)
			{
				$command="Select vehicle_id as vh_id, vehicle_regnumber as vh_name from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and vehicle_isactive='".ACTIVE."'
and vehicle_id !=COALESCE((select trip_register_vehicle_id  from vts_trip_register where (('".$todayDateTime."' < trip_register_time_start) or ('".$todayDateTime."' < trip_register_time_end) or trip_register_time_end is null) and
trip_register_vehicle_id=vehicle_id limit 1),'-1') ";
				if($isEdit == '1')
					$command.=" or vehicle_id = ".$vhID;
				$command.=" order by vehicle_regnumber;";						
				//log_message('debug','**Driver Screen[get_VehicleList]**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
				
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle related to vehicle group');
			return null;
		}
	}
	/*
	 * To insert, delete or edit the driver details from the vts_driver table
	 */
	
	public function InsertOrUpdateOrdelete_vts_driver($driver_id,$data,$operation)
	{
		try
		{			
			if($operation=="insert")
			{
				$check=$this->db->insert('vts_driver', $data);				
				if($check==1)
				{
					//to get the current primary key
					$query=$this->db->query("SELECT currval(pg_get_serial_sequence('vts_driver', 'driver_id')) as id;");
					$result=$query->row_array();
					if($data['driver_rf_id'])
					{						
						$this->db->where('rf_id',$data['driver_rf_id']);
						$this->db->update('vts_rf_card', array('is_assigned' => '1' ));
					}
					return $result['id'];
				}else 
					return -1;
			}
			else if($operation=="edit")
			{
				$this->db->query('update vts_driver set driver_dob=null where driver_id ='. $driver_id);				
				$this->db->where('driver_id', $driver_id);
				$this->db->update('vts_driver', $data);				
				return true;
			}
			else if($operation=="delete")
			{				
				$this->db->where('md5(driver_id::varchar)', $driver_id);
				if($this->db->delete('vts_driver'))
				{
					return true;
				}else
				{
					return FALSE;
				}
			}
		}catch (Exception $ex)
		{
			log_message('error', 'delete or update  - vts_driver',$ex->getMessage());
			return false;
		}
	}
	public function InsertOrUpdateOrdelete_vts_driver_Tripreg($trip_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{				
				$check=$this->db->insert('vts_trip_register', $data);
				if($check==1)					
					return true;
				else
					return false;
			}
			else if($operation=="edit")
			{
				$this->db->where('trip_id', $trip_id);
				$this->db->update('vts_trip_register', $data);
				return true;
			}
		}catch (Exception $ex)
		{
			log_message('error', 'delete or update  - vts_driver_tripReg',$ex->getMessage());
			return false;
		}
	}
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in driver screen
	 */
	public function get_allClients()
	{
		try{
			
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}
	
	/*
	 * This function is used to all the drivers from the
	 * vts_driver_group table to fill the drop down in Driver view.
	 */
	public function get_driverGroup($session_login_client_id)
	{
		try{
			if($session_login_client_id!=AUTOGRADE_USER)
			{
				$this->db->order_by('driver_group','ASC');
				$this->db->select('driver_group, driver_group_id');
				$this->db->where('driver_group_isactive', ACTIVE);
				if($session_login_client_id!=AUTOGRADE_USER)
				{
					$this->db->where('md5(driver_group_client_id::varchar)', $session_login_client_id);
				}
				$query = $this->db->get('vts_driver_group');
				return $query->result_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all driver group names - vts_driver_group');
			return null;
		}
	}
	

	/*
	 * This function is used to get all the ID types of IDCardType from the
	 * vts_config and vts_config_type table to fill the drop down in Driver view.
	 */
	public function get_driverIdType()
	{
		try{
			
			$command="select ct.config_type_id, ct.config_type, c.config_id, c.config_item from vts_config_type ct, vts_config c where ct.config_type_id = c.config_type_id and ct.config_type = 'IDCardType' order by c.config_item";
			$query = $this->db->query($command);
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all driver names - vts_config');
			return null;
		}
	}

	/**
	 * This function get the 
	 * 
	 */
	public function get_vts_rf_card($c_id, $key='auto', $key_val=null, $old_id=null){
		$result=array();

		$sql="SELECT rf_id, rf_card_no FROM vts_rf_card where";
		$sql.=($key=='auto')?" rf_card_no LIKE '$key_val%'" : " rf_id=$key_val";
		$sql.=($c_id!='1')?" and rf_client=$c_id":"";
		$sql.=($old_id)?" and (is_assigned='0' OR rf_id = $old_id)":" and is_assigned='0'";
		$sql.=" order by rf_card_no";

		$query=$this->db->query($sql);

		if($query && $key=='auto')
		{
			$result=$query->result_array();
		}
		else if($query && $key=='id')
		{
			$result=$query->row_array();
			$result=$result['rf_card_no'];
		}

		return $result;
	}	
	
}
?>
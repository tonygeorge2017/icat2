<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_allocation_dealer_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
	}
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 * @param
	 *  $clientID - md5 of client id
	 *  $rowPerPage - no. of row per page in table for pagination
	 *  $startNo - starting row of table
	 *  $date - md5 of device release dealer date
	 *  $distributorID - md distributor id
	 * Return type - result array
	 */
	public function get_deviceDetailList($dealerID=null,$rowPerPage=null,$startNo=null,$date=null,$distributorID=null)
	{
		try
		{
			if($distributorID!=null && $rowPerPage == null && $date == null){
				$command="select device_distributor_id,count(device_imei)as no_of_device,device_imei,device_dealer_id,get_dealer_name(device_dealer_id)as dealer_name, device_release_dealer_date from vts_device where";

			if($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER)
			{
				$command.=" device_distributor_id='".$GLOBALS['ID']['sess_distributorid']."'";
			}
			else
			{
				$command.=" device_distributor_id='".$distributorID."'";
			}
				$command.=" and device_release_distributor_date is not null and device_dealer_id is not null
				group by device_distributor_id,device_release_dealer_date,device_dealer_id,device_imei;";
				
				$query = $this->db->query($command);
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		elseif($distributorID!=null && $rowPerPage!=null){
			$command="select device_distributor_id,count(device_imei)as no_of_device,device_imei,device_dealer_id,get_dealer_name(device_dealer_id)as dealer_name, device_release_dealer_date from vts_device where";
			

			if($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER)
			{
				$command.=" device_distributor_id='".$GLOBALS['ID']['sess_distributorid']."'";
			}
			else 
			{
				$command.=" device_distributor_id='".$distributorID."'";
			}
	
			$command.=" and device_release_distributor_date is not null and device_dealer_id is not null 
						group by device_distributor_id,device_release_dealer_date,device_dealer_id,device_imei order by device_dealer_id limit ".$rowPerPage." offset ".$startNo.";";
			
			$query = $this->db->query($command);
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		else if($distributorID!=null && $date != null && $dealerID!=null)
		{
			$command="select device_id, device_distributor_id, device_dealer_id,device_imei, device_release_dealer_date, device_client_id from vts_device where
					md5(device_release_dealer_date::varchar)='".$date."' and md5(device_distributor_id::varchar)='".$distributorID."' and md5(device_dealer_id::varchar) ='".$dealerID."' and device_is_active='".ACTIVE."';";
			
			$query=$this->db->query($command);
			$result =  $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
			else 
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - user',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get all the dealer from the
	 * vts_dealer table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allDealers($distributor){
		try
		{
			$this->db->order_by('dealer_name','ASC');
			$this->db->select('dealer_id, dealer_name');
			$this->db->where('dealer_isactive', ACTIVE);
			if($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER)
				$this->db->where('dealer_distributor_id', $GLOBALS['ID']['sess_distributorid']);
			else 
				$this->db->where('dealer_distributor_id', $distributor);
			$query = $this->db->get('vts_dealer');
			return $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all dealer name - vts_dealer');
			return null;
		}
	}
	
	/*
	 * This function is used to get all available device for given dealer id
	 * @param
	 *  $dealerID - dealer id
	 *  $editing - 1 or 0
	 *  $dateTime - md5 of device release client date
	 * Return type - result array
	 */
	public function get_availableDevice($distributorID,$editing,$dateTime=null,$dealer_id=null)
	{
		try{
			if(!empty($distributorID))
			{
				//$command="select device_id, device_imei, device_slno from vts_device where device_is_active='".ACTIVE."' and device_distributor_id='".$distributorID."'";

				$command="select device_id, device_imei, device_slno from vts_device where device_is_active='".ACTIVE."'";
				if($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER)
					$command.=" and device_distributor_id='".$GLOBALS['ID']['sess_distributorid']."'";
				else 
					$command.=" and device_distributor_id='".$distributorID."'";
				if($editing==NOT_ACTIVE)
					$command.=" and device_dealer_id is null;";//and device_client_id is null
				else if($dateTime!=null)
					$command.=" and( (md5(device_release_dealer_date::varchar)='".$dateTime."' and device_dealer_id='".$dealer_id."') or device_dealer_id is null);";//and device_client_id is null
				
				log_message('debug','This is the query for devices to display'.$command);
				
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}else 
				return  null;
		}catch (Exception $ex)
		{
			log_message('error','get all available device name - vts_device');
			return null;
		}
	}
	/*
	 * To edit the device allocation to dealer from the device table
	 * @param
	 *  $data - associative array of details like dealer id ,release date, etc..
	 *  $listOfDevice - array of devices to update
	 * Return type - bool
	 */	
	public function Update_deviceToDealer($data,$listOfDevice)
	{
		try
		{
			$command="UPDATE vts_device SET device_dealer_id=null, device_release_dealer_date=null where device_distributor_id='".$data['device_distributor_id']."' and device_dealer_id='".$data['device_dealer_id']."' and device_release_dealer_date='".$data['device_release_dealer_date']."';";// and device_client_id!='".null."'
			$this->db->query($command);
			if($listOfDevice!=null)
			{
				foreach ($listOfDevice as $device)
				{
// 					$command="UPDATE vts_device SET device_dealer_id='".$data['device_dealer_id']."' where device_distributor_id='".$data['device_distributor_id']."' and device_id='".$device."';";
					$command="UPDATE vts_device SET device_dealer_id='".$data['device_dealer_id']."',device_release_dealer_date='".$data['device_release_dealer_date']."' where device_distributor_id='".$data['device_distributor_id']."' and device_id='".$device."';";
					$query = $this->db->query($command);
				}
			}
			return true;
		}catch (Exception $ex)
		{
			log_message('error insert or update  - vts_dealer',$ex->getMessage());
			return false;
		}
	}
	/*
	 * To delete the device allocation to client from the device table
	 * @param
	 *  $URLdealerID - md5 of dealer id
	 *  $URLclientID - md5 client id
	 *  $URLdate - md5 of device release client date
	 */
	public function delete_deviceToDealer($URLdistributorID, $URLdealerID, $URLimei, $URLdate)
	{
			try
			{
// 				$command="UPDATE vts_device SET device_dealer_id=null, device_release_dealer_date=null where md5(device_distributor_id::varchar)='".$URLdistributorID."' and md5(device_dealer_id::varchar)='".$URLdealerID."' and md5(device_release_dealer_date::varchar)='".$URLdate."' and device_client_id is null;";
				$command="UPDATE vts_device SET device_dealer_id=null, device_release_dealer_date=null where md5(device_distributor_id::varchar)='".$URLdistributorID."' and md5(device_dealer_id::varchar)='".$URLdealerID."' and md5(device_imei::varchar)='".$URLimei."' and md5(device_release_dealer_date::varchar)='".$URLdate."' and device_client_id is null;";
				$this->db->query($command);
				
				if($this->db->affected_rows() > 0)
					return true;
				else
					return FALSE;
			}
			catch (Exception $ex)
			{
				log_message('error insert or update  - vts_device',$ex->getMessage());
				return false;
			}
	}
	/*
	 * To get device which is related to the given dealer, distributor id and datetime
	 * @param
	 *  $distributorID - distributor id
	 *  $dealerID - dealer id
	 *  $dateTime - device release distributor date
	 * Return type - result array
	 */
	public function get_dealerLinkedDevice($distributorID,$dealerID,$dateTime)
	{
		try{
			if($distributorID!=null && $dealerID!=null && $dateTime!=null)
			{
				$command="select device_distributor_id, device_dealer_id, device_release_dealer_date from vts_device where
device_release_dealer_date='".$dateTime."' and device_distributor_id='".$distributorID."' and device_dealer_id ='".$dealerID."';";
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	/*
	 * This function is used to all the distributors from the
	 * distrinutor table to fill the drop down in Device view.
	 */
	public function get_allDistributor()
	{
		try{
			$this->db->order_by('dist_id','ASC');
			$this->db->select('dist_name, dist_id');
			$this->db->where('dist_isactive', ACTIVE);
			if($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER)
				$this->db->where('dist_id', $GLOBALS['ID']['sess_distributorid']);
			$query = $this->db->get('vts_distributor');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all dealer names - vts_dealer');
			return null;
		}
	}
	
}
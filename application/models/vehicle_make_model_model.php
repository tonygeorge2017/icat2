<?php

class Vehicle_make_model_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.		
	}
	//This fucntion used to fill the table in the vehicle make & madel screen And also used to get the total count
	public function get_makeModelList($getTotalCount=false, $startNo='0'){
		try {
			$query="select * from ((select fsv_id parent_id, fsv_model parent_name from vts_fuel_sensor_vehicle  where fsv_parent_id is null order by fsv_id) x 
left outer join (select fsv_id, fsv_model, fsv_parent_id from vts_fuel_sensor_vehicle)y on x.parent_id=y.fsv_parent_id left outer join(select fs_value_fsv_id, count(fs_value_fsv_id) sensor_value_count from 
vts_fuel_sensor_value group by fs_value_fsv_id order by fs_value_fsv_id) z on y.fsv_id=z.fs_value_fsv_id )as xx 
order by parent_name limit ".ROW_PER_PAGE." offset $startNo;";
			$query=($getTotalCount)?"select count(fsv_id) total_count from vts_fuel_sensor_vehicle where fsv_parent_id is not null":$query;
			//log_message('debug','--get_makeModelList-'.$query);
			$result=$this->db->query($query);
			return $result->result_array();
		} catch (Exception $e) {
			log_message('error',$e->getMessage());
		}		
	}
	
	public function get_currentFuelSensorValues($id='0')
	{
		try{
			$query="SELECT fsv_id, fsv_model, fs_value_fsv_id, fs_value_liters, fs_value_volts 
FROM vts_fuel_sensor_vehicle left outer join vts_fuel_sensor_value on fs_value_fsv_id=fsv_id where
md5(fsv_id::varchar)='$id' order by fs_value_liters;";
			//log_message('debug','--get_currentFuelSensorValues-'.$query);
			$result=$this->db->query($query);
			return $result->result_array();
		}catch (Exception $e) {
			log_message('error',$e->getMessage());
		}		
	}
	
	public function get_currentParentDetailsToEdit($id='0')
	{
		try {
			$query="select fsv_id parent_id, fsv_model parent_name from vts_fuel_sensor_vehicle where fsv_parent_id is null and md5(fsv_id::varchar)='$id' order by fsv_model;";
			//log_message('debug','--get_currentParentDetailsToEdit-'.$query);
			$result=$this->db->query($query);
			return $result->result_array();
			
		} catch (Exception $e) {
			log_message('error',$e->getMessage());
		}
	}
	//
	//This function is used to update fuel sensore values
	//(i.e. replace the values in vts_fuel_sensor_value table)
	public function update_fuelvalues($data)
	{
		try 
		{
			$rowcount=0;
			$modelId=$data["modleID"];
			$data["sensorValues"]=str_replace(" ","",$data["sensorValues"]);
			$data["sensorValues"]=str_replace(";"," ",$data["sensorValues"]);
			if(!empty($data["sensorValues"]))
			{
				$fuel_voltage_pair_list=explode(' ', trim($data["sensorValues"]));
				//log_message('debug',print_r($fuel_voltage_pair_list,true));
				$this->db->delete("vts_fuel_sensor_value",array('fs_value_fsv_id'=>$modelId));
				foreach ($fuel_voltage_pair_list as $fuel_volt_pair)
				{
					$split_fuelvolt=explode('-', trim($fuel_volt_pair));				
					$insertData["fs_value_fsv_id"]=$modelId;
					$insertData["fs_value_liters"]=(isset($split_fuelvolt[0]))? $split_fuelvolt[0]:'';
					$insertData["fs_value_volts"]=(isset($split_fuelvolt[0]))?$split_fuelvolt[1]:'';
					$this->db->insert("vts_fuel_sensor_value",$insertData);	
					$rowcount++;
				}
				return "$rowcount Sensor Values Inserted / Updated.";
			}		
		}
		catch (Exception $e) {
			log_message('error',$e->getMessage());
			return "Error";
		}		
	}
	
	public function insert_newMakeModelDetails($data)
	{
		try 
		{			
			$this->db->trans_start();
			if($data["companyNameID"]==null)
			{	
				$insertData["fsv_model"]=$data["companyName"];
				$this->db->insert("vts_fuel_sensor_vehicle",$insertData);
				$query=$this->db->query("SELECT currval(pg_get_serial_sequence('vts_fuel_sensor_vehicle', 'fsv_id')) as id;");
				$result=$query->row_array();
				$id=$result['id'];								
			}else{
				$id=$data["companyNameID"];
			}
			$insertData["fsv_model"]=$data["modleName"];
			$insertData["fsv_parent_id"]=$id;
			$this->db->insert("vts_fuel_sensor_vehicle",$insertData);			
			$query=$this->db->query("SELECT currval(pg_get_serial_sequence('vts_fuel_sensor_vehicle', 'fsv_id')) as id;");
			$result=$query->row_array();
			$id=$result['id'];								
			$this->db->trans_complete();
			return $id;
		} 
		catch (Exception $e)
		{
			log_message('error',$e->getMessage());
		}
	}
	
	public function get_modelForAutoComplete($startName,$isParent='parent')
	{
		try 
		{
			$query="Select fsv_id, fsv_model from vts_fuel_sensor_vehicle where upper(fsv_model) like upper('$startName%') and fsv_parent_id is";
			$query.=($isParent=='parent')?" null":" not null";
			$query.=" order by fsv_model;";
			//log_message("debug","--get_modelForAutoComplete- $query");
			$result=$this->db->query($query);
			return $result->result_array();
		}
		catch (Exception $e) 
		{
			log_message('error',$e->getMessage());
		}
		
	
	}
}
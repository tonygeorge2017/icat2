<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fuel_mis_ii_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
	}

	public function get_allClient()
	{
		$this->db->order_by('client_name','ASC');
		$this->db->select('client_id, client_name');
		$this->db->where('client_id !=', AUTOGRADE_USER);
		$query = $this->db->get('vts_client');
		$result=$query->result_array();
		//log_message('debug',print_r($result,true));
		return $result;
	}

	public function get_alldriver($id)
	{
		$command="Select driver_id as id,driver_name as value from vts_driver where driver_client_id=$id order by driver_name asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_allvhgp($id)
	{
		$command="Select vehicle_group_id as id,vehicle_group as value from vts_vehicle_group where vehicle_group_client_id=$id order by vehicle_group asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_allvh($id)
	{
		$command="Select vehicle_id as id,vehicle_regnumber as value,vehicle_mileage as fmg, vehicle_fuel_consumption as fcm from vts_vehicle where vehicle_group_id=$id order by vehicle_regnumber asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_run_data($id, $f_date)
	{
		$time_diff=$GLOBALS['ID']['sess_time_zonediff'];
		$command="Select EXTRACT(EPOCH from(gps_datetime + interval '1 hour' * $time_diff))epo,(gps_datetime + interval '1 hour' * $time_diff) dt,gps_latitude lat,gps_longitude lng,gps_digitalinputstatus st,gps_speed spd,gps_driver_id dr from server_gps_data where gps_digitalinputstatus='Y'
		and gps_vehicle_id=$id 
		and (gps_datetime + interval '1 hour' * $time_diff)::date = '$f_date';";
		//log_message('debug',$command);
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
}
?>
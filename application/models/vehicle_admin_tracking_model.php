<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_admin_tracking_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
		$GLOBALS['sess_time_zonediff']=$GLOBALS['ID']['sess_time_zonediff'];
	}	
	
	public  function get_gps_data($testerspeed,$imeino,$fdate,$tdate)
	{		
		try
		{	
			$time_diff=$GLOBALS['sess_time_zonediff'];
			if($imeino!=0)//to get current gps location for selected vehicle only.(i.e. One vehicle at a time)
			{
				$test="'test'";
				 // to back track the gps loction for selected vehicle.(i.e. inbetween particular date&time)
				$command='SELECT gps_angleorientationcog as "angle",gps_runningno as "runno",gps_imeino as "imeino", gps_digitalinputstatus as "status", '.$test.' as "driver",  '.$test.' as "vehicle", '.$testerspeed.' as "speedLimit", 
gps_latitude as "lat", gps_longitude as "lng", gps_speed as "speed",gps_datetime as "gpsdatetime", (gps_datetime + interval \'1 hour\' * '.$time_diff.')as "dateTime", 
gps_digitalinputstring as "digitalio", gps_adcavalue as "adca",	gps_adcbvalue as "adcb", gps_adccvalue as "adcc", gps_adcdvalue as "adcd", gps_adcevalue as "adce",
gps_cankvalue as "cank", gps_canlvalue as "canl", gps_canmvalue as "canm", gps_cannvalue as "cann", gps_canovalue as "cano"	FROM';
				$command.="  server_gps_data  where gps_datetime between '".$fdate."' and '".$tdate."' and gps_imeino like '%$imeino' and (gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$') order by gps_datetime asc;";
			//log_message('debug',"********".$command);
			$query=$this->db->query($command);
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
			}
			else 
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	//this function is used to get all current vehicles for given vehicle group(location).
	public function get_all_live_vehicle_data($testerspeed, $imeino=null)
	{
		try 
		{
			if($imeino!=null)
			{
			//
				$time_diff=$GLOBALS['sess_time_zonediff'];
				$command='select  live_runningno as "runno", live_imeino as "imeino", live_latitude as "lat", live_longitude as "lng", live_speed as "speed", live_vehicle_regnumber as "vehicle",live_driver_name as "driver", 
				'.$testerspeed.' as "speedLimit", live_digitalinputstatus as "status", live_datetime as "gpsdatetime", (live_datetime + interval \'1 hour\' * '.$time_diff.') as "dateTime", 
				live_digitalinputstring as "digitalio", live_adcavalue as "adca",	live_adcbvalue as "adcb", live_adccvalue as "adcc", live_adcdvalue as "adcd", live_adcevalue as "adce",
				live_cankvalue as "cank", live_canlvalue as "canl", live_canmvalue as "canm", live_cannvalue as "cann", live_canovalue as "cano" FROM live_gps_data
				where 1=1';

				$command.=" and live_imeino like '%$imeino'";
				//log_message('debug',"***get_all_live_vehicle_data*****".$command);			
				$query=$this->db->query($command);			
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else{
					$test="'test'";
					// to back track the gps loction for selected vehicle.(i.e. inbetween particular date&time)
					$command='SELECT gps_runningno as "runno", gps_imeino as "imeino", gps_digitalinputstatus as "status", '.$test.' as "driver",  '.$test.' as "vehicle", '.$testerspeed.' as "speedLimit", 
					gps_latitude as "lat", gps_longitude as "lng", gps_speed as "speed",gps_datetime as "gpsdatetime",(gps_datetime + interval \'1 hour\' * '.$time_diff.')as "dateTime", 
					gps_digitalinputstring as "digitalio", gps_adcavalue as "adca",	gps_adcbvalue as "adcb", gps_adccvalue as "adcc", gps_adcdvalue as "adcd", gps_adcevalue as "adce",
					gps_cankvalue as "cank", gps_canlvalue as "canl", gps_canmvalue as "canm", gps_cannvalue as "cann", gps_canovalue as "cano"	FROM';
					$command.="  server_gps_data  where gps_imeino like '%$imeino' order by gps_data_id desc limit 1;";
					$query=$this->db->query($command);			
					$result = $query->result_array();
					if ($result != null)
						return $result;
					else
						return null;
				}
			}
			else
				return null;
		}catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}		
	}	

}
<?php

class Client_license_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
	}	
	
	/*
	 * This function used to return the client detail relate the give parameter(i.e. clientID)
	 * if any error, then return null.
	 */
	public function get_vts_client_license($rowPerPage=null,$startNo=null,$client_id=null)
	{
		try
		{
			if($rowPerPage==null & $client_id==null)
			{
				$command="select client_name, client_license_cancel_remarks, client_license_is_active, client_contact_person, client_contact_phone,client_license_id, client_license_client_id, client_license_start_date, client_license_expiry_date  from vts_client_license license1,vts_client client where client_license_expiry_date = (select max(client_license_expiry_date) from vts_client_license license2 where license2.client_license_client_id = license1.client_license_client_id)and client_id = client_license_client_id;";
				$query = $this->db->query($command);
// 				log_message('error','****2nd');
				return $query->result_array();
			}
			else if($client_id==null)
			{
				$command="select client_name, client_license_cancel_remarks, client_license_is_active, client_contact_person, client_contact_phone,client_license_id, client_license_client_id, client_license_start_date, client_license_expiry_date  from vts_client_license license1,vts_client client where client_license_expiry_date = (select max(client_license_expiry_date) from vts_client_license license2 where license2.client_license_client_id = license1.client_license_client_id)and client_id = client_license_client_id limit ".$rowPerPage." offset ".$startNo.";";
				$query = $this->db->query($command);
// 				log_message('error','****2nd');
				return $query->result_array();
					
			}
			else
			{
				$command="select client_license_users, client_license_cancel_remarks, client_license_vehicles, client_license_drivers, client_license_remark, client_license_is_active, client_name, client_contact_person, client_contact_phone,client_license_id, client_license_client_id, client_license_start_date, client_license_expiry_date  from vts_client_license license1,vts_client client where client_license_expiry_date = (select max(client_license_expiry_date) from vts_client_license license2 where license2.client_license_client_id = license1.client_license_client_id)and client_id = client_license_client_id and client_license_id ='".$client_id."';";
				$query = $this->db->query($command);
// 				log_message('error','*****************************'.$command);
				return $query->row_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_client_license',$ex->getMessage());
			return null;
		}
	}
	
	
	/*
	 * To delete the user role from the vts_client table
	 */
	
	public function editOrRenewOrCancel_vts_client_license($client_id,$data1,$operation)
	{
		try
		{
			if($operation=="renew")
			{
				$data1['client_license_created_datetime'] = date('Y/m/d H:i:s');
				$this->db->insert('vts_client_license', $data1);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('client_license_id', $client_id);
				$this->db->update('vts_client_license', $data1);
				return true;
			}
			else if($operation=="cancel")
			{
				$data1['client_license_cancelled_by_user_id'] = $GLOBALS['ID']['sess_userid'];
				$data1['client_license_cancelled_datatime'] = date('Y/m/d H:i:s');
				$this->db->where('client_license_id', $client_id);
// 				$this->db->delete('vts_client');
				$this->db->update('vts_client_license', $data1);
				return true;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_client_license',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is used to all the dealers from the
	 * dealer table to fill the drop down in cleint view.
	 */
	public function get_client_details($client_license_client_id=null)
	{
		try{
// 			$this->db->order_by('dealer_name','ASC');
// 			$this->db->select('*');
// 			$query = $this->db->get_where('vts_client',array('client_id'=>$client_license_client_id));
		if($client_license_client_id != null)
			$query = $this->db->query("select * from vts_client where client_id =". $client_license_client_id.";");
// 		else
// 			$query = $this->db->query("select * from vts_client");
			
			return $query->row_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get client names - vts_client');
			return null;
		}
	}


}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Parent_tracking_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
		$GLOBALS['sess_time_zonediff']=$GLOBALS['ID']['sess_time_zonediff'];
		$GLOBALS['sessParentID']=$GLOBALS['ID']['sess_parentid'];
	}	
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients(){
		try
		{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name ');
			return null;
		}
	}
		
	public function get_student()
	{
		try{
			$result = array();			
			$command="SELECT pt_stop_id st_id, pt_stop_parent_id st_pid, pt_stop_student_name st_name, pt_stop_student_class st_class, pt_stop_student_rfid st_rfid, stop_id stp_id, stop_name stp_name, stop_latitude stp_lat, stop_longitude stp_lng FROM vts_parent_stop inner join vts_stops_list on stop_id  =pt_stop_vh_stop_id where pt_stop_isactive='1' AND stop_is_busstop = '1' AND pt_stop_parent_id=".$GLOBALS['sessParentID'].";";		

			log_message('error', '***get_student*** '. $command);	
			$student_query = $this->db->query($command);
			
			if($student_query)
				$result = $student_query->result_array();
			return $result;
		}catch(Exception $ex)
		{
			log_message('error','get all get_student');
			return array();
		}
	}
	
	public function live_track($id, $dt)
	{
		$result = array();		
		$date_value=new DateTime();
		$curr_datetime = $date_value->format('Y-m-d H:i:s');		
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		$p_stud_id = 0;
		$live_result = array();
		//$swipe_hist = array();
		if($id){	
			/*
			$command = "SELECT pt_stop_student_name stud_name, rf_dr, rf_drname, rf_drmobile, driver_name dr, driver_mobile dr_mob, to_char((rfid_gps_timestamp + interval '1 hour' * $time_diff), 'DD-MM-YYYY HH24:MI:SS') rfid_tm, to_char((live_datetime + interval '1 hour' * $time_diff), 'DD-MM-YYYY HH24:MI:SS') tm, live_vehicle_regnumber vh, live_vehicle_speedlimit splmt, live_latitude lat, live_longitude lng, ((live_speed :: numeric) * 1.852) sp, live_digitalinputstatus ig, live_angleorientationcog ang, rfid_dec rfid FROM live_gps_data inner join vts_rfid_log on rfid_vehicle =  live_vehicle_id inner join vts_parent_stop on pt_stop_id = rfid_student left join vts_driver on driver_id = live_driver_id left join ( select r.rfid_vehicle rf_vh, r.rfid_driver rf_dr, d.driver_name rf_drname, d.driver_mobile rf_drmobile from vts_rfid_log r inner join vts_driver d on d.driver_id = r.rfid_driver	where r.rfid_gps_timestamp > '$dt' and r.rfid_driver is not null order by r.rfid_gps_timestamp desc limit 1) x on rf_vh=live_vehicle_id	where live_datetime >= '$dt' AND rfid_added_on at time zone 'utc'  >= '$dt' AND pt_stop_parent_id = ".$GLOBALS['sessParentID'];
			$command .= ($id != 'All')?" AND rfid_student = $id" : " ";
			*/
			$command = "SELECT rfid_student stud_id, pt_stop_student_name stud_name, rfid_dec rf_dec, rf_dr, rf_drname, rf_drmobile, driver_name dr, driver_mobile dr_mob,";
			$command .= " to_char((rfid_gps_timestamp + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH24:MI:SS') rfid_tm,";
			$command .= " to_char((rfid_added_on + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH24:MI:SS') rfid_rectm, ";
			$command .= " to_char((live_datetime + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH24:MI:SS') tm,";
			$command .= " extract(epoch from  ('".$curr_datetime."'::TIMESTAMP - live_datetime::TIMESTAMP))/60 as diff,";
			$command .= " live_vehicle_regnumber vh, live_vehicle_speedlimit splmt,";
			$command .= " live_latitude lat, live_longitude lng, live_fix, gps_latitude rf_lat, gps_longitude rf_lng, gps_fix rf_gps_fix,";
			$command .= " ((live_speed :: numeric) * 1.852) sp, live_digitalinputstatus ig, live_angleorientationcog ang, rfid_dec rfid";
			$command .= " FROM live_gps_data ";
			$command .= " inner join vts_rfid_log on rfid_vehicle =  live_vehicle_id";
			$command .= " inner join vts_parent_stop on pt_stop_id = rfid_student";
			$command .= " inner join server_gps_data on gps_data_id = rfid_gps_log_id";
			$command .= " left join vts_driver on driver_id = live_driver_id";
			$command .= " left join (";
				$command .= " select r.rfid_vehicle rf_vh, r.rfid_driver rf_dr, d.driver_name rf_drname, d.driver_mobile rf_drmobile";
				$command .= " from vts_rfid_log r inner join vts_driver d on d.driver_id = r.rfid_driver";
				$command .= " where";
				$command .= " r.rfid_gps_timestamp > '$dt'";
				$command .= " AND d.driver_client_id = ".$GLOBALS['sessClientID'];
				$command .= " and r.rfid_driver is not null";
				$command .= " order by r.rfid_gps_timestamp desc limit 1";
			$command .= " ) x on rf_vh=live_vehicle_id";
			$command .= " where live_datetime >= '$dt'";
			$command .= " AND pt_stop_parent_id=".$GLOBALS['sessParentID'];
			$command .= " AND rfid_added_on at time zone 'utc'  >= '$dt'";
			$command .= ($id != 'All')?" AND rfid_student = $id" : " ";
			$command .= " order by rfid_student, rfid_added_on desc;";
		    log_message('error', '***live_track*** '. $command);
			
			$query_res=$this->db->query($command);
			if($query_res)
			{
				$result = $query_res->result_array();
			}			
			
			foreach($result as $k => $row)
			{
				if($row['stud_id'] != $p_stud_id || $k == 0)
				{		 		
					$l_result['stud_id'] = $row['stud_id'];// => 3
					$l_result['stud_name'] = $row['stud_name'];// => Prajeesh
					$l_result['stud_rfid'] = $row['rf_dec'];// => 0005105627
					$l_result['dr'] = ($row['rf_dr']) ? $row['rf_drname'] : $row['dr'];// => Driver 01
					$l_result['dr_mob'] = ($row['rf_dr']) ? $row['rf_drmobile'] : $row['dr_mob'];// => 5124441111
					$l_result['rfid_tm'] = $row['rfid_tm'];// => 25-11-2017  15:42:20
					$l_result['rfid_rectm'] = $row['rfid_rectm'];// => 25-11-2017  21:12:28
					$l_result['tm'] = $row['tm'];// => 25-11-2017  16:44:00
					$l_result['diff'] = $row['diff'];// => 330.266666666667
					$l_result['vh'] = $row['vh'];// => Bus-01
					$l_result['splmt'] = $row['splmt'];// => 40
					$l_result['lat'] = $row['lat'];// => 9.99926666666667
					$l_result['lng'] = $row['lng'];// => 76.2887833333333
					$l_result['live_fix'] = $row['live_fix'];// => A
					$l_result['rf_lat'] = $row['rf_lat'];// => 9.99926666666667
					$l_result['rf_lng'] = $row['rf_lng'];// => 76.2887833333333
					$l_result['rf_gps_fix'] = $row['rf_gps_fix'];// => A
					$l_result['sp'] = $row['sp'];// => 0.000
					$l_result['ig'] = $row['ig'];// => Y
					$l_result['ang'] = $row['ang'];// => 0
					$l_result['rfid'] = $row['rfid'];// => 0005105627
					$l_result['notify_msg'] = '<div id="content"><h3 id="firstHeading" class="firstHeading">Tracking Detail of '.$l_result['stud_name'].'</h3><div id="bodyContent"><p><b>Vehicle : </b>'.$l_result['vh'].'<br><b>Driver : </b>'.$l_result['dr'].'<br><b>Mobile : </b>'.$l_result['dr_mob'].'<br><b>Speed(km) : </b>'.$l_result['sp'].'<br><b>Speed Limit(km) : </b>'.$l_result['splmt'].'<br><b>RF ID : </b>'.$l_result['rfid'].'<br><b>Swipe at : </b>'.$l_result['rfid_tm'].'<br><b>Tracking : </b>'.$l_result['tm'].'</p></div></div>';
					$l_result['tooltip_msg'] = 'Tracking Detail of '.$l_result['stud_name'].'<br>Vehicle : '.$l_result['vh'].'<br>Driver : '.$l_result['dr'].'<br>Mobile : '.$l_result['dr_mob'].'<br>Speed(km) : '.$l_result['sp'].'<br>Speed Limit(km) : '.$l_result['splmt'].'<br>RF ID : '.$l_result['rfid'].'<br>Swipe at : '.$l_result['rfid_tm'].'<br>Tracking : '.$l_result['tm'];
					$live_result[] = $l_result;
				}	
				$p_stud_id = $row['stud_id'];
			}
		}
		//log_message('error', '***live_track*Result** '. print_r($live_result, true));
		return $live_result;
	}
    /*
	This function return list of group by swiped vehicles
	*/
	public function swiped_vehicles($studentid, $fdate, $tdate)
	{
		$result=array();
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		$this->swiped_vehicles01($studentid, $fdate, $tdate);// This function call is just for testing. Need to remove once its OK.
		if($studentid && $fdate && $tdate && $studentid != 'All')
		{
			$command="SELECT rfid_student, rfid_vehicle vhid, vehicle_regnumber vh,";
			$command .= " array_to_string(array_agg(to_char((rfid_gps_timestamp + interval '1 hour' * $time_diff), 'DD-MM-YYYY HH24:MI:SS')), ', ') swap_time,";
			$command .= " count(*) swap_cnt";
			$command .= " FROM vts_rfid_log";
			$command .= " inner join vts_vehicle on vehicle_id=rfid_vehicle";
			$command .= " where rfid_student = $studentid";
			$command .= " AND rfid_gps_timestamp between '$fdate' AND '$tdate'";
			$command .= " group by rfid_student, rfid_vehicle, vehicle_regnumber;";
				//AND rfid_added_on at time zone 'utc' between '$fdate' AND '$tdate'"
			log_message('error', '***swiped_vehicles*** '. $command);		
			$swipe_query = $this->db->query($command);
			if($swipe_query)
				$result = $swipe_query->result_array();
		}
		return $result;
	}
	
	/*
	This function return list , but not group by swiped vehicles
	*/
	public function swiped_vehicles01($studentid, $fdate, $tdate)
	{
		$result=array();
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		if($studentid && $fdate && $tdate && $studentid != 'All')
		{
			$command="SELECT rfid_student, rfid_dec rfid, rfid_vehicle vhid, vehicle_regnumber vh, gps_latitude lat, gps_longitude lng, gps_fix fix, ";
			$command .= " to_char((rfid_gps_timestamp + interval '1 hour' * $time_diff), 'DD-MM-YYYY HH24:MI:SS') swap_time,";
			$command .= " to_char(((rfid_added_on at time zone 'utc') + interval '1 hour' * $time_diff), 'DD-MM-YYYY HH24:MI:SS') swap_rectime";
			$command .= " FROM vts_rfid_log";
			$command .= " inner join vts_vehicle on vehicle_id=rfid_vehicle";
			$command .= " inner join server_gps_data on gps_data_id = rfid_gps_log_id";
			$command .= " where rfid_student = $studentid";
			$command .= " AND ((rfid_gps_timestamp between '$fdate' AND '$tdate') OR (rfid_added_on at time zone 'utc'  between '$fdate' AND '$tdate'))";
			$command .= " ORDER BY rfid_student, rfid_vehicle, rfid_added_on;";
				//AND rfid_added_on at time zone 'utc' between '$fdate' AND '$tdate'"
			log_message('error', '***swiped_vehicles01*** '. $command);		
			$swipe_query = $this->db->query($command);
			if($swipe_query)
				$result = $swipe_query->result_array();
		}
		return $result;
	}

	public function history_tracking($vehicleid, $fdate, $tdate)
	{
		$result = array();	
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];	
		if($vehicleid && $fdate && $tdate)
		{
			$command="SELECT gps_angleorientationcog as ang, gps_digitalinputstatus as ign, driver_name as driver, vehicle_regnumber as vehicle,";
			$command .= " vehicle_speedlimit as splt, gps_latitude as lat, gps_longitude as lng, gps_speed as speed,";
			$command .= " (gps_datetime + interval '1 hour' * $time_diff) as dtime";
			$command .= " FROM vts_vehicle";
			$command .= " left outer join server_gps_data on vehicle_id=gps_vehicle_id";			
			$command .= " left outer join vts_driver on driver_id=gps_driver_id";
			$command .= " where gps_datetime between '$fdate' and '$tdate'";
			$command .= " and vehicle_id ='$vehicleid'";
			$command .= " and (gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$')";
			$command .= " and (gps_fix='A' OR gps_fix is null)";
			$command .= " order by gps_datetime asc;";
			log_message('error', '***history_tracking*** '. $command);
			$history_query = $this->db->query($command);
			if($history_query)
				$result = $history_query->result_array();
		}
		return $result;
	}
}
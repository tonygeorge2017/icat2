<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tracking_Data_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
		$GLOBALS['sess_time_zonediff']=$GLOBALS['ID']['sess_time_zonediff'];
	}
	
	public  function get_gps_data($vh_id,$track_strt_date,$track_end_date)
	{
		try
		{
			$time_diff=$GLOBALS['sess_time_zonediff'];
			
			$command="SELECT vehicle_regnumber as temp_vehicle_name, gps_runningno, gps_imeino, (gps_datetime + interval '1 hour' * $time_diff)as gps_datetime, gps_latitude, 
gps_longitude, round((gps_speed::numeric * 1.852),2) as gps_speed,  gps_digitalinputstatus ,driver_name as driver_name         
FROM server_gps_data inner join vts_vehicle on vehicle_id=gps_vehicle_id 
left outer join vts_driver on driver_id=gps_driver_id 
where gps_vehicle_id ='".$vh_id."' and (gps_datetime + interval '1 hour' * $time_diff) 
between '".$track_strt_date." 00:00:00' and '".$track_end_date." 23:59:59' order by gps_datetime asc;";
			 // to back track the gps loction for selected vehicle.(i.e. inbetween particular date&time)
			//$command="SELECT temp_time_diff, temp_vehicle_name, gps_imeino, gps_latitued,gps_longitude,gps_speed,gps_datetime FROM gps_data, temp_vehicle where gps_imeino ='".$imeiNo."' and gps_datetime between timestamp '".$fdate."' - time '05:30' and timestamp '".$tdate."' - time '05:30' and temp_device_imei_no = gps_imeino order by gps_datetime asc;";
			/*$command="SELECT get_vehicle_name(trip_register_vehicle_id) as temp_vehicle_name, gps_runningno, gps_imeino, get_user_datetime(gps_datetime,".$GLOBALS['sessUserID'].") as gps_datetime, gps_latitude, 
       gps_longitude, round((gps_speed::numeric * 1.852),2) as gps_speed,  gps_digitalinputstatus ,get_driver_name(gps_driver_id) as driver_name         
  	   FROM server_gps_data, vts_trip_register where gps_vehicle_id ='".$vh_id."' and get_user_datetime(gps_datetime,".$GLOBALS['sessUserID'].") 
  	   between '".$track_strt_date." 00:00:00' and '".$track_end_date." 23:59:59' and 
  	   trip_register_vehicle_id = gps_vehicle_id order by gps_datetime asc;";*/
			//log_message('debug',"****** $command");
			$query=$this->db->query($command);
			
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients(){
		try
		{
			$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER." order by client_name";
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	public function get_all_vhGp_vehicle($vehicleGpID)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{
				$command="select vehicle_id, vehicle_regnumber from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and  vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle group for the given client ID
	 */
	public function get_all_vhGp($clientID=null,$sessClient=null,$sessUser=null)
	{
		try
		{
			if($GLOBALS['sessClientID']!=null)
			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$clientID."' order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and vehicle_group_client_id='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	
	public  function get_vehicle_name($vh_id)
	{
		try
		{
			$command="SELECT get_vehicle_name('".$vh_id."') as temp_vehicle_name;";
			$query=$this->db->query($command);
			$row = $query->row_array ();			
			if ($row != null)
				return $row ['temp_vehicle_name'];
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get vehicle name for given imei no',$ex->getMessage());
			return null;
		}
	}
	//csv
	public function InsertOrdelete_vts_tmp_report_tracking($data,$operation,$chk_btn)
	{
		try
		{
			if($operation=="insert")
			{
				if(!empty($data))
				{
					foreach ($data as $data1)
					{
						$data2['vehicle'] = $data1['temp_vehicle_name'];
						$data2['gps_runningno'] = $data1['gps_runningno'];
	                 	$data2['imei'] = $data1['gps_imeino'];
						$data2['date_time'] = $data1['gps_datetime'];
						$data2['speed'] = $data1['gps_speed'];
						$data2['status'] = $data1['gps_digitalinputstatus'];
						$data2['latitude'] = $data1['gps_latitude'];
						$data2['longitude'] = $data1['gps_longitude'];
						$data2['spot'] = $data1['spot'];
	                 	$data2['driver'] = $data1['driver_name'];
						$data2['user_id'] = $GLOBALS['sessUserID'];
		
						$this->db->insert('vts_tmp_report_tracking', $data2);
					}
				}
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('user_id', $GLOBALS['sessUserID']);
				if($this->db->delete('vts_tmp_report_tracking'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_tmp_report_tracking',$ex->getMessage());
			return false;
		}
	}
	
	public function get_alltrackingdata()
	{
		try{
			$command = "select * from vts_tmp_report_tracking where user_id = '".$GLOBALS['sessUserID']."' order by date_time;";
			$query_final = $this->db->query($command);
			return $query_final->result_array();
		}
		catch(Exception $ex)
		{
			log_message('error','get all tracking - vts_tmp_report_tracking');
			return null;
		}
	}
	
 }
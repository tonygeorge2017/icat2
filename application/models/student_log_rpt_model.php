<?php if(! defined('BASEPATH')) exit ('No direct script access allowed');
class Student_log_rpt_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$GLOBALS['ID'] = $this->session->userdata('login');
		log_message('debug','session'.print_r($GLOBALS['ID'],true));
	}
	
	public function get_allClient()
	{
		$result = array();
		$this->db->order_by('client_name','ASC');
		$this->db->select('client_id as "id", client_name as "name"');
		$this->db->where('client_is_school =', '1');
		if($GLOBALS['ID']['sess_user_type'] != AUTOGRADE_USER)
		{
			$this->db->where('client_id', $GLOBALS['ID']['sess_clientid']);
		}
		$query = $this->db->get('vts_client');
		if($query)
			$result=$query->result_array();
		log_message('debug','**Student_log_rpt_model**get_allClient '.print_r($result,true));
		return $result;
	}
	
	public function get_allvhgp($id)
	{
		$result = array();
		$this->db->order_by('vehicle_group');
		$this->db->select('vehicle_group_id as "id", vehicle_group as "name"');
		$this->db->where('vehicle_group_isactive', '1');
		if($GLOBALS['ID']['sess_user_type'] != AUTOGRADE_USER){
			$this->db->where('vehicle_group_client_id', $GLOBALS['ID']['sess_clientid']);
		}
		else{
			$this->db->where('vehicle_group_client_id', $id);
		}
		$query = $this->db->get('vts_vehicle_group');
		if($query)
			$result=$query->result_array();
		log_message('debug','**Student_log_rpt_model**get_allvhgp '.print_r($result,true));
		return $result;
	}
	
	public function get_allvh($id)
	{
		$result = array();
		$this->db->order_by('vehicle_regnumber');
		$this->db->select('vehicle_id as "id", vehicle_regnumber as "name"');
		$this->db->where('vehicle_isactive', '1');
		if($GLOBALS['ID']['sess_user_type'] != AUTOGRADE_USER){
			$this->db->where('vehicle_client_id', $GLOBALS['ID']['sess_clientid']);
		}
		$this->db->where('vehicle_group_id', $id);
		$query = $this->db->get('vts_vehicle');
		if($query)
			$result=$query->result_array();
		log_message('debug','**Student_log_rpt_model**get_allvh '.print_r($result,true));
		return $result;
	}
	
	public function get_log($vh_gp, $fdate, $tdate, $vh='All')
	{
		$result = array();
		/*
		{ text: 'Sl. No.', style: 'tableHeader' },
		{ text: 'Date & Time', style: 'tableHeader'}, 						
		{ text: 'Student', style: 'tableHeader'}, 
		{ text: 'Class', style: 'tableHeader'},
		{ text: 'Mobile No.', style: 'tableHeader'},
		{ text: 'Bus', style: 'tableHeader' }
		*/
		$this->db->select("rfid_dec rfid, rfid_gps_timestamp gts, (rfid_gps_timestamp + interval '1hour' * ".$GLOBALS['ID']['sess_time_zonediff'].") isdts, rfid_added_on added, vehicle_regnumber vh, pt_stop_student_name st, pt_stop_student_class stclass, parent_name pt, parent_mobile ptmob");
		$this->db->from('vts_rfid_log');
		$this->db->join('vts_vehicle','vehicle_id = rfid_vehicle','inner');
		$this->db->join('vts_parent_stop','pt_stop_id = rfid_student', 'inner');
		$this->db->join('vts_parent', 'parent_id = pt_stop_parent_id', 'inner');
		$this->db->where('vehicle_group_id', $vh_gp);
		$this->db->where('rfid_added_on >=', $fdate);
		$this->db->where('rfid_added_on <=', $tdate);
		if(is_array($vh))
		{
			$this->db->where_in('vehicle_id', $vh);
		}
		$this->db->order_by('rfid_added_on');
		$this->db->order_by('vehicle_regnumber');
		$query = $this->db->get();
		log_message('debug', '**get_log** : '.$this->db->last_query());
		if($query)
			$result=$query->result_array();		
		return $result;
	}
}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_geofence_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');	
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
	}
	public function get_VhRouteList( $rowPerPage=null, $startNo=null, $routeID=null)
	{
		try
		{			
			$command="select r.route_id, r.route_user_define_id, r.route_name,  r.route_is_geofence, r.route_is_active,count(rs.rt_stp_stop_id) as stop_count
from vts_route r left outer join vts_route_stop rs on r.route_id=rs.rt_stp_route_id where 1=1 and route_is_geofence='".ACTIVE."'"; 
			
			if($routeID!=null) 
				$command.=" and md5(r.route_id::varchar)='".$routeID."'";
			$command.=" group by r.route_id, r.route_user_define_id, r.route_name, r.route_is_geofence, r.route_is_active order by r.route_name ";
			if($rowPerPage!=null)
				$command .= " limit ".$rowPerPage." offset ". $startNo;
			$command .= ";";
			//log_message("debug","***get_VhRouteList***".$command);
			$query = $this->db->query($command);
			$result=null;
			if($query)
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		} 
		catch (Exception $e) {
			log_message('error', 'get_VhRouteList '.$e->getMessage());
			return null;
		}
	}

	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allClients(){
		try
		{
			/*$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER; 
			if($GLOBALS['sessClientID']!=AUTOGRADE_USER)
				$command.=" and client_id=".$GLOBALS['sessClientID'];
			$command.=" order by client_name";
			$query = $this->db->query($command);
			$result=$query->result_array();
			*/
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			$result = $query->result_array();
			
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to insert or edit the route
	 */	
	public function insert_edit_route($p_data, $p_routeID=null, $p_opt="insert")
	{
		try
		{
			$status=0;
			if($p_opt == "insert" && $p_routeID == null)
			{
				$status=$this->db->insert('vts_route', $p_data);
				$command="SELECT currval(pg_get_serial_sequence('vts_route', 'route_id')) as id;";
				if($status == 1)
				{
					$query=$this->db->query("$command");
					$result=$query->row_array();
					$status=$result['id'];
				}
			}
			elseif($p_opt == "update" && $p_routeID != null)
			{
				$this->db->where('route_id', $p_routeID);
				$status=$this->db->update('vts_route', $p_data);												
			}
			return $status;
			
		}catch(Exception $ex)
		{
			log_message('error',"Error on inserting or edit data in vts_route. ".$ex->getMessage());
		}
	}
	/*
	 * This function used to get the route details
	 */
	public function get_routeDetails($p_routeID)
	{
		try {
			if($p_routeID!=null)
			{
				$command="select  sl.stop_id as stop_id, sl.stop_name as name, sl.stop_latitude as lat, sl.stop_longitude as lng
from vts_stops_list sl inner join vts_route_stop rs on sl.stop_id=rs.rt_stp_stop_id
where rs.rt_stp_route_id='".$p_routeID."' order by rs.rt_stp_slno";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
			
		}catch(Exception $ex)
		{
			log_message('error',"Error on getting route details".$ex->getMessage());
		}
	}
	/*
	 * This function used to insert the marker details and route in vts_stops_list
	 */
	public function insert_stop_list($vhRouteMarkerDetails)
	{
		try{			
			
			$routeID=$vhRouteMarkerDetails->route_id;
			$routeName=$vhRouteMarkerDetails->route_name;
			$latLngDetails=$vhRouteMarkerDetails->details;
			$this->db->trans_start();
			if(count($vhRouteMarkerDetails->undo)>0)
			{
				$id_list_to_delete_on_undo=$vhRouteMarkerDetails->undo;
				$this->db->where_in('rt_stp_stop_id', $id_list_to_delete_on_undo);
				$this->db->delete('vts_route_stop');
			}
				
			$command_slno="select max(rs.rt_stp_slno)as slno from vts_route_stop rs where  rs.rt_stp_route_id='".$routeID."'";
			$query_slno=$this->db->query("$command_slno");
			$result_slno=$query_slno->row_array();
			$slno=$result_slno['slno']+1;
			foreach ($latLngDetails as $row) {
				$stops_list_status=0;$route_stop_status=0;			
				$stops_list['stop_name']=$row->name;
				$stops_list['stop_latitude']=$row->lat;
				$stops_list['stop_longitude']=$row->lng;
				
				if($row->stop_id == null)
				{					
					$stops_list['stop_remarks']=$routeName;
					$stops_list_status=$this->db->insert('vts_stops_list', $stops_list);
					$command="SELECT currval(pg_get_serial_sequence('vts_stops_list', 'stop_id')) as id;";
					if($stops_list_status == 1)
					{
						$query=$this->db->query("$command");
						$result=$query->row_array();											
						$route_stop['rt_stp_route_id']=$routeID;
						$route_stop['rt_stp_stop_id']=$result['id'];						
						$route_stop['rt_stp_slno']=$slno;
						$route_stop_status=$this->db->insert('vts_route_stop', $route_stop);
// 						if($route_stop_status==0)
// 							return false;
						$slno++;
					}
				}else{					
					$this->db->where('stop_id', $row->stop_id);
					$this->db->update('vts_stops_list', $stops_list);
				}				
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === TRUE)			
				return true;
			else
				return false;
		}catch(Exception $ex)
		{
			log_message('error',"Error on inserting list of stops ".$ex->getMessage());
		}
	}
	/*
	 * This function is used to delete the markers.
	 */
	public function delete_markers($routeID){
		try{
			$this->db->trans_start();
			$this->db->where("rt_stp_route_id", $routeID);
			$this->db->delete('vts_route_stop');
			$this->db->trans_complete();
			if ($this->db->trans_status() === TRUE)
				return true;
			else
				return false;
			
		}catch (Exception $ex)
		{
			g_message('error',"Error on delete_markers ".$ex->getMessage());
		}
	}
}
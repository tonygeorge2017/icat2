<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Geofence_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients(){
		try
		{
			/*$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER." order by client_name";
			$query = $this->db->query($command);
			$result=$query->result_array();*/
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	
	/*
	 * This function is used to get the vehicle group for the given client ID
	 */
	public function get_all_vhGp($clientID=null,$sessClient=null,$sessUser=null)
	{
		log_message("debug","client id--".$sessClient."---session client id-- ".$sessClient);
		try
		{
// 			if($GLOBALS['sessClientID']!=null)
// 			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
				{
					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$clientID."' order by vehicle_group;";
				}
				else if($GLOBALS['ID']['sess_user_type'] == DEALER_USER & $GLOBALS['sessClientID']==null)
				{
// 					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and md5(vehicle_group_client_id::varchar)=md5('".$clientID."') order by vehicle_group;";
				}
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
				{
					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and vehicle_group_client_id='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				}
				else
				{
					$command="select vehicle_group_id vh_gp_id, vehicle_group vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
				}
// 				log_message('debug',$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
// 			}
// 			else
// 				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	
	
	public function get_all_vhGp_vehicle($vehicleGpID)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{ 				
				$command="select vehicle_id vh_id, vehicle_regnumber vh_name from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and  vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;";
				$query = $this->db->query($command);
				//log_message('debug','**Vehicle**'.$command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	
	public  function get_gps_data($vh_id,$fdate,$tdate)
	{		
		try
		{	
			if($vh_id!=0)//to get current gps location for selected vehicle only.(i.e. One vehicle at a time)
			{
				 // to back track the gps loction for selected vehicle.(i.e. inbetween particular date&time)
				$command='SELECT gps_digitalinputstatus as "status", driver_name as "driver",  vehicle_regnumber as "vehicle", vehicle_speedlimit as "speedLimit", 
gps_latitude as "lat", gps_longitude as "lng", gps_speed as "speed",get_user_datetime(gps_datetime,'.$GLOBALS['sessUserID'].')as "dateTime" FROM';
				$command.=" vts_vehicle left outer join server_gps_data on vehicle_id=gps_vehicle_id left outer join vts_driver on driver_id=gps_driver_id where vehicle_id ='".$vh_id."' 
and gps_datetime between '".$fdate."' and '".$tdate."' and (gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$') order by gps_datetime asc;";
			//log_message('debug',"********".$command);
			$query=$this->db->query($command);
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
			}
			else 
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	//this function is used to get all current vehicles for given vehicle group(location).
	public function get_all_live_vehicle_data($vhGp, $vh_id=null)
	{
		try 
		{
			$command='select  data.gps_latitude as "lat", data.gps_longitude as "lng", data.gps_speed as "speed", 
veh.vehicle_regnumber as "vehicle",drv.driver_name as "driver", 
veh.vehicle_speedlimit as "speedLimit",
data.gps_digitalinputstatus as "status",
get_user_datetime(data.gps_datetime,'.$GLOBALS['sessUserID'].') as "dateTime" from server_gps_data data
inner join vts_vehicle veh on veh.vehicle_id = data.gps_vehicle_id
inner join vts_driver drv on drv.driver_id = data.gps_driver_id
inner join ';
			$command.="(select v.vehicle_id, max(d.gps_datetime) as date_time from vts_vehicle v 
inner join server_gps_data d on v.vehicle_id = d.gps_vehicle_id
and v.vehicle_group_id= '".$vhGp."'
and v.vehicle_isactive= '".ACTIVE."'";
			if($vh_id!=null)
				$command.=" and v.vehicle_id='".$vh_id."'";
			$command.=" group by v.vehicle_id) as maxdatetime on (data.gps_vehicle_id = maxdatetime.vehicle_id and data.gps_datetime = maxdatetime.date_time)
and (data.gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$');";
			
			//log_message('debug',"***get_all_live_vehicle_data*****".$command);
			$query=$this->db->query($command);
		$result = $query->result_array();
		if ($result != null)
			return $result;
		else
			return null;
		}catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}		
	}
	
	/*
	 * This function is used to get the selected client time difference
	 */
	public function get_time_diff($clientId)
	{
		$command="Select client_time_diff from vts_client where client_id='".$clientId."'";
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row['client_time_diff'];
		else
			return null;
	}
	
	/*
	 * this function is used to get the vehicle predefined route
	 */
	public function get_vh_route($vh_id)
	{		
		$command='Select sl.stop_latitude as "rLat", sl.stop_longitude as "rLng", vehicle_regnumber';
		$command.="||' route' ";
		$command.=' as "vehicleName" from vts_stops_list sl,  vts_vehicle v, vts_vehicle_route vr, vts_route_stop rs where sl.stop_id =rs.rt_stp_stop_id  and v.vehicle_id=vr.vh_rt_vehicle_id and';
		$command.="  vr.vh_rt_route_id=rs.rt_stp_route_id and v.vehicle_id = '".$vh_id."' order by rs.rt_stp_slno;";
		log_message('debug','**command** '.$command);
		$query=$this->db->query($command);
		$result = $query->result_array();		
		return $result;		
	}
}
<?php
Class Settings_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	/*
	 * function to get settings table data 
	 */
	public function get_settings_data()
	{
		try
		{
		$this->db->order_by('setting_id','ASC');
		$this->db->select('*');
		$this->db->from('vts_setting');
		$this->db->where('setting_is_user_defined','1');
		$query = $this->db->get();
		return $query->result();
		}
		catch ( Exception $e ) 
		{
			log_message ( 'error', 'get settings data from settings table', $e->getMessage () );
			return null;
		}
	}
	/*
	 * function to get settings data by passing setting id as parameter
	 */
	public function get_settings_edit_data($setting_id)
	{
		try {
		$this->db->select('*');
		$this->db->from('vts_setting');
		$array = array('setting_is_user_defined' => '1', 'setting_id' => $setting_id);
		$this->db->where($array);
		$query = $this->db->get();
		return $query->row();
		}
		catch ( Exception $e)
		{
			log_message ( 'error', 'edit data in settings table', $e->getMessage () );
			return null;
		}
	}
	/*
	 * function to update setings table
	 */
	public function update_settings_data($setting_id,$data)
	{
		try 
		{
			$this->db->where('setting_id',$setting_id);
			$this->db->update('vts_setting',$data);
		} catch (Exception $e){
			log_message ( 'error', 'update data in settings table', $e->getMessage () );
			return null;
		}
	}
}
?>

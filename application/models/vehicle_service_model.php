<?php

class Vehicle_service_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	
	
	
	
	public function get_all_vhGp_vehicle($vhGrp)
	{
		try
		{
			$command="SELECT vehicle_id, vehicle_regnumber FROM vts_vehicle";
			$query=$this->db->query($command);
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_vehicle',$ex->getMessage());
			return null;
		}
	}
	
	
	/*
	 * This function used to return the vehicle group detail relate the give parameter(i.e. vehicle_group_id)
	 * if any error, then return null.
	 */
	public function get_vts_vehicle_service($rowPerPage=null,$startNo=null,$vehicle_service_id=null,$session_login_client_id=null)
	{
		try
		{
		$command = "select get_service_name(vh_ser_service_id) as service_name, get_vehicle_name(vh_ser_vehicle_id) as vehicle_name, vh_ser_id, vh_ser_service_id, vh_ser_vehicle_id, cast(vh_ser_service_date as date) as vh_ser_service_date, vh_ser_remarks, vehicle_group_client_id, vehicle_group_id, vehicle_group from vts_vehicle_service,vts_vehicle_group where vehicle_group_id=get_vehicle_gp_id(vh_ser_vehicle_id)";
			if($rowPerPage==null & ($vehicle_service_id==null & $session_login_client_id!=md5(AUTOGRADE_USER)))
			{
			
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$command.=" and md5(vehicle_group_client_id::varchar)='". $session_login_client_id."'";
				}	
				$command.=";";
				$query=$this->db->query($command);
				return $query->result_array();	
			}
			else if($vehicle_service_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$command.=" and md5(vehicle_group_client_id::varchar)='". $session_login_client_id."'";
				}		
				$command.=" limit ".$rowPerPage." offset ".$startNo;	
				$command.=";";
				$query=$this->db->query($command);
				return $query->result_array();
							
			}
			else if($session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$command = "select vh_ser_id, vh_ser_service_id, vh_ser_vehicle_id, vh_ser_service_date, vh_ser_remarks, vehicle_group_client_id, vehicle_group_id, vehicle_group, service_name from vts_vehicle_service,vts_vehicle_group,vts_service where vehicle_group_id=get_vehicle_gp_id(vh_ser_vehicle_id) and md5(vh_ser_id::varchar)='".$vehicle_service_id."';";
// 				$command = "select vh_ser_id, vh_ser_service_id, vh_ser_vehicle_id, vh_ser_service_date, vh_ser_remarks, vehicle_group_client_id, vehicle_group_id from vts_vehicle_service,vts_vehicle_group where vehicle_group_id=get_vehicle_gp_id(vh_ser_vehicle_id);";
				$query=$this->db->query($command);
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_vehicle_service',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the vehicle group details from the vts_vehicle_group table
	 */
	
	public function InsertOrUpdateOrdelete_vts_vehicle_service($vehicle_service_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_vehicle_service', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('vh_ser_id', $vehicle_service_id);
				$this->db->update('vts_vehicle_service', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('md5(vh_ser_id::varchar)', $vehicle_service_id);
				if($this->db->delete('vts_vehicle_service'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_vehicle_group',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in vehicle service screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehical service name - vts_vehicle_service');
			return null;
		}
	}
	
	/*
	 * This function is to get all the vehicles from vehicle table and shown
	 * client name in drop down in vehicle service screen
	 */
	public function get_allVehicles($vehicleGpID=null)//$session_login_client_id,
	{
		try{
// 			if($session_login_client_id!=AUTOGRADE_USER)
// 			{
			if($vehicleGpID!=null)
				{
				$this->db->order_by('vehicle_regnumber','ASC');
				$this->db->select('vehicle_id, vehicle_regnumber');
				$this->db->where('vehicle_isactive', ACTIVE);
// 				if($session_login_client_id!=AUTOGRADE_USER)
// 				{
// 					$this->db->where('md5(vehicle_client_id::varchar)', $session_login_client_id);
// 				}
				
					$this->db->where('vehicle_group_id', $vehicleGpID);
				
				$query = $this->db->get('vts_vehicle');
				return $query->result_array();
				}return null;
// 			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehical names - vts_vehicle');
			return null;
		}
	}
	
	
	/*
	 * This function is used to get the vehiclegroup from the
	 * vehicle group table to fill the vehicle group dropDownBox in vehicle service view.
	 */
	public function get_allVehicleGroups($session_login_client_id){
		try{
			if($session_login_client_id!=AUTOGRADE_USER)
			{
				$this->db->order_by('vehicle_group','ASC');
				$this->db->select('vehicle_group_id, vehicle_group');
				$this->db->where('vehicle_group_isactive', ACTIVE);
				if($session_login_client_id!=AUTOGRADE_USER)
				{
					$this->db->where('md5(vehicle_group_client_id::varchar)', $session_login_client_id);
				}
				$query = $this->db->get('vts_vehicle_group');
				return $query->result_array();
			}
				
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group name - vts_vehicle_group');
			return null;
		}
	}
	

	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in vehicle service screen
	 */
	public function get_allServices()
	{
		try{
			$this->db->order_by('service_name','ASC');
			$this->db->select('service_id, service_name');
			$query = $this->db->get('vts_service');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all service names - vts_service');
			return null;
		}
	}

}
?>
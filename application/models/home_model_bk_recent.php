<?php

class Home_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');	
	}
	public function get_run_data($clid)
	{
		$curr_utc_datetime=gmdate('Y-m-d H:i:s');
		$time_diff=$GLOBALS['ID']['sess_time_zonediff'];
		$command="select  live_imeino as imei, to_char((live_datetime + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH12:MI:SS') as rec, extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - live_datetime::TIMESTAMP))/60 as diff, live_vehicle_regnumber as vh, live_vehicle_speedlimit as sp, live_latitude as lat, live_longitude as lng, concat(live_latitude,',',live_longitude) as latlng, live_digitalinputstatus as ig from live_gps_data inner join vts_device on device_imei=live_imeino and device_client_id=$clid order by live_vehicle_regnumber";
		//$command="select  chk_new_device_imei as imei, to_char((chk_new_device_last_datetime + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH12:MI:SS') as rec, extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - chk_new_device_last_datetime::TIMESTAMP))/60 as diff, live_vehicle_regnumber as vh, live_vehicle_speedlimit as sp, live_latitude as lat, live_longitude as lng, concat(live_latitude,',',live_longitude) as latlng, chk_new_device_ingition as ig, chk_new_device_power as pw, chk_new_device_valid as valid from vts_check_new_device inner join vts_device on device_imei=chk_new_device_imei and device_client_id=$clid inner join live_gps_data on device_imei=live_imeino order by live_vehicle_regnumber";
		//log_message('debug',$command);
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
}
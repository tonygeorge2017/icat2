<?php

class Fuel_report_model extends CI_Model {
	
	/*
	 * This function return array to controller to generate Table, Graph and Export data
	 */
	public $i = 0;
	
	public function get_fuel_report_graph_model( $vhicle_group, $vehicle, $start, $end ) {
		
		
		$CI =& get_instance();
		$CI->load->library('commonfunction');
		
		$result = array();
		$data = array();
		
		$distance_data = array();
		
		$config_temp = array();
		
		$sess_data = $this->session->userdata('login');
		$userid = $sess_data['sess_userid'];
		$client = $sess_data['sess_clientid'];
		
		$result["xaxis_title"] = "Date";
		$result["yaxis_title"] = "Liters (L)";
		$result["graph_width"] = "700";
		$result["graph_height"] = "400";
		
		$from_date = $start." 00:00:00";
		$to_date = $end." 23:59:59";
		
		$time_diff_to_check_fuel_gap = 45*60;	//Minutes* 1 second
		$idle_fuel_change_percentage = 10;
				
		/*
		 * Select statement for getting vehicle brand, model, mfd, fuel type, and voltage type
		 */
		
		$stmt = "SELECT 
					vb.fsv_model AS brand_name,
					A.vehicle_regnumber AS vehicle_name, 
					A.vehicle_model_id, 
					A.model_name, 
					A.vehicle_fuel_type_id, 
					A.vehicle_voltage_type_id,
					A.vehicle_yearofmanufacture,
					MAX(f.fs_value_liters) AS tank_capacity
					 FROM (
						SELECT 
							v.vehicle_id,
							v.vehicle_regnumber,
							v.vehicle_model_id, 
							v.vehicle_fuel_type_id, 
							v.vehicle_yearofmanufacture,
							v.vehicle_voltage_type_id,
							vbm.fsv_model AS model_name, 
							vbm.fsv_parent_id 
						FROM vts_vehicle v JOIN vts_fuel_sensor_vehicle vbm ON v.vehicle_model_id=vbm.fsv_id  
					) A JOIN vts_fuel_sensor_vehicle vb ON A.fsv_parent_id=vb.fsv_id LEFT JOIN vts_fuel_sensor_value f ON A.vehicle_model_id=f.fs_value_fsv_id WHERE A.vehicle_id='$vehicle' 
					GROUP BY 
					vb.fsv_model, 
					A.vehicle_regnumber,
					A.vehicle_model_id, 
					A.model_name, 
					A.vehicle_fuel_type_id, 
					A.vehicle_voltage_type_id,
					A.vehicle_yearofmanufacture ";
		
		$query = $this->db->query( $stmt );
		
		$title = "";
		$model = 0;
		$voltage_type = 0;
		$tank_capacity = 0;
		
		foreach( $query->result() AS $res ) {
			
			$voltage_type = $res->vehicle_voltage_type_id;
			$model = $res->vehicle_model_id;
			$tank_capacity = !empty( $res->tank_capacity ) ? $res->tank_capacity : 0;
			
			$title.= $res->vehicle_name." (".$res->brand_name. " - ". $res->model_name. ") Fuel sensor report";
			
		}
		
		$query->free_result();
		$result["tank_capacity"] = $tank_capacity;
		$result["title"] = $title;
		$result["sub_title"] = "Between ".$from_date." And ".$to_date;
		
		
		/*
		 * Select statement for getting volatge it's liters as a referance array
		 */
		
		$stmt = "SELECT ROUND(AVG ( fs_value_liters ), 2) AS fs_value_liters, fs_value_volts FROM vts_fuel_sensor_value WHERE fs_value_fsv_id='$model' GROUP BY fs_value_volts ORDER BY fs_value_liters ASC";
		
		$query = $this->db->query( $stmt );
		
		foreach( $query->result() AS $res ) {
			
			$config_temp[] = $res->fs_value_volts;
			
			$key = (string) $res->fs_value_volts;
			
			$fuel_config[ "'{$key}'" ] = (string)$res->fs_value_liters;
			
			log_message("DEBUG", "Voltage: ".$key." Fuel: ".$res->fs_value_liters );
			
		}
		
		//log_message("DEBUG", "Fuel config data: ".json_encode( $fuel_config ) );
				
		$query->free_result();
		
		$stmt = "SELECT get_user_datetime(gps_datetime,'$userid') AS datetime, gps_latitude,gps_longitude FROM server_gps_data WHERE gps_vehicle_id='$vehicle' AND get_user_datetime(gps_datetime,'$userid') BETWEEN '".$from_date."' AND '".$to_date."' AND gps_digitalinputstatus='Y' ORDER BY get_user_datetime(gps_datetime,'$userid') ASC ";
		
		$query = $this->db->query( $stmt );
		
		$distance_data = $query->result_array();
				
		/*
		 * Select statement for getting GPS datetime and voltage value based on voltage type of vehicle
		 */
		
		if( $voltage_type == 29 ) {
			
			$sensor_port = "12v"; 
			
			$sql = "SELECT 
						AVG(gps_adcbvalue::integer)::integer AS volt_data, 
						to_timestamp( floor( ( extract('epoch' from get_user_datetime(gps_datetime,'$userid') ) / 900 ) ) * 900) AT TIME ZONE 'UTC' AS datetime
					FROM 
						server_gps_data 
					WHERE 
						gps_vehicle_id='$vehicle'
					AND 
						get_user_datetime(gps_datetime,'$userid') BETWEEN '".$from_date."' AND '".$to_date."'
					AND 
						gps_digitalinputstatus='Y'  
					GROUP BY datetime 
					ORDER BY datetime ASC";
			
		} else if( $voltage_type == 28 ) {
			
			$sensor_port = "24v"; 
			
			$sql = "SELECT 
						AVG(gps_adcavalue::integer)::integer AS volt_data,
						to_timestamp( floor( ( extract('epoch' from get_user_datetime(gps_datetime,'$userid') ) / 900 ) ) * 900) AT TIME ZONE 'UTC' AS datetime
					FROM 
						server_gps_data 
					WHERE 
						gps_vehicle_id='$vehicle' 
					AND 
						get_user_datetime(gps_datetime,'$userid') BETWEEN '".$from_date."' AND '".$to_date."' 
					AND 
						gps_digitalinputstatus='Y'
					GROUP BY datetime 
					ORDER BY datetime ASC";
		
		}
		
		if( $voltage_type !=0 ) {
		
			$query = $this->db->query( $sql );
			
			$fuel_liters = 0;
			
			$temp_from_date = "";
			$temp_fuel = 0;
			$fuel_change = 0;
			
			foreach ( $query->result() AS $index => $res ) {
				
				$key = $CI->commonfunction->fuel_voltage_conversion( array("sensor_port"=> $sensor_port, "sesnor_input" => $res->volt_data ) );
				$key = round( $key ,2 );
				//$key = number_format( (float)$key, 2, '.', '');
				
				if( isset( $fuel_config[ "'{$key}'" ] ) ) {
				
					$fuel_liters = $fuel_config[ "'{$key}'" ];
					
				
				} else {
					
					$fuel_liters = $this->calc_missing_fuel_volume( $key, $config_temp, $fuel_config );
				}
				
				//log_message("DEBUG", "Voltage: ".$key." Fuel: ".$fuel_liters );
								
				if( $index == 0 ) {
					
					$temp_from_date = date("Y-m-d H:i:s", ( strtotime( $res->datetime ) - (15*60) ) );
					$temp_to_date = $res->datetime;
					$temp_fuel = $fuel_liters;
					
				} else {
					
					$temp_to_date = $res->datetime;
				}
				
				
				$vehicle_run_data = $this->get_vehicle_distance_report( $distance_data, $temp_from_date, $temp_to_date, $this->i );
				
				if( $fuel_liters > $temp_fuel )
					$fuel_change = $fuel_liters - $temp_fuel;
				else
					$fuel_change = $temp_fuel - $fuel_liters;
				
				if( $fuel_change !=0 )
					$mileage = round( ($vehicle_run_data/$fuel_change), 2);
				else 
					$mileage = 0;
				
				$data[] = array(
				
						"liters" => $fuel_liters,
						"time_value" => $res->datetime,
						"fuel_percentage" => number_format( ( ( $fuel_liters / $tank_capacity ) * 100 ), 2 ),
						"vehicle_run_data" => ( is_numeric( $vehicle_run_data ) ? round($vehicle_run_data,2) : 0 ),
						"mileage" => $mileage
				
				);

				$temp_from_date = $res->datetime;
				$temp_fuel = $fuel_liters;
			}
			
			$query->free_result();
			
			$result["data"] = $data;
			
		} else {
			
			$result["error"] = "Please configure vehicle fuel sensor voltage type";
			
		}
				
		$this->db->close();
		
		return $result;
	}
	
	/*
	 * This function to return array of all vehicle list to controller
	 */
	public function get_Vehicle_List_Model( $grpId ) {
		
		$result = array();
		
		$grp_id = !empty($grpId)?$grpId:0;
		
		$this->db->select("vehicle_id, vehicle_regnumber")
		->from("vts_vehicle")
		->where( "vehicle_group_id", $grpId );
		
		$query = $this->db->get();
		
		foreach( $query->result() AS $res ) {
			
			$result[] = array(
					
				"vcle_id" => $res->vehicle_id,
				"vcle_regnumber" => $res->vehicle_regnumber
			);
			
		}
		
		$query->free_result();		// Freeup memory
		
		$this->db->close();			// Close DB connection manually
		
		return $result;
	}
	
	/*
	 * This function is to calculte fuel volume which is not in fuel config table.
	 */
	private function calc_missing_fuel_volume( $volt, $arr_config, $arr_fuel_config ) {
		
		sort( $arr_config );
				
		$min_volt = 0;
		$max_volt = 0;
		$min_ltr = 0;
		$max_ltr = 0;
		
		$liters = 0;
		
		foreach ( $arr_config AS $key => $val ) {
		
			if ( $volt < $val ) {
				
				if( isset( $arr_config[$key-1] ) )
					$min_volt = (string)$arr_config[$key-1];
				$max_volt = (string)$val;
				break;
			}
		}
		
		if( isset( $arr_fuel_config[ "'{$max_volt}'" ] ) )
			$min_ltr = $arr_fuel_config[ "'{$max_volt}'" ];
		if( isset( $arr_fuel_config[ "'{$min_volt}'" ] ) )
			$max_ltr = $arr_fuel_config[ "'{$min_volt}'" ];
		
		if( $min_ltr > 0 || $max_ltr > 0 )
			$liters =  $min_ltr + ( ( $max_volt - $volt ) / ( $max_volt - $min_volt ) ) * ( $max_ltr - $min_ltr );
		$liters = round( $liters ,1 );
		
		return $liters;
		
		
	}

	private function get_vehicle_distance_report( $distance_array, $from_date, $to_date, $start ) {
	
		
		$new_arry = array();
		
		for( $j = $start; $j < sizeof( $distance_array ); $j++ ) {
			
			if( strtotime( $distance_array[$j]['datetime'] ) > strtotime( $to_date ) ) { 
				break; 
			} else if( ( strtotime( $distance_array[$j]['datetime'] ) >= strtotime( $from_date ) ) && ( $distance_array[$j]['datetime'] <= strtotime( $to_date ) ) ) {
			
				$new_arry[] = array( $distance_array[$j]['gps_latitude'], $distance_array[$j]['gps_longitude'] );
				$this->i = $j;
				
			}
			
		}
	
		$data = array();
	
		$lat_from = 0;
		$lat_to = 0;
		$lang_from = 0;
		$lang_to = 0;
	
		foreach( $new_arry AS $index => $res ) {
				
				
			if( sizeof( $new_arry )-1 > $index ) {
	
				$lat_from = isset( $new_arry[ $index ][0] ) ? $new_arry[ $index ][0]:0;
				$lat_to = isset( $new_arry[ $index+1 ][0] ) ? $new_arry[ $index+1 ][0]: 0;
	
				$lang_from = isset( $new_arry[ $index ][1] ) ? $new_arry[ $index ][1]:0;
				$lang_to = isset( $new_arry[ $index+1 ][1] ) ? $new_arry[ $index+1 ][1]:0;
	
				$data[] = $this->calc_distance( $lat_from, $lang_from, $lat_to, $lang_to, "K");
	
			}
				
		}
	
		$distance = array_sum( $data );
	
		return $distance;
	}
	
	public function fuel_report_graph_model( $vhicle_group, $vehicle, $start, $end ) {
	
	
		$CI =& get_instance();
		$CI->load->library('commonfunction');
	
		$result = array();
		$data = array();
	
		
	
		$config_temp = array();
	
		$sess_data = $this->session->userdata('login');
		$userid = $sess_data['sess_userid'];
		$client = $sess_data['sess_clientid'];
	
		$result["xaxis_title"] = "Date";
		$result["yaxis_title"] = "Liters (L)";
		$result["graph_width"] = "700";
		$result["graph_height"] = "400";
	
		$from_date = $start." 00:00:00";
		$to_date = $end." 23:59:59";
	
		$time_diff_to_check_fuel_gap = 45*60;	//Minutes* 1 second
		$idle_fuel_change_percentage = 10;
		
	
		/*
		 * Select statement for getting vehicle brand, model, mfd, fuel type, and voltage type
		 */
	
		$stmt = "SELECT
					vb.fsv_model AS brand_name,
					A.vehicle_regnumber AS vehicle_name,
					A.vehicle_model_id,
					A.model_name,
					A.vehicle_fuel_type_id,
					A.vehicle_voltage_type_id,
					A.vehicle_yearofmanufacture,
					MAX(f.fs_value_liters) AS tank_capacity
				FROM (
					SELECT
						v.vehicle_id,
						v.vehicle_regnumber,
						v.vehicle_model_id,
						v.vehicle_fuel_type_id,
						v.vehicle_yearofmanufacture,
						v.vehicle_voltage_type_id,
						vbm.fsv_model AS model_name,
						vbm.fsv_parent_id
					FROM vts_vehicle v JOIN vts_fuel_sensor_vehicle vbm ON v.vehicle_model_id=vbm.fsv_id
				) A JOIN vts_fuel_sensor_vehicle vb ON A.fsv_parent_id=vb.fsv_id LEFT JOIN vts_fuel_sensor_value f ON A.vehicle_model_id=f.fs_value_fsv_id WHERE A.vehicle_id='$vehicle'
				GROUP BY
					vb.fsv_model,
					A.vehicle_regnumber,
					A.vehicle_model_id,
					A.model_name,
					A.vehicle_fuel_type_id,
					A.vehicle_voltage_type_id,
					A.vehicle_yearofmanufacture ";
	
		$query = $this->db->query( $stmt );
	
		$title = "";
		$model = 0;
		$voltage_type = 0;
		$tank_capacity = 0;
	
		foreach( $query->result() AS $res ) {
			
			$voltage_type = $res->vehicle_voltage_type_id;
			$model = $res->vehicle_model_id;
			$tank_capacity = !empty( $res->tank_capacity ) ? $res->tank_capacity : 0;
				
			$title.= $res->vehicle_name." (".$res->brand_name. " - ". $res->model_name. ") Fuel sensor report";
				
		}
	
		$query->free_result();
		$result["tank_capacity"] = $tank_capacity;
		$result["title"] = $title;
		$result["sub_title"] = "Between ".$from_date." And ".$to_date;
		
		//if( $tank_capacity > 50 ) 
		//	$idle_fuel_fill = 10; // 10Liters
		//else
			$idle_fuel_fill = 5; // 5Liters
		/*
		* Select statement for getting volatge it's liters as a referance array
		*/

		$stmt = "SELECT ROUND(AVG ( fs_value_liters ), 2) AS fs_value_liters, fs_value_volts FROM vts_fuel_sensor_value WHERE fs_value_fsv_id='$model' GROUP BY fs_value_volts";

		$query = $this->db->query( $stmt );

		foreach( $query->result() AS $res ) {
			
			$config_temp[] = $res->fs_value_volts;
				
			$key = (string) $res->fs_value_volts;
				
			$fuel_config[ "'{$key}'" ] = (string)$res->fs_value_liters;
				
		}
	
		$query->free_result();
	
			
		 /*
		 * Select statement for getting GPS datetime and voltage value based on voltage type of vehicle
		 */
	
		 if( $voltage_type == 29 ) {
		 	
			$sensor_port = "12v";
				
			$sql = "SELECT
						AVG(gps_adcbvalue::integer)::integer AS volt_data,
						to_timestamp( floor( ( extract('epoch' from get_user_datetime(gps_datetime,'$userid') ) / 900 ) ) * 900) AT TIME ZONE 'UTC' AS datetime
					FROM
						server_gps_data
					WHERE
						gps_vehicle_id='$vehicle'
					AND
						get_user_datetime(gps_datetime,'$userid') BETWEEN '".$from_date."' AND '".$to_date."'
					AND
						gps_digitalinputstatus='Y'
					GROUP BY datetime
					ORDER BY datetime ASC";
			
			
				
		} else if( $voltage_type == 28 ) {
			
			$sensor_port = "24v";
			
			$sql = "SELECT
						AVG(gps_adcavalue::integer)::integer AS volt_data,
						to_timestamp( floor( ( extract('epoch' from get_user_datetime(gps_datetime,'$userid') ) / 900 ) ) * 900) AT TIME ZONE 'UTC' AS datetime
					FROM
						server_gps_data
					WHERE
						gps_vehicle_id='$vehicle'
					AND
						get_user_datetime(gps_datetime,'$userid') BETWEEN '".$from_date."' AND '".$to_date."'
					AND
						gps_digitalinputstatus='Y'
					GROUP BY datetime
					ORDER BY datetime ASC";

		}
		
		log_message("DEBUG","This is SQL".$sql);
	
		if( $voltage_type !=0 ) {
	
			$query = $this->db->query( $sql );
						
			$fuel_liters = 0;
		
			$temp_from_date = "";
			$temp_fuel = 0;
			$fuel_change = 0;
			
			$new_fuel = 0;
		
			foreach ( $query->result() AS $index => $res ) {
	
				$key = $CI->commonfunction->fuel_voltage_conversion( array("sensor_port"=> $sensor_port, "sesnor_input" => $res->volt_data ) );
				$key = round( $key ,2 );
																			
				if( isset( $fuel_config[ "'{$key}'" ] ) )
					$fuel_liters = $fuel_config[ "'{$key}'" ];
				else
					$fuel_liters = $this->calc_missing_fuel_volume( $key, $config_temp, $fuel_config );
	
				if( $index == 0 ) {
					
					$temp_from_date = date("Y-m-d H:i:s", ( strtotime( $res->datetime ) - (15*60) ) );
					$temp_to_date = $res->datetime;
					$temp_fuel = $fuel_liters;
						
				} else {
					
					$temp_to_date = $res->datetime;
				}
	
				/*
				* When there is a substantial difference between the last received data
				* (quantitative definition of 'substantial' can be defined to be, say, 5%+
				* (or 10%+ or some other more appropriate value that you and Tony can pick))
				* separted by a substantial time gap (again, can be defined to be anything more than, say, 45 minutes),
				* then instead of smoothening the plot by connecting the two points directly,
				* can we look at making an assumption that the fuel was put in ~45 minutes in the past
				*/
	
				/*
				* Checking whether Iginition is OFF for more than '$time_diff_to_check_fuel_gap' minutes or not
				*/
				if( ( strtotime( $temp_to_date ) - strtotime( $temp_from_date ) ) >= $time_diff_to_check_fuel_gap ) {
		
						$fuel_change = $fuel_liters - $temp_fuel;
		
						if( $fuel_change > 0 )		//To calculate the percentage increase
							$fuel_change = ( $fuel_change / $fuel_liters ) * 100;
	 					else if( $fuel_change < 0 ) //To calculate the percentage decrease
							$fuel_change = ( $fuel_change / $temp_fuel ) * 100;
		
						/*
						* Check increased/decreased fuel change is greater than or eaqual to '$idle_fuel_change_percentage'
						* Add the last received volume before ignition off to next immediate ignition on
						*/
		
						if( $fuel_change >= $idle_fuel_change_percentage )
							array_push( $data,array("liters" => $temp_fuel,"time_value" => $temp_to_date ) );
					
				}
				
				if( ( $new_fuel - $fuel_liters ) < 0 ) {
					
					if( -( ( $new_fuel - $fuel_liters ) ) >= $idle_fuel_fill ) {
						$data[] = array( "liters" => $fuel_liters, "time_value" => $res->datetime );
						$new_fuel = $fuel_liters;
					}
					
				} else if( ( $new_fuel - $fuel_liters ) >= 0 ) {
					
					$data[] = array( "liters" => $fuel_liters, "time_value" => $res->datetime );
					$new_fuel = $fuel_liters;
				}
	
					
				$temp_from_date = $res->datetime;
				$temp_fuel = $fuel_liters;
			}
		
			$query->free_result();
		
			$result["data"] = $data;
		
		} else {
		
			$result["error"] = "Please configure vehicle fuel sensor voltage type";
		
		}
	
		$this->db->close();
	
		return $result;
	}
	
	
	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::                                                                         :*/
	/*::  This routine calculates the distance between two points (given the     :*/
	/*::  latitude/longitude of those points). It is being used to calculate     :*/
	/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
	/*::                                                                         :*/
	/*::  Definitions:                                                           :*/
	/*::    South latitudes are negative, east longitudes are positive           :*/
	/*::                                                                         :*/
	/*::  Passed to function:                                                    :*/
	/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
	/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
	/*::    unit = the unit you desire for results                               :*/
	/*::           where: 'M' is statute miles (default)                         :*/
	/*::                  'K' is kilometers                                      :*/
	/*::                  'N' is nautical miles                                  :*/
	/*::  Worldwide cities and other features databases with latitude longitude  :*/
	/*::  are available at http://www.geodatasource.com                          :*/
	/*::                                                                         :*/
	/*::  For enquiries, please contact sales@geodatasource.com                  :*/
	/*::                                                                         :*/
	/*::  Official Web site: http://www.geodatasource.com                        :*/
	/*::                                                                         :*/
	/*::         GeoDataSource.com (C) All Rights Reserved 2015		   		     :*/
	/*::                                                                         :*/
	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private function calc_distance($lat1, $lon1, $lat2, $lon2, $unit) {
	
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = is_nan($dist) ? 0 : $dist;
		
		$dist = rad2deg($dist);
			
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		
		return ($miles * 1.609344);		//Return distance in Kilometers
	}
	
}
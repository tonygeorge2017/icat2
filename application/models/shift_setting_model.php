<?php
Class Shift_setting_model extends CI_Model{
	
	public function __construct()
	{		
		$this->load->database();
		$GLOBALS['ID'] = $this->session->userdata('login');		
	}	
	public function get_shiftCategorylist($cl_id=null)
	{
		if($cl_id)
		{
			$command="select shift_category_id,shift_category_name,no_of_shift,working_hrs,description,isactive,client_id, shift_name,shift_start_time,shift_end_time from vts_shift_category inner join vts_shift_detail on shift_category=shift_category_id where md5(client_id::varchar)='$cl_id' order by shift_category_id";
			//log_message('debug',$command);
			$query = $this->db->query($command);
			return $query->result_array();
		}else{return array();}
	}
	public function get_allClient()
	{
		$this->db->order_by('client_name','ASC');
		$this->db->select('client_id, client_name');
		$this->db->where('client_id !=', AUTOGRADE_USER);
		$query = $this->db->get('vts_client');
		$result=$query->result_array();
		//log_message('debug',print_r($result,true));
		return $result;
	}
	public function insert_data($data,$arrName,$arrHr,$arrMnt,$arrEnd)
	{		
		if($this->db->insert('vts_shift_category', $data)) {
			$id=$this->db->insert_id();			
			foreach($arrName as $key=>$value){
				$this->db->insert('vts_shift_detail',array("shift_name"=>$value,"shift_start_time"=>$arrHr[$key].':'.$arrMnt[$key].':00',"shift_end_time"=>$arrEnd[$key],"shift_category"=>$id));
			}
			return $id;
		}else
			return false; 
	}
	public function delete_shift_detail($id)
	{
		$this->db->where('md5(shift_category::varchar)',$id);
		$this->db->delete('vts_shift_detail');
	}
	public function delete_shift_category($id)
	{
		$this->db->where('md5(shift_category_id::varchar)',$id);
		$this->db->delete('vts_shift_category');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contactus_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
	}
	
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 * return type - row array
	 */
	public function get_userDetails()
	{
		try
		{
			if($GLOBALS['ID']['sess_user_name']!=null)
			{
				$query=$query=$this->db->query("select user_id, user_client_id, client_name, user_user_name, user_email, user_mobile from vts_users, vts_client where client_id=user_client_id and user_email='".$GLOBALS['ID']['sess_user_name']."'");
				$result = $query->row_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else 
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - user',$ex->getMessage());
			return null;
		}
	}
}
<?php

class Driver_group_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	
	/*
	 * This function is used to get the drivergroup from the
	 * vts_driver_group table to fill the parent group dropDownBox in drivergroup view.
	 */
	public function get_all_parent_driver_group($session_login_client_id){
		try{
			if($session_login_client_id!=AUTOGRADE_USER)
			{
			$this->db->order_by('driver_group','ASC');
			$this->db->select('driver_group_id, driver_group');
			$this->db->where('driver_group_isactive', ACTIVE);
			if($session_login_client_id!=AUTOGRADE_USER)
			{
				$this->db->where('md5(driver_group_client_id::varchar)', $session_login_client_id);
			}
			$query = $this->db->get('vts_driver_group');
			return $query->result_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all driver group name - vts_driver_group');
			return null;
		}
	}
	
	/*
	 * This function used to return the driver group detail relate the give parameter(i.e. driver_group_id)
	 * if any error, then return null.
	 */
	public function get_vts_driver_group($rowPerPage=null,$startNo=null,$driver_group_id=null,$session_login_client_id=null)
	{
		try
		{
			if($rowPerPage==null & $driver_group_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$this->db->order_by('driver_group','ASC');
				$this->db->select('vts_driver_group.driver_group_id as driver_group_id, vts_driver_group.driver_group as driver_group, vts_driver_group.driver_parent_group_id as driver_parent_group_id, get_driver_group_name(driver_parent_group_id) as driver_group_parent, driver_group_client_id');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(driver_group_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_driver_group');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($driver_group_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('driver_group','ASC');
				$this->db->select('vts_driver_group.driver_group_id as driver_group_id, vts_driver_group.driver_group as driver_group, vts_driver_group.driver_parent_group_id as driver_parent_group_id, get_driver_group_name(driver_parent_group_id) as driver_group_parent, driver_group_client_id');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(driver_group_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_driver_group');
				$query = $this->db->get();
				return $query->result_array();
			}
			else if($session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$query=$this->db->get_where('vts_driver_group',array('md5(driver_group_id::varchar)'=>$driver_group_id));
				return $query->row_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_driver_group',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To delete the user role from the vts_driver_group table
	 */
	
	public function InsertOrUpdateOrdelete_vts_driver_group($driver_group_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_driver_group', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('driver_group_id', $driver_group_id);
				$this->db->update('vts_driver_group', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('md5(driver_group_id::varchar)', $driver_group_id);
				if($this->db->delete('vts_driver_group'))
				{
					return true;
				}
				else
				{
					return FALSE;
				}
					
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_driver_group',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in driver group screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}
	
}
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trip_register_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 */
	public function get_tripList($sessClientID, $clientID=null,$rowPerPage=null,$startNo=null,$sessUserId=null)
	{
		try
		{		
			$command="select trip_id, trip_register_vehicle_id, get_vehicle_name(trip_register_vehicle_id) as vehicle_name, trip_register_driver_id, get_driver_name(trip_register_driver_id) as driver_name, trip_register_time_start,
						trip_register_time_end,trip_register_remarks,trip_register_client_id, trip_register_time_start::date as o_from, trip_register_time_end::date as o_end, shift_name from vts_trip_register
						 left outer join vts_shift_detail on shift_id=trip_register_shift 
						 where md5(trip_register_client_id::varchar)='".$clientID."'order by trip_register_vehicle_id";
			
			if($sessClientID!=md5(AUTOGRADE_USER))
			{				
				if($rowPerPage!=null)
					$command .= " limit ".$rowPerPage." offset ". $startNo;
				$command .= ";";
				
				//log_message('debug','**Trip Reg Screen[get_tripList]**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
								
		}
		catch (Exception $ex)
		{
			log_message('error', 'get all detail - user',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients(){
		try
		{	
			/*$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER." order by client_name";	
			$query = $this->db->query($command);
			$result=$query->result_array();*/
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	
	/*
	 * This function is used to get all the vehicle group for select clientId from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allVehicleGroup($clientID=-1)
	{
		try
		{		
			if($GLOBALS['sessClientID']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type']==DEALER_USER)
				$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and md5(vehicle_group_client_id::varchar)='".$clientID."' order by vehicle_group;";
			else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
				$command="select vehicle_group_id, vehicle_group from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and md5(vehicle_group_client_id::varchar)='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
			else
				$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
			//log_message('debug','**Trip Reg Screen[get_allVehicleGroup]**'.$command);
			$query = $this->db->query($command);			
			$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}

	public function get_allDriver($tripID=-1, $driverGpID, $startDate=null, $endDate=null)
	{
		if($driverGpID)
		{
			$result=array();
			$command="Select d.driver_id, d.driver_name, t.trip_register_time_start,t.trip_register_time_end, t.trip_register_time_start::date as o_from, t.trip_register_time_end::date as o_end, t.trip_register_shift from vts_driver d left outer join vts_trip_register t on d.driver_id=t.trip_register_driver_id where driver_isactive='1' and d.driver_drivergroup_id=$driverGpID order by d.driver_name,d.driver_id ";
			//log_message('debug',$command);
			$query_result=$this->db->query($command);
			$result=$query_result->result_array();
			if(count($result)>0)
				return $result;
			else
				return array();
		}
		else
			return array();
	}

	public function get_VehicleList($tripID=-1, $vehicleGpID=null, $startDate=null, $endDate=null)
	{
		//log_message('debug',"$tripID, $vehicleGpID, $startDate, $endDate");
		if($vehicleGpID)
		{
			$result=array();
			$command="Select v.vehicle_id, v.vehicle_regnumber, t.trip_register_time_start,t.trip_register_time_end, t.trip_register_time_start::date as o_from, t.trip_register_time_end::date as o_end, t.trip_register_shift from vts_vehicle v left outer join vts_trip_register t on v.vehicle_id=t.trip_register_vehicle_id where vehicle_isactive='1' and v.vehicle_group_id=$vehicleGpID order by v.vehicle_regnumber,v.vehicle_id;";
			//log_message('debug',$command);
			$query_result=$this->db->query($command);
			$result=$query_result->result_array();
			if(count($result)>0)
				return $result;
			else
				return array();
		}
		else
			return array();
	}

	/*
	 * This function is used to get all the device tyepe from the device type table 
	 * to fill the drop down in user view.
	 */
	public function get_allDriverGroup($clientID=-1)
	{
		try
		{	

			if($GLOBALS['sessClientID']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
				$command="select driver_group_id, driver_group from vts_driver_group where driver_group_isactive='".ACTIVE."' and md5(driver_group_client_id::varchar)='".$clientID."' order by driver_group;";
			else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
				$command="select driver_group_id, driver_group from vts_driver_group, vts_user_driver_group_link
						where dr_gp_link_user_id='".$GLOBALS['sessUserID']."' and md5(driver_group_client_id::varchar)='".$clientID."' and
						driver_group_isactive='".ACTIVE."' and driver_group_id=dr_gp_link_driver_group_id order by driver_group;";
			else
				$command="select driver_group_id, driver_group from vts_driver_group where driver_group_isactive='".ACTIVE."' and driver_group_client_id='".$GLOBALS['sessClientID']."' order by driver_group;";
			//log_message('debug','**Trip Reg Screen[get_allDriverGroup]**'.$command);		
			$query = $this->db->query($command);			
			$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all device type');
			return null;
		}
	}
	
	/*
	 * To edit or insert the device installation table
	 */
	public function InsertOrUpdate_trip($tripID,$data,$operation)
	{
		try
		{				
			if($operation=="insert")
			{				
				if($this->db->insert('vts_trip_register', $data))
					return true;
				else
					return false;
			}
			else if($operation=="edit" && $tripID!=null)
			{
				$this->db->where('md5(trip_id::varchar)', $tripID);				
				if($this->db->update('vts_trip_register', $data))
					return true;
				else
					return false;
			}
			else if($operation=="delete" && $tripID!=null)
			{
				$this->db->where('md5(trip_id::varchar)', $tripID);				
				if($this->db->delete('vts_trip_register'))
					return true;
				else
					return false;
			}
		}catch (Exception $ex)
		{
			log_message('error','insert or update  - vts_trip_register'.$ex->getMessage());
			return false;
		}
	}
	/*
	 * To edit server_gps_data table
	 */
	public function InsertOrUpdate_gpsData($vhID, $data, $startDate, $endDate=null, $operation)
	{
		try
		{
			if($operation=="edit" && $vhID!=null)
			{
				$command="SELECT d.device_imei FROM vts_device_installation di, vts_device d where di.dev_install_removed_date is null 
						and d.device_id=di.dev_install_device_id and di.dev_install_vehicle_id='".$vhID."' limit 1;";
				$query = $this->db->query($command);
				$result=$query->row_array();
				if ($result != null)
					$imeiNo= $result['device_imei'];
				else
					$imeiNo= null;
				if($imeiNo!=null)
				{
					$this->db->where('gps_imeino', $imeiNo);
					$this->db->where('gps_datetime >=', $startDate);
					if($endDate!=null)
						$this->db->where('gps_datetime <=', $endDate);
					$this->db->where('(gps_driver_id is null or gps_vehicle_id is null)');				
					if($this->db->update('server_gps_data', $data))
						return true;
					else
						return false;
				}
			}
		}
		catch (Exception $ex)
		{
			log_message('error','update  - InsertOrUpdate_gpsData'.$ex->getMessage());
			return false;
		}
	}
	/*
	 * This function is used to get the selected client time difference
	 */
	public function get_time_diff($clientId)
	{
		$command="Select time_zone_diff from vts_client, vts_time_zone where time_zone_id=client_time_zone_id and md5(client_id::varchar)='".$clientId."'";
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row['time_zone_diff'];
		else
			return null;
	}
	/*
	 * This function is used to get the details about the trip details.
	 */
	public function get_trip_details($tripId)
	{
		$command="select get_driver_gp_id(trip_register_driver_id) as driver_gp_id, get_vehicle_gp_id(trip_register_vehicle_id) as vehicle_gp_id, trip_id, trip_register_vehicle_id, get_vehicle_name(trip_register_vehicle_id) as vehicle_name,trip_register_driver_id, get_driver_name(trip_register_driver_id) as driver_name, trip_register_time_start, trip_register_time_end, trip_register_remarks, trip_register_client_id, get_client_name(trip_register_client_id) as client_name, shift_category, trip_register_shift from vts_trip_register left outer join vts_shift_detail on shift_id=trip_register_shift where md5(trip_id::varchar)='".$tripId."';";
		//log_message('debug','**trip reg(get_trip_details)**'.$command);
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row;
		else
			return null;
	}	

	public function get_shift_category($id)	
	{
		if($id)
		{
			$result=array();
			$command="select shift_category_id, shift_category_name, no_of_shift, working_hrs from vts_shift_category where md5(client_id::varchar)='$id' and isactive='1' order by shift_category_name;";
			$query=$this->db->query($command);
			log_message('debug',$command);
			$result=$query->result_array();
			if($result)
				return $result;
			else
				return array();
		}else
				return array();
	}

	public function get_shift($id)	
	{
		if($id)
		{
			$result=array();
			$command="select shift_id, shift_name, shift_start_time, shift_end_time from vts_shift_detail where shift_category=$id order by shift_name;";
			$query=$this->db->query($command);
			log_message('debug',$command);
			$result=$query->result_array();
			if($result)
				return $result;
			else
				return array();
		}else
				return array();
	}
}
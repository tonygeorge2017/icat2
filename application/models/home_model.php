<?php

class Home_model extends CI_Model{	

	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');		
	}

	public function get_run_data($clid)
	{
		$result=array();
		$curr_utc_datetime = gmdate('Y-m-d H:i:s');	
		$time_diff=$GLOBALS['ID']['sess_time_zonediff'];
		$command="select  vehicle_group vhgp, live_vehicle_id vhid, live_angleorientationcog ang, live_imeino as imei, to_char((live_datetime + interval '1 hour' * $time_diff), 
		'DD-MM-YYYY  HH24:MI:SS') as rec, extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - live_datetime::TIMESTAMP))/60 as diff, 
		live_vehicle_regnumber as vh, live_speed as sp, live_vehicle_speedlimit, live_latitude as lat, live_longitude as lng, 
		concat(live_latitude,',',live_longitude) as latlng, live_digitalinputstatus as ig, vehicle_idealspeed haltspeed, client_dashboard_lost lost from live_gps_data 
		inner join vts_device on device_imei=live_imeino and device_client_id=$clid 
		inner join vts_vehicle_group on vehicle_group_id=live_vehicle_group_id and vehicle_group_isactive='1'
		inner join vts_vehicle on vehicle_id=live_vehicle_id and vehicle_isactive='1'
		inner join vts_client on client_id=$clid";
		
		if($GLOBALS['ID']['sess_client_admin']=='0')
			$command.=" inner join vts_user_vehicle_group_link on vh_gp_link_vehicle_group_id=live_vehicle_group_id and vh_gp_link_user_id='".$GLOBALS['ID']['sess_userid']."'";
		
		$command.=" order by live_vehicle_regnumber";
		
		/*
		$command="select  chk_new_device_imei as imei, to_char((chk_new_device_last_datetime + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH12:MI:SS') as rec, extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - chk_new_device_last_datetime::TIMESTAMP))/60 as diff, live_vehicle_regnumber as vh, live_vehicle_speedlimit as sp, live_latitude as lat, live_longitude as lng, concat(live_latitude,',',live_longitude) as latlng, chk_new_device_ingition as ig, chk_new_device_power as pw, chk_new_device_valid as valid from vts_check_new_device inner join vts_device on device_imei=chk_new_device_imei and device_client_id=$clid inner join live_gps_data on device_imei=live_imeino order by live_vehicle_regnumber";
		*/
		//log_message('debug',$command);
		$query=$this->db->query($command);
		if($query)
			$result = $query->result_array();				
		
		return $result;
	}

	public function get_insurance_alert($clid, $diff)
	{
		$result=array();
		$curr_utc_datetime = gmdate('Y-m-d H:i:s');	
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		$command = "Select vehicle_id id, md5(vehicle_client_id::varchar) clid, md5(vehicle_id::varchar) vhid, md5('edit') edit, vehicle_model_id mod_id, vehicle_regnumber vhno, insurance_agency insag, vehicle_insurancepolicynumber plcno, vehicle_insurancepolicytype plctyp, vehicle_insuranceexpirydate exdate, vehicle_insuranceterms instrm, (vehicle_insuranceexpirydate::date - '".$curr_utc_datetime ."'::date) as diff from vts_vehicle inner join vts_insurance_agency on insurance_agency_id=vehicle_insuranceagency_id where vehicle_client_id=$clid and (vehicle_insuranceexpirydate::date - now()::date) < $diff order by diff";
		$query = $this->db->query($command);
		if($query)
			$result = $query->result_array();
		//log_message('debug', print_r($result,true));
		return $result;
	}

	public function get_shift_alert($clid, $diff)
	{
		$result=array();
		$curr_utc_datetime = gmdate('Y-m-d H:i:s');	
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		$command = "select trip_register_driver_id id, driver_name dr, vehicle_regnumber vh, shift_name sht, trip_register_time_start sdt, trip_register_time_end edt, (trip_register_time_end::date - now()::date) as diff from 
			vts_trip_register inner join vts_driver on driver_id=trip_register_driver_id and driver_isactive='1' 
			inner join vts_driver_group on driver_group_id=driver_drivergroup_id and driver_group_isactive='1'
			inner join vts_vehicle v on v.vehicle_id =trip_register_vehicle_id and v.vehicle_isactive='1' 
			inner join vts_vehicle_group vg on vg.vehicle_group_id=v.vehicle_group_id and vg.vehicle_group_isactive='1'
			left outer join vts_shift_detail on shift_id=trip_register_shift
			where 
			trip_register_client_id=$clid and (trip_register_time_end::date - now()::date) < $diff
			and trip_register_driver_id not in (select ck.trip_register_driver_id from vts_trip_register ck where (ck.trip_register_time_end::date - now()::date) >= $diff)
			order by 
			driver_name";
		$query = $this->db->query($command);
		if($query)
			$result = $query->result_array();
		//log_message('debug', print_r($result,true));
		return $result;

	}

	public function get_digitalIO_alert($clid)
	{
		$result=array();
		$curr_utc_datetime = gmdate('Y-m-d H:i:s');	
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		$command = "Select vehicle_regnumber vh, device_imei imei, chk_new_device_last_datetime dt, 
		substring(chk_new_device_digital_inputs,7,1) pw, substring(chk_new_device_digital_inputs,6,1) tp 
		from vts_check_new_device 
		inner join vts_device on device_imei = chk_new_device_imei 
		inner join vts_device_installation on dev_install_device_id = device_id and dev_install_removed_date is null 
		inner join vts_vehicle v on v.vehicle_id = dev_install_vehicle_id and v.vehicle_isactive = '1' and dev_install_removed_date is null 
		inner join vts_vehicle_group vg on vg.vehicle_group_id = v.vehicle_group_id and vehicle_group_isactive = '1' 
		where device_client_id = $clid /*and chk_new_device_last_datetime::date >=now()::date */ and chk_new_device_digital_inputs is not null and (substring(chk_new_device_digital_inputs,7,1) = '0' or substring(chk_new_device_digital_inputs,6,1) = '1') order by chk_new_device_last_datetime desc";
		$query = $this->db->query($command);
		if($query)
			$result = $query->result_array();
		//log_message('debug', print_r($result,true));
		return $result;

	}

}
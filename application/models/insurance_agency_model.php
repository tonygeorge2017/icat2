<?php

class Insurance_agency_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	
	
	
	/*
	 * This function used to return the insurance agency detail relate the give parameter(i.e. insurance_agency_id)
	 * if any error, then return null.
	 */
	public function get_vts_insurance_agency($rowPerPage=null,$startNo=null,$insurance_agency_id=null,$session_login_client_id=null)
	{
		try
		{
			if($rowPerPage==null & ($insurance_agency_id==null & $session_login_client_id!=md5(AUTOGRADE_USER)))
			{
// 				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('insurance_agency','ASC');
				$this->db->select('insurance_agency_id, insurance_agency, insurance_agency_contactperson, insurance_agency_contactphone, insurance_agency_client_id');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(insurance_agency_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_insurance_agency');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($insurance_agency_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$this->db->limit($rowPerPage, $startNo);
				
				$this->db->order_by('insurance_agency','ASC');
				$this->db->select('insurance_agency_id, insurance_agency, insurance_agency_contactperson, insurance_agency_contactphone, insurance_agency_client_id');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(insurance_agency_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_insurance_agency');
				$query = $this->db->get();
				return $query->result_array();
							
			}
			else// if($session_login_client_id!=AUTOGRADE_USER)
			{
				$query=$this->db->get_where('vts_insurance_agency',array('md5(insurance_agency_id::varchar)'=>$insurance_agency_id));
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_insurance_agency',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the insurance agency details from the vts_insurance_agency table
	 */
	
	public function InsertOrUpdateOrdelete_vts_insurance_agency($insurance_agency_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_insurance_agency', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('insurance_agency_id', $insurance_agency_id);
				$this->db->update('vts_insurance_agency', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('md5(insurance_agency_id::varchar)', $insurance_agency_id);
				if($this->db->delete('vts_insurance_agency'))
					return true;
				else
					return FALSE;
			}
			
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_insurance_agency',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in insurance agency screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}
	
}
?>
<?php
class Common_model extends CI_Model {
	
	/**
	 * Function to communicate with DB
	 * returns the result of setting
	 *
	 * $s_keyName : Key Name to fetch the data
	 * from DB
	 *
	 * returns string
	 */
	public function get_setting_value($s_keyName) {
		
		// $query = $this->db->get('select KeyValue from Setting where upper(KeyName) = ?',upper($s_keyName));
		// return $query->result();
		$query = $this->db->query ( "select setting_key_value from vts_setting where upper(setting_key_name) = '" . strtoupper ( $s_keyName ) . "'" );
		$row = $query->row_array ();
		return $row ['setting_key_value'];
	}
	
	/**
	 * Function to update the data to
	 * setting table
	 *
	 * $s_keyName : Key Name for setting table
	 * $s_keyValue : Key Value for setting table
	 */
	public function update_setting_value($s_keyName, $s_keyValue) {
		try {
			
			$this->db->where ( 'setting_key_name', $s_keyName );
			$this->db->get ( 'vts_setting' );
			$this->KeyValue = $s_keyValue;
			$this->db->update ( 'vts_setting', $this );
		} catch ( Exception $e ) {
			
			log_message ( 'error, update to table - Setting', $e->getMessage () );
			return;
		}
	}
	
	/**
	 * Function to insert the data to
	 * EventLog table
	 *
	 * $userID : ID of Logged-in user
	 * $userIP : User's IP
	 * $screenID : Screen ID
	 * $eventDescription : Description about the event
	 *
	 * inserts the value to DB.
	 */
	public function insert_event_value($s_userID, $s_userIP, $s_screenID, $s_eventDesc) {
		try {
			// Data to be inserted in the DB.
			$data = array (
					'event_log_user_id' => $s_userID,
					'event_log_time' => date ( "Y/m/d H:i:s" ),
					'event_log_user_ip' => $s_userIP,
					'event_log_screen_id' => $s_screenID,
					'event_log_description' => $s_eventDesc 
			);
			// Inserts the data to table EventLog
			$this->db->insert ( 'vts_event_log', $data );
		} catch ( Exception $e ) {
			
			log_message ( 'error, inserting to table - EventLog', $e->getMessage () );
			return;
		}
	}
	public function get_location() {
		// $data['userid']= $session_data['userid'];
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
			$query = $this->db->query ( "select client_time_diff,client_time_zone from vts_client  c, vts_users u where u.user_client_id = c.client_id and u.user_id=" . $GLOBALS ['ID'] ['sess_userid'] );
			return $query->row_array ();
		}
	}
	/* get values for menu */
	public function get_menu() {
		try {
			
			$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
			$logged_in = $GLOBALS ['ID'] ['sess_userid'];
			
			$count = 0;
			$count = $this->get_count ();
			if ($count == null)
				$count = 0;
			$this->db->select ( "get_vts_user_screens(" . $logged_in . ")" );
			$this->db->limit ( $count, 0 );
			$query = $this->db->get ();
			
			$arr = array ();
			if ($query != null) {
				$result = $query->result ();
				// log_message('error','==Count:'.count($result));
				foreach ( $query->result () as $row ) {
					
					array_push ( $arr, $row->get_vts_user_screens );
					// log_message('error','^^cm^^--'.$row->get_vts_user_screens);
				}
			} // echo print_r($arr,true);
			
			return $arr;
		} catch ( Exception $e ) {
			log_message ( 'error', 'get menus data from modeules and screen table', $e->getMessage () );
			return null;
		}
	}
	public function get_count() {
		$command = "select count(screen_name) no_screen from vts_screen";
		$query = $this->db->query ( $command );
		$row = $query->row_array ();
		if ($row != null)
			return $row ['no_screen'];
		else
			return null;
	}
		/*
	 * menu test
	 */
	public function menu_display() {
		// getting location value
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$clnt=$GLOBALS ['ID'] ['sess_clientid'];
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['timezone'] = ($GLOBALS ['ID'] ['sess_time_zonename']) ? $GLOBALS ['ID'] ['sess_time_zonename'] : ''; // ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
		}
		if($GLOBALS ['ID'] ['sess_user_type'] == '6')
		{
			$GLOBALS ['main_menu']='<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<li><a href="'.base_url("index.php/parent_tracking_ctrl").'">Vehicle Tracking</a></li>
			</ul>';
		}
		else if($GLOBALS ['ID'] ['sess_is_admin']!='0')
		{
			$GLOBALS ['main_menu']='<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
			$GLOBALS ['main_menu'].='<li class="dropdown-submenu">';
			$GLOBALS ['main_menu'].='<a class="not_close_onclick_menu" tabindex="-1" href="#">Master</a>';
			$GLOBALS ['main_menu'].='<ul class="dropdown-menu">';			
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/vehicle_tracking_ctrl").'">Vehicle Tracking</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/vehicle_group_ctrl").'">Vehicle Group</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/vehicle_ctrl").'">Vehicle</a></li>	';	         
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/driver_group_ctrl").'">Driver Group</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/driver_ctrl").'">Driver</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/device_installation_ctrl").'">Device Installation</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/shift_setting_ctrl").'">Shift Detail</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/trip_register_ctrl").'">Trip Register</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/fuel_coupon_ctrl").'">Fuel Management</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/user_management_ctrl").'">Users </a></li>';
			if($GLOBALS ['ID'] ['sess_is_admin']!='0')
				$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/user_management_ctrl").'">Users </a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/vehicle_route_ctrl").'">Define Route</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/vehicle_route_management_ctrl").'">Route Management</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/route_violation_ctrl").'">Route Violation</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/vehicle_geofence_ctrl").'">Define Geo-Boundary</a></li>';
			$GLOBALS ['main_menu'].='<li><a href="'.base_url("index.php/geofence_mgnt_ctrl").'">Geo-Boundary Management</a></li>';
			$GLOBALS ['main_menu'].='</ul>';
			$GLOBALS ['main_menu'].='</li>';
			   $GLOBALS ['main_menu'].='<li class="dropdown-submenu">
				  <a class="not_close_onclick_menu" tabindex="-1" href="#">Reports</a>
				  <ul class="dropdown-menu">';
				  if($clnt=='121' || $clnt=='1')
				  {
					$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/fuel_mis_i").'">Fuel Report-I(Distance)</a></li>';
					//$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/fuel_mis_ii").'">Fuel Report-II(Ignition-ON)</a></li>';
					$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/fuel_coupon_ctrl").'">Fuel Management</a></li>';
				  }
					$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/speed_violations_ctrl").'">Speed Violation</a></li>';
					$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/working_hr_rpt_ctrl").'">Working Hour </a></li>';/*
					/*
						$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/daily_activity_rpt_ctrl").'">Daily Activity Report</a></li>';
						$GLOBALS ['main_menu'].='<li><a href="'. base_url("index.php/vehicle_summary_rpt_ctrl").'">Vehicle Summery Report</a></li>';
					*/					
				  $GLOBALS ['main_menu'].='</ul>
			   </li>
			</ul>';
			/*
			<li><a href="'.base_url("index.php/fuel_coupon_ctrl").'">Fuel Coupon</a></li>
			<li><a href="'.base_url("index.php/detailed_activity_report").'">Detailed Activity</a></li>
					 <li><a href="'.base_url("index.php/vehicle_summary_report").'">Vehicle Summary</a></li>
					 <li><a href="'.base_url("index.php/trip_wise_report").'">Trip-wise Report</a></li>
					 <li><a href="'.base_url("index.php/route_deviation_report").'">Route Deviation</a></li>
			*/
		}else{
			$GLOBALS ['main_menu']='<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<li><a href="'.base_url("index.php/vehicle_tracking_ctrl").'">Vehicle Tracking</a></li>
			</ul>';
		}
		/*
		 * 		<li><a href="'.base_url("index.php/insurance_agency_ctrl").'">Insurance Agency</a></li>
				<li><a href="'.base_url("index.php/vehicle_service_ctrl").'">Vehicle Service</a></li>
				<li><a href="'.base_url("index.php/vehicle_stoppage_report").'">Vehicle Stoppage</a></li>
		*/
	}
	public function get_gps_data() {
		$date_time = date ( 'Y-m-d H:i:s' );
		// echo $date_time;
		
		$query = $this->db->query ( "select count (*) as moving_vehicles from(Select max(gps_datetime),gps_imeino from server_gps_data gp, vts_trip_register where gps_vehicle_id=trip_register_vehicle_id
		and gps_digitalinputstatus = 'Y'
		and gps_datetime >= '" . $date_time . "' group by gps_imeino)as cunt" );
		$row = $query->row_array ();
		return $row ['moving_vehicles'];
	}
	/*
	*function to get driver group data
	*/
	public function get_drivers_group_data($clientid) {
		try {
			$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
			$user_id = $GLOBALS ['ID'] ['sess_userid'];
			$clientid = (isset($clientid))?$clientid:"'0'";
			//log_message ( "debug", "--dr--userid-----" . $user_id . "-dr-clientid--" . $clientid );
			$command = "select * from get_vts_driver_groups_for_userid('" . $user_id . "'," . $clientid . ");";
			$query = $this->db->query ( $command );
			$res = $query->result_array ();
			return $res;
		} catch ( Exception $e ) {
			log_message ( 'error', 'get driver group data data from get_vts_driver_groups_for_userid function', $e->getMessage () );
			return null;
		}
	}
	
	/*
	 * function to get dynaminc driver data in dashboard 12/06/15
	 */
	public function get_no_drivers($clientid, $drvgpid) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$clientid = (isset($clientid))?$clientid:"'0'";
		if (! isset ( $drvgpid )) {
			$cmd = "select get_driver_count_dashboard(" . $clientid . ") as drivers ";
		} else {
			$cmd = "select get_driver_count_dashboard(" . $clientid . ",'" . $drvgpid . "')as drivers ";
		}
		//log_message ( "debug", "--client---" . $clientid, "--drgp--" . $drvgpid );
		
		$query = $this->db->query ( $cmd );
		$row = $query->row_array ();
		return $row ['drivers'];
	}
	/*
	 * created by shruthi
	 * function to get all vehicles related to particular client and vehicle group
	 * @parameters $clientid and $vehiclegroupid
	 */
	public function get_vehicles_data($clientid, $vhgpid) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$client_id = (isset($clientid))?$clientid:"'0'";
		if (! isset ( $vhgpid )) {
			$cmd = "select get_vehicle_count_dashboard(" . $client_id . ") as vehicles ";
		} else {
			$cmd = "select get_vehicle_count_dashboard(" . $client_id . ",'" . $vhgpid . "')as vehicles ";
		}
		$query = $this->db->query ( $cmd );
		$row = $query->row_array ();
		return $row ['vehicles'];
	}
	/*
	 * created by shruthi
	 * function to get all vehicle groups for selected client.
	 * @parameter $clientid
	 */
	public function get_vehicles_group_data($clientid) {
		try {
			/* new loginc by using function with parameter as client id */
			$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
			$client_id = (isset($clientid))?$clientid:"'0'";
			$user_id = $GLOBALS ['ID'] ['sess_userid'];
			$command = "select * from get_vts_vehicle_groups_for_userid('" . $user_id . "'," . $client_id . ");";
			$query = $this->db->query ( $command );
			$res = $query->result_array ();
			return $res;
		} catch ( Exception $e ) {
			log_message ( 'error', 'get settings data from vts_vehicle_group table', $e->getMessage () );
			return null;
		}
	}
	/*
	 * created by shruthi
	 * function to get speed violations data
	 */
	public function get_speed_violations_data($clientid,$vh_gp_id,$drv_gp_id) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$user_id = $GLOBALS ['ID'] ['sess_userid'];
		$clientid = (isset($clientid))?$clientid:"'0'";
		//$frm_date = Date("2015/06/21",strtotime("-10 days"));
		//$to_date ="2015/06/21";
		//$frm_date = Date("Y/m/d",strtotime("-10 days"));
		$date = date_create(date('Y/m/d'));
		date_sub($date, date_interval_create_from_date_string(SPEED_VIO_DATE_DIFF));
		$frm_date = date_format($date, 'Y/m/d');
		
		$to_date =date("Y/m/d");
		//log_message("debug","---from dtae".$frm_date."---to date---".$to_date);
		$vh_list = "";
		$dr_list = "";
		$drv_gp_id = (isset ( $drv_gp_id )) ? $drv_gp_id : '0';
		$vh_gp_id = (isset ( $vh_gp_id )) ? $vh_gp_id : '0';
		$command = "select * from get_vehicle_speed_violations(" . $clientid . ",'" . $user_id . "','" . $frm_date . "','" . $to_date . "','" . $vh_gp_id . "','" . $vh_list . "','" . $drv_gp_id . "','" . $dr_list . "')order by from_datetime desc limit '".SPEED_VIO_LIMIT_VALUE."';";
		//log_message ( "debug", "---command---" . $command ."---limit--".SPEED_VIO_LIMIT_VALUE);
		$query = $this->db->query ( $command );
		// $arr = array ();
		$res = $query->result_array ();
		//log_message("debug","---db gt spd vio res--".$command);
		return $res;
	}
		/*
	 * created by shruthi
	 * Function to get service status data parameter client id
	 */
	public function get_service_status_data($clientid) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
	//25-aug-15 commented	//$client_id = (isset($GLOBALS ['ID'] ['sess_clientid']))?$GLOBALS ['ID'] ['sess_clientid']:"'0'";
		$client_id = (isset($clientid))?$clientid:"'0'";
		//25-aug-15 commented//$this->db->select ( "get_vts_service_status(" . $client_id . ") as service_data" );
		$command  = "select * from get_vts_service_status(".$client_id.")";
		//25-aug-15 commented$query = $this->db->get ();
		// $arr = array ();
		$query = $this->db->query ( $command );
		$res = $query->result_array ();
		return $res;
	}
	
	/*
	 * created by shruthi 01-jul-15
	 * function to get data for dashboard middle table
	 * @parameter $drivergroupid, $vehiclegroupid, $userid, $clientid,$date
	 * return data for dashboard middle grid.
	 */
	public function get_dashboard_table($clientid, $drv_gp_id, $vh_gp_id) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		// $client_id = $GLOBALS['ID']['sess_clientid'];
		$clientid = (isset($clientid))?$clientid:"'0'";
		$user_id = $GLOBALS ['ID'] ['sess_userid'];
		$date = date("Y/m/d");// for today's date
		//$date = "2015/06/03";
		$drv_gp_id = (isset ( $drv_gp_id )) ? $drv_gp_id : '0';
		$vh_gp_id = (isset ( $vh_gp_id )) ? $vh_gp_id : '0';
		$command = "select * from get_vehicle_data_for_dashboard('" . $date . "'," . $clientid . ",'" . $user_id . "','" . $drv_gp_id . "','" . $vh_gp_id . "');";
		//log_message ( "debug", "---command---" . $command );
		$query = $this->db->query ( $command );
		// $arr = array ();
		$res = $query->result_array ();
		//log_message("debug","---get-dashboard--".print_r($res,true));
		return $res;
	}
	
	/**
	 * Function to get the Spot of the
	 * Vehicle
	 *
	 * @param s $latitude        	
	 * @param s $longitude
	 *        	Returns $string of particular spot
	 *        	
	 *        	
	 */
	public function getPlaceName($latitude, $longitude) {
		$geocode = file_get_contents ( 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . ($latitude) . ',' . ($longitude) . '&key='.MAP_API_KEY );
		$output = json_decode ( $geocode );
		
		if (! isset ( $output->results [1]->formatted_address )) {
			return 'Spot Name not identified. ( Latitude - ' . round($latitude,4) . ' , Longitude - ' . round($longitude,4) . ')';
		} else {
			return $output->results [1]->formatted_address;
		}
	}
	
	/*
	 * Get Time Zone from the vts_time_zone table()
	 */
	public function get_time_zone($timeZoneID = null) {
		try {
			$command = "Select time_zone_id,time_zone_name,time_zone_proper_name,time_zone_diff from vts_time_zone";
			if ($timeZoneID != null)
				$command .= " where time_zone_id='" . $timeZoneID . "'";
			$command .= " order by time_zone_name;";
			$query = $this->db->query ( $command );
			if ($timeZoneID == null)
				$result = $query->result_array ();
			else
				$result = $query->row_array ();
			if ($result != null)
				return $result;
			else
				return null;
		} catch ( Exception $ex ) {
			log_message ( 'error', $ex->getMessage () . " vts_time_zone" );
		}
	}
	
	/*
	 * Function to get mail details from settings table
	 */
	public function get_mail_details() {
		try {
			
			$query = $this->db->query ( "select setting_key_name , setting_key_value from vts_setting where setting_key_name in ('SMTPServer', 'SMTPUserName','SMTPPassword','SMTPPort','VTSSupportMailAddress') order by setting_key_name" );
			return $query->result_array ();
		} catch ( Exception $e ) {
			
			log_message ( 'error, To fetch the value from Setting Table', $e->getMessage () );
			return;
		}
	}
	/*
	 * Return the user time
	 */
	public function get_usertime() {
		$data = $this->session->userdata ( 'login' );
		$dates = gmdate ( 'Y-m-d H:i:s' );
		if ($data != null) {
			$user_id = $data ['sess_userid'];
			$dates = gmdate ( 'Y-m-d H:i:s' );
			$command = "Select get_user_datetime('" . $dates . "', '" . $user_id . "') as userdate ";
			$query = $this->db->query ( $command );
			$row = $query->row_array ();
			if ($row != null)
				return $row ['userdate'];
			else
				return $dates;
		}
		return $dates;
	}
	
	/*
	 * This function is used to get the required format datetime(i.e UTC or user date)
	 */
	Public function get_dateTime($given_date, $reveser = 0) {
		$sess_data = $this->session->userdata ( 'login' );
		if ($sess_data != null) {
			$user_id = $sess_data ['sess_userid'];
			$command = "Select get_user_datetime('" . $given_date . "', '" . $user_id . "','" . $reveser . "') as convert_date ";
			$query = $this->db->query ( $command );
			$row = $query->row_array ();
			if ($row != null)
				return $row ['convert_date'];
			else
				return $dates;
		}
		return $dates;
	}
	
	/*
	 * This function is used to check the session
	 */
	public function check_session() {
		$GLOBALS ['sessionData'] = $this->session->userdata ( 'login' );
		if ($GLOBALS ['sessionData'] != null) {
			return true;
		} else {
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
		
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients() {
		try {
			$command = "select client_id, client_name from vts_client where client_id !=" . AUTOGRADE_USER . " order by client_name";
			$query = $this->db->query ( $command );
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		} catch ( Exception $ex ) {
			log_message ( 'error', 'get all client name - vts_client' );
			return null;
		}
	}
	
	/*
	 * Created By sunita on 18/06/2015
	 * This function is to get the client name which is selected from the dropdown(if they are autograde user's)
	 */
	public function get_dropdownCntName($clientid) {
		try {
			$clientid = (isset($clientid))?$clientid:"'0'";
			$command = "select client_name from vts_client where client_id = " . $clientid;
			$query = $this->db->query ( $command );
			$row = $query->row_array ();
			return $row ['client_name'];
		} catch ( Exception $e ) {
			log_message ( 'error', 'get the client name which is selected from the dropdown' );
			return null;
		}
	}
	/*function to check given email is exist in user table or not 
	 * @parameter to be passed- $email
	 * return true if email exist 
	 */
	public function check_user_email($email)
	{
		//log_message("debug","-----model chek user email---");
		$usermail = $this->get_user_table_data($email);
		//log_message("debug","-----model  email---".print_r($usermail,true));
		if($usermail)
		{
			return false;
		}else 
			return true;
	}
	/*
	 * function to get all usernames from user table by passing email as paramater
	 */
	public function get_user_table_data($email)
	{
		try {
		$command = "select user_email from vts_users where user_email = '".$email."'";
		$query = $this->db->query ( $command );
		$row = $query->row_array ();
		return $row;
		}catch ( Exception $e ) {
			log_message ( 'error', 'get user email from vts_users table' );
			return null;
		}
	}
	
		/*
		 * function to get purchase details and sales details of devices belongs to logged in distributor.
		 */
		public function get_device_distributor_details($distid)
		{
			$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
			//$logged_in = $GLOBALS ['ID'] ['sess_distributorid'];
			$current_date = date("Y/m/d");
			$command  = "select * from get_distributor_data_for_dashboard('".$current_date."', '".$distid."');";
			$query = $this->db->query ( $command );
			$row = $query->result_array ();		
			//log_message("debug","----fucnton----".print_r($row,true));
			return $row;
		}
		
		/*
		 * function to get purchase details and sales details of devices belongs to logged in dealer.
		 */
		public function get_device_dealer_details($dealerid)
		{
			$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
			//$logged_in = $GLOBALS ['ID'] ['sess_distributorid'];
			$current_date = date("Y/m/d");
			$command  = "select * from get_dealer_data_for_dashboard('".$current_date."', '".$dealerid."');";
			$query = $this->db->query ( $command );
			$row = $query->result_array ();
			//log_message("debug","----fucnton----".print_r($row,true));
			return $row;
		}
		
	
}
?>
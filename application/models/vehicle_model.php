<?php

class Vehicle_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	
	/*
	 * This function is used to get the vehiclegroup from the
	 * vehiclegroup table to fill the vehicle group dropDownBox in vehicle view.
	 */
	public function get_all_vehicle_group($session_login_client_id){
		try{
			if($session_login_client_id!=AUTOGRADE_USER)
			{
				$this->db->order_by('vehicle_group','ASC');
				$this->db->select('vehicle_group_id, vehicle_group');
				$this->db->where('vehicle_group_isactive', ACTIVE);
				if($session_login_client_id!=AUTOGRADE_USER)
				{
					$this->db->where('md5(vehicle_group_client_id::varchar)', $session_login_client_id);
				}
				$query = $this->db->get('vts_vehicle_group');
				return $query->result_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group name - vts_vehicle_group');
			return null;
		}
	}
	
	/*
	 * This function used to return the vehicle group detail relate the give parameter(i.e. vehicle_id)
	 * if any error, then return null.
	 */
	public function get_vts_vehicle($rowPerPage=null,$startNo=null,$vehicle_id=null,$session_login_client_id=null)
	{
		try
		{
			if($rowPerPage==null & ($vehicle_id==null & $session_login_client_id!=md5(AUTOGRADE_USER)))
			{
				log_message("debug","first if condition---");
				$this->db->order_by('vehicle_regnumber','ASC');
				$this->db->select('vehicle_id, vehicle_regnumber, vehicle_fuel_type_id, vehicle_model_id, vehicle_group_id, vehicle_client_id, get_vehicle_group_name(vehicle_group_id) vehicle_group_name');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(vehicle_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_vehicle');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($vehicle_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				log_message("debug","first else if condition---");
				$this->db->limit($rowPerPage, $startNo);
				
				$this->db->order_by('vehicle_regnumber','ASC');
				$this->db->select('vehicle_id, vehicle_regnumber, vehicle_fuel_type_id, vehicle_model_id, vehicle_group_id, vehicle_client_id, get_vehicle_group_name(vehicle_group_id) vehicle_group_name');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(vehicle_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_vehicle');
				$query = $this->db->get();
				return $query->result_array();
							
			}
			else if($session_login_client_id!=md5(AUTOGRADE_USER))
			{
				log_message("debug","second else if condition---");
				$query=$this->db->get_where('vts_vehicle',array('md5(vehicle_id::varchar)'=>$vehicle_id));
				return $query->row_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_vehicle',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the vehicle details from the vts_vehicle table
	 */
	public function InsertOrUpdateOrdelete_vts_vehicle($vehicle_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_vehicle', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('vehicle_id', $vehicle_id);
				$this->db->update('vts_vehicle', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('md5(vehicle_id::varchar)', $vehicle_id);
				if($this->db->delete('vts_vehicle'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_vehicle',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in vehicle screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}
	
	/*
	 * This function is to get all agencies list to display in the dropdown
	 */
	public function get_all_agencies($session_login_client_id)
	{
		try{
			if($session_login_client_id!=AUTOGRADE_USER)
			{
				$this->db->order_by('insurance_agency','ASC');
				$this->db->select('insurance_agency_id, insurance_agency');
				$this->db->where('insurance_agency_isactive', ACTIVE);
				if($session_login_client_id!=AUTOGRADE_USER)
				{
					$this->db->where('md5(insurance_agency_client_id::varchar)', $session_login_client_id);
				}
				$query = $this->db->get('vts_insurance_agency');
				return $query->result_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all insurance agency- vts_insurance_agency');
			return null;
		}
	}
	
	/*
	 * This function is to get the vehicle Insurance Policy Type from vts_config_type table and display in the dropdown
	 */
	public function get_vehiclePolicyType()
	{
		try{
			$command="select ct.config_type_id, ct.config_type, c.config_id, c.config_item from vts_config_type ct, vts_config c where ct.config_type_id = c.config_type_id and ct.config_type = 'InsuranceType' order by c.config_item";
			$query = $this->db->query($command);
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all config items - vts_config');
			return null;
		}
	}
	
	/*
	 * This function is used to get all the vehicle select vehicleGpId from the
	 * vts_client table to fill the drop down.
	 */
	public function get_all_vhGp_vehicle( $vehicleGpID=null, $todayDateTime=null, $vhID=-1, $isEdit='0')
	{
		try
		{
			$command="";
			if($vehicleGpID!=null & $todayDateTime!=null)
			{
				$command="Select vehicle_id as vh_id, vehicle_regnumber as vh_name from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and vehicle_isactive='".ACTIVE."'
and vehicle_id !=COALESCE((select trip_register_vehicle_id  from vts_trip_register where (('".$todayDateTime."' < trip_register_time_start) or ('".$todayDateTime."' < trip_register_time_end) or trip_register_time_end is null) and
trip_register_vehicle_id=vehicle_id limit 1),'-1') ";
				if($isEdit == '1')
					$command.=" or vehicle_id = ".$vhID;
				$command.=" order by vehicle_regnumber;";
				//log_message('debug','**Driver Screen[get_VehicleList]**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
	
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle related to vehicle group');
			return null;
		}
	}
	
	/*
	 * This function is to get all the fuel companies from vts_fuel_sensor_vehicle table and shown
	 *  in drop down in vehicle screen
	 */
	public function get_allCompanies()
	{
		try{
			$this->db->order_by('fsv_model','ASC');
			$this->db->select('fsv_id, fsv_model');
			$this->db->where('fsv_parent_id IS NULL');
			$query = $this->db->get('vts_fuel_sensor_vehicle');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all fuel vehicels - vts_fuel_sensor_vehicle');
			return null;
		}
	}
	
	/*
	 * This function is used to get all the company models select depending on the company selected from the
	 * vts_fuel_sensor_vehicle table to fill the drop down.
	 */
	public function get_all_cmpy_models( $companyId ) {
		
		$this->db->select("fsv_id, fsv_model")
		->from("vts_fuel_sensor_vehicle")
		->where("fsv_parent_id", $companyId);
		
		$query = $this->db->get();
		
		$result = $query->result_array();
		
		return $result;
		
	}
	
	/*
	 * This function is to get the vehicle Fuel Type from vts_config_type table and display in the dropdown
	 */
	public function get_FuelType()
	{
		try{
			$command="select ct.config_type_id, ct.config_type, c.config_id, c.config_item from vts_config_type ct, vts_config c where ct.config_type_id = c.config_type_id and ct.config_type = '".FUELTYPE."' order by c.config_item";
			log_message("debug","command---".$command);
			$query = $this->db->query($command);
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all config items - vts_config');
			return null;
		}
	}
	
	/*
	 * This function is to get the vehicle Voltage Type from vts_config_type table and display in the dropdown
	 */
	public function get_VoltageType()
	{
		try{
			$command="select ct.config_type_id, ct.config_type, c.config_id, c.config_item from vts_config_type ct, vts_config c where ct.config_type_id = c.config_type_id and ct.config_type = '".VOLTAGETYPE."' order by c.config_item";
			log_message("debug","command---".$command);
			$query = $this->db->query($command);
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all config items - vts_config');
			return null;
		}
	}
	
	public function getCompany_name($model_id)
	{
		try{
			$command="SELECT t1.fsv_model, t1.fsv_id  FROM vts_fuel_sensor_vehicle t1 INNER JOIN vts_fuel_sensor_vehicle t2 ON t1.fsv_id=t2.fsv_parent_id WHERE t1.fsv_id=1 LIMIT 1";
			$query = $this->db->query($command);
			return $query->result_array();
		}
		catch(Exception $ex)
		{
			log_message("error","get the company name");
			return null;
		}
	}
	
	
	public function get_Vehicle_Brand_Model( $model_no ) {
		
		$data = array();
		
		$this->db->select( "t2.fsv_id AS brand_id, t2.fsv_model AS brand_name" )
		->from("vts_fuel_sensor_vehicle t1")
		->join("vts_fuel_sensor_vehicle t2", "t2.fsv_id=t1.fsv_parent_id", "INNER")
		->where("t1.fsv_id", $model_no )
		->limit(1);
		
		$query = $this->db->get();
		
		foreach( $query->result() AS $res ) {
			
			$data = array(
					
				"brand_id" => $res->brand_id,
				"brand_name" => $res->brand_name
			);
		}
		
		$query->free_result();
		
		return $data;
		
		$this->db->close();
		
	}
	
}
?>
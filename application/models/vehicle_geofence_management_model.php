<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_geofence_management_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');	
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	public function get_VhRouteList($clientID=null, $rowPerPage=null, $startNo=null, $vhicleID=null, $delete=false)
	{
		try
		{
			$checkWith=($delete)?"vr.vh_rt_id":"vr.vh_rt_vehicle_id";
			$command="SELECT vr.vh_rt_id, vr.vh_rt_route_id, vr.vh_rt_alert_type as alert_type, r.route_name, vr.vh_rt_vehicle_id, v.vehicle_group_id, v.vehicle_regnumber, r.route_client_id
FROM vts_vehicle_route vr inner join vts_vehicle v on v.vehicle_id=vr.vh_rt_vehicle_id inner join vts_route r on r.route_id=vr.vh_rt_route_id and r.route_is_geofence='".ACTIVE."' ";
			if($GLOBALS['sessClientAdmin']==NOT_ACTIVE && $GLOBALS['sessClientAdmin']!=AUTOGRADE_USER)
			{
				$command.=" inner join vts_vehicle_group vg on v.vehicle_group_id=vg.vehicle_group_id";
				$command.=" inner join vts_user_vehicle_group_link vgl on vg.vehicle_group_id=vgl.vh_gp_link_vehicle_group_id and vgl.vh_gp_link_user_id='".$GLOBALS['sessUserID']."'";
			}
			$command.=" where 1=1";
			if($clientID != null)								
				$command.=" and md5(r.route_client_id::varchar)='".$clientID."'";			
			if($vhicleID!=null)
				$command.=" and md5($checkWith::varchar)='".$vhicleID."'";
			$command.=" order by v.vehicle_regnumber";
			if($rowPerPage!=null)
				$command .= " limit ".$rowPerPage." offset ". $startNo;
			$command .= ";";
			//log_message("debug","***get_VhRouteList***".$command);
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $e) {
			log_message('error', 'get_VhRouteList '.$e->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the list of routs
	 */
	public function get_RouteList($clientID)
	{
		try
		{
			if($clientID!=null){
				$command="select route_user_define_id, route_id, route_name from vts_route where md5(route_client_id::varchar)='".$clientID."' and route_is_geofence='".ACTIVE."' and route_is_active='".ACTIVE."' order by route_name;";
				$query=$this->db->query($command);
				$result=$query->result_array();
				return $result;
			}else 
				return null;
			
		}catch (Exception $e) {
			log_message('error', 'get_RouteList '.$e->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients(){
		try
		{
		/*	$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER." order by client_name";
			$query = $this->db->query($command);
			$result=$query->result_array();*/
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	
	/*
	 * This function is used to get the vehicle group for the given client ID
	 */
	public function get_all_vhGp($clientID=null,$sessClient=null,$sessUser=null)
	{
		try
		{
// 			if($GLOBALS['sessClientID']!=null)
// 			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and md5(vehicle_group_client_id::varchar)='".$clientID."' order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and md5(vehicle_group_client_id::varchar)='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
				//log_message('debug','--vhgp--'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
// 			}
// 			else
// 				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	
	
	public function get_all_vhGp_vehicle($vehicleGpID, $vhID=0)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{
				$command="select vehicle_id as vh_id, vehicle_regnumber as vh_name from vts_vehicle v  where v.vehicle_group_id='".$vehicleGpID."' and  v.vehicle_isactive='".ACTIVE."' and( v.vehicle_id not in (COALESCE((select vh_rt_vehicle_id from vts_vehicle_route INNER JOIN vts_route ON vh_rt_route_id=route_id AND route_is_geofence='1' where v.vehicle_id=vh_rt_vehicle_id limit 1 offset 0), 0))";
				$command.=($vhID!=0)?' or v.vehicle_id ='.$vhID  :' ';
				$command.=" ) order by v.vehicle_regnumber;";
				$query = $this->db->query($command);
				//log_message('debug','**Vehicle**'.$command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error',' get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	
	public function insert_edit_vhroute($data, $vhIdOrRouteId=0, $opt="insert")
	{
		try{
			//log_message('debug','*****insert_edit_vhroute****');
			$this->db->trans_start();
			if($opt=="insert")
			{			
				$this->db->insert_batch('vts_vehicle_route', $data);
			}
			else if($opt=="edit")
			{
				$this->db->where('vh_rt_vehicle_id', $vhIdOrRouteId);
				$this->db->delete('vts_vehicle_route');
				$this->db->insert_batch('vts_vehicle_route', $data);
				$this->db->where('vehicle_id',$vhIdOrRouteId);
				$this->db->update('vts_device_config',array('update_required'=>'1'));
			}
			else if($opt=="delete")
			{
				$this->db->where('md5(vh_rt_id::varchar)', $vhIdOrRouteId);
				$this->db->delete('vts_vehicle_route');
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === TRUE)
				return true;
			else
				return false;
		}catch (Exception $ex){
			log_message('error', 'insert_edit_vhroute ',$ex->getMessage());
			return null;
		}
	}
}
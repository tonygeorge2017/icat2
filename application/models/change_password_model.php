<?php
class Change_password_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	/*
	 * This function used to return the user detail relate the give parameter(i.e. $userName)
	 * if any error, then return null. 
	 * @param 
	 * 	$userName - user login email id
	 *  $pwd - user login password
	 *  $id - user id
	 *  return type -  row array 
	 */
	public function get_user_detail($userName,$pwd=null,$id=null)
	{
		try
		{
			if($id!=null)
				$query=$this->db->query("select user_user_name, user_email from vts_users where md5(user_id :: varchar)='".$id."'");
			else if($pwd==null)
				$query=$this->db->get_where('vts_users',array('user_email'=>$userName));
			else
				$query=$this->db->get_where('vts_users',array('user_email'=>$userName,'user_password'=>$pwd));
			$row=$query->row_array();
			if($row!=null)
				return $query->row_array();
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get user detail - Users',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function will update the User password and return true if succeed else false
	 * @param
	 *  $id - user id 
	 *  $newpwd - user login new password
	 * return type - bool
	 */
	public function update_pwd($id,$newpwd)
	{
		try
		{
			$data = array('user_password' => $newpwd,'user_last_pwd_change_date'=>date("Y/m/d"));
			$this->db->where('user_id', $id);
			$this->db->update('vts_users', $data);
			return true;
		}
		catch (Exception $ex)
		{
			log_message('error, update to table - Users',$ex->getMessage());
			return FALSE;
		}
	}
	/*
	 * This function is used to get the minimum password length from the setting table.
	 * it will return MinPasswordLength if it have any value else return 'Non'.
	 * return type - integer
	 */
	public function getMinPwdLength()
	{
		try
		{
			$query=$this->db->get_where('vts_setting',array('setting_key_name'=>'MinPasswordLength'));
			$row=$query->row_array();
			if(!empty($row['setting_key_value']))
				return $row['setting_key_value'];
			else 
				return 0;
		}catch (Exception $ex)
		{
			log_message('error, get min length of password from setting',$ex->getMessage());
			return 0;
		}
	}
	/*
	 * This function get starting character of the user name and
	 * check with db and list all the related records.
	 */
	public function get_all_user($userName)
	{
		try{			
			$query=$this->db->query("select user_id, user_email from vts_users where lower(user_email) like lower('".$userName."%')");
			$row=$query->result_array();			
			return $row;
			
		}catch (Exception $ex)
		{
			log_message('error, get all the user name',$ex->getMessage());
			return null;
		}
	
	}
}
<?php

class Speed_violations_model extends CI_Model{

	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS['sess_usr_id'] = $GLOBALS ['ID']['sess_userid'];
		$GLOBALS['sess_clnt_id'] = $GLOBALS['ID']['sess_clientid'];
	}

	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in speed violations screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}

	/*
	 * This function is used to get the vehiclegroup from the
	 * vehicle group table to fill the vehicle group dropDownBox in speed violations screen view.
	 */
	public function get_allVehicleGroups($clientID=0){
		try{
				$command = "select * from get_vts_vehicle_groups_for_userid(".$GLOBALS['sess_usr_id'].",". $clientID.")";
				$query = $this->db->query($command);
			
				if ($query != null)
					return $result=$query->result_array();
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group name - vts_vehicle_group');
			return null;
		}
	}

	/*
	 * This function is to get all the vehicles from vehicle table and shown
	 * client name in drop down in vehicle service screen
	 */
	public function get_allVehicles($vehicleGpID=null)
	{
		try
		{
			if($vehicleGpID!=null)
			{
				$this->db->order_by('vehicle_regnumber','ASC');
				$this->db->select('vehicle_id, vehicle_regnumber');
				$this->db->where('vehicle_isactive', ACTIVE);
				$this->db->where('vehicle_group_id', $vehicleGpID);
				
				$query = $this->db->get('vts_vehicle');
				return $query->result_array();
			}return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehical names - vts_vehicle');
			return null;
		}
	}

	/*
	 * This function is used to get all the device from the device table
	 * to fill the drop down in user view.
	 */
	public function get_allDriver($driverGpID)
	{
		try
		{
			if($driverGpID!=null)
			{
				$command="Select driver_id, driver_name from vts_driver where driver_drivergroup_id='".$driverGpID."' and  driver_isactive='".ACTIVE."' order by driver_id;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
			{
				return null;
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all device which is relted to the given device type');
			return null;
		}
	}

	/*
	 * This function is used to get all the device tyepe from the device type table
	 * to fill the drop down in user view.
	 */
	public function get_allDriverGroup($sessClientID, $clientID=0)
	{
		try
		{
			if($sessClientID==AUTOGRADE_USER)
				$command="select driver_group_id, driver_group from vts_driver_group where driver_group_isactive='".ACTIVE."' and md5(driver_group_client_id::varchar)='".$clientID."' order by driver_group ;";
			else
				$command="select driver_group_id, driver_group from vts_driver_group where driver_group_isactive='".ACTIVE."' and md5(driver_group_client_id::varchar)='".md5($sessClientID)."' order by driver_group ;";

			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all device type');
			return null;
		}
	}

	/*
	 * This function is to get the records from get_vehicle_speed_violations function
	 */
	public function get_values($cnt_id, $data, $sess_usr_id)
	{
		try{
			$query = " SELECT * from get_vehicle_speed_violations(".$cnt_id.",". $sess_usr_id.", '".$data ['spd_vio_strt_date'] ." 00:00:00', '".$data ['spd_vio_end_date'] ." 23:59:59'";

			if (isset ( $data ['spd_vio_vclegroup_id'] ))
				$query .= ", " . $data ['spd_vio_vclegroup_id'] . "";
			else
				$query .= ", 0";
			
			if (isset ( $data['spd_vio_vehicle_id']))
			{
				$query .= ",'";
				if(count($data['spd_vio_vehicle_id']) == 1)
					$query .= $data['spd_vio_vehicle_id'][0];
				else
				{
					for ($i=0;$i<count($data['spd_vio_vehicle_id'])-1 ;$i++)
						$query .=  $data['spd_vio_vehicle_id'][$i].",";
					$query .=  $data['spd_vio_vehicle_id'][count($data['spd_vio_vehicle_id'])-1];
				}
				$query .= "'";
			}
			else
				$query .= ", ''";
				
			if (isset ( $data ['spd_vio_dvrgroup_id'] ))
				$query .= ", " . $data ['spd_vio_dvrgroup_id'] . "";
			else
				$query .= ", 0";
			
			if (isset ( $data['spd_vio_driver_id']))//$vcle_id
			{
				$query .= ",'";
				if(count($data['spd_vio_driver_id']) == 1)
					$query .= $data['spd_vio_driver_id'][0];
				else
				{
					for ($i=0;$i<count($data['spd_vio_driver_id'])-1 ;$i++)
						$query .=  $data['spd_vio_driver_id'][$i].",";
						$query .=  $data['spd_vio_driver_id'][count($data['spd_vio_driver_id'])-1];
				}
				$query .= "'";
			}
			else
				$query .= ", ''";
			
				$query .= ")";
				$query .= " order by vehicle, from_datetime";
				log_message("debug",($query));
			$query_final = $this->db->query($query);
			
		
			return $query_final->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get values');
			return null;
		}
	}

	/*
	 * This fucntion is to insert or delete the data to the temp table
	 */
	public function InsertOrdelete_vts_tmp_report_speedviolations($data,$operation,$chk_btn)
	{
		try
		{
			if($operation=="insert")
			{
				foreach ($data as $data1)
				{
					$data2['user_id'] = $GLOBALS['sess_usr_id'];
					$data2['vehicle'] = $data1['vehicle'];
					$data2['imei'] = $data1['imeino'];
					$data2['driver'] = $data1['driver'];
					$data2['from_date_time'] = $data1['from_datetime'];
					$data2['to_date_time'] = $data1['to_datetime'];
					$data2['duration'] = $data1['duration'];
					$data2['maxspeed'] = $data1['maxspeed'];
					$data2['avgspeed'] = $data1['avgspeed'];
					$data2['speedlimit'] = $data1['speedlimit'];
					$data2['latitude'] = $data1['latitude'];
					$data2['longitude'] = $data1['longitude'];
					$data2['spot'] = $data1['spot'];
						
					$this->db->insert('vts_tmp_report_speedviolations', $data2);
				}
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('user_id', $GLOBALS['sess_usr_id']);
				if($this->db->delete('vts_tmp_report_speedviolations'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_tmp_report_speedviolations',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all speed violation records to display the grid in view 
	 */
	public function get_allSpeedViolations()
	{
		try{
			$command = "select * from vts_tmp_report_speedviolations where user_id = '".$GLOBALS['sess_usr_id']."' order by vehicle, from_date_time;";
			$query_final = $this->db->query($command);
			return $query_final->result_array();
		}
		catch(Exception $ex)
		{
			log_message('error','get all Speed Violations - vts_tmp_report_speedviolations');
			return null;
		}
	}
	
	public function get_allSpeedViolations_map()
	{
		try{
			$command = "select * from vts_tmp_report_speedviolations where user_id = ".$GLOBALS['sess_usr_id'].";";
			$query_final = $this->db->query($command);
			return $query_final->result_array();
		}
		catch(Exception $ex)
		{
			log_message('error','get all Speed Violations - vts_tmp_report_speedviolations');
			return null;
		}
	}
	
	public function getVehicleName($vcle_id)
	{
		try{
			$command = "select vehicle_regnumber from vts_vehicle where vehicle_id =". $vcle_id;
			$query_final = $this->db->query($command);
			$row = $query_final->row_array();
			if($row!=null)
				return $row['vehicle_regnumber'];
			else
				return  null;
		}
		catch(Exception $ex)
		{
			log_message('error', 'get all vehicle name');
			return null;
		}
	}
	
	public function getDriverName($dvr_id)
	{
		try{
			$command = "select driver_name from vts_driver where driver_id =". $dvr_id;
			$query_final = $this->db->query($command);
			$row = $query_final->row_array();
			if($row!=null)
				return $row['driver_name'];
			else
				return  null;
		}
		catch(Exception $ex)
		{
			log_message('error', 'get all driver name');
			return null;
		}
	}
	
	public function speed_violation_graph_model( $data ) {
		
		$obj = json_decode( $data );
		
		$results = array();
		
		$results["title"] = $this->session->userdata ( 'SESSION_DATA_1' );	//Title of chart
		$results["sub_title"] = "Between ". $obj->from_date. " and ".$obj->to_date; //Sub-title of chart
		$results["graph_height"] = 350;	//Height of chart
		$results["graph_width"] = 600;	//Width of chart
		
		$results["xaxis_title"] = "Vehicle";	//X-Axis title
		$results["yaxis_title"] = "No. of Violations";	//Y-Axis title
		$results["isShowValues"] = true;	//true, to display values on bar
		
		$user_id = $GLOBALS['sess_usr_id'];
		$client_id = $obj->client_name;
		$from_date = $obj->from_date ." 00:00:00";
		$to_date = $obj->to_date ." 23:59:59";
		
		$this->db->select("vehicle, COUNT(vehicle) AS violation_count")
		->from("vts_tmp_report_speedviolations")
		->where("user_id", $user_id)
		->where("from_date_time >=", $from_date )
		->where("from_date_time <=", $to_date )
		->group_by("vehicle");
		
		$query = $this->db->get();
		
		foreach( $query->result() AS $res ) {
			$results["data"][] = array(
					
				"vehicle_name"=>$res->vehicle,
				"violation_count"=>$res->violation_count
			);
		}
		
	
		return $results;
	
	}
	
}
?>
<?php
class Forget_password_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	/*
	 * This function is used to check whether the given email id
	 * is present in the vts_user table or not
	 * @param
	 *  $user_name - user login email id
	 * Return type - row array
	 */
	public function user_check($user_name)
	{	
		try
		{	
			$query = $this->db->query("select * from vts_users where upper(user_email) = '".strtoupper($user_name)."'");
			return $query->row_array();
		}catch (Exception $ex)
		{
			return null;
		}
	}
}
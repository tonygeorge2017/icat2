<?php

class Vehicle_group_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	
	
	
	/*
	 * This function is used to get the vehiclegroup from the
	 * itemgroup table to fill the parent group dropDownBox in vehiclegroup view.
	 */
	public function get_all_parent_vehicle_group($session_login_client_id){
		try{
			if($session_login_client_id!=AUTOGRADE_USER)
			{
				$this->db->order_by('vehicle_group','ASC');
				$this->db->select('vehicle_group_id, vehicle_group');
				$this->db->where('vehicle_group_isactive', ACTIVE);
				if($session_login_client_id!=AUTOGRADE_USER)
				{
					$this->db->where('md5(vehicle_group_client_id::varchar)', $session_login_client_id);
				}
				$query = $this->db->get('vts_vehicle_group');
				return $query->result_array();
			}
			
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group name - vts_vehicle_group');
			return null;
		}
	}
	
	
	/*
	 * This function used to return the vehicle group detail relate the give parameter(i.e. vehicle_group_id)
	 * if any error, then return null.
	 */
	public function get_vts_vehicle_group($rowPerPage=null,$startNo=null,$vehicle_group_id=null,$session_login_client_id=null)
	{
		try
		{
			if($rowPerPage==null & ($vehicle_group_id==null & $session_login_client_id!=md5(AUTOGRADE_USER)))
			{
				$this->db->order_by('vehicle_group','ASC');
				$this->db->select('vehicle_group_client_id,vts_vehicle_group.vehicle_group_id as vehicle_group_id, vts_vehicle_group.vehicle_group as vehicle_group, vts_vehicle_group.vehicle_parent_group_id as vehicle_parent_group_id, get_vehicle_group_name(vehicle_parent_group_id) as vehicle_group_parent');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(vehicle_group_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_vehicle_group');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($vehicle_group_id==null & $session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$this->db->limit($rowPerPage, $startNo);
				
				$this->db->order_by('vehicle_group','ASC');
				$this->db->select('vehicle_group_client_id,vts_vehicle_group.vehicle_group_id as vehicle_group_id, vts_vehicle_group.vehicle_group as vehicle_group, vts_vehicle_group.vehicle_parent_group_id as vehicle_parent_group_id, get_vehicle_group_name(vehicle_parent_group_id) as vehicle_group_parent');
				if($session_login_client_id!=md5(AUTOGRADE_USER))
				{
					$this->db->where('md5(vehicle_group_client_id::varchar)', $session_login_client_id);
				}
				$this->db->from('vts_vehicle_group');
				$query = $this->db->get();
				return $query->result_array();
			}
			else if($session_login_client_id!=md5(AUTOGRADE_USER))
			{
				$query=$this->db->get_where('vts_vehicle_group',array('md5(vehicle_group_id::varchar)'=>$vehicle_group_id));
				return $query->row_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_vehicle_group',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the vehicle group details from the vts_vehicle_group table
	 */
	public function InsertOrUpdateOrdelete_vts_vehicle_group($vehicle_group_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_vehicle_group', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('vehicle_group_id', $vehicle_group_id);
				$this->db->update('vts_vehicle_group', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('md5(vehicle_group_id::varchar)', $vehicle_group_id);
				if($this->db->delete('vts_vehicle_group'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_vehicle_group',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in vehicle group screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}
	
}
?>
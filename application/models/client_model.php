<?php

class Client_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	
	
	/*
	 * This function used to return the client detail relate the give parameter(i.e. clientID)
	 * if any error, then return null.
	 */
	public function get_vts_client($rowPerPage=null,$startNo=null,$client_id=null)
	{
		$GLOBALS['ID'] = $this->session->userdata('login');
		try
		{
			if($rowPerPage==null & $client_id==null)
			{
				$this->db->order_by('client_name','ASC');
// 				$this->db->select('vts_client.client_id as client_id, vts_client.client_name as client_name, vts_client.client_email as client_email, vts_client.client_contact_designation as client_contact_designation');
			/*	$this->db->select('*');
				$this->db->from('vts_client');*/
				
				
				$this->db->select('*');
				$this->db->from('vts_client');
				if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
					$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
				$query = $this->db->get();
				return $query->result_array();
			}
			else if($client_id==null)
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('client_name','ASC');
// 				$this->db->select('vts_client.client_id as client_id, vts_client.client_name as client_name, vts_client.client_email as client_email, vts_client.client_contact_designation as client_contact_designation');
				$this->db->select('*');
				$this->db->from('vts_client');
				if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
					$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
				$query = $this->db->get();
				return $query->result_array();
			}
			else
			{
				$query=$this->db->get_where('vts_client',array('client_id'=>$client_id));
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_client',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To delete the user role from the vts_client table
	 */
	
	public function InsertOrUpdateOrdelete_vts_client($client_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_client', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('client_id', $client_id);
				$this->db->update('vts_client', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('client_id', $client_id);
				$this->db->delete('vts_client');
				return true;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_client',$ex->getMessage());
			return false;
		}
	}
	
	public function get_max_client_id()
	{
		$max_client_id = $this->db->select_max('client_id');
		$query = $this->db->get('vts_client');
		$row = $query->row_array();
		return $row['client_id'];
	}
	
	/*
	 * This function is used to all the dealers from the
	 * dealer table to fill the drop down in cleint view.
	 */
	public function get_allDealers()
	{
		try{
				$this->db->order_by('dealer_name','ASC');
				$this->db->select('dealer_name, dealer_id');
				$this->db->where('dealer_isactive', ACTIVE);
				if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
					$this->db->where('dealer_id', $GLOBALS['ID']['sess_dealerid']);
				$query = $this->db->get('vts_dealer');
				return $query->result_array();
//		}
		}
		catch (Exception $ex)
		{
			log_message('error','get all dealer names - vts_dealer');
			return null;
		}
	}
	
// **********************  User Model for saving in to vts_user table come's below ********************

	/**
	 * Function to insert the data to User table from client form
	 *
	 * $user_client_id   : Client id as a forgin key
	 * $user_user_name   : Contact person name
	 * $user_email : user contact email
	 * $user_is_active  : client is active field value
	 *
	 * inserts the value to DB.
	 **/
	
	public function insert_user_value($u_client_id, $u_user_name, $u_email, $u_is_active, $u_time_zn_id, $u_phn, $u_type_id, $operation) {
	
		try {
			//Data to be inserted in the DB.
			$data = array(
					'user_client_id' => $u_client_id,
					'user_user_name' => $u_user_name,
					'user_email' => $u_email,
					'user_is_active' => $u_is_active,
					'user_is_client_admin' => '1',
					'user_is_admin' => '1',
					'user_time_zone_id' => $u_time_zn_id,
					'user_mobile' => $u_phn,
					'user_type_id' => $u_type_id
			);
			//Inserts the data to table EventLog
			if($operation == 'insert')
			{
				$data['user_password'] = md5(microtime() . rand());
				$this->db->insert('vts_users', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('user_client_id', $u_client_id);
				$this->db->update('vts_users', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('user_client_id', $u_client_id);
				$this->db->delete('vts_users');
				return true;
			}
				
		}  catch (Exception $e) {
	
			log_message('error, inserting to table - vts_users',$e->getMessage());
			return;
		}
	}

// **********************  clientLicense Model for saving in to vts_client_license table come's below ********************
	/*
	 * This function used to return the client detail relate the give parameter(i.e. clientID)
	 * if any error, then return null.
	 */
	public function get_vts_client_license($rowPerPage=null,$startNo=null,$client_license_client_id=null)
	{
		try
		{
			if($rowPerPage==null & $client_license_client_id==null)
			{
				$this->db->order_by('client_license_client_id','ASC');
				$this->db->select('*');
				$this->db->from('vts_client_license');
				$query = $this->db->get();
				return $query->result_array();
			}
			else if($client_license_client_id==null)
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('client_license_client_id','ASC');
				$this->db->select('*');
				$this->db->from('vts_client_license');
				$query = $this->db->get();
				return $query->result_array();
					
			}
			else
			{
				$query=$this->db->query("select * from vts_client_license where client_license_client_id=".$client_license_client_id." and client_license_start_date=(select max(client_license_start_date) from vts_client_license where client_license_client_id=".$client_license_client_id.")");
				return $query->row_array();
			}
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_client_license',$ex->getMessage());
			return null;
		}
	}
	
	
	public function insert_update_client_license_value($client_license_client_id, $client_license_start_date, $client_license_expiry_date, $client_license_users, $client_license_vehicles, $client_license_drivers, $operation) {
	
		try {
			//Data to be inserted in the DB.
			$data1 = array(
					'client_license_client_id' => $client_license_client_id,
					'client_license_start_date' => $client_license_start_date,
					'client_license_expiry_date' => $client_license_expiry_date,
					'client_license_users' => $client_license_users,
					'client_license_vehicles' => $client_license_vehicles,
					'client_license_drivers' => $client_license_drivers,
					'client_license_created_by_user_id' => '1', //this is logged in user id from sessions[session[$loggedinuserid]]
					'client_license_created_datetime' => date('Y/m/d H:i:s'),
					'client_license_is_active' => '1'
			);
			//Inserts the data to table EventLog
			if($operation == 'insert')
			{
				$this->db->insert('vts_client_license', $data1);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('client_license_client_id', $client_license_client_id);
				$this->db->update('vts_client_license', $data1);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('client_license_client_id', $client_license_client_id);
				$this->db->delete('vts_client_license');
				return true;
			}
				
		}  catch (Exception $e) {
	
			log_message('error, inserting to table - vts_users',$e->getMessage());
			return;
		}
	}
	
	/*
	 * This function is to get the user_id inorder to send the email after creating the user successfully from client screen
	 */
	public function get_user_id($usr_email)
	{
		try
		{
			$command = "select user_id from vts_users as user_id where user_email ='".$usr_email ."' " ;
			$query = $this->db->query($command);
			$row = $query->row_array();
			return $row['user_id'];
		}
		catch(Exception $ex)
		{
			log_message('error, getting user_id error - vts_users',$e->getMessage());
			return null;
		}
	
	}
	
	/*
	 * function to get all usernames from user table by passing email as paramater
	 */
	public function get_user_table_data($email)
	{
		try {
			$command = "select user_client_id, user_email from vts_users where user_email = '".$email."'";
			$query = $this->db->query ( $command );
			$row = $query->row_array ();
			if($row != null)
				return $row;
			else
				return array('user_client_id'=>"-1", 'user_email'=>"");
		}catch ( Exception $e ) {
			log_message ( 'error', 'get user email from vts_users table' );
			return null;
		}
	}
	
	/*
	 * This function is to update all the related user of the client(x)
	 * only the isactive field will be updated
	 */
	public function update_isactive_in_user($cnt_id,$isactive)
	{
		try{
			if($isactive == '1')
			{
				$command = "UPDATE vts_users SET user_is_active='1' WHERE user_is_active = '0' and user_client_id = '".$cnt_id."' and user_is_client_admin = '1'";
				$query = $this->db->query($command);
				return true;
			}
			else
			{
				$command = "UPDATE vts_users SET user_is_active='0' WHERE user_is_active = '1' and user_client_id = '".$cnt_id."'";
				$query = $this->db->query($command);
				return true;
			}
		}catch ( Exception $e ){
			log_message ( 'error', 'update isactive in vts_users table' );
			return null;
		}
	}
	
	/*
	 * This function is to get the Access Permissions List from the config
	 * and config-type tables
	 */
	public function get_accessPermissions()
	{
		try{
			$command="select ct.config_type_id, ct.config_type, c.config_id, c.config_item from vts_config_type ct, vts_config c where ct.config_type_id = c.config_type_id and ct.config_type = '".ACCESSPERMISSIONS."'";
			$query = $this->db->query($command);
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all AccessPermissions types - vts_config');
			return null;
		}
	}
	
	/*
	 * This function is used to Insert the client permissions list in the vts_client_permission table
	 */
	public function insert_update_clientPermissions($cnt_id, $listOfPermissions, $operation)
	{
		try
		{
			if($operation=="insert")
			{
				if($listOfPermissions!=null)
				{
					foreach ($listOfPermissions as $permission)
					{
						$data['client_id'] = $cnt_id;
						$data['permission'] = $permission;
						$this->db->insert('vts_client_permission', $data);
					}
				}
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('client_id', $cnt_id);
				$this->db->delete('vts_client_permission');
				if($listOfPermissions!=null)
				{
					foreach ($listOfPermissions as $permission)
					{
						$data['client_id'] = $cnt_id;
						$data['permission'] = $permission;
						$this->db->insert('vts_client_permission', $data);
					}
				}
				return true;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_client',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is used to get the client permission records from vts_client_permission
	 */
	public function get_vts_client_permissions($clientId)
	{
		try {
			$command = "select * from vts_client_permission where client_id='".$clientId."'";
			$query = $this->db->query($command);
			$result = $query->result_array();
			if($result != null)
				return $result;
			else
				return null;
		}catch ( Exception $e ) {
			log_message ( 'error', 'get client permissions from vts_client_permission table' );
			return null;
		}
	}
}
?>
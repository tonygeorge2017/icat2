<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_installation_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 * @param
	 *  $sessClientID - session client id
	 *  $clientID - md5 of client id
	 *  $rowPerPage - no of row display per page 
	 *  $startNo - starting row
	 *  $userId - user id
	 * Return type - result array
	 */
	public function get_deviceInstalledList($sessClientID, $clientID=null,$rowPerPage=null,$startNo=null,$userId=null)
	{
		try
		{
			$command="";
			if($clientID!=md5(AUTOGRADE_USER))
			{
				$command="select dev_install_id, dev_install_device_id, get_device_name(dev_install_device_id) as device_name, get_device_slno(dev_install_device_id) as device_slno,
dev_install_vehicle_id, get_vehicle_name(dev_install_vehicle_id) as vehicle_name, dev_install_installed_date::date,
dev_install_removed_date::date,dev_install_remarks,dev_install_client_id, dev_install_mobile_no as sim_no from vts_device_installation";
				if($sessClientID!=AUTOGRADE_USER)
					$command.=", vts_users";//if the user is not the autograde user then check with vts_user table also
				$command.=" where ";
				if($sessClientID!=AUTOGRADE_USER)
					$command.="user_id='".$userId."' and md5(user_client_id::varchar)= '".$clientID."' and ";
				
				$command.=" dev_install_removed_date is null and md5(dev_install_client_id::varchar)='".$clientID."'order by device_name";
				
				if($rowPerPage!=null)
					$command .= " limit ".$rowPerPage." offset ". $startNo;
				$command .= ";";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - user',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allClients(){
		try
		{
		/*	$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER." and client_dealer_id=".$GLOBALS['ID']['sess_dealerid']." order by client_name";
			log_message("debug","fffcommand===".$command);
			$query = $this->db->query($command);
			$result=$query->result_array();*/
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->db->where('client_dealer_id', $GLOBALS['ID']['sess_dealerid']);
			}
			$query = $this->db->get('vts_client');
			return $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get all the vehicle group for select clientId from the
	 * vts_client table to fill the drop down in user view.
	 * @param
	 *  $clientID - md5 of client id
	 *  $userID - user id
	 * Return type - result array
	 */
	public function get_allVehicleGroup($clientID=0, $userID=0, $vhGpID=null)
	{
		try
		{		
			if($GLOBALS['sessClientID']==AUTOGRADE_USER  || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
				$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and md5(vehicle_group_client_id::varchar)='".$clientID."'";
			else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
				$command="select vehicle_group_id, vehicle_group from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$userID."' and md5(vehicle_group_client_id::varchar)='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id";
			else
				$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."'";
			if($vhGpID!=null)
				$command.=" and vehicle_group_id='".$vhGpID."'";
			$command.=" order by vehicle_group;";
			//log_message("debug","command===".$command);
			$query = $this->db->query($command);
			$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	/*
	 * This function is used to get all the vehicle select vehicleGpId from the
	 * vts_client table to fill the drop down in user view.
	 * @param
	 *  $clientID - md5 of client id
	 *  $vehicleGpID - vehicle group id
	 *  $vhID - vehicle ID
	 * Return type - result array
	 */
	public function get_VehicleList($clientID, $vehicleGpID=null, $vhID=null)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{
				$command="select vehicle_id, vehicle_regnumber from vts_vehicle where vehicle_group_id='".$vehicleGpID."' 
and ";				
				$command.=($vhID==null)?"vehicle_id !=COALESCE((select dev_install_vehicle_id  from vts_device_installation where 
 dev_install_removed_date is null and dev_install_vehicle_id=vehicle_id limit 1),'-1')
and md5(vehicle_client_id::varchar)='".$clientID."' and vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;":"vehicle_id='".$vhID."';";
				//log_message("debug","hhh query===".$command);	
				$query = $this->db->query($command);
					$result=$query->result_array();
					if ($result != null)
						return $result;
					else
						return null;
			}
			else
				return null;
			
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle related to vehicle group');
			return null;
		}
	}
	/*
	 * This function is used to get all the device from the device table 
	 * to fill the drop down in user view.
	 * @param
	 *  $deviceType - device type
	 *  $clientDevice -  md5 of client id
	 * Return type - result array
	 */
	public function get_allDevice($deviceType=0,$clientDevice=null, $devID=null)
	{
		try
		{
			if($deviceType!=null && $clientDevice!=null)
			{
				$command="Select device_id, (device_imei||' ('||device_slno||')')::varchar as device_imei, device_slno from vts_device where"; 
				$command.=($devID==null)? " device_device_type_id='".$deviceType."' and md5(device_client_id::varchar)='".$clientDevice."' and device_is_active='".ACTIVE."'
and device_id !=COALESCE((select dev_install_device_id  from vts_device_installation where 
 dev_install_removed_date is null and dev_install_device_id=device_id limit 1),'-1')
 order by device_imei;":" device_id ='".$devID."';";
				//log_message('debug','***get_allDevice**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
			{
				return null;
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all device which is relted to the given device type');
			return null;
		}
	}
	/*
	 * This function is used to get all the device tyepe from the device type table 
	 * to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allDeviceType($p_deviceTypeID=null)
	{
		try
		{
			$command="Select device_type_id, device_type_name from vts_device_type";
			if($p_deviceTypeID!=null)
				$command.=" where device_type_id='".$p_deviceTypeID."'";
			$command.=" order by device_type_name;";
				$query = $this->db->query($command);
			$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all device type');
			return null;
		}
	}
	/*
	 * To edit or insert the device installation table
	 * @param
	 *  $installedID - md5 of installated id
	 *  $data -  associative array which hole new row column detils.
	 *  $operation - operation(i.e insert or edit)
	 *  $vhid - vehicle id
	 * Return type - bool
	 */
	public function InsertOrUpdate_device($installedID,$data,$operation,$vhid=null)
	{
		//log_message("debug", "inside insert---");
		try
		{
			if($operation=="insert")
			{
				if($this->db->insert('vts_device_installation', $data))
					return true;
				else
					return false;
			}
			else if($operation=="remove")
			{
				$removeDate=$data['dev_install_removed_date'];
				//change the date&time to removel date&time in trip register if the start or end date is greater than the device removel date&time.
				$command="update vts_trip_register set trip_register_time_end='".$removeDate."',trip_register_remarks='Trip Register date is change to device removed date and time'
						where trip_register_vehicle_id='".$vhid."' and (trip_register_time_end > '".$removeDate."' or trip_register_time_end is null);
								update vts_trip_register set trip_register_time_start='".$removeDate."',trip_register_remarks='Trip Register date is change to device removed date and time'
						where trip_register_vehicle_id='".$vhid."' and trip_register_time_start > '".$removeDate."';";
				$this->db->query($command);
				$this->db->where('md5(dev_install_id::varchar)', $installedID);
				if($this->db->update('vts_device_installation', $data))
					return true;
				else
					return false;
			}
			else if($operation=="edit")
			{
				$this->db->where('md5(dev_install_id::varchar)', $installedID);
				if($this->db->update('vts_device_installation', $data))
					return true;
				else
					return false;
			}
		}catch (Exception $ex)
		{
			log_message('error insert or update  - vts_device_installation',$ex->getMessage());
			return false;
		}
	}
	/*
	 * This function is used to find whether the device is removed one or not
	 * @param
	 *  $deviceId - device id
	 *  $vehicle -  vehicle id.
	 * Return type - integer
	 */
	public function is_device_exist($deviceId, $vehicle)
	{
		$command="Select dev_install_id from vts_device_installation where dev_install_removed_date is null and dev_install_device_id='".$deviceId."' and dev_install_vehicle_id='".$vehicle."'";
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row['dev_install_id'];
		else
			return null;
	}
	/*
	 * This function is used to get the selected client time difference
	 * @param
	 *  $clientId - md5 of client id
	 * Return type - string
	 */
	public function get_time_diff($clientId)
	{
		$command="select time_zone_diff from vts_client, vts_time_zone where time_zone_id=client_time_zone_id and md5(client_id::varchar)='".$clientId."'";
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row['time_zone_diff'];
		else
			return null;
	}
	/*
	 * This function is used to get the details about the installed device and vehicle 
	 * for given installation id
	 * @param
	 *  $installationId - md5 of installation id
	 * Return type - row array
	 */
	public function get_install_vehicle_details($installationId)
	{
		$command="select device_device_type_id, dev_install_id, dev_install_device_id, dev_install_installed_date::date, 
				dev_install_remarks, dev_install_client_id, dev_install_sim_provider, dev_install_is_atc_sim,
				dev_install_mobile_no, dev_install_vehicle_id vehicle_id, 
				get_vehicle_name(dev_install_vehicle_id) as vehicle_name,
				get_device_name(dev_install_device_id) as device_name,  
				get_client_name(dev_install_client_id) as client_name,
				get_vehicle_gp_id(dev_install_vehicle_id) as vh_gp_id from vts_device_installation, vts_device 
				where device_id=dev_install_device_id and md5(dev_install_id::varchar)='".$installationId."';";
		//log_message('debug','***get_install_vehicle_details****'.$command);
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row;
		else
			return null;
	}
}
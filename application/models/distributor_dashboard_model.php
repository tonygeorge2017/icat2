<?php
class Distributor_dashboard_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	//rest of code come here
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_all_distributor() {
		try {
			$command = "select dist_id, dist_name from vts_distributor order by dist_name";
			$query = $this->db->query ( $command );
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		} catch ( Exception $ex ) {
			log_message ( 'error', 'get all client name - vts_client' );
			return null;
		}
	}
	/* 
	 * function to get purchase details and sales details of devices belongs to logged in distributor.
	 */
	public function get_device_details_this_yr()
	{
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$logged_in = $GLOBALS ['ID'] ['sess_distributorid'];
		//$year =  date("Y");
		//$year1 = date("Y-01-01");
	//	$current_date = date("Y-m-d");
		
		//$command = "select count(*) from vts_device where device_distributor_id=".$logged_in.";";
		$command = "select count(*) from vts_device where device_distributor_id=".$logged_in." and 
					device_release_distributor_date between (date_part('year',current_date)||'-01-01')::date 
				    and current_date;";//used for this year
		$query = $this->db->query ( $command );
		//log_message("debug","---query---".$command);
		
		$result = $query->row_array ();
		//log_message("debug","---query res---".print_r($result,true));
		return $result['count'];
	}
	/*
	 * function to get details purchase div for distributor
	 */
	public function get_device_purchase_table_m()
	{
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$logged_in = $GLOBALS ['ID'] ['sess_distributorid'];
		$command = "select vdv.device_imei,vdv.device_is_active,vdv.device_dealer_id,vd.dealer_name,
					vdv.device_release_dealer_date,vdv.device_remarks from vts_device vdv
					left outer join vts_dealer vd on vd.dealer_id = vdv.device_dealer_id
					where device_distributor_id=".$logged_in." and 
					device_release_distributor_date between (date_part('year',current_date)||'-01-01')::date 
					and current_date;";//year
		$query = $this->db->query ( $command );
		//log_message("debug","---query---".$command);
		
		$result = $query->result_array ();
		//log_message("debug","---query res---".print_r($result,true));
		return $result;
	}
}
?>
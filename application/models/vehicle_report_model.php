<?php

class Vehicle_report_model extends CI_Model{

	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS['sess_usr_id'] = $GLOBALS ['ID']['sess_userid'];
		$GLOBALS['sess_clnt_id'] = $GLOBALS['ID']['sess_clientid'];
	}
	
	/*
	 * This function is to get all the clients from client table and shown
	 * client name in drop down in vehicle report screen
	 */
	public function get_allClients()
	{
		try{
			$this->db->order_by('client_name','ASC');
			$this->db->select('client_id, client_name');
			$this->db->where('client_id !=', AUTOGRADE_USER);
			$query = $this->db->get('vts_client');
			return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all client names - vts_client');
			return null;
		}
	}

	/*
	 * This function is used to get the vehiclegroup from the
	 * vehicle group table to fill the vehicle group dropDownBox in Vehicle Report screen view.
	 */
	public function get_allVehicleGroups($clientID){
		try
		{
			$command = "select * from get_vts_vehicle_groups_for_userid(".$GLOBALS['sess_usr_id'].",". $clientID.")";
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($query != null)
				return $result=$query->result_array();
			else
				return null;
	
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group name - vts_vehicle_group');
			return null;
		}
	}
	
	/*
	 * This function is to get all the vehicles from vehicle table and shown
	 * client name in drop down in vehicle service screen
	 */
	public function get_allVehicles($vehicleGpID=null)//$session_login_client_id,
	{
		try
		{
			if($vehicleGpID!=null)
			{
				$this->db->order_by('vehicle_regnumber','ASC');
				$this->db->select('vehicle_id, vehicle_regnumber');
				$this->db->where('vehicle_isactive', ACTIVE);
				$this->db->where('vehicle_group_id', $vehicleGpID);

				$query = $this->db->get('vts_vehicle');
				return $query->result_array();
			}return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehical names - vts_vehicle');
			return null;
		}
	}
	
	/*
	 * This function is to get the values from the get_vehicle_run_data table
	 */
	public function get_values($cnt_id, $data)
	{
		try{
			$query = " select * from get_vehicle_run_data(".$cnt_id.",".$GLOBALS['sess_usr_id'] ;
			
			$query .= ", '".$data ['from_date'] ." 00:00:00'";
			$query .= ", '".$data ['to_date'] ." 23:59:59'";
			$query .= ", " . $data ['vehicle_gp_id'].", '";

			if(count($data['vehicle_id']) == 1)
				$query .= $data['vehicle_id'][0];
			else
			{
				for ($i=0;$i<count($data['vehicle_id'])-1 ;$i++)
					$query .=  $data['vehicle_id'][$i].",";
							$query .=  $data['vehicle_id'][count($data['vehicle_id'])-1];
			}
				$query .= "'";
			$query .= ")";
			$query .= " order by vehicle, from_datetime";
			//log_message("debug","****XX**$query");
			$query_final = $this->db->query($query);
			
			return $query_final->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get_values');
			return null;
		}
	}
	

	/*
	 * This is used to save and delete the data into vts_tmp_report_vehicledetails table, once we click the 'GO' button
	 */
	public function InsertOrdelete_vts_tmp_report_vehicledetails($data, $operation)
	{
		try
		{
			if($operation=="insert")
			{
				foreach ($data as $data1)
				{
					$data2['user_id'] = $GLOBALS['sess_usr_id'];
					$data2['imeino'] = $data1['imeino'];
					$data2['from_datetime'] = $data1['from_datetime'];
					$data2['to_datetime'] = $data1['to_datetime'];
					$data2['duration'] = $data1['duration'];
					$data2['status'] = $data1['status'];
					$data2['distance'] = $data1['distance'];
					$data2['vehiclename'] = $data1['vehicle'];
					$data2['drivername'] = $data1['driver'];
					$data2['vehicle_id'] = $data1['vehicle_id'];
					$data2['driver_id'] = $data1['driver_id'];
					
					$this->db->insert('vts_tmp_report_vehicledetails', $data2);
				}
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('user_id', $GLOBALS['sess_usr_id']);
				if($this->db->delete('vts_tmp_report_vehicledetails'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_tmp_report_vehicledetails',$ex->getMessage());
			return false;
		}
	}

	/*
	 * This function is to get all the Vehicel Reports from the vts_tmp_report_vehicledetails table and send to the CSV and PDF files 
	 */
	public function get_allVehicleReports()
	{
		try
		{
			$command = "select vehiclename, drivername, from_datetime, to_datetime, duration, status, distance from vts_tmp_report_vehicledetails where user_id = ".$GLOBALS['sess_usr_id']." order by vehiclename, from_datetime ;"; //from_datetime, to_datetime, ignition_status, status_duration, travel_distance
			$query_final = $this->db->query($command);
			return $query_final->result_array();
		}
		catch(Exception $ex)
		{
			log_message('error','get all Vehicle Report - vts_tmp_report_vehicledetails');
			return null;
		}
	}

	/*
	 * This function is to get all vehicle names from vts_vehicle table and display in the dropdown
	 */
	public function getVehicleName($vcle_id)
	{
		try{
			$command = "select vehicle_regnumber from vts_vehicle where vehicle_id =". $vcle_id;
			$query_final = $this->db->query($command);
			$row = $query_final->row_array();
			if($row!=null)
				return $row['vehicle_regnumber'];
			else
				return  null;
		}
		catch(Exception $ex)
		{
			log_message('error', 'get all vehicle name');
			return null;
		}
	}
	
	/*
	 * This function is to get all Vehicle report records to display the grid in view
	 */
	public function get_allDataToPlot($arr_data) {
		try {
			$cnt_id = $arr_data['client_id'];
			$vhcl_grp = $arr_data['vehicle_group'];
			$vhcl = $arr_data['vehicle'];
			
			$from_date = date('Y-m-d', strtotime($arr_data['from_date']));
			$to_date = date('Y-m-d', strtotime($arr_data['to_date']));
			
// 			$command = "select client_id, date, sum(distance) as distance from vts_daily_data where CAST(date AS TEXT) = '$from_date' and client_id = '$cnt_id' group by client_id, date order by client_id, date;";
// 			$command = "select client_id, date, sum(distance) as distance from vts_daily_data where CAST(date AS TEXT) LIKE '2015-05%' and client_id = '3' group by client_id, date order by client_id, date";
			
			$command = "select client_id, date, vehicle_group_id, sum(distance) as distance from vts_daily_data where CAST(date AS TEXT) BETWEEN '$from_date' AND '$to_date' and client_id = '$cnt_id' and vehicle_group_id = '$vhcl_grp'";

			if(count($vhcl)>1)
			{
				$command .= " and vehicle_id IN(";
				foreach($vhcl as $vh)
				{
					$vh_id = $vh['vh_link_vehicle_id'];
					$command .= $vh_id.',';
				}
				$command .= " 0)";
			}
			else 
			{
				$vhcl_id = $vhcl['0']['vh_link_vehicle_id'];
				$command .=	" and vehicle_id ='$vhcl_id'";
			}
			$command .= " group by client_id, vehicle_group_id, date order by client_id, date;";
			
				
			$query_final = $this->db->query ( $command );
			return $query_final->result_array ();
		} catch ( Exception $ex ) {
			log_message ( 'error', 'get all vehicle report' );
			return null;
		}
	}
	public function get_valuesToPlot($data)
	{
		try{
			$query = " select sum(distance) distance,from_datetime::date as date from get_vehicle_run_data(".$data['client_id'].",".$GLOBALS['sess_usr_id'] ;
				
			$query .= ", '".$data ['from_date'] ." 00:00:00'";
			$query .= ", '".$data ['to_date'] ." 23:59:59'";
			$query .= ", " . $data ['vehicle_group'].", '";
			$vhcl = $data['vehicle'];
			if(count($vhcl) == 1)
				$query .= $data[0]['vh_link_vehicle_id'];
			else
			{
				for ($i=0;$i<count($vhcl)-1 ;$i++)
					$query .=  $vhcl[$i]['vh_link_vehicle_id'].",";
					$query .=  $vhcl[count($vhcl)-1]['vh_link_vehicle_id'];
			}
			$query .= "'";
			$query .= ")";
			$query .= " group by date order by date;";
			//log_message("debug","****XXCCC**$query");
				$query_final = $this->db->query($query);
					
				return $query_final->result_array();
		}
		catch (Exception $ex)
		{
		log_message('error','get_values');
			return null;
		}
	}
	
}
?>
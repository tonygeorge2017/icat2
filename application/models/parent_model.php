<?php

class Parent_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}	
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allClients(){
		try{
			$command="select client_id, client_name from vts_client where client_id <>'".AUTOGRADE_USER."' order by client_name";
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get the list of route
	 * @param
	 * 	$clientID - selected client id
	 * 	$sessClient -  session client id
	 *  $sessUser - session user id
	 * Return type - result array
	 */
	public function get_route($clientID=null)
	{
		try
		{
			if($GLOBALS['sessClientID']!=null)
			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
					$command="select route_id r_id, route_name r_val from vts_route where route_is_active='".ACTIVE."' and md5(route_client_id::varchar)='".$clientID."' order by route_name;";				
				else
					$command="select route_id r_id, route_name r_val from vts_route where route_is_active='".ACTIVE."' and route_client_id='".$GLOBALS['sessClientID']."' order by route_name;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}	

	/*
	 * This function is used to fetch the stop detail
	 * to fill the dropdown list in view.
	 * @param
	 *  $routeID - route id
	 * Return type - result array
	 */
	public function get_route_stop($routeID)
	{
		try
		{
			if($routeID!=null)
			{				
				$command="Select stop_id, stop_name, route_name from vts_route_stop inner join vts_stops_list on stop_id=rt_stp_stop_id inner join vts_route on route_id=rt_stp_route_id where stop_is_busstop='1' and route_is_geofence='0' and route_id=$routeID order by route_name, stop_name;";
				$query=$this->db->query($command);
				$result=$query->result_array();
				$result=($result!=null)?$result:null;
				return $result;
			}
			else
				return null;
				
		}catch (Exception $e)
		{
			log_message("Error","get_vh_stop");
		}
	}
	/*
	 * This function use to get the parents detail list for given parameter
	 * @param
	 *  $p_clientID - client id
	 *  $p_stopID - stop id
	 * Return type - result array
	 */
	public function get_parent_list($p_clientID, $p_stopID=null, $p_search=null, $rowPerPage=null, $startNo=null)
	{
		try{			
			$p_stopID=(isset($p_stopID))?$p_stopID:'0';
			$command="SELECT parent_id, parent_name, parent_mobile, parent_email, count(pt_stop_parent_id) as child_count
FROM vts_parent left outer join vts_parent_stop on parent_id=pt_stop_parent_id where md5(parent_client_id::varchar)='".$p_clientID."' 
group by parent_id, parent_name, parent_mobile, parent_email order by parent_name";
			if($rowPerPage!=null)
				$command.=" limit ".$rowPerPage." offset ".$startNo;
			$command.=";";
			$query=$this->db->query($command);
				$result=$query->result_array();
				$result=($result!=null)?$result:null;
				return $result;			
		}
		catch(Exception $e)
		{
			log_message('Error','get_parent_list');
		}
	}
	/*
	 * This function is used to insert or edit the details
	 * of the parent
	 * @param
	 *  $p_data - array of value to inser or update
	 *  $p_parentID - parent id used only for edit
	 *  $p_opt - insert or edit
	 * Return type - integer (i.e. currently inserted row id or status)
	 */
	public function insert_edit_parent($p_data, $p_parentID=null, $p_opt="insert",$p_isUserTableData=false)
	{
		try{
			if($p_opt == "insert" && $p_parentID == null)
			{
				$status=0;
				if(!$p_isUserTableData)
				{
					$status=$this->db->insert('vts_parent', $p_data);
					$command="SELECT currval(pg_get_serial_sequence('vts_parent', 'parent_id')) as id;";
				}
				else
				{
					$status=$this->db->insert('vts_users', $p_data);
					$command="SELECT currval(pg_get_serial_sequence('vts_users', 'user_id')) as id;";
				}
				if($status == 1)
				{					
					$query=$this->db->query("$command");
					$result=$query->row_array();
					$status=$result['id'];
				}
				
			}else if($p_opt == "edit" && $p_parentID != null)
			{
				$this->db->where('parent_id', $p_parentID);
				$status=$this->db->update('vts_parent', $p_data);
			}
			return $status;
		}catch(Exception $ex)
		{
			log_message('error',"Error on inserting data in vts_parent table.");
		}
	}
	/*
	 * This function is used to insert or edit the student(vts_parent_stop table) details
	 * of the parent
	 * @param
	 *  $p_data - array of value to inser or update
	 *  $p_stopID - parent id used only for edit
	 *  $p_opt - insert or edit
	 * Return type - integer (i.e. currently inserted row id or status)
	 */
	public function insert_edit_parent_stop($p_data, $p_stopID=null, $p_opt="insert")
	{
		try{
			$status=false;
			if($p_opt == "insert" && $p_stopID == null)
			{
				$status=$this->db->insert('vts_parent_stop', $p_data);
				if($status == 1)
				{
					$status =  true;
					if($p_data['pt_stop_student_rfid'])
					{						
						$this->db->where('rf_id',$p_data['pt_stop_student_rfid']);
						$this->db->update('vts_rf_card', array('is_assigned' => '1' ));
					}
				}
	
			}else if($p_opt == "edit" && $p_stopID != null)
			{
				$this->db->where('pt_stop_id', $p_stopID);
				$status=$this->db->update('vts_parent_stop', $p_data);
			}
			return $status;
		}catch(Exception $ex)
		{
			log_message('error',"Error on inserting data in vts_parent_stop table.");
		}
	}	
	/*
	 * This function is used to get parent details
	 * @param
	 *  $parentID - parent id
	 * Retyrn type - result array
	 */
	public function get_parent_detail($parentID)
	{
		try{
			if($parentID!=null)
			{
				$command="SELECT parent_id, parent_name, parent_mobile, parent_email, parent_password, parent_client_id, parent_is_active 
						FROM vts_parent where md5(parent_id::varchar)='".$parentID."' order by parent_name;";
				$query=$this->db->query($command);
				
				if ($query != null)
					return $query->row_array ();
				else
					return null;
				
			}else
				return null;
			
		}catch(Exception $ex)
		{
			log_message('error','Error while fetching parent details from parent table.');
		}
	}
	/**
	 * This function is used to get the parent details while typing mobile no	 * 
	 * @param   $mobile mobile no.	 
	 * @name get_existing_parent
	 */	
	public function get_existing_parent($client, $mobile, $sel_client)
	{
		$result=array();
		if($client && $mobile)
		{
			$sql="SELECT parent_id, parent_name, parent_mobile, parent_email, parent_client_id, parent_is_active FROM vts_parent where parent_mobile like '$mobile%' and";
			$sql.=($client==AUTOGRADE_USER)?" parent_client_id=$sel_client" : " parent_client_id=$client";
			$sql.=" order by parent_name";

			$query=$this->db->query($sql);
			if($query)
				$result=$query->result_array();
		}
		return $result;
	}
	
	/*
	 * This function is used to get student details
	 * @param
	 *  $p_parentID - parent id
	 *  $p_stopID - stop id of parent stop table
	 * Retyrn type - result array(if only parent id) OR row array(if parent is and stop id)
	 */
	public function get_student_detail($p_parentID,$p_stopID=null)
	{
		try{
			if($p_parentID!=null)
			{
				$command="SELECT pt_stop_id, pt_stop_parent_id, pt_stop_vh_stop_id, pt_stop_student_name, pt_stop_student_class, pt_stop_isactive, pt_stop_student_id, pt_stop_student_rfid, stop_name, rt_stp_route_id, pt_notify_rfid, pt_notify_route_violation, pt_notify_over_speed FROM vts_parent_stop inner join vts_stops_list on stop_id = pt_stop_vh_stop_id inner join vts_route_stop on rt_stp_stop_id=pt_stop_vh_stop_id where md5(pt_stop_parent_id::varchar)='".$p_parentID."'";
				if($p_stopID!=null)
					$command.=" and md5(pt_stop_id::varchar)='".$p_stopID."'";
				$command.=";";
				$query=$this->db->query($command);	
				if ($query != null && $p_stopID==null)
					return $query->result_array ();
				else if ($query != null && $p_stopID!=null)
					return $query->row_array ();
				else
					return null;
			}else
				return null;
				
		}catch(Exception $ex)
		{
			log_message('error','Error while fetching student details from parent stop table.');
		}
	}
	/*
	 * This function is used to check the email is alreatd is exist
	 * in the parent table or not.
	 * @param
	 *  $p_email - parent email id
	 *  $p_parent_id - parent id
	 * Return Type - bool
	 */
	public function check_mail_id($p_email, $p_parent_id=null)
	{
		try
		{
			if($p_email!=null)
			{
				$command="Select user_email from vts_users where upper(user_email)='".strtoupper($p_email)."'";
				if($p_parent_id!=null)
					$command.= "and user_parent_id <> '".$p_parent_id."'";
				$command.=";";				
				$query=$this->db->query($command);
				$result=$query->result_array();
				if(count($result)>=1)
					return true;
				else 
					return false;
				
			}else{
				return false;
			}
			
		}catch(Exception $ex)
		{
			log_message('error','Error while checking the given mail is already exist in parent table or not.');
		}	
	}
	/*
	 * This function used to check the uesr type duplicate name
	 * of the childrens.
	 * @param
	 *  $p_studentname - name of the student
	 *  $p_parent_id - parent id
	 *  $p_studentid - vts_parent_stop table running no.
	 * Return Type - bool
	 */
	public function check_stud_name($p_studentname,$p_parent_id,$p_studentid=null)
	{
		try
		{
			if($p_studentname!=null && $p_parent_id!=null)
			{
				$command="Select pt_stop_student_name from vts_parent_stop where upper(pt_stop_student_name)='".strtoupper($p_studentname)."' and pt_stop_parent_id='".$p_parent_id."'";
				if($p_studentid!=null)
					$command.= " and pt_stop_id <> '".$p_studentid."'";
				$command.=";";
				$query=$this->db->query($command);
				$result=$query->result_array();
				if(count($result)>=1)
					return true;
				else
					return false;
	
			}else{
				return false;
			}
				
		}catch(Exception $ex)
		{
			log_message('error','Error while checking the given student name is already exist in vts_parent_stop  table for same parent or not.');
		}
	}

	/**
	 * This function get the 
	 * 
	 */
	public function get_vts_rf_card($c_id, $key='auto', $key_val=null, $old_id=null){
		$result=array();

		$sql="SELECT rf_id, rf_card_no FROM vts_rf_card where";
		$sql.=($key=='auto')?" rf_card_no LIKE '%$key_val%'" : " rf_id=$key_val";
		$sql.=" and md5(rf_client::varchar)='$c_id'";
		$sql.=($old_id)?" and (is_assigned='0' OR rf_id = $old_id)":" and is_assigned='0'";
		$sql.=" order by rf_card_no";
		
		$query=$this->db->query($sql);

		if($query && $key=='auto')
		{
			$result=$query->result_array();
		}
		else if($query && $key=='id')
		{
			$result=$query->row_array();
			$result=$result['rf_card_no'];
		}
		return $result;
	}

	/*
	 * This function is used to get the client dealer and distributor
	 */
	public function find_MoreDetails($id)
	{
		try
		{
			$command="select parent_id,client_id,client_dealer_id, dealer_distributor_id, client_time_zone_id  from vts_client left outer join vts_dealer on dealer_id=client_dealer_id
left outer join vts_parent on client_id=parent_client_id where ".$id." limit 1 offset 0;";
			
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
	
		}catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}	
}
?>
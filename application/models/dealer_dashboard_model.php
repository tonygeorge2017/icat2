<?php
class Dealer_dashboard_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	//rest of code shud come here
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_all_dealers() {
		try {
			$command = "select dealer_id, dealer_name from vts_dealer order by dealer_name";
			$query = $this->db->query ( $command );
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		} catch ( Exception $ex ) {
			log_message ( 'error', 'get all client name - vts_client' );
			return null;
		}
	}
}
?>

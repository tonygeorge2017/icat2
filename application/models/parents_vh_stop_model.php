<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Parents_vh_stop_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allClients(){
		try{
			$command="select client_id, client_name from vts_client where client_id <>'".AUTOGRADE_USER."' order by client_name";
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle group the given parameter
	 * @param
	 * 	$clientID - selected client id
	 * 	$sessClient -  session client id
	 *  $sessUser - session user id
	 * Return type - result array
	 */
	public function get_all_vhGp($clientID=null)
	{
		try
		{
			if($GLOBALS['sessClientID']!=null)
			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$clientID."' order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and vehicle_group_client_id='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";				
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle for the given vehicle group
	 * @param
	 * 	$vehicleGpID - selected vehicel group id
	 * Return type - result array
	 */
	public function get_all_vhGp_vehicle($vehicleGpID)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{
				$command="select vehicle_id, vehicle_regnumber from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and  vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;";
				$query = $this->db->query($command);			
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to fetch the stop detail
	 * to fill the dropdown list in view.
	 * @param
	 *  $vhID - vehicle id
	 * Return type - result array
	 */	
	public function get_vh_stop($vhID)
	{
		try
		{
			if($vhID!=null)
			{
				$command="select stop_id, stop_name, vh_vehicle_id from vts_stops_list,vts_vehicle_stops where vh_stops_id=stop_id and vh_vehicle_id='".$vhID."' order by stop_name;";
				$query=$this->db->query($command);
				$result=$query->result_array();
				$result=($result!=null)?$result:null;
				return $result;
			}
			else
				return null;
			
		}catch (Exception $e)
		{
			log_message("Error","get_vh_stop");
		}		
	}
	/*
	 * This function use to get the parents detail list for given parameter
	 * @param
	 *  $p_clientID - client id
	 *  $p_stopID - stop id
	 * Return type - result array
	 */
	public function get_parent_list($p_clientID, $p_stopID, $p_search=null)
	{
		try{			
			$p_stopID=(isset($p_stopID))?$p_stopID:'0';
			$command="select parent_id, parent_name, parent_student_name, parent_student_class, parent_client_id, pt_stop_parent_id from vts_parent
					left outer join vts_parent_stop on parent_id=pt_stop_parent_id and pt_stop_vh_stop_id='".$p_stopID."' where parent_is_active='".ACTIVE."' and parent_client_id='".$p_clientID."'";
			if($p_search!=null)
				$command.=" and (upper(parent_name) like upper('%".$p_search."%') or upper(parent_student_name) like upper('%".$p_search."%'))";
			$command.=" order by parent_student_name;";
			$query=$this->db->query($command);
				$result=$query->result_array();
				$result=($result!=null)?$result:null;
				return $result;
			
		}catch(Exception $e)
		{
			log_message('Error','get_parent_list');
		}
	}
	/*
	 * This function is used to insert the selected parent stop detail
	 * into vts_parent_stop table
	 * @param
	 *  $p_stopID - stop id
	 *  $p_parentList - array of parent id
	 * Return type - bool
	 */
	public function save_parent_stop($p_stopID,$p_parentList)
	{
		try
		{
			$l_isSuccess=false;
			$this->db->delete('vts_parent_stop', array('pt_stop_vh_stop_id' => $p_stopID));
			$l_isSuccess=true;
			if(!empty($p_parentList))
			{
				$l_isSuccess=false;
				foreach ($p_parentList as $parentId)
				{
					$data = array('pt_stop_parent_id' => $parentId,'pt_stop_vh_stop_id' => $p_stopID);
					$this->db->insert('vts_parent_stop', $data);
				}
				$l_isSuccess=true;
			}
			return $l_isSuccess;
		}
		catch(Exception $e)
		{
			log_message('Error','save_parent_stop');
			return false;
		}
		
	}
}
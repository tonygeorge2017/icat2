<?php

class Parent_dashboard_model extends CI_Model{	

	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');				
	}

	public function get_run_data()
	{
		$result = array('crnt' => array(), 'hist' => array(), 'pickup_alert' => array(), 'speed_alert' => array());
		$curr_utc_datetime = gmdate('Y-m-d H:i:s');	
		$date_value=new DateTime();
		$notify_date = $date_value->modify("-7 days")->format('Y-m-d H:i:s');
		$time_diff = $GLOBALS['ID']['sess_time_zonediff'];
		$clid = $GLOBALS ['ID'] ['sess_clientid'];
		$pid = $GLOBALS ['ID'] ['sess_parentid'];
		
		$live = "SELECT vehicle_id vhid, vehicle_regnumber vh_reg, vehicle_speedlimit splt, client_dashboard_lost lost, pt_stop_id stu_id, pt_stop_student_name st_name, pt_stop_student_class stu_class, pt_stop_student_rfid stu_rfid, stop_name s_name, stop_latitude s_lat, stop_longitude s_lng, to_char((live_datetime + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH24:MI:SS') as rec, extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - live_datetime::TIMESTAMP))/60 as diff, live_latitude lat, live_longitude lng, live_angleorientationcog ang, live_speed sp, live_digitalinputstatus ig,vehicle_idealspeed	haltspeed FROM vts_vehicle INNER JOIN vts_client on client_id=vehicle_client_id INNER JOIN vts_vehicle_route ON vh_rt_vehicle_id = vehicle_id INNER JOIN vts_route_stop ON rt_stp_route_id = vh_rt_route_id INNER JOIN vts_parent_stop ON pt_stop_vh_stop_id = rt_stp_stop_id INNER JOIN vts_stops_list ON stop_id = rt_stp_stop_id INNER JOIN live_gps_data ON live_vehicle_id=vehicle_id WHERE vehicle_isactive = '1' AND client_id = $clid AND pt_stop_parent_id = $pid order by live_vehicle_regnumber;";		
		//log_message('debug',$live);
		$live_query=$this->db->query($live);
		if($live_query)
			$result['crnt'] = $live_query->result_array();
		
		$history = "Select rfid_student st, driver_id dr, to_char((rfid_gps_timestamp + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH24:MI:SS') as tm, rfid_vehicle vh, live_vehicle_regnumber vhn, driver_name drname, driver_mobile drmobile, pt_stop_student_name stname, pt_stop_student_class stclass, gps_latitude swip_lat, gps_longitude swip_lng from vts_rfid_log inner join vts_parent_stop on pt_stop_id = rfid_student inner join server_gps_data on gps_data_id=rfid_gps_log_id left join live_gps_data on live_vehicle_id = rfid_vehicle left join vts_driver on driver_id = live_driver_id WHERE pt_stop_parent_id = $pid AND ((rfid_added_on::date = now()::date AND gps_fix = 'V') OR ((rfid_gps_timestamp + interval '1 hour' * $time_diff)::date = now()::date AND gps_fix = 'A')) order by rfid_gps_timestamp asc;";		
		//log_message('debug',$history);

		$history_query=$this->db->query($history);
		if($history_query)
			$result['hist'] = $history_query->result_array();		
		
		$pickup = "Select rfid_student st, live_driver_id dr, to_char((rfid_gps_timestamp + interval '1 hour' * $time_diff), 'DD-MM-YYYY  HH24:MI:SS') as tm, rfid_vehicle vh, live_vehicle_regnumber vhn, driver_name drname, driver_mobile drmobile, pt_stop_student_name stname, pt_stop_student_class stclass from vts_rfid_log inner join vts_parent_stop on pt_stop_id = rfid_student left join live_gps_data on live_vehicle_id = rfid_vehicle left join vts_driver on driver_id = live_driver_id WHERE pt_stop_parent_id = $pid order by rfid_gps_timestamp desc limit 10;";		
		//log_message('debug',$pickup);		
		$pickup_query=$this->db->query($pickup);		
		if($pickup_query)
			$result['pickup_alert'] = $pickup_query->result_array();	
		
		$speed = "SELECT pt_stop_student_name stname, notify_vehicle_id n_vhid, driver_name dr, driver_mobile drmobile, notify_msg msg, notify_datetime dt FROM vts_rfid_log inner join vts_notification_history on rfid_vehicle = notify_vehicle_id AND notify_datetime::date = rfid_gps_timestamp::date inner join vts_parent_stop on pt_stop_id = rfid_student left join vts_driver on driver_id = notify_driver_id WHERE pt_stop_parent_id = $pid AND notify_type = 'Over Speed' AND notify_datetime > '$notify_date' order by notify_datetime desc limit 20;";		
		//log_message('debug',$speed);		
		$speed_query=$this->db->query($speed);		
		if($speed_query)
			$result['speed_alert'] = $speed_query->result_array();	
		
		return $result;
	}
	
	public function get_alert_list()
	{
		$result = array();
		$pid = $GLOBALS ['ID'] ['sess_parentid'];
		$sql = "SELECT user_id, fcm_alert_route, fcm_alert_speed, fcm_alert_rfid from vts_users where user_parent_id = $pid";
		$speed_query=$this->db->query($sql);		
		if($speed_query)
			$result = $speed_query->result_array();		
		return $result;
	}
	
	public function update_alert_list($data)
	{
		$pid = $GLOBALS ['ID'] ['sess_parentid'];
		$this->db->where("user_parent_id", $pid);
		$test = $this->db->update('vts_users', $data);	
		return $test;
	}

}
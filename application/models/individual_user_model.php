<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Individual_user_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['sessionData'] = $this->session->userdata('login');		
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];		
	}
	
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 */
	public function get_userList($rowPerPage=null,$startNo=null,$userID=null,$userEmail=null)
	{
		try
		{	
			if($userEmail!=null){
				$query = $this->db->query("select user_id from vts_users where upper(user_email)='".$userEmail."';");
				$result = $query->row_array();
				if ($result != null)
					return $result['user_id'];
				else
					return null;
			}
			elseif($userID==null && $GLOBALS['sessClientID']==AUTOGRADE_USER)//Admin(Autograde user)
			{
				$command="select user_id, user_client_id, user_user_name, user_email, user_is_admin from vts_users where user_is_client_admin!='1' and  user_client_id='".INDIVIDUAL_CLIENT."' order by user_user_name";
				if($rowPerPage!=null)
					$command .= " limit ".$rowPerPage." offset ". $startNo;
				$query=$this->db->query($command);				
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}			
			else
			{
				$query=$query=$this->db->query("select user_id, user_client_id, user_user_name, user_email, user_mobile, user_time_zone_id, user_is_active, user_is_client_admin, user_is_admin from vts_users where md5(user_id::varchar)='".$userID."'  and user_client_id ='". INDIVIDUAL_CLIENT."'");
				$result = $query->row_array();
				if ($result != null)
					return $result;
				else
					return null;
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - user',$ex->getMessage());
			return null;
		}
	}	
	
	
	/*
	 * This function is user to given individual user device installation details
	 */
	public function  get_individualUserDeviceDetails($userID,$userDeviceID=null,$rowPerPage=null,$startNo=null)
	{
		try{
			if($userID!=null)
			{			
				$command="Select user_dev_id, user_dev_user_id, device_dealer_id, user_dev_device_id, get_device_name(user_dev_device_id)as device_imeino, 
user_dev_vehicle_id,get_vehicle_name(user_dev_vehicle_id)as vehicle_name, vehicle_speedlimit, user_dev_install_date::date, user_dev_expire_date::date 
from vts_user_device,vts_vehicle,vts_device where md5(user_dev_user_id::varchar)='".$userID."' and vehicle_id=user_dev_vehicle_id and device_id=user_dev_device_id";
				if($userDeviceID!=null)
					$command.=" and md5(user_dev_id::varchar)='".$userDeviceID."'";
				else if($rowPerPage!=null)
					$command.=" limit ".$rowPerPage." offset ".$startNo;				
				$command.=";";
				//log_message("error",'**Edit Device Installation Details**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else 
				return null;
		}catch (Exception $ex)
		{
			log_message('error, get all vts_user_device ',$ex->getMessage());
			return null;
		}
	}	
	
	
/*
 * This function is user to get all the dealers
 */
	public function  get_allDealers()
	{
		try{
		$command="Select dealer_id, dealer_name from vts_dealer where dealer_isactive='1'";		
		$query = $this->db->query($command);
		$result=$query->result_array();
		if ($result != null)
			return $result;
		else
			return null;
		}catch (Exception $ex)
		{
			log_message('error, get all dealers ',$ex->getMessage());
			return null;
		}
	}
	
	
	
	
	/*
	 * To edit or insert the user from the users table
	 */
	
	public function InsertOrUpdate_user($userID,$data,$operation)
	{
		try
		{			
			if($operation=="insert")
			{
				$this->db->insert('vts_users', $data);
				return true;
			}
			else if($operation=="edit")
			{	
				$this->db->where('user_id', $userID);
				$this->db->update('vts_users', $data);
				return true;
			}			
		}catch (Exception $ex)
		{
			log_message('error insert or update  - vts_users',$ex->getMessage());
			return false;
		}
	}
	
	public function Add_userVehicleGroupLink($userID)
	{
		try
		{
			$data = array('vh_gp_link_user_id' => $userID,'vh_gp_link_vehicle_group_id' => INDIVIDUAL_VEHICLE_GP);
			$this->db->insert('vts_user_vehicle_group_link', $data);				
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	
	
	//-------------------------------process of driver group--------------------
	
	

	public function Add_userDriverGroupLink($userID)
	{
		try
		{			
			$data = array('dr_gp_link_user_id' => $userID,'dr_gp_link_driver_group_id' => INDIVIDUAL_DRIVER_GP);
			$this->db->insert('vts_user_driver_group_link', $data);			
				
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}	
	//-------------------------------device--------
	/*
	 * This function is used to get all the device from the device table
	 * to fill the drop down in user view.
	 */
	public function get_allDevice($dealerID=0,$deviceID=null)
	{
		try
		{
			if($dealerID!=null)
			{
				$dealerID=($dealerID!=null)?$dealerID:'0';
				if($deviceID!=null)
 					$command="select get_device_for_individual_user('".$dealerID."', '".$deviceID."');";
				else 
					$command="select get_device_for_individual_user('".$dealerID."');";				
				$query = $this->db->query($command);
				$result=$query->result_array();				
				if ($result != null)
				{	
					$row=array();				
					for($i=0;$i<count($result);$i++)
					{
						$str = explode ( ",", $result[$i]['get_device_for_individual_user'] );
						$str = str_replace ( '(', '', $str );
						$str = str_replace ( ')', '', $str );						
						if($str!=null)
						{
							$row[$i]['device_id']=$str[0];
							$row[$i]['device_imei']=$str[1];
						}
					}					
					return $row;
				}
				else
					return null;
			}
			else
			{
				return null;
			}
		}
		catch (Exception $ex)
		{
			log_message('error','get all device');
			return null;
		}
	}
	
	/*
	 * This function is used to get the vehicle list fron the vehicle table
	 */
	public function get_allVehicle()
	{
		try
		{
			$command="Select vehicle_id, vehicle_regnumber from vts_vehicle";
			//log_message('error',$command);
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
			
		}catch (Exception $ex)
		{
			log_message('error','get all vehicle');
			return null;
		}
		
	} 
	/*
	 * This function is used to get the selected client time difference
	 */
	public function get_time_diff($clientId)
	{
		$command="Select client_time_diff from vts_client where md5(client_id::varchar)='".$clientId."'";
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row['client_time_diff'];
		else
			return null;
	}
	
	/*
	 * To edit or insert the user from the users table
	 */
	
	public function Insert_addDevice($data)
	{
		try
		{
			$vh_id="'null'";$driver_id="'null'";
			$remarks="User Id:".$data['userID'].", Name: ".$data['userFullName'].", Login email:".$data['userEmail'];
			$command="update vts_device set device_client_id='".INDIVIDUAL_CLIENT."' , device_release_client_date='".$data['installDate']."' , device_remarks='".$remarks."' where device_id='".$data['deviceID']."';";
			$command.="insert into vts_vehicle (vehicle_client_id,vehicle_group_id,vehicle_regnumber,vehicle_speedlimit, vehicle_isactive,vehicle_remarks) values ('".INDIVIDUAL_CLIENT."','".INDIVIDUAL_VEHICLE_GP."','".$data['vehicleRegNo']."','".$data['speedLimit']."','".ACTIVE."','".$remarks."');";
			$command.="insert into vts_driver (driver_client_id,driver_drivergroup_id,driver_name,driver_address,driver_mobile,driver_isactive,driver_remarks) values ('".INDIVIDUAL_CLIENT."','".INDIVIDUAL_DRIVER_GP."','".$data['userFullName']."','".$data['userEmail']."','".$data['mobileNo']."','".ACTIVE."','".$remarks."');";			
			$this->db->query($command);
			$command1="select vehicle_id from vts_vehicle where vehicle_regnumber='".$data['vehicleRegNo']."';";
			$query=$this->db->query($command1);			
			$result1 = $query->row_array();
			if ($result1 != null)
				$vh_id = "'".$result1['vehicle_id']."'";
			else
				$vh_id = "'null'";
			
			$command2="select driver_id from vts_driver where driver_address='".$data['userEmail']."';";
			$query2=$this->db->query($command2);
			$result2 = $query2->row_array();
			if ($result2 != null)
				$driver_id = "'".$result2['driver_id']."'";
			else
				$driver_id = "'null'";
			
			$command3="insert into vts_device_installation (dev_install_client_id,dev_install_device_id,dev_install_vehicle_id,dev_install_installed_date,dev_install_remarks) values ('".INDIVIDUAL_CLIENT."','".$data['deviceID']."',".$vh_id.",'".$data['installDate']."','".$remarks."');";
			$command3.="insert into vts_trip_register (trip_register_client_id,trip_register_vehicle_id,trip_register_driver_id,trip_register_time_start,trip_register_time_end,trip_register_remarks)values ('".INDIVIDUAL_CLIENT."' ,".$vh_id." ,".$driver_id.",'".$data['installDate']."','".$data['expireDate']."','".$remarks."');";
			$command3.="insert into vts_user_device (user_dev_user_id,user_dev_device_id,user_dev_vehicle_id,user_dev_install_date,user_dev_expire_date) values ('".$data['userID']."','".$data['deviceID']."',".$vh_id.",'".$data['installDate']."','".$data['expireDate']."');";
			$query3=$this->db->query($command3);
			if($driver_id!="'null'" && $vh_id!="'null'")
				return true;
			else 
				return false;
			
		}catch (Exception $ex)
		{
			log_message('error insert - individual users',$ex->getMessage());
			return false;
		}
	}
	
	
	public function Update_changes($data)
	{
		try 
		{
			$remarks="User Id:".$data['userID'].", Name: ".$data['userFullName'].", Login email:".$data['userEmail'];
			$command="update vts_vehicle set vehicle_regnumber='".$data['vehicleRegNo']."',vehicle_speedlimit='".$data['speedLimit']."' where vehicle_id='".$data['vehicleID']."';";
			$this->db->query($command);
			$command="";
			if($data['preInstallDate']!=$data['installDate'] && $data['preDeviceID']==$data['deviceID'])//Installation date alone changed, then it enter into the condition
			{
				$command="update vts_device set device_release_client_date='".$data['installDate']."' where device_id='".$data['deviceID']."';";
				$command.="update vts_device_installation set dev_install_installed_date='".$data['installDate']."' where dev_install_device_id='".$data['deviceID']."';";
				$command.="update vts_trip_register set trip_register_time_start='".$data['installDate']."',trip_register_time_end='".$data['expireDate']."' where trip_register_vehicle_id='".$data['vehicleID']."';";
				$command.="update vts_user_device set user_dev_install_date='".$data['installDate']."',user_dev_expire_date='".$data['expireDate']."' where user_dev_id='".$data['userDeviceID']."';";				
			}
			else if($data['preDeviceID']!=$data['deviceID'])
			{	
				$command="update vts_device set device_remarks=null, device_client_id=null, device_release_client_date=null where device_id='".$data['preDeviceID']."';";
				$command.="update vts_device set device_client_id='".INDIVIDUAL_CLIENT."' , device_release_client_date='".$data['installDate']."' , device_remarks='".$remarks."' where device_id='".$data['deviceID']."';";
				$command.="update vts_device_installation set dev_install_installed_date='".$data['installDate']."',dev_install_device_id='".$data['deviceID']."' where dev_install_vehicle_id='".$data['vehicleID']."';";
				$command.="update vts_trip_register set trip_register_time_start='".$data['installDate']."', trip_register_time_end='".$data['expireDate']."' where trip_register_vehicle_id='".$data['vehicleID']."';";
				$command.="update vts_user_device set user_dev_install_date='".$data['installDate']."', user_dev_expire_date='".$data['expireDate']."', user_dev_device_id='".$data['deviceID']."' where user_dev_id='".$data['userDeviceID']."';";				
			}
			$this->db->query($command);			
			return  true;
				
		}catch (Exception $ex)
		{
			log_message('error update  - individual users',$ex->getMessage());
			return false;
		}
	}
}
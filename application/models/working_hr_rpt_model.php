<?php
Class Working_hr_rpt_model extends CI_Model{
	
	public function __construct()
	{		
		$this->load->database();
		$GLOBALS['ID'] = $this->session->userdata('login');		
	}
	
	public function get_allClient()
	{
		$this->db->order_by('client_name','ASC');
		$this->db->select('client_id, client_name');
		$this->db->where('client_id !=', AUTOGRADE_USER);
		$query = $this->db->get('vts_client');
		$result=$query->result_array();
		//log_message('debug',print_r($result,true));
		return $result;
	}
	
	public function get_allvhgp($id)
	{
		$command="Select vehicle_group_id as id,vehicle_group as value from vts_vehicle_group";
		if($GLOBALS['ID']['sess_client_admin']=='0')
			$command.=" inner join vts_user_vehicle_group_link on vh_gp_link_vehicle_group_id=vehicle_group_id and vh_gp_link_user_id='".$GLOBALS['ID']['sess_userid']."'";
		$command.=" where vehicle_group_client_id=$id order by vehicle_group asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_allvh($id)
	{
		$command="Select vehicle_id as id,vehicle_regnumber as value from vts_vehicle where vehicle_group_id=$id order by vehicle_regnumber asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_run_data($id, $f_date)
	{
		$time_diff=$GLOBALS['ID']['sess_time_zonediff'];
		$command="Select EXTRACT(EPOCH from(gps_datetime + interval '1 hour' * $time_diff))epo,(gps_datetime + interval '1 hour' * $time_diff) dt,gps_latitude lat,gps_longitude lng,gps_digitalinputstatus st,gps_speed spd from server_gps_data where  gps_vehicle_id=$id and gps_datetime between (('$f_date 00:00:00')::timestamp  - interval '1 hour' * $time_diff ) and (('$f_date 23:59:59')::timestamp  - interval '1 hour' * $time_diff ) and gps_digitalinputstatus='Y' order by epo;";
		//log_message('debug',$command);
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
}
<?php

class Service_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	

	
	/*
	 * This function used to return the device type detail relate the give parameter(i.e. service_id)
	 * if any error, then return null.
	 */
	public function get_vts_service($rowPerPage=null,$startNo=null,$service_id=null)
	{
		try
		{
			if($rowPerPage==null & $service_id==null)
			{
				$this->db->order_by('service_name','ASC');
				$this->db->select('service_id, service_name, service_interval');
				$this->db->from('vts_service');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($service_id==null)
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('service_name','ASC');
				$this->db->select('service_id, service_name, service_interval');
				$this->db->from('vts_service');
				$query = $this->db->get();
				return $query->result_array();
							
			}
			else
			{
				$query=$this->db->get_where('vts_service',array('service_id'=>$service_id));
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_service',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the Service details from the vts_service table
	 */
	
	public function InsertOrUpdateOrdelete_vts_service($service_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_service', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('service_id', $service_id);
				$this->db->update('vts_service', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('service_id', $service_id);
				if($this->db->delete('vts_service'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_service',$ex->getMessage());
			return false;
		}
	}
	
}
?>

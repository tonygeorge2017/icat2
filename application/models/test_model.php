<?php
class Test_model extends CI_Model{
	
	public function get_vehicle_report_data() {
		$query = $this->db->query("SELECT 
			gps_latitude, 
			gps_longitude, 
			to_char(gps_datetime::timestamp at time zone 'IST','YYYY-MM-DD HH:MI:SS') AS dt,
			driver_name
		FROM 
			server_gps_data JOIN vts_driver ON gps_driver_id=driver_id 
		WHERE 
			gps_imeino='865733025268004' AND to_char(gps_datetime::timestamp at time zone 'IST','YYYY-MM-DD HH:MI:SS') 
		BETWEEN 
			'2017-01-01 00:00:00' AND '2017-01-02 23:59:59' 
		ORDER BY dt ASC ");
		
		$result = $query->result_array ();
		
		return json_encode($result);
	}
	
}
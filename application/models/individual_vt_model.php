<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Individual_vt_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
		$GLOBALS['sessUserType']=$GLOBALS['ID']['sess_user_type'];
	}
	
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allIndividualUsers($clientID,$userID){
		try
		{
			if($GLOBALS['sessUserType']==AUTOGRADE_USER)
			{
				$command="select distinct user_id as id, user_user_name as name, user_id from  vts_users where (user_client_id ='".INDIVIDUAL_CLIENT."' or user_parent_id is not null) and user_id <> '".INDIVIDUAL_CLIENT."' order by user_user_name;";
			}
			else if($GLOBALS['sessUserType']==INDIVIDUAL_CLIENT)
			{
				$command="select distinct user_id as id, user_user_name as name, user_id from vts_users,vts_user_device where user_id=user_dev_user_id and user_client_id ='".INDIVIDUAL_CLIENT."' and user_id='".$userID."' order by user_user_name;";
			}
			else if($GLOBALS['sessUserType']==PARENT_USER)
			{
				$command="select user_id as id, parent_id, parent_name as name, user_id from vts_users, vts_parent where parent_id=user_parent_id and user_client_id ='".$clientID."' and user_id='".$userID."' order by parent_name;";
			}
			//log_message('debug','**Get User**'.$command);
			$query = $this->db->query($command);			
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all user name - vts_users');
			return null;
		}
	}	
	
	public function get_all_user_vehicle($userID)
	{
		try
		{
			$command="";
			$userType=$GLOBALS['sessUserType'];
			if($userID!=null)
			{ 	
				if($GLOBALS['sessUserType']==AUTOGRADE_USER)
				{				
					$command="select user_type_id,user_parent_id from vts_users where user_id='".$userID."'";
					$query = $this->db->query($command);
					$result=$query->row_array();
					$userType=$result['user_type_id'];					
				}
				if($userType==INDIVIDUAL_CLIENT)
					$command="select v.vehicle_id as vh_id, v.vehicle_regnumber as vh_name from vts_user_device ud,
vts_vehicle v, vts_vehicle_group vg where v.vehicle_group_id=vg.vehicle_group_id and vg.vehicle_group_isactive='".ACTIVE."' 
and v.vehicle_id=ud.user_dev_vehicle_id and	v.vehicle_isactive ='".ACTIVE."' and ud.user_dev_user_id='".$userID."'order by v.vehicle_regnumber;";
				else if($userType==PARENT_USER)
					$command="select ps.pt_stop_parent_id, v.vehicle_id as vh_id, v.vehicle_regnumber as vh_name from 
vts_vehicle_stops vs, vts_parent_stop ps, vts_vehicle v,vts_vehicle_group vg, vts_users u where v.vehicle_group_id=vg.vehicle_group_id 
and vg.vehicle_group_isactive='".ACTIVE."' and  vs.vh_stop_id=ps.pt_stop_vh_stop_id and ps.pt_stop_isactive='".ACTIVE."' 
and v.vehicle_id=vs.vh_vehicle_id and v.vehicle_isactive='".ACTIVE."' and ps.pt_stop_parent_id=u.user_parent_id 
and u.user_id='".$userID."' group by ps.pt_stop_parent_id,v.vehicle_id order by v.vehicle_regnumber";
				//log_message('debug','**Get Vehicle**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_user_device',$ex->getMessage());
			return null;
		}
	}
	
	
	
	public  function get_gps_data($vh_id,$fdate,$tdate)
	{		
	try
		{	
			if($vh_id!=0)//to get current gps location for selected vehicle only.(i.e. One vehicle at a time)
			{
				 // to back track the gps loction for selected vehicle.(i.e. inbetween particular date&time)
				$command='SELECT gps_digitalinputstatus as "status", driver_name as "driver",  vehicle_regnumber as "vehicle", vehicle_speedlimit as "speedLimit", 
gps_latitude as "lat", gps_longitude as "lng", gps_speed as "speed",get_user_datetime(gps_datetime,'.$GLOBALS['sessUserID'].')as "dateTime" FROM';
				$command.=" vts_vehicle left outer join server_gps_data on vehicle_id=gps_vehicle_id left outer join vts_driver on driver_id=gps_driver_id where vehicle_id ='".$vh_id."' 
and gps_datetime between '".$fdate."' and '".$tdate."' and (gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$') order by gps_datetime asc;";
			//log_message('debug',"********".$command);
			$query=$this->db->query($command);
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
			}
			else 
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	//this function is used to get all current individual user vehicles for given userID.
	public function get_all_live_vehicle_data($userID, $vhID=null)
	{
	try
		{			
			$command="";
			$userType=$GLOBALS['sessUserType'];
			if($userID!=null)
			{ 	
				if($GLOBALS['sessUserType']==AUTOGRADE_USER)
				{				
					$command="select user_type_id,user_parent_id from vts_users where user_id='".$userID."'";
					$query = $this->db->query($command);
					$result=$query->row_array();
					$userType=$result['user_type_id'];					
				}
				if($userType==INDIVIDUAL_CLIENT)
				{
					$command='select  data.gps_latitude as "lat", data.gps_longitude as "lng", data.gps_speed as "speed",veh.vehicle_regnumber as "vehicle",
drv.driver_name as "driver", veh.vehicle_speedlimit as "speedLimit",data.gps_digitalinputstatus as "status",
get_user_datetime(data.gps_datetime,'.$GLOBALS['sessUserID'].') as "dateTime" from server_gps_data data
inner join vts_vehicle veh on veh.vehicle_id = data.gps_vehicle_id
inner join vts_driver drv on drv.driver_id = data.gps_driver_id
inner join';
					$command.=" (select v.vehicle_id, max(d.gps_datetime) as date_time from vts_vehicle v
inner join vts_vehicle_group vg on vg.vehicle_group_id=v.vehicle_group_id and vg.vehicle_group_isactive='".ACTIVE."' 
inner join server_gps_data d on v.vehicle_id = d.gps_vehicle_id and v.vehicle_group_id= '".INDIVIDUAL_VEHICLE_GP."' and v.vehicle_isactive= '".ACTIVE."'
inner join vts_user_device ud on ud.user_dev_vehicle_id=v.vehicle_id
and ud.user_dev_user_id='".$userID."'";
					if($vhID!=null)
						$command.=" and v.vehicle_id='".$vhID."'";
					$command.=" group by v.vehicle_id)
as maxdatetime on (data.gps_vehicle_id = maxdatetime.vehicle_id and data.gps_datetime = maxdatetime.date_time)
and (data.gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$');";
				}
				else if($userType==PARENT_USER)
				{
					$command='select  data.gps_latitude as "lat", data.gps_longitude as "lng", data.gps_speed as "speed",veh.vehicle_regnumber as "vehicle",
drv.driver_name as "driver", veh.vehicle_speedlimit as "speedLimit",data.gps_digitalinputstatus as "status",
get_user_datetime(data.gps_datetime,'.$GLOBALS['sessUserID'].') as "dateTime" from server_gps_data data 
inner join vts_vehicle veh on veh.vehicle_id = data.gps_vehicle_id
inner join vts_driver drv on drv.driver_id = data.gps_driver_id
inner join';
					$command.=" (select v.vehicle_id, max(d.gps_datetime) as date_time from vts_vehicle v
inner join vts_vehicle_group vg on vg.vehicle_group_id=v.vehicle_group_id and vg.vehicle_group_isactive='".ACTIVE."' 
inner join server_gps_data d on v.vehicle_id = d.gps_vehicle_id and v.vehicle_isactive= '".ACTIVE."' 
inner join vts_vehicle_stops vhs on v.vehicle_id = vhs.vh_vehicle_id
inner join vts_parent_stop vps on vps.pt_stop_vh_stop_id = vhs.vh_stop_id 
inner join vts_users u on u.user_parent_id=vps.pt_stop_parent_id and u.user_id='".$userID."'";
					if($vhID!=null)
						$command.=" and v.vehicle_id='".$vhID."'";
					$command.=" group by v.vehicle_id) 
as maxdatetime on (data.gps_vehicle_id = maxdatetime.vehicle_id and data.gps_datetime = maxdatetime.date_time)
and (data.gps_speed ~'^([0-9]+\.?[0-9]*|\.[0-9]+)$');";
				}
				//log_message('debug','**get_all_live_vehicle_data Vehicle**'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}		
	}
	
/*
 * This function is used
 */
	public function get_usertime()
	{
		$dates=gmdate('Y-m-d H:i:s');
		$command="Select get_user_datetime('".$dates."', '".$GLOBALS['sessUserID']."') as userdate ";
		$query=$this->db->query($command);
		$row = $query->row_array();
		if ($row != null)
			return $row['userdate'];
		else
			return null;
	
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Fuel_coupon_model extends CI_Model{
	
	public function __construct()
	{		
		$this->load->database();
		$GLOBALS['ID'] = $this->session->userdata('login');		
	}

	public function get_allClient()
	{
		$this->db->order_by('client_name','ASC');
		$this->db->select('client_id, client_name');
		$this->db->where('client_id !=', AUTOGRADE_USER);
		$query = $this->db->get('vts_client');
		$result=$query->result_array();
		//log_message('debug',print_r($result,true));
		return $result;
	}
	/*
	 * This function is used to get the vehicle group for the given client ID
	 */
	public function get_vhgp($clientID=null)
	{
		try
		{
			$command="select vehicle_group_id id, vehicle_group gpname from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='$clientID' order by vehicle_group;";
			//log_message('debug',$command);
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return array();
		}
		catch (Exception $ex)
		{			
			return array();
		}
	}

	public function get_vhehicle($id)
	{
		$command="select vehicle_id id, vehicle_regnumber vehicle, vehicle_mileage milage, vehicle_fuel_consumption consumption, vehicle_idealspeed idealspeed  from vts_vehicle where vehicle_isactive='".ACTIVE."' and vehicle_group_id=$id order by vehicle_regnumber;";
		$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return array();
	}

	public function get_driverGp($id)
	{
		$command="select driver_group_id id, driver_group drivergp from vts_driver_group where driver_group_isactive='".ACTIVE."' and driver_group_client_id=$id order by driver_group;";
		$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return array();		
	}

	public function get_driver($id)
	{
		$command="select driver_id id, driver_name driver from vts_driver where driver_isactive='".ACTIVE."' and driver_drivergroup_id=$id order by driver_name;";
		$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return array();		
	}

	public function get_last($id)
	{
		$command="select vts_fuel_coupon_non_bunk as pbnk, vts_fuel_coupon_fuel as pfuel,  vts_fuel_coupon_excess_fuel as pexfuel, to_char(vts_fuel_coupon_date, 'YYYY-MM-DD HH24:MI:SS') as pdate, vts_fule_coupon_vehicle as vhid, vts_fuel_coupon_driver as drid from vts_fuel_coupon inner join vts_fuel_bunk on vts_fuel_coupon_fuel_bunk=vts_fuel_bunk_id where vts_fule_coupon_vehicle=$id order by vts_fuel_coupon_date desc limit 1;";
		$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return array();		
	}

	public function get_fuelBunk($id)
	{
		$command="select vts_fuel_bunk_id id, vts_fuel_bunk_name as bunk, vts_fuel_bunk_address as address,vts_fuel_bunk_phone as phone from vts_fuel_bunk where  vts_fuel_bunk_client=$id order by vts_fuel_bunk_name;";
		$query = $this->db->query($command);
			$result=$query->result_array();
		if ($result != null)
			return $result;
		else
			return array();	
	}

	public function get_run_data($id, $f_date, $startBit)
	{
		$time_diff=$GLOBALS['ID']['sess_time_zonediff'];
		$command="Select EXTRACT(EPOCH from(gps_datetime + interval '1 hour' * $time_diff))epo,(gps_datetime + interval '1 hour' * $time_diff) dt,gps_latitude lat,gps_longitude lng,gps_digitalinputstatus st,gps_speed spd from server_gps_data where	";
		if($startBit=="F")
		{
			$command.="  gps_datetime between (('$f_date')::timestamp  - interval '1 hour' * $time_diff )";
			$fs_date=substr($f_date,0,10);
			$command.=" and (('$fs_date 23:59:59')::timestamp  - interval '1 hour' * $time_diff )";
		}
		else{
			$fs_date=substr($f_date,0,10);
			//$command.="  (gps_datetime + interval '1 hour' * $time_diff) between '$fs_date 00:00:00' and '$fs_date 23:59:59'";
			$command.=" gps_datetime between (('$fs_date 00:00:00')::timestamp  - interval '1 hour' * $time_diff ) and (('$fs_date 23:59:59')::timestamp  - interval '1 hour' * $time_diff )";
		}
		$command.=" and gps_vehicle_id=$id and gps_digitalinputstatus='Y'";
		//log_message('debug',$command);
		$query=$this->db->query($command);
		$result = $query->result_array();
		return $result;
	}

	public function save_coupon($vhgp,$vh,$dr,$bnk,$fuel,$usr,$rmfuel,$print,$bunkplace)
	{
		$data['vts_fule_coupon_vehicle']=$vh;
		$data['vts_fuel_coupon_driver']=$dr;
		$data['vts_fuel_coupon_fuel_bunk']=$bnk;
		$data['vts_fuel_coupon_fuel']=$fuel;
		$data['vts_fuel_coupon_excess_fuel']=$rmfuel;
		$data['vts_fuel_coupon_created_by']=$usr;
		$data['vts_fuel_coupon_non_bunk']=$bunkplace;
		$this->db->insert('vts_fuel_coupon',$data);
		$id=$this->db->insert_id();		
		return $id;
	}

	public function get_coupon($id, $selected_date, $print="dateselect")
	{
		$command="SELECT vts_fuel_coupon_id as cid, vehicle_regnumber as vhname, driver_name as drname, vts_fuel_coupon_date::date as dt, vts_fuel_coupon_fuel as fl, 
       vts_fule_coupon_vehicle as vh, vts_fuel_coupon_driver as dr, vts_fuel_coupon_fuel_bunk bk FROM
       vts_fuel_coupon inner join vts_vehicle on vts_fule_coupon_vehicle=vehicle_id and vehicle_group_id='$id' 
       inner join vts_driver on driver_id=vts_fuel_coupon_driver 
  	   where 1=1 ";
  	   if($print=="dateselect")
  	   		$command.=" and vts_fuel_coupon_date::date='$selected_date' order by vehicle_regnumber;";
  	   	else if($print=="today")
  	   		$command.=" and vts_fuel_coupon_date::date=now()::date order by vehicle_regnumber;";
  	   //log_message('debug',$command);
		$query=$this->db->query($command);
		$result = $query->result_array();
		return $result;
	}

	public function get_current_coupon($id)
	{
		$command="SELECT vts_fuel_coupon_id as cid, vehicle_regnumber as vhname, driver_name as drname, vts_fuel_coupon_date::date as dt, vts_fuel_coupon_fuel as fl, 
       vts_fule_coupon_vehicle as vh, vts_fuel_coupon_driver as dr, vts_fuel_coupon_fuel_bunk bk FROM
       vts_fuel_coupon inner join vts_vehicle on vts_fule_coupon_vehicle=vehicle_id 
       inner join vts_driver on driver_id=vts_fuel_coupon_driver 
  	   where vts_fuel_coupon_id=$id;";
  	   //log_message('debug',$command);
		$query=$this->db->query($command);
		$result = $query->result_array();
		return $result;
	}	
	
	public function get_row_count()
	{		
		if($GLOBALS['clientID'])
		{
			$command="SELECT count(distinct vts_fuel_coupon_id) row_cnt FROM vts_fuel_coupon inner join vts_fuel_bunk on vts_fuel_bunk_id=vts_fuel_coupon_fuel_bunk WHERE vts_fuel_bunk_client=".$GLOBALS['clientID']." group by vts_fuel_bunk_client;";
			$query=$this->db->query($command);
			$row = $query->row_array ();
			return $row ['row_cnt'];
		}
		else{
			return 0;
		}
	}
	
	public function get_fuel_coupon($page=0)
	{
		$result=array();
		if($GLOBALS['clientID'])
		{
			$command="SELECT vts_fuel_coupon_id fid, vts_fuel_coupon_date::date fdate, vts_fuel_coupon_fuel ffuel, vts_fuel_coupon_excess_fuel fex, vehicle_regnumber fvhreg, vts_fuel_coupon_non_bunk fbunk FROM vts_fuel_coupon inner join vts_vehicle on vehicle_id=vts_fule_coupon_vehicle inner join vts_fuel_bunk on vts_fuel_bunk_id=vts_fuel_coupon_fuel_bunk WHERE vts_fuel_bunk_client=".$GLOBALS['clientID']." order by vts_fuel_coupon_date desc limit ".ROW_PER_PAGE." offset ".$page.";";
			//log_message('debug',$command);
			$query=$this->db->query($command);
			$result = $query->result_array();
		}
		return $result;
	}
}
?>
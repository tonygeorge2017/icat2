<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_monitoring_model extends CI_Model{

	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sess_time_zonediff']=$GLOBALS['ID']['sess_time_zonediff'];
	}
	/*
	 * This function is used to fetch all the values from vts_check_new_device.
	 * @param
	 *  $p_imei - IMEI no. of the device.
	 * Return type - result array if $p_imei is null or row array
	 */
	public function get_device_details($data=null,$p_imei=null)
	{
		try
		{
			$time_diff=$GLOBALS['sess_time_zonediff'];
			$curr_utc_datetime=gmdate('Y-m-d H:i:s');
			$command="SELECT get_distributor_name(dev.device_distributor_id)dist_name, get_dealer_name(dev.device_dealer_id)dealer_name,
di.dev_install_sim_provider, di.dev_install_is_atc_sim, di.dev_install_mobile_no, dev.device_slno, v.vehicle_regnumber vehicle_name,
dev.device_client_id, get_client_name(dev.device_client_id) as client_name, chk.chk_new_device_imei, chk.chk_new_device_first_gpsdate, 
chk.chk_new_device_first_gpstime, (chk.chk_new_device_first_datetime + interval '1 hour' * ".$time_diff.") as first_datetime, 
chk.chk_new_device_last_gpsdate, chk.chk_new_device_last_gpstime, (chk.chk_new_device_last_datetime + interval '1 hour' * ".$time_diff.") as last_datetime, 
chk.chk_new_device_runno, chk.chk_new_device_lat, chk.chk_new_device_lat_dir, chk.chk_new_device_lng, chk.chk_new_device_lng_dir,
chk.chk_new_device_ingition, chk.chk_new_device_power, chk.chk_new_device_valid, 
chk.chk_new_device_valid_count, chk.chk_new_device_invalid_count, chk.chk_new_device_first_runno,
(chk.chk_new_device_valid_count + chk.chk_new_device_invalid_count) as total,
extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - chk.chk_new_device_last_datetime::TIMESTAMP))/60 as timediff_btw_curnt_lat,
extract(epoch from  ('".$curr_utc_datetime."'::TIMESTAMP - chk.chk_new_device_first_datetime::TIMESTAMP))/60 as timediff_btw_curnt_fst,
extract(epoch from  (chk.chk_new_device_last_datetime::TIMESTAMP - chk.chk_new_device_first_datetime::TIMESTAMP))/60 as timediff_btw_fst_lat, chk.chk_new_device_digital_inputs digital_inputs
FROM vts_check_new_device chk LEFT OUTER JOIN vts_device dev ON chk.chk_new_device_imei=dev.device_imei 
LEFT OUTER JOIN vts_device_installation di on di.dev_install_device_id=dev.device_id and di.dev_install_removed_date is null
LEFT OUTER JOIN vts_vehicle v on di.dev_install_vehicle_id = v.vehicle_id where 1=1";
			if($data!=null)
			{				
				if($data['imeiNo']!=null)
				{
					$command.=" and chk.chk_new_device_imei like '%".$data['imeiNo']."%'";
				}
	
				if($data['slNo']!=null)
				{
					$command.=" and device_slno = '".$data['slNo']."'";
				}
				if($data['distributorID']!=null)
				{
					$command.=" and device_distributor_id = '".$data['distributorID']."'";
				}
	
				if($data['dealerID']!=null)
				{
					$command.=" and device_dealer_id = '".$data['dealerID']."'";
				}
				if($data['clientID']!=null)
				{
					$command.=" and device_client_id = '".$data['clientID']."'";
				}
				if($data['vehicleGroupID']!=null)
				{
					$command.=" and vehicle_group_id = '".$data['vehicleGroupID']."'";
				}
				if($data['vehicleID']!=null)
				{
					$command.=" and dev_install_vehicle_id = '".$data['vehicleID']."'";
				}
				if($data['AllDevice'])
				{
					$chkdate=new DateTime($curr_utc_datetime);
					$chkdate=$chkdate->modify("-7 days");
					$command.=" and chk.chk_new_device_last_datetime::Date >= '".$chkdate->format('Y-m-d')."'";
				}
			}
			if($p_imei!=null)
				$command.=" and md5(chk_new_device_imei) like '%".$p_imei."%'";
			$command.=" group by dev.device_imei, di.dev_install_sim_provider, di.dev_install_is_atc_sim,
di.dev_install_mobile_no, dev.device_distributor_id, dev.device_dealer_id, dev.device_client_id, dev.device_slno,
v.vehicle_regnumber,chk.chk_new_device_imei, chk.chk_new_device_first_gpsdate,chk.chk_new_device_first_gpstime,chk.chk_new_device_first_datetime,
chk.chk_new_device_last_gpsdate,chk.chk_new_device_last_gpstime,chk.chk_new_device_last_datetime,chk.chk_new_device_runno,chk.chk_new_device_lat,
chk.chk_new_device_lat_dir, chk.chk_new_device_lng, chk.chk_new_device_lng_dir,
chk.chk_new_device_ingition, chk.chk_new_device_power, chk.chk_new_device_valid, 
chk.chk_new_device_valid_count, chk.chk_new_device_invalid_count, chk.chk_new_device_first_runno, chk.chk_new_device_digital_inputs
order by client_name, chk.chk_new_device_first_datetime desc;";
			//log_message('debug','--'.$command);
			$query = $this->db->query($command);
			if($p_imei != null)
				$result= $query->row_array();
			else
				$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * @param
	 *  $client - client id
	 *  $dist - distributor id
	 *  $dealerID - dealer id
	 * Return type - result array
	 */
	public function get_all_Clients($client=null,$dist=null,$dealerID=null){
		try{
			if($client!=null)
			{
				$command="select client_id, client_name from vts_client,vts_dealer,vts_distributor where dealer_distributor_id=dist_id and client_dealer_id=dealer_id
						and client_id <> '".AUTOGRADE_USER."'";
				$command.=($client!='all')?" and (lower(client_name) like lower('".$client."%') or lower(client_name) like lower('% ".$client."%'))":"";
				$command.=($dist!=null)?" and dist_id='".$dist."'":"";
				$command.=($dealerID!=null)?" and dealer_id='".$dealerID."'":"";
				$command.=" order by client_name;";
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle group the given parameter
	 * @param
	 * 	$clientID - selected client id
	 * 	$sessClient -  session client id
	 *  $sessUser - session user id
	 * Return type - result array
	 */
	public function get_all_vhGp($clientID=null)
	{
		try
		{
			if($GLOBALS['sessClientID']!=null)
			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$clientID."' order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and vehicle_group_client_id='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle for the given vehicle group
	 * @param
	 * 	$vehicleGpID - selected vehicel group id
	 * Return type - result array
	 */
	public function get_all_vhGp_vehicle($vehicleGpID)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{
				$command="select vehicle_id, vehicle_regnumber from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and  vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - get_all_vhGp_vehicle',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the distributor
	 * Return type - result array
	 */
	public function get_all_Distributor()
	{
		try
		{
			$command="select dist_id, dist_name from vts_distributor order by dist_name;";
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - get_all_Distributor',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the dealer for the given distributor
	 * @param
	 * 	$distributorID - selected distributor id
	 * Return type - result array
	 */
	public function get_all_Dealer($distributorID=null)
	{
		try
		{
			$command="";
			if($distributorID!=null)
			{
				$command="select dealer_id, dealer_name from vts_dealer where dealer_distributor_id='".$distributorID."' order by dealer_name;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - get_all_Dealer',$ex->getMessage());
			return null;
		}
	}
}
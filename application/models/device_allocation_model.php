<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_allocation_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
	}
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 * @param
	 *  $clientID - md5 of client id
	 *  $rowPerPage - no. of row per page in table for pagination
	 *  $startNo - starting row of table
	 *  $date - md5 of device release client date
	 *  $dealerID - md dealer id
	 * Return type - result array
	 */
	public function get_deviceDetailList($clientID=null,$rowPerPage=null,$startNo=null,$date=null,$dealerID=null)
	{
		try
		{
			if($dealerID!=null && $rowPerPage == null && $date == null){
				$command="select device_dealer_id,count(device_imei)as no_of_device,device_client_id,device_imei,get_client_name(device_client_id)as client_name, device_release_client_date::date from vts_device where
md5(device_dealer_id::varchar)='".$dealerID."' and device_release_client_date is not null and device_client_id is not null
group by device_dealer_id,device_release_client_date::date,device_client_id,device_imei;";
				$query = $this->db->query($command);
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			elseif($dealerID!=null && $rowPerPage!=null){
				$command="select device_dealer_id,count(device_imei)as no_of_device,device_imei,device_client_id,get_client_name(device_client_id)as client_name, device_release_client_date::date from vts_device where
md5(device_dealer_id::varchar)='".$dealerID."' and device_release_client_date is not null and device_client_id is not null 
group by device_dealer_id,device_release_client_date::date,device_client_id,device_imei order by device_client_id limit ".$rowPerPage." offset ".$startNo.";";
				$query = $this->db->query($command);
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else if($dealerID!=null && $date != null && $clientID!=null)
			{
				$command="select device_id, device_dealer_id, device_client_id, device_release_client_date::date, dev_install_vehicle_id,device_imei 
 from vts_device left outer join vts_device_installation on dev_install_device_id=device_id and dev_install_removed_date is null where
md5(device_release_client_date::date::varchar)='".$date."' and md5(device_dealer_id::varchar)='".$dealerID."' and md5(device_client_id::varchar) ='".$clientID."' and device_is_active='".ACTIVE."';";
				//log_message('debug','--edit part in model--'.$command);
				$query=$this->db->query($command);
				$result =  $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else 
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - user',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allClients($dealer){
		try{
			if($dealer!=null)
			{
				$command="select client_id, client_name from vts_client where md5(client_dealer_id::varchar)='".$dealer."' and client_is_active='".ACTIVE."' order by client_name";
				$query = $this->db->query($command);				
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle from the
	 * vts_vehicle_group table to fill the drop down in user view.
	 * Return type - result array
	 */
	public function get_allDealers($dealer=null)
	{
		try{			
			$command="select dealer_id, dealer_name from vts_dealer where dealer_isactive='".ACTIVE."'";
			$command.=($dealer!=null)?" and md5(dealer_id::varchar)='".$dealer."'":' ';
			$command.=" order by dealer_name;";
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		}catch (Exception $ex)
		{
			log_message('error','get all Dealer name - vts_dealer');
			return null;
		}
	}
	/*
	 * This function is used to get all available device for given dealer id
	 * @param
	 *  $dealerID - dealer id
	 *  $editing - 1 or 0
	 *  $dateTime - md5 of device release client date
	 * Return type - result array
	 */
	public function get_availableDevice($dealerID,$editing,$dateTime=null,$clientID=null)
	{
		try{
			if(!empty($dealerID))
			{
				$command="select device_id, device_imei, device_slno from vts_device where device_is_active='".ACTIVE."' and md5(device_dealer_id::varchar)='".$dealerID."'";
				if($editing==NOT_ACTIVE)
					$command.=" and device_client_id is null;";
				else if($dateTime!=null && $clientID!=null)
					$command.=" 
							and ( (md5(device_release_client_date::date::varchar)='".$dateTime."' and device_client_id='".$clientID."')or device_client_id is null);";
				//log_message('debug','aaaaaaaaa '.$command);
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}else 
				return  null;
		}catch (Exception $ex)
		{
			log_message('error','get all available device name - vts_device');
			return null;
		}
	}
	/*
	 * To edit the device allocation to client from the device table
	 * @param
	 *  $data - associative array of details like client id ,release date, etc..
	 *  $listOfDevice - array of devices to update
	 * Return type - bool
	 */	
	public function Update_deviceToClient($data,$listOfDevice)
	{
		try
		{
			$command="UPDATE vts_device SET device_client_id=null, device_release_client_date=null where device_dealer_id='".$data['device_dealer_id']."' and device_client_id='".$data['device_client_id']."' and device_release_client_date='".$data['device_release_client_date']."';";
			$this->db->query($command);
			if($listOfDevice!=null)
			{
				foreach ($listOfDevice as $device)
				{
					$command="UPDATE vts_device SET device_client_id='".$data['device_client_id']."',device_release_client_date='".$data['device_release_client_date']."' where device_dealer_id='".$data['device_dealer_id']."' and device_id='".$device."';";
					$query = $this->db->query($command);
				}
			}
			return true;
		}catch (Exception $ex)
		{
			log_message('error insert or update  - vts_users',$ex->getMessage());
			return false;
		}
	}
	/*
	 * To delete the device allocation to client from the device table
	 * @param
	 *  $URLdealerID - md5 of dealer id
	 *  $URLclientID - md5 client id
	 *  $URLdate - md5 of device release client date
	 */
	public function delete_deviceToClient($URLdealerID, $URLclientID, $URLdate)
	{
		try
		{
			$command="UPDATE vts_device SET device_client_id=null, device_release_client_date=null where md5(device_dealer_id::varchar)='".$URLdealerID."' and md5(device_client_id::varchar)='".$URLclientID."' and md5(device_release_client_date::date::varchar)='".$URLdate."';";
			$this->db->query($command);
		}catch (Exception $ex)
		{
			log_message('error insert or update  - vts_users',$ex->getMessage());
			return false;
		}
	}
	/*
	 * To get device which is related to the given client, dealer id and datetime
	 * @param
	 *  $dealerID - dealer id
	 *  $clientID - client id
	 *  $dateTime - device release client date
	 * Return type - result array
	 */
	public function get_clientLinkedDevice($dealerID,$clientID,$dateTime)
	{
		try{
			if($dealerID!=null && $clientID!=null && $dateTime!=null)
			{
				$command="select device_dealer_id, device_client_id, device_release_client_date::date from vts_device where
device_release_client_date='".$dateTime."' and device_dealer_id='".$dealerID."' and device_client_id ='".$clientID."';";
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}
	
	public  function get_gps_data($vh_id,$fdate,$tdate)
	{
		try
		{
			 // to back track the gps loction for selected vehicle.(i.e. inbetween particular date&time)
			//$command="SELECT temp_time_diff, temp_vehicle_name, gps_imeino, gps_latitued,gps_longitude,gps_speed,gps_datetime FROM gps_data, temp_vehicle where gps_imeino ='".$imeiNo."' and gps_datetime between timestamp '".$fdate."' - time '05:30' and timestamp '".$tdate."' - time '05:30' and temp_device_imei_no = gps_imeino order by gps_datetime asc;";
			$command="SELECT vehicle_regnumber as temp_vehicle_name, gps_runningno, gps_imeino, gps_datetime, gps_latitude, 
gps_longitude, gps_speed,  gps_digitalinputstatus ,driver_name as driver_name         
FROM server_gps_data inner join vts_vehicle on vehicle_id=gps_vehicle_id 
left outer join vts_driver on driver_id=gps_driver_id 
where gps_vehicle_id ='".$vh_id."' and gps_datetime 
between '".$fdate."' and '".$tdate."' order by gps_datetime asc;";
// 			log_message('error','**Report**'.$command);
			$query=$this->db->query($command);
			$result = $query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}
	public  function get_vehicle_name($vh_id)
	{
		try
		{
			$command="SELECT get_vehicle_name('".$vh_id."') as temp_vehicle_name;";
			$query=$this->db->query($command);
			$row = $query->row_array ();			
			if ($row != null)
				return $row ['temp_vehicle_name'];
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get vehicle name for given imei no',$ex->getMessage());
			return null;
		}
	}
}
<?php

class Device_type_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	

	
	/*
	 * This function used to return the device type detail relate the give parameter(i.e. device_type_id)
	 * if any error, then return null.
	 */
	public function get_vts_device_type($rowPerPage=null,$startNo=null,$device_type_id=null)
	{
		try
		{
			if($rowPerPage==null & $device_type_id==null)
			{
				$this->db->order_by('device_type_name','ASC');
				$this->db->select('vts_device_type.device_type_id as device_type_id, vts_device_type.device_type_name as device_type_name');
				$this->db->from('vts_device_type');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($device_type_id==null)
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('device_type_name','ASC');
				$this->db->select('vts_device_type.device_type_id as device_type_id, vts_device_type.device_type_name as device_type_name');
				$this->db->from('vts_device_type');
				$query = $this->db->get();
				return $query->result_array();
							
			}
			else
			{
				$query=$this->db->get_where('vts_device_type',array('device_type_id'=>$device_type_id));
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_device_type',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the Device group details from the vts_device_type table
	 */
	
	public function InsertOrUpdateOrdelete_vts_device_type($device_type_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_device_type', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('device_type_id', $device_type_id);
				$this->db->update('vts_device_type', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('device_type_id', $device_type_id);
				if($this->db->delete('vts_device_type'))
					return true;
				else
					return FALSE;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_device_type',$ex->getMessage());
			return false;
		}
	}
	
}
?>
					
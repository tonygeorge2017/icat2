<?php

class Dealer_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	
	// **********************  User Model for saving in to vts_user table come's below ********************
	
	/**
	 * Function to insert the data to User table from client form
	 *
	 * $user_client_id   : Client id as a forgin key
	 * $user_user_name   : Contact person name
	 * $user_email : user contact email
	 * $user_is_active  : client is active field value
	 *
	 * inserts the value to DB.
	 **/
	
	public function insert_user_value($u_dealer_id,$u_distributor_id, $u_user_name, $u_email, $u_is_active, $u_time_zn_id, $u_phn, $u_user_type_id, $operation) {
	
		try {
			//Data to be inserted in the DB.
			$data = array(
					'user_dealer_id' => $u_dealer_id,
					'user_distributor_id'=> $u_distributor_id,
					'user_user_name' => $u_user_name,
					'user_email' => $u_email,
					'user_is_active' => $u_is_active,
					'user_is_client_admin' => '1',
					'user_is_admin' => '1',
					'user_time_zone_id' => $u_time_zn_id,
					'user_mobile' => $u_phn,
					'user_type_id' => $u_user_type_id
			);
			//Inserts the data to table EventLog
			if($operation == 'insert')
			{
				$data['user_password'] = md5(microtime() . rand());
				$this->db->insert('vts_users', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('user_dealer_id', $u_dealer_id);
				$this->db->update('vts_users', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('user_dealer_id', $u_dealer_id);
				$this->db->delete('vts_users');
				return true;
			}
	
		}  catch (Exception $e) {
	
			log_message('error, inserting to table - vts_users',$e->getMessage());
			return;
		}
	}
	/*
	 * function to get recently added last row of dealer
	 */
	public function get_max_dealer_id()
	{
		$max_dealer_id = $this->db->select_max('dealer_id');
		$query = $this->db->get('vts_dealer');
		$row = $query->row_array();
		return $row['dealer_id'];
	}
	/*
	 * This function used to return the dealer details relate the give parameter(i.e. dealer_id)
	 * if any error, then return null.
	 */
	public function get_vts_dealer($rowPerPage=null,$startNo=null,$dealer_id=null)
	{
		try
		{
			if($rowPerPage==null & $dealer_id==null)
			{
				$this->db->order_by('dealer_name','ASC');
				$this->db->select('vts_dealer.dealer_id as dealer_id, vts_dealer.dealer_name as dealer_name, vts_dealer.dealer_location as dealer_location, vts_dealer.dealer_contact_phone as dealer_contact_phone, vts_dealer.dealer_contact_email as dealer_contact_email, vts_dealer.dealer_distributor_id as dealer_distributor_id');
				$this->db->from('vts_dealer');
				$query = $this->db->get();
				return $query->result_array();	
			}
			else if($dealer_id==null)
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('dealer_name','ASC');
				$this->db->select('vts_dealer.dealer_id as dealer_id, vts_dealer.dealer_name as dealer_name, vts_dealer.dealer_location as dealer_location, vts_dealer.dealer_contact_phone as dealer_contact_phone, vts_dealer.dealer_contact_email as dealer_contact_email, vts_dealer.dealer_distributor_id as dealer_distributor_id');
				$this->db->from('vts_dealer');
				$query = $this->db->get();
				return $query->result_array();
							
			}
			else
			{
				$query=$this->db->get_where('vts_dealer',array('dealer_id'=>$dealer_id));
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_dealer',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the dealer details from the vts_dealer table
	 */
	
	public function InsertOrUpdateOrdelete_vts_dealer($dealer_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_dealer', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('dealer_id', $dealer_id);
				$this->db->update('vts_dealer', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('dealer_id', $dealer_id);
				$this->db->delete('vts_dealer');
				return true;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_dealer',$ex->getMessage());
			return false;
		}
	}
	
	/*
	 * This function is to get all the Distributor name and display in the dropdown
	 */
	public function get_allDistributorNames()
	{
		try
		{
				$this->db->order_by('dist_name','ASC');
				$this->db->select('dist_name, dist_id');
				$this->db->where('dist_isactive', ACTIVE);
				$query = $this->db->get('vts_distributor');
				return $query->result_array();
		}
		catch (Exception $ex)
		{
			log_message('error','get all device type names - vts_device_type');
			return null;
		}
	}
	/*
	 * This function is to get the user_id inorder to send the email after creating the user successfully from client screen
	 */
	public function get_user_id($usr_email)
	{
		try
		{
			$command = "select user_id from vts_users as user_id where user_email ='".$usr_email ."' " ;
			$query = $this->db->query($command);
			$row = $query->row_array();
			return $row['user_id'];
		}
		catch(Exception $ex)
		{
			log_message('error, getting user_id error - vts_users',$e->getMessage());
			return null;
		}
	
	}
	
}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_management_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');		
	}
	
	/*
	 * This function used to return the user detail relate the give parameter(i.e. userID)
	 * if any error, then return null.
	 */
	public function get_userList($typeID=null,$id=null, $rowPerPage=null,$startNo=null,$userID=null,$userEmail=null)
	{
		try
		{	//user_client_id, user_dealer_id, user_distributor_id, user_parent_id,
			$condition="";$secCondition="";$getIDForDropDown="";
			if($typeID!=null)
			{
				if($typeID==md5(AUTOGRADE_USER))//if autograde client
				{
					$condition="and md5(user_client_id::varchar) ='". $id."'";
					$secCondition=" and user_client_id is not null and user_parent_id is null";
					$getIDForDropDown=" user_client_id as id, ";
				}
				elseif($typeID==md5(INDIVIDUAL_CLIENT) || $typeID==md5(OHTER_CLIENT_USER))//if other client
				{
					$condition="and md5(user_client_id::varchar) ='". $id."'";
					$secCondition=" and user_client_id is not null and user_parent_id is null and user_client_id <> '".AUTOGRADE_USER."'";
					$getIDForDropDown=" user_client_id as id, ";
				}
				elseif($typeID==md5(DISTRIBUTOR_USER))//if distributor
				{
					$condition="and md5(user_distributor_id::varchar) ='". $id."'";
					$secCondition=" and user_distributor_id is not null and user_client_id is null and user_dealer_id is null";
					$getIDForDropDown=" user_distributor_id as id, ";
				}
				elseif($typeID==md5(DEALER_USER))//if dealer
				{
					$condition="and md5(user_dealer_id::varchar) ='". $id."'";
					$secCondition=" and user_dealer_id is not null and user_client_id is null";
					$getIDForDropDown=" user_dealer_id as id, ";
				}
				elseif($typeID==md5(PARENT_USER))//if parent
				{
					$condition="and md5(user_parent_id::varchar) ='". $id."'";
					$secCondition=" and user_parent_id is not null";
					$getIDForDropDown=" user_parent_id as id, ";
				}
				else 			
					$condition="";
			}
			
			if($userEmail!=null){
				$command="select user_id from vts_users where user_email='".$userEmail."';";
				$query = $this->db->query($command);
				//log_message('debug','**get_userList1--'.$command);
				$result = $query->row_array();
				if ($result != null)
					return $result['user_id'];
				else
					return null;
			}			
			elseif($typeID==null)
			{
				$command="select user_id, user_client_id, user_user_name, user_email, user_mobile, user_is_admin, user_dealer_id, user_distributor_id, user_parent_id, user_type_id from vts_users where 1=1 ".$secCondition." order by user_user_name;";
				//log_message('debug','**get_userList2--'.$command);
				$query=$this->db->query($command);				
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			elseif($userID==null && $id!=null)
			{
				$result="";
				$command="select user_id, user_client_id, user_user_name, user_email, user_mobile, user_is_admin, user_dealer_id, user_distributor_id, user_parent_id, user_type_id from vts_users  where 1=1 ".$condition." ".$secCondition." order by user_user_name";
				if($rowPerPage!=null)
					$command .= " limit ".$rowPerPage." offset ". $startNo;
				$command.=";";
				//log_message('debug','**get_userList3--'.$command);
				$query=$this->db->query($command);				
				$result = $query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			elseif($userID!=null)
			{
				$command="select user_id,".$getIDForDropDown." user_user_name, user_email, user_mobile, user_time_zone_id, user_is_active, user_is_client_admin, user_is_admin, user_type_id from vts_users where md5(user_id::varchar)='".$userID."';";
				//log_message('debug','**get_userList4--'.$command);
				$query=$this->db->query($command);
				$result = $query->row_array();
				if ($result != null)
					return $result;
				else
					return null;
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - user',$ex->getMessage());
			return null;
		}
	}	
	
	/*
	 * This function is used to find the time diff ID
	 */
	public function get_ClientsTimeDiff($p_clientID,$p_loginType=null)
	{
		try{
			if($p_clientID!=null)
			{
				$command="";
				if($p_loginType==md5(AUTOGRADE_USER) || $p_loginType==md5(OHTER_CLIENT_USER))
				{
					$command="select client_time_zone_id from vts_client where md5(client_id::varchar)='".$p_clientID."';";
				}				
				elseif($p_loginType==md5(PARENT_USER))
				{
					$command="select client_time_zone_id from vts_parent,vts_client where parent_client_id=client_id and md5(parent_id::varchar)='".$p_clientID."';";
				}
				//log_message('debug','**get_ClientsTimeDiff--'.$command);
				if(!empty($command))
				{
					$query=$this->db->query($command);
					$result = $query->row_array();
					if ($result != null)
						return $result['client_time_zone_id'];
					else
						return null;
				}
				else
					return null;
			}else
				return null;
		}catch (Exception $ex)
		{
			log_message('error','get_ClientsTimeDiff - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * @param
	 * 	$p_loginType - losin user type(i.e parent or dealer or client etc..)
	 * 	$p_id - it may be the client id, parent id
	 * Return Type - result array
	 */
	public function get_allClients($p_loginType)
	{
		try
		{	
			if($p_loginType!=null)
			{
				$command="select client_id as value, client_name as name, client_time_zone_id from vts_client where ";
				if($p_loginType==md5(AUTOGRADE_USER) || $p_loginType==md5(INDIVIDUAL_CLIENT))
				{
					$command.=" md5(client_id::varchar)='".$p_loginType."'";
					$command.=($GLOBALS['ID']['sess_user_type']!=AUTOGRADE_USER)?" and client_id='".$GLOBALS['ID']['sess_clientid']."'":'';
					$command.=" order by client_name;";
				}				
				elseif($p_loginType==md5(OHTER_CLIENT_USER))
				{
					$command.=" client_id <>'".AUTOGRADE_USER."' and client_id <>'".INDIVIDUAL_CLIENT."'";
					$command.=($GLOBALS['ID']['sess_user_type']!=AUTOGRADE_USER)?" and client_id='".$GLOBALS['ID']['sess_clientid']."'":'';
					$command.=" order by client_name;";
				}
				elseif($p_loginType==md5(DISTRIBUTOR_USER))
				{
					$command="select dist_id as value, dist_name as name from vts_distributor";
					$command.=($GLOBALS['ID']['sess_user_type']!=AUTOGRADE_USER)?" and dist_id='".$GLOBALS['ID']['sess_distributorid']."'":'';
					$command.=" order by dist_name;";
				}
				elseif(md5($GLOBALS['ID']['sess_user_type'])==md5(DEALER_USER))
				{
					$command="select client_id as value, client_name as name from vts_client where client_id != ".AUTOGRADE_USER." and client_dealer_id = ".$GLOBALS['ID']['sess_dealerid'];
				}
				elseif(md5($GLOBALS['ID']['sess_user_type'])==md5(AUTOGRADE_USER) && $p_loginType != DEALER_USER)
				{
					$command="select dealer_id as value, dealer_name as name from vts_dealer";
					$command.=($GLOBALS['ID']['sess_user_type']!=AUTOGRADE_USER)?" where dealer_id='".$GLOBALS['ID']['sess_dealerid']."'":'';
					$command.=" order by dealer_name;";
					
				}
				elseif($p_loginType==md5(PARENT_USER))
				{
					$command="select parent_id as value, parent_name as name from vts_parent";
					$command.=($GLOBALS['ID']['sess_user_type']!=AUTOGRADE_USER)?" and parent_id='".$GLOBALS['ID']['sess_parentid']."'":'';
					$command.=" order by parent_name;";
				}
				else
				{				
					return null;
				}
				$query = $this->db->query($command);
// 				log_message('debug','**'.$command);
				
					$result = $query->result_array ();
					if ($result != null)
						return $result;
					else
						return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}	
	/*
	 * This function is used to get the vehicle from the
	 * vts_vehicle_group table to fill the drop down in user view.
	 */
	public function get_allvehicleGroup($clientId)
	{
		try{
			$command="select vehicle_group_id as vh_gp_id,vehicle_group as vh_gp_name from vts_vehicle_group where md5(vehicle_group_client_id::varchar)='".$clientId."' and vehicle_group_isactive='1' order by vehicle_group;";
			//$command="select distinct(vehicle_parent_group_id) as vh_gp_id , get_vehicle_group_name(vehicle_parent_group_id) as vh_gp_name from vts_vehicle_group where md5(vehicle_group_client_id::varchar)='".$clientId."' and vehicle_parent_group_id is not null order by vh_gp_name;";			
			$query = $this->db->query($command);
// 			log_message('debug','****'.$command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;		
		}catch (Exception $ex)
		{
			log_message('error','get all vehicle group name - vts_vehicle_group');
			return null;
		}
	}
	
	
	/*
	 * To edit or insert the user from the users table
	 */
	
	public function InsertOrUpdate_user($userID,$data,$operation)
	{
		try
		{			
			if($operation=="insert")
			{
				$this->db->insert('vts_users', $data);
				return true;
			}
			else if($operation=="edit")
			{	
				$this->db->where('user_id', $userID);
				$this->db->update('vts_users', $data);
				return true;
			}			
		}catch (Exception $ex)
		{
			log_message('error insert or update  - vts_users',$ex->getMessage());
			return false;
		}
	}	
	public function delete_userVehicleGroupLink($userID)
	{
		try
		{
			$this->db->delete('vts_user_vehicle_group_link', array('vh_gp_link_user_id' => $userID));
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	public function Add_userVehicleGroupLink($userID,$vehicleGpIdList)
	{
		try
		{
			if(!empty($vehicleGpIdList))
			{
				foreach ($vehicleGpIdList as $vhGpId)			
				{
					$data = array('vh_gp_link_user_id' => $userID,'vh_gp_link_vehicle_group_id' => $vhGpId);
					$this->db->insert('vts_user_vehicle_group_link', $data);
				}
			}
				
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	public function get_userVehicleGroupLink($userID)
	{
		try{
			if($userID!=null)
			{
				$command="select vh_gp_link_vehicle_group_id from vts_user_vehicle_group_link where vh_gp_link_user_id='".$userID."';";
				$query = $this->db->query($command);		
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	//-------------------------------process of driver group--------------------
	
	/*
	 * This function is used to get the vehicle from the
	 * vts_vehicle_group table to fill the drop down in user view.
	 */
	public function get_allDriverGroup($clientId)
	{
		try{
			$command="select driver_group_id as dr_gp_id,driver_group as dr_gp_name from vts_driver_group where md5(driver_group_client_id::varchar)='".$clientId."' and driver_group_isactive='".ACTIVE."' order by driver_group;";
			//$command="select distinct(vehicle_parent_group_id) as vh_gp_id , get_vehicle_group_name(vehicle_parent_group_id) as vh_gp_name from vts_vehicle_group where md5(vehicle_group_client_id::varchar)='".$clientId."' and vehicle_parent_group_id is not null order by vh_gp_name;";
			$query = $this->db->query($command);
			//log_message('debug','****'.$command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		}catch (Exception $ex)
		{
			log_message('error','get all driver group name - vts_vehicle_group');
			return null;
		}
	}
	
	public function delete_userDriverGroupLink($userID)
	{
		try
		{
			$this->db->delete('vts_user_driver_group_link', array('dr_gp_link_user_id' => $userID));
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	public function Add_userDriverGroupLink($userID,$driverGpIdList)
	{
		try
		{
			if(!empty($driverGpIdList))
			{
				foreach ($driverGpIdList as $drGpId)
				{
					$data = array('dr_gp_link_user_id' => $userID,'dr_gp_link_driver_group_id' => $drGpId);
					$this->db->insert('vts_user_driver_group_link', $data);
				}
			}
	
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	
	public function get_userDriverGroupLink($userID)
	{
		try{
			if($userID!=null)
			{
				$command="select dr_gp_link_driver_group_id from vts_user_driver_group_link where dr_gp_link_user_id='".$userID."';";
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		} catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	/*
	 * This function is used to get the login type of the user
	 */
	public function get_Login_user_type()
	{	 	
		try
		{
			$command="select type_id, type_name from vts_user_types;";
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
			
		}catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
	/*
	 * This function is used to get the client dealer and distributor
	 */
	public function find_MoreDetails($id)
	{
		try
		{
			$command="select parent_id,client_id,client_dealer_id, dealer_distributor_id  from vts_client left outer join vts_dealer on dealer_id=client_dealer_id
left outer join vts_parent on client_id=parent_client_id where ".$id." limit 1 offset 0;";
			//log_message('debug',"***--Find_MoreDetails".$command);
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
				
		}catch ( Exception $ex )
		{
			log_message ( 'error', $ex->getMessage () );
		}
	}
}
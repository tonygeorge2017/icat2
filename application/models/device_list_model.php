<?php

class Device_list_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	public function get_device_details($data, $ToolTipDetailReq=false)
	{
		try
		{
			$command="select "; 
			$command.=($ToolTipDetailReq)?" get_distributor_name(device_distributor_id)dist_name, get_dealer_name(device_dealer_id)dealer_name, 
					dev_install_sim_provider, dev_install_is_atc_sim, dev_install_mobile_no,":"";
			$command.=" device_slno, device_imei, get_client_name(device_client_id)client_name,   
vehicle_regnumber vehicle_name, max(gps_datetime)last_data_date, min(gps_datetime)first_data_date
from vts_device left outer join vts_device_installation on dev_install_device_id=device_id and dev_install_removed_date is null 
left outer join server_gps_data on gps_imeino=device_imei left outer join vts_vehicle on dev_install_vehicle_id = vehicle_id";
			if($data!=null)
			{
				$command.=($data['imeiNo']!=null || $data['slNo']!=null ||$data['distributorID']!=null ||$data['dealerID']!=null ||$data['clientID']!=null ||$data['vehicleGroupID']!=null ||$data['vehicleID']!=null )?" where 1=1 ":" ";				
				if($data['imeiNo']!=null)
				{
					$command.=" and device_imei like '%".$data['imeiNo']."%'";					
				}
				
				if($data['slNo']!=null)
				{
					$command.=" and device_slno = '".$data['slNo']."'";					
				}				
				if($data['distributorID']!=null)
				{
					$command.=" and device_distributor_id = '".$data['distributorID']."'";					
				}
				
				if($data['dealerID']!=null)
				{
					$command.=" and device_dealer_id = '".$data['dealerID']."'";
				}
				if($data['clientID']!=null)
				{
					$command.=" and device_client_id = '".$data['clientID']."'";
				}
				if($data['vehicleGroupID']!=null)
				{
					$command.=" and vehicle_group_id = '".$data['vehicleGroupID']."'";
				}
				if($data['vehicleID']!=null)
				{
					$command.=" and dev_install_vehicle_id = '".$data['vehicleID']."'";
				}				
			} 
			$command.=" group by device_imei, dev_install_sim_provider, dev_install_is_atc_sim, 
dev_install_mobile_no, device_distributor_id, device_dealer_id, device_client_id, device_slno, 
vehicle_regnumber order by device_slno;";
			$query = $this->db->query($command);
			$result = $query->result_array ();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 * @param	 
	 *  $client - client id
	 *  $dist - distributor id
	 *  $dealerID - dealer id
	 * Return type - result array
	 */
	public function get_all_Clients($client=null,$dist=null,$dealerID=null){
		try{
			if($client!=null)
			{
				$command="select client_id, client_name from vts_client,vts_dealer,vts_distributor where dealer_distributor_id=dist_id and client_dealer_id=dealer_id 
						and client_id <> '".AUTOGRADE_USER."'";
				$command.=($client!='all')?" and (lower(client_name) like lower('".$client."%') or lower(client_name) like lower('% ".$client."%'))":"";
				$command.=($dist!=null)?" and dist_id='".$dist."'":"";
				$command.=($dealerID!=null)?" and dealer_id='".$dealerID."'":"";
				if($GLOBALS['ID']['sess_user_type'] == OHTER_CLIENT_USER)
				{
					$command.=" and client_id=".$GLOBALS['ID']['sess_clientid'];
				}
				$command.=" order by client_name;";
				$query = $this->db->query($command);
				$result = $query->result_array ();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle group the given parameter
	 * @param
	 * 	$clientID - selected client id
	 * 	$sessClient -  session client id
	 *  $sessUser - session user id
	 * Return type - result array
	 */
	public function get_all_vhGp($clientID=null)
	{
		try
		{
// 			if($GLOBALS['sessClientID']!=null)
// 			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$clientID."' order by vehicle_group;";
				else if(($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER )& $GLOBALS['sessClientID']==null)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and md5(vehicle_group_client_id::varchar)=md5('".$clientID."') order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and vehicle_group_client_id='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id, vehicle_group from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
// 			}
// 			else
// 				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	/*
	 * This function is used to get the vehicle for the given vehicle group
	 * @param
	 * 	$vehicleGpID - selected vehicel group id
	 * Return type - result array
	 */
	public function get_all_vhGp_vehicle($vehicleGpID)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{
				$command="select vehicle_id, vehicle_regnumber from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and  vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - get_all_vhGp_vehicle',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the distributor
	 * Return type - result array
	 */
	public function get_all_Distributor()
	{
		try
		{
			/*$command="select dist_id, dist_name from vts_distributor order by dist_name;";
			$query = $this->db->query($command);*/
			$this->db->order_by('dist_name','ASC');
			$this->db->select('dist_id, dist_name');
// 			$this->db->where('client_id !=', AUTOGRADE_USER);
			if($GLOBALS['ID']['sess_user_type'] == DISTRIBUTOR_USER)
			{
				$this->db->where('dist_id', $GLOBALS['ID']['sess_distributorid']);
			}
			$query = $this->db->get('vts_distributor');
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - get_all_Distributor',$ex->getMessage());
			return null;
		}
	}
	/*
	 * This function is used to get the dealer for the given distributor
	 * @param
	 * 	$distributorID - selected distributor id
	 * Return type - result array
	 */
	public function get_all_Dealer($distributorID=null)
	{
		try
		{
			$command="";
			if($distributorID!=null)
			{
				$command="select dealer_id, dealer_name from vts_dealer where dealer_distributor_id='".$distributorID."'";
				if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
				{
					$command.=" and dealer_id='".$GLOBALS['ID']['sess_dealerid']."'";
				}
				$command.=" order by dealer_name;";
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - get_all_Dealer',$ex->getMessage());
			return null;
		}
	}
}
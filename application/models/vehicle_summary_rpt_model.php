<?php
Class Vehicle_summary_rpt_model extends CI_Model{
	
	public function __construct()
	{		
		$this->load->database();
		$GLOBALS['ID'] = $this->session->userdata('login');		
	}
	
	public function get_allClient()
	{
		$this->db->order_by('client_name','ASC');
		$this->db->select('client_id, client_name');
		$this->db->where('client_id !=', AUTOGRADE_USER);
		$query = $this->db->get('vts_client');
		$result=$query->result_array();
		//log_message('debug',print_r($result,true));
		return $result;
	}

	public function get_alldriver($id)
	{
		$command="Select driver_id as id,driver_name as value from vts_driver where driver_client_id=$id order by driver_name asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_allvhgp($id)
	{
		$command="Select vehicle_group_id as id,vehicle_group as value from vts_vehicle_group where vehicle_group_client_id=$id order by vehicle_group asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
	
	public function get_allvh($id)
	{
		$command="Select vehicle_id as id,vehicle_regnumber as value from vts_vehicle where vehicle_group_id=$id order by vehicle_regnumber asc;";				
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}

	/*
	 * this function is used to get the vehicle predefined route
	 */
	public function get_vh_route($vh_id)
	{		
		$command='Select r.route_id as "id", r.route_name as "route",';		
		$command.=' sl.stop_name as "stop", stop_timelimit as "tlimit", stop_is_busstop as "bstop",'; 		
		$command.=' sl.stop_latitude as "rLat", sl.stop_longitude as "rLng" from vts_stops_list sl,  vts_vehicle_route vr, vts_route_stop rs, vts_route r where r.route_id=vr.vh_rt_route_id '; 
		$command.=" and r.route_user_define_id!='0'"; 
		$command.=" and r.route_is_active='1' ";
		$command.=" and sl.stop_id =rs.rt_stp_stop_id  ";
		$command.=" and vr.vh_rt_route_id=rs.rt_stp_route_id"; 
		$command.=" and vr.vh_rt_vehicle_id  = '$vh_id' ";
		$command.=" order by r.route_id, rs.rt_stp_slno;";
		//log_message('debug','**command** '.$command);
		$query=$this->db->query($command);
		$result = $query->result_array();	
		return $result;		
	}
	
	public function get_run_data($id, $f_date)
	{
		$time_diff=$GLOBALS['ID']['sess_time_zonediff'];
		$command="Select EXTRACT(EPOCH from(gps_datetime + interval '1 hour' * $time_diff))epo,(gps_datetime + interval '1 hour' * $time_diff) dt,gps_latitude lat,gps_longitude lng,gps_digitalinputstatus st,gps_speed spd,gps_driver_id dr from server_gps_data where gps_vehicle_id=$id and gps_datetime between (('$f_date 00:00:00')::timestamp  - interval '1 hour' * $time_diff ) and (('$f_date 23:59:59')::timestamp  - interval '1 hour' * $time_diff ) and gps_digitalinputstatus='Y';";
		//log_message('debug',$command);
		$query=$this->db->query($command);				
		$result = $query->result_array();
		return $result;
	}
}
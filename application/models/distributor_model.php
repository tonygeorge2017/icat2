<?php

class Distributor_model extends CI_Model{
	
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
	}	
	
	/**
	 * Function to insert the data to User table from distributor form
	 *
	 * $u_distributor_id   : Client id as a forgin key
	 * $user_user_name   : Contact person name
	 * $user_email : user contact email
	 * $user_is_active  : client is active field value
	 *
	 * inserts the value to DB.
	 **/
	
	public function insert_user_value($u_distributor_id, $u_user_name, $u_email, $u_is_active, $u_time_zn_id, $u_phn, $u_user_type_id, $operation) {
	
		try {
			//Data to be inserted in the DB.
			$data = array(
					'user_distributor_id'=> $u_distributor_id,
					'user_user_name' => $u_user_name,
					'user_email' => $u_email,
					'user_is_active' => $u_is_active,
					'user_is_client_admin' => '1',
					'user_is_admin' => '1',
					'user_time_zone_id' => $u_time_zn_id,
					'user_mobile' => $u_phn,
					'user_type_id' => $u_user_type_id
			);
			//Inserts the data to table EventLog
			if($operation == 'insert')
			{
				$data['user_password'] = md5(microtime() . rand());
				$this->db->insert('vts_users', $data);
				return true;
			}
			else if($operation=="edit")
			{
				$this->db->where('user_distributor_id', $u_distributor_id);
				$this->db->update('vts_users', $data);
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('user_distributor_id', $u_distributor_id);
				$this->db->delete('vts_users');
				return true;
			}
	
		}  catch (Exception $e) {
	
			log_message('error, inserting to table - vts_users',$e->getMessage());
			return;
		}
	}
	/*
	 * function to get recently added last row of distributor
	 */
	public function get_max_distributor_id()
	{
		$max_distributor_id = $this->db->select_max('dist_id');
		$query = $this->db->get('vts_distributor');
		$row = $query->row_array();
		return $row['dist_id'];
	}
	/*
	 * This function is to get the user_id inorder to send the email after creating the user successfully from client screen
	 */
	public function get_user_id($usr_email)
	{
		try
		{
			$command = "select user_id from vts_users as user_id where user_email ='".$usr_email ."' " ;
			$query = $this->db->query($command);
			$row = $query->row_array();
			return $row['user_id'];
		}
		catch(Exception $ex)
		{
			log_message('error, getting user_id error - vts_users',$e->getMessage());
			return null;
		}
	
	}
	/*
	 * This function used to return the distributor details relate the give parameter(i.e. distributor_id)
	 * if any error, then return null.
	 */
	public function get_vts_distributor($rowPerPage=null,$startNo=null,$distributor_id=null)
	{
		try
		{
			if($rowPerPage==null & $distributor_id==null)
			{
				$this->db->order_by('dist_name','ASC');
				$this->db->select('*'); //vts_distributor.distributor_id as distributor_id, vts_distributor.distributor_name as distributor_name, vts_distributor.distributor_location as distributor_location, vts_distributor.distributor_contact_phone as distributor_contact_phone, vts_distributor.distributor_contact_email as distributor_contact_email
				$this->db->from('vts_distributor');
				$query = $this->db->get();
// 				log_message('error','*****1st');
				return $query->result_array();	
			}
			else if($distributor_id==null)
			{
				$this->db->limit($rowPerPage, $startNo);
				$this->db->order_by('dist_name','ASC');
				$this->db->select('*');//vts_distributor.distributor_id as distributor_id, vts_distributor.distributor_name as distributor_name, vts_distributor.distributor_location as distributor_location, vts_distributor.distributor_contact_phone as distributor_contact_phone, vts_distributor.distributor_contact_email as distributor_contact_email
				$this->db->from('vts_distributor');
				$query = $this->db->get();
// 				log_message('error','****2nd');
				return $query->result_array();
							
			}
			else
			{
				$query=$this->db->get_where('vts_distributor',array('dist_id'=>$distributor_id));
				return $query->row_array();
			}		
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - vts_distributor',$ex->getMessage());
			return null;
		}
	}
	
	/*
	 * To insert, delete or edit the distributor details from the vts_distributor table
	 */
	
	public function InsertOrUpdateOrdelete_vts_distributor($distributor_id,$data,$operation)
	{
		try
		{
			if($operation=="insert")
			{
				$this->db->insert('vts_distributor', $data);
				return true;
			}
			else if($operation=="edit")
			{
// 				log_message("debug","update--");
				$this->db->where('dist_id', $distributor_id);
				$this->db->update('vts_distributor', $data);
// 				log_message("debug","updatedddd--");
				return true;
			}
			else if($operation=="delete")
			{
				$this->db->where('dist_id', $distributor_id);
				$this->db->delete('vts_distributor');
				return true;
			}
		}catch (Exception $ex)
		{
			log_message('error, delete or update  - vts_distributor',$ex->getMessage());
			return false;
		}
	}
	
}
?>
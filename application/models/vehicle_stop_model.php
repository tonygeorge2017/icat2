<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_stop_model extends CI_Model{
	public function __construct()
	{
		$this->load->database(); //this will load database config automatically.
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
	}
	
	/*
	 *This function is used to select the all details related to vehicle group and vehicle 
	 */
	public function get_marker_details($vhGp, $vhID)
	{
		try
		{		
			if($vhGp!=null)
			{
				$command = "SELECT stop_id, stop_name, stop_latitude, stop_longitude, stop_client_id,stop_remarks, stop_vh_group, 
						 vh_stop_id, vh_vehicle_id, vh_stops_id FROM vts_stops_list left outer join vts_vehicle_stops on 
						stop_id=vh_stops_id ";
				if($vhID!=null)
					$command .= "and vh_vehicle_id='".$vhID."' "; 
				$command .= "where stop_vh_group='".$vhGp."';";
// 				log_message('error','--query--'.$command);
				$query=$this->db->query($command);
				$result=$query->result_array();
				if($result!=null)
					return $result;
				else 
					return null;
			}
			else 
				return null;
		}catch(Exception $e)
		{
			log_message('error','error occure on get marker details function');
		}
	}
	/*
	 * This function is used to insert the stop details to vts_stops_list table.
	 */
	public function insert_stop($data,$table,$stopid=null)
	{
		try{
			if($stopid==null)//insert new stop details
			{
				$check=$this->db->insert($table, $data);
				if($check==1 && $table!='vts_vehicle_stops')
				{
					//to get the current primary key
					$query=$this->db->query("SELECT currval(pg_get_serial_sequence('vts_stops_list', 'stop_id')) as id;");
					$result=$query->row_array();
					$check=$result['id'];
				}
			}
			else
			{
				$this->db->where('stop_id', $stopid);
				$check=$this->db->update($table, $data);
			}
			//echo $check;			
			return $check;
		}catch (Exception $ex){
			log_message('error','vts_stops_list');
			return null;
		}
	}
	
	/*
	 * This function is used to add or remove the vehicle from 
	 * vts_vehicle_stops table.
	 */
	public function add_remove_vehicle($stopID, $vhID, $opt)
	{
		try
		{			
			if($opt=="add")
			{
				$data=array('vh_vehicle_id'=>$vhID,'vh_stops_id'=>$stopID);
				$check=$this->db->insert('vts_vehicle_stops', $data);
				if($check==1)
					return true;
				else 
					return false; 
			}
			else if($opt=="remove")
			{
				$this->db->where('vh_stops_id', $stopID);
				$this->db->where('vh_vehicle_id', $vhID);
				if($this->db->delete('vts_vehicle_stops'))
					return true;
				else
					return false;
			}
			
		}catch(Exception $e)
		{
			log_message("error","Error occure while add or remove vehicle from vts_vehicle_stops table");
		}
	}
	/*
	 * This function is used to get all the client from the
	 * vts_client table to fill the drop down in user view.
	 */
	public function get_allClients(){
		try
		{
			$command="select client_id, client_name from vts_client where client_id !=".AUTOGRADE_USER." order by client_name";
			$query = $this->db->query($command);
			$result=$query->result_array();
			if ($result != null)
				return $result;
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all client name - vts_client');
			return null;
		}
	}
	
	/*
	 * This function is used to get the vehicle group for the given client ID
	 */
	public function get_all_vhGp($clientID=null,$sessClient=null,$sessUser=null)
	{
		try
		{
			if($GLOBALS['sessClientID']!=null)
			{
				if($GLOBALS['sessClientID']==AUTOGRADE_USER)
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$clientID."' order by vehicle_group;";
				else if($GLOBALS['sessClientAdmin']==NOT_ACTIVE)
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group, vts_user_vehicle_group_link
						where vh_gp_link_user_id='".$GLOBALS['sessUserID']."' and vehicle_group_client_id='".$clientID."' and
						vehicle_group_isactive='".ACTIVE."' and vehicle_group_id=vh_gp_link_vehicle_group_id order by vehicle_group;";
				else
					$command="select vehicle_group_id as vh_gp_id, vehicle_group as vh_gp_name from vts_vehicle_group	where vehicle_group_isactive='".ACTIVE."' and vehicle_group_client_id='".$GLOBALS['sessClientID']."' order by vehicle_group;";
// 				log_message('error','--vhgp--'.$command);
				$query = $this->db->query($command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error','get all vehicle group related to client id and user');
			return null;
		}
	}
	
	
	public function get_all_vhGp_vehicle($vehicleGpID)
	{
		try
		{
			$command="";
			if($vehicleGpID!=null)
			{ 				
				$command="select vehicle_id as vh_id, vehicle_regnumber as vh_name from vts_vehicle where vehicle_group_id='".$vehicleGpID."' and  vehicle_isactive='".ACTIVE."' order by vehicle_regnumber;";
				$query = $this->db->query($command);
				//log_message('error','**Vehicle**'.$command);
				$result=$query->result_array();
				if ($result != null)
					return $result;
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception $ex)
		{
			log_message('error, get all detail - temp_vehicle',$ex->getMessage());
			return null;
		}
	}	
}
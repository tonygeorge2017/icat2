<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


	class Commonfunction extends CI_Controller {
		public function __construct()
		{
			parent::__construct();
			$this->load->model('common_model');
		}
	
	
		/**
		 * Created By : Abdulla Nofal
		 * Created Date : 09-May-2015
		 *
		 * Updated Date : 20-May-2015
		 *
		 * Common function to generate CSV file
		 *
		 * Params : Filename
		 * Tablename
		 * Return type : Generates the CSV file according
		 * to the params
		 */
		public function csv_create($report_title, $report_sub_title, $table_data) {  // $header, $cnt_id, $new_data
			$fp = fopen ( 'php://output', 'w' );
		
			if ($this->session->userdata ('login')) {
				$session_data = $this->session->userdata ('login');
				$sess_client_id = $GLOBALS['ID']['sess_clientid'];
			/*	if($sess_client_id == AUTOGRADE_USER)
				{
					$get_dropdown_cnt_name = $this->db->query ( "select client_name from vts_client where client_id = '" . $cnt_id . "'" );
					$row = $get_dropdown_cnt_name->row_array ();
					$client_name =  $row['client_name'];
				}
					
				$arr_client_name = array (
						"Client Name : "
						,"conditionalItem" => ($sess_client_id == AUTOGRADE_USER) ? $client_name : $session_data ['sess_client_name']
				);
				fputcsv ( $fp, $arr_client_name );*/
			} 
		
			//Remove below code
	//		$table_name = "server_gps_data";
		
			//Code to set the report title.
			$arr_report_title = array (
					"Title : ",
					$report_title
			);
			fputcsv ( $fp, $arr_report_title );
		
			//Code to set the report sub-title.
			$arr_report_sub_title = array (
					"Sub Title : ",
					$report_sub_title
			);
			fputcsv ( $fp, $arr_report_sub_title );
			
			$arr_new_line = array ();
			fputcsv ( $fp, $arr_new_line );
			
		//	fputcsv ( $fp, $new_data );
			
			
	/*		$arr_new_line = array ();
			fputcsv ( $fp, $arr_new_line );
		
			$arr_new_line = array ();
			fputcsv ( $fp, $arr_new_line );*/
		
			//Code to set the table data.
			$arr_label = array (
					"Table Data: "
			);
			fputcsv ( $fp, $arr_label );
		
			//$query_header = $this->db->query ( "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $table_name . "'" );
		
			// Logic to set the headers in CSV.
			//foreach ( $query_header->result_array () as $row ) {
				//foreach ( $row as $result ) {
					//$header [] = $result;
				//}
			//}
		
			// Set the header content.
			header ( 'Content-type: application/csv' );
			// header('Content-Disposition: attachment; filename='.$report_title.'_'.date('Ymd H:i:s').'.csv');
			header ( 'Content-Disposition: attachment; filename=' . str_replace(' ', '_', $report_title) . '_' . date ( 'Ymd' ) . '.csv' );
			// Writes the data to file.
// 			log_message("debug","header  ".$header);
		//	fputcsv ( $fp, $header );
			
			//$query = $this->db->query ( $query );
			
			// Logic to write the rows in file.
			//foreach ( $query->result_array () as $row ) {
			//log_message("debug","hhhhh---".print_r($table_data,true));

				foreach ($table_data as $row)
				{
					//$array_list = (array) $row;
					fputcsv ( $fp, $row );
				}
				
				
			$arr_new_line = array ();
			fputcsv ( $fp, $arr_new_line );
		
			$arr_new_line = array ();
			fputcsv ( $fp, $arr_new_line );
		
// 			fputcsv ( $fp, array ("Count : ",count($query->result_array ())));
			
			$arr_new_line = array ();
			fputcsv ( $fp, $arr_new_line );
		
			//Code to set the Username(from Session) and Date.
			$arr_user_name = array (
					"User : ",
					$session_data ['sess_user_name'],
					"Date: ",
					date ( 'Y-m-d' )
			);
			fputcsv ( $fp, $arr_user_name );
		}
		
		/**
		 * Send mail function
		 *
		 * @param $to
		 * @param $cc
		 * @param $bcc
		 * @param $subject
		 * @param $message
		 *
		 * return true on success, false on failure
		 *
		 **/
		public function send_mail($to, $to_name, $cc, $bcc, $subject, $message, $reply_to, $reply_to_name ) {
			
			require_once("system/helpers/PHPMailer524/class.phpmailer.php");

			$mail_details = $this->common_model->get_mail_details ();
			
			$mail = new PHPMailer(true);
			/* $send_using_gmail = true;
			//Send mail using gmail
			if($send_using_gmail){
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->SMTPAuth = true; // enable SMTP authentication
				$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
				$mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
				$mail->Port = 465; // set the SMTP port for the GMAIL server
				$mail->Username = "autogradebang@gmail.com"; // GMAIL username
				$mail->Password = "bangautograde"; // GMAIL password
			} */
			
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->SMTPAuth = true; // enable SMTP authentication
			$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
			$mail->Host = $mail_details [2] ['setting_key_value'];
			$mail->Port = $mail_details [1] ['setting_key_value'];
			$mail->Username = $mail_details [3] ['setting_key_value'];
			$mail->Password = $mail_details [0] ['setting_key_value'];
			
			//Typical mail data
			$email = $to;
			$name = $to_name ;
			$email_from = $reply_to;
			$name_from = $reply_to_name;
			
			$mail->AddAddress($email, $name);
			$mail->SetFrom($email_from, $name_from);
			$mail->AddReplyTo($reply_to, '');
			$mail->Subject = $subject;
			$mail->MsgHTML($message);
			//$mail->Body = $message;
				
			try{
				$mail->Send();
				//echo "Success!";
			} catch(Exception $e){
				//Something went bad
				//echo "Fail :(".$e;
			}
			
		}
		
		/**
		 * Function to log a mail sent/failed
		 *
		 * @param $from
		 * @param $to
		 * @param $cc
		 * @param $bcc
		 * @param $subject
		 * @param $message
		 *
		 */
		public function write_file($from, $to, $cc, $bcc, $subject, $message) {
			// Prepare file name.
			$file_name = "mail_" . $to . "_" . date ( 'Ymd His' ) . ".log";
		
			// MAIL_LOG_PATH - Path.
			$file = fopen ( MAIL_LOG_PATH . $file_name, "w" );
		
			// Writing to the file.
			fwrite ( $file, "Date :" . date ( 'Ymd' ) . "\nTime : " . date ( 'H:i:s A' ) . "\n\nFrom : " . $from . "\nTo : " . $to . "\nCC : " . $cc . "\nBCC : " . $bcc . "\nSubject : " . $subject . "\nMessage : " . $message . "\n" );
		
			// Close the file.
			fclose ( $file );
		}
		
		/**
		 * Function to draw a bar graph with co-ordinates as (date and numeric values)
		 *
		 * @param $data
		 * @param $title
		 *
		 */
		public function draw_bar_graph_date( $data ) {
			
			
			require_once ('application/libraries/JpGraph/jpgraph_bar.php');
			require_once("application/libraries/JpGraph/jpgraph_date.php");
				
			$plotArray = array ();
			$dataToX = array ();
			for($i = 0; $i < count ( $data ); $i ++) {
				array_push ( $plotArray, $data [$i] ['distance'] );
				array_push ( $dataToX,  $data [$i] ['date'] );
			}
			// Create the new graph
			$graph = new Graph(1000,600);
			$graph->SetScale('textint');
			
			$graph->ygrid->SetFill(false);
			$graph->yaxis->HideLine(false);
			
			// the x-axis labels
			$graph->SetMargin(60,40,30,130);
			$graph->title->Set ( $data[0]['title']);
			$graph->subtitle->Set ( $data[0]['sub_title'] );
			
			// Set the angle for the labels to 90 degrees
			$graph->xaxis->SetLabelAngle(60);
			$graph->xaxis->SetTickLabels($dataToX);
			$graph->xaxis->SetLabelFormatString('d M y',true);
			
			$graph->xaxis->title->Set( $data[0]['xaxis_title'] );
			$graph->yaxis->title->Set( $data[0]['yaxis_title'] );
			$graph->xaxis->title->SetMargin (-40,0,0,0);
			$graph->yaxis->title->SetMargin (20,0,0,0);
			$line = new BarPlot($plotArray);
			
			$line->SetFillColor('lightblue@0.5');
			$line->value->Show();
			$line->value->SetColor("purple");
			$line->value->SetFormat('%0.001f KMS');
			
			$graph->Add($line);
			
			$line->SetColor("white");
			$line->SetFillGradient("#4B0082","pink",GRAD_LEFT_REFLECTION);
			$line->SetWidth(45);
			
			
			$img = $graph->Stroke(_IMG_HANDLER);
			ob_start();
			imagepng($img);
			$imageData = ob_get_contents();
			ob_end_clean();
			
			return '<img src="data:image/png;base64, '. base64_encode($imageData) .'" />';
		}
		
		public function draw_line_fuel_graph_date( $data ) {
				
				
			require_once ('application/libraries/JpGraph/jpgraph_line.php');
			require_once("application/libraries/JpGraph/jpgraph_date.php");
			require_once ('application/libraries/JpGraph/jpgraph_plotline.php');
		
			$plotArray = array ();
			$dataToX = array ();
				
			for($i = 0; $i < count ( $data["data"] ); $i ++) {
				
				array_push ( $plotArray, (float)$data['data'] [$i] ['liters'] );
				array_push ( $dataToX, strtotime ( $data['data'] [$i] ['time_value'] ) );
			}
			// Create the new graph
			$graph = new Graph($data["graph_width"], $data['graph_height']);
				
			// Slightly larger than normal margins at the bottom to have room for
			// the x-axis labels
			$graph->SetMargin(60,40,30,130);
			$graph->title->Set ( $data['title'] );
			$graph->subtitle->Set (  $data['sub_title'] );
				
			// Fix the Y-scale to go between [0,100] and use date for the x-axis
			$graph->SetScale('datlin');
				
			// Set the angle for the labels to 90 degrees
			$graph->xaxis->SetLabelAngle(60);
			$graph->xaxis->title->Set( $data['xaxis_title'] );
			$graph->xaxis->title->SetMargin (-40,0,0,0);
			//$graph->xaxis->SetTextLabelInterval(3);
			$graph->xaxis->SetLabelFormatString('d M y  H:i',true);
				
			$graph->yaxis->title->Set( $data['yaxis_title'] );
			$graph->yaxis->title->SetMargin (10,0,0,0);
			
			//Set Y-axis starting point always from Zero
			$graph->yaxis->scale->SetAutoMin(0);
			
			//Set vehicle tank capacity value as maximum Y-axis value
			if( $data["tank_capacity"] > 0 )
				$graph->yaxis->scale->SetAutoMax( $data["tank_capacity"] );
			
			$line = new LinePlot($plotArray,$dataToX);
			// 			$line->SetLegend('Year 2005');
			$line->SetFillColor('lightblue@0.5');
			$graph->Add($line);
			
			$img = $graph->Stroke(_IMG_HANDLER);
			ob_start();
			imagepng($img);
			$imageData = ob_get_contents();
			ob_end_clean();
				
			return '<img src="data:image/png;base64, '. base64_encode($imageData) .'" />';
		
		}
		
		
		public function draw_line_graph_int( $data ) {
			
			require_once ('application/libraries/JpGraph/jpgraph_line.php');
			
			$datay = array(20,15,23,15);
			
			$xdata = array();
			$ydata = array();
						
			foreach( $data["data"] AS $rec ) {
				
				$ydata[] = $rec['liters'];
				$xdata[] = $rec['volt_received'];
				
			}
			// Setup the graph
			$graph = new Graph($data["graph_width"], $data["graph_height"], 'auto');
			$graph->SetScale("intlin");
			
			
			$graph->img->SetAntiAliasing(false);
			$graph->title->Set($data['title']);
			//$graph->subtitle->Set ( $data["sub_title"] );
			$graph->SetBox(false);
			
			$graph->img->SetAntiAliasing();
			
			$graph->yaxis->HideZeroLabel();
			$graph->yaxis->HideLine(false);
			$graph->yaxis->HideTicks(false,false);
			
			$graph->xgrid->Show();
			$graph->xgrid->SetLineStyle("solid");
			//$graph->xaxis->SetTickLabels( $xdata );
			$graph->xgrid->SetColor('#E3E3E3');
			
			$graph->xaxis->title->Set( $data['xaxis_title'] );
			$graph->xaxis->title->SetMargin (10,0,0,0);
			
			$graph->yaxis->title->Set( $data['yaxis_title'] );
			$graph->yaxis->title->SetMargin (1,0,0,0);
			
			// Create the first line
			$p = new LinePlot($ydata, $xdata);
			$graph->Add($p);
			$p->SetColor("#6495ED");
			//$p->SetLegend('Line 1');
			
			$graph->legend->SetFrameWeight(1);
			
			// Output line
			$img = $graph->Stroke(_IMG_HANDLER);
			ob_start();
			imagepng($img);
			$imageData = ob_get_contents();
			ob_end_clean();
			
			return '<img src="data:image/png;base64, '. base64_encode($imageData) .'" />';
			
		}
		
		/**
		 * Function to draw a bar graph
		 *
		 * @param $data
		 *
		 */
		
		public function draw_bar_graph( $data ) {
			
			// We want a bar graph, so use JpGraph's bar chart library
			require_once ('application/libraries/JpGraph/jpgraph_bar.php');
				
			$vehicle_names = array();
			$violations = array();
				
			foreach( $data["data"] AS $res ) {
				$vehicle_names[] = $res["vehicle_name"];
				$violations[] = $res["violation_count"];
			}
			
			// Create the graph. These two calls are always required
			$graph = new Graph( $data["graph_width"], $data["graph_height"],'auto');
			$graph->SetScale("textlin");
			
			//$graph->ygrid->SetColor('gray');
			$graph->ygrid->SetFill(false);
			$graph->xaxis->SetTickLabels( $vehicle_names );
			$graph->yaxis->HideLine(false);
			$graph->yaxis->HideTicks(false,false);
			
			// Create the bar plots
			$b1plot = new BarPlot( $violations );
			if( $data["isShowValues"] )
				$b1plot->value->Show();
			// ...and add it to the graPH
			$graph->Add($b1plot);
			
			
			$b1plot->SetColor("white");
			$b1plot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION);
			$b1plot->SetWidth(45);
			$graph->title->Set($data["title"]);
			$graph->subtitle->Set ( $data["sub_title"] );
			$graph->xaxis->title->Set ( $data["xaxis_title"] );
			$graph->yaxis->title->Set ( $data["yaxis_title"] );
			
			// Display the graph
			$img = $graph->Stroke(_IMG_HANDLER);
			ob_start();
			imagepng($img);
			$imageData = ob_get_contents();
			ob_end_clean();
			
			return '<img src="data:image/png;base64, '. base64_encode($imageData) .'" />';
		}
		
		
		/*
		 * Fuel sensor voltage converstion
		 * 
		 */
		public function fuel_voltage_conversion( $data ) {
			
			$sensor_port = $data['sensor_port'];
			$sesnor_input = $data['sesnor_input'];
			
			
			$scaling_factor = 0;
			$final_voltage = 0;
			
			if( $sensor_port == "24v" )		//24 Voltage sensor
				$scaling_factor = SCALING_FACTOR_A;
			else if( $sensor_port == "12v" ) 	//12 Voltage sensor
				$scaling_factor = SCALING_FACTOR_B;
			
			$final_voltage = ( ( ( $sesnor_input * VOLTAGE_REF ) / FRAME_VAL ) * $scaling_factor ) ;
			
			return $final_voltage;
		}
	
}
?>
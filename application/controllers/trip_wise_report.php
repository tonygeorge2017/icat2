<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	class Trip_wise_report extends CI_Controller {

		public function __construct() {

			parent::__construct ();

			$this->load->model("common_model");

			$GLOBALS['page_title'] = "Trip-wise Report";
			$GLOBALS['ID'] = $this->session->userdata('login');
			$session_userid = $GLOBALS['ID']['sess_userid'];

			// to get user ip and host name
			$host_name = exec ( "hostname" ); // to get "hostname"
			$host_name = trim ( $host_name ); // remove any spaces before and after
			$ip = gethostbyname ( $host_name );
			$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
		}

		/*
		 * Function to display
		 * the view
		 */
		public function index() {
			$this->display();
		}

		public function stats() {

			$data = array(
					array(
							"id"=> '1',
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 1",							
							"distance"=> "55"
					),
					array(
							"id"=> '2',
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 2",							
							"distance"=> "60"
					),
					array(
							"id"=> '3',
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 3",							
							"distance"=> "20"
					),
					array(
							"id"=> '4',
							"date"=> "2016-12-01",
							"route_name"=> "Route 3",
							"vehicle_name"=> "Vehicle 1",							
							"distance"=> "10"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-01",
							"route_name"=> "Route 3",
							"vehicle_name"=> "Vehicle 3",							
							"distance"=> "30"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-01",
							"route_name"=> "Route 3",
							"vehicle_name"=> "Vehicle 4",							
							"distance"=> "33"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle7",							
							"distance"=> "26"
					)
						
			);

			echo json_encode($data);
		}

		/*
		 * This function is used to render the view
		 */
		private function display() {
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'trip_wise_report_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer_rmc' );
		}

	}

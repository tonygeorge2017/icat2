<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parent_ctrl extends CI_Controller {
/*
 * Class constructor
 */	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('parent_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		//variables for parent modal
		$GLOBALS['parentoutcome']="";
		$GLOBALS['clientID']=null;
		$GLOBALS['parentID']=null;
		$GLOBALS['parentName']=null;
		$GLOBALS['parentEmail']=null;
		$GLOBALS['parentMobile']=null;
		$GLOBALS['parentActive']=NOT_ACTIVE;
		$GLOBALS['parentList']=array();
		$GLOBALS['pageLink']=null;
		
		//variables for student modal
		$GLOBALS['studentoutcome']="";
		$GLOBALS['parentStopID']=null;		
		$GLOBALS['studentID']=null;
		$GLOBALS['studentOldRfid']=null;
		$GLOBALS['studentRfid']=null;
		$GLOBALS['studentRfvalue']=null;
		$GLOBALS['studentName']=null;
		$GLOBALS['studentClass']=null;
		$GLOBALS['stopID']=null;		
		$GLOBALS['routeID']=null;		
		$GLOBALS['routeList']=array();
		$GLOBALS['stopList']=array();
		$GLOBALS['studentActive']=NOT_ACTIVE;
		$GLOBALS['activeRfNotify']=ACTIVE;
		$GLOBALS['activeRouteNotify']=ACTIVE;
		$GLOBALS['activeSpeedNotify']=ACTIVE;
		$GLOBALS['studentList']=array();//hold details about the childerns & their stops for selected parent
		$GLOBALS['editOrError'] = NOT_ACTIVE;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination(md5($GLOBALS['ID']['sess_clientid']));		
	}
	
	/*
	 * This function is used to validate and add the new vts_parent in database.
	 */
	public function vts_parent_validation()
	{
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		if($GLOBALS['temp']!="-1")
		{
			
			$this->table_pagination(md5($GLOBALS['temp']));
		}
		else
		{
			$this->form_validation->set_message('required', '%s required.');
			$this->form_validation->set_rules('ClientID', 'Client', 'required');			
			$this->form_validation->set_rules('ParentName', 'Parent name', 'required|callback_check_parentname');
			$this->form_validation->set_rules('ParentEmail', 'Parent email', 'required|callback_check_parentemail');
			$this->form_validation->set_rules('MobileNo', 'Mobile no', 'required|callback_check_mobile');			
			//Get the posted values
			$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null);
			$GLOBALS['parentID']=(null!=($this->input->post('ParentID'))?$this->input->post('ParentID'):null);
			$GLOBALS['parentName']=(null!=($this->input->post('ParentName'))?trim($this->input->post('ParentName')):null);
			$GLOBALS['parentEmail']=(null!=($this->input->post('ParentEmail'))?trim($this->input->post('ParentEmail')):null);
			$GLOBALS['parentMobile']=(null!=($this->input->post('MobileNo'))?trim($this->input->post('MobileNo')):null);
			$GLOBALS['parentActive']=(null!=($this->input->post('Active'))?$this->input->post('Active'):NOT_ACTIVE);
			if($this->form_validation->run()==false)
			{
				$this->table_pagination(md5($GLOBALS['clientID']));
			}
			else
			{
				$data['parent_name']=$GLOBALS['parentName'];
				$data['parent_mobile']=$GLOBALS['parentMobile'];
				$data['parent_email']=$GLOBALS['parentEmail'];
				$data['parent_client_id']=$GLOBALS['clientID'];
				$data['parent_is_active']=$GLOBALS['parentActive'];
				if($GLOBALS['parentID']==null)//insert
				{
					$data['parent_password']=mt_rand();
					$id=$this->parent_model->insert_edit_parent($data);
					if($id >= 1)
					{
						$GLOBALS['parentID']=$id;
						
						$row=$this->parent_model->find_MoreDetails(" client_id='".$GLOBALS['clientID']."'");
						if($row!=null)
						{
							$userData['user_client_id']=$GLOBALS['clientID'];
							$userData['user_user_name']=$GLOBALS['parentName'];
							$userData['user_email']=$GLOBALS['parentEmail'];
							$userData['user_password']=mt_rand();
							$userData['user_is_active']=$GLOBALS['parentActive'];
							$userData['user_is_admin']=ACTIVE;
							$userData['user_mobile']=$GLOBALS['parentMobile'];
							$userData['user_time_zone_id']=$row[0]['client_time_zone_id'];
							$userData['user_type_id']=PARENT_USER;					
							$userData['user_dealer_id']=$row[0]['client_dealer_id'];
							$userData['user_distributor_id']=$row[0]['dealer_distributor_id'];
							$userData['user_parent_id']=$GLOBALS['parentID'];
							$userid=$this->parent_model->insert_edit_parent($userData, null,"insert",true);//to insert in user table and get id
							if($userid>=1)
							{
								$GLOBALS['parentoutcome']='<div style="color: green;">New Parent has been created.</div>';
								$s_msg='Dear '.$data['parent_name'].',<br><br>&nbsp;Your email has been registered as a New Login in the Autograde T.A.N.K. System.<br>&nbsp;&nbsp;Please click the below link to set your new password and to start using the system :<br>&nbsp;&nbsp;<a href='.base_url("index.php/change_password_ctrl/view/".md5($userid)).'>Click Here</a><br><br>Thanks & Regards,<br>Autograde T.A.N.K. Support.';
								$this->load->library ( 'commonfunction' );
								$this->commonfunction->send_mail($GLOBALS['parentEmail'],$GLOBALS['parentName'],CC_TO,BCC_TO,'Vehicle Tracking System : New Parent Registered',$s_msg,REPLY_TO,'VTS Support');
								if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
									$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], PARENT_DETAILS ,"New Parent Created - Parent ID:".$GLOBALS['parentID'].", Parent Name:".$GLOBALS['parentName'].", Parent Email:".$GLOBALS['parentEmail'].",Client ID:".$GLOBALS['clientID']);
							}
							else
							{
								$GLOBALS['parentoutcome']='<div style="color: red;">New Parent login created failed.</div>';
							}
						}					
						
					}else
					{
						$GLOBALS['parentoutcome']='<div style="color: red;">New Parent creation failed.</div>';
					}
				}
				else //edit
				{
					$id=$this->parent_model->insert_edit_parent($data,$GLOBALS['parentID'],'edit');
					if($id >= 1)
					{
						$GLOBALS['parentoutcome']='<div style="color: green;">Successfully Updated.</div>';
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], PARENT_DETAILS ,"Update Parent - Parent ID:".$GLOBALS['parentID'].", Parent Name:".$GLOBALS['parentName'].", Parent Email:".$GLOBALS['parentEmail'].",Client ID:".$GLOBALS['clientID']);
					}
					else
						$GLOBALS['parentoutcome']='<div style="color: red;">Update Failed.</div>';
				}
				$this->table_pagination(md5($GLOBALS['clientID']));
			}
			
		}
	}
	/*created by shruthi
	 * function to validate username
	 */
	public function check_parentname($parentname)
	{
		if(is_numeric($parentname))
		{
			$GLOBALS['parentName']=null;
			$this->form_validation->set_message ( 'check_parentname', '%s should not be a numeric.' );
			return FALSE;
		}else
			return true;
	}
	
	/*created by shruthi
	 * function to validate mobile no
	 */
	public function check_mobile($mob)
	{
		if (empty ( $mob )) {
			$this->form_validation->set_message ( 'check_mobile', '%s required.' );
			return FALSE;
		}else if (!is_numeric($mob)) // Check whether the given value is numeric.
		{
			$GLOBALS['parentMobile']=null;
			$this->form_validation->set_message ( 'check_mobile', 'Mobile number should be numeric.' );
			return FALSE;
		}
		else if (strlen($mob) < 10 || strlen($mob) > 10) // Check whether mobile number should allow only 10 numbers
		{
			$GLOBALS['parentMobile']=null;
			$this->form_validation->set_message ( 'check_mobile', 'Please Enter 10 digit valid mobile number.' );
			return FALSE;
		}
		else
			return true;
	}/*created by shruthi
	 * function to validate Email
	 */
	public function check_parentemail($parentemail)
	{
		if(!filter_var($parentemail, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['parentEmail']=null;
			$this->form_validation->set_message('check_parentemail', 'Invalid email format.');
			return FALSE;
		}
		else if($this->parent_model->check_mail_id($parentemail,$GLOBALS['parentID'])){
			$GLOBALS['parentEmail']=null;
			$this->form_validation->set_message('check_parentemail', 'Email already exist.');
			return FALSE;
		}
		return true;
	}
	/*
	 * This function is used to validate the student details and stop details
	 */
	public function vts_student_validation()
	{
		$this->form_validation->set_message('required', '%s required.');
		$this->form_validation->set_rules('StudentID', 'Student ID', 'required');
		$this->form_validation->set_rules('StudentName', 'Student name', 'required|callback_check_studentname');
		$this->form_validation->set_rules('StudentClass', 'Student class', 'required');
		$this->form_validation->set_rules('routeID', 'Route', 'required');	
		$this->form_validation->set_rules('StopID', 'Stop', 'required');
		
			
		//Get the posted values		
		$GLOBALS['clientID']=(null!=($this->input->post('ModalClientID'))?$this->input->post('ModalClientID'):null);
		$GLOBALS['parentID']=(null!=($this->input->post('ModalParentID'))?$this->input->post('ModalParentID'):null);
		$GLOBALS['parentStopID']=(null!=($this->input->post('ModalStopID'))?$this->input->post('ModalStopID'):null);		
		$GLOBALS['studentID']=(null!=($this->input->post('StudentID'))?trim($this->input->post('StudentID')):null);
		$GLOBALS['studentRfid']=(null!=($this->input->post('StudentRFID'))?trim($this->input->post('StudentRFID')):null);
		$GLOBALS['studentOldRfid']=(null!=($this->input->post('OldId'))?trim($this->input->post('OldId')):null);
		$GLOBALS['studentRfvalue']=(null!=($this->input->post('StudentRFValue'))?trim($this->input->post('StudentRFValue')):null);
		$GLOBALS['studentName']=(null!=($this->input->post('StudentName'))?trim($this->input->post('StudentName')):null);
		$GLOBALS['studentClass']=(null!=($this->input->post('StudentClass'))?trim($this->input->post('StudentClass')):null);
		$GLOBALS['routeID']=(null!=($this->input->post('routeID'))?$this->input->post('routeID'):null);	
		$GLOBALS['stopID']=(null!=($this->input->post('StopID'))?$this->input->post('StopID'):null);					
		$GLOBALS['studentActive']=(null!=($this->input->post('Active'))?$this->input->post('Active'):NOT_ACTIVE);
		$GLOBALS['activeRfNotify']=(null!=($this->input->post('ActiveRfNotify'))?$this->input->post('ActiveRfNotify'):NOT_ACTIVE);;
		$GLOBALS['activeRouteNotify']=(null!=($this->input->post('ActiveRouteNotify'))?$this->input->post('ActiveRouteNotify'):NOT_ACTIVE);;
		$GLOBALS['activeSpeedNotify']=(null!=($this->input->post('ActiveSpeedNotify'))?$this->input->post('ActiveSpeedNotify'):NOT_ACTIVE);;
		$result=$this->parent_model->get_parent_detail(md5($GLOBALS['parentID']));
		if($result!=null)
		{
			$GLOBALS['clientID']=$result['parent_client_id'];
			$GLOBALS['parentID']=$result['parent_id'];
			$GLOBALS['parentName']=$result['parent_name'];
			$GLOBALS['parentMobile']=$result['parent_mobile'];
			$GLOBALS['parentEmail']=$result['parent_email'];
			$GLOBALS['parentActive']=$result['parent_is_active'];			
			$GLOBALS['studentList']=$this->parent_model->get_student_detail($GLOBALS['parentID']);
		}
		if($this->form_validation->run()==false)
		{	
			$GLOBALS['editOrError'] = ACTIVE;
			$this->table_pagination(md5($GLOBALS['clientID']));
		}
		else
		{
			$data['pt_stop_parent_id']=$GLOBALS['parentID'];
			$data['pt_stop_vh_stop_id']=$GLOBALS['stopID'];
			$data['pt_stop_student_name']=$GLOBALS['studentName'];
			$data['pt_stop_student_class']=$GLOBALS['studentClass'];
			$data['pt_stop_isactive']=$GLOBALS['studentActive'];
			$data['pt_stop_student_id']=$GLOBALS['studentID'];			
			$data['pt_stop_student_rfid']=$GLOBALS['studentRfid'];
			$data['pt_notify_rfid']=$GLOBALS['activeRfNotify'];
			$data['pt_notify_route_violation']=$GLOBALS['activeRouteNotify'];
            $data['pt_notify_over_speed']=$GLOBALS['activeSpeedNotify'];
			if($GLOBALS['parentStopID']==null)//insert
			{
				if($this->parent_model->insert_edit_parent_stop($data))
				{
					$GLOBALS['parentoutcome']='<div style="color: green;">Children Added Successfully.</div>';
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], PARENT_DETAILS ,
								"New Student Created - For Parent ID:".$GLOBALS['parentID'].", Student Name:".$GLOBALS['studentName'].", Class:".$GLOBALS['studentClass'].",vh stop ID:".$GLOBALS['parentStopID']);
				}
				else
					$GLOBALS['parentoutcome']='<div style="color: red;">Failed To Add.</div>';
			}
			else //edit
			{
				if($this->parent_model->insert_edit_parent_stop($data,$GLOBALS['parentStopID'],'edit'))
				{
					$GLOBALS['parentoutcome']='<div style="color: green;">Successfully Updated.</div>';
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], PARENT_DETAILS ,
								"Update Student Details - For Parent ID:".$GLOBALS['parentID'].", Student Name:".$GLOBALS['studentName'].", Class:".$GLOBALS['studentClass'].",vh stop ID:".$GLOBALS['parentStopID']);
				}
				else
					$GLOBALS['parentoutcome']='<div style="color: red;">Update Failed.</div>';
			}
			$GLOBALS['parentStopID']=null;
			$GLOBALS['studentID']=null;
			$GLOBALS['studentRfid']=null;
			$GLOBALS['studentRfvalue']=null;
			$GLOBALS['studentName']=null;
			$GLOBALS['studentClass']=null;
			$GLOBALS['stopID']=null;			
			$GLOBALS['routeID']=null;						
			$GLOBALS['routeList']=array();
			$GLOBALS['stopList']=array();	
			$GLOBALS['activeRfNotify']=ACTIVE;
			$GLOBALS['activeRouteNotify']=ACTIVE;
			$GLOBALS['activeSpeedNotify']=ACTIVE;
			$this->table_pagination(md5($GLOBALS['clientID']));
		}
	}
	/*created by shruthi
	 * function to validate username
	 */
	public function check_studentname($studentname)
	{
		if(is_numeric($studentname))
		{
			$GLOBALS['studentName']=null;
			$this->form_validation->set_message ( 'check_studentname', '%s should not be a numeric.' );
			return FALSE;
		}else if($this->parent_model->check_stud_name($studentname,$GLOBALS['parentID'],$GLOBALS['parentStopID']))
		{
			$GLOBALS['studentName']=null;
			$this->form_validation->set_message('check_studentname', '%s already exist.');
			return FALSE;
		}
			return true;
	}
	/*
	 * This function is used to fetch parent details which is 
	 * related to given parent id in md5 format
	 * @param
	 *  $p_mdParentID - parent id in md5 format
	 * Return typem - void
	 */
	public function edit_parentManagement($p_mdParentID,$p_stopID=null)
	{
		$result=$this->parent_model->get_parent_detail($p_mdParentID);
		if($result!=null)
		{
			$GLOBALS['clientID']=$result['parent_client_id'];
			$GLOBALS['parentID']=$result['parent_id'];
			$GLOBALS['parentName']=$result['parent_name'];
			$GLOBALS['parentMobile']=$result['parent_mobile'];
			$GLOBALS['parentEmail']=$result['parent_email'];			
			$GLOBALS['parentActive']=$result['parent_is_active'];
			$GLOBALS['studentList']=$this->parent_model->get_student_detail($p_mdParentID);
			if($p_stopID!=null)
			{
				$studResult=$this->parent_model->get_student_detail($p_mdParentID,$p_stopID);
				if($studResult!=null)
				{
					$GLOBALS['parentStopID']=$studResult['pt_stop_id'];					
					$GLOBALS['studentID']=$studResult['pt_stop_student_id'];
					$GLOBALS['studentRfid']=$studResult['pt_stop_student_rfid'];
					$GLOBALS['studentOldRfid']=$studResult['pt_stop_student_rfid'];
					if($GLOBALS['studentRfid']!=null)
						$GLOBALS['studentRfvalue']=$this->parent_model->get_vts_rf_card(md5($GLOBALS['clientID']),'id', $GLOBALS['studentRfid'], $GLOBALS['studentRfid']);
					$GLOBALS['studentName']=$studResult['pt_stop_student_name'];
					$GLOBALS['studentClass']=$studResult['pt_stop_student_class'];
					$GLOBALS['stopID']=$studResult['pt_stop_vh_stop_id'];					
					$GLOBALS['routeID']=$studResult['rt_stp_route_id'];
					$GLOBALS['studentActive']=$studResult['pt_stop_isactive'];
					$GLOBALS['activeRfNotify']=$studResult['pt_notify_rfid'];
					$GLOBALS['activeRouteNotify']=$studResult['pt_notify_route_violation'];
					$GLOBALS['activeSpeedNotify']=$studResult['pt_notify_over_speed'];
					$GLOBALS['editOrError'] = ACTIVE;
				}
			}
		}
		$this->table_pagination(md5($GLOBALS['clientID']));
	}	
	
	/*
	 * This function is used to get the vehicle for the selected vehicle group and
	 * return back the list of vehicles in JSON format with out refresh the page.
	 * @param
	 *  $vhGrp - vehicle group id
	 * Return type - JSON string
	 */
	public function get_stop($routeID=null)
	{
		$output="";
		if($routeID!=null)
		{
			$vehicle_stop=$this->parent_model->get_route_stop($routeID);
			if($vehicle_stop!=null)
			{
				$output=json_encode($vehicle_stop);
			}
		}
		echo($output);
	}

	public function get_existing_parent()
	{
		$result_set=array();
		$result=$this->parent_model->get_existing_parent($GLOBALS['sessClientID'], $_GET['mob'], $_GET['cid']);
		foreach ($result as $key => $value) {
			$result_set[]= array('id' => $value['parent_id'], 'value' => $value['parent_mobile'], 'pname' => $value['parent_name'], 'pemail' => $value['parent_email'], 'pactive' => $value['parent_is_active'], 'pid'=> md5($value['parent_id']));
		}
		echo json_encode($result_set);
	}
	/**
	 * This function is used to get the RF ID details for autocomplete
	 */
	public function get_rf_details()
	{		
		$result_set = array();
		if(isset($_GET['rf']))
		{			
			$result=$this->parent_model->get_vts_rf_card($_GET['cid'],'auto',$_GET['rf'],$_GET['opt']);
			if(count($result)>=1)
			{
				foreach ($result as $key => $value) {
					$result_set[]=array('id' => $value['rf_id'], 'value' => $value['rf_card_no'] );
				}
			}
		}
		echo json_encode($result_set);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($clientID=null,$pageNo=0){
	
		if($clientID!=null)
		{
			$GLOBALS['clientID']=$clientID;
		}
		if(isset($GLOBALS['clientID']))
		{
			$config['base_url'] = base_url("index.php/parent_ctrl/table_pagination/".$clientID."/");
			$config['uri_segment'] = 4;
			$config['per_page']=ROW_PER_PAGE;	
			$GLOBALS['routeList']=$this->parent_model->get_route($GLOBALS['clientID']);		
			if(isset($GLOBALS['routeID']))
				$GLOBALS['stopList']=$this->parent_model->get_route_stop($GLOBALS['routeID']);
			$GLOBALS['parentList']=$this->parent_model->get_parent_list($GLOBALS['clientID']);
			$config['total_rows'] = count($GLOBALS['parentList']);
			$GLOBALS['parentList']=$this->parent_model->get_parent_list($GLOBALS['clientID'],null,null,ROW_PER_PAGE,$pageNo);
			if($GLOBALS['parentID']!=null)
				$GLOBALS['studentList']=$this->parent_model->get_student_detail(md5($GLOBALS['parentID']));			
			$this->pagination->initialize($config);
			$GLOBALS['pageLink']= $this->pagination->create_links();
		}		
		$this->display();
	}

	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if ($this->common_model->check_session())
		{
			$this->common_model->menu_display();
			$GLOBALS['clientList']=$this->parent_model->get_allClients();
			$this->load->view('header_footer/header_individual_users',$GLOBALS);
			$this->load->view('parent_view',$GLOBALS);
			$this->load->view('header_footer/footer');
		}
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trip_register_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('trip_register_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['outcome']=null;//jsut show result
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=null;
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['tripRegID']=null;
		$GLOBALS['clientID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['driverGroupID']=null;
		$GLOBALS['driverID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['startDate']=null;
		$GLOBALS['endDate']=null;
		$GLOBALS['remarks']=null;
		$GLOBALS['clientTimeDiff']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['clientList']=null;
		$GLOBALS['driverList']=null;
		$GLOBALS['driverGroupList']=null;
		$GLOBALS['vehicleList']=null;
		$GLOBALS['vehicleGroupList']=null;		
		$GLOBALS['tripList']=null;
		$GLOBALS['shiftCategoryList'] = array();
		$GLOBALS['shiftCategoryID'] = null;
		$GLOBALS['shiftList'] = array();
		$GLOBALS['shiftID'] = null;
		//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination(md5($GLOBALS['sessClientID']));			
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function validate_trip_list()
	{
		$GLOBALS['onClientID']=(null!=($this->input->post('OnClientID'))?$this->input->post('OnClientID'):null);
		$GLOBALS['shiftCategoryID']=(null!=($this->input->post('shiftCategoryID'))?$this->input->post('shiftCategoryID'):null);
		$GLOBALS['shiftID']=(null!=($this->input->post('shiftID'))?$this->input->post('shiftID'):null);
		$GLOBALS['tripRegID']=(null!=($this->input->post('TripRegID'))?$this->input->post('TripRegID'):null);
		$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null);
		$GLOBALS['driverGroupID']=(null!=($this->input->post('DriverGroupID'))?$this->input->post('DriverGroupID'):null);
		$GLOBALS['driverID']=(null!=($this->input->post('DriverID'))?$this->input->post('DriverID'):null);
		$GLOBALS['vehicleGroupID']=(null!=($this->input->post('VehicleGroupID'))?$this->input->post('VehicleGroupID'):null);
		$GLOBALS['vehicleID']=(null!=($this->input->post('VehicleID'))?$this->input->post('VehicleID'):null);
		$GLOBALS['startDate']=(null!=($this->input->post('StartDateTime'))?trim($this->input->post('StartDateTime')):null);
		$GLOBALS['endDate']=(null!=($this->input->post('EndDateTime'))?trim($this->input->post('EndDateTime')):null);
		if($GLOBALS['startDate']!=null)
		{
			$sdate = new DateTime($GLOBALS['startDate']);
			$GLOBALS['startDate']=$sdate->format('Y-m-d H:i:s');
		}
		if($GLOBALS['endDate']!=null)
		{
			$edate = new DateTime($GLOBALS['endDate']);
			$GLOBALS['endDate']=$edate->format('Y-m-d H:i:s');
		}
		$GLOBALS['remarks']=(null!=($this->input->post('Remarks'))?trim($this->input->post('Remarks')):null);
		if($GLOBALS['onClientID']==-1)
		{
			$this->form_validation->set_message('required', '%s required.');
			$this->form_validation->set_rules('ClientID', 'Client', 'required');
			$this->form_validation->set_rules('StartDateTime', 'Start Date & Time', 'callback_check_start_date');
			$this->form_validation->set_rules('EndDateTime', 'End Date & Time', 'callback_check_end_date');
			$this->form_validation->set_rules('DriverGroupID', 'Driver group', 'required');
			$this->form_validation->set_rules('DriverID', 'Driver', 'required');
			$this->form_validation->set_rules('VehicleGroupID', 'vehicle group', 'required');
			$this->form_validation->set_rules('VehicleID', 'Vehicle', 'required');			
			// if any of the form rule is failed, then it show the error msg in view.
			if ($this->form_validation->run() == FALSE)
			{
				$this->table_pagination(md5($GLOBALS['clientID']));
			}
			else
			{
				$data['trip_register_vehicle_id']=$GLOBALS['vehicleID'];
				$data['trip_register_driver_id']=$GLOBALS['driverID'];
				$data['trip_register_time_start']=$GLOBALS['startDate'];
				if($GLOBALS['endDate']!=null)
					$data['trip_register_time_end']=$GLOBALS['endDate'];
				$data['trip_register_remarks']=$GLOBALS['remarks'];
				$data['trip_register_shift']=$GLOBALS['shiftID'];
				$data['trip_register_client_id']=$GLOBALS['clientID'];
				if($GLOBALS['tripRegID']==null)
				{
					if($this->trip_register_model->InsertOrUpdate_trip(null,$data,'insert'))
					{
						$GLOBALS['outcome']='<div style="color: green;">Registered Successfully.</div>';
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], "18",
									"insert new trip reg with driver id= ".$GLOBALS['driverID']." and vehicle id=".$GLOBALS['vehicleID']." time between '".$GLOBALS['startDate']."' to '".$GLOBALS['endDate']."' for client id=".$GLOBALS['clientID'])." and remarks=".$GLOBALS['remarks'];
						//The following $gps_data array is used to fill the server_gps_data table.
						 $gps_data['gps_vehicle_id']= $GLOBALS['vehicleID'];
						 $gps_data['gps_driver_id']= $GLOBALS['driverID'];
							//Convert the given user's start and end datetime to GMT datetime to check with
						//gps_datetime in server_gps_data table for update driver and vehicle in that.
						$startDt=$this->common_model->get_dateTime($GLOBALS['startDate'], '1');
						$endDt=($GLOBALS['endDate']!=null)?$this->common_model->get_dateTime($GLOBALS['endDate'], '1'):null;
						$this->trip_register_model->InsertOrUpdate_gpsData($GLOBALS['vehicleID'], $gps_data, $startDt, $endDt, 'edit');
						$this->clear_all_fields();
					}
					else {
						$GLOBALS['outcome']='<div style="color: red;">Please check the device and vehicle.</div>';
					}
				}
				else {
					if($this->trip_register_model->InsertOrUpdate_trip(md5($GLOBALS['tripRegID']),$data,'edit'))
					{
						$GLOBALS['outcome']='<div style="color: green;">Updated Successfully.</div>';
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], "18",
									"Update the trip reg id=".$GLOBALS['tripRegID']." with driver id= ".$GLOBALS['driverID']." and vehicle id=".$GLOBALS['vehicleID']." time between '".$GLOBALS['startDate']."' to '".$GLOBALS['endDate']."' for client id=".$GLOBALS['clientID'])." and remarks=".$GLOBALS['remarks'];
						$this->clear_all_fields();
					}
				}
				$this->table_pagination(md5($GLOBALS['clientID']));
			}
		}
		else
		{
			$this->table_pagination(md5($GLOBALS['onClientID']));
		}
	}
	/*
	 * This function is used to clear all the declared value
	 * to global variable.
	 */
	public function clear_all_fields()
	{
		$GLOBALS['tripRegID']=null;
		$GLOBALS['driverGroupID']=null;
		$GLOBALS['driverID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['startDate']=null;
		$GLOBALS['endDate']=null;
		$GLOBALS['remarks']=null;
	}
	/*
	 * This function is used to validate the given start date
	 * @param
	 *  $startdate - date & time
	 * Return type - bool
	 */
	public function check_start_date($startdate)
	{
		$client_TimeDiff=$this->trip_register_model->get_time_diff(md5($GLOBALS['clientID']));
		$diff=0;
		$dates=gmdate('Y-m-d H:i:s');
		if($client_TimeDiff!=null)
			$diff=$client_TimeDiff*60;
		$date_value=new DateTime($dates);
		$date_value=$date_value->modify($diff." minutes");
		$currentdatevalue=$date_value->format('Y-m-d H:i:s');
		if (empty($startdate)) //Check whether the device installation date field is empty
		{
			$GLOBALS['outcome']='<div style="color: red;">Start date & time required.</div>';
			return FALSE;
		}
		else if(!empty($startdate))
		{
			$start_date = new DateTime($startdate);
			$clientstartdate=$start_date->format('Y-m-d H:i:s');
// 			if($clientstartdate < $currentdatevalue & empty($GLOBALS['tripRegID']))
// 			{
// 				//$GLOBALS['outcome']='<div style="color: red;">Start date & time must be greater than or equal to todays date & time.</div>';
// 				//return FALSE;
// 				return true;
// 			}
// 			else 
				if(!empty($GLOBALS['endDate']))
			{
				$end_date = new DateTime($GLOBALS['endDate']);
				$clientenddate=$end_date->format('Y-m-d H:i:s');
				if($clientstartdate > $clientenddate)
				{
					$GLOBALS['outcome']='<div style="color: red;">Start date & time must be less than end date & time.</div>';
					return FALSE;
				}
			}
		}
		else
			return true;
	}
	/*
	 * This function is used to validate the given end date
	 * @param
	 *  $enddate - end date & time
	 * Return type - bool
	 */
	public function check_end_date($enddate)
	{
		$client_TimeDiff=$this->trip_register_model->get_time_diff(md5($GLOBALS['clientID']));
		$diff=0;
		$dates=gmdate('Y-m-d H:i:s');
		if($client_TimeDiff!=null)
			$diff=$client_TimeDiff*60;
		$date_value=new DateTime($dates);
		$date_value=$date_value->modify($diff." minutes");
		$currentdatevalue=$date_value->format('Y-m-d H:i:s');
		if (empty($enddate)) //Check whether the device installation date field is empty
		{
// 			$GLOBALS['outcome']='<div style="color: red;">End date & time required.</div>';
// 			return FALSE;
		
			if(!empty($enddate))
			{
				$end_date = new DateTime($enddate);
				$clientenddate=$end_date->format('Y-m-d H:i:s');
				if($clientenddate < $currentdatevalue)
				{
					$GLOBALS['outcome']='<div style="color: red;">End date & time must be greater than or equal to todays date & time.</div>';
					return FALSE;
				}
				else if(!empty($GLOBALS['startDate']))
				{
					$start_date = new DateTime($GLOBALS['startDate']);
					$clientstartdate=$start_date->format('Y-m-d H:i:s');
					if($clientenddate < $clientstartdate)
					{
						$GLOBALS['outcome']='<div style="color: red;">End date & time must be greater than start date & time.</div>';
						return FALSE;
					}
				}
			}
		}
		else
			return true;
	}
	/*
	 * This function is used to add or remove the given client installation date&time
	 * @param
	 *  $client - md5 of client id
	 *  $tripId -  md5 of trip id
	 *  $operation - md5 of edit or delete
	 * Return type - void
	 */
	public function manage_trip($client, $tripId, $operation)
	{
		$result=$this->trip_register_model->get_trip_details($tripId);
		if($result!=null)
		{
			$GLOBALS['tripRegID']=$result['trip_id'];
			$GLOBALS['clientID']=$result['trip_register_client_id'];
			$GLOBALS['vehicleGroupID']=$result['vehicle_gp_id'];
			$GLOBALS['driverGroupID']=$result['driver_gp_id'];
			$GLOBALS['driverID']=$result['trip_register_driver_id'];
			$GLOBALS['vehicleID']=$result['trip_register_vehicle_id'];
			$GLOBALS['startDate']=$result['trip_register_time_start'];
			$GLOBALS['endDate']=$result['trip_register_time_end'];
			$GLOBALS['remarks']=$result['trip_register_remarks'];
			$GLOBALS['shiftCategoryID'] = $result['shift_category'];
			$GLOBALS['shiftID'] = $result['trip_register_shift'];
		}
		if(md5('edit')==$operation)
		{			
			$this->table_pagination($client);
		}
		elseif (md5('delete')==$operation)
		{
			if($this->trip_register_model->InsertOrUpdate_trip($tripId,null,'delete'))
			{
				$GLOBALS['outcome']='<div style="color: green;">Deleted Successfully.</div>';
				if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
					$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], "18",
									"Delete the trip reg id=".$GLOBALS['tripRegID']." with driver id= ".$GLOBALS['driverID']." and vehicle id=".$GLOBALS['vehicleID']." time between '".$GLOBALS['startDate']."' to '".$GLOBALS['endDate']."' for client id=".$GLOBALS['clientID'])." and remarks=".$GLOBALS['remarks'];
			}
			else
			{
				$GLOBALS['outcome']='<div style="color: red;">Sorry.</div>';
			}
			$this->clear_all_fields();
			$this->table_pagination($client);
		}
	}
	/*
	 *This function is used to fill the drop down list box in
	 * view depending upon the device type selection without refresh the page.
	 * @param
	 *  $tripid - trip id
	 *  $drivergpid - driver id
	 *  $startDate -  start date
	 *  $endDate - end date
	 * Return type - void
	 */
	public function get_driver($tripid, $drivergpid, $startDate, $endDate=null)
	{
		$sdate = new DateTime($startDate); 
		$sdate=$sdate->format('Y-m-d H:i:s');
		$edate=null;
		$output="";
		if($endDate!=null)
		{
			$edate = new DateTime($endDate);
			$edate=$edate->format('Y-m-d H:i:s');
		}
		$output=json_encode($this->trip_register_model->get_allDriver($tripid, $drivergpid, $sdate, $edate));	
		echo($output);
	}
	/*
	 * This function is used to fill the drop down list box in
	 * view depending upon the vehicle group selection without refresh the page.
	 * @param
	 *  $tripid - trip id
	 *  $vhGp - vehicle group id
	 *  $startDate -  start date
	 *  $endDate - end date
	 * Return type - void
	 */
	public function get_vhehicle_gp($tripid, $vhGp, $startDate, $endDate=null)
	{
		$sdate = new DateTime($startDate); 
		$sdate=$sdate->format('Y-m-d H:i:s');
		$edate=null;
		$output="";
		if($endDate!=null)
		{
			$edate = new DateTime($endDate);
			$edate=$edate->format('Y-m-d H:i:s');
		}
		$output=json_encode($this->trip_register_model->get_VehicleList($tripid, $vhGp, $sdate, $edate));	
		echo($output);
	}
	/*
	This function is used to fetch the shift details onchange of
	shift category by the user
	@param
	$id - shift category id selected by the user.
	*/	
	public function get_shift($id)
	{
		$output=json_encode($this->trip_register_model->get_shift($id));
		echo $output;
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $client - client id
	 *  $pageNo -  page no
	 * Return type - void
	 */
	public function table_pagination($client, $pageNo=0)
	{
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		if(!empty($GLOBALS['sessClientID']) || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$GLOBALS['clientID']=$client;
			$GLOBALS['shiftCategoryList'] = $this->trip_register_model->get_shift_category($client);
			$GLOBALS['shiftList'] = $this->trip_register_model->get_shift($GLOBALS['shiftCategoryID']);
			$GLOBALS['clientTimeDiff']=$this->trip_register_model->get_time_diff($client);
			$GLOBALS['driverGroupList']=$this->trip_register_model->get_allDriverGroup($GLOBALS['clientID']);
			$GLOBALS['clientList']=$this->trip_register_model->get_allClients();
			$GLOBALS['vehicleGroupList']=$this->trip_register_model->get_allVehicleGroup($GLOBALS['clientID']);
			$tripid=($GLOBALS['tripRegID']!=null)?$GLOBALS['tripRegID']:-1;
			$GLOBALS['vehicleList']=$this->trip_register_model->get_VehicleList($tripid, $GLOBALS['vehicleGroupID'], $GLOBALS['startDate'], $GLOBALS['endDate']);			
			$GLOBALS['driverList']=$this->trip_register_model->get_allDriver($tripid, $GLOBALS['driverGroupID'], $GLOBALS['startDate'], $GLOBALS['endDate']);
			$config['uri_segment']=4;
			$config['base_url'] = base_url("index.php/trip_register_ctrl/table_pagination/".$GLOBALS['clientID']."/");
			$GLOBALS['tripList']=$this->trip_register_model->get_tripList($GLOBALS['sessClientID'],$GLOBALS['clientID'],null,null,$GLOBALS['sessUserID']);
			$config['total_rows'] = count($GLOBALS['tripList']);
			$config['per_page']=ROW_PER_PAGE;
			$GLOBALS['tripList']=$this->trip_register_model->get_tripList($GLOBALS['sessClientID'],$GLOBALS['clientID'], ROW_PER_PAGE, $pageNo, $GLOBALS['sessUserID']);
			$this->pagination->initialize($config);
			$GLOBALS['pageLink']= $this->pagination->create_links();
			$this->display($GLOBALS);
		}
		else
		{
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
	/*
	 * This function is used to render the view wiht give $date as argument.
	 */
	private function display($data)
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('trip_register_view',$data);
		$this->load->view('header_footer/footer');
	}
}
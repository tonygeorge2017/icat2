<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Parent_tracking_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessParentID']=$GLOBALS['ID']['sess_parentid'];
		$GLOBALS['clientID']=null;
		$GLOBALS['studentList']=array();
		$this->load->model('parent_tracking_model');		
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$GLOBALS['studentList']=$this->parent_tracking_model->get_student();
		$this->display();
	}
	
	public function get_student()
	{
		$result=$this->parent_tracking_model->get_student();
		echo json_encode($result);
	}
	
	/*
	public function fetch_track_data()
	{
		$result=$this->parent_tracking_model->live_track($_GET['id'], $_GET['dt']);
		echo json_encode($result);
	}
	*/
	
	public function fetch_track_data()
	{
		$curr_utc_datetime = new DateTime();
		$live_date=$this->common_model->get_dateTime($curr_utc_datetime->format('Y-m-d').' 00:00:00', 1);
		//$live_date = '2017-11-13 00:00:00';
		$result=$this->parent_tracking_model->live_track($_GET['id'], $live_date);
		echo json_encode($result);
	}

	public function get_swiped_vehicles()
	{		
		$f_date=$this->common_model->get_dateTime($_GET['dt'].' 00:00:00', 1);
		$t_date=$this->common_model->get_dateTime($_GET['dt'].' 23:59:59', 1);
		$result=$this->parent_tracking_model->swiped_vehicles($_GET['id'], $f_date, $t_date);	
		echo json_encode($result);
	}

	public function fetch_hist_track()
	{	
		$result = array();
		if(isset($_GET['id']) && isset($_GET['dt']))	
		{
			$f_date=$this->common_model->get_dateTime($_GET['dt'].' 00:00:00',  1);
			$t_date=$this->common_model->get_dateTime($_GET['dt'].' 23:59:59',  1);
			$result=$this->parent_tracking_model->history_tracking($_GET['id'], $f_date, $t_date);
		}
		echo json_encode($result);
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if($this->common_model->check_session())
		{
			$this->common_model->menu_display();			
			$this->load->view('parent_tracking_view',$GLOBALS);
		}
	}
}
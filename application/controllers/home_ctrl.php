<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Home_ctrl extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );
		$this->load->model ( 'home_model' );
		$this->common_model->check_session ();
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['clientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS['spot']=array();
	}
	
	public function index() {
		$this->display ();
	}
	
	public function view() {
		$GLOBALS ['clientID'] = (null != trim ( $this->input->post ( 'ClientID' ) ) ? trim ( $this->input->post ( 'ClientID' ) ) : null);
		$this->display ();
	}
	public function dashboard($clid) {		
		$data = $this->home_model->get_run_data($clid);		
		echo json_encode($data);
	}

	public function insurance($clid, $diff=15) {	
		$data = $this->home_model->get_insurance_alert($clid, $diff);		
		echo json_encode($data);
	}
	
	public function shift($clid, $diff=15) {	
		$data = $this->home_model->get_shift_alert($clid, $diff);		
		echo json_encode($data);
	}

	public function digitalIO($clid) {	
		$data = $this->home_model->get_digitalIO_alert($clid);		
		echo json_encode($data);
	}

	public function display() {
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['timezone'] = ($GLOBALS ['ID'] ['sess_time_zonename']) ? $GLOBALS ['ID'] ['sess_time_zonename'] : ''; 
			$GLOBALS ['clientList'] = $this->common_model->get_allClients ();
			$this->common_model->menu_display ();
			}
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'home_view', $GLOBALS ); // load view Home page
		$this->load->view ( 'header_footer/footer_rmc' );
	}
	
	public function logout() {
		$this->session->unset_userdata ( 'login' );
		$this->session->sess_destroy ();
		redirect ( 'login_ctrl', 'refresh' );
	}
}
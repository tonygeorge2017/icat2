<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	class Vehicle_stoppage_report extends CI_Controller {

		public function __construct() {

			parent::__construct ();

			$this->load->model("common_model");

			$GLOBALS['page_title'] = "Vehicle Stoppage Report";
			$GLOBALS['ID'] = $this->session->userdata('login');
			$session_userid = $GLOBALS['ID']['sess_userid'];

			// to get user ip and host name
			$host_name = exec ( "hostname" ); // to get "hostname"
			$host_name = trim ( $host_name ); // remove any spaces before and after
			$ip = gethostbyname ( $host_name );
			$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
		}

		/*
		 * Function to display
		 * the view
		 */
		public function index() {
			$this->display();
		}

		public function stats() {

			$data = array(
					array(
							"id"=> '1',
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					),
					array(
							"id"=> '2',
							"date"=> "2016-12-02",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					),
					array(
							"id"=> '3',
							"date"=> "2016-12-03",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					),
					array(
							"id"=> '4',
							"date"=> "2016-12-04",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-05",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-05",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-05",
							"route_name"=> "Route 1",
							"on_time"=> "5",
							"off_time"=> "3",
							"total_time"=> "8",
							"distance"=> "50"
					)
						
			);

			echo json_encode($data);
		}

		/*
		 * This function is used to render the view
		 */
		private function display() {
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'vehicle_stoppage_report_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer_rmc' );
		}

	}

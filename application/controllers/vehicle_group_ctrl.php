<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle_group_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('vehicle_group_model');
		$this->load->helper(array('form', 'url'));
		
		//the below data will save to the vts_vehicle_group table
		$GLOBALS['vcle_group_id']=null;
		$GLOBALS['vcle_group']=null;
		$GLOBALS['vcle_parent_group_id']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['vcle_group_client_id']=null;
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['vcle_group_client_id']=$GLOBALS['ID']['sess_clientid'];
		
		$GLOBALS['parentItemGroupList']=$this->vehicle_group_model->get_all_parent_vehicle_group($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['vehicleGroupList']=$this->vehicle_group_model->get_vts_vehicle_group(null,null,null,md5($GLOBALS['ID']['sess_clientid']));
		$GLOBALS['clientList']=$this->vehicle_group_model->get_allClients();
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	/*
	 * This function is to desplay the index page
	 */
	public function index()
	{
		$this->table_pagination(md5($GLOBALS['vcle_group_client_id']));
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_vehicle in database.
	 */
	public function vts_vehicle_group_validation()
	{
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		if($GLOBALS['temp']!="-1")
			$this->table_pagination(md5($GLOBALS['temp']));
		else 
		{
		$GLOBALS['vcle_group_id']=(null!=trim($this->input->post('VehicleGroupId'))?trim($this->input->post('VehicleGroupId')):null);
		$GLOBALS['vcle_group']=(null!=trim($this->input->post('VehicleGroup'))?trim($this->input->post('VehicleGroup')):null);
		$GLOBALS['vcle_parent_group_id']=(null!=trim($this->input->post('ParentGroup'))?trim($this->input->post('ParentGroup')):null);
		$GLOBALS['active']=(null!=($this->input->post('VehicleGroupIsActive'))?$this->input->post('VehicleGroupIsActive'):NOT_ACTIVE);
		
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$GLOBALS['vcle_group_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		}
		else
			$GLOBALS['vcle_group_client_id']=$GLOBALS['ID']['sess_clientid'];
		
		$GLOBALS['temp']=$GLOBALS['vcle_group_client_id'];
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('VehicleGroup', 'Vehicle Group', 'callback_check_vehicle_group');

		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_cnt_id');
		}
		
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
		{
			$this->table_pagination(md5($GLOBALS['vcle_group_client_id']));
		}
		else
		{
			$data['vehicle_group']=$GLOBALS['vcle_group'];
			$data['vehicle_parent_group_id']=$GLOBALS['vcle_parent_group_id'];
			$data['vehicle_group_isactive']=$GLOBALS['active'];
			$data['vehicle_group_client_id']=$GLOBALS['vcle_group_client_id'];
			
			if($GLOBALS['vcle_group_id']!=null) //edit
			{
				$data['vehicle_group_id']=$GLOBALS['vcle_group_id'];
				if ($this->vehicle_group_model->InsertOrUpdateOrdelete_vts_vehicle_group($GLOBALS['vcle_group_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_GROUP,
								"Updated Vehicle Group: Vehicle Grop ID=".trim($GLOBALS['vcle_group_id']).", Vehicle Group=".trim($GLOBALS['vcle_group']).",Vehicle Parent Group Id=".trim($GLOBALS['vcle_parent_group_id']).", Vehicle Is Active=".trim($GLOBALS['active']).", Vehicle Group Client Id=".trim($GLOBALS['vcle_group_client_id']));
	
						$GLOBALS['outcome']="Vehicle Group ".'"'.trim($GLOBALS['vcle_group']) .'"'." updated successfully";
						$GLOBALS['temp']=$GLOBALS['vcle_group_client_id'];
						$GLOBALS['vcle_group_id']=null;
						$GLOBALS['vcle_parent_group_id']=null;
						$GLOBALS['active']=null;
				}
			}
			else	//insert
			{
				if($this->vehicle_group_model->InsertOrUpdateOrdelete_vts_vehicle_group(null,$data,'insert'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_GROUP,
								"Created New Vehicle Group: Vehicle Group=".trim($GLOBALS['vcle_group']).",Vehicle Parent Group Id=".trim($GLOBALS['vcle_parent_group_id']).", Vehicle Is Active=".trim($GLOBALS['active']).", Vehicle Group Client Id=".trim($GLOBALS['vcle_group_client_id']));
	
							$GLOBALS['outcome']="New Vehicle Group ".'"'.trim($GLOBALS['vcle_group']).'"'." created successfully";
							$GLOBALS['temp']=$GLOBALS['vcle_group_client_id'];
							$GLOBALS['vcle_group_id']=null;
							$GLOBALS['vcle_parent_group_id']=null;
							$GLOBALS['active']=null;
				}
			}
			$this->table_pagination(md5($GLOBALS['temp']));
		}
		}
	}
	
	/*
	 * This function is to check the client id, only for autograde users
	 */
	public function check_cnt_id($cnt_id)
	{
		if (empty($cnt_id))
		{
			$this->form_validation->set_message('check_cnt_id', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_group_client_id']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_vehicle_group name
	 */
	public function check_vehicle_group($vcle_grp)
	{
		$GLOBALS['vehicleGroupList']=$this->vehicle_group_model->get_vts_vehicle_group(null,null,null,md5($GLOBALS['temp']));
		$GLOBALS['vcle_group']=null;
		$vcle_grp1=str_replace(" ", "", $vcle_grp);
		if (empty($vcle_grp1)) //Check whether the Vehicle name field is empty
		{
			$this->form_validation->set_message('check_vehicle_group', '%s required');
			return FALSE;
		}
		else if (is_numeric($vcle_grp1)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_vehicle_group', '%s should not be numeric' );
			return FALSE;
		}
		else if (strlen($vcle_grp1) < MIN_FIELD_LENGTH || strlen($vcle_grp1) > MAX_FIELD_LENGTH ) // Check whether the item group name is between 5 to 20 characters
		{
			$this->form_validation->set_message ( 'check_vehicle_group', 'Enter 5-20 characters name' );
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $vcle_grp1))
		{
			$this->form_validation->set_message ( 'check_vehicle_group', 'Please enter valid name' );
			return FALSE;
		}
		else if(preg_match("/[+]/", $vcle_grp1))
		{
			$this->form_validation->set_message ( 'check_vehicle_group', 'Please enter valid name' );
			return FALSE;
		}
		else if(preg_match('/^[0-9]/', $vcle_grp1))
		{
			$this->form_validation->set_message ( 'check_vehicle_group', 'Please enter valid group name' );
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['vehicleGroupList'] as $row ) {
				if ($GLOBALS ['vcle_group_id'] == null) {
					if (trim ( strtoupper ( $row ['vehicle_group'] ) ) == strtoupper ( $vcle_grp )) // Check whether itemgroupname already exist in itemgroup table
					{
						$GLOBALS['outcome_with_db_check']="Vehicle Group ".'"'.trim($row['vehicle_group']).'"'." already exist";
						$this->form_validation->set_message('check_vehicle_group', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['vehicle_group'] ) ) == strtoupper ( $vcle_grp ) && $GLOBALS ['vcle_group_id'] != trim ( $row ['vehicle_group_id'] )) // Check whether vehicel group id already exist in vts_vehicel_group table while update
					{
						$GLOBALS['outcome_with_db_check']="Vehicle Group ".'"'.trim($row['vehicle_group']).'"'." already exist";
						$this->form_validation->set_message('check_vehicle_group', '');
						return FALSE;
					}
				}
			}
		}
		$GLOBALS['vcle_group']=(null!=trim($this->input->post('VehicleGroup'))?trim($this->input->post('VehicleGroup')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_vehicle_group
	 */
	public function editOrDelete_vts_vehicle_group($id,$opt,$vhgpId)
	{
		$operation=$opt;	//it hold the operation to perform(i.e. edit or delete).
		$VehicleGroupId=$vhgpId;	//it hold the vehicle group id to edit or delete.
	
		$row=$this->vehicle_group_model->get_vts_vehicle_group(null,null,$VehicleGroupId,$id);
		if($operation==md5("edit"))
		{
			$operation=="edit";
			$GLOBALS=array('vcle_group_id'=>trim($row['vehicle_group_id']),'vcle_group'=>trim($row['vehicle_group']),'vcle_parent_group_id'=>trim($row['vehicle_parent_group_id']),'active'=>trim($row['vehicle_group_isactive']),'vcle_group_client_id'=>trim($row['vehicle_group_client_id']));//It used to fill up the textbox in view.
		}
		else if($operation==md5("delete"))
		{
			try
			{
				if($this->vehicle_group_model->InsertOrUpdateOrdelete_vts_vehicle_group($VehicleGroupId,null,"delete"))
				{
					$GLOBALS['outcome']="Vehicle Group ".'"'.trim($row['vehicle_group']).'"'." deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_GROUP,
								"Vehicle Group is deleted: Vehicle Grop ID=".trim($GLOBALS['vcle_group_id']).", Vehicle Group=".trim($GLOBALS['vcle_group']).",Vehicle Parent Group Id=".trim($GLOBALS['vcle_parent_group_id']).", Vehicle Is Active=".trim($GLOBALS['active']).", Vehicle Group Client Id=".trim($GLOBALS['vcle_group_client_id']));
				}
				else
				{
					$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['vehicle_group'] ).'"' . " Can't able to delete(Referred some where else)";
				}
			}catch (Exception $ex){
				$this->table_pagination($id);
			}
		}
		$this->table_pagination($id);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id=null,$pageNo=0)
	{
		if($cl_id!=null)
		{
			$GLOBALS['vcle_group_client_id']=$cl_id;
		}
		$GLOBALS['vehicleGroupList']=$this->vehicle_group_model->get_vts_vehicle_group(null,null,null,$GLOBALS['vcle_group_client_id']);
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['parentItemGroupList']=$this->vehicle_group_model->get_all_parent_vehicle_group($GLOBALS['vcle_group_client_id']);
		$config['base_url'] = base_url("index.php/vehicle_group_ctrl/table_pagination/".$GLOBALS['vcle_group_client_id']);
		$config['uri_segment'] = URI_SEGMENT_FOR_FOUR;
		$GLOBALS['clientList']=$this->vehicle_group_model->get_allClients();
		
		$config['total_rows'] = count($GLOBALS['vehicleGroupList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['vehicleGroupList']=$this->vehicle_group_model->get_vts_vehicle_group(ROW_PER_PAGE,$pageNo,null,$GLOBALS['vcle_group_client_id']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('vehicle_group_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
}

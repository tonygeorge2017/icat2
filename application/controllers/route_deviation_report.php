<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	class Route_deviation_report extends CI_Controller {

		public function __construct() {

			parent::__construct ();

			$this->load->model("common_model");

			$GLOBALS['page_title'] = "Route Deviation Report";
			$GLOBALS['ID'] = $this->session->userdata('login');
			$session_userid = $GLOBALS['ID']['sess_userid'];

			// to get user ip and host name
			$host_name = exec ( "hostname" ); // to get "hostname"
			$host_name = trim ( $host_name ); // remove any spaces before and after
			$ip = gethostbyname ( $host_name );
			$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
		}

		/*
		 * Function to display
		 * the view
		 */
		public function index() {
			$this->display();
		}

		public function stats() {

			$data = array(
					array(
							"id"=> '1',
							"date"=> "2016-12-03",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 1",
							"spot"=> "Place name"
					),
					array(
							"id"=> '2',
							"date"=> "2016-12-03",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 1",
							"spot"=> "Place name"
					),
					array(
							"id"=> '3',							
							"date"=> "2016-12-03",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 1",
							"spot"=> "Place name"							
					),
					array(
							"id"=> '4',							
							"date"=> "2016-12-03",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 1",
							"spot"=> "Place name"
					),
					array(
							"id"=> '5',
							"date"=> "2016-12-03",
							"route_name"=> "Route 1",
							"vehicle_name"=> "Vehicle 1",
							"spot"=> "Place name"
					)				
						
			);

			echo json_encode($data);
		}

		/*
		 * This function is used to render the view
		 */
		private function display() {
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'route_deviation_report_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer_rmc' );
		}

	}

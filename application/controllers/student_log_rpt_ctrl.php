<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Student_log_rpt_ctrl extends CI_Controller{
	
	private $data = array('sessClientID'=>null, 'clientID'=>null, 'ID'=>null, 'timezone'=>null);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');		
		$this->common_model->check_session();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$this->data['sessClientID'] = $GLOBALS['ID']['sess_userid'];
		$this->data['ID'] = $GLOBALS['ID'];
		$this->data['timezone'] = $GLOBALS['ID']['sess_time_zonename'];
		$this->load->model('student_log_rpt_model');		
	}
	
	public function index()
	{
		log_message('debug', '**Student_log_rpt_ctrl**index'.$this->data['sessClientID']);
		$this->display($this->data);		
	}
	
	public function get_client()
	{		
		$result=json_encode($this->student_log_rpt_model->get_allClient());
		//log_message('debug',$result);
		echo $result;
	}
	
	public function get_vehiclegp($id)
	{
		if($id)
		{			
			$result=array();
			$result=$this->student_log_rpt_model->get_allvhgp($id);
			if(count($result) >= 1)
				$result=json_encode($result);
			else
				$result=json_encode(array('id'=>null,'value'=>'-- No Group Found --'));
			//log_message('debug',$result);
			echo $result;
		}
		else{
			echo json_encode(array('id'=>null,'value'=>'-- Client Required --'));
		}
	}	

	public function get_vehicle($id)
	{
		if($id)
		{
			$result=array();
			$result=$this->student_log_rpt_model->get_allvh($id);
			if(count($result) >= 1)
				$result=json_encode($result);
			else
				$result=json_encode(array('id'=>null,'value'=>'-- No Vehicle Found --'));
			//log_message('debug',$result);
			echo $result;
		}
		else{
			echo json_encode(array('id'=>null,'value'=>'-- Vehicle Group Required --'));
		}
	}
	
	public function get_rfid_log()
	{
		$result = array('result' => array(), 'error' => array());
		$vh_gp = $this->input->get('vh_gp');
		$fdate = $this->input->get('fdate');
		$tdate = $this->input->get('tdate');
		$vh = $this->input->get('vh');
		$result['result'] = $this->student_log_rpt_model->get_log($vh_gp, $fdate, $tdate, $vh);
		echo json_encode($result);
	}
	
	public function display($data)
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$data);
		$this->load->view('student_log_rpt_view',$data);
		$this->load->view('header_footer/footer',$data);
	}
}
?>
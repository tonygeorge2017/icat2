<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fuel_coupon_ctrl extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");
		$this->load->model('fuel_coupon_model');
		$GLOBALS['ID']=$this->session->userdata('login');
		$session_userid = $GLOBALS['ID']['sess_userid'];
		$GLOBALS['sess_clientid']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sess_user_type']=$GLOBALS['ID']['sess_user_type'];			
		$GLOBALS['pageLink']=null;
		if($GLOBALS['sess_user_type']==AUTOGRADE_USER)
			$GLOBALS['clientID']=$GLOBALS['ID']['sess_select_client'];
		else
			$GLOBALS['clientID']=$GLOBALS['sess_clientid'];
		$GLOBALS['sess_user_id']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['clientChangeID']=null;
		$GLOBALS['fuel_list']=array();
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";				
	}

	public function index()
	{		
		$this->table_pagination();
	}

	public function client_change()
	{
		$GLOBALS['clientID']=$this->input->post('clientChangeID');
		$GLOBALS['ID']['sess_select_client']=$GLOBALS['clientID'];
		$this->session->set_userdata('login',$GLOBALS['ID']);
		$this->table_pagination();
	}

	public function get_client()
	{		
		//$this->load->model('fuel_coupon_model');
		$result=json_encode($this->fuel_coupon_model->get_allClient());
		//log_message('debug',$result);
		echo $result;
	}

	public function get_vehicle_gp($id)
	{
		$result=json_encode($this->fuel_coupon_model->get_vhgp($id));
		//log_message('debug',$result);
		echo $result;
	}

	public function get_vehicle($id)
	{
		$result=json_encode($this->fuel_coupon_model->get_vhehicle($id));
		//log_message('debug',$result);
		echo $result;
	}

	public function get_driverGp($id)
	{
		$result=json_encode($this->fuel_coupon_model->get_driverGp($id));
		//log_message('debug',$result);
		echo $result;
	}

	public function get_driver($id)
	{
		$result=json_encode($this->fuel_coupon_model->get_driver($id));
		//log_message('debug',$result);
		echo $result;
	}

	public function get_last($id)
	{
		$result=json_encode($this->fuel_coupon_model->get_last($id));
		//log_message('debug',$result);
		echo $result;
	}

	public function get_fuelBunk($id)
	{
		$result=json_encode($this->fuel_coupon_model->get_fuelBunk($id));
		//log_message('debug',$result);
		echo $result;
	}

	public function get_rundata($id, $f_date,$startBit)
	{				
		$result=array();
		$result=$this->fuel_coupon_model->get_run_data($id, $f_date, $startBit);
		if(count($result) >= 1)
			$result=json_encode($result);
		else
			$result=json_encode(array());
		//log_message('debug',$result);
		echo $result;		
	}

	public function save_coupon($vhgp,$vh,$dr,$bnk,$fuel,$usr,$rmfuel,$print,$bunkplace)
	{
		$result=$this->fuel_coupon_model->save_coupon($vhgp,$vh,$dr,$bnk,$fuel,$usr,$rmfuel,$print,urldecode($bunkplace));
		if($print == "now")
			$result= $this->fuel_coupon_model->get_current_coupon($result);			 
		else if($print == "today")
			$result= $this->fuel_coupon_model->get_coupon($vhgp, date("Y-m-d"), $print);
		//log_message('debug',$result);		
		echo json_encode($result);		
	}

	public function get_coupon($id, $selected_date)
	{
		$result=$this->fuel_coupon_model->get_coupon($id, $selected_date);
		echo json_encode($result);
	}

	public function table_pagination($page=0)
	{		
		$row_cnt=$this->fuel_coupon_model->get_row_count();		
		$GLOBALS['fuel_list']=$this->fuel_coupon_model->get_fuel_coupon($page);
		$config['base_url'] = base_url("index.php/fuel_coupon_ctrl/table_pagination/");		
		$config['total_rows'] = $row_cnt;
		$config['per_page']=ROW_PER_PAGE;
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();		
		$this->display();
	}

	public function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('fuel_coupon_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}

}
?>
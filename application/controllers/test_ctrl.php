<?php
class Test_ctrl extends CI_Controller {
	public function __construct() {
		
		parent::__construct ();
		$this->load->model("common_model");
		$this->load->model("test_model");
		$this->load->library ( 'session' );
		
		$this->common_model->check_session();
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$session_userid = $GLOBALS['ID']['sess_userid'];
		
		// to get user ip and host name
		$host_name = exec ( "hostname" ); // to get "hostname"
		$host_name = trim ( $host_name ); // remove any spaces before and after
		$ip = gethostbyname ( $host_name );
		$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
	}
	
	public function index() {
		
		$this->common_model->menu_display ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'test_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer_rmc' );
		
	}
	
	public function get_vehicle_report_data() {
		echo $this->test_model->get_vehicle_report_data ();
	}
	
}
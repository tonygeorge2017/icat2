<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_stop_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		if($this->common_model->check_session())
		{
			$GLOBALS['ID'] = $this->session->userdata('login');
			$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
			$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		}
		$GLOBALS['clientID']="";
		$GLOBALS['vehicleGroupList']=array();
		$GLOBALS['vehicleList']=array();
		$GLOBALS['stopID']="";
		$GLOBALS['stopName']="";
		$GLOBALS['vehicleGroupID']="";
		$GLOBALS['vehicleID']="";
		$GLOBALS['latitude']="";
		$GLOBALS['longitude']="";
		$GLOBALS['remarks']="";
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['editOrError']=NOT_ACTIVE;
		$this->load->model('vehicle_stop_model');
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$GLOBALS['clientID']=($GLOBALS['sessClientID']!=AUTOGRADE_USER)?$GLOBALS['sessClientID']:null;
		$this->display();
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function vehicle_stop_validation()
	{
		$GLOBALS['stopID']=trim((null!=($this->input->post('StopID'))?$this->input->post('StopID'):null));
		$GLOBALS['stopName']=trim((null!=($this->input->post('StopName'))?$this->input->post('StopName'):null));
		$GLOBALS['clientID']=trim((null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null));
		$GLOBALS['vehicleGroupID']=trim((null!=($this->input->post('VehicleGroup'))?$this->input->post('VehicleGroup'):null));
		$GLOBALS['vehicleID']=trim((null!=($this->input->post('Vehicle'))?$this->input->post('Vehicle'):null));
		$GLOBALS['latitude']=trim((null!=($this->input->post('Latitude'))?$this->input->post('Latitude'):null));
		$GLOBALS['longitude']=trim((null!=($this->input->post('Longitude'))?$this->input->post('Longitude'):null));
		$GLOBALS['remarks']=trim((null!=($this->input->post('Remarks'))?$this->input->post('Remarks'):null));
		$GLOBALS['editOrError']=trim((null!=($this->input->post('EditOrError'))?$this->input->post('EditOrError'):null));
		$this->form_validation->set_message('required', '%s required.');
		$this->form_validation->set_rules('StopName', 'Stop Name', 'required');
		// if any of the form rule is failed, then it show the error msg in view.
		if ($this->form_validation->run() == FALSE)
		{
			$GLOBALS['editOrError']=ACTIVE;
			$this->display();
		}
		else
		{
			if($GLOBALS['stopID']==null)//insert
			{
				$stopList_data['stop_name']=$GLOBALS['stopName'];
				$stopList_data['stop_latitude']=$GLOBALS['latitude'];
				$stopList_data['stop_longitude']=$GLOBALS['longitude'];
				$stopList_data['stop_client_id']=$GLOBALS['clientID'];
				$stopList_data['stop_vh_group']=$GLOBALS['vehicleGroupID'];
				$stopList_data['stop_remarks']=$GLOBALS['remarks'];
				//to insert details into vts_stops_list and return the currently inserted row id.
				$insertedID=$this->vehicle_stop_model->insert_stop($stopList_data,'vts_stops_list');
				$vhStop_data['vh_vehicle_id']=$GLOBALS['vehicleID'];
				$vhStop_data['vh_stops_id']=$insertedID;
				if($insertedID > 0)
				{
					$GLOBALS['stopID']=$insertedID;
					if($GLOBALS['vehicleID']!=null)
						$this->vehicle_stop_model->insert_stop($vhStop_data, 'vts_vehicle_stops'); //to insert vehicleid and currently inserted stopid into vts_vehicle_stops.
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], VEHICLE_STOPS,"Insert Vehicle Stop ID:".$GLOBALS['stopID'].", Stop Name:".$GLOBALS['stopName'].", was adde to vehicle: ".$GLOBALS['vehicleID']." of vehicle group ID:".$GLOBALS['vehicleGroupID']);
					$GLOBALS['outcome']='<br><div style="color: green;">'.$stopList_data['stop_name'].' added..</div><br>';
				}
			}else //edit
			{
				$this->vehicle_stop_model->insert_stop($stopList_data,'vts_stops_list',$GLOBALS['stopID']);//to update the vts_stop_list.
				if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
					$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], VEHICLE_STOPS,"Update Vehicle Stop ID:".$GLOBALS['stopID'].", Stop Name:".$GLOBALS['stopName'].", was adde to vehicle: ".$GLOBALS['vehicleID']." of vehicle group ID:".$GLOBALS['vehicleGroupID']);
				$GLOBALS['outcome']='<br><div style="color: green;">'.$stopList_data['stop_name'].' updated..</div><br>';
			}
			$this->display();
		}
	}
	/*
	 * This function store the stop detail
	 * @param
	 *  $stopDetail - JSON array format.
	 * Return type -  JSON string
	 */
	public function store_stop_details($stopDetail)
	{
		$vhStopDetails=json_decode(urldecode($stopDetail));
		$result['result']='failed';
		$result['selected']='no';
		$result['placeTitle']=$stopList_data['stop_name']=$vhStopDetails->placeTitle;
		$result['lat']=$stopList_data['stop_latitude']=$vhStopDetails->lat;
		$result['lng']=$stopList_data['stop_longitude']=$vhStopDetails->lng;
		$result['clientID']=$stopList_data['stop_client_id']=$vhStopDetails->clientID;
		$result['vhGpID']=$stopList_data['stop_vh_group']=$vhStopDetails->vhGpID;
		$result['remarks']=$stopList_data['stop_remarks']=$vhStopDetails->remarks;
		$result['vhID']=$vhStop_data['vh_vehicle_id']=$vhStopDetails->vhID;
		if($vhStopDetails->clientID!=null && $vhStopDetails->vhGpID!=null && $vhStopDetails->lat!=null && $vhStopDetails->lng!=null)
		{
			//to insert details into vts_stops_list and return the currently inserted row id.
			$insertedID=$this->vehicle_stop_model->insert_stop($stopList_data,'vts_stops_list');
			$vhStop_data['vh_stops_id']=$insertedID;
			if($insertedID > 0)
			{
				$result['result']='success';
				$result['stopID']=$insertedID;
				if($vhStop_data['vh_vehicle_id']!=null)
				{
					//to insert vehicleid and currently inserted stopid into vts_vehicle_stops.
					$this->vehicle_stop_model->insert_stop($vhStop_data, 'vts_vehicle_stops');
					$result['selected']='yes';
				}
			}
		}
		echo json_encode($result);
	}
	/*
	 * This function user to add or remove vehicle stop
	 * @param
	 *  $stopID - stop id.
	 *  $vhID - vehicle id
	 *  $opt - delete or edit option
	 * Return type -  JSON string
	 */
	public function add_remove_vhStop($stopID, $vhID, $opt)
	{
		$result=$this->vehicle_stop_model->add_remove_vehicle($stopID, $vhID, $opt);
		echo '{"result":"'.$result.'"}';
	}
	/*
	 * This function is used to get the vehicle group for the selected client and return back the list of
	 * vehicle group in JSON format with out refresh the page.
	 * @param
	 *  $client - Selected client ID or session client ID depending upon the login(i.e. Autograde user or other user)
	 * Return type -  JSON string
	 */
	public function get_client_vhgp($client)
	{
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$client_vh_gps=$this->vehicle_stop_model->get_all_vhGp($client,$GLOBALS['sessClientID'],$GLOBALS['sessUserID']);
		$output="";
		if($client_vh_gps!=null)
		{
			$output =json_encode($client_vh_gps);
		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and return back the list of
	 * vehicles in JSON format with out refresh the page.
	 * @param
	 *  $vhGrp (optional) - selected vehicle group id.
	 * Return type -  JSON string
	 */
	public function get_vhl_grp_vehicle($vhGrp=null)
	{
		$output="";
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=$this->vehicle_stop_model->get_all_vhGp_vehicle($vhGrp);
			$output="";
			if($vhl_grp_vehicle!=null)
			{
				$output =json_encode($vhl_grp_vehicle);
			}
		}
		echo($output);
	}
	/*
	 * This function is used to fetch all the marker which is related to selected
	 * vehicle group and the vehicle
	 * @param $vhGrp - Selected vehicle group ID
	 * @param $vhID (optional) - Selected Vehicle ID
	 * Return type -  JSON string
	 */
	public function get_vhgrp_markers($vhGrp, $vhID=null)
	{
		$output="";
		if($vhGrp!=null)
		{
			$markerList=$this->vehicle_stop_model->get_marker_details($vhGrp, $vhID);
			if($markerList!=null)
				$output=json_encode($markerList);
		}
		echo $output;
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if($this->common_model->check_session())
		{
			$this->common_model->menu_display();
			$GLOBALS['clientList']=$this->vehicle_stop_model->get_allClients();
			if($GLOBALS['clientID']!=null)
				$GLOBALS['vehicleGroupList']=$this->vehicle_stop_model->get_all_vhGp($GLOBALS['clientID'],$GLOBALS['sessClientID'],$GLOBALS['sessUserID']);
			if($GLOBALS['vehicleGroupID']!=null)
				$GLOBALS['vehicleList']=$this->vehicle_stop_model->get_all_vhGp_vehicle($GLOBALS['vehicleGroupID']);
			$this->load->view('header_footer/header_vehicle_stop',$GLOBALS);
			$this->load->view('vehicle_stop_view',$GLOBALS);
		}
	}
}
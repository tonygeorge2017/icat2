<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_allocation_ctrl extends CI_Controller {
	/*
	 * constructor for this class
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_allocation_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['editing']=NOT_ACTIVE;
		$GLOBALS['dealerID']=$GLOBALS['sessionData']['sess_dealerid'];
		$GLOBALS['checkID']=null;
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		$GLOBALS['sessUserType']=$GLOBALS['sessionData']['sess_user_type'];
		$GLOBALS['clientID']=null;
		$GLOBALS['selectedDevice']=null;
		$GLOBALS['availableDeviceList']=null;
		$GLOBALS['deviceCount']=NOT_ACTIVE;
		$GLOBALS['dealerList']=null;
		$GLOBALS['givenDate']=null;
		$GLOBALS['deviceDetailList']=$this->device_allocation_model->get_deviceDetailList($GLOBALS['clientID'],null,null,null);
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;		
		$GLOBALS['dateToEdit']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['isformError']=NOT_ACTIVE;
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		if($GLOBALS['sessUserType']==AUTOGRADE_USER)
		{		
			$GLOBALS['dealerID']=AUTOGRADE_USER;			
		}
		elseif($GLOBALS['sessUserType']==DEALER_USER)
		{
			$GLOBALS['dealerID']=$GLOBALS['sessionData']['sess_dealerid'];			
		}
		$this->table_pagination(md5($GLOBALS['dealerID']));
	}
	/*
	 * This function is used to validate and add the new user in database.
	 */
	public function device_allocation_validation()
	{
		$GLOBALS['checkID']=trim((null!=($this->input->post('CheckID'))?$this->input->post('CheckID'):null));
		$GLOBALS['dealerID']=trim((null!=($this->input->post('DealerID'))?$this->input->post('DealerID'):null));
		$GLOBALS['selectedDevice']=(null!=($this->input->post('deviceID'))?$this->input->post('deviceID'):null);
		$GLOBALS['clientID']=trim((null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null));
		$GLOBALS['editing']=trim((null!=($this->input->post('IsEditing'))?$this->input->post('IsEditing'):null));
		$GLOBALS['deviceCount']=trim((null!=($this->input->post('DeviceCount'))?$this->input->post('DeviceCount'):NOT_ACTIVE));
		if(empty($GLOBALS['checkID']))
		{
			$GLOBALS['givenDate']=trim((null!=($this->input->post('givenDateTime'))?trim($this->input->post('givenDateTime')):null));
			if($GLOBALS['givenDate'])
			{
				$give_date = new DateTime($GLOBALS['givenDate']);
				$GLOBALS['givenDate']=$give_date->format('Y-m-d');
			}
			$this->form_validation->set_message('required', '%s required.');
			$this->form_validation->set_rules('ClientID', 'Client', 'required');
			$this->form_validation->set_rules('givenDateTime', 'Client reales date', 'callback_check_client_date');
			if($GLOBALS['editing']==NOT_ACTIVE)
				$this->form_validation->set_rules('deviceID', 'Available', 'callback_check_Avail_Device');
			if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view.
			{
				$GLOBALS['isformError']=ACTIVE;
				$row=$GLOBALS['selectedDevice'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['selectedDevice'][$i]=array('device_id'=>$_row);
						$i++;
					}
				}
				$this->table_pagination(md5($GLOBALS['dealerID']));
			}
			else
			{
				$data['device_dealer_id']=$GLOBALS['dealerID'];
				$data['device_client_id']=$GLOBALS['clientID'];
				$data['device_release_client_date']=$GLOBALS['givenDate'];
				if( $GLOBALS['deviceCount']!=0 )
				{
					if($this->device_allocation_model->Update_deviceToClient($data,$GLOBALS['selectedDevice']))
					{
						$GLOBALS['outcome']='<div style="color: green;">Device has been allocated to the client</div>';
					}
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_ALLOCATION_TO_CLIENT,
							"Device Allocation To Client:"." DealerID=".trim($GLOBALS['dealerID']).", ClientID=".trim($GLOBALS['clientID']).", RealesDate=".trim($GLOBALS['givenDate']).", No.of Device=".count($GLOBALS['selectedDevice']));
					$this->clear_all();
				}
				else
				{
					$GLOBALS['outcome']='<div style="color: red;">Device(s) are not avialable for the client</div>';
				}
				$this->table_pagination(md5($GLOBALS['dealerID']));
			}
		}
		else
		{
			$GLOBALS['dealerID']=$GLOBALS['checkID'];
			$this->clear_all();
			$this->table_pagination(md5($GLOBALS['dealerID']));
		}
	}
	/*
	 * This function is used to validate the given client reales date&time
	 * @param
	 *  $date - date & time
	 * Return type - bool
	 */
	public function check_client_date($date)
	{
		if (empty($date)) //Check whether the device client reales date field is empty
		{
			$GLOBALS['outcome']='<div style="color: red;">Date is required</div>';
			return FALSE;
		}
		else
			return true;
	}
	/*
	 * This function is the call back function of form validation, it is used to validate the vehicle group
	 * @param
	 *  $device - device id
	 * Return type - bool
	 */
	public function check_Avail_Device($device)
	{
		if($GLOBALS['deviceCount']>0)
		{
			if (empty($device))
			{
				$this->form_validation->set_message('check_Avail_Device', 'Please select at least one device from Available Device(s)');
				$GLOBALS['outcome']='<div style="color: red;">Please select at least one device from Available Device(s)</div><br />';
				return FALSE;
			}
		}
		return true;
	}
	/*
	 * This function is used to fetch the data for edit.
	 * @param
	 *  $URLdealerID - md5 of dealer id
	 *  $URLclientID - md5 client id
	 *  $URLdate - md5 of date & time
	 *  $URLopt - md5 of option(edit or delete)
	 * Return type - bool
	 */
	public function edit_deviceAllocation($URLdealerID, $URLclientID, $URLdate, $URLopt)
	{		
		if($URLdealerID!=null && $URLclientID!=null && $URLdate!=null)
		{
			$row=$this->device_allocation_model->get_deviceDetailList($URLclientID,null,null,$URLdate,$URLdealerID);
			if($row!=null)
			{
				$GLOBALS['dealerID']=trim($row[0]['device_dealer_id']);
				$GLOBALS['givenDate']=trim($row[0]['device_release_client_date']);//It used to fill up the textbox in view.
				if($URLopt==md5('edit'))
				{
					$GLOBALS['clientID']=trim($row[0]['device_client_id']);
					$GLOBALS['selectedDevice']=$row;
					$GLOBALS['editing']=ACTIVE;
					$GLOBALS['dateToEdit']=$URLdate;
				}
				elseif($URLopt==md5('delete'))
				{
					$this->device_allocation_model->delete_deviceToClient($URLdealerID, $URLclientID, $URLdate);
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_ALLOCATION,
								"Remove Device Allocation To Client:"." DealerID=".trim($row[0]['device_dealer_id']).", ClientID=".trim($row[0]['device_client_id']).", RealesDate=".trim($row[0]['device_release_client_date']).", No.of Device=".count($row));
					$GLOBALS['editing']=NOT_ACTIVE;
				 }
			}
		}
		$GLOBALS['isformError']=NOT_ACTIVE;
		$this->table_pagination(md5($GLOBALS['dealerID']));
	}
	/*
	 * This function is used to clear all global data.
	 */
	public function clear_all()
	{
		$GLOBALS['givenDate']=null;
		$GLOBALS['editing']=NOT_ACTIVE;
		$GLOBALS['selectedDevice']=null;
		$GLOBALS['dealerList']=null;
		$GLOBALS['deviceDetailList']=null;
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $dealer - dealer id
	 *  $pageNo - page no
	 * Return type - void
	 */
	public function table_pagination($dealer,$pageNo=0)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];	
		$GLOBALS['sessIsAdmin']	=$GLOBALS['sessionData']['sess_is_admin'];
		if($GLOBALS['sessionData']!=null)
		{
			$GLOBALS['dealerID']=$dealer;
			$userdate=$this->common_model->get_usertime();
			$date_value=new DateTime($userdate);
			$datevalue=$date_value->format('Y-m-d');
			$GLOBALS['currDate']=new DateTime($datevalue);
			if($GLOBALS['isformError']==NOT_ACTIVE)
			{
				$GLOBALS['availableDeviceList']=$this->device_allocation_model->get_availableDevice($dealer,$GLOBALS['editing'],$GLOBALS['dateToEdit'],$GLOBALS['clientID']);
			}
			else
			{
				if($GLOBALS['editing'])
					$GLOBALS['availableDeviceList']=$this->device_allocation_model->get_availableDevice($dealer,$GLOBALS['editing'],md5($GLOBALS['givenDate']),$GLOBALS['clientID']);
				else
					$GLOBALS['availableDeviceList']=$this->device_allocation_model->get_availableDevice($dealer,NOT_ACTIVE);
			}
			if(($GLOBALS['sessClientID'] == AUTOGRADE_USER && $GLOBALS['sessIsAdmin'] == '1' )|| $GLOBALS['sessionData']['sess_dealerid']== AUTOGRADE_USER )			
				$GLOBALS['dealerList']=$this->device_allocation_model->get_allDealers();
			else
				$GLOBALS['dealerList']=$this->device_allocation_model->get_allDealers($dealer);
			
			$GLOBALS['clientList']=$this->device_allocation_model->get_allClients($dealer);			
			$config['base_url'] = base_url("index.php/device_allocation_ctrl/table_pagination/".$dealer."/");
			$config['uri_segment']=4;
			$GLOBALS['deviceDetailList']=$this->device_allocation_model->get_deviceDetailList(null,null,null,null,$dealer);
			$config['total_rows'] = count($GLOBALS['deviceDetailList']);
			$config['per_page']=ROW_PER_PAGE;
			$GLOBALS['deviceDetailList']=$this->device_allocation_model->get_deviceDetailList(null,ROW_PER_PAGE,$pageNo,null,$dealer);
			$this->pagination->initialize($config);
			$GLOBALS['pageLink']= $this->pagination->create_links();
			$this->display();
		}
		else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('device_allocation_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_license_ctrl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('client_license_model');
		$this->load->helper(array('form', 'url'));
		//the below data will save to the vts_client_license table
		$GLOBALS['client_name']=null;
		$GLOBALS['client_license_start_date']=null;
		$GLOBALS['client_license_expiry_date']=null;
		$GLOBALS['client_license_users']=null;
		$GLOBALS['client_license_vehicles']=null;
		$GLOBALS['client_license_drivers']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['client_license_remark']=null;
		$GLOBALS['clientLicenseList']=$this->client_license_model->get_vts_client_license();
		$GLOBALS['operation']=null;
		$GLOBALS['next_expiry_date']=null;
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['client_license_cancel_remarks'] =null;
		$GLOBALS['client_license_cancelled_by_user_id'] = null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	public function index()
	{
		$this->table_pagination();
		//$this->display();
	}

	/*
	 * This function is used to validate and add the new vts_client in database.
	 */
	public function vts_client_license_validation()
	{

		$GLOBALS['operation']=(null!=trim($this->input->post('Operation'))?trim($this->input->post('Operation')):null);
		$GLOBALS['client_name']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		//this data is to save in vts_client_license table
		$GLOBALS['client_license_id']=(null!=trim($this->input->post('ClientLicenseId'))?trim($this->input->post('ClientLicenseId')):null);
		$GLOBALS['client_license_client_id']=(null!=trim($this->input->post('ClientLicenseClientId'))?trim($this->input->post('ClientLicenseClientId')):null);
		
		$GLOBALS['client_license_start_date']=(null!=trim($this->input->post('ClientLicenseStartDate'))?trim($this->input->post('ClientLicenseStartDate')):null);
		$GLOBALS['client_license_expiry_date']=(null!=trim($this->input->post('ClientLicenseExpiryDate'))?trim($this->input->post('ClientLicenseExpiryDate')):null);
		$GLOBALS['client_license_users']=(null!=trim($this->input->post('ClientLicenseUsers'))?trim($this->input->post('ClientLicenseUsers')):null);
		$GLOBALS['client_license_vehicles']=(null!=trim($this->input->post('ClientLicenseVehicles'))?trim($this->input->post('ClientLicenseVehicles')):null);
		$GLOBALS['client_license_drivers']=(null!=trim($this->input->post('ClientLicenseDrivers'))?trim($this->input->post('ClientLicenseDrivers')):null);
		$GLOBALS['client_license_remark']=(null!=trim($this->input->post('ClientLicenseRemark'))?trim($this->input->post('ClientLicenseRemark')):null);
		
		$GLOBALS['active']=(null!=($this->input->post('ClientLicenseIsActive'))?$this->input->post('ClientLicenseIsActive'):NOT_ACTIVE);
		$GLOBALS['next_expiry_date']=(null!=trim($this->input->post('NextExpiry'))?trim($this->input->post('NextExpiry')):null);
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.

		$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_client_name');
		
		$this->form_validation->set_rules('ClientLicenseUsers', 'Users', 'callback_check_client_users');
		$this->form_validation->set_rules('ClientLicenseVehicles', 'Vehicles', 'callback_check_client_vehicles');
		$this->form_validation->set_rules('ClientLicenseDrivers', 'Drivers', 'callback_check_client_drivers');
		
		if($GLOBALS['operation']=='renew')
		{
			$this->form_validation->set_rules('NextExpiry', 'NextExpiry', 'callback_check_next_expiry');
		}
		
		
		
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Client in vts_client_license table.
		{
			
			$this->table_pagination();
		}
		else
		{
			//data for vts_client_license table
			$data1['client_license_client_id']=$GLOBALS['client_license_client_id'];
			$data1['client_license_start_date']=$GLOBALS['client_license_start_date'];
			$data1['client_license_expiry_date']=$GLOBALS['client_license_expiry_date'];
			$data1['client_license_users']=$GLOBALS['client_license_users'];
			$data1['client_license_vehicles']=$GLOBALS['client_license_vehicles'];
			$data1['client_license_drivers']=$GLOBALS['client_license_drivers'];
			
			$data1['client_license_is_active']=$GLOBALS['active'];
			
			if($GLOBALS['client_license_id']!=null)		//edit
			{
				
				IF($GLOBALS['operation']=="edit")
				{
					$data1['client_license_remark']=$GLOBALS['client_license_remark'];
					if($this->client_license_model->editOrRenewOrCancel_vts_client_license($GLOBALS['client_license_id'], $data1, 'edit'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT_LICENSE,
									"Update Client License:"." Client License Client Id=".trim($GLOBALS['client_license_client_id']).",Start Date=".trim($data1['client_license_start_date']).", Expiry Date=".trim($data1['client_license_expiry_date']).",Users=".trim($data1['client_license_users']).",drivers=".trim($data1['client_license_drivers']).",vehicles=".trim($data1['client_license_vehicles']).",License Remark=".trim($data1['client_license_remark']).",Is Active=".trim($data1['client_license_is_active']));
						
							$GLOBALS['outcome']="Client License Details updated successfully";
							$GLOBALS['client_license_id']=null;
							$GLOBALS['active']=null;
							$GLOBALS['client_license_remark']=null;
							$GLOBALS['client_name']=null;
						
					}
				}
				elseif ($GLOBALS['operation']=="renew")
				{
					$data1['client_license_remark']=$GLOBALS['client_license_remark'];
					$data1['client_license_expiry_date']=$GLOBALS['next_expiry_date'];
					if($this->client_license_model->editOrRenewOrCancel_vts_client_license(null, $data1, 'renew'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT_LICENSE,
									"Renew Client License:"." Client License Client Id=".trim($GLOBALS['client_license_client_id']).",Start Date=".trim($data1['client_license_start_date']).", Expiry Date=".trim($data1['client_license_expiry_date']).",Users=".trim($data1['client_license_users']).",drivers=".trim($data1['client_license_drivers']).",vehicles=".trim($data1['client_license_vehicles']).",License Remark=".trim($data1['client_license_remark']).",Is Active=".trim($data1['client_license_is_active']));
					
						$GLOBALS['outcome']="Client License Renewal done successfully";
						$GLOBALS['client_license_id']=null;
						$GLOBALS['active']=null;
						$GLOBALS['client_license_remark']=null;
						$GLOBALS['client_name']=null;
					
					}
				}
				elseif ($GLOBALS['operation']=="cancel")
				{
					$data1['client_license_cancel_remarks'] = $GLOBALS['client_license_remark'];
					if($this->client_license_model->editOrRenewOrCancel_vts_client_license($GLOBALS['client_license_id'], $data1, 'cancel'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT_LICENSE,
									"Cancel Client License:"." Client License Client Id=".trim($GLOBALS['client_license_client_id']).",Start Date=".trim($data1['client_license_start_date']).", Expiry Date=".trim($data1['client_license_expiry_date']).",Users=".trim($data1['client_license_users']).",drivers=".trim($data1['client_license_drivers']).",vehicles=".trim($data1['client_license_vehicles']).",License Cancel Remarks=".trim($data1['client_license_cancel_remarks']).",Is Active=".trim($data1['client_license_is_active']));
							
						if($GLOBALS['active']=="0")
						{
							$GLOBALS['outcome']="Client License cancelled";
						}
						else
						{
							$GLOBALS['outcome']="Client License Activate";
						}
						$GLOBALS['client_license_id']=null;
						$GLOBALS['active']=null;
						$GLOBALS['client_license_remark']=null;
						$GLOBALS['client_name']=null;
					}
				}
			}
			$this->table_pagination();
		}
	}

	/*
	 * This function is to validate the expiry date
	 */
	public function check_next_expiry($Next_Expiry)
	{
		if(empty($Next_Expiry)) //Check whether the Client next expiry field is empty
		{
			$GLOBALS['next_expiry_date']=null;
			$this->form_validation->set_message('check_next_expiry', 'Please select license duration');
			return FALSE;
		}
		return true;
	}
	
	/*
	* This function is the callback function of form validation, it is used to validate the vts_client license users name
	*/
	
	public function check_client_users($clnt_license_users)
	{
		$GLOBALS['client_license_users']=null;
		if(empty($clnt_license_users)) //Check whether the Client name field is empty
		{
			$this->form_validation->set_message('check_client_users', 'Number of User(s) required');
			return FALSE;
		}
		else if(!is_numeric($clnt_license_users))//Check whether the license user should be numeric value.
		{
			$this->form_validation->set_message('check_client_users', 'It should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_license_users))
		{
			$this->form_validation->set_message('check_client_users', 'Enter valid number of users');
			return FALSE;
		}
		$GLOBALS['client_license_users']=(null!=trim($this->input->post('ClientLicenseUsers'))?trim($this->input->post('ClientLicenseUsers')):null);
		return true;
	}
	
	/*
	 * validation method for the client vehicles field
	 */

	public function check_client_vehicles($clnt_license_vehicles)
	{
		$GLOBALS['client_license_vehicles']=null;
		if(empty($clnt_license_vehicles)) //Check whether the Client license vehicle field is empty
		{
			$this->form_validation->set_message('check_client_vehicles', 'Number of Vehicle(s) required');
			return FALSE;
		}
		else if(!is_numeric($clnt_license_vehicles))//Check whether the license vehicle numeric value.
		{
			$this->form_validation->set_message('check_client_vehicles', 'It should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_license_vehicles))
		{
			$this->form_validation->set_message('check_client_vehicles', 'Enter valid number of vehicles');
			return FALSE;
		}
		$GLOBALS['client_license_vehicles']=(null!=trim($this->input->post('ClientLicenseVehicles'))?trim($this->input->post('ClientLicenseVehicles')):null);
		return true;
	}
	
	
	/*
	 * validation method for the client drivers field
	 */
	public function check_client_drivers($clnt_license_drivers)
	{
		$GLOBALS['client_license_drivers']=null;
		if(empty($clnt_license_drivers)) //Check whether the Client license driver field empty
		{
			$this->form_validation->set_message('check_client_drivers', 'Number of Driver(s) required');
			return FALSE;
		}
		else if(!is_numeric($clnt_license_drivers))//Check whether the Client license driver should be numeric value.
		{
			$this->form_validation->set_message('check_client_drivers', 'It should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_license_drivers))
		{
			$this->form_validation->set_message('check_client_drivers', 'Enter valid number of drivers');
			return FALSE;
		}
		$GLOBALS['client_license_drivers']=(null!=trim($this->input->post('ClientLicenseDrivers'))?trim($this->input->post('ClientLicenseDrivers')):null);
		return true;
	}
	
	public function check_client_name($clnt_name)
	{
		$GLOBALS['client_name']=null;
		if(empty($clnt_name)) //Check whether the Client license driver field empty
		{
			$this->form_validation->set_message('check_client_name', '%s required');
			return FALSE;
		}
		$GLOBALS['client_name']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		return true;
	}
	
	public function check_license_start_date($clnt_start_date)
	{
// 		$GLOBALS['client_license_start_date']=null;
		if(empty($clnt_start_date)) //Check whether the Client license driver field is empty
		{
			$this->form_validation->set_message('check_license_start_date', '%s required');
			return FALSE;
		}
		$GLOBALS['client_license_expiry_date']=(null!=trim($this->input->post('ClientLicenseStartDate'))?trim($this->input->post('ClientLicenseStartDate')):null);
		return true;
	}
	
	public function check_license_expiry_date($clnt_expiry_date)
	{
// 		$GLOBALS['client_license_expiry_date']=null;
		if(empty($clnt_name)) //Check whether the Client license driver field is empty
		{
			$this->form_validation->set_message('check_license_expiry_date', '%s required');
			return FALSE;
		}
		$GLOBALS['client_license_expiry_date']=(null!=trim($this->input->post('ClientLicenseExpiryDate'))?trim($this->input->post('ClientLicenseExpiryDate')):null);
		return true;
	}
	
	public function check_client_license_renewal_date($clnt_renewal_date)
	{
		$GLOBALS['client_license_id']=null;
		if(empty($clnt_renewal_date)) //Check whether the Client license driver field is empty
		{
			$this->form_validation->set_message('check_client_license_renewal_date', '%s required');
			return FALSE;
		}
		$GLOBALS['client_license_id']=(null!=trim($this->input->post('ClientLicenseRenewalDate'))?trim($this->input->post('ClientLicenseRenewalDate')):null);
		return true;
	}
	/*
	 * This function used to edit or delete the vts_client_license
	 */
	public function editOrRenewOrCancel_vts_client_license()
	{
		$posted=$this->input->get_post('client_license_id');
		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$splitPosted[0];			//it hold the operation to perform(i.e. edit or delete).
		$ClientId=$splitPosted[1];			//it hold the client id to edit or delete.
		
		$row1=$this->client_license_model->get_vts_client_license(null,null,$ClientId);
		if($operation=="edit")
		{
			$GLOBALS=array('client_license_cancel_remarks'=>trim($row1['client_license_cancel_remarks']),'client_name'=>trim($row1['client_name']),'client_license_id'=>trim($row1['client_license_id']),'client_license_client_id'=>trim($row1['client_license_client_id']),'client_license_start_date'=>trim($row1['client_license_start_date']),'client_license_expiry_date'=>trim($row1['client_license_expiry_date']),'client_license_users'=>trim($row1['client_license_users']),'client_license_vehicles'=>trim($row1['client_license_vehicles']),'client_license_drivers'=>trim($row1['client_license_drivers']),'client_license_remark'=>trim($row1['client_license_remark']),'active'=>trim($row1['client_license_is_active']),);//It used to fill up the textbox in view.
			$GLOBALS['operation']="edit";
		}
		else if($operation=="renew")
		{
			try
			{
					$GLOBALS=array('client_name'=>trim($row1['client_name']),'client_license_id'=>trim($row1['client_license_id']),'client_license_client_id'=>trim($row1['client_license_client_id']),'client_license_start_date'=>trim($row1['client_license_start_date']),'client_license_expiry_date'=>trim($row1['client_license_expiry_date']),'client_license_users'=>trim($row1['client_license_users']),'client_license_vehicles'=>trim($row1['client_license_vehicles']),'client_license_drivers'=>trim($row1['client_license_drivers']),'client_license_remark'=>trim($row1['client_license_remark']),'active'=>trim($row1['client_license_is_active']),);//It used to fill up the textbox in view.
					$GLOBALS['operation']="renew";
					
			}catch (Exception $ex){
				$this->table_pagination();
				
			}
		}
		else if($operation=="cancel")
		{
			try
			{
				$GLOBALS=array('client_license_cancel_remarks'=>trim($row1['client_license_cancel_remarks']),'client_name'=>trim($row1['client_name']),'client_license_id'=>trim($row1['client_license_id']),'client_license_client_id'=>trim($row1['client_license_client_id']),'client_license_start_date'=>trim($row1['client_license_start_date']),'client_license_expiry_date'=>trim($row1['client_license_expiry_date']),'client_license_users'=>trim($row1['client_license_users']),'client_license_vehicles'=>trim($row1['client_license_vehicles']),'client_license_drivers'=>trim($row1['client_license_drivers']),'client_license_remark'=>trim($row1['client_license_remark']),'active'=>trim($row1['client_license_is_active']),);//It used to fill up the textbox in view.
				$GLOBALS['operation']="cancel";
			}catch (Exception $ex){
				$this->table_pagination();
			}
		}
		$this->table_pagination();
	}

	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){

		$GLOBALS['clientLicenseList']=$this->client_license_model->get_vts_client_license();
		$config['base_url'] = base_url("index.php/client_license_ctrl/table_pagination/");
		$config['total_rows'] = count($GLOBALS['clientLicenseList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['clientLicenseList']=$this->client_license_model->get_vts_client_license(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}

	/*
	 * This function is used to render the view
	 */
	private function display()
	{  
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('client_license_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
}
<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Terms_of_use extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );
		//$this->common_model->check_session();
		//$GLOBALS ['product_name'] = $this->commonmodel->get_setting_value ( 'ProductName' ); // getting browser title in each page.
		//$GLOBALS ['screen_name'] = TERMS_OF_USE;
		$GLOBALS['screen']="termsofuse";
		
	}
	function index() {
		//$GLOBALS ['screen_name'] = TERMS_OF_USE;
		if ($this->session->userdata ( 'login' )) {
			$client_time_zone = $this->common_model->get_location ();
			$GLOBALS ['timezone'] = ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			
			$this->common_model->menu_display();
			
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'terms_of_use_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		} else {
			$this->load->view ( 'header_footer/header1' , $GLOBALS);
			$this->load->view ( 'terms_of_use_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' ); // $this->dispaly();
		}
	}
	
}
?>
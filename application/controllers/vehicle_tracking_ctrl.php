<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_tracking_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['clientID']=null;
		$this->load->model('vehicle_tracking_model');
		$GLOBALS['vehicleGroupList']=$this->vehicle_tracking_model->get_all_vhGp($GLOBALS['sessClientID'],$GLOBALS['sessClientID'],$GLOBALS['sessUserID']);
		$row=$GLOBALS['vehicleGroupList'];
		$GLOBALS['vehicleList']=null;
		if($GLOBALS['vehicleGroupList']!=null)
		{
			$GLOBALS['vehicleList']=$this->vehicle_tracking_model->get_all_vhGp_vehicle($row[0]['vh_gp_id']);
		}
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$userdate=$this->common_model->get_usertime();
		$date_value=new DateTime($userdate);
		$datevalue=$date_value->format('Y-m-d');
		$GLOBALS['startDate']=new DateTime($datevalue.' 00:00:00');
		$GLOBALS['endDate']=new DateTime($datevalue.' 23:59:59');
		$this->display();
	}
	/*
	 * This function is used to fetch the data from server_gps_data table
	 * to place the marker in the map
	 * @param
	 *  $vh_id - vehicle id
	 *  $_fdate - from date
	 *  $_tdate - to date
	 *  $track_type - track type (live track or track history)
	 *  $vhGp - vehicle group id
	 *  $step_by_step - bulk or step-by-step
	 * Return type - JSON string
	 */
	public function fetch_gps_data($vh_id,$_fdate,$_tdate,$track_type,$vhGp,$step_by_step=null)
	{
		$get_in=false;
		$diff=$GLOBALS['ID']['sess_time_zonediff'];
		$fdate = new DateTime($_fdate); $tdate = new DateTime($_tdate);
		$check_diff=date_diff($fdate,$tdate);
		$diff_in_str=$check_diff->format("%R%a days");
		$fdate_rtn = new DateTime($_fdate); $tdate_rtn = new DateTime($_tdate);
		$fdate_rtn=$fdate_rtn->format('Y-m-d H:i:s');
		$tdate_rtn=$tdate_rtn->format('Y-m-d H:i:s');
		$outp='[{"lat":"Na","lng":"Na","speed":"Na","fdateTime":"'.$fdate_rtn.'","tdateTime":"'.$tdate_rtn.'"}]';
		if($step_by_step==null)
			$get_in=true;
		else if($diff_in_str > 0)
		{
			$get_in=false;
			$outp='[{"lat":"Na","lng":"TimeOutRange","speed":"Na","fdateTime":"'.$fdate_rtn.'","tdateTime":"'.$tdate_rtn.'"}]';
		}
		else
			$get_in=true;
		if($get_in)
		{
			$fdate=$this->common_model->get_dateTime($fdate->format('Y-m-d H:i:s'),'1');
			$tdate=$this->common_model->get_dateTime($tdate->format('Y-m-d H:i:s'),'1');
			if($track_type=="live" && $vh_id==0)//For all vehicle live gps current position data
			{
				$gps_result=$this->vehicle_tracking_model->get_all_live_vehicle_data($vhGp);
			}
			elseif($track_type=="live" && $vh_id!=0)
			{
				$gps_result=$this->vehicle_tracking_model->get_all_live_vehicle_data($vhGp, $vh_id);
			}
			else
			{
				$gps_result=$this->vehicle_tracking_model->get_gps_data($vh_id,$fdate,$tdate);
			}
			if($gps_result!=null)
			{
				$outp = json_encode($gps_result);
			}
		}
		echo($outp);
	}
	/*
	 * This function is used to get the vehicle group for the selected client and return back the list of
	 * vehicle group in JSON format with out refresh the page.
	 *  $client - client id
	 * Return type - JSON string
	 */
	public function get_client_vhgp($client)
	{
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$client_vh_gps="";
		$client_vh_gps=json_encode($this->vehicle_tracking_model->get_all_vhGp($client,$GLOBALS['sessClientID'],$GLOBALS['sessUserID']));		
		echo($client_vh_gps);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and return back the list of
	 * vehicles in JSON format with out refresh the page.
	 *  $vhGrp - vehicel group id
	 * Return type - JSON string
	 */
	public function get_vhl_grp_vehicle($vhGrp=null)
	{
		$vhl_grp_vehicle="";
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=json_encode($this->vehicle_tracking_model->get_all_vhGp_vehicle($vhGrp));
			
		}
		echo($vhl_grp_vehicle);
	}
	/*
	 * This function is usde to respond the from date and to date to the user for the selected time period
	 *  $cl_id - client id
	 *  $timeSelection - Today, Yesterday or Last week
	 * Return type - JSON string
	 */
	public function get_time($cl_id,$timeSelection)
	{
		$userdate=$this->common_model->get_usertime();
		$date_value=new DateTime($userdate);
		$datevalue=$date_value->format('Y-m-d');
		$fdate = new DateTime($datevalue.' 00:00:00'); $tdate = new DateTime($datevalue.' 23:59:59');
		if($timeSelection=='yesterday')
		{
			$fdate=$fdate->modify("-1 days");
			$tdate=$tdate->modify("-1 days");
		}
		else if($timeSelection=='lastweek')
		{
			$fdate=$fdate->modify("-7 days");
			$tdate=$tdate->modify("-1 days");
		}
		$outp='[{"FromDate":"'.$fdate->format('Y-m-d').'T'.$fdate->format('H:i').'","ToDate":"'.$tdate->format('Y-m-d').'T'.$tdate->format('H:i').'"}]';
		echo($outp);
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if($this->common_model->check_session())
		{
			$this->common_model->menu_display();
			$GLOBALS['clientList']=$this->vehicle_tracking_model->get_allClients();
			$this->load->view('header_footer/header_track_screen',$GLOBALS);
			$this->load->view('vehicle_tracking_view',$GLOBALS);
		}
	}
}
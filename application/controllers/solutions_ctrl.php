<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
class Solutions_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );

		if($this->session->userdata('login')!=null)
			$GLOBALS['user_type_id']=$GLOBALS['ID']['sess_user_type'];
		$GLOBALS['screen']="contactus";
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	
	/*
	 * This function is to download the PDF file from the downloads folder
	 */
	function downloadPDF($name)
	{
		$this->load->helper('download');
		$filename = $name.".pdf";
		$path = file_get_contents(base_url()."downloads/".$filename);//
		force_download($filename, $path);
	}
	/*
	 *This function call automatically when call Solution_ctrl
	 */
	public function index()
	{
		$this->dispaly();
	}

	/*
	 * This function is used to render the solution_view
	 */
	function dispaly()
	{
		if ($this->session->userdata ( 'login' ))
		{
			$this->common_model->check_session();
			
			$this->common_model->menu_display();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'solutions_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		} else {
			$this->load->view ( 'header_footer/header1',$GLOBALS );
			$this->load->view ( 'solutions_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
	}

}
?>
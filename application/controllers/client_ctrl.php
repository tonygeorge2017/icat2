<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_ctrl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('client_model');		
		$this->load->helper(array('form', 'url'));
		$GLOBALS['client_id']=null;
		$GLOBALS['client_name']=null;
		$GLOBALS['client_address']=null;
		$GLOBALS['client_post_code']=null;
		$GLOBALS['client_phone']=null;
		$GLOBALS['client_email']=null;
		$GLOBALS['client_contact_person']=null;
		$GLOBALS['client_contact_designation']=null;
		$GLOBALS['client_contact_email']=null;
		$GLOBALS['client_contact_phone']=null;
		$GLOBALS['client_remark']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['isschool']=NOT_ACTIVE;
		$GLOBALS['client_dealer_id']=null; //column which saves the client_dealer_id from vts_dealer table
		$GLOBALS['timeZoneID']=null;
		//the below data will save to the vts_client_license table
		$GLOBALS['client_license_start_date']=null;
		$GLOBALS['client_license_expiry_date']=null;
		$GLOBALS['client_license_users']=null;
		$GLOBALS['client_license_vehicles']=null;
		$GLOBALS['client_license_drivers']=null;
		
		$GLOBALS['clientList']=$this->client_model->get_vts_client();
		$GLOBALS['clientCategorySMSList']=$this->client_model->get_accessPermissions();
		$GLOBALS['selectedPermissions']=null;
		$GLOBALS['user_type_id']= OHTER_CLIENT_USER;// other client user id is 3 which is defined in constants.
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	public function index()
	{
		$this->table_pagination();
		//$this->display();
	}

	/*
	 * This function is used to validate and add the new vts_client in database.
	 */
	public function vts_client_validation()
	{
		$GLOBALS['client_id']=(null!=trim($this->input->post('ClientId'))?trim($this->input->post('ClientId')):null);
		$GLOBALS['client_name']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		$GLOBALS['client_address']=(null!=trim($this->input->post('ClientAddress'))?trim($this->input->post('ClientAddress')):null);
		$GLOBALS['client_post_code']=(null!=trim($this->input->post('ClientPostCode'))?trim($this->input->post('ClientPostCode')):null);
		$GLOBALS['client_phone']=(null!=trim($this->input->post('ClientPhone'))?trim($this->input->post('ClientPhone')):null);
		$GLOBALS['client_email']=(null!=trim($this->input->post('ClientEmail'))?trim($this->input->post('ClientEmail')):null);
		$GLOBALS['client_contact_person']=(null!=trim($this->input->post('ClientContactPerson'))?trim($this->input->post('ClientContactPerson')):null);
		$GLOBALS['client_contact_designation']=(null!=trim($this->input->post('ClientContactDesignation'))?trim($this->input->post('ClientContactDesignation')):null);
		$GLOBALS['client_contact_email']=(null!=trim($this->input->post('ClientContactEmail'))?trim($this->input->post('ClientContactEmail')):null);
		$GLOBALS['client_contact_phone']=(null!=trim($this->input->post('ClientContactPhone'))?trim($this->input->post('ClientContactPhone')):null);
		$GLOBALS['client_remark']=(null!=trim($this->input->post('ClientRemark'))?trim($this->input->post('ClientRemark')):null);
		$GLOBALS['active']=(null!=trim($this->input->post('ClientIsActive'))?trim($this->input->post('ClientIsActive')):NOT_ACTIVE);
		$GLOBALS['isschool']=(null!=trim($this->input->post('ClientIsSchool'))?trim($this->input->post('ClientIsSchool')):NOT_ACTIVE);
		$GLOBALS['client_dealer_id']=(null!=trim($this->input->post('ClientDealerId'))?trim($this->input->post('ClientDealerId')):null);
		$GLOBALS['timeZoneID']=(null!=($this->input->post('TimeZoneID'))?$this->input->post('TimeZoneID'):null);
		
		//this data is to save in vts_client_license table
		$GLOBALS['client_license_start_date']=(null!=trim($this->input->post('ClientLicenseStartDate'))?trim($this->input->post('ClientLicenseStartDate')):null);
		$GLOBALS['client_license_expiry_date']=(null!=trim($this->input->post('ClientLicenseExpiryDate'))?trim($this->input->post('ClientLicenseExpiryDate')):null);
		$GLOBALS['client_license_users']=(null!=trim($this->input->post('ClientLicenseUsers'))?trim($this->input->post('ClientLicenseUsers')):null);
		$GLOBALS['client_license_vehicles']=(null!=trim($this->input->post('ClientLicenseVehicles'))?trim($this->input->post('ClientLicenseVehicles')):null);
		$GLOBALS['client_license_drivers']=(null!=trim($this->input->post('ClientLicenseDrivers'))?trim($this->input->post('ClientLicenseDrivers')):null);
		
		$GLOBALS['selectedPermissions']=(null!=($this->input->post('permissionID'))?$this->input->post('permissionID'):null);
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.

 		$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_client_name');
		if(!empty($GLOBALS['client_post_code']))
		{
			$this->form_validation->set_rules('ClientPostCode','Post Code','callback_check_post_code');
		}
 		$this->form_validation->set_rules('ClientPhone', 'Client Phone', 'callback_check_client_phone');
		$this->form_validation->set_rules('ClientEmail', 'Cleint Email', 'callback_check_client_email');  //set rule for User Email field.
		$this->form_validation->set_rules('ClientContactPerson','Contact Person', 'callback_check_contact_person');
		
		if(!empty($GLOBALS['client_contact_designation']))
		{
			$this->form_validation->set_rules('ClientContactDesignation','Designation','callback_check_designation');
		}		
		$this->form_validation->set_rules('ClientContactEmail','Contact Email','callback_check_contact_email');
		$this->form_validation->set_rules('ClientContactPhone','Contact Phone', 'callback_check_contact_phone');
		$this->form_validation->set_rules('TimeZoneID','Time Zone ', 'required');
	
		
		//here is we give the 3 fields to be required we can't submit the form while updating....

		if(!empty($GLOBALS['client_license_users']))
		{
			$this->form_validation->set_rules('ClientLicenseUsers', 'ClientLicenseUsers', 'callback_check_client_users');
		}
		if(!empty($GLOBALS['client_license_vehicles']))
		{
			$this->form_validation->set_rules('ClientLicenseVehicles', 'ClientLicenseVehicles', 'callback_check_client_vehicles');
		}
		if(!empty($GLOBALS['client_license_drivers']))
		{
			$this->form_validation->set_rules('ClientLicenseDrivers', 'ClientLicenseDrivers', 'callback_check_client_drivers');
		}

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Client in vts_client table.
		{
			$row=$GLOBALS['selectedPermissions'];
			if(!empty($row))
			{
				$i=0;
				foreach ($row as $_row)
				{
					$GLOBALS['selectedPermissions'][$i]=array('config_item'=>$_row);
					$i++;
				}
			}
			$this->table_pagination();
		}
		else
		{
			$data['client_name']=$GLOBALS['client_name'];
			$data['client_address']=$GLOBALS['client_address'];
			$data['client_post_code']=$GLOBALS['client_post_code'];
			$data['client_phone']=$GLOBALS['client_phone'];
			$data['client_email']=$GLOBALS['client_email'];
			$data['client_contact_person']=$GLOBALS['client_contact_person'];
			$data['client_contact_designation']=$GLOBALS['client_contact_designation'];
			$data['client_contact_email']=$GLOBALS['client_contact_email'];
			$data['client_contact_phone']=$GLOBALS['client_contact_phone'];
			$data['client_remark']=$GLOBALS['client_remark'];
			$data['client_is_active']=$GLOBALS['active'];
			$data['client_is_school']=$GLOBALS['isschool'];
			$data['client_dealer_id']=$GLOBALS['client_dealer_id'];
			$data['client_time_zone_id']=$GLOBALS['timeZoneID'];
			//data for vts_client_license table
			$data1['client_license_start_date']=$GLOBALS['client_license_start_date'];
			$data1['client_license_expiry_date']=$GLOBALS['client_license_expiry_date'];
			$data1['client_license_users']=$GLOBALS['client_license_users'];
			$data1['client_license_vehicles']=$GLOBALS['client_license_vehicles'];
			$data1['client_license_drivers']=$GLOBALS['client_license_drivers'];
			
			if($GLOBALS['client_id']!=null)		//edit
			{
				if ($this->client_model->InsertOrUpdateOrdelete_vts_client($GLOBALS['client_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT,
								"Update Client:"."  Client ID=".trim($GLOBALS['client_id']).", Client Name=".trim($GLOBALS['client_name']).",Client Address=".trim($GLOBALS['client_address']).", Client Post Code=".trim($GLOBALS['client_post_code']).", Client Phone=".trim($GLOBALS['client_phone']).", Client Email=".trim($GLOBALS['client_email']).", Client Contact Person=".trim($GLOBALS['client_contact_person']).", Client Contact Designation=".trim($GLOBALS['client_contact_designation']).", Client COntact Email=".trim($GLOBALS['client_contact_email']).", Client Contact Phone=".trim($GLOBALS['client_contact_phone']).", Client Remarks=".trim($GLOBALS['client_remark']).", Client Is Active=".trim($GLOBALS['active']).", Client Dealer Id=".trim($GLOBALS['client_dealer_id']).", Client Time Zone ID=".trim($GLOBALS['timeZoneID']));//.", Client Time Zone=".trim($GLOBALS['client_time_zone']).", Client Time Diff=".trim($GLOBALS['client_time_diff']));
										
					//if($this->client_model->insert_user_value($GLOBALS['client_id'], $data['client_contact_person'], $data['client_contact_email'], $data['client_is_active'],$data['client_time_zone_id'], $data['client_contact_phone'], 'edit'))
					//{
					if($this->client_model->update_isactive_in_user($GLOBALS['client_id'],$GLOBALS['active']))
					{
						if($this->client_model->insert_update_clientPermissions($GLOBALS['client_id'],$GLOBALS['selectedPermissions'],'edit'))
						{
							if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
								$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT,
										"Update User:"." User Client Id=".trim($GLOBALS['client_id']).",User Name=".trim($data['client_contact_person']).", Contact Email=".trim($data['client_contact_email']).",User Is Active=".trim($data['client_is_active']));
							
								$GLOBALS['outcome']="Client Details has been updated";
								$GLOBALS['client_id']=null;
								$GLOBALS['active']=null;
								$GLOBALS['isschool']=null;
								$GLOBALS['client_dealer_id']=null;
								$GLOBALS['timeZoneID'] = null;
								$GLOBALS['selectedPermissions'] = null;
						}
					}
					//}
				}
			}
			else	//insert
			{
				if($this->client_model->InsertOrUpdateOrdelete_vts_client(null,$data,'insert'))
				{
					$client_id = $this->client_model->get_max_client_id();
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT,
								"Create New Client:"." Client ID=".trim($client_id).", Client Name=".trim($GLOBALS['client_name']).",Client Address=".trim($GLOBALS['client_address']).", Client Post Code=".trim($GLOBALS['client_post_code']).", Client Phone=".trim($GLOBALS['client_phone']).", Client Email=".trim($GLOBALS['client_email']).", Client Contact Person=".trim($GLOBALS['client_contact_person']).", Client Contact Designation=".trim($GLOBALS['client_contact_designation']).", Client COntact Email=".trim($GLOBALS['client_contact_email']).", Client Contact Phone=".trim($GLOBALS['client_contact_phone']).", Client Remarks=".trim($GLOBALS['client_remark']).", Client Is Active=".trim($GLOBALS['active']).", Client Dealer Id=".trim($GLOBALS['client_dealer_id']).", Client Time Zone ID=".trim($GLOBALS['timeZoneID']));//.", Client Time Zone=".trim($GLOBALS['client_time_zone']).", Client Time Diff=".trim($GLOBALS['client_time_diff']));
					
					if($this->client_model->insert_user_value($client_id, $data['client_contact_person'], $data['client_contact_email'], $data['client_is_active'],$data['client_time_zone_id'], $data['client_contact_phone'], $GLOBALS['user_type_id'], 'insert'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT,
									"Create New User:"." User Client ID=".trim($client_id).",User Name=".trim($data['client_contact_person']).", Contact Email=".trim($data['client_contact_email']).",User Is Active=".trim($data['client_is_active']).",User Time Zone Id=".trim($data['client_time_zone_id']).",User Contact Phone=".trim($data['client_contact_phone']).",User Type Id=".trim($GLOBALS['user_type_id']));
						
						$users_id = $this->client_model->get_user_id(trim($data['client_contact_email']));

						$s_msg='Dear '.$data['client_contact_person'].',<br><br>&nbsp; Your email has been registered as a new login in the Autograde T.A.N.K. system.<br>&nbsp;&nbsp;Please click the below link to set your new password and to start using the System :</br></br>&nbsp;&nbsp;<a href='.base_url("index.php/change_password_ctrl/view/".md5($users_id)).'>Click Here</a></br></br>Thanks & Regards,<br>Autograde T.A.N.K. Support.';
						$this->load->library ( 'commonfunction' );
						$this->commonfunction->send_mail($GLOBALS['client_contact_email'],$data['client_contact_person'],CC_TO,BCC_TO,'Vehicle Tracking System : New User Registered',$s_msg, REPLY_TO,'VTS Support');
						
						
						if($this->client_model->insert_update_client_license_value($client_id, $data1['client_license_start_date'], $data1['client_license_expiry_date'], $data1['client_license_users'], $data1['client_license_vehicles'], $data1['client_license_drivers'], 'insert'))
						{
// 							if(!empty($GLOBALS['selectedPermissions']))
// 							{
								if($this->client_model->insert_update_clientPermissions($client_id,$GLOBALS['selectedPermissions'],'insert'))
								{
									if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
										$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], CLIENT,
												"Create Client License:"." Client License Client ID=".trim($client_id).",Client License Start Date=".trim($data1['client_license_start_date']).", Contact License Expiry Date=".trim($data1['client_license_expiry_date']).",Client License Users=".trim($data1['client_license_users']).",Client License Drivers=".trim($data1['client_license_drivers']).",Client License Vehicles=".trim($data1['client_license_vehicles']));
									
									$GLOBALS['outcome']="New Client ".'"'.trim($GLOBALS['client_name']).'"'." created successfully and mail has been sent to given Mail ID";
									$GLOBALS['client_id']=null;
									$GLOBALS['active']=null;
									$GLOBALS['client_dealer_id']=null;
									$GLOBALS['timeZoneID'] = null;
									$GLOBALS['selectedPermissions'] = null;
								}
// 							}
						}
						
					}
				}
			}
			$this->table_pagination();
		}
	}

	/*
	 * This function is the callback function of form validation, it is used to validate the vts_client name
	 */
	public function check_client_name($client_name)
	{
		$GLOBALS['clientList']=$this->client_model->get_vts_client();
		
		$GLOBALS['client_name']=null;
		if (empty($client_name)) //Check whether the Client name field is empty
		{
			$this->form_validation->set_message('check_client_name', '%s required');
			return FALSE;
		}
		else if (strlen($client_name)<3) //Check whether the Client name is <3 characters
		{
			$this->form_validation->set_message('check_client_name', 'Name Should contain atleast 3 characters');
			return FALSE;
		}
		else if(preg_match('/[0-9]/', $client_name))//Check whether the New password have numeric value.
		{
			$this->form_validation->set_message('check_client_name', 'Numeric values are not allowed');
			return FALSE;
		}
		else if(preg_match('/[#$%^&@*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $client_name))//Check whether the client name should not allow special character(s).
		{
			$this->form_validation->set_message('check_client_name', 'Invalid Client Name');
			return FALSE;
		}
		else
		{
			foreach ($GLOBALS['clientList'] as $row)
			{
				if($GLOBALS['client_id']==null)
				{
					if(trim( strtoupper ($row['client_name']))== strtoupper($client_name))//Check whether Client Name already exist in vts_vlient table
					{
// 						$this->form_validation->set_message('check_client_name', '%s already exist.');
						$GLOBALS['outcome_with_db_check']="Client Name ".'"'.trim($row['client_name']).'"'." already exist";
						$this->form_validation->set_message('check_client_name', '');
						return FALSE;
					}
				}
				else
				{
					if(trim(strtoupper($row['client_name']))==strtoupper($client_name) && $GLOBALS['client_id']!=trim($row['client_id']))//Check whether Client already exist in vts_client table while update
					{
// 						$this->form_validation->set_message('check_client_name', '%s already exist.');
						$GLOBALS['outcome_with_db_check']="Client Name ".'"'.trim($row['client_name']).'"'." already exist";
						$this->form_validation->set_message('check_client_name', '');
						return FALSE;
					}
				}
			}
		}
		$GLOBALS['client_name']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		return true;
	}
	/*
	 * This function is the callback function of form validation, it is used to validate the email
	 */
	public function check_client_email($client_email)
	{
		if (empty($client_email)) //Check whether the client email field is empty
		{
			$this->form_validation->set_message('check_client_email', '%s required');
			return FALSE;
		}
		else if(!filter_var($client_email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['client_email']=null;
			$this->form_validation->set_message('check_client_email', 'Invalid email format');
			return FALSE;
		}
		else
		{
			foreach ($GLOBALS['clientList'] as $row)
			{
				if($GLOBALS['client_id']==null)
				{
					if(trim(strtoupper($row['client_email']))==strtoupper($client_email))//Check whether user already exist in user table
					{
						$GLOBALS['client_email']=null;
						$GLOBALS['outcome_with_db_check']="Client Email ".'"'.trim($row['client_email']).'"'." already exist";
						$this->form_validation->set_message('check_client_email', '');
						return FALSE;
					}
				}
				else
				{
					if(trim(strtoupper($row['client_email']))==strtoupper($client_email) && $GLOBALS['client_id']!=trim($row['client_id']))//Check whether user already exist in user table while update
					{
						$GLOBALS['client_email']=null;
						$GLOBALS['outcome_with_db_check']="Client Email ".'"'.trim($row['client_email']).'"'." already exist";
						$this->form_validation->set_message('check_client_email', '');
						return FALSE;
					}
				}
			}
		}
		$GLOBALS['client_email']=(null!=trim($this->input->post('ClientEmail'))?trim($this->input->post('ClientEmail')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_client name
	 */
	
	public function check_client_users($clnt_license_users)
	{
		$GLOBALS['client_license_users']=null;
		if(!is_numeric($clnt_license_users))//Check whether the client license user is not numeric value.
		{
			$this->form_validation->set_message('check_client_users', 'It should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_license_users))
		{
			$this->form_validation->set_message('check_client_users', 'Enter valid number of users');
			return FALSE;
		}
		$GLOBALS['client_license_users']=(null!=trim($this->input->post('ClientLicenseUsers'))?trim($this->input->post('ClientLicenseUsers')):null);
		return true;
	}
	
	/*
	 * validation method for the client vehicles field
	 */
	
	public function check_client_vehicles($clnt_license_vehicles)
	{
		$GLOBALS['client_license_vehicles']=null;
		if(!is_numeric($clnt_license_vehicles))//Check whether the client license vehicle should be numeric value.
		{
			$this->form_validation->set_message('check_client_vehicles', 'It should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_license_vehicles))
		{
			$this->form_validation->set_message('check_client_vehicles', 'Enter valid number of vehicles');
			return FALSE;
		}
		$GLOBALS['client_license_vehicles']=(null!=trim($this->input->post('ClientLicenseVehicles'))?trim($this->input->post('ClientLicenseVehicles')):null);
		return true;
	}
	
	
	/*
	 * validation method for the client drivers field
	 */
	public function check_client_drivers($clnt_license_drivers)
	{
		$GLOBALS['client_license_drivers']=null;
		if(!is_numeric($clnt_license_drivers))//Check whether the client license driver should be numeric value.
		{
			$this->form_validation->set_message('check_client_drivers', 'It should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_license_drivers))
		{
			$this->form_validation->set_message('check_client_drivers', 'Enter valid number of drivers');
			return FALSE;
		}
		$GLOBALS['client_license_drivers']=(null!=trim($this->input->post('ClientLicenseDrivers'))?trim($this->input->post('ClientLicenseDrivers')):null);
		return true;
	}
	
	/*
	 * This function is to validate the client postal code
	 */
	public function check_post_code($clnt_pst_code)
	{
		$GLOBALS['client_post_code']=null;
		if(!is_numeric($clnt_pst_code))
		{
			$this->form_validation->set_message('check_post_code', 'Postal code should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_pst_code))
		{
			$this->form_validation->set_message('check_post_code', 'Invalid Postal code');
			return FALSE;
		}
		$GLOBALS['client_post_code']=(null!=trim($this->input->post('ClientPostCode'))?trim($this->input->post('ClientPostCode')):null);
		return true;
	}
	
	/*
	 * This function is to validate the client phone
	 */
	public function check_client_phone($clnt_phone)
	{
		$GLOBALS['client_phone']=null;
	
		if (empty($clnt_phone))
		{
			$this->form_validation->set_message('check_client_phone', 'Client Phone number required');
			return FALSE;
		}
		else if (!is_numeric($clnt_phone)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_client_phone', 'Phone number should be numeric' );
			return FALSE;
		}
		else if (strlen($clnt_phone) < 10 || strlen($clnt_phone) > 10) // Check whether the Agency branch number should allow only 10 numbers
		{
			$this->form_validation->set_message ( 'check_client_phone', 'Please Enter valid phone number' );
			return FALSE;
		}
		else if(preg_match('$\.$', $clnt_phone))
		{
			$this->form_validation->set_message ( 'check_client_phone', 'Please Enter valid phone number' );
			return FALSE;
		}
	
		$GLOBALS['client_phone']=(null!=trim($this->input->post('ClientPhone'))?trim($this->input->post('ClientPhone')):null);
		return true;
	}
	
	/*
	 * This function is to validate the contact person name
	 */
	public function check_contact_person($cact_psn)
	{
		$GLOBALS['client_contact_person']=null;
		if(empty($cact_psn))
		{
			$this->form_validation->set_message('check_contact_person','Contact Name required');
			return FALSE;
		}
		else if (is_numeric($cact_psn)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Invalid Contact Name' );
			return FALSE;
		}
		else if (strlen($cact_psn) < 3)
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Name should contain atleast 3 charecters' );
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $cact_psn))
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Invalid Contact Name' );
			return FALSE;
		}
		$GLOBALS['client_contact_person']=(null!=trim($this->input->post('ClientContactPerson'))?trim($this->input->post('ClientContactPerson')):null);
		return true;
	}
	
	/*
	 * This function is to validate the client designation
	 */
	public function check_designation($cact_degn)
	{
		$GLOBALS['client_contact_designation']=null;
		if(is_numeric($cact_degn))
		{
			$this->form_validation->set_message('check_designation', 'Designation cannot be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $cact_degn))
		{
			$this->form_validation->set_message('check_designation', 'Invalid Designation');
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $cact_degn))
		{
			$this->form_validation->set_message( 'check_designation', 'Invalid Designation' );
			return FALSE;
		}
		$GLOBALS['client_contact_designation']=(null!=trim($this->input->post('ClientContactDesignation'))?trim($this->input->post('ClientContactDesignation')):null);
		return true;
	}
	
	/*
	 * This function is to validate the client contact email id
	 * (to check duplicate)
	 * 
	 */
	public function check_contact_email($cact_email)
	{
		if (empty($cact_email)) //Check whether the client email field is empty
		{
			$this->form_validation->set_message('check_contact_email', '%s required');
			return FALSE;
		}
		else if(!filter_var($cact_email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['client_contact_email']=null;
			$this->form_validation->set_message('check_contact_email', 'Invalid email format');
			return FALSE;
		}
		else
		{
	/*		foreach ($GLOBALS['clientList'] as $row)
			{
				if($GLOBALS['client_id']==null)
				{
					if(trim(strtoupper($row['client_contact_email']))==strtoupper($cact_email))//Check whether user already exist in user table
					{
						$GLOBALS['client_contact_email']=null;
						$GLOBALS['outcome_with_db_check']="Contact Email ".'"'.trim($row['client_contact_email']).'"'." already exist";
						$this->form_validation->set_message('check_contact_email', '');
						return FALSE;
					}
				}
				else
				{
					if(trim(strtoupper($row['client_contact_email']))==strtoupper($cact_email) && $GLOBALS['client_id']!=trim($row['client_id']))//Check whether user already exist in user table while update
					{
						$GLOBALS['client_contact_email']=null;
						$GLOBALS['outcome_with_db_check']="Contact Email ".'"'.trim($row['client_contact_email']).'"'." already exist";
						$this->form_validation->set_message('check_contact_email', '');
						return FALSE;
					}
				}
			}*/
			$user_mail = $this->client_model->get_user_table_data($cact_email);
			foreach ($GLOBALS['clientList'] as $row)
			{
			if ($GLOBALS ['client_id'] == null) {
				if($user_mail['user_client_id']!=-1)
				{
					$GLOBALS['client_contact_email']=null;
					$GLOBALS['outcome_with_db_check']="Given contact email-id already exists";
					$this->form_validation->set_message('check_contact_email', '');
					return FALSE;
				}
			}else {
					if((trim($row['client_contact_email'])==$cact_email && $GLOBALS['client_id']!=trim($row['client_id'])))//Check whether user already exist in user table while update
					{
						$GLOBALS['client_contact_email']=null;
						$GLOBALS['outcome_with_db_check']="Given contact email-id already exists";
						$this->form_validation->set_message('check_contact_email', '');
						return FALSE;
					}
// 					if(($user_mail['user_email'] == $cact_email && $user_mail['user_client_id'] != trim($row['client_id'])))
// 					{
// 						$GLOBALS['client_contact_email']=null;
// 						$GLOBALS['outcome_with_db_check']="Given contact email-id already exists";
// 						$this->form_validation->set_message('check_contact_email', '');
// 						return FALSE;
// 					}
				}
			}
		$GLOBALS['client_contact_email']=(null!=trim($this->input->post('ClientContactEmail'))?trim($this->input->post('ClientContactEmail')):null);
		return true;
		}
	}	
	
	/*
	 * This function is to validate the client contact phone number
	 */
	public function check_contact_phone($cact_phone)
	{
		$GLOBALS['client_contact_phone']=null;
		if (empty($cact_phone))
		{
			$this->form_validation->set_message('check_contact_phone', 'Contact Phone number required');
			return FALSE;
		}
		else if (!is_numeric($cact_phone)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Phone number should be numeric' );
			return FALSE;
		}
		else if (strlen($cact_phone) < 10 || strlen($cact_phone) > 10) // Check whether the Agency branch number should allow only 10 numbers
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Please Enter valid phone number' );
			return FALSE;
		}
		else if(preg_match('$\.$', $cact_phone))
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Please Enter valid phone number' );
			return FALSE;
		}
		$GLOBALS['client_contact_phone']=(null!=trim($this->input->post('ClientContactPhone'))?trim($this->input->post('ClientContactPhone')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_client
	 */
	public function editOrDelete_vts_client()
	{
		$posted=$this->input->get_post('client_id');
		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$splitPosted[0];			//it hold the operation to perform(i.e. edit or delete).
		$ClientId=$splitPosted[1];			//it hold the client id to edit or delete.
		
		$row=$this->client_model->get_vts_client(null,null,$ClientId);
		$row1=$this->client_model->get_vts_client_license(null,null,$ClientId);
		$row2=$this->client_model->get_vts_client_permissions($ClientId);
		if($operation=="edit")
		{
			$GLOBALS['client_id']=trim($row['client_id']);
			$GLOBALS['client_name']=trim($row['client_name']);
			$GLOBALS['client_address']=trim($row['client_address']);
			$GLOBALS['client_post_code']=trim($row['client_post_code']);
			$GLOBALS['client_phone']=trim($row['client_phone']);
			$GLOBALS['client_email']=trim($row['client_email']);
			$GLOBALS['client_contact_person']=trim($row['client_contact_person']);
			$GLOBALS['client_contact_designation']=trim($row['client_contact_designation']);
			$GLOBALS['client_contact_email']=trim($row['client_contact_email']);
			$GLOBALS['client_contact_phone']=trim($row['client_contact_phone']);
			$GLOBALS['client_remark']=trim($row['client_remark']);
			$GLOBALS['active']=trim($row['client_is_active']);
			$GLOBALS['isschool']=trim($row['client_is_school']);
			$GLOBALS['client_dealer_id']=trim($row['client_dealer_id']);
			$GLOBALS['timeZoneID']=trim($row['client_time_zone_id']);
			$GLOBALS['client_license_start_date']=trim($row1['client_license_start_date']);
			$GLOBALS['client_license_expiry_date']=trim($row1['client_license_expiry_date']);
			$GLOBALS['client_license_users']=trim($row1['client_license_users']);
			$GLOBALS['client_license_vehicles']=trim($row1['client_license_vehicles']);
			$GLOBALS['client_license_drivers']=trim($row1['client_license_drivers']);//It used to fill up the textbox in view.
			$GLOBALS['selectedPermissions']=$row2;
		}
		else if($operation=="delete")
		{
			try
			{
				if($this->client_model->InsertOrUpdateOrdelete_vts_client($ClientId,null,$operation))
				{
					
					if($this->client_model->insert_user_value($GLOBALS['client_id'], null, null, null, null, null, null, $operation))
					$GLOBALS['outcome']=trim($row['client_name'])." client deleted";
				}
					
			}catch (Exception $ex){
				$this->table_pagination();
				
			}
		}
		$this->table_pagination();
	}

	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['dealerList']=$this->client_model->get_allDealers();
		$GLOBALS['timeZoneList']=$this->common_model->get_time_zone();
		$config['base_url'] = base_url("index.php/client_ctrl/table_pagination/");
		$GLOBALS['clientList']=$this->client_model->get_vts_client();
		$GLOBALS['clientCategorySMSList']=$this->client_model->get_accessPermissions();

		$config['total_rows'] = count($GLOBALS['clientList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['clientList']=$this->client_model->get_vts_client(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}

	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('client_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}

}
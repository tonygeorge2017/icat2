<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_monitoring_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_monitoring_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['deviceList']=array();
		$GLOBALS['deviceDetail']="";
		$GLOBALS['distributorList']=array();
		$GLOBALS['dealerList']=array();
		$GLOBALS['vehicleGroupList']=array();
		$GLOBALS['vehicleList']=array();		
		$GLOBALS['imeiNo']=null;
		$GLOBALS['slNo']=null;
		$GLOBALS['distributorID']=null;
		$GLOBALS['dealerID']=null;
		$GLOBALS['clientName']=null;
		$GLOBALS['clientID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleID']=null;
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$data['imeiNo']=null;
		$data['slNo']=null;
		$data['distributorID']=null;
		$data['dealerID']=null;
		$data['clientID']=null;
		$data['vehicleGroupID']=null;
		$data['vehicleID']=null;
		$data['AllDevice']=true;
		if($GLOBALS['sessClientID']==AUTOGRADE_USER)
		{
			$this->display($data);
		}
		else{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect("login_ctrl");
		}
	}
	public function auto_refresh(){
		
		$tableHeader="<table border=\"1\" class=\"user-dts\" style=\"width: 100%; margin: auto;left: 0;right: 0;\">";
		$tableHeader .="<tr><th style=\"text-align:center;\" rowspan=\"2\">Client</th><th style=\"text-align:center;\" rowspan=\"2\">IMEI No.</th><th style=\"text-align:center;\" rowspan=\"2\">Vehicle</th><th style=\"text-align:center;\" rowspan=\"2\">FOTA</th>";
		$tableHeader .="<th style=\"text-align:center;\" colspan=\"2\">Received Data &amp; Time</th><th style=\"text-align:center;\" colspan=\"4\">Device Status</th>";
		$tableHeader .="<th style=\"text-align:center;\" colspan=\"2\">GPS Data Count</th><th style=\"text-align:center;\" rowspan=\"2\">Get Detail</th>";
		$tableHeader .="</tr><tr><th style=\"text-align:center;\"><i>First Data</i></th><th style=\"text-align:center;\"><i>Last Data</i></th>";
		$tableHeader .="<th style=\"text-align:center;\"><i>Valid</i></th><th style=\"text-align:center;\"><i>Ignition</i></th>";
		$tableHeader .="<th style=\"text-align:center;\"><i>Power</i></th><th style=\"text-align:center;\"><i>Digital Input</i></th><th style=\"text-align:center;\"><i>Valid</i></th><th style=\"text-align:center;\"><i>Invalid</i></th></tr>";
		
		
		
		$json_query=$this->input->post('query');
		$queryDetails=json_decode(urldecode($json_query));
		
		$data['imeiNo']=$queryDetails->imei;
		$data['slNo']=$queryDetails->slno;
		$data['distributorID']=$queryDetails->dist;
		$data['dealerID']=$queryDetails->dealer;
		$data['clientID']=$queryDetails->client;
		$data['vehicleGroupID']=$queryDetails->vhgp;
		$data['vehicleID']=$queryDetails->vh;
		$data['AllDevice']=$queryDetails->alldevice;
		$result=$this->device_monitoring_model->get_device_details($data);
		if($result!=null){
			foreach ($result as $row)
			{
				 $l_isvalid=($row['chk_new_device_valid']=='A')?'YES':'NO';
				 $l_ispowerup=($row['chk_new_device_power']=='Y')?'YES':'NO';
				 $l_ignition=($row['chk_new_device_ingition']=='Y')?'YES':'NO';
				 $l_clientname=(isset($row['client_name']))?$row['client_name']:'Unknown';
				 $l_vhregno=(isset($row['vehicle_name']))?$row['vehicle_name']:'Unknown';
			     $color=null; 
			     if($row['timediff_btw_curnt_lat']==null || $row['timediff_btw_fst_lat']==null)
			     {
			     	if($row['timediff_btw_curnt_fst'] < DEVICE_ALERT_TIME_DIFF)// the difference betweent the first date and last date is less than the interval mentioned in the constant, then green else red.
			     		$color='bgcolor="#CDFF88"'; // change the color to green for new device
			        else 
			        	$color='bgcolor="#FE9997"'; //alert the user by showing the row in red color.
			     }
			     else if( $row['timediff_btw_fst_lat'] < NEW_DEVICE_UPTO_TIME_DIFF && $row['timediff_btw_curnt_lat'] < DEVICE_ALERT_TIME_DIFF)
			     	$color='bgcolor="#CDFF88"'; // change the color to green for new device
			     else if($row['timediff_btw_curnt_lat'] >= DEVICE_ALERT_TIME_DIFF)//alert the user by showing the row in red color.
			     	$color='bgcolor="#FE9997"';
			     else 
			     	$color=null;
			     $tableHeader .='<tr '.$color.'>'; //green #7CB000 red #F05133
			     $tableHeader .='<td>'. $l_clientname.' </td>';
			     $tableHeader .='<td>'. trim($row['chk_new_device_imei']).'</td>';
			     $tableHeader .='<td>'. $l_vhregno.' </td>';
				 $tableHeader .='<td> -- </td>';
			     $tableHeader .='<td>'. trim($row['first_datetime']).'</td>';
			     $tableHeader .='<td>'. trim($row['last_datetime']).'</td>';
			     $tableHeader .=' <td>'. $l_isvalid.'</td>';
			     $tableHeader .='<td>'. $l_ignition.'</td>';
			     $tableHeader .='<td>'. $l_ispowerup.'</td>';
			     $tableHeader .='<td>'. trim($row['digital_inputs']).'</td>';
			     $tableHeader .='<td>'. trim($row['chk_new_device_valid_count']).'</td>';
			     $tableHeader .='<td>'. trim($row['chk_new_device_invalid_count']).'</td>';
			     $tableHeader .="<td><a href=\"#DeviceDetails\" title=\"Click this to get the details\" onclick=\"get_deviceDetails('". md5($row['chk_new_device_imei'])."')\"><i class=\"glyphicon glyphicon-file\"></i></a></td>";
			     $tableHeader .='</tr>';
			}
		}else {
			$tableHeader .='<tr><td colspan="11">Sorry..! Result not found.</td></tr>';
		}
		$tableHeader .="</table>";
		echo $tableHeader;
	}
	/*
	 * This function is used to get to query
	 */
	public function fetch_device_list()
	{		
		$this->form_validation->set_rules('AutoClinetName', 'Clinet name', 'callback_check_client');
		$this->form_validation->set_rules('ImeiNo', 'Device IMEI', 'callback_check_device_imei');
		$this->form_validation->set_rules('SlNo', 'Device SLNO', 'callback_check_device_slno');
		$GLOBALS['imeiNo']=(null!=trim($this->input->post('ImeiNo'))?trim($this->input->post('ImeiNo')):null);
		$GLOBALS['slNo']=(null!=trim($this->input->post('SlNo'))?trim($this->input->post('SlNo')):null);
		$GLOBALS['distributorID']=(null!=trim($this->input->post('DistributorID'))?trim($this->input->post('DistributorID')):null);
		$GLOBALS['dealerID']=(null!=trim($this->input->post('DealerID'))?trim($this->input->post('DealerID')):null);
		$GLOBALS['clientID']=(null!=trim($this->input->post('ClinetID'))?trim($this->input->post('ClinetID')):null);
		$GLOBALS['clientName']=(null!=trim($this->input->post('AutoClinetName'))?trim($this->input->post('AutoClinetName')):null);
		$GLOBALS['vehicleGroupID']=(null!=trim($this->input->post('VehicleGroupID'))?trim($this->input->post('VehicleGroupID')):null);
		$GLOBALS['vehicleID']=(null!=trim($this->input->post('VehicleID'))?trim($this->input->post('VehicleID')):null);
		
		
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view.
		{
			$this->display();
		}
		else
		{
			$data['imeiNo']=$GLOBALS['imeiNo'];
			$data['slNo']=$GLOBALS['slNo'];
			$data['distributorID']=$GLOBALS['distributorID'];
			$data['dealerID']=$GLOBALS['dealerID'];
			$data['clientID']=$GLOBALS['clientID'];
			$data['vehicleGroupID']=$GLOBALS['vehicleGroupID'];
			$data['vehicleID']=$GLOBALS['vehicleID'];			
			$this->display($data);
		}
	}
	
	/*
	 * This function is used to validate the givn client name
	 * @param
	 *  $client- given client name by the user
	 * Return type bool
	 */
	public function check_client($client)
	{
		if (empty($client))
		{
			return true;
		}
		elseif (is_numeric($client))
		{
			$GLOBALS['clientName']=null;
			$this->form_validation->set_message ( 'check_client', '%s should not be numeric' );
			return FALSE;
		}
		elseif($GLOBALS['clientID']==null)
		{
			$GLOBALS['clientName']=null;
			$this->form_validation->set_message ( 'check_client', '%s not found' );
			return FALSE;
		}
		else
			return true;
	}
	/*
	 * This function is the callback function of form validation,
	 * it is used to validate the vts_device IMEI No.
	 * @param
	 *  $dvs_imei- given imei no by the user
	 * Return type bool
	 */
	public function check_device_imei($dvs_imei)
	{
		if (empty($dvs_imei)) // Check whether the given Device IMEI is empty.
		{
			return true;
		}
		if (!is_numeric($dvs_imei)) // Check whether the given Device IMEI is numeric.
		{
			$GLOBALS['imeiNo']=null;
			$this->form_validation->set_message ( 'check_device_imei', '%s should be numeric' );
			return FALSE;
		}
		else
			return true;
	}
	
	/*
	 * This function is to validate the device slno number
	 */
	public function check_device_slno($dvs_slno)
	{
		if (empty($dvs_slno)) // Check whether the given Device Slno is empty.
		{
			return true;
		}
		if (!is_numeric($dvs_slno)) // Check whether the given Device Slno is numeric.
		{
			$GLOBALS['slnoNo']=null;
			$this->form_validation->set_message ( 'check_device_slno', '%s should be numeric' );
			return FALSE;
		}
		else
			return true;
	}
	
	/*
	 * This function is used to get the dealer for the selected distributor and
	 * return back the list of dealer in JSON format with out refresh the page.
	 * @param
	 *  $distributor - distributor id
	 * Return type - JSON string
	 */
	public function get_dist_dealer($distributor=null)
	{
		$output="";
		if($distributor!=null)
		{
			$dealers=$this->device_monitoring_model->get_all_Dealer($distributor);
			if($dealers!=null)
			{
				$output=json_encode($dealers);
			}
		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle group for the selected client and
	 * return back the list of vehicle group in JSON format with out refresh the page.
	 * @param
	 *  $client - client id
	 * Return type - JSON string
	 */
	public function get_client_vhgp($client)
	{
		$client_vh_gps=$this->device_monitoring_model->get_all_vhGp($client);
		$output="";
		if($client_vh_gps!=null)
		{
			$output=json_encode($client_vh_gps);
		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and
	 * return back the list of vehicles in JSON format with out refresh the page.
	 * @param
	 *  $vhGrp - vehicle group id
	 * Return type - JSON string
	 */
	public function get_vhl_grp_vehicle($vhGrp=null)
	{
		$output="";
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=$this->device_monitoring_model->get_all_vhGp_vehicle($vhGrp);
			if($vhl_grp_vehicle!=null)
			{
				$output=json_encode($vhl_grp_vehicle);
			}
		}
		echo($output);
	}
	/*
	 * function calling from autocomplete function of view
	 * to get values to item field
	 */
	public function  get_client_list_name()
	{
		if (isset($_GET['client']))
		{
			$dist=(isset($_GET['client']))?$_GET['dist']:null;
			$deal=(isset($_GET['client']))?$_GET['dealer']:null;
			$clientNameStartWith = strtolower($_GET['client']);
			$query= $this->device_monitoring_model->get_all_Clients($clientNameStartWith,$dist,$deal);
			if(count($query) > 0)
			{
				//$i=0;
				foreach ($query as $row)
				{
					$new_array['value'] = htmlentities(stripslashes($row['client_name']));
					$new_array['id'] = htmlentities(stripslashes($row['client_id'])); //build an array
					$row_set[] =  $new_array;
					//$i++;
				}
				echo json_encode($row_set); //format the array into json data
			}
		}
	}
	/*
	 * This function is used to refresh the view
	 * @param
	 *  $p_imeino - imei no of the device
	 * Return type - void
	 */
	public function get_device_list($p_imeino)
	{
		$l_deviceDetail=$this->device_monitoring_model->get_device_details(null,$p_imeino);
		//log_message('debug',print_r($l_deviceDetail));
		if(isset($l_deviceDetail))
		{
			$l_isatcsim='< UNKNOWN >';
			$l_isvalid=($l_deviceDetail['chk_new_device_valid']=='A')?'YES':'NO';
			$l_ispowerup=($l_deviceDetail['chk_new_device_power']=='Y')?'YES':'NO';
			$l_ignition=($l_deviceDetail['chk_new_device_ingition']=='Y')?'YES':'NO';
			$l_distname=(isset($l_deviceDetail['dist_name']))?$l_deviceDetail['dist_name']:'< UNKNOWN >';
			$l_dealername=(isset($l_deviceDetail['dealer_name']))?$l_deviceDetail['dealer_name']:'< UNKNOWN >';
			$l_clientname=(isset($l_deviceDetail['client_name']))?$l_deviceDetail['client_name']:'< UNKNOWN >';
			if(isset($l_deviceDetail['dev_install_is_atc_sim']))
				if(($l_deviceDetail['dev_install_is_atc_sim'])!=null)
					$l_isatcsim=($l_deviceDetail['dev_install_is_atc_sim']=='1')?'Autograde':'Other';
				else
					$l_isatcsim='< UNKNOWN >';
			$GLOBALS['deviceDetail']="Distributor : ".$l_distname;
			$GLOBALS['deviceDetail'].="\nDealer : ".$l_dealername;
			$GLOBALS['deviceDetail'].="\nClient : ".$l_clientname;
			$GLOBALS['deviceDetail'].="\n\nIMEI No. : ".$l_deviceDetail['chk_new_device_imei'];
			$GLOBALS['deviceDetail'].="\nSl. No. : ".$l_deviceDetail['device_slno'];
			$GLOBALS['deviceDetail'].="\nMobile No. : ".$l_deviceDetail['dev_install_mobile_no'];
			$GLOBALS['deviceDetail'].="\nSIM Supplied By: ".$l_isatcsim;
			$GLOBALS['deviceDetail'].="\nSIM Provider : ".$l_deviceDetail['dev_install_sim_provider'];
			$GLOBALS['deviceDetail'].="\nVehicle : ".$l_deviceDetail['vehicle_name'];
			$GLOBALS['deviceDetail'].="\n\nReceived Date & Time (YYYY-MM-DD HH mm ss) ";
			$GLOBALS['deviceDetail'].="\n  First : ".$l_deviceDetail['first_datetime'];
			$GLOBALS['deviceDetail'].="\n  Last : ".$l_deviceDetail['last_datetime'];
			$GLOBALS['deviceDetail'].="\n\nGPS Date & Time (YYMMDD HHmmss) ";
			$GLOBALS['deviceDetail'].="\n  First : ".$l_deviceDetail['chk_new_device_first_gpsdate']." ".$l_deviceDetail['chk_new_device_first_gpstime'];
			$GLOBALS['deviceDetail'].="\n  Last : ".$l_deviceDetail['chk_new_device_last_gpsdate']." ".$l_deviceDetail['chk_new_device_last_gpstime'];
			$GLOBALS['deviceDetail'].="\n\nDevice Status ";
			$GLOBALS['deviceDetail'].="\n  Digital Input(Entire Bit) : ".$l_deviceDetail['digital_inputs'];
			$GLOBALS['deviceDetail'].="\n  Ignition : ".$l_ignition;//$l_deviceDetail['chk_new_device_ingition'];
			$GLOBALS['deviceDetail'].="\n  Power : ".$l_ispowerup;//$l_deviceDetail['chk_new_device_power'];
			$GLOBALS['deviceDetail'].="\n  Is Valid : ".$l_isvalid;//$l_deviceDetail['chk_new_device_valid'];
			$GLOBALS['deviceDetail'].="\n\nGPS Data Count ";
			$GLOBALS['deviceDetail'].="\n  Valid : ".$l_deviceDetail['chk_new_device_valid_count'];
			$GLOBALS['deviceDetail'].="\n  Invalid : ".$l_deviceDetail['chk_new_device_invalid_count'];
			$GLOBALS['deviceDetail'].="\n  Total : ".$l_deviceDetail['total'];
			$GLOBALS['deviceDetail'].="\n\nRunning No. ";
			$GLOBALS['deviceDetail'].="\n  First : ".$l_deviceDetail['chk_new_device_first_runno'];
			$GLOBALS['deviceDetail'].="\n  Last : ".$l_deviceDetail['chk_new_device_runno'];
			$GLOBALS['deviceDetail'].="\n\nLatitude & Longitude. ";
			$GLOBALS['deviceDetail'].="\n  Lat : ".$l_deviceDetail['chk_new_device_lat']."".$l_deviceDetail['chk_new_device_lat_dir'];
			$GLOBALS['deviceDetail'].="\n  Lng : ".$l_deviceDetail['chk_new_device_lng']."".$l_deviceDetail['chk_new_device_lng_dir'];
		}
		echo($GLOBALS['deviceDetail']);
	}
	/*
	 * This function is used to render the view
	 */
	private function display($data=null)
	{
		$GLOBALS['distributorList']=$this->device_monitoring_model->get_all_Distributor();
		if($GLOBALS['distributorID']!=null)
			$GLOBALS['dealerList']=$this->device_monitoring_model->get_all_Dealer($GLOBALS['distributorID']);
		if($GLOBALS['clientID']!=null)
			$GLOBALS['vehicleGroupList']=$this->device_monitoring_model->get_all_vhGp($GLOBALS['clientID']);
		if($GLOBALS['vehicleGroupID']!=null)
			$GLOBALS['vehicleList']=$this->device_monitoring_model->get_all_vhGp_vehicle($GLOBALS['vehicleGroupID']);
		$GLOBALS['deviceList']=$this->device_monitoring_model->get_device_details($data);
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('device_monitoring_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Settings_ctrl extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->model ( 'common_model' );
		$this->common_model->check_session ();
		$this->load->model ( 'settings_model' );
		$GLOBALS ['eventLogRequired'] = $this->common_model->get_setting_value ( "ActivityLoggingRequired" ); // whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$host_name = exec ( "hostname" ); // to get "hostname"
		$host_name = trim ( $host_name ); // remove any spaces before and after
		$ip = gethostbyname ( $host_name );
		$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
		$GLOBALS['ID'] = $this->session->userdata('login');
	}
	function index() {
		$this->value_settings ();
	}
	/*
	 * function to get the values from settings table
	 */
	public function value_settings() {
		$data ['row'] = $this->settings_model->get_settings_data ();
		$this->common_model->menu_display ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'settings_view', $data );
		$this->load->view ( 'header_footer/footer' );
	}
	
	/*
	 * function enables to edit only selected row from the view.
	 */
	public function edit() {
		$setting_id = $this->input->get ( 'setting_id' );
		$data ['row'] = $this->settings_model->get_settings_data ();
		$data ['row_edit'] = $this->settings_model->get_settings_edit_data ( $setting_id );
		
		$this->common_model->menu_display ();
		
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'settings_view', $data );
		$this->load->view ( 'header_footer/footer' );
	}
	/*
	 * function to validate and update edited row value into the database as well as display in screen.
	 */
	function save() {
		$this->form_validation->set_rules ( 'setting_key_value', 'Key Value', 'trim|callback_check_KeyValue' ); // trim|required|xss_clean|strtoupper
		
		if ($this->form_validation->run () == FALSE) {
			$this->value_settings ();
		} else {
			
			$data = array (
					'setting_key_value' => $this->input->post ( 'setting_key_value' ) 
			);
			$id = $this->input->post ( 'id' );
			$this->settings_model->update_settings_data ( $id, $data );
			$data ['row'] = $this->settings_model->get_settings_data ();
			$data ['output'] = 'key Value successfully updated.';
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'settings_view', $data );
			$this->load->view ( 'header_footer/footer' );
			if (trim ( $GLOBALS ['eventLogRequired'] ) == REQUIRED) // parameters are user id, ip, screen id, event description
				$this->common_model->insert_event_value ($GLOBALS['ID']['sess_userid'], $GLOBALS ['ip'], SETTINGS, "Settings data updated: " . "Settings ID=" . $id . ", Key Value=" . $data ['setting_key_value'] );
		}
	}
	
	/*
	 * function to validate given input before updating.
	 */
	function check_KeyValue($setting_key_value) {
		$settingName = trim ( $this->input->post ( 'SetName' ) );
		// log_message("error","********************".$keyvalue."**********".$settingName."**********");
		if (empty ( $setting_key_value )) {
			$this->form_validation->set_message ( 'check_KeyValue', '%s required.' );
			return false;
		} else if ($settingName == 'ActivityLoggingRequired') {
			if ($setting_key_value == 'Y')
				return true;
			else if ($setting_key_value == 'N')
				return true;
			else {
				$this->form_validation->set_message ( 'check_KeyValue', '%s Should be Y or N.' );
				return false;
			}
		} else if ($settingName == 'MinPasswordLength') {
			if (! is_numeric ( $setting_key_value )) {
				$this->form_validation->set_message ( 'check_KeyValue', 'Enter numeric value.' );
				return false;
			} else {
				return true;
			}
		} elseif ($settingName == 'DefaultMailExtension') {
			if (! preg_match ( '/\w\.+\D/', $setting_key_value )) {
				$this->form_validation->set_message ( 'check_KeyValue', 'Enter valid mail extension.' );
				return false;
			} else
				return true;
		} else {
			return true;
		}
	}
}
?>
<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	class Detailed_activity_report extends CI_Controller {

		public function __construct() {

			parent::__construct ();

			$this->load->model("common_model");

			$GLOBALS['page_title'] = "Detailed Activity Report";
			$GLOBALS['ID'] = $this->session->userdata('login');
			$session_userid = $GLOBALS['ID']['sess_userid'];

			// to get user ip and host name
			$host_name = exec ( "hostname" ); // to get "hostname"
			$host_name = trim ( $host_name ); // remove any spaces before and after
			$ip = gethostbyname ( $host_name );
			$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
		}

		/*
		 * Function to display
		 * the view
		 */
		public function index() {
			$this->display();
		}

		public function stats() {

			$data = array(
					array(
							"id"=> '1',
							"vehicle_name"=> "Vehicle1",
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"no_of_trip"=> "3",
							"no_of_violation"=> "0",							
							"distance"=> "30"
					),
					array(
							"id"=> '2',
							"vehicle_name"=> "Vehicle2",
							"date"=> "2016-12-01",
							"route_name"=> "Route 2",
							"no_of_trip"=> "3",
							"no_of_violation"=> "3",							
							"distance"=> "50"
					),
					array(
							"id"=> '3',
							"vehicle_name"=> "Vehicle3",
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"no_of_trip"=> "5",
							"no_of_violation"=> "3",							
							"distance"=> "50"
					),
					array(
							"id"=> '4',
							"vehicle_name"=> "Vehicle5",
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"no_of_trip"=> "5",
							"no_of_violation"=> "3",							
							"distance"=> "50"
					),
					array(
							"id"=> '5',
							"vehicle_name"=> "Vehicle1",
							"date"=> "2016-12-01",
							"route_name"=> "Route 2",
							"no_of_trip"=> "5",
							"no_of_violation"=> "3",							
							"distance"=> "50"
					),
					array(
							"id"=> '6',
							"vehicle_name"=> "Vehicle4",
							"date"=> "2016-12-01",
							"route_name"=> "Route 4",
							"no_of_trip"=> "5",
							"no_of_violation"=> "3",							
							"distance"=> "50"
					),
					array(
							"id"=> '7',
							"vehicle_name"=> "Vehicle6",
							"date"=> "2016-12-01",
							"route_name"=> "Route 1",
							"no_of_trip"=> "5",
							"no_of_violation"=> "3",							
							"distance"=> "50"
					)
						
			);

			echo json_encode($data);
		}

		/*
		 * This function is used to render the view
		 */
		private function display() {
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'detailed_activity_report_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer_rmc' );
		}

	}

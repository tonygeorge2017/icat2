<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_group_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('driver_group_model');
		$this->load->helper(array('form', 'url'));
		//the below data will save to the vts_driver_group table
		$GLOBALS['dver_group_id']=null;
		$GLOBALS['dver_group']=null;
		$GLOBALS['dver_parent_group_id']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['dver_group_client_id']=null;
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		
		$GLOBALS['parentItemGroupList']=$this->driver_group_model->get_all_parent_driver_group($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['driverGroupList']=$this->driver_group_model->get_vts_driver_group(null,null,null,md5($GLOBALS['ID']['sess_clientid']));
// 		$GLOBALS['clientList']=$this->driver_group_model->get_allClients();
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	public function index()
	{
		$this->table_pagination(md5($GLOBALS['ID']['sess_clientid']));
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_driver_group in database.
	 */
	public function vts_driver_group_validation()
	{
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		if($GLOBALS['temp']!="-1")
			$this->table_pagination(md5($GLOBALS['temp']));
		else
		{
		$GLOBALS['dver_group_id']=(null!=trim($this->input->post('DriverGroupId'))?trim($this->input->post('DriverGroupId')):null);
		$GLOBALS['dver_group']=(null!=trim($this->input->post('VehicleGroup'))?trim($this->input->post('DriverGroup')):null);
		$GLOBALS['dver_parent_group_id']=(null!=trim($this->input->post('ParentGroup'))?trim($this->input->post('ParentGroup')):null);
		$GLOBALS['active']=(null!=($this->input->post('DriverGroupIsActive'))?$this->input->post('DriverGroupIsActive'):NOT_ACTIVE);
		
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$GLOBALS['dver_group_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		}
		else
		{
			$GLOBALS['dver_group_client_id']=$GLOBALS['ID']['sess_clientid'];
		}
		$GLOBALS['temp']=$GLOBALS['dver_group_client_id'];
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('DriverGroup', 'Driver Group', 'callback_check_driver_group');
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_cnt_id');
		}

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Driver in vts_driver_group table.
		{
			$this->table_pagination(md5($GLOBALS['dver_group_client_id']));
		}
		else
		{
			$data['driver_group']=$GLOBALS['dver_group'];
			$data['driver_parent_group_id']=$GLOBALS['dver_parent_group_id'];
			$data['driver_group_isactive']=$GLOBALS['active'];
			$data['driver_group_client_id']=$GLOBALS['dver_group_client_id'];
			
			if($GLOBALS['dver_group_id']!=null) //edit
			{
				$data['driver_group_id']=$GLOBALS['dver_group_id'];
				if ($this->driver_group_model->InsertOrUpdateOrdelete_vts_driver_group($GLOBALS['dver_group_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER_GROUP,
								"Updated Driver Group: Driver Group ID=".trim($GLOBALS['dver_group_id']).", Driver Group=".trim($GLOBALS['dver_group']).",Driver Parent Group Id=".trim($GLOBALS['dver_parent_group_id']).", Driver Is Active=".trim($GLOBALS['active']).", Driver Group Client Id=".trim($GLOBALS['dver_group_client_id']));
	
						$GLOBALS['outcome']="Driver Group ".'"'.trim($GLOBALS['dver_group']).'"'." updated successfully";
						$GLOBALS['temp']=$GLOBALS['dver_group_client_id'];
						$GLOBALS['dver_group_id']=null;
						$GLOBALS['dver_parent_group_id']=null;
						$GLOBALS['active']=null;
				}
			}
			else	//insert
			{
				if($this->driver_group_model->InsertOrUpdateOrdelete_vts_driver_group(null,$data,'insert'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER_GROUP,
								"Created New Driver Group: Vehicle Group=".trim($GLOBALS['dver_group']).",Driver Parent Group Id=".trim($GLOBALS['dver_parent_group_id']).", Driver Is Active=".trim($GLOBALS['active']).", Driver Group Client Id=".trim($GLOBALS['dver_group_client_id']));
	
							$GLOBALS['outcome']="New Driver Group ".'"'.trim($GLOBALS['dver_group']).'"'." created successfully";
							$GLOBALS['temp']=$GLOBALS['dver_group_client_id'];
							$GLOBALS['dver_group_id']=null;
							$GLOBALS['dver_parent_group_id']=null;
							$GLOBALS['active']=null;
				}
			}
			$this->table_pagination(md5($GLOBALS['temp']));
		}
		}
	}
	
	/*
	 * This function is to check the client name, only for the Autograde Users
	 */
	public function check_cnt_id($cnt_id)
	{
		if (empty($cnt_id))
		{
			$this->form_validation->set_message('check_cnt_id', '%s required');
			return FALSE;
		}
		$GLOBALS['dver_group_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the driver group name
	 */
	public function check_driver_group($dver_grp)
	{
		$GLOBALS['driverGroupList']=$this->driver_group_model->get_vts_driver_group(null,null,null,md5($GLOBALS['temp']));
		
		$GLOBALS['dver_group']=null;
		$dver_grp1=str_replace(" ", "", $dver_grp);
		
		if (empty($dver_grp1)) //Check whether the Driver Group field is empty
		{
			$this->form_validation->set_message('check_driver_group', '%s required');
			return FALSE;
		}
		else if (is_numeric($dver_grp1)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_driver_group', '%s should not be numeric' );
			return FALSE;
		}
		else if (strlen($dver_grp1) < MIN_FIELD_LENGTH || strlen($dver_grp1) > MAX_FIELD_LENGTH ) // Check whether the driver group name is between 5-20 characters
		{
			$this->form_validation->set_message ( 'check_driver_group', 'Group name should be in between 5-20 characters' );
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $dver_grp1))
		{
			$this->form_validation->set_message ( 'check_driver_group', 'Please enter valid name' );
			return FALSE;
		}
		else if(preg_match("/[+]/", $dver_grp1))
		{
			$this->form_validation->set_message ( 'check_driver_group', 'Please enter valid name' );
			return FALSE;
		}
		else if(preg_match('/^[0-9]/', $dver_grp1))
		{
			$this->form_validation->set_message ( 'check_driver_group', 'Please enter valid group name' );
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['driverGroupList'] as $row ) {
				if ($GLOBALS ['dver_group_id'] == null) {
					if (trim ( strtoupper ( $row ['driver_group'] ) ) == strtoupper ( $dver_grp )) // Check whether driver group already exist in vts_driver_group table
					{
						$GLOBALS['outcome_with_db_check']="Driver Group ".'"'.trim($row['driver_group']).'"'." already exist";
						$this->form_validation->set_message('check_driver_group', '');
						
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['driver_group'] ) ) == strtoupper ( $dver_grp ) && $GLOBALS ['dver_group_id'] != trim ( $row ['driver_group_id'] )) // Check whether driver group already exist in vts_driver_group table while update
					{
						$GLOBALS['outcome_with_db_check']="Driver Group ".'"'.trim($row['driver_group']).'"'." already exist";
						$this->form_validation->set_message('check_driver_group', '');
						return FALSE;
					}
				}
			}
		}
		$GLOBALS['dver_group']=(null!=trim($this->input->post('DriverGroup'))?trim($this->input->post('DriverGroup')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_driver_group
	 */
	public function editOrDelete_vts_driver_group($id,$opt,$drgpId)
	{
		$posted=$this->input->get_post('driver_group_id');
		$operation=$opt;			//it hold the operation to perform(i.e. edit or delete).
		$DriverGroupId=$drgpId;			//it hold the driver group id to edit or delete.
	
		$row=$this->driver_group_model->get_vts_driver_group(null,null,$DriverGroupId,$id);
		if($operation==md5("edit"))
		{
			$operation=="edit";
			$GLOBALS=array('dver_group_id'=>trim($row['driver_group_id']),'dver_group'=>trim($row['driver_group']),'dver_parent_group_id'=>trim($row['driver_parent_group_id']),'active'=>trim($row['driver_group_isactive']),'dver_group_client_id'=>trim($row['driver_group_client_id']));//It used to fill up the textbox in view.
		}
		else if($operation==md5("delete"))
		{
			try
			{
				if($this->driver_group_model->InsertOrUpdateOrdelete_vts_driver_group($DriverGroupId,null,"delete"))
				{
					$GLOBALS['outcome']="Driver Group ".'"'.trim($row['driver_group']).'"'." deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER_GROUP,
								"Driver Group is deleted: Driver Grop ID=".trim($GLOBALS['dver_group_id']).", Driver Group=".trim($GLOBALS['dver_group']).",Driver Parent Group Id=".trim($GLOBALS['dver_parent_group_id']).", Driver Is Active=".trim($GLOBALS['active']).", Driver Group Client Id=".trim($GLOBALS['dver_group_client_id']));
				}else
				{
					$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['driver_group'] ).'"' . " Can't able to delete(Referred some where else)";
				}
					
			}catch (Exception $ex){
				$this->table_pagination($id);
			}
		}
		$this->table_pagination($id);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id=null,$pageNo=0){
		
		if($cl_id!=null)
		{
			$GLOBALS['dver_group_client_id']=$cl_id;
		}
		$config['uri_segment'] = URI_SEGMENT_FOR_FOUR;
		$GLOBALS['ID'] = $this->session->userdata('login');
 		$GLOBALS['parentItemGroupList']=$this->driver_group_model->get_all_parent_driver_group($GLOBALS['dver_group_client_id']);
		$config['base_url'] = base_url("index.php/driver_group_ctrl/table_pagination/".$GLOBALS['dver_group_client_id']);
		$GLOBALS['driverGroupList']=$this->driver_group_model->get_vts_driver_group(null,null,null,$GLOBALS['dver_group_client_id']);
		$GLOBALS['clientList']=$this->driver_group_model->get_allClients();
		
		$config['total_rows'] = count($GLOBALS['driverGroupList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['driverGroupList']=$this->driver_group_model->get_vts_driver_group(ROW_PER_PAGE,$pageNo,null,$GLOBALS['dver_group_client_id']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('driver_group_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}

}

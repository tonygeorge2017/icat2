<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Home_ctrl extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );
		$this->common_model->check_session ();
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['clientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS['spot']=array();
	}
	public function index() {
		$this->display ( $GLOBALS );
	}
	public function view() {
		$GLOBALS ['clientID'] = (null != trim ( $this->input->post ( 'ClientID' ) ) ? trim ( $this->input->post ( 'ClientID' ) ) : null);
		$this->display ();
	}
	public function display() {
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['timezone'] = ($GLOBALS ['ID'] ['sess_time_zonename']) ? $GLOBALS ['ID'] ['sess_time_zonename'] : ''; // ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			$GLOBALS ['clientList'] = $this->common_model->get_allClients ();
			$this->common_model->menu_display ();
			//$this->get_speed_violations_data ();
			//$this->get_service_status ();
			$this->get_vehicles_grid_data ();
			
		}
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'home_view', $GLOBALS ); // load view Home page
		$this->load->view ( 'header_footer/footer' );
	}
	/*
	 * function to get no of drivers and called from ajax view
	 */
	public function get_drivers($clientid, $drvgpid = null) {
		$no_drivers = $this->common_model->get_no_drivers ( $clientid, $drvgpid );
		echo $no_drivers;
	}
	/*
	 * function to get no of vehicles and called from ajax view
	 */
	public function get_vehicles($clientid, $vhgpid = null) {
		$no_vehicles = $this->common_model->get_vehicles_data ( $clientid, $vhgpid );
		echo $no_vehicles;
	}
	/*
	 * function to get middle grid data and called from ajax view
	 */
	public function get_grid($clientid, $drvgpid, $vhgpid) {
		$grid_data = array();//$this->common_model->get_dashboard_table ( $clientid, $drvgpid, $vhgpid );
		//$speed = $this->get_speed_grid($clientid, $vhgpid,$drvgpid);
		$html2 = "";
		$html = "<table>
					<tr>
					<th>Vehicle Running Details</th>
					<th>Today</th>
					<th>This Month</th>
					<th>Last Month</th>
					<th>This Year</th>
				</tr>";
		foreach ( $grid_data as $row ) {
			$html2 .= "<tr>
						<td>" . $row ['title'] . "</td>
						<td>" . $row ['today'] . "</td>
						<td>" . $row ['thismonth'] . "</td>
						<td>" . $row ['lastmonth'] . "</td>
						<td>" . $row ['thisyear'] . "</td>
					</tr>";
		}
		"</table>";
		//$this->get_speed_violations_data($clientid,$vhgpid,$drvgpid);
		echo $html . $html2;
		
	}
/*
 * function to get speed grid data and called from ajax view
*/
	public function get_speed_grid($clientid,$vhgpid,$drvgpid) {
		$speed_data = array();//$this->common_model->get_speed_violations_data($clientid,$vhgpid,$drvgpid);
		$html1 = "";
		$html3="";
		$html2 = "<table>
					<tr>
					<th>Car</th>
					<th>From Date</th>
				    <th>To Date</th>
					<th>Speed</th>
					<th>Driver</th>
					<th>Spot</th>
				</tr>";
		if(!empty($speed_data)) {
		foreach ( $speed_data as $row ) {
			$row['spot'] = $this->common_model->getPlaceName ( $row ['latitude'], $row ['longitude'] );
			$html3 .= "<tr>
						<td>" . $row ['vehicle'] . "</td>
						<td>" . $row ['from_datetime'] . "</td>
						<td>" . $row ['to_datetime'] . "</td>
						<td>" . $row ['maxspeed'] . "</td>
						<td>" . $row ['driver'] . "</td>
						<td>" . $row ['spot'] . "</td>
					</tr>";
			}
		//}
		"</table>";
	//if(!empty($speed_data)) {
		echo $html2 .$html3 . $html1;
		}else {
		$html2 .= "<tr><td style='padding-top: 10px;' colspan='4'> No records found. </td></tr>";
		echo $html2 . $html1;
		} 
	
	}
	
	/*
	 * fucntion to get sevice grid data calling from view
	 */
	public function get_service_grid($clientid) {
		$service_data = array();//$this->common_model->get_service_status_data($clientid);
		$html1 = "";
		$html3="";
		$html2 = "<table>
					<tr>
					<th>Car</th>
					<th>Date</th>
				    <th>Service</th>
					<th>Status</th>
				</tr>";
		if(!empty($service_data)) {
			foreach ( $service_data as $row ) {
				$html3 .= "<tr>
						<td>" . $row ['vehicle'] . "</td>
						<td>" . $row ['servicedate'] . "</td>
						<td>" . $row ['service'] . "</td>
						<td>" . $row ['status'] . "</td>
					</tr>";
			}
			//}
			"</table>";
			//if(!empty($speed_data)) {
			echo $html2 .$html3 . $html1;
		}else {
			$html2 .= "<tr><td style='padding-top: 10px;' colspan='4'> No records found. </td></tr>";
			echo $html2 . $html1;
		}
	
	}
	
	/*
	 * created by shruthi 01/07/15
	 * function to get values for dashboard
	 */
	public function get_vehicles_grid_data($drv_gp_id = null, $vh_gp_id = null) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		if ($GLOBALS ['sessClientID'] == AUTOGRADE_USER) {
			$GLOBALS ['vhgp'] = $this->common_model->get_vehicles_group_data ( $GLOBALS ['clientID'] );
			$GLOBALS ['vehicles'] = $this->common_model->get_vehicles_data ( $GLOBALS ['clientID'], $vh_gp_id );
			$GLOBALS ['moving_vehicles'] = array();//$this->common_model->get_gps_data ();
			$GLOBALS ['drgp'] = $this->common_model->get_drivers_group_data ( $GLOBALS ['clientID'] );
			$GLOBALS ['drivers'] = $this->common_model->get_no_drivers ( $GLOBALS ['clientID'], $drv_gp_id );
			$GLOBALS ['grid_data'] = array();//$this->common_model->get_dashboard_table ( $GLOBALS ['clientID'], $drv_gp_id, $vh_gp_id );
			$GLOBALS['speed_data'] = array();//$this->common_model->get_speed_violations_data($GLOBALS ['clientID'],$vh_gp_id,$drv_gp_id);
			$i=0;
			foreach ($GLOBALS['speed_data'] as $row)
			{
				//if(isset($row['latitude']))
				$GLOBALS['spot'][$i]=$row['spot'] = $this->common_model->getPlaceName ( $row ['latitude'], $row ['longitude'] );
				$i++;
			}
			$GLOBALS['service_data']=$this->common_model->get_service_status_data($GLOBALS ['clientID']);
		} else {
			$GLOBALS ['vhgp'] = $this->common_model->get_vehicles_group_data ( $GLOBALS ['sessClientID'] );
			$GLOBALS ['vehicles'] = $this->common_model->get_vehicles_data ( $GLOBALS ['sessClientID'], $vh_gp_id );
			$GLOBALS ['moving_vehicles'] = array();//$this->common_model->get_gps_data ();
			$GLOBALS ['drgp'] = $this->common_model->get_drivers_group_data ( $GLOBALS ['clientID'] );
			$GLOBALS ['drivers'] = $this->common_model->get_no_drivers ( $GLOBALS ['sessClientID'], $drv_gp_id );
			$GLOBALS ['grid_data'] = array();//$this->common_model->get_dashboard_table ( $GLOBALS ['sessClientID'], $drv_gp_id, $vh_gp_id );
			$GLOBALS['speed_data'] = array();//$this->common_model->get_speed_violations_data($GLOBALS ['sessClientID'],$vh_gp_id,$drv_gp_id);
			$i=0;
			foreach ($GLOBALS['speed_data'] as $row)
			{
				$GLOBALS['spot'][$i]=$row['spot'] = $this->common_model->getPlaceName ( $row ['latitude'], $row ['longitude'] );
				$i++;
			}
			$GLOBALS['service_data']=array();//$this->common_model->get_service_status_data($GLOBALS ['sessClientID']);
		}
	}
	/*
	 * function to get values for speed violation grid old function
	 */
// 	public function get_speed_violations_data() {
// 		$GLOBALS ['row1'] = $this->common_model->get_speed_violations_data ();
// 		// print_r($GLOBALS['row1']);
// 		$GLOBALS ['string1'] = null;
// 		if ($GLOBALS ['row1'] != NULL) {
// 			for($i = 0; $i < 5; $i ++) {
// 				$string = str_replace ( '(', '', $GLOBALS ['row1'] [$i] ['dd'] );
// 				$string = str_replace ( ')', '', $string );
// 				$string = str_replace ( '""', ' ', $string );
// 				$string = str_replace ( '"', '', $string );
				
// 				$str = explode ( ",", $string );
// 				$GLOBALS ['string1'] [$i] = $str;
// 				$GLOBALS ['address'] [$i] = $this->common_model->getPlaceName ( $str [7], $str [8] );
// 			}
// 		} else {
// 			return false;
// 		}
// 	}
	/*
	 * function to get service status data  old function
	 */
// 	public function get_service_status() {
// 		$GLOBALS ['row2'] = $this->common_model->get_service_status_data ();
// 		$GLOBALS ['string2'] = null;
// 		if (count ( $GLOBALS ['row2'] ) == 0) {
// 		return false;
// 		} else {
// 			for($i = 0; $i < count ( $GLOBALS ['row2'] ); $i ++) {
// 				$string = str_replace ( '(', '', $GLOBALS ['row2'] [$i] ['service_data'] );
// 				$string = str_replace ( ')', '', $string );
// 				$string = str_replace ( '""', ' ', $string );
// 				$string = str_replace ( '"', '', $string );
// 				$str = explode ( ",", $string );
// 				$GLOBALS ['string2'] [$i] = $str;
// 			}
// 		}
// 	}
	
	
	public function logout() {
		$this->session->unset_userdata ( 'login' );
		$this->session->sess_destroy ();
		redirect ( 'login_ctrl', 'refresh' );
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Geofence_mgnt_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		
		$this->load->model('geofence_mgnt_model');
		$this->load->helper(array('form', 'url'));
		
		$GLOBALS['clientList']=array();
		
		
		$GLOBALS['vehicleGroupList']=array();
		$GLOBALS['vehicleList']=array();
		$GLOBALS['routeList']=array();
		$GLOBALS['vhRouteList']=array();
		$GLOBALS['editrouteList']=array();
		$GLOBALS['vhID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['deviceID']=null;
		$GLOBALS['editVhID']=null;
		$GLOBALS['routeID']=array();		
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;
		//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination();
	}
	/*
	 * This function is used to validate the posted values 
	 * before store.
	 */
	public function vh_route_validation(){

			$this->form_validation->set_message('required', '%s required');			
			
			
			
			
			$GLOBALS['deviceID']=(null!=($this->input->post('Device'))?$this->input->post('Device'):null);
			$GLOBALS['routeID']=(null!=($this->input->post('RouteArr'))?$this->input->post('RouteArr'):null);
			$routeArray=$GLOBALS['routeID'];			
			//log_message('debug',"********testtest".print_r($GLOBALS,true));
			//log_message('debug','RouteArray'.print_r($routeArray,true));
			$fence_data=array();
			foreach ($routeArray as $indx=>$value)
			{
			  $fence_data[$indx]["vh_rt_device_id"]=$GLOBALS['deviceID'];
			  $fence_data[$indx]["vh_rt_route_id"]=$value;
			  $temp=$this->input->post('AlertType_'.$value);
			  $fence_data[$indx]["vh_rt_alert_type"]=$temp[0];
			}
			//log_message('debug','RouteArray'.print_r($fence_data,true));
			
			
			$this->form_validation->set_rules('Device', 'Device', 'required');			
			$this->form_validation->set_rules('RouteArr', 'Route', 'required');
			if ($this->form_validation->run() != FALSE)
			{
				//log_message("debug","#####test1");
				//$data['vh_rt_vehicle_id']=$GLOBALS['deviceID'];
				//$data['vh_rt_route_id']=$GLOBALS['routeID'];
				if($GLOBALS['vhID']==null)//insert
				{
					//log_message("debug","#####test2");
					if($this->geofence_mgnt_model->insert_edit_vhroute($fence_data))
					{
	
						$GLOBALS['outcome']='<div style="color: green;">Route assigning succeed.</div>';
						$this->clear_all();
					}
					else{
						$GLOBALS['outcome']='<div style="color: red;">Route assigning failed.</div>';
					}
				}
				else {//edit
					//log_message("debug","#####test3");
					if($this->geofence_mgnt_model->insert_edit_vhroute($fence_data, $GLOBALS['vhID'], "edit"))
					{

						$GLOBALS['outcome']='<div style="color: green;">Updation succeed.</div>';
						$this->clear_all();
					}
					else{
						$GLOBALS['outcome']='<div style="color: red;">Updation failed.</div>';
					}
				}
			}
		
		
		$this->table_pagination();
	}
	private function clear_all()
	{
		$GLOBALS['vhID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['deviceID']=null;
		$GLOBALS['routeID']=null;
	}
	/* This function is used to get the vehicle for the selected vehicle group and return back the list of
	* vehicles in JSON format with out refresh the page.
	*  $vhGrp - vehicel group id
	* Return type - JSON string
	*/
	public function get_vhl_grp_vehicle($vhGrp=null, $vhID=0)
	{		
		$vhl_grp_vehicle=null;
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=json_encode($this->geofence_mgnt_model->get_all_vhGp_vehicle($vhGrp, $vhID));
				
		}
		echo($vhl_grp_vehicle);
	}
	/*
	 * This function is used to edit the route details
	 */
	public function edit_routedetails($vhRouteOrVehicleID, $opt='edit')
	{
		
		$delete=($opt==md5('delete'))?true:false;
		$result=$this->geofence_mgnt_model->get_VhRouteList( null, null, $vhRouteOrVehicleID, $delete);
		print_r($result);
		if($result!=null)
		{
			$GLOBALS['clientID']=isset($result[0]['route_client_id'])?$result[0]['route_client_id']:null;
			if($opt=='edit')
			{
				$GLOBALS['vhID']=isset($result[0]['vh_rt_device_id'])?$result[0]['vh_rt_device_id']:null;
				
				$GLOBALS['editVhID']=$GLOBALS['deviceID']=isset($result[0]['vh_rt_device_id'])?$result[0]['vh_rt_device_id']:null;
				$GLOBALS['routeID']=isset($result[0]['vh_rt_route_id'])?$result[0]['vh_rt_route_id']:null;
				foreach ($result as $row)
					$GLOBALS['editrouteList'][$row["vh_rt_route_id"]]=$row["alert_type"];
			}
			else if($opt==md5('delete'))
			{
				if($this->geofence_mgnt_model->insert_edit_vhroute(null, $vhRouteOrVehicleID, 'delete'))
				{
					
					$GLOBALS['outcome']='<div style="color: green;">Removed successfully.</div>';
				}
				else
				{
					$GLOBALS['outcome']='<div style="color: red;">Removal failed.</div>';
				}
			}				
		}
		$this->table_pagination();
	}

	/*
	 * This function is used to perform the pagination.
	 */
	public function table_pagination($pageNo=0)
	{
		
		
	
			$device_id=($GLOBALS['deviceID']!=null)? $GLOBALS['editVhID']:0;		
			$GLOBALS['device_list']=$this->geofence_mgnt_model->get_all_device($device_id);
		
		$GLOBALS['routeList']=$this->geofence_mgnt_model->get_RouteList();
		$GLOBALS['vhRouteList']=$this->geofence_mgnt_model->get_VhRouteList( ROW_PER_PAGE, $pageNo);
		
		$config['uri_segment']=URI_SEGMENT_FOR_FOUR;
		$config['base_url'] = base_url("index.php/geofence_mgnt_ctrl/table_pagination");
		$config['total_rows'] = count($this->geofence_mgnt_model->get_VhRouteList());
		$config['per_page']=ROW_PER_PAGE;
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('geofence_mgnt_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
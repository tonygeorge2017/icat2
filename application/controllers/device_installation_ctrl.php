<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_installation_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_installation_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['outcome']=null;//jsut show result
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['ID']['sess_client_admin'];
		$GLOBALS['installationID']=null;
		$GLOBALS['clientID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['deviceTypeID']=null;
		$GLOBALS['deviceID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['installedDate']=null;
		$GLOBALS['removedDate']=null;
		$GLOBALS['remarks']=null;
		$GLOBALS['clientList']=null;
		$GLOBALS['deviceList']=null;
		$GLOBALS['deviceTypeList']=null;
		$GLOBALS['vehicleList']=null;
		$GLOBALS['vehicleGroupList']=null;
		$GLOBALS['deviceInstalledList']=null;
		$GLOBALS['isEdit']=NOT_ACTIVE;
		$GLOBALS['sim_number']=null;
		$GLOBALS['provider']=null;
		$GLOBALS['isAtcSim']=NOT_ACTIVE;
		//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination(md5($GLOBALS['sessClientID']));
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function validate_device_installation()
	{
		$GLOBALS['onClientID']=(null!=($this->input->post('OnClientID'))?$this->input->post('OnClientID'):null);
		$GLOBALS['installationID']=(null!=($this->input->post('InstallationID'))?$this->input->post('InstallationID'):null);
		$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null);
		$GLOBALS['deviceTypeID']=(null!=($this->input->post('DeviceTypeID'))?$this->input->post('DeviceTypeID'):null);
		$GLOBALS['deviceID']=(null!=($this->input->post('DeviceID'))?$this->input->post('DeviceID'):null);
		$GLOBALS['vehicleGroupID']=(null!=($this->input->post('VehicleGroupID'))?$this->input->post('VehicleGroupID'):null);
		$GLOBALS['vehicleID']=(null!=($this->input->post('VehicleID'))?$this->input->post('VehicleID'):null);
		$GLOBALS['installedDate']=(null!=($this->input->post('InstallDate'))?trim($this->input->post('InstallDate')):null);
		$GLOBALS['removedDate']=(null!=($this->input->post('RemovedDateTime'))?trim($this->input->post('RemovedDateTime')):null);
		$GLOBALS['remarks']=(null!=($this->input->post('Remarks'))?trim($this->input->post('Remarks')):null);
		$GLOBALS['sim_number']=(null!=trim($this->input->post('SIMNumber'))?trim($this->input->post('SIMNumber')):null);
		$GLOBALS['provider']=(null!=trim($this->input->post('Provider'))?trim($this->input->post('Provider')):null);
		$GLOBALS['isAtcSim']=(null!=($this->input->post('ByAutograde'))?$this->input->post('ByAutograde'):NOT_ACTIVE);
		
		if($GLOBALS['onClientID']==-1)
		{
			$this->form_validation->set_message('required', ' %s required');
			$this->form_validation->set_rules('ClientID', 'Client', 'required');
			$this->form_validation->set_rules('DeviceTypeID', 'Device Type', 'required');
			$this->form_validation->set_rules('DeviceID', 'Device', 'required');
			$this->form_validation->set_rules('VehicleGroupID', 'Vehicle Group', 'required');
			$this->form_validation->set_rules('VehicleID', 'Vehicle', 'required');
			$this->form_validation->set_rules('InstallDate', 'Install Date', 'callback_check_install_date');
			
			if(!empty($GLOBALS['sim_number']))
			{
				$this->form_validation->set_rules('SIMNumber', 'SIM Number', 'callback_check_sim_number');
			}
			if(!empty($GLOBALS['provider']))
			{
				$this->form_validation->set_rules('Provider', 'Provider', 'callback_check_provider');
			}
// 			$this->form_validation->set_rules('ByAutograde', 'ByAutograde', 'callback_check_b');
			
			// if any of the form rule is failed, then it show the error msg in view.
			if ($this->form_validation->run() == FALSE)
			{
				$this->table_pagination(md5($GLOBALS['clientID']));
			}
			else
			{
				$data['dev_install_client_id']=$GLOBALS['clientID'];
				$data['dev_install_device_id']=$GLOBALS['deviceID'];
				$data['dev_install_vehicle_id']=$GLOBALS['vehicleID'];
				$data['dev_install_installed_date']=$GLOBALS['installedDate'];
				$data['dev_install_remarks']=$GLOBALS['remarks'];
				$data['dev_install_mobile_no']=$GLOBALS['sim_number'];
				$data['dev_install_sim_provider']=$GLOBALS['provider'];
				$data['dev_install_is_atc_sim']=$GLOBALS['isAtcSim'];
				
				//$GLOBALS['installationID']=$this->device_installation_model->is_device_exist($GLOBALS['deviceID'],$GLOBALS['vehicleID']);
				if($GLOBALS['installationID']==null)
				{
					if($this->device_installation_model->InsertOrUpdate_device(null,$data,'insert'))
					{
						$GLOBALS['outcome']='<div style="color: green;">Installed Successfully</div>';
						// parameters are user id, ip, screen id, event description
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_INSTALLATION,
									"Install device id= ".$GLOBALS['deviceID']." to vehicle id= ".$GLOBALS['vehicleID']." on '".$GLOBALS['installedDate']."' for client id= ".$GLOBALS['clientID']."' with sim number= ".$GLOBALS['clientID']."' and the sim provider of= ".$GLOBALS['clientID']."' is by Autograde = ".$GLOBALS['clientID']);
						$this->clear_all();
					}
					else {
						$GLOBALS['outcome']='<div style="color: red;">Please check the device and vehicle</div>';
					}
				}
				else {
					if($this->device_installation_model->InsertOrUpdate_device(md5($GLOBALS['installationID']),$data,'edit'))
					{
						$GLOBALS['outcome']='<div style="color: green;">Updated Successfully</div>';
						// parameters are user id, ip, screen id, event description
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_INSTALLATION,
									"Update Device Info: device id= ".$GLOBALS['deviceID']." to vehicle id=".$GLOBALS['vehicleID']." on '".$GLOBALS['installedDate']."' for client id=".$GLOBALS['clientID']."' with sim number= ".$GLOBALS['clientID']."' and the sim provider of= ".$GLOBALS['clientID']."' is by Autograde = ".$GLOBALS['clientID']);
						$this->clear_all();
					}
				}
				$this->table_pagination(md5($GLOBALS['clientID']));
			}
		}
		else
		{
			$this->clear_all();
			$this->table_pagination(md5($GLOBALS['onClientID']));
		}
	}
	/*
	 * This function is usde to clear all the fields
	 */
	private function clear_all(){
		$GLOBALS['installationID']=null;
		$GLOBALS['deviceID']=null;
		$GLOBALS['deviceTypeID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['installedDate']=null;
		$GLOBALS['sim_number']=null;
		$GLOBALS['provider']=null;
		$GLOBALS['isAtcSim']=NOT_ACTIVE;
		$GLOBALS['remarks']=null;
		$GLOBALS['isEdit']=NOT_ACTIVE;
	}
	/*This function is to validate the sim number
	 * sim number -- numeric only
	 */
	public function check_sim_number($sim_num)
	{
		$GLOBALS['sim_number']=null;
		if (!is_numeric($sim_num)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_sim_number', 'Phone number should be numeric' );
			return FALSE;
		}
		else if (strlen($sim_num) < 10 || strlen($sim_num) > 10) // Check whether the number should allow only 10 numbers
		{
			$this->form_validation->set_message ( 'check_sim_number', 'Please enter 10digits valid phone number' );
			return FALSE;
		}
		else if(preg_match('$\.$', $sim_num))
		{
			$this->form_validation->set_message ( 'check_sim_number', 'Please enter valid phone number' );
			return FALSE;
		}
		$GLOBALS['sim_number']=(null!=trim($this->input->post('SIMNumber'))?trim($this->input->post('SIMNumber')):null);
		return true;
	}
	
	/*
	 * This function is to validate the provider field
	 * should accept only the alphabets
	 */
	public function check_provider($provider)
	{
		$GLOBALS['provider']=null;
		if (is_numeric($provider)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_provider', 'Please enter valid network provider' );
			return FALSE;
		}
		else if(preg_match('$\.$', $provider))
		{
			$this->form_validation->set_message ( 'check_provider', 'Please enter valid network provider' );
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $provider))
		{
			$this->form_validation->set_message ( 'check_provider', 'Please enter valid network provider' );
			return FALSE;
		}
		else if(preg_match("/[+]/", $provider))
		{
			$this->form_validation->set_message ( 'check_provider', 'Please enter valid network provider' );
			return FALSE;
		}
		$GLOBALS['provider']=(null!=trim($this->input->post('Provider'))?trim($this->input->post('Provider')):null);
		return true;
	}
	
	/*
	 * This function is used to validate the given client installation date&time
	 * @param
	 *  $installdate - date & time
	 * Return type - bool
	 */
	public function check_install_date($installdate)
	{
		$dates=$this->common_model->get_usertime();
		$date_value=new DateTime($dates);
		$datevalue=$date_value->format('Y-m-d');
		//Check whether the device installation date field is empty
		if (empty($installdate))
		{
			$GLOBALS['outcome']='<div style="color: red;">Installed Date is required.</div><br>';
			return FALSE;
		}
		else if(!empty($installdate))
		{
			$install_date = new DateTime($installdate);
			$clientdate=$install_date->format('Y-m-d');
			if($clientdate > $datevalue)
			{
				$GLOBALS['outcome']='<div style="color: red;">';
				$GLOBALS['outcome'].="Installed date should be less than or equal to today's date.</div><br>";
				return FALSE;
			}
		}
		else
			return true;
	}
	/*
	 * This function is used to remove device from vehicle
	 * @param
	 *  $client - client id
	 *  $installationId - installation id
	 * Return type - void
	 */
	public function edit_remove_device_istallation($client, $installationId, $opt=null)
	{		
		$data['dev_install_removed_date']=$dates=$this->common_model->get_usertime();
		$vh_dev_details=$this->device_installation_model->get_install_vehicle_details($installationId);
		$vehicle="";
		$device="";
		$insDate="";
		$client_name="";
		if($vh_dev_details!=null)
		{
			$vehicleid=$vh_dev_details["vehicle_id"];
			$vehicle=$vh_dev_details["vehicle_name"];
			$device=$vh_dev_details["device_name"];
			$insDate=$vh_dev_details["dev_install_installed_date"];
			$client_name=$vh_dev_details["client_name"];
			$GLOBALS['installationID']=$vh_dev_details["dev_install_id"];
			$GLOBALS['clientID']=$vh_dev_details["dev_install_client_id"];
			$GLOBALS['vehicleGroupID']=$vh_dev_details["vh_gp_id"];
			$GLOBALS['deviceTypeID']=$vh_dev_details["device_device_type_id"];
			$GLOBALS['deviceID']=$vh_dev_details["dev_install_device_id"];
			$GLOBALS['vehicleID']=$vh_dev_details["vehicle_id"];
			$GLOBALS['installedDate']=$vh_dev_details["dev_install_installed_date"];
			$GLOBALS['sim_number']=$vh_dev_details["dev_install_mobile_no"];
			$GLOBALS['provider']=$vh_dev_details["dev_install_sim_provider"];
			$GLOBALS['isAtcSim']=$vh_dev_details["dev_install_is_atc_sim"];
			$GLOBALS['remarks']=$vh_dev_details["dev_install_remarks"];
		}
		if($opt==null)//remove
		{
			if($this->device_installation_model->InsertOrUpdate_device($installationId,$data,'remove',$vehicleid))
			{
				$GLOBALS['outcome']='<div style="color: green;">Removed Successfully</div>';
				// parameters are user id, ip, screen id, event description
				if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
					$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_INSTALLATION,
							"Remove device '".$device."' from vehicle '".$vehicle."' which is installed at '".$insDate."' for client '".$client_name."'");
				$this->clear_all();
			}
			else {
				$GLOBALS['outcome']='<div style="color: red;">';
				$GLOBALS['outcome'].="Can't Delete</div>";
			}
		}
		else {
			$GLOBALS['isEdit']=ACTIVE;
		}
		$this->table_pagination($client);
	}
	/*
	 * This function is used to fill the drop down list box in
	 * view depending upon the device type selection without refresh the page.
	 * @param
	 *  $devTyp - device type
	 *  $clientDevice - client id
	 * Return type - JSON string
	 */
	public function get_device($devTyp, $clientDevice)
	{
		$devices=json_encode($this->device_installation_model->get_allDevice($devTyp,md5($clientDevice)));
		echo($devices);
	}
	/*
	 * This function is used to fill the drop down list box in
	 * view depending upon the vehicle group selection without refresh the page.
	 * @param
	 *  $client - client id
	 *  $vhGp - vehicle group id
	 * Return type - JSON string
	 */
	public function get_vhehicle_gp($client, $vhGp)
	{
		$vehicle=json_encode($this->device_installation_model->get_VehicleList(md5($client), $vhGp));
		echo ($vehicle);
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $client - client id
	 *  $pageNo - page no
	 * Return type - void
	 */
	public function table_pagination($client,$pageNo=0)
	{
		$GLOBALS['clientID']=$client;
		$devID=null;
		$vhID=null;
		if($GLOBALS['isEdit']==ACTIVE)
		{
			$devID=$GLOBALS['deviceID'];
			$vhID=$GLOBALS['vehicleID'];
		}
		$GLOBALS['clientTimeDiff']=$GLOBALS['ID']['sess_time_zonediff'];
		$GLOBALS['installedDate']=($GLOBALS['installedDate']==null)?$this->common_model->get_usertime():$GLOBALS['installedDate'];
		$GLOBALS['deviceTypeList']=$this->device_installation_model->get_allDeviceType($GLOBALS['deviceTypeID']);
		$GLOBALS['clientList']=$this->device_installation_model->get_allClients();
		$GLOBALS['vehicleList']=$this->device_installation_model->get_VehicleList($GLOBALS['clientID'],$GLOBALS['vehicleGroupID'],$vhID);
		$GLOBALS['vehicleGroupList']=$this->device_installation_model->get_allVehicleGroup($GLOBALS['clientID'],$GLOBALS['sessUserID'], $GLOBALS['vehicleGroupID']);
		$GLOBALS['deviceList']=$this->device_installation_model->get_allDevice($GLOBALS['deviceTypeID'],$client,$devID);
		$config['uri_segment']=4;
		$config['base_url'] = base_url("index.php/device_installation_ctrl/table_pagination/".$client."/");
		$GLOBALS['deviceInstalledList']=$this->device_installation_model->get_deviceInstalledList($GLOBALS['sessClientID'],$GLOBALS['clientID'],null,null,$GLOBALS['sessUserID']);
		$config['total_rows'] = count($GLOBALS['deviceInstalledList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['deviceInstalledList']=$this->device_installation_model->get_deviceInstalledList($GLOBALS['sessClientID'],$GLOBALS['clientID'], ROW_PER_PAGE, $pageNo, $GLOBALS['sessUserID']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	/*
	 * This function is used to render the view wiht give $date as argument.
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('device_installation_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
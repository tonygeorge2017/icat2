<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle_service_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('vehicle_service_model');		
		$this->load->helper(array('form', 'url'));
		//the below data will save to the vts_vehicle_service table
		$GLOBALS['vcle_srvc_id']=null;
		$GLOBALS['vcle_srvc_vclegroup_id']=null;
		$GLOBALS['vcle_srvc_service_id']=null;
		$GLOBALS['vcle_srvc_vehicle_id']=null;
		$GLOBALS['vcle_srvc_date']=null;
		$GLOBALS['vcle_srvc_remarks']=null;
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		
		$GLOBALS['vehicleServiceList']=$this->vehicle_service_model->get_vts_vehicle_service(null,null,null,md5($GLOBALS['ID']['sess_clientid']));
		$GLOBALS['clientList']=$this->vehicle_service_model->get_allClients();
		$GLOBALS['vehicleList']=$this->vehicle_service_model->get_allVehicles($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['vehicleGroupList']=$this->vehicle_service_model->get_allVehicleGroups($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['serviceList']=$this->vehicle_service_model->get_allServices();
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	public function index()
	{
		$this->table_pagination(md5($GLOBALS['ID']['sess_clientid']));
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vcle_srvc_id in database.
	 */
	public function vts_vehicle_service_validation()
	{
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		if($GLOBALS['temp']!="-1")
			$this->table_pagination(md5($GLOBALS['temp']));
		else 
		{
		$GLOBALS['vcle_srvc_id']=(null!=($this->input->post('VehicleServiceId'))?$this->input->post('VehicleServiceId'):null);
		$GLOBALS['vcle_srvc_service_id']=(null!=($this->input->post('Service'))?$this->input->post('Service'):null);
		$GLOBALS['vcle_srvc_vehicle_id']=(null!=($this->input->post('Vehicle'))?$this->input->post('Vehicle'):null);
		$GLOBALS['vcle_srvc_date']=(null!=trim($this->input->post('VehicleServiceDate'))?trim($this->input->post('VehicleServiceDate')):null);
		$GLOBALS['vcle_srvc_remarks']=(null!=trim($this->input->post('VehicleServiceRemarks'))?trim($this->input->post('VehicleServiceRemarks')):null);
		$GLOBALS['vcle_srvc_vclegroup_id']=(null!=($this->input->post('VehicleGroup'))?$this->input->post('VehicleGroup'):null);
		
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$GLOBALS['vcle_srvc_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		}
		else
			$GLOBALS['vcle_srvc_client_id']=$GLOBALS['ID']['sess_clientid'];
		
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('Service', 'Service', 'callback_check_service');
		$this->form_validation->set_rules('Vehicle', 'Vehicle', 'callback_check_vehicle');
		$this->form_validation->set_rules('VehicleGroup','Vehicle Group','callback_check_vcle_group');
		
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$this->form_validation->set_rules('ClientName','Client Name','callback_check_client_id');
		}
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Service in Vehicle table.
		{
				
			$this->table_pagination(md5($GLOBALS['vcle_srvc_client_id']));
				
		}
		else
		{
			
			$data['vh_ser_service_id']=$GLOBALS['vcle_srvc_service_id'];
			$data['vh_ser_vehicle_id']=$GLOBALS['vcle_srvc_vehicle_id'];
			$data['vh_ser_service_date']=$GLOBALS['vcle_srvc_date'];
			$data['vh_ser_remarks']=$GLOBALS['vcle_srvc_remarks'];
			
			
			
			if($GLOBALS['vcle_srvc_id']!=null)	//edit
			{
				$data['vh_ser_id']=$GLOBALS['vcle_srvc_id'];
				if ($this->vehicle_service_model->InsertOrUpdateOrdelete_vts_vehicle_service($GLOBALS['vcle_srvc_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_SERVICE,
								"Updated Vehicle Service: Vehicle Service ID=".trim($GLOBALS['vcle_srvc_id']).", Service=".trim($GLOBALS['vcle_srvc_service_id']).",Vehicle =".trim($GLOBALS['vcle_srvc_vehicle_id']).", Vehicle Service Date=".trim($GLOBALS['vcle_srvc_date']).", Vehicle Service Remarks=".trim($GLOBALS['vcle_srvc_remarks']));
	
						$GLOBALS['outcome']="Vehicle Service updated successfully";
						$GLOBALS['temp']=$GLOBALS['vcle_srvc_client_id'];
						$GLOBALS['vcle_srvc_id']=null;
						$GLOBALS['vcle_srvc_date']=null;
						$GLOBALS['vcle_srvc_service_id']=null;
						$GLOBALS['vcle_srvc_vehicle_id']=null;
						$GLOBALS['vcle_srvc_vclegroup_id']=null;
						
				}
			}
			else	//insert
			{
				if($this->vehicle_service_model->InsertOrUpdateOrdelete_vts_vehicle_service(null,$data,'insert'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_SERVICE,
								"Created New Vehicle Service: Service=".trim($GLOBALS['vcle_srvc_service_id']).",Vehicle =".trim($GLOBALS['vcle_srvc_vehicle_id']).", Vehicle Service Date=".trim($GLOBALS['vcle_srvc_date']).", Vehicle Service Remarks=".trim($GLOBALS['vcle_srvc_remarks']));
	
							$GLOBALS['outcome']="New Vehicle Service created successfully";
							$GLOBALS['temp']=$GLOBALS['vcle_srvc_client_id'];
							$GLOBALS['vcle_srvc_service_id']=null;
							$GLOBALS['vcle_srvc_date']=null;
							$GLOBALS['vcle_srvc_vehicle_id']=null;
							$GLOBALS['vcle_srvc_vclegroup_id']=null;
				}
			}
			$this->table_pagination(md5($GLOBALS['temp']));
		}
		}
	}
	
	/*
	 * This function is to validate the client dropdown
	 */
	public function check_client_id($vcle_cnt_id)
	{
		if (empty($vcle_cnt_id))
		{
			$this->form_validation->set_message('check_client_id', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_srvc_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		return true;
	}
	
	/*
	 * This function is to validate the vehicle group field
	 */
	public function check_vcle_group($vcle_gup)
	{
		$GLOBALS['vcle_srvc_vclegroup_id']=null;
		if (empty($vcle_gup))
		{
			$this->form_validation->set_message('check_vcle_group', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_srvc_vclegroup_id']=(null!=($this->input->post('VehicleGroup'))?$this->input->post('VehicleGroup'):null);
		return true;
	}
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_vehicle_service service
	 */
	public function check_service($vcle_srvc)
	{
		$GLOBALS['vcle_srvc_service_id']=null;
		if (empty($vcle_srvc)) //Check whether the Service field is empty
		{
			$this->form_validation->set_message('check_service', '%s required');
			return FALSE;
		}
		
		$GLOBALS['vcle_srvc_service_id']=(null!=($this->input->post('Service'))?$this->input->post('Service'):null);
		return true;
	}
	
	/*This function is the callback function of form validation, it is used to validate the vts_vehicle_service vehicle field */
	
	public function check_vehicle($vcle_srvc_vcle)
	{
		$GLOBALS['vcle_srvc_vehicle_id']=null;
		if (empty($vcle_srvc_vcle))
		{
			$this->form_validation->set_message('check_vehicle', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_srvc_vehicle_id']=(null!=($this->input->post('Vehicle'))?$this->input->post('Vehicle'):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_vehicle_service
	 */
	public function editOrDelete_vts_vehicle_service($id,$opt,$vhId)
	{
// 		$posted=$opt;//$this->input->get_post('vehicle_group_id');
// 		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$opt;			//it hold the operation to perform(i.e. edit or delete).
		$VehicleServiceId=$vhId;			//it hold the vehicle service id to edit or delete.
		$row=$this->vehicle_service_model->get_vts_vehicle_service(null,null,$VehicleServiceId,$id);
		if($operation==md5("edit"))
		{
			//log_message('debug', 'inside edit sss'.count($row));
			$operation=="edit";
			$GLOBALS=array('vcle_srvc_id'=>trim($row['vh_ser_id']),'vcle_srvc_client_id'=>trim($row['vehicle_group_client_id']),'vcle_srvc_vclegroup_id'=>trim($row['vehicle_group_id']),'vcle_srvc_vehicle_id'=>trim($row['vh_ser_vehicle_id']),'vcle_srvc_service_id'=>trim($row['vh_ser_service_id']),'vcle_srvc_date'=>trim($row['vh_ser_service_date']),'vcle_srvc_remarks'=>trim($row['vh_ser_remarks']));//It used to fill up the textbox in view.
				
		}
		else if($operation==md5("delete"))
		{
			try
			{
				if($this->vehicle_service_model->InsertOrUpdateOrdelete_vts_vehicle_service($VehicleServiceId,null,"delete"))
				{
					$GLOBALS['outcome_with_db_check']="Vehicle Service deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_SERVICE,
								"Vehicle Service is deleted: Vehicle Service Id=".$VehicleServiceId);
				} else
				{
					$GLOBALS ['outcome_with_db_check'] = trim ( $row ['vh_ser_id'] ) . " Can't able to delete(Referred some where else)";
				}
					
			}catch (Exception $ex){
				$this->table_pagination($id);
	
			}
		}
		$this->table_pagination($id);
	}
	

	/*
	 *this function is used to fill the drop down list box in
	 * view depending upon the vehicle group selection without refresh the page.
	 */
	public function get_vehicle_gp($vhGp)
	{
		$vehicle=$this->vehicle_service_model->get_allVehicles($vhGp);
		/* foreach ($vehicle as $row){
				//log_message('error','**'.$row['vehicle_id'].','.$row['vehicle_regnumber']);
		} */
		$output="";
		if($vehicle!=null)
		{
			$output = "[";
			foreach ($vehicle as $row)
			{
				if ($output != "[") {$output .= ",";}
				$output .= '{"vehicle_id":"'.$row['vehicle_id'].'",';
				$output .= '"vehicle_regnumber":"'.$row['vehicle_regnumber'].'"}';
			}
			$output .="]";
		}
		echo($output);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id=null,$pageNo=0)
	{
		if($cl_id!=null)
		{
			$GLOBALS['vcle_srvc_client_id']=$cl_id;
		}
		$GLOBALS['vcle_srvc_client_id']=$cl_id;
		$GLOBALS['ID'] = $this->session->userdata('login');
		
		$config['base_url'] = base_url("index.php/vehicle_service_ctrl/table_pagination/".$GLOBALS['vcle_srvc_client_id']);
		$config['uri_segment'] = URI_SEGMENT_FOR_FOUR;
		$GLOBALS['clientList']=$this->vehicle_service_model->get_allClients();
		$GLOBALS['vehicleList']=$this->vehicle_service_model->get_allVehicles($GLOBALS['vcle_srvc_vclegroup_id']);
		$GLOBALS['vehicleGroupList']=$this->vehicle_service_model->get_allVehicleGroups($GLOBALS['vcle_srvc_client_id']);
		$GLOBALS['serviceList']=$this->vehicle_service_model->get_allServices();
		$GLOBALS['vehicleServiceList']=$this->vehicle_service_model->get_vts_vehicle_service(null,null,null,$GLOBALS['vcle_srvc_client_id']);
		
		$config['total_rows'] = count($GLOBALS['vehicleServiceList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['vehicleServiceList']=$this->vehicle_service_model->get_vts_vehicle_service(ROW_PER_PAGE,$pageNo,null,$GLOBALS['vcle_srvc_client_id']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	
	public function get_vhl_grp_vehicle($vhGrp)
	{
		$vhl_grp_vehicle=$this->vehicle_service_model->get_all_vhGp_vehicle($vhGrp);
		/* foreach ($vhl_grp_vehicle as $row){
			log_message('error','**'.$row['vehicle_id'].','.$row['vehicle_regnumber']);
		} */
		$output="";
		if($vhl_grp_vehicle!=null)
		{
			$output = "[";
			foreach ($vhl_grp_vehicle as $row)
			{
				if ($output != "[") {$output .= ",";}
				$output .= '{"vh_id":"'.$row['vehicle_id'].'",';
				$output .= '"vh_regnum":"'.$row['vehicle_regnumber'].'"}';
			}
			$output .="]";
		}
		echo($output);
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('vehicle_service_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
	
	
}

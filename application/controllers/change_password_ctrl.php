<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Change_password_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('change_password_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['outcome']=null;//jsut sent the dummy value to show result late
		$GLOBALS['user_name']=null;
		$GLOBALS['user_id']=null;
		$GLOBALS['user_type_id']=null;
		$GLOBALS['user_is_admin']=NOT_ACTIVE;	
		$GLOBALS['current_pwd']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['ID'] = $this->session->userdata('login');
		if($this->session->userdata('login')!=null)
		{
			$GLOBALS['user_type_id']=$GLOBALS['ID']['sess_user_type'];
			$GLOBALS['user_is_admin']=$GLOBALS['ID']['sess_is_admin'];
		}
		$GLOBALS['screen']="chngpwd";
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function load automatically when call this ctrl
	 */
	public function index()
	{
		$this->common_model->check_session();
		
		
		$GLOBALS['user_name']=$GLOBALS['ID']['sess_user_name'];
		if($GLOBALS['user_type_id'] == AUTOGRADE_USER && $GLOBALS['user_is_admin'] == ACTIVE)
		{
			$GLOBALS['showCurrentPwd']=false;
			$GLOBALS['readOnly']="";
		}
		else 
		{
			$GLOBALS['showCurrentPwd']=true;
			$GLOBALS['readOnly']="readonly";
		}
		
		$this->display();
	}
	/*
	 * This function will hide the Current password field in view
	 * @param
	 *  $id - user id
	 */
	public function view($id)
	{
		$data['result']=$this->change_password_model->get_user_detail(null,null,$id);
		if($data['result']!=null)
		{
			$GLOBALS['user_name']=$data['result']['user_email'];
			$GLOBALS['showCurrentPwd']=false;
			$GLOBALS['readOnly']="readonly";
			$this->display();
		}
		else
		{
			redirect('forget_password_ctrl');
		}
	}
	/*
	 * This function is used to validate all fields in the Change Password page
	 * which is access from Menu or Link(i.e. link in email).
	 * @param
	 *  $is_show - bool(true or false)
	 * Return type - void
	 */
	public function field_validation($is_show)
	{
		$ck_check=$is_show;
		$GLOBALS['showCurrentPwd']=($ck_check==md5('show'))?true:false;
		$GLOBALS['readOnly']=($GLOBALS['user_type_id'] == AUTOGRADE_USER && $GLOBALS['user_is_admin'] == ACTIVE)?"":"readonly";		
		$GLOBALS['user_name']=trim($this->input->post('username'));
		$this->form_validation->set_message('required', '%s required.');//this will help to the change the message to display 'required' form validation rule.
		$this->form_validation->set_message('matches', 'Its not match with New Password');//this will help to the change the message to display 'matches' form validation rule.
		$this->form_validation->set_rules('username', 'Username', 'callback_check_UserName'); //set rule for username field.
		if($GLOBALS['showCurrentPwd'])//if current password is visible in view, then do the validation. else just skip it.
		{
			$GLOBALS['current_pwd']=(null!=($this->input->post('CurrentPassword'))?trim($this->input->post('CurrentPassword')):null);
			$this->form_validation->set_rules('CurrentPassword', 'Current Password', 'callback_check_Current_password');//set rule for CurrentPassword field.			
		}
		$this->form_validation->set_rules('NewPassword', 'New Password', 'callback_check_password');//set rule for NewPassword field.
		$this->form_validation->set_rules('ConfirmPassword', 'Confirm Password', 'required|trim');//set rule for ConfirmPassword field.
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the New password in users table.
		{
			$this->display();
		}
		else
		{
			$data = array(
					'user_name' => trim($this->input->post('username')),
					'new_password' => md5(trim($this->input->post('NewPassword'))),
					'confirm_password' =>md5(trim($this->input->post('ConfirmPassword'))),
					'result'=>null,
					'outcome'=>null,
					'showCurrentPwd'=>($ck_check==md5('show'))?true:false
			);
			$data['result']=$this->change_password_model->get_user_detail($data['user_name']);
			$GLOBALS['showCurrentPwd']=($ck_check==md5('show'))?true:false;
			if($data['result']!=null)
			{
				$data['outcome']='<div style="color: green;">Update Succeed.</div>';
				$this->change_password_model->update_pwd($data['result']['user_id'],$data['new_password']);
				if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
					$this->common_model->insert_event_value("1", $GLOBALS['ip'],CHANGE_PASSWORD, "Change password: User ID=".trim($data['result']['user_id']).", User Name=".trim($data['result']['user_email']).", New Password=".trim($data['new_password'])); //insert the change password log in Eventlog table for future reference.
				if(!$GLOBALS['showCurrentPwd'])
				{
					$GLOBALS['title']="Thank you.";
					$GLOBALS['outcome']="Your password reset successfully for(".$GLOBALS['user_name'].").";
					$GLOBALS['isErrorMsg']=NOT_ACTIVE;
					$GLOBALS['ctrlName']="login_ctrl";
					$GLOBALS['linkText']="Go to login page.";
					$this->messagePage();
				}
				else
				{
					$GLOBALS['title']="Thank you.";
					$GLOBALS['outcome']='<div style="color: green;">Your password reset successfully for ('.$GLOBALS['user_name'].').</div>';
					$GLOBALS['isErrorMsg']=NOT_ACTIVE;
					$this->messagePage();
				}
			}
			else
			{
				$data['outcome']='<div style="color: red;">Please check the User Name.</div>';
				$this->display();
			}
		}
	}
	/*
	 * This function is call_back_ function. It used to validate the user email.
	 * It will return TRUE if it have correct email format, else FALSE.
	 *@param
	 * $email - is an argument which hold the usename field value.
	 *Return type - bool
	 */
	public function check_UserName($email)
	{
		if (empty($email)) //Check whether the user name field is empty
		{
			$this->form_validation->set_message('check_UserName', '%s required.');
			return FALSE;
		}
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['user_name']=null;
			$this->form_validation->set_message('check_UserName', 'Invalid email format.');
			return FALSE;
		}
		return true;
	}
	/*
	 * This function is call_back_ function. It used to validate the New password.
	 * It will return TRUE if it have correct password format what we framed, else FALSE.
	 * @param
	 *  $pwd - is an argument which hold the new password field value.
	 * Return type - bool
	 */
	public function check_password($pwd)
	{
		$pwdLen=$this->change_password_model->getMinPwdLength(); // it get minimum password length through getMinPwdLength() function in change_password_model model.
		$pwdLen=trim($pwdLen);
		//$pwdLen = ($pwdLen!=0) ? $pwdLen : 3;
		if (empty($pwd)) //Check whether the New password field is empty
		{
			$this->form_validation->set_message('check_password', '%s required.');
			return FALSE;
		}
		else if(strlen($pwd)<$pwdLen)//Check whether the New password length is less than minimum password length described in the Setting table.
		{
			$this->form_validation->set_message('check_password', 'Minimum '.$pwdLen.' char required.');
			return FALSE;
		}
		else if($pwd==trim($this->input->post('CurrentPassword')))//Check whether the New password and Current password is same.
		{
			$this->form_validation->set_message('check_password', 'It should not current password.');
			return FALSE;
		}
		else if(!preg_match('/[#$%^&@*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $pwd))//Check whether the New password have special character(s).
		{
			$this->form_validation->set_message('check_password', 'It should special character & alpha-numeric.');
			return FALSE;
		}
		else if(!preg_match('/[0-9]/', $pwd))//Check whether the New password have numeric value.
		{
			$this->form_validation->set_message('check_password', 'It should special character & alpha-numeric');
			return FALSE;
		}
		else if($pwd!=trim($this->input->post('ConfirmPassword')))//Check whether the New password and Confirm password is same.
		{
			$this->form_validation->set_message('check_password', 'Its not match with confirm password.');
			return FALSE;
		}
		else
		{
			return true;
		}
	}
	/*
	 *This function is call_back_ function. It used to validate the Current password.
	 * It will return TRUE if it not empty and user & current password is correct, else FALSE.
	 *@param
	 * $pwd - is an argument which hold the Current password field value.
	 * Return type - bool
	 */
	public function check_Current_password($pwd)
	{
		if (empty($pwd))//Check whether the Current password field is empty
		{
			$this->form_validation->set_message('check_Current_password', '%s required.');
			$GLOBALS['current_pwd']=null;
			return FALSE;
		}
		else
		{
			$data = array(
					'user_name' => trim($this->input->post('username')),
					'current_password' => md5(trim($this->input->post('CurrentPassword'))),
					'result'=>null
			); //Store the UserName and MD5 encrypt Current Password in $data array.
				$data['result']=$this->change_password_model->get_user_detail($data['user_name'],$data['current_password']);//check the username in user table and store the resutl in $data array on index 'result'.
			if($data['result']==null) //if return result is null then return FALSE, else TRUE
			{
				$this->form_validation->set_message('check_Current_password', "Check Email & Password.");
				$GLOBALS['current_pwd']=null;
				return FALSE;
			}
			else
				return  true;
		}
	}
	/*
	 * function calling from autocomplete function of view
	 * to get values to item field
	 */
	public function  get_user_list_name()
	{		
		if (isset($_GET['userName']))
		{			
			$userNameStartWith = strtolower($_GET['userName']);
			$query= $this->change_password_model->get_all_user($userNameStartWith);
			$row_set[]=array();
			foreach ($query as $row)
			{
				$new_array['value'] = htmlentities(stripslashes($row['user_email']));
				$new_array['id'] = htmlentities(stripslashes($row['user_id'])); //build an array
				$row_set[] =  $new_array;
			}
			echo json_encode($row_set);//format the array into json data
		}		 
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if ($this->session->userdata ( 'login' ))
		{
			$this->common_model->check_session();
			$this->common_model->menu_display();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'change_password_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
		else
		{
			$this->load->view ( 'header_footer/header1',$GLOBALS );
			$this->load->view ( 'change_password_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
	}
	/*
	 * This function is used to render the message screen view
	 */
	private function messagePage()
	{
		if ($this->session->userdata ( 'login' ))
		{
			$client_time_zone = $this->common_model->get_location ();
			$GLOBALS ['timezone'] = ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			$this->common_model->menu_display();
			$GLOBALS['linkText']="Go to home page.";			
			if($GLOBALS['user_type_id'] == AUTOGRADE_USER || $GLOBALS['user_type_id'] == OHTER_CLIENT_USER)
				$GLOBALS['ctrlName']="home_ctrl";
			else if($GLOBALS['user_type_id'] == INDIVIDUAL_CLIENT)
				$GLOBALS['ctrlName']="individual_vt_ctrl";
			else if($GLOBALS['user_type_id']==DISTRIBUTOR_USER)
				$GLOBALS['ctrlName']="distributor_dashboard_ctrl";
			else if($GLOBALS['user_type_id']==DEALER_USER)
				$GLOBALS['ctrlName']="dealer_dashboard_ctrl";
			else if($GLOBALS['user_type_id']==PARENT_USER)
				$GLOBALS['ctrlName']="individual_vt_ctrl";			
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'message_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
		else
		{
			$GLOBALS['linkText']="Go to login page.";
			$GLOBALS['ctrlName']="login_ctrl";
			$this->load->view ( 'header_footer/header1',$GLOBALS );
			$this->load->view ( 'message_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
	}
}
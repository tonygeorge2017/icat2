<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('report_model');
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['vehicle_id']=null;
		$GLOBALS['vehicle_name']=null;
// 		$GLOBALS['device_imei_no']=null;
// 		$GLOBALS['driver_name']=null;
// 		$GLOBALS['device_location']=null;			
		$GLOBALS['gps_result']=null;
		$GLOBALS['from_date']=null;
		$GLOBALS['to_date']=null;		
	}
	
	public function index()
	{
			
	}
	
	/*
	 * This function is used to validate and add the new user in database.
	 */
	public function report_generation($vh_id,$_fdate,$_tdate)
	{			
		$GLOBALS['vehicle_name']=$this->report_model->get_vehicle_name($vh_id);
		$fdate = new DateTime($_fdate); 
		$tdate = new DateTime($_tdate);
		$GLOBALS['from_date']=$fdate->format('Y-m-d H:i:s');
		$GLOBALS['to_date']=$tdate->format('Y-m-d H:i:s');
		$fdate=$this->common_model->get_dateTime($fdate->format('Y-m-d H:i:s'),'1');//this is used to change to user date time
		$tdate=$this->common_model->get_dateTime($tdate->format('Y-m-d H:i:s'),'1');		
		$GLOBALS['gps_result']=$this->report_model->get_gps_data($vh_id,$fdate,$tdate);
		$this->display();
	}	

		
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		if($GLOBALS['sessionData']!=null)
		{
			$this->common_model->menu_display();
			$this->load->view('header_footer/header',$GLOBALS);
			$this->load->view('report_view',$GLOBALS);
			$this->load->view('header_footer/footer');
		}else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
}
<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Vehicle_report_ctrl extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct ();
		$this->load->library ( 'pagination' );
		$this->load->library ( 'JpGraph/Graph' );
		$this->load->library ( 'session' );
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model ( 'vehicle_report_model' );
		
		$this->load->helper ( array (
				'form',
				'url'
		) );
		
		$GLOBALS ['outcome'] = null;
		$GLOBALS ['outcome_with_db_check'] = null;
		
		$GLOBALS ['eventLogRequired'] = $this->common_model->get_setting_value ( "ActivityLoggingRequired" ); // whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS ['no_data'] = false;
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$session_userid = $GLOBALS['ID']['sess_userid'];
		
		$GLOBALS ['vcle_rept_client_id'] = null;
		$GLOBALS ['vcle_rept_from_date'] = null;
		$GLOBALS ['vcle_rept_to_date'] = null;
		$GLOBALS ['vcle_rept_vclegroup_id'] = null;
		$GLOBALS ['vcle_rept_vehicle_id'] = null;
		$GLOBALS ['query'] = null;
		$GLOBALS ['clientList'] = $this->vehicle_report_model->get_allClients ();
		$GLOBALS['vcle_rept_data_fmdb'] = $this->vehicle_report_model->get_allVehicleReports();
		$GLOBALS ['get_gridDetails'] = array ();
		
		$GLOBALS['csvpdf_button'] = null;
		
		// to get user ip and host name
		$host_name = exec ( "hostname" ); // to get "hostname"
		$host_name = trim ( $host_name ); // remove any spaces before and after
		$ip = gethostbyname ( $host_name );
		$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
	}
	
	/*
	 * Function to display 
	 * the view 
	 */
	public function index() {
		$this->table_pagination ( md5 ( $GLOBALS ['vcle_rept_client_id'] ) );
		// $this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_vehicle in database.
	 */
	public function vts_vehicle_report_validation($cnt_id = null) {
		$GLOBALS ['temp'] = (null != ($this->input->post ( 'temp' )) ? $this->input->post ( 'temp' ) : null);
		
		if ($GLOBALS ['temp'] != "-1") {
			$this->table_pagination ( md5 ( $GLOBALS ['temp'] ) );
		} else if ($cnt_id == null) {
			$GLOBALS ['vcle_rept_client_id'] = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
			$GLOBALS ['vcle_rept_from_date'] = (null != trim ( $this->input->post ( 'VehicleReportFromDate' ) ) ? trim ( $this->input->post ( 'VehicleReportFromDate' ) ) : null);
			$GLOBALS ['vcle_rept_to_date'] = (null != trim ( $this->input->post ( 'VehicleReportToDate' ) ) ? trim ( $this->input->post ( 'VehicleReportToDate' ) ) : null);
			$GLOBALS ['vcle_rept_vclegroup_id'] = (null != trim ( $this->input->post ( 'VehicleGroup' ) ) ? trim ( $this->input->post ( 'VehicleGroup' ) ) : null);
			$GLOBALS ['vcle_rept_vehicle_id'] = (null != $this->input->post ( 'Vehicle' ) ? $this->input->post ( 'Vehicle' ) : null);
			$vcl_arr =array();
			$vcl_arr = $GLOBALS ['vcle_rept_vehicle_id'];
			$vcl_cnt = count($GLOBALS ['vcle_rept_vehicle_id']);
			$GLOBALS['vh_gp_name'] = (null != trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) ? trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) : null);
			$GLOBALS['vh_name'] = (null != trim ( $this->input->post ( 'hiddenVehicle' ) ) ? trim ( $this->input->post ( 'hiddenVehicle' ) ) : null);
				
			$get_values_array = array('from_date'=>$GLOBALS ['vcle_rept_from_date'], 'to_date'=>$GLOBALS ['vcle_rept_to_date'], 'vehicle_gp_id'=>$GLOBALS ['vcle_rept_vclegroup_id'], 'vehicle_id'=>$GLOBALS ['vcle_rept_vehicle_id']);
			
			
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
				$GLOBALS ['vcle_rept_client_id'] = (null != ($this->input->post ( 'ClientName' )) ? $this->input->post ( 'ClientName' ) : null);
			} else
				$GLOBALS ['vcle_rept_client_id'] = $GLOBALS ['ID'] ['sess_clientid'];
			
			$GLOBALS ['temp'] = $GLOBALS ['vcle_rept_client_id'];
			
			$this->form_validation->set_message ( 'required', '%s required' ); // this will help to the change the message to display 'required' form validation rule.
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER)
			{
				$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_cnt_id');
			}
			$this->form_validation->set_rules('VehicleReportFromDate', 'From Date', 'callback_check_rept_from_dt');
			$this->form_validation->set_rules('VehicleReportToDate', 'To Date', 'callback_check_rept_to_dt');
			$this->form_validation->set_rules('VehicleGroup', 'Vehicle Group', 'callback_check_vcle_group');
			$this->form_validation->set_rules('Vehicle', 'Vehicle', 'callback_check_vcle');
			
			if ($this->form_validation->run () == FALSE)
			{ // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
				if($vcl_cnt > 0)
				{
					$i=0;
					foreach ($vcl_arr as $_row)
					{
						$GLOBALS['vcle_rept_vehicle_id'][$i]=array('vh_link_vehicle_id'=>$_row);
						$i++;
					}
				}
			}
			else 
			{
				$GLOBALS ['get_gridDetails'] = $this->vehicle_report_model->get_values ($GLOBALS ['temp'], $get_values_array);
				if($vcl_cnt > 0)//!empty($row)
				{
					$i=0;
					foreach ($vcl_arr as $_row)
					{
						$GLOBALS['vcle_rept_vehicle_id'][$i]=array('vh_link_vehicle_id'=>$_row);
						$i++;
					}
				}
				$GLOBALS ['no_data'] = true;
				$arr1 = array();
				$i = 0;
				
				$diff=$GLOBALS['ID']['sess_time_zonediff'];

				$new_array = $GLOBALS ['get_gridDetails'];
				
				$this->vehicle_report_model->InsertOrdelete_vts_tmp_report_vehicledetails($new_array,'delete');
				$this->vehicle_report_model->InsertOrdelete_vts_tmp_report_vehicledetails($new_array,"insert");
				
				$report_title = "Vehicle Report";
				if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER)
				{
					$client_name = $this->common_model->get_dropdownCntName($GLOBALS ['vcle_rept_client_id']);
					$report_title .= " ( ".$client_name." )";
				}
				
				$report_sub_title = "For the period from ".date('Y-m-d', strtotime($GLOBALS ['vcle_rept_from_date'])) . " to " . date('Y-m-d', strtotime($GLOBALS ['vcle_rept_to_date']))." ( Vehicle Group Name = ".$GLOBALS['vh_gp_name'];
				
				$vcle_name = array();
				if($vcl_cnt > 0)
				{
					$i = 0;
					$report_sub_title .= ", Vehicle Name = ";
					$report_sub_title1 = "";
					foreach($GLOBALS ['vcle_rept_vehicle_id'] as $row)
					{
						$v_id = $row['vh_link_vehicle_id'];
						$name = $this->vehicle_report_model->getVehicleName($v_id);
						array_push($vcle_name, $name);

						if($vcl_cnt == 1)
						{
							$report_sub_title1 .= $vcle_name[$i];
						}
						else
						{
							if($i == $vcl_cnt-1){
								$report_sub_title1 .= $vcle_name[$i];
							}else{
								$report_sub_title1 .= $vcle_name[$i].", ";
							}
						}
						$i++;
					}
					$report_sub_title .= "( ".$report_sub_title1.")";
				}
				$vehicle_name_list = "Vehicle(s) : ( ".$report_sub_title1." )";
				$report_sub_title .= " )";
				
				$this->session->set_userdata ( 'SESSION_DATA_1', $report_title );
				$this->session->set_userdata ( 'SESSION_DATA_2', $report_sub_title );
				
				$form_data_array = array("client_id"=>$GLOBALS ['vcle_rept_client_id'],"from_date"=>$GLOBALS ['vcle_rept_from_date'],"to_date"=>$GLOBALS ['vcle_rept_to_date'],"vehicle_group"=>$GLOBALS ['vcle_rept_vclegroup_id'],"vehicle"=>$GLOBALS ['vcle_rept_vehicle_id']);
				$this->session->set_userdata ( 'SESSION_DATA_arr', $form_data_array );
				$this->session->set_userdata ( 'SESSION_GRAPH_VEHICLE_LIST', $vehicle_name_list );
				
// 				$distanceArray1 = $this->vehicle_report_model->get_allDataToPlot ( $form_data_array );
				
// 					$totalDistance = null;
// 					foreach ($distanceArray1 as $distArr1)
// 					{
// 						$totalDistance += $distArr1['distance'];
// 					}
				$distanceArray1=$GLOBALS ['get_gridDetails'];
				$totalDistance = 0;
				foreach ($distanceArray1 as $distArr1)
				{
					$totalDistance += $distArr1['distance'];
				}
				$this->session->set_userdata ( 'SESSION_TOTAL_DISTANCE', round($totalDistance, 2) );
				
			}
			$this->table_pagination ( md5 ( $GLOBALS ['temp'] ) );
		}
		else
			{
				$GLOBALS['csvpdf_button'] = (null != trim ( $this->input->post ( 'clicked_btn' ) ) ? trim ( $this->input->post ( 'clicked_btn' ) ) : null);
				
				$title = $this->session->userdata ('SESSION_DATA_1');
				$sub_title = $this->session->userdata ('SESSION_DATA_2');
				
				$headers = array( array(
						"vehiclename" => "Vehicle",
						"drivername" => "Driver",
						"from_datetime" => "From Date/Time",
						"to_datetime" => "To Date/Time",
						"duration" => "Duration",
						"status" => "Status",
						"distance" => "Distance"
				 ));
				$GLOBALS['vcle_rept_data_fmdb'] = $this->vehicle_report_model->get_allVehicleReports();
				$i=0;
				foreach ($GLOBALS['vcle_rept_data_fmdb'] as $row)
				{
					$headers[$i+1]["vehiclename"]= $row["vehiclename"];
					$headers[$i+1]["drivername"]= $row["drivername"];
					$headers[$i+1]["from_datetime"]= $row["from_datetime"];
					$headers[$i+1]["to_datetime"]= $row["to_datetime"];
					$headers[$i+1]["duration"]= $row["duration"];
					$headers[$i+1]["status"]= $row["status"];
					$headers[$i+1]["distance"]= $row["distance"];
					$i++;
				}
				
				if($GLOBALS['csvpdf_button'] == 'csv')
				{
					$this->load->library ( 'commonfunction' );
					$this->commonfunction->csv_create ( $title, $sub_title, $headers);
				}
				else if($GLOBALS['csvpdf_button'] == 'pdf')
				{
					$this->load->helper ( array (
							'dompdf',
							'file'
					) );
					
					$session_data = $this->session->userdata ('login');
					$title = $this->session->userdata ( 'SESSION_DATA_1' );
					$sub_title = $this->session->userdata ( 'SESSION_DATA_2' );
					
					// starts from here----
					$html = " <html>
					<head></head>
					<body>
					<table>
					<tr><td>
					<table>
					<tr><td><b>$title</td></td></tr>
					<tr><td>$sub_title</td><td></td></tr>
					</table>
					</td><br/>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>
					<table>
					<tr>
					<td> <img alt='' src='". base_url('assets/images/logo.png')."'/></td>
					</tr>
					</table>
					</td>
					</tr>
					</table>
					<hr>
					<table> ";
					$table_data="";
					$i = 0;
					foreach ( $headers as $header ) {
									if ($i == 1)
									{
//									$table_data .= "<tr width:1000%;><td><hr></td></tr>";
					
									}
									else{
										//log_message("debug", "Inside else...");
					}
					
					$table_data .= " <tr><td>" . $header ['vehiclename'] . "</td><td>"
					. $header ['drivername'] . "</td><td>" . $header ['from_datetime'] . "</td><td>"
													. $header ['to_datetime'] . "</td><td>" . $header['duration'] . "</td><td>" . $header['status'] . "</td><td>"
																		. $header ['distance'] . "</td></tr>";
																				$i++;
					
					}
					$html_1 = "</table>
							<hr>
										<table>&nbsp;&nbsp;&nbsp;&nbsp;
											<tr>
												<td><b>Prepared by </b> : ".$session_data ['sess_user_name']."</td>
												<td></td>
												<td style='padding-left:390px;'><b>".date ( 'Y-m-d' )."</b></td>
											</tr>
										</table>
							</body>
							</html>";
			
					$html = $html.$table_data.$html_1;
					
				// ends here-----------
					
				pdf_create ( $html, str_replace(' ', '_', $title) . '_' . date ( 'Ymd' ) );
			 }
			}
	}
	
	
	
	public function vts_vehicle_report_graph()
	{
			$form_data = $this->session->userdata('SESSION_DATA_arr');
			$vehicle_name = $this->session->userdata('SESSION_GRAPH_VEHICLE_LIST');
			
			$distanceArray = $this->vehicle_report_model->get_valuesToPlot ( $form_data );
			$subTitle = $vehicle_name."\nFor the duration of ".date('Y-m-d', strtotime($form_data['from_date']))." to ".date('Y-m-d', strtotime($form_data['to_date']));
			
			
			$distanceArray[0]['title'] = "Total Distance Travelled";
			$distanceArray[0]['sub_title'] = $subTitle;
			$distanceArray[0]['xaxis_title'] = "Date";
			$distanceArray[0]['yaxis_title'] = "Total Distance in KMS";
			
			$this->load->library ( 'commonfunction' );
			if(count($distanceArray)>0 & isset($distanceArray[0]['date']))
			{
				$GLOBALS['graph'] = $this->commonfunction->draw_bar_graph_date ( $distanceArray);
			}
			else
			{
				$GLOBALS['graph'] = "<div style='color: red;font-size: 24px;margin-top: 60px;margin-left:37%'>Data Not found</div>";
			} 
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			
			$this->load->view( 'vehicle_report_run_data_view', $GLOBALS);
			$this->load->view ( 'header_footer/footer' );
	}
	
	/*
	 * This function is to validate the client dropdown this is only for the autograde users
	 */
	public function check_cnt_id($cnt_id)
	{
		if (empty ( $cnt_id )) {
			$this->form_validation->set_message ( 'check_cnt_id', '%s required' );
			return FALSE;
		}
		$GLOBALS ['vcle_rept_client_id'] = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
		return true;
	}
	
	/*
	 * This function is to validate the vehicle Report from date
	 */
	public function check_rept_from_dt($date)
	{
		if (empty ( $date )) 
		{
			$this->form_validation->set_message ( 'check_rept_from_dt', '%s required' );
			return FALSE;
		}
		$GLOBALS ['vcle_rept_from_date'] = (null != trim ( $this->input->post ( 'VehicleReportFromDate' ) ) ? trim ( $this->input->post ( 'VehicleReportFromDate' ) ) : null);
		return true;
	}
	
	/*
	 * This function is to validate the Vehicle Report to date
	 */
	public function check_rept_to_dt($date)
	{
		if (empty ( $date ))
		{
			$this->form_validation->set_message ( 'check_rept_to_dt', '%s required' );
			return FALSE;
		}
		$GLOBALS ['vcle_rept_to_date'] = (null != trim ( $this->input->post ( 'VehicleReportToDate' ) ) ? trim ( $this->input->post ( 'VehicleReportToDate' ) ) : null);
		return true;
	}

	/*
	 * this function is to validate vehicle group dropdown
	 */
	public function check_vcle_group($vcle_grp)
	{
		$GLOBALS['vcle_rept_vclegroup_id']=null;
		if(empty($vcle_grp))
		{
			$this->form_validation->set_message('check_vcle_group', "%s required");
			return FALSE;
		}
		$GLOBALS['vcle_rept_vclegroup_id']=(null !=trim($this->input->post('VehicleGroup')) ? trim($this->input->post('VehicleGroup')) : null);
		return true;
	}

	/*
	 *this function is to validate vehicle dropdown 
	 */
	public function check_vcle($vcle)
	{
		$GLOBALS['vcle_rept_vehicle_id']=null;
		if($GLOBALS['vcle_rept_vclegroup_id']!=null)//do the following validation only if vehicle group is selected.
		{
			if(empty($vcle))
			{
				$this->form_validation->set_message('check_vh_gp', 'Please select at least one vehicle to view the vehicle report');
				$GLOBALS['outcome_with_db_check']='Please select at least one vehicle to view the vehicle report';
				return FALSE;
			}
		}
		return true;
	}
	
	/*
	 * this function is used to fill the drop down list box in
	 * view depending upon the vehicle group selection without refresh the page.
	 */
	public function get_vehicle_gp($vhGp) {
		$vehicle = $this->vehicle_report_model->get_allVehicles ( $vhGp );
		foreach ( $vehicle as $row ) {
		}
		$output = "";
		if ($vehicle != null) {
			$output = "[";
			foreach ( $vehicle as $row ) {
				if ($output != "[") {
					$output .= ",";
				}
				$output .= '{"vehicle_id":"' . $row ['vehicle_id'] . '",';
				$output .= '"vehicle_regnumber":"' . $row ['vehicle_regnumber'] . '"}';
			}
			$output .= "]";
		}
		echo ($output);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id = null) {
		if ($cl_id != null) {
			$GLOBALS ['vcle_rept_client_id'] = $cl_id;
		}
		$cntId = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
		if($cntId == null)
			$cntId = $GLOBALS ['ID'] ['sess_clientid'];
		
		$GLOBALS ['clientList'] = $this->vehicle_report_model->get_allClients ();
		$GLOBALS ['vehicleGroupList'] = $this->vehicle_report_model->get_allVehicleGroups ( $cntId );
		$GLOBALS ['vehicleList'] = $this->vehicle_report_model->get_allVehicles ( $GLOBALS ['vcle_rept_vclegroup_id'] );
		$GLOBALS['vcle_rept_data_fmdb'] = $this->vehicle_report_model->get_allVehicleReports();

		$this->display ();
	}

	/*
	 * This function is used to render the view
	 */
	private function display() {
		$this->common_model->menu_display ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'vehicle_report_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}

}

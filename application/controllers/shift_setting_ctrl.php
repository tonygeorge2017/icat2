<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shift_setting_ctrl extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->helper( array('form', 'url') );
		$this->load->model('common_model');
		$this->common_model->check_session();
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");
		$this->load->model('shift_setting_model');
		$GLOBALS['ID'] = $this->session->userdata('login');
		$session_userid = $GLOBALS['ID']['sess_userid'];
		$GLOBALS['sess_clientid']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sess_user_type']=$GLOBALS['ID']['sess_user_type'];		
		if($GLOBALS['sess_user_type']==AUTOGRADE_USER)
			$GLOBALS['clientID']=null;
		else
			$GLOBALS['clientID']=$GLOBALS['sess_clientid'];
		$GLOBALS['categoryID']=null;
		$GLOBALS['categoryName']=null;
		$GLOBALS['noOfShift']=1;
		$GLOBALS['wkHr']=1;
		$GLOBALS['wkMnt']=0;
		$GLOBALS['description']=null;
		$GLOBALS['isactive']=NOT_ACTIVE;
		$GLOBALS['categoryList']=array();
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";		
	}
	public function index()
	{
		//log_message('debug','client Id: '.$GLOBALS['clientID']);
		if($GLOBALS['clientID'])
			$this->table_pagination(md5($GLOBALS['clientID']));
		else
			$this->table_pagination();
	}
	public function client_change()
	{
		$GLOBALS['clientID']=$this->input->post('clientChangeID');
		log_message('debug','client Id: '.$GLOBALS['clientID']);
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	public function input_validation()
	{
		$GLOBALS['clientID']=(null!=trim($this->input->post('client'))?trim($this->input->post('client')):null);
		$GLOBALS['categoryID']=(null!=trim($this->input->post('categoryID'))?trim($this->input->post('categoryID')):null);
		$GLOBALS['categoryName']=(null!=trim($this->input->post('categoryName'))?trim($this->input->post('categoryName')):null);
		$GLOBALS['noOfShift']=(null!=trim($this->input->post('noOfShift'))?trim($this->input->post('noOfShift')):1);
		$GLOBALS['wkHr']=(null!=trim($this->input->post('wkHr'))?trim($this->input->post('wkHr')):1);
		$GLOBALS['wkMnt']=(null!=trim($this->input->post('wkMnt'))?trim($this->input->post('wkMnt')):1);
		$GLOBALS['description']=(null!=trim($this->input->post('description'))?trim($this->input->post('description')):null);
		$GLOBALS['shiftNameList']=(null!=$this->input->post('shift')?$this->input->post('shift'):array());
		$GLOBALS['shiftHrsList']=(null!=$this->input->post('SHrs')?$this->input->post('SHrs'):array());
		$GLOBALS['shiftMntList']=(null!=$this->input->post('SMint')?$this->input->post('SMint'):array());
		$GLOBALS['endTime']=(null!=$this->input->post('end')?$this->input->post('end'):array());
		$GLOBALS['isactive']=(null!=trim($this->input->post('isactive'))?trim($this->input->post('isactive')):NOT_ACTIVE);		
		$data['client_id']=$GLOBALS['clientID'];
		$data['shift_category_name']=$GLOBALS['categoryName'];
		$data['no_of_shift']=$GLOBALS['noOfShift'];
		$data['working_hrs']=$GLOBALS['wkHr'].':'.$GLOBALS['wkMnt'].':00';
		$data['description']=$GLOBALS['description'];
		$data['isactive']=$GLOBALS['isactive'];
		if($GLOBALS['categoryID']==null)//insert
		{
			//log_message('debug',print_r($GLOBALS['shiftNameList'],true));
			$GLOBALS['categoryID']=$this->shift_setting_model->insert_data($data,$GLOBALS['shiftNameList'],$GLOBALS['shiftHrsList'],$GLOBALS['shiftMntList'],$GLOBALS['endTime']);
		}else{ //edit
			$data['shift_category_id']=$GLOBALS['categoryID'];
		}
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	public function delete($cl_id,$id)
	{
		$this->shift_setting_model->delete_shift_detail($id);
		$this->shift_setting_model->delete_shift_category($id);
		$this->table_pagination($cl_id);
	}
	public function get_client()
	{		
		$result=json_encode($this->shift_setting_model->get_allClient());
		//log_message('debug',$result);
		echo $result;
	}
	public function table_pagination($cl_id=null)
	{
		$GLOBALS['categoryList']=$this->shift_setting_model->get_shiftCategorylist($cl_id);
		$this->display();
	}
	
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('shift_setting_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
} 
?>
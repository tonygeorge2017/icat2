<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Device_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_model');
		$this->load->helper(array('form', 'url'));
		
		//the below data will save to the vts_device table
		$GLOBALS['dvice_id']=null;
		$GLOBALS['dvice_device_type_id']=null;
		$GLOBALS['dvice_imei']=null;
		$GLOBALS['dvice_prepare_date']=null;
		$GLOBALS['dvice_distributor_id']=null;
		$GLOBALS['dvice_release_distributor_date']=null;
		$GLOBALS['dvice_remarks']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['dvice_dealer_id']=null;
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['dvice_slno']=null;

		$GLOBALS['DeviceList']=$this->device_model->get_vts_device();
		$GLOBALS['ID'] = $this->session->userdata('login');
		//echo print_r($GLOBALS['ID'],true);
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
		
	}

	public function index()
	{
		if($GLOBALS['ID']['sess_clientid']==AUTOGRADE_USER)
		{
			$this->table_pagination(); 
		//$this->display();
		}
		else{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect("login_ctrl");
		}
	}
	
	/*
	 * This function is used to validate and add the new vts_device in database.
	 */
	public function vts_device_validation()
	{
		$GLOBALS['dvice_id']=(null!=($this->input->post('DeviceId'))?$this->input->post('DeviceId'):null);
		$GLOBALS['dvice_device_type_id']=(null!=trim($this->input->post('DeviceTypeId'))?trim($this->input->post('DeviceTypeId')):null);
		$GLOBALS['dvice_imei']=(null!=trim($this->input->post('DeviceIMEI'))?trim($this->input->post('DeviceIMEI')):null);
		$GLOBALS['dvice_prepare_date']=(null!=trim($this->input->post('DevicePrepareDate'))?trim($this->input->post('DevicePrepareDate')):null);
		$GLOBALS['dvice_distributor_id']=(null!=trim($this->input->post('DeviceDistributorId'))?trim($this->input->post('DeviceDistributorId')):null);
		$GLOBALS['dvice_release_distributor_date']=(null!=trim($this->input->post('DeviceReleaseDate'))?trim($this->input->post('DeviceReleaseDate')):null);
		$GLOBALS['dvice_remarks']=(null!=trim($this->input->post('DeviceRemarks'))?trim($this->input->post('DeviceRemarks')):null);
		$GLOBALS['dvice_slno']=(null!=trim($this->input->post('DeviceSlno'))?trim($this->input->post('DeviceSlno')):null);
		$GLOBALS['active']=(null!=($this->input->post('DeviceIsActive'))?$this->input->post('DeviceIsActive'):NOT_ACTIVE);
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('DeviceTypeId','Device Type','callback_check_device_type');
		$this->form_validation->set_rules('DeviceIMEI', 'Device IMEI', 'callback_check_device_imei');
		$this->form_validation->set_rules('DevicePrepareDate', 'Prepared Date', 'callback_check_prepare_date');
		
		if(isset($GLOBALS['dvice_release_distributor_date']))
		{
			$this->form_validation->set_rules('DeviceDistributorId', 'Distributor name', 'callback_check_distributor_name');
			$this->form_validation->set_rules('DeviceReleaseDate', 'Release Date', 'callback_check_release_date');
			$this->form_validation->set_rules('DeviceSlno', 'Device Slno', 'callback_check_device_slno');
// 			if(empty($GLOBALS['dvice_prepare_date']))
// 			{
// 				$this->form_validation->set_rules('DevicePrepareDate', 'Prepare Date', 'callback_check_prepare_date');
// 				$this->form_validation->set_rules('DeviceReleaseDate', 'Release Date', 'callback_check_release_date');
// 			}
		}
		
		if(isset($GLOBALS['dvice_distributor_id']))
		{
			$this->form_validation->set_rules('DeviceReleaseDate', 'Release Date', 'callback_check_release_date');
		}

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
		{
			$this->table_pagination();
		}
		else
		{
			$data['device_device_type_id']=$GLOBALS['dvice_device_type_id'];
			$data['device_imei']=$GLOBALS['dvice_imei'];
			$data['device_prepare_date']=$GLOBALS['dvice_prepare_date'];
			$data['device_distributor_id']=$GLOBALS['dvice_distributor_id'];
			$data['device_release_distributor_date']=$GLOBALS['dvice_release_distributor_date'];
			$data['device_remarks']=$GLOBALS['dvice_remarks'];
			$data['device_is_active']=$GLOBALS['active'];
			$data['device_slno']=$GLOBALS['dvice_slno'];
			
			if($GLOBALS['dvice_id']!=null)//edit
			{
				$data['device_id']=$GLOBALS['dvice_id'];
				if ($this->device_model->InsertOrUpdateOrdelete_vts_device($GLOBALS['dvice_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEVICE,
								"Updated Device: Device ID=".trim($GLOBALS['dvice_id']).", Device IMEI Number=".trim($GLOBALS['dvice_imei']).",Device Prepare date=".trim($GLOBALS['dvice_prepare_date']).", Device Dealer Id=".trim($GLOBALS['dvice_distributor_id']).", Device Release Date=".trim($GLOBALS['dvice_release_distributor_date']).", Device Remarks=".trim($GLOBALS['dvice_remarks']).", Device Is Active=".trim($GLOBALS['active']).", Device Slno=".trim($GLOBALS['dvice_slno']));
	
						$GLOBALS['outcome']="Device ".'"'.trim($GLOBALS['dvice_imei']) .'" with "'.trim($GLOBALS['dvice_slno']).'"'." Serial number updated successfully";
						$GLOBALS['dvice_id']=null;
						$GLOBALS['dvice_device_type_id']=null;
						$GLOBALS['dvice_distributor_id']=null;
						$GLOBALS['active']=null;
						$GLOBALS['dvice_slno']=null;
				}
			}
			else	//insert
			{
				$GLOBALS['dvice_imei_list']=array();
				$GLOBALS['dvice_imei'] = trim($GLOBALS['dvice_imei'],",");
				$GLOBALS['dvice_imei_list']=explode ( ",", $GLOBALS['dvice_imei'] );
				
				$GLOBALS['dvice_slno_list']=array();
				$GLOBALS['dvice_slno'] = trim($GLOBALS['dvice_slno'],",");
				$GLOBALS['dvice_slno_list']=explode ( ",", $GLOBALS['dvice_slno'] );
				$i=0;
				foreach ($GLOBALS['dvice_imei_list'] as $imei_no)
				{
					$data['device_imei']=trim($imei_no);
					$data['device_slno']=trim($GLOBALS['dvice_slno_list'][$i]);
					if($this->device_model->InsertOrUpdateOrdelete_vts_device(null,$data,'insert'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEVICE,
									"Created New Device: Device IMEI Number=".trim($data['device_imei']).",Device Prepare date=".trim($GLOBALS['dvice_prepare_date']).", Device Dealer Id=".trim($GLOBALS['dvice_distributor_id']).", Device Release Date=".trim($GLOBALS['dvice_release_distributor_date']).", Device Remarks=".trim($GLOBALS['dvice_remarks']).", Device Is Active=".trim($GLOBALS['active']).", Device Slno=".trim($GLOBALS['dvice_slno']));
					}
					$i++;
				}
				if(count($GLOBALS['dvice_imei_list'])>1)
					$GLOBALS['outcome']="New List of Devices created successfully";
				else
					$GLOBALS['outcome']="New Device ".'"'.trim($data['device_imei']).'" with '.trim($data['device_slno'])." Serial number created successfully";
				$GLOBALS['dvice_id']=null;
				$GLOBALS['dvice_device_type_id']=null;
				$GLOBALS['dvice_distributor_id']=null;
				$GLOBALS['active']=null;
				$GLOBALS['dvice_slno']=null;
			}
			$this->table_pagination();
		}
	}
	
	
	/*
	 * This function is to validate the device slno
	 * Should uniqu and should allow only the numeric values
	 */
	public function check_device_slno($dvs_slno)
	{
		$temp['dvice_slno'] = trim($dvs_slno,",");
		$temp['dvice_imei'] = trim($GLOBALS['dvice_imei'],",");
		if (empty($temp['dvice_slno'])) //Check whether the Device Slno field is empty
		{
			$GLOBALS['dvice_slno']=null;
			$this->form_validation->set_message('check_device_slno', '%s required');
			return FALSE;
		}
		else
		{
			$temp['dvice_slno_list']=array();
			$temp['dvice_slno_list']=explode ( ",", $temp['dvice_slno'] );
			$temp['dvice_imei_list']=array();
			$temp['dvice_imei_list']=explode ( ",", $temp['dvice_imei'] );
			
			//log_message("debug", "slnor countt---".print_r($temp['dvice_slno_list'], true)." --- imei count".print_r($temp['dvice_imei_list'], true));
			if(count($temp['dvice_slno_list']) != count($temp['dvice_imei_list']))
			{
				$GLOBALS['dvice_slno']=null;
				$this->form_validation->set_message('check_device_slno', 'Number of IMEI & Serial numbers should be same');
				return FALSE;
			}
			foreach ($temp['dvice_slno_list'] as $slno)
			{
				$dvs_slno=trim($slno);
				if (empty($dvs_slno)) //Check whether the Device Slno field is empty
				{
					$GLOBALS['dvice_slno']=null;
					$this->form_validation->set_message('check_device_slno', '%s required');
					return FALSE;
				}
				else if (!is_numeric($dvs_slno)) // Check whether the given Device Slno is numeric.
				{
					$GLOBALS['dvice_slno']=null;
					$this->form_validation->set_message ( 'check_device_slno', '%s should be numeric' );
					return FALSE;
				}
				else if (strlen($dvs_slno) > 6 ) // Check whether the Device Slno should be 15 characters
				{
					$GLOBALS['dvice_slno']=null;
					$this->form_validation->set_message ( 'check_device_slno', 'Slno number should be less that 6digits' );
					return FALSE;
				}
				else if(preg_match('$\.$', $dvs_slno))
				{
					$GLOBALS['dvice_slno']=null;
					$this->form_validation->set_message ( 'check_device_slno', 'Please Enter valid Slno number' );
					return FALSE;
				}
				else
				{
					foreach ( $GLOBALS ['DeviceList'] as $row ) {
						if ($GLOBALS ['dvice_id'] == null) {
							if (trim ( strtoupper ( $row ['device_slno'] ) ) == strtoupper ( $dvs_slno )) // Check whether Device Slno number already exist in vts_device table
							{
								//$GLOBALS['dvice_slno']=null;
								$GLOBALS['outcome_with_db_check']="Slno ".'"'.trim($row['device_slno']).'"'." already exist";
								$this->form_validation->set_message('check_device_slno', '');
								return FALSE;
							}
						} else {
							if (trim ( strtoupper ( $row ['device_slno'] ) ) == strtoupper ( $dvs_slno ) && $GLOBALS ['dvice_id'] != trim ( $row ['device_id'] )) // Check whether Device Slno already exist in vts_device table while update
							{
								//$GLOBALS['dvice_slno']=null;
								$GLOBALS['outcome_with_db_check']="Slno ".'"'.trim($row['device_slno']).'"'." already exist";
								$this->form_validation->set_message('check_device_slno', '');
								return FALSE;
							}
						}
					}
				}
			}
		}
		//$GLOBALS['dvice_imei']=(null!=($this->input->post('DeviceSlno'))?$this->input->post('DeviceSlno'):null);
		return true;
	}
	
	/*
	 * This function is to validate the distributor name
	 */
	public function check_distributor_name($dvs_dist_id)
	{
		$GLOBALS['dvice_distributor_id']=null;
		if(empty($dvs_dist_id))
		{
			$GLOBALS['outcome_with_db_check']="As you have selected the release date, please select the distributor";
			$this->form_validation->set_message('check_distributor_name', '');
			return FALSE;
		}
		$GLOBALS['dvice_distributor_id']=(null!=($this->input->post('DeviceDistributorId'))?$this->input->post('DeviceDistributorId'):null);
		return true;
	}
	
	/*
	 * This function is to validate the device type
	 */
	public function check_device_type($dvs_type)
	{
		$GLOBALS['dvice_device_type_id']=null;
		if(empty($dvs_type))
		{
			$this->form_validation->set_message('check_device_type', '%s required');
			return FALSE;
		}
		$GLOBALS['dvice_device_type_id']=(null!=($this->input->post('DeviceTypeId'))?$this->input->post('DeviceTypeId'):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_device IMEI name
	 */
	public function check_device_imei($dvs_imei)
	{
		$temp['dvice_imei'] = trim($dvs_imei,",");
		if (empty($temp['dvice_imei'])) //Check whether the Device IMEI field is empty
		{
			$GLOBALS['dvice_imei']=null;
			$this->form_validation->set_message('check_device_imei', '%s required');
			return FALSE;
		}
		else
		{
			$temp['dvice_imei_list']=array();
			$temp['dvice_imei_list']=explode ( ",", $temp['dvice_imei'] );
			foreach ($temp['dvice_imei_list'] as $imei)
			{
				$dvs_imei=trim($imei);
				if (empty($dvs_imei)) //Check whether the Device IMEI field is empty
				{
					$GLOBALS['dvice_imei']=null;
					$this->form_validation->set_message('check_device_imei', '%s required');
					return FALSE;
				}
				else if (!is_numeric($dvs_imei)) // Check whether the given Device IMEI is numeric.
				{
					$GLOBALS['dvice_imei']=null;
					$this->form_validation->set_message ( 'check_device_imei', '%s should be numeric' );
					return FALSE;
				}
				else if (strlen($dvs_imei) < 15 || strlen($dvs_imei) > 15 ) // Check whether the Device IMEI should be 15 characters
				{
					$GLOBALS['dvice_imei']=null;
					$this->form_validation->set_message ( 'check_device_imei', 'Please enter the 15 digits valid IMEI number' );
					return FALSE;
				}
				else if(preg_match('$\.$', $dvs_imei))
				{
					$GLOBALS['dvice_imei']=null;
					$this->form_validation->set_message ( 'check_device_imei', 'Please Enter valid IMEI number' );
					return FALSE;
				}
				else
				{
					foreach ( $GLOBALS ['DeviceList'] as $row ) {
						if ($GLOBALS ['dvice_id'] == null) {
							if (trim ( strtoupper ( $row ['device_imei'] ) ) == strtoupper ( $dvs_imei )) // Check whether Device IMEI number already exist in vts_device table
							{
								//$GLOBALS['dvice_imei']=null;
								$GLOBALS['outcome_with_db_check']="Device ".'"'.trim($row['device_imei']).'"'." already exist";
								$this->form_validation->set_message('check_device_imei', '');
								return FALSE;
							}
						} else {
							if (trim ( strtoupper ( $row ['device_imei'] ) ) == strtoupper ( $dvs_imei ) && $GLOBALS ['dvice_id'] != trim ( $row ['device_id'] )) // Check whether Device IMEI already exist in vts_device table while update
							{
								//$GLOBALS['dvice_imei']=null;
								$GLOBALS['outcome_with_db_check']="Device ".'"'.trim($row['device_imei']).'"'." already exist";
								$this->form_validation->set_message('check_device_imei', '');
								return FALSE;
							}
						}
					}
				}
			}
		}
		//$GLOBALS['dvice_imei']=(null!=($this->input->post('DeviceIMEI'))?$this->input->post('DeviceIMEI'):null);
		return true;
	}
	
	public function check_release_date($dce_release_date)
	{
		if (empty($GLOBALS['dvice_release_distributor_date']))
		{
			$GLOBALS['outcome_with_db_check']="As you have selected the distributor, please select release date";
			$this->form_validation->set_message('check_release_date', '');
			return FALSE;
			
		}
		else if( $GLOBALS['dvice_release_distributor_date'] < $GLOBALS['dvice_prepare_date'])
		{
			$GLOBALS['outcome_with_db_check']="Release date should be greater than Prepare date";
			$this->form_validation->set_message('check_release_date', '');
			return FALSE;
		}
		$GLOBALS['dvice_release_distributor_date']=(null!=trim($this->input->post('DeviceReleaseDate'))?trim($this->input->post('DeviceReleaseDate')):null);
		return true;
	}
	
	/*
	 * This function is to validate the prepared date field
	 */
	public function check_prepare_date($dce_prepare_date)
	{
		$GLOBALS['dvice_prepare_date'] = null;
		if (empty($dce_prepare_date))
		{
			$this->form_validation->set_message('check_prepare_date', '%s required');
			return FALSE;
		}
		$GLOBALS['dvice_prepare_date']=(null!=trim($this->input->post('DevicePrepareDate'))?trim($this->input->post('DevicePrepareDate')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_vehicle_group
	 */
	public function editOrDelete_vts_device()
	{
		$posted=$this->input->get_post('device_id');
		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$splitPosted[0];	//it hold the operation to perform(i.e. edit or delete).
		$DeviceId=$splitPosted[1];	//it hold the device id to edit or delete.
	
		$row=$this->device_model->get_vts_device(null,null,$DeviceId);
		if($operation=="edit")
		{
			$GLOBALS=array('dvice_id'=>trim($row['device_id']),'dvice_imei'=>trim($row['device_imei']),'dvice_prepare_date'=>trim($row['device_prepare_date']),'dvice_release_distributor_date'=>trim($row['device_release_distributor_date']),'dvice_remarks'=>trim($row['device_remarks']),'active'=>trim($row['device_is_active']),'dvice_device_type_id'=>trim($row['device_device_type_id']), 'dvice_distributor_id'=>trim($row['device_distributor_id']), 'dvice_dealer_id'=>trim($row['device_dealer_id']),'dvice_slno'=>trim($row['device_slno']));//It used to fill up the textbox in view.
		}
		else if($operation=="delete")
		{
			try
			{
				if($this->device_model->InsertOrUpdateOrdelete_vts_device($DeviceId,null,$operation))
				{
					$GLOBALS['outcome_with_db_check']="Device ".'"'.trim($row['device_imei']).'"'." Deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEVICE,
								"Device is deleted: Device ID=".trim($DeviceId));
				}else
				{
					$GLOBALS ['outcome_with_db_check'] = 'Device "'.trim ( $row ['device_imei'] ).'"' . " Can't able to Delete(Referred some where else)";
				}
			}catch (Exception $ex){
				$this->table_pagination();
	
			}
		}
		$this->table_pagination();
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		
		$GLOBALS['distributorList']=$this->device_model->get_allDistributor();
		$GLOBALS['deviceTypeList']=$this->device_model->get_allDeviceTypes();
		
		$GLOBALS['DeviceList']=$this->device_model->get_vts_device();
		
		$config['base_url'] = base_url("index.php/device_ctrl/table_pagination/");
		$config['total_rows'] = count($GLOBALS['DeviceList']);
// 		$config['uri_segment'] = 3;
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['DeviceList']=$this->device_model->get_vts_device(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('device_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('service_model');
		$this->load->helper(array('form', 'url'));
		//the below data will save to the vts_service table
		$GLOBALS['srvc_id']=null;
		$GLOBALS['srvc_name']=null;
		$GLOBALS['srvc_inval']=null;

		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ServiceList']=$this->service_model->get_vts_service();
		$GLOBALS['ID'] = $this->session->userdata('login');
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";

	}

	public function index()
	{
		$this->table_pagination();
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_device_type in database.
	 */
	public function vts_service_validation()
	{
		$GLOBALS['srvc_id']=(null!=($this->input->post('ServiceId'))?$this->input->post('ServiceId'):null);
		$GLOBALS['srvc_name']=(null!=trim($this->input->post('ServiceName'))?trim($this->input->post('ServiceName')):null);
		$GLOBALS['srvc_inval']=(null!=trim($this->input->post('ServiceInterval'))?trim($this->input->post('ServiceInterval')):null);
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('ServiceName', 'Service Name', 'callback_check_service_name');
		$this->form_validation->set_rules('ServiceInterval', 'Service Interval', 'callback_check_service_interval');
		

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Service Type in vts_service table.
		{
				
			$this->table_pagination();
		}
		else
		{
			$data['service_name']=$GLOBALS['srvc_name'];
			$data['service_interval']=$GLOBALS['srvc_inval'];

			
			if($GLOBALS['srvc_id']!=null)		//edit
			{
				$data['service_id']=$GLOBALS['srvc_id'];
				if ($this->service_model->InsertOrUpdateOrdelete_vts_service($GLOBALS['srvc_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], SERVICE,
								"Updated Service: Service ID=".trim($GLOBALS['srvc_id']).",Service Name=".trim($GLOBALS['srvc_name']).", Service Interval=".trim($GLOBALS['srvc_inval']));
	
						$GLOBALS['outcome']="Service ".'"'.trim($GLOBALS['srvc_name']) .'"'." updated successfully";
						$GLOBALS['srvc_id']=null;
	
				}
			}
			else						 		//insert
			{
				if($this->service_model->InsertOrUpdateOrdelete_vts_service(null,$data,'insert'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], SERVICE,
								"Created New Service: Service Name=".trim($GLOBALS['srvc_name']).", Service Intervel=".trim($GLOBALS['srvc_inval']));
	
							$GLOBALS['outcome']="New Service ".'"'.trim($GLOBALS['srvc_name']).'"'." created successfully";
							$GLOBALS['srvc_id']=null;
				}
			}
			$this->table_pagination();
		}
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_service name
	 */
	public function check_service_name($srvc_nm)
	{
		$GLOBALS['srvc_name']=null;
		if (empty($srvc_nm)) //Check whether the Service name field is empty
		{
			$this->form_validation->set_message('check_service_name', '%s required');
			return FALSE;
		}
		else if (strlen($srvc_nm) < 5 || strlen($srvc_nm) > 40 ) // Check whether the service name should be in between 5 and 20 characters
		{
			$this->form_validation->set_message ( 'check_service_name', 'Service name should be in between 5-40 characters' );
			return FALSE;
		}
		else if (is_numeric($srvc_nm) || preg_match('$\./$', $srvc_nm) || preg_match("/\\\\/", $srvc_nm)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_service_name', 'Invalid Service Name' );
			return FALSE;
		}
		else if (preg_match('/([!@^&*_\-{}`~<>,\.?%$#\*])/', $srvc_nm))
		{
			$this->form_validation->set_message('check_service_name','Service Name is invalid');
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['ServiceList'] as $row ) {
				if ($GLOBALS ['srvc_id'] == null) {
					if (trim ( strtoupper ( $row ['service_name'] ) ) == strtoupper ( $srvc_nm )) // Check whether service name already exist in vts_service table
					{
						$GLOBALS['outcome_with_db_check']="Device Type ".'"'.trim($row['service_name']).'"'." already exist";
						$this->form_validation->set_message('check_service_name', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['service_name'] ) ) == strtoupper ( $srvc_nm ) && $GLOBALS ['srvc_id'] != trim ( $row ['service_id'] )) // Check whether service name already exist in vts_service table while update
					{
						$GLOBALS['outcome_with_db_check']="Service ".'"'.trim($row['service_name']).'"'." already exist";
						$this->form_validation->set_message('check_service_name', '');
						return FALSE;
					}
				}
			}
		}
		
		$GLOBALS['srvc_name']=(null!=trim($this->input->post('ServiceName'))?trim($this->input->post('ServiceName')):null);
		return true;
	}
	
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_service name
	 */
	public function check_service_interval($srvc_int)
	{
		$GLOBALS['srvc_inval']=null;
		if (empty($srvc_int)) //Check whether the Service name field is empty
		{
			$this->form_validation->set_message('check_service_interval', '%s required');
			return FALSE;
		}
		else if (!is_numeric($srvc_int))
		{
			$this->form_validation->set_message('check_service_interval', 'Intervel should be a numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $srvc_int))
		{
			$this->form_validation->set_message ( 'check_service_interval', 'Please Enter valid Intervel' );
			return FALSE;
		}
		$GLOBALS['srvc_inval']=(null!=trim($this->input->post('ServiceInterval'))?trim($this->input->post('ServiceInterval')):null);
		return true;
	}
	
	
	/*
	 * This function used to edit or delete the vts_service
	 */
	public function editOrDelete_vts_service()
	{
		$posted=$this->input->get_post('service_id');
		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$splitPosted[0];			//it hold the operation to perform(i.e. edit or delete).
		$ServiceId=$splitPosted[1];			//it hold the device type id to edit or delete.
	
		$row=$this->service_model->get_vts_service(null,null,$ServiceId);
		if($operation=="edit")
		{
			$GLOBALS=array('srvc_id'=>trim($row['service_id']),'srvc_name'=>trim($row['service_name']),'srvc_inval'=>trim($row['service_interval']));//It used to fill up the textbox in view.
				
		}
		else if($operation=="delete")
		{
			try
			{
				if($this->service_model->InsertOrUpdateOrdelete_vts_service($ServiceId,null,$operation))
				{
					$GLOBALS['outcome_with_db_check']="Service  ".'"'.trim( $row['service_name']).'"'." deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], SERVICE,
								"Service is deleted: Service ID=".$ServiceId);
					
// 						$GLOBALS['outcome']="Service  ".'"'.trim($row['service_name']).'"'." Deleted.";
				}else
				{
					$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['service_name'] ).'"' . " Can't able to delete(Referred some where else)";
				}
				
					
			}catch (Exception $ex){
				$this->table_pagination();
	
			}
		}
		$this->table_pagination();
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){
		$GLOBALS['ID'] = $this->session->userdata('login');
		$config['base_url'] = base_url("index.php/service_ctrl/table_pagination/");
		$GLOBALS['ServiceList']=$this->service_model->get_vts_service();
		$config['total_rows'] = count($GLOBALS['ServiceList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['ServiceList']=$this->service_model->get_vts_service(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('service_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
	
}

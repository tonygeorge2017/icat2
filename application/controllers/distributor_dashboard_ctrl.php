<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributor_dashboard_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('distributor_dashboard_model');
		$this->load->helper(array('form', 'url'));
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['clientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['distributorID'] = $GLOBALS ['ID'] ['sess_distributorid'];
		$GLOBALS['sessdistributorID']= $GLOBALS ['ID'] ['sess_distributorid'];
		$GLOBALS['dist_device_details']=array();
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	//rest of code shud come here
	public function index() {
		if ($GLOBALS ['sessClientID'] == AUTOGRADE_USER) {
			$GLOBALS ['distributorID']=AUTOGRADE_USER;
		}else {
			$GLOBALS ['distributorID'] = $GLOBALS ['ID'] ['sess_distributorid'];
		}
		$this->display ();
	}
	
	public function view() {
		$GLOBALS ['distributorID'] = (null != trim ( $this->input->post ( 'DistributorID' ) ) ? trim ( $this->input->post ( 'DistributorID' ) ) : null);
		$this->display ();
	}
	public function display() {
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['timezone'] = ($GLOBALS ['ID'] ['sess_time_zonename']) ? $GLOBALS ['ID'] ['sess_time_zonename'] : ''; // ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			//$GLOBALS ['clientList'] = $this->common_model->get_allClients ();
			$GLOBALS ['distributorList'] = $this->distributor_dashboard_model->get_all_distributor ();
			//$GLOBALS['count_devices_yr']=$this->common_model->get_device_details();
			//$GLOBALS['count_devices_mnth']=$this->common_model->get_device_details();
			
			//$this->get_purchase($div_id);
	
			$this->common_model->menu_display ();
			//$this->get_speed_violations_data ();
			//$this->get_service_status ();
			$this->get_device_data ();
				
		}
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'distributor_dashboard_view', $GLOBALS ); // load view Home page
		$this->load->view ( 'header_footer/footer' );
	}
	/*
	 * functio get purchase details for mnth in a grid
	 * 
	 */
	public function get_purchase($div_id)
	{
		if(isset($div_id))
		{
		$grid_data = $this->distributor_dashboard_model->get_device_purchase_table_m();
		//$speed = $this->get_speed_grid($clientid, $vhgpid,$drvgpid);
		$html2 = "";
		$html = "<table>
					<tr>
					<th>No of Devices Purchase Details for Month</th>
					<th>Device IMEI No</th>
					<th>Is Active</th>
					<th>Dealer Id</th>
					<th>Dealer Name</th>
					<th>Release Date</th>
					<th>Remarks</th>
				</tr>";
		foreach ( $grid_data as $row ) {
			$html2 .= "<tr>
						<td>" . $row ['device_imei'] . "</td>
						<td>" . $row ['device_is_active'] . "</td>
						<td>" . $row ['device_dealer_id'] . "</td>
						<td>" . $row ['dealer_name'] . "</td>
						<td>" . $row ['device_release_dealer_date'] . "</td>
						<td>" . $row ['device_remarks'] . "</td>
								
					</tr>";
		}
		"</table>";
		//$this->get_speed_violations_data($clientid,$vhgpid,$drvgpid);
		echo $html . $html2;
	}
	}
	
	/*
	 * function to get count of purchase & sales details 
	 * 
	 */
	public function get_device_data()
	{
		$GLOBALS['dist_device_details'] = $this->common_model->get_device_distributor_details($GLOBALS ['distributorID']);
			
	}
}
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fuel_report_ctrl extends CI_Controller {
	
	/*
	 * class constructor
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('fuel_report_model');
		$this->load->model('vehicle_report_model');
		$this->load->library ( 'JpGraph/Graph' );
		$this->load->library ( 'commonfunction' );
		$this->common_model->check_session();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['clientID']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS ['vcle_rept_client_id'] = null;
		$GLOBALS ['vcle_rept_vclegroup_id'] = null;
		$GLOBALS ['clientList'] = $this->vehicle_report_model->get_allClients ();

	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index() {
		
		$this->get_Vehicle_List ( $GLOBALS ['vcle_rept_client_id'] );
		
		
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display() {
		
		if($this->common_model->check_session()) {
			
			$this->common_model->menu_display( "fuel_report_ctrl" );
			$this->load->view('header_footer/header',$GLOBALS);
			$this->load->view('fuel_report_view',$GLOBALS);
			$this->load->view ( 'header_footer/footer' );
			
		}
	}
	
	/**
	 * @param string $cnt_id
	 * 
	 * Validate fuel report form and display HTML table as output
	 */
	
	public function get_fuel_report_table( $cnt_id = null ) {
		
		$GLOBALS ['temp'] = (null != ($this->input->post ( 'is_client' )) ? $this->input->post ( 'is_client' ) : null);
		
		if ($GLOBALS ['temp'] != "-1") {
			
			$this->get_Vehicle_List ( $this->input->post("is_client") );
			
		} else if( $cnt_id == null ) {
			
			$GLOBALS ['vcle_rept_client_id'] = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
			$GLOBALS ['vcle_rept_from_date'] = (null != trim ( $this->input->post ( 'fuel_sense_StartDate' ) ) ? trim ( $this->input->post ( 'fuel_sense_StartDate' ) ) : null);
			$GLOBALS ['vcle_rept_to_date'] = (null != trim ( $this->input->post ( 'fuel_sense_EndDate' ) ) ? trim ( $this->input->post ( 'fuel_sense_EndDate' ) ) : null);
			$GLOBALS ['vcle_rept_vclegroup_id'] = (null != trim ( $this->input->post ( 'VehicleGroup' ) ) ? trim ( $this->input->post ( 'VehicleGroup' ) ) : null);
			$GLOBALS ['vcle_rept_vehicle_id'] = (null != $this->input->post ( 'vehicle_name' ) ? $this->input->post ( 'vehicle_name' ) : null);
			
			$GLOBALS ['vh_gp_name'] = (null != trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) ? trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) : null);
			$GLOBALS ['vh_name'] = (null != trim ( $this->input->post ( 'hiddenVehicle' ) ) ? trim ( $this->input->post ( 'hiddenVehicle' ) ) : null);
			
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
				$GLOBALS ['vcle_rept_client_id'] = (null != ($this->input->post ( 'ClientName' )) ? $this->input->post ( 'ClientName' ) : null);
			} else
				$GLOBALS ['vcle_rept_client_id'] = $GLOBALS ['ID'] ['sess_clientid'];
				
			$GLOBALS ['temp'] = $GLOBALS ['vcle_rept_client_id'];
			
			$this->form_validation->set_message ( 'required', '%s required' );
			
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
				$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_cnt_id');
			}
			$this->form_validation->set_rules('vehicle_name', 'Vehicle Name', 'callback_check_vcle_name');
			$this->form_validation->set_rules('fuel_sense_StartDate', 'Start Date', 'callback_check_start_date');
			$this->form_validation->set_rules('fuel_sense_EndDate', 'End Date', 'callback_check_end_date');
			$this->form_validation->set_rules('VehicleGroup', 'Vehicle Group', 'callback_check_vcle_group');
			
			if ($this->form_validation->run() == FALSE) {
					
				$this->get_Vehicle_List ( $GLOBALS ['temp'] );
				
			} else {
				
				$i_vehicle_grp = trim ( $this->input->post ( 'VehicleGroup' ) );
				$i_vehicle_grp = $this->security->xss_clean( $i_vehicle_grp );
				
				$i_vehicle = $this->input->post("vehicle_name");
				$i_vehicle = trim( $i_vehicle );
				$i_vehicle = $this->security->xss_clean( $i_vehicle );
			
				$start = $this->input->post("fuel_sense_StartDate");
				$start = trim( $start );
				$start = $this->security->xss_clean( $start );
			
				$end = $this->input->post("fuel_sense_EndDate");
				$end = trim( $end );
				$end = $this->security->xss_clean( $end );
			
				$this->session->set_userdata( array("vehicle_group"=>$i_vehicle_grp, "vehicle_name" => $i_vehicle, "fuel_start_date"=>$start, "fuel_end_date"=>$end) );
				
				$data = $this->fuel_report_model->get_fuel_report_graph_model( $i_vehicle_grp, $i_vehicle, $start, $end );
				
				$GLOBALS["selected_vehicle_name"] = $i_vehicle;
				
				$report_sub_title = "For the period from " . date('Y-m-d', strtotime($GLOBALS ['vcle_rept_from_date'])) . " to " . date('Y-m-d', strtotime($GLOBALS ['vcle_rept_to_date']));
				$test = false;
				
				$report_sub_title .= " ( Vehicle Group Name: ".$GLOBALS ['vh_gp_name'].", Vehicle Name: ".$GLOBALS ['vh_name']." )";
				
				$report_title = "Fuel Sensor Report ";
				if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
					$client_name = $this->common_model->get_dropdownCntName ( $GLOBALS ['vcle_rept_client_id'] );
					$report_title .= "(".$client_name.")";
				}
				
				$this->session->set_userdata ( 'SESSION_DATA_1', $report_title );
				$this->session->set_userdata ( 'SESSION_DATA_2', $report_sub_title );
								
				if( isset( $data["data"] ) ) {
					
					if( sizeof( $data["data"] ) !=0 )
						$GLOBALS["fuel_sensor_data"] = json_encode( $data["data"] );
					else
						$GLOBALS["fuel_sensor_data"] = "No_Data";
					
				} else {
					
					$GLOBALS["fuel_sensor_data"] = $data["error"];
					
				}
					
				$this->get_Vehicle_List( $GLOBALS ['temp'] );
				
			}
			
		}
		
		
			
	}
	
	/*
	 * Export fuel report data as CSV or PDF based on option selected
	 */
	public function fuel_report_export() {
		
		$i_vehicle_grp = $this->session->userdata("vehicle_group");
		$i_vehicle_name = $this->session->userdata("vehicle_name");
		$s_fuel_start_date = $this->session->userdata("fuel_start_date");
		$s_fuel_end_date = $this->session->userdata("fuel_end_date");
		
		$data = $this->fuel_report_model->get_fuel_report_graph_model( $i_vehicle_grp, $i_vehicle_name, $s_fuel_start_date, $s_fuel_end_date );
		$session_data = $this->session->userdata ('login');
		
		$title = $this->session->userdata ( 'SESSION_DATA_1' );
		$sub_title = $this->session->userdata ( 'SESSION_DATA_2' );
			
		$headers = array (
				array (
						"sno" => "Sl. No",
						"date" => "Date",
						"time" => "Time",
						"distance_travelled" => "Distance (Kms)",
						"liter" => "Fuel ltr",
						"fuel_percent" => "Fuel Percentage",
						"mileage" => "Mileage (Kms/Ltr)",
				)
		);
			
		$i = 0;
			
		foreach( $data["data"] AS $row ) {
			$headers [$i + 1] ["sno"] = $i + 1;
			$headers [$i + 1] ["date"] = date("d/m/y", strtotime( $row ["time_value"] ) );
			$headers [$i + 1] ["time"] = date("H:i:m", strtotime( $row ["time_value"] ) );
			$headers [$i + 1] ["distance_travelled"] = $row ["vehicle_run_data"];
			$headers [$i + 1] ["liter"] = $row ["liters"];
			$headers [$i + 1] ["fuel_percent"] = $row ["fuel_percentage"]."%";
			$headers [$i + 1] ["mileage"] = $row ["mileage"];
			$i ++;
		
		}
		
		if( $this->input->post("btn_csv") ) {
			
			$this->load->library ( 'commonfunction' );
			$this->commonfunction->csv_create ( str_replace(' ', '_', $title), $sub_title, $headers );
			
			
		} else if( $this->input->post("btn_pdf") ) {
			
			$this->load->helper ( array (
						'dompdf',
						'file' 
				) );
				
				// starts from here----
				$html = " <html>
							<head></head>
							<body>
							<table>
								<tr><td>
									<table>
										<tr><td><b>$title</td></td></tr>
										<tr><td>$sub_title</td><td></td></tr>
									</table>
									</td><br/>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>
										<table>
										<tr>
											<td> <img alt='' src='". base_url('assets/images/logo.png')."'/></td>
										</tr>
										</table>
									</td>
								</tr>
							</table>
							<hr>
							<table> ";
								$table_data="";
								$i = 0;
								foreach ( $headers as $header ) {
									
									$table_data.= "<tr>";
										$table_data.="<td style='width: 100px'>".$header['sno']."</td>";
										$table_data.="<td style='width: 100px'>" . $header ['date'] . "</td>";
										$table_data.="<td style='width: 100px'>" . $header ['time'] . "</td>";
										$table_data.="<td style='width: 100px'>" . $header ['distance_travelled'] . "</td>";
										$table_data.="<td style='width: 100px'>" . $header ['liter'] . "</td>";
										$table_data.="<td style='width: 100px'>" . $header ['fuel_percent'] . "</td>";
										$table_data.="<td style='width: 100px'>" . $header ['mileage'] . "</td>";
									$table_data.="</tr>";
									
									$i++;
								}
								$html_1 = "</table>
											<hr>
											<table>&nbsp;&nbsp;&nbsp;&nbsp;
												<tr>
													<td><b>Prepared by </b> : ".$session_data ['sess_user_name']."</td>
													<td></td>
													<td style='padding-left:390px;'><b>".date ( 'Y-m-d' )."</b></td>
												</tr>
											</table>
							</body>
							</html>";
					
					$html = $html.$table_data.$html_1;
					
				pdf_create ( $html, str_replace(' ', '_', $title) . '_' . date ( 'Ymd' ) );
			
		}
		
	}
	
	/*
	 * Validation callback function to validate Vehicle name
	 */
	public function check_vcle_name( $vcle_grp ) {
		
		$GLOBALS['selected_vehicle_name']=null;
		if( empty( $vcle_grp ) ) {
// 			$this->form_validation->set_message('check_vcle_group', "%s required");
			$GLOBALS['outcome_with_db_check']='Please select vehicle';
			return FALSE;
		}
		$GLOBALS['selected_vehicle_name']=(null !=trim($this->input->post('vehicle_name')) ? trim($this->input->post('vehicle_name')) : null);
		return true;
		
	}
	
	/*
	 * Validation callback function to validate From date
	 */
	public function check_start_date( $vcle_grp ) {
		$GLOBALS['start_date']=null;
		if( empty( $vcle_grp ) ) {
			// 			$this->form_validation->set_message('check_vcle_group', "%s required");
			$GLOBALS['error_start_date']='Please select From date';
			return FALSE;
		}
		$GLOBALS['start_date']=(null !=trim($this->input->post('fuel_sense_StartDate')) ? trim($this->input->post('fuel_sense_StartDate')) : null);
		return true;
	}
	
	
	/*
	 * Validation callback function to validate To date
	 */
	public function check_end_date( $vcle_grp ) {
		$GLOBALS['end_date']=null;
		if(empty($vcle_grp)) {
			// 			$this->form_validation->set_message('check_vcle_group', "%s required");
			$GLOBALS['error_end_date']='Please select To date';
			return FALSE;
		}
		$GLOBALS['end_date']=(null !=trim($this->input->post('fuel_sense_EndDate')) ? trim($this->input->post('fuel_sense_EndDate')) : null);
		return true;
	}
	
	
	/*
	 * Validation callback function to validate client name
	 */
	public function check_client_name( $client ) {
		
		$GLOBALS['vcle_rept_client_id'] = null;
		
		if( empty( $client ) ) {
	
			$GLOBALS['error_client_name'] = 'Please select client name';
			
			return FALSE;
		}
		
		$GLOBALS['vcle_rept_client_id']= ( null != trim( $this->input->post('ClientName') ) ? trim( $this->input->post('ClientName') ) : null );
		
		return true;
		
	}
	
	/*
	 * this function is to validate vehicle group dropdown
	 */
	public function check_vcle_group($vcle_grp)	{
		$GLOBALS['vcle_rept_vclegroup_id']=null;
		if(empty($vcle_grp)) {
			$GLOBALS["error_vehicle_grp"] = "Please select vehicle group";
			return FALSE;
		}
		$GLOBALS['vcle_rept_vclegroup_id']=(null !=trim($this->input->post('VehicleGroup')) ? trim($this->input->post('VehicleGroup')) : null);
		return true;
	}
	
	/*
	 * This function is to generate graph from given input data
	 * inputs are: vehicle name, from and end to date
	 */
	public function fuel_report_graph() {
	
		$this->common_model->menu_display ();
		$this->load->library ( 'commonfunction' );
		
		$i_vehicle_grp = $this->session->userdata("vehicle_group");
		$i_vehicle_name = $this->session->userdata("vehicle_name");
		$s_fuel_start_date = $this->session->userdata("fuel_start_date");
		$s_fuel_end_date = $this->session->userdata("fuel_end_date");
		
		$data = $this->fuel_report_model->fuel_report_graph_model( $i_vehicle_grp, $i_vehicle_name, $s_fuel_start_date, $s_fuel_end_date );
		
		
		if( isset( $data["data"] ) ) {
			
			if( sizeof( $data["data"] )!=0 )
				$GLOBALS ['line_chart'] = $this->commonfunction->draw_line_fuel_graph_date( $data );
			else
				$GLOBALS ['line_chart'] = "No_DATA";
			
		} else {
			
			$GLOBALS ['line_chart'] = $data["error"];
			
		}
		
		$this->common_model->menu_display ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'fuel_report_graph_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}
	
	/*
	 * This function to get all vehicle names based on client id
	 */
	private function get_Vehicle_List( $cl_id = null ) {
		
		if ($cl_id != null) {
			
			$GLOBALS ['vcle_rept_client_id'] = $cl_id;
		}
		$cntId = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
		if($cntId == null)
			$cntId = $GLOBALS ['ID'] ['sess_clientid'];
		
		$GLOBALS ['clientList'] = $this->vehicle_report_model->get_allClients();
		$GLOBALS ['vehicleGroupList'] = json_encode( $this->vehicle_report_model->get_allVehicleGroups ( $cntId ));
		$result = $this->fuel_report_model->get_Vehicle_List_Model( $GLOBALS ['vcle_rept_vclegroup_id'] );
		$GLOBALS['vehicle_list'] =  json_encode( $result );
				
		$this->display();
	}
	
	
	/*
	 * This function to get all vehicle groups of a client
	 */
	public function get_grp_vehicles() {
	
		
		$result = $this->fuel_report_model->get_Vehicle_List_Model( $this->input->get("requestData") );
		echo json_encode( $result );
	}
	
}
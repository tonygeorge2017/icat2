<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_admin_tracking_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];		
		$this->load->model('vehicle_admin_tracking_model');		
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		if($GLOBALS['sessClientID']==AUTOGRADE_USER)
		{
			$userdate=$this->common_model->get_usertime();
			$date_value=new DateTime($userdate);
			$datevalue=$date_value->format('Y-m-d');
			$GLOBALS['startDate']=new DateTime($datevalue.' 00:00:00');
			$GLOBALS['endDate']=new DateTime($datevalue.' 23:59:59');
			$this->display();
		}
		else{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect("login_ctrl");
		}
	}
	/*
	 * This function is used to fetch the data from server_gps_data table
	 * to place the marker in the map
	 * @param
	 *  $imeino - imei no
	 *  $_fdate - from date
	 *  $_tdate - to date
	 *  $track_type - track type (live track or track history)	
	 *  $step_by_step - bulk or step-by-step
	 * Return type - JSON string
	 */
	public function fetch_gps_data($testerspeed,$imeino,$_fdate,$_tdate,$track_type,$step_by_step=null)
	{
		$get_in=false;
		$diff=$GLOBALS['ID']['sess_time_zonediff'];
		$fdate = new DateTime($_fdate); $tdate = new DateTime($_tdate);
		$check_diff=date_diff($fdate,$tdate);
		$diff_in_str=$check_diff->format("%R%a days");
		$fdate_rtn = new DateTime($_fdate); $tdate_rtn = new DateTime($_tdate);
		$fdate_rtn=$fdate_rtn->format('Y-m-d H:i:s');
		$tdate_rtn=$tdate_rtn->format('Y-m-d H:i:s');
		$outp='[{"lat":"Na","lng":"Na","speed":"Na","fdateTime":"'.$fdate_rtn.'","tdateTime":"'.$tdate_rtn.'"}]';
		if($step_by_step==null)
			$get_in=true;
		else if($diff_in_str > 0)
		{
			$get_in=false;
			$outp='[{"lat":"Na","lng":"TimeOutRange","speed":"Na","fdateTime":"'.$fdate_rtn.'","tdateTime":"'.$tdate_rtn.'"}]';
		}
		else
			$get_in=true;
		if($get_in)
		{
			$fdate=$this->common_model->get_dateTime($fdate->format('Y-m-d H:i:s'),'1');
			$tdate=$this->common_model->get_dateTime($tdate->format('Y-m-d H:i:s'),'1');
			if($track_type=="live" && $imeino==0)//For all vehicle live gps current position data
			{
				$gps_result=$this->vehicle_admin_tracking_model->get_all_live_vehicle_data($testerspeed);
			}
			elseif($track_type=="live" && $imeino!=0)
			{
				$gps_result=$this->vehicle_admin_tracking_model->get_all_live_vehicle_data($testerspeed, $imeino);
			}
			else
			{
				$gps_result=$this->vehicle_admin_tracking_model->get_gps_data($testerspeed,$imeino,$fdate,$tdate);
			}
			if($gps_result!=null)
			{
				$outp = json_encode($gps_result);
			}
		}
		echo($outp);
	}
	
	/*
	 * This function is usde to respond the from date and to date to the user for the selected time period
	 *  $cl_id - client id
	 *  $timeSelection - Today, Yesterday or Last week
	 * Return type - JSON string
	 */
	public function get_time($cl_id,$timeSelection)
	{
		$userdate=$this->common_model->get_usertime();
		$date_value=new DateTime($userdate);
		$datevalue=$date_value->format('Y-m-d');
		$fdate = new DateTime($datevalue.' 00:00:00'); $tdate = new DateTime($datevalue.' 23:59:59');
		if($timeSelection=='yesterday')
		{
			$fdate=$fdate->modify("-1 days");
			$tdate=$tdate->modify("-1 days");
		}
		else if($timeSelection=='lastweek')
		{
			$fdate=$fdate->modify("-7 days");
			$tdate=$tdate->modify("-1 days");
		}
		$outp='[{"FromDate":"'.$fdate->format('Y-m-d').'T'.$fdate->format('H:i').'","ToDate":"'.$tdate->format('Y-m-d').'T'.$tdate->format('H:i').'"}]';
		echo($outp);
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if($this->common_model->check_session())
		{
			$this->common_model->menu_display();			
			$this->load->view('header_footer/header_admin_track_screen',$GLOBALS);
			$this->load->view('vehicle_admin_tracking_view',$GLOBALS);
		}
	}
}
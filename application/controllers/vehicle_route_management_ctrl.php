<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_route_management_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('vehicle_route_management_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];	
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		$GLOBALS['sessClientAdmin']=$GLOBALS['sessionData']['sess_client_admin'];
		$GLOBALS['clientList']=array();
		$GLOBALS['clientList']=$this->vehicle_route_management_model->get_allClients();
		if($GLOBALS['sessUserID']==AUTOGRADE_USER)
			$GLOBALS['clientID']=(isset($GLOBALS['clientList'][0]['client_id']))?$GLOBALS['clientList'][0]['client_id']:$GLOBALS['sessionData']['sess_clientid'];
		else 
			$GLOBALS['clientID']=$GLOBALS['sessionData']['sess_clientid'];		
		$GLOBALS['vehicleGroupList']=array();
		$GLOBALS['vehicleList']=array();
		$GLOBALS['routeList']=array();
		$GLOBALS['vhRouteList']=array();
		$GLOBALS['editrouteList']=array();
		$GLOBALS['vhID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['editVhID']=null;
		$GLOBALS['routeID']=array();		
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	/*
	 * This function is used to validate the posted values 
	 * before store.
	 */
	public function vh_route_validation(){
		$GLOBALS['checkSelectedClientID']=(null!=($this->input->post('OnClientID'))?$this->input->post('OnClientID'):null);
		if($GLOBALS['checkSelectedClientID']==-1)
		{
			$this->form_validation->set_message('required', '%s required');			
			$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null);
			$GLOBALS['vhID']=(null!=($this->input->post('VhID'))?$this->input->post('VhID'):null);
			$GLOBALS['vehicleGroupID']=(null!=($this->input->post('VehicleGroup'))?$this->input->post('VehicleGroup'):null);
			$GLOBALS['editVhID']=(null!=($this->input->post('EditVhID'))?$this->input->post('EditVhID'):null);
			$GLOBALS['vehicleID']=(null!=($this->input->post('Vehicle'))?$this->input->post('Vehicle'):null);
			$GLOBALS['routeID']=(null!=($this->input->post('RouteArr'))?$this->input->post('RouteArr'):null);
			$routeArray=$GLOBALS['routeID'];			
			//log_message('debug',"********testtest".print_r($GLOBALS,true));
			//log_message('debug','RouteArray'.print_r($routeArray,true));
			$fence_data=array();
			foreach ($routeArray as $indx=>$value)
			{
			  $fence_data[$indx]["vh_rt_vehicle_id"]=$GLOBALS['vehicleID'];
			  $fence_data[$indx]["vh_rt_route_id"]=$value;			  
			}
			//log_message('debug','RouteArray'.print_r($fence_data,true));
			
			$this->form_validation->set_rules('ClientID', 'Client', 'required');
			$this->form_validation->set_rules('VehicleGroup', 'Vehicle group', 'required');
			$this->form_validation->set_rules('Vehicle', 'Vehicle', 'required');			
			$this->form_validation->set_rules('RouteArr', 'Route', 'required');
			if ($this->form_validation->run() != FALSE)
			{
				//log_message("debug","#####test1");
				//$data['vh_rt_vehicle_id']=$GLOBALS['vehicleID'];
				//$data['vh_rt_route_id']=$GLOBALS['routeID'];
				if($GLOBALS['vhID']==null)//insert
				{
					//log_message("debug","#####test2");
					if($this->vehicle_route_management_model->insert_edit_vhroute($fence_data))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessionData']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE_MANAGEMENT,
									"Route assigning:".json_encode($fence_data).", Client ID=".$GLOBALS['clientID']);
						$GLOBALS['outcome']='<div style="color: green;">Route assigning succeed.</div>';
						$this->clear_all();
					}
					else{
						$GLOBALS['outcome']='<div style="color: red;">Route assigning failed.</div>';
					}
				}
				else {//edit
					//log_message("debug","#####test3");
					if($this->vehicle_route_management_model->insert_edit_vhroute($fence_data, $GLOBALS['vhID'], "edit"))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessionData']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE_MANAGEMENT,
									"Update Route assigning:".json_encode($fence_data).", Client ID=".$GLOBALS['clientID']);
						$GLOBALS['outcome']='<div style="color: green;">Updation succeed.</div>';
						$this->clear_all();
					}
					else{
						$GLOBALS['outcome']='<div style="color: red;">Updation failed.</div>';
					}
				}
			}
		}
		else{
			$GLOBALS['clientID']=$GLOBALS['checkSelectedClientID'];
		}
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	private function clear_all()
	{
		$GLOBALS['vhID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['routeID']=null;
	}
	/* This function is used to get the vehicle for the selected vehicle group and return back the list of
	* vehicles in JSON format with out refresh the page.
	*  $vhGrp - vehicel group id
	* Return type - JSON string
	*/
	public function get_vhl_grp_vehicle($vhGrp=null, $vhID=0)
	{		
		$vhl_grp_vehicle=null;
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=json_encode($this->vehicle_route_management_model->get_all_vhGp_vehicle($vhGrp, $vhID));
				
		}
		echo($vhl_grp_vehicle);
	}
	/*
	 * This function is used to edit the route details
	 */
	public function edit_routedetails($vhRouteOrVehicleID, $opt='edit')
	{
		$delete=($opt==md5('delete'))?true:false;
		$result=$this->vehicle_route_management_model->get_VhRouteList(null, null, null, $vhRouteOrVehicleID, $delete);
		if($result!=null)
		{
			$GLOBALS['clientID']=isset($result[0]['route_client_id'])?$result[0]['route_client_id']:null;
			if($opt=='edit')
			{
				$GLOBALS['vhID']=isset($result[0]['vh_rt_vehicle_id'])?$result[0]['vh_rt_vehicle_id']:null;
				$GLOBALS['vehicleGroupID']=isset($result[0]['vehicle_group_id'])?$result[0]['vehicle_group_id']:null;
				$GLOBALS['editVhID']=$GLOBALS['vehicleID']=isset($result[0]['vh_rt_vehicle_id'])?$result[0]['vh_rt_vehicle_id']:null;
				$GLOBALS['routeID']=isset($result[0]['vh_rt_route_id'])?$result[0]['vh_rt_route_id']:null;
				foreach ($result as $row)
					$GLOBALS['editrouteList'][$row["vh_rt_route_id"]]=true;
			}
			else if($opt==md5('delete'))
			{
				if($this->vehicle_route_management_model->insert_edit_vhroute(null, $vhRouteOrVehicleID, 'delete'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessionData']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE_MANAGEMENT,
								"Removed assigned route:"." Route ID=".$vhRouteOrVehicleID.", Vehicle ID=".$result[0]['vh_rt_vehicle_id'].", Client ID=".$GLOBALS['clientID']);
					$GLOBALS['outcome']='<div style="color: green;">Removed successfully.</div>';
				}
				else
				{
					$GLOBALS['outcome']='<div style="color: red;">Removal failed.</div>';
				}
			}				
		}
		$this->table_pagination(md5($GLOBALS['clientID']));
	}

	/*
	 * This function is used to perform the pagination.
	 */
	public function table_pagination($client=null,$pageNo=0)
	{
		$GLOBALS['clientID']=$client;
		$GLOBALS['vehicleGroupList']=$this->vehicle_route_management_model->get_all_vhGp($client);
		if($GLOBALS['vehicleGroupList']!=null)
		{
			$GLOBALS['vehicleGroupID']=($GLOBALS['vehicleGroupID']!=null)?$GLOBALS['vehicleGroupID']:$GLOBALS['vehicleGroupList'][0]['vh_gp_id'];
			$vhID=($GLOBALS['vhID']!=null)?$GLOBALS['editVhID']:0;		
			$GLOBALS['vehicleList']=$this->vehicle_route_management_model->get_all_vhGp_vehicle($GLOBALS['vehicleGroupID'], $vhID);
		}
		$GLOBALS['routeList']=$this->vehicle_route_management_model->get_RouteList($client);
		$GLOBALS['vhRouteList']=$this->vehicle_route_management_model->get_VhRouteList($client, ROW_PER_PAGE, $pageNo);
		$config['uri_segment']=URI_SEGMENT_FOR_FOUR;
		$config['base_url'] = base_url("index.php/vehicle_route_management_ctrl/table_pagination/".$client);
		$config['total_rows'] = count($this->vehicle_route_management_model->get_VhRouteList($client));
		$config['per_page']=ROW_PER_PAGE;
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('vehicle_route_management_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
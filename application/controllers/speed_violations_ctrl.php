<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Speed_violations_ctrl extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->library('pagination');
		$this->load->library('session');
		$this->load->model('speed_violations_model');
		$this->load->model('common_model');
		// load core JpGraph as CI library
		$this->load->library ( 'JpGraph/Graph' );
		$this->common_model->check_session();
		$this->load->helper ( array (
				'form',
				'url'
		) );
		
		$GLOBALS ['outcome'] = null;
		$GLOBALS ['outcome_with_db_check'] = null;
		
		$GLOBALS ['no_data'] = false;
		$GLOBALS ['spd_vio_client_id'] = null;
		$GLOBALS ['spd_vio_strt_date'] = null;
		$GLOBALS ['spd_vio_end_date'] = null;
		$GLOBALS ['spd_vio_vclegroup_id'] = null;
		$GLOBALS ['spd_vio_vehicle_id'] = null;
		$GLOBALS ['spd_vio_dvrgroup_id'] = null;
		$GLOBALS ['spd_vio_driver_id'] = null;
		$GLOBALS ['query'] = null;
		$GLOBALS ['active'] = NOT_ACTIVE;
		
		$GLOBALS ['csvpdf_button'] = null;
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$session_userid = $GLOBALS ['ID'] ['sess_userid'];
		$GLOBALS ['clientList'] = $this->speed_violations_model->get_allClients ();
		$GLOBALS ['spd_vltn_data_fmdb'] = null;
		$GLOBALS ['get_gridDetails'] = array ();
		
		// to get user ip and host name
		$host_name = exec ( "hostname" ); // to get "hostname"
		$host_name = trim ( $host_name ); // remove any spaces before and after
		$ip = gethostbyname ( $host_name );
		$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
	}
	
	/*
	 * Function to display the view
	 */
	public function index() {
		$this->table_pagination ( md5 ( $GLOBALS ['spd_vio_client_id'] ) );
	}
	
	/*
	*This function is used to validate and add the new vts_vehicle in database.
	*/
	public function vts_speed_violations_validation($cnt_id = null)
	{
		$GLOBALS ['temp'] = (null != ($this->input->post ( 'temp' )) ? $this->input->post ( 'temp' ) : null);
		
		if ($GLOBALS ['temp'] != "-1") {
			$this->table_pagination ( md5 ( $GLOBALS ['temp'] ) );
		} else if ($cnt_id == null) {
			$GLOBALS ['spd_vio_client_id'] = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
			$GLOBALS ['spd_vio_vclegroup_id'] = (null != trim ( $this->input->post ( 'VehicleGroup' ) ) ? trim ( $this->input->post ( 'VehicleGroup' ) ) : null);
			$GLOBALS ['spd_vio_vehicle_id'] = (null != $this->input->post ( 'Vehicle' ) ? $this->input->post ( 'Vehicle' ) : null);
			log_message("debug","valueee---".print_r($GLOBALS ['spd_vio_vehicle_id'], true));
			$GLOBALS ['spd_vio_dvrgroup_id'] = (null != trim ( $this->input->post ( 'DriverGroupID' ) ) ? trim ( $this->input->post ( 'DriverGroupID' ) ) : null);
			$GLOBALS ['spd_vio_driver_id'] = (null != $this->input->post ( 'DriverID' ) ? $this->input->post ( 'DriverID' ) : null);
			$GLOBALS ['show_button'] = (null != trim ( $this->input->post ( 'ShowButton' ) ) ? trim ( $this->input->post ( 'ShowButton' ) ) : null);
			$GLOBALS ['active'] = (null != trim ( $this->input->post ( 'Spot' ) ) ? trim ( $this->input->post ( 'Spot' ) ) : NOT_ACTIVE);
			
			$GLOBALS ['vh_gp_name'] = (null != trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) ? trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) : null);
			$GLOBALS ['vh_name'] = (null != trim ( $this->input->post ( 'hiddenVehicle' ) ) ? trim ( $this->input->post ( 'hiddenVehicle' ) ) : null);
			$GLOBALS ['dr_gp_name'] = (null != trim ( $this->input->post ( 'hiddenDriverGroup' ) ) ? trim ( $this->input->post ( 'hiddenDriverGroup' ) ) : null);
			$GLOBALS ['dr_name'] = (null != trim ( $this->input->post ( 'hiddenDriver' ) ) ? trim ( $this->input->post ( 'hiddenDriver' ) ) : null);
			
			$GLOBALS ['spd_vio_strt_date'] = (null != trim ( $this->input->post ( 'SpeedVioStartDate' ) ) ? trim ( $this->input->post ( 'SpeedVioStartDate' ) ) : null);
			$GLOBALS ['spd_vio_end_date'] = (null != trim ( $this->input->post ( 'SpeedVioEndDate' ) ) ? trim ( $this->input->post ( 'SpeedVioEndDate' ) ) : null);
			
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
				$GLOBALS ['spd_vio_client_id'] = (null != ($this->input->post ( 'ClientName' )) ? $this->input->post ( 'ClientName' ) : null);
			} else {
				$GLOBALS ['spd_vio_client_id'] = $GLOBALS ['ID'] ['sess_clientid'];
			}
			
			$GLOBALS ['temp'] = $GLOBALS ['spd_vio_client_id'];
			$this->form_validation->set_message ( 'required', '%s required' );	// this will help to the change the message to display 'required' form validation rule.

			
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
				$this->form_validation->set_rules ( 'ClientName', 'Client Name', 'callback_check_cnt_id' );
			}
			$this->form_validation->set_rules ( 'SpeedVioStartDate', 'From Date', 'callback_check_strt_dt' );
			$this->form_validation->set_rules ( 'SpeedVioEndDate', 'To Date', 'callback_check_end_dt' );
			
			if ($this->form_validation->run () == FALSE)
			{ // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
				$row=$GLOBALS['spd_vio_vehicle_id'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['spd_vio_vehicle_id'][$i]=array('vh_link_vehicle_id'=>$_row);
						$i++;
					}
				}
				$row=$GLOBALS['spd_vio_driver_id'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['spd_vio_driver_id'][$i]=array('dr_link_driver_id'=>$_row);
						$i++;
					}
				}
			//$this->table_pagination( md5($GLOBALS ['temp']) );
			}
			else
			{
				$GLOBALS ['get_gridDetails'] = $this->speed_violations_model->get_values ($GLOBALS ['spd_vio_client_id'], $GLOBALS, $GLOBALS ['ID'] ['sess_userid'] );
				// STARTS here--- This condition to check the vehicel checkbox's so that the correct vehicels id's should go to the model (This should be after the above line only)
				$row=$GLOBALS['spd_vio_vehicle_id'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['spd_vio_vehicle_id'][$i]=array('vh_link_vehicle_id'=>$_row);
						$i++;
					}
				}
				// STARTS here--- This condition to check the driver checkbox's so that the correct driver id's should go to the model
				$row=$GLOBALS['spd_vio_driver_id'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['spd_vio_driver_id'][$i]=array('dr_link_driver_id'=>$_row);
						$i++;
					}
				}
				//ENDS here---
				
				$GLOBALS ['no_data'] = true;
				$arr = $GLOBALS ['get_gridDetails'];
				if ($GLOBALS ['active'] == 1) {
					$spot_add = array ();
					$i = 0;
					foreach ( $arr as $arr1 ) {
						array_push ( $spot_add, $this->common_model->getPlaceName ( $arr1 ['latitude'], $arr1 ['longitude'] ) );
						$arr [$i] ['spot'] = $spot_add [$i];
						$i ++;
					}
					$GLOBALS ['get_gridDetails'] = $arr;
					$query = $arr;
				} else {
					$nospot = array ();
					foreach ( $arr as $arr1 ) {
						$arr1 ['spot'] = "( Latitude - " . round ( $arr1 ['latitude'], 4 ) . " , Longitude - " . round ( $arr1 ['longitude'], 4 ) . ")";
						array_push ( $nospot, $arr1 );
					}
					$GLOBALS ['get_gridDetails'] = $nospot;
					$query = $nospot;
				}
				
				$this->speed_violations_model->InsertOrdelete_vts_tmp_report_speedviolations ( $query, 'delete', $GLOBALS ['active'] );
				$this->speed_violations_model->InsertOrdelete_vts_tmp_report_speedviolations ( $query, "insert", $GLOBALS ['active'] );
				
				
				$report_sub_title = "For the period from " . date('Y-m-d', strtotime($GLOBALS ['spd_vio_strt_date'])) . " to " . date('Y-m-d', strtotime($GLOBALS ['spd_vio_end_date']));
				$test = false;
				if (isset ( $GLOBALS ['vh_gp_name'] ) || isset ( $GLOBALS ['vh_name'] ) || isset ( $GLOBALS ['dr_gp_name'] ) || isset ( $GLOBALS ['dr_name'] ))
					$report_sub_title .= " ( ";
				if (isset ( $GLOBALS ['vh_gp_name'] ) && (count($GLOBALS ['spd_vio_vehicle_id']) == 0) && isset ( $GLOBALS ['dr_gp_name'] )) {
					$report_sub_title .= "Vehicle Group Name: " . $GLOBALS ['vh_gp_name'];
					$report_sub_title .= ",";
				} else if (isset ( $GLOBALS ['vh_gp_name'] )) {
					$report_sub_title .= "Vehicle Group Name: " . $GLOBALS ['vh_gp_name'];
				}
	
				// ------Starts from here------ This is to append the vehicle name's to the subtitle
				$vcle_name = array();
				if(count($GLOBALS ['spd_vio_vehicle_id']) > 0)
				{
					$vcle_count = count($GLOBALS ['spd_vio_vehicle_id']);
					$i = 0;
					$report_sub_title .= ", Vehicle Name: ";
					$report_sub_title1 = "";
					foreach($GLOBALS ['spd_vio_vehicle_id'] as $row)
					{
						$v_id = $row['vh_link_vehicle_id'];
						$name = $this->speed_violations_model->getVehicleName($v_id);
						array_push($vcle_name, $name);
						
						if(count($GLOBALS ['spd_vio_vehicle_id']) == 1)
						{
							$report_sub_title1 .= $vcle_name[$i];
						}
						else
						{
							if($i == $vcle_count-1)
							{
								$report_sub_title1 .= $vcle_name[$i];
							}
							else
							{
								$report_sub_title1 .= $vcle_name[$i].", ";
							}
						}
						$i++;
					}
					$report_sub_title .= "( ".$report_sub_title1.")";
				}
					
				if ((count($GLOBALS ['spd_vio_vehicle_id']) > 0) && isset ( $GLOBALS ['dr_gp_name'] )) {
					$report_sub_title .= ", Driver Group Name: " . $GLOBALS ['dr_gp_name'];
				} else if ((count($GLOBALS ['spd_vio_vehicle_id']) == 0) && isset ( $GLOBALS ['dr_gp_name'] )) {
					$report_sub_title .= " Driver Group Name: " . $GLOBALS ['dr_gp_name'];
				}
 
				//------Ends here------

				//------Starts from here------ This is to append the driver name's to the subtitle
				$drv_name = array();
				if(count($GLOBALS ['spd_vio_driver_id']) > 0)
				{
					$drv_count = count($GLOBALS ['spd_vio_driver_id']);
					$i = 0;
					$report_sub_title .= ", Vehicle Name: "; //. $GLOBALS ['vh_name'];
					$report_sub_title1 = "";
					foreach($GLOBALS ['spd_vio_driver_id'] as $row)
					{
						$d_id = $row['dr_link_driver_id'];
						$name = $this->speed_violations_model->getDriverName($d_id);
						array_push($drv_name, $name);
				
						if(count($GLOBALS ['spd_vio_driver_id']) == 1)
						{
							$report_sub_title1 .= $drv_name[$i];
						}
						else
						{
							if($i == $drv_count-1)
							{
								$report_sub_title1 .= $drv_name[$i];
							}
							else
							{
								$report_sub_title1 .= $drv_name[$i].", ";
							}
						}
						$i++;
					}
					$report_sub_title .= "( ".$report_sub_title1.")";
				}

				if (isset ( $GLOBALS ['vh_gp_name'] ) || isset ( $GLOBALS ['dr_gp_name'] ))
					$report_sub_title .= " )";
				
				$report_title = "Speed Violation Report ";
				if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
					$client_name = $this->common_model->get_dropdownCntName ( $GLOBALS ['spd_vio_client_id'] );
					$report_title .= "(".$client_name.")";
				}
				
				$this->session->set_userdata ( 'SESSION_DATA_1', $report_title );
				$this->session->set_userdata ( 'SESSION_DATA_2', $report_sub_title );
			}
			$this->table_pagination ( md5 ( $GLOBALS ['temp'] ) );
			
			$session_data = array();
			if ($GLOBALS ['ID'] ['sess_clientid'] == AUTOGRADE_USER) {
				$session_data["client_name"] = (null != ($this->input->post ( 'ClientName' )) ? $this->input->post ( 'ClientName' ) : null);
			} else {
				$session_data["client_name"] = $GLOBALS ['ID'] ['sess_clientid'];
			}
			$session_data["from_date"] = $GLOBALS ['spd_vio_strt_date'];
			$session_data["to_date"] = $GLOBALS ['spd_vio_end_date'];
			$session_data["vehicle_grp"] = $GLOBALS ['spd_vio_vclegroup_id'];
			$session_data["vehicles"] = $GLOBALS ['spd_vio_vehicle_id'];
			$session_data["driver_grp"] = $GLOBALS ['spd_vio_dvrgroup_id'];
			$session_data["drivers"] = $GLOBALS ['spd_vio_driver_id'];
				
			$obj = json_encode( $session_data );
				
			$this->session->set_userdata( array("speed_voilation_graph_input"=> $obj) );
			
		} else {
			$GLOBALS ['csvpdf_button'] = (null != trim ( $this->input->post ( 'clicked_btn' ) ) ? trim ( $this->input->post ( 'clicked_btn' ) ) : null);
			
			$title = $this->session->userdata ( 'SESSION_DATA_1' );
			$sub_title = $this->session->userdata ( 'SESSION_DATA_2' );
			
			$headers = array (
					array (
							"vehicle" => "Vehicle Name",
// 							"imei" => "IMEI Number",
							"driver" => "Driver",
							"from_date_time" => "From Date/Time",
							"to_date_time" => "To Date/Time",
							"avgspeed" => "Avg Speed(km/h)",
							"speedlimit" => "Speed Limit",
							"maxspeed" => "Max Speed",
							"duration" => "Duration",
							"spot" => "Spot" 
						) 
			);
			
			$GLOBALS ['spd_vltn_data_fmdb'] = $this->speed_violations_model->get_allSpeedViolations ();
 			$i = 0;
			foreach ( $GLOBALS ['spd_vltn_data_fmdb'] as $row ) {
				$headers [$i + 1] ["vehicle"] = $row ["vehicle"];
// 				$headers [$i + 1] ["imei"] = $row ["imei"];
				$headers [$i + 1] ["driver"] = $row ["driver"];
				$headers [$i + 1] ["from_date_time"] = $row ["from_date_time"];
				$headers [$i + 1] ["to_date_time"] = $row ["to_date_time"];
				$headers [$i + 1] ["avgspeed"] = $row ["avgspeed"];
				$headers [$i + 1] ["speedlimit"] = $row ["speedlimit"];
				$headers [$i + 1] ["maxspeed"] = $row ["maxspeed"];
				$headers [$i + 1] ["duration"] = $row ["duration"];
				$headers [$i + 1] ["spot"] = $row ["spot"];
				$i ++;
			}

			if ($GLOBALS ['csvpdf_button'] == 'csv') {
				$this->load->library ( 'commonfunction' );
				$this->commonfunction->csv_create ( $title, $sub_title, $headers );
			} else if ($GLOBALS ['csvpdf_button'] == 'pdf') {
				$this->load->helper ( array (
						'dompdf',
						'file' 
				) );
				
				$session_data = $this->session->userdata ('login');
				
				$title = $this->session->userdata ( 'SESSION_DATA_1' );
				$sub_title = $this->session->userdata ( 'SESSION_DATA_2' );
				
				
				// starts from here----
				$html = " <html>
							<head></head>
							<body>
							<table>
								<tr><td>
									<table>
										<tr><td><b>$title</td></td></tr>
										<tr><td>$sub_title</td><td></td></tr>
									</table>
									</td><br/>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>
										<table>
										<tr>
											<td> <img alt='' src='". base_url('assets/images/logo.png')."'/></td>
										</tr>
										</table>
									</td>
								</tr>
							</table>
							<hr>
							<table> ";
								$table_data="";
								$i = 0;
								foreach ( $headers as $header ) {
									if ($i == 1)
									{
// 										$table_data .= "<tr width:1000%;><td><hr></td></tr>";
									}
									else{
										log_message("debug", "checking the condition...");
									}
									$table_data .= " <tr><td>" . $header ['vehicle'] . "</td><td>"
													. $header ['driver'] . "</td><td>" . $header ['from_date_time'] . "</td><td>"
													. $header ['to_date_time'] . "</td><td>" 
													. $header ['avgspeed'] . "</td><td>" . $header ['speedlimit']. "</td><td>" 
													. $header['maxspeed'] . "</td><td>". $header['duration']. "</td><td>" . $header ['spot'] . "</td></tr>";
									$i++;
								}
								$html_1 = "</table>
											<hr>
											<table>&nbsp;&nbsp;&nbsp;&nbsp;
												<tr>
													<td><b>Prepared by </b> : ".$session_data ['sess_user_name']."</td>
													<td></td>
													<td style='padding-left:390px;'><b>".date ( 'Y-m-d' )."</b></td>
												</tr>
											</table>
							</body>
							</html>";
					
					$html = $html.$table_data.$html_1;
				
				// ends here-----------
				
				pdf_create ( $html, str_replace(' ', '_', $title) . '_' . date ( 'Ymd' ) );
			}
		}
	}
	
	/*
	 * This function is to validate the client dropdown it is only for the autograde users
	 */
	public function check_cnt_id($cnt_id) {
		if (empty ( $cnt_id )) {
			$this->form_validation->set_message ( 'check_cnt_id', '%s required' );
			return FALSE;
		}
		$GLOBALS ['spd_vio_client_id'] = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
		return true;
	}
	
	/*
	 * Function is to validate the start date field
	 */
	public function check_strt_dt($start_date) {
		if (empty ( $start_date )) {
			$this->form_validation->set_message ( 'check_strt_dt', '%s required' );
			return FALSE;
		}
		$GLOBALS ['spd_vio_strt_date'] = (null != trim ( $this->input->post ( 'SpeedVioStartDate' ) ) ? trim ( $this->input->post ( 'SpeedVioStartDate' ) ) : null);
		return true;
	}
	
	/*
	 * Function is to validate the end date field
	 */
	public function check_end_dt($end_date) {
		if (empty ( $end_date )) {
			$this->form_validation->set_message ( 'check_end_dt', '%s required' );
			return FALSE;
		} else if (strtotime ( $GLOBALS ['spd_vio_strt_date'] ) > strtotime ( $GLOBALS ['spd_vio_end_date'] )) 
		{
 			$GLOBALS ['outcome_with_db_check'] = "To date should be greater than From date";
			$this->form_validation->set_message ( 'check_end_dt', '' );
			return FALSE;
		}
		$GLOBALS ['spd_vio_end_date'] = (null != trim ( $this->input->post ( 'SpeedVioEndDate' ) ) ? trim ( $this->input->post ( 'SpeedVioEndDate' ) ) : null);
		return true;
	}
	
	/*
	 * this function is used to fill the drop down list box in
	 * view depending upon the device type selection without refresh the page.
	 */
	public function get_driver($drivergpid)
	{
		$driver = $this->speed_violations_model->get_allDriver ( $drivergpid );
		$output = "";
		if ($driver != null) {
			$output = "[";
			foreach ( $driver as $row ) {
				if ($output != "[") {
					$output .= ",";
				}
				$output .= '{"driver_id":"' . $row ['driver_id'] . '",';
				$output .= '"driver_name":"' . $row ['driver_name'] . '"}';
			}
			$output .= "]";
		}
		echo ($output);
	}
	
	/*
	 * this function is used to fill the drop down list box in
	 * view depending upon the vehicle group selection without refresh the page.
	 */
	public function get_vehicle_gp($vhGp) {
		$vehicle = $this->speed_violations_model->get_allVehicles ( $vhGp );
		foreach ( $vehicle as $row ) {
		}
		$output = "";
		if ($vehicle != null) {
			$output = "[";
			foreach ( $vehicle as $row ) {
				if ($output != "[") {
					$output .= ",";
				}
				$output .= '{"vehicle_id":"' . $row ['vehicle_id'] . '",';
				$output .= '"vehicle_regnumber":"' . $row ['vehicle_regnumber'] . '"}';
			}
			$output .= "]";
		}
		echo ($output);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id = null) {
		if ($cl_id != null) {
			$GLOBALS ['spd_vio_client_id'] = $cl_id;
 		}
		$cntId = (null != trim ( $this->input->post ( 'ClientName' ) ) ? trim ( $this->input->post ( 'ClientName' ) ) : null);
		$GLOBALS ['clientList'] = $this->speed_violations_model->get_allClients ();
		if($cntId == null)
			$cntId = $GLOBALS ['ID'] ['sess_clientid'];

		$GLOBALS ['vehicleGroupList'] = $this->speed_violations_model->get_allVehicleGroups ( $cntId );
		$GLOBALS ['vehicleList'] = $this->speed_violations_model->get_allVehicles ( $GLOBALS ['spd_vio_vclegroup_id'] );
		$GLOBALS ['driverGroupList'] = $this->speed_violations_model->get_allDriverGroup ( $GLOBALS ['ID'] ['sess_clientid'], $GLOBALS ['spd_vio_client_id'] );
		$GLOBALS ['driverList'] = $this->speed_violations_model->get_allDriver ( $GLOBALS ['spd_vio_dvrgroup_id'] );
		$this->display ();
	}

	/*
	 * This function is used to render the view
	 */
	private function display() {
		$this->common_model->menu_display ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'speed_violations_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}

	/*
	 * This function is used to show all the speed violation point in map for the given user id
	 */
	public function map_view() {
		$this->common_model->menu_display ();
		// json_encode($this->speed_violations_model->get_allSpeedViolations());
		$GLOBALS ['gpsdata'] = json_encode ( $this->speed_violations_model->get_allSpeedViolations_map () );
		$this->load->view ( 'speed_violation_map_view', $GLOBALS );
	}
	
	public function get_speed_violation_graph() {
	
		if( $this->input->is_ajax_request() ) {
			
			$this->load->library ( 'commonfunction' );
			echo $this->commonfunction->draw_bar_graph();
	
		} else {
				
			exit();
				
		}
	}
	
	public function Speed_violation_graph() {
	
		$this->common_model->menu_display ();
		$this->load->library ( 'commonfunction' );
		$data = $this->speed_violations_model->speed_violation_graph_model( $this->session->userdata("speed_voilation_graph_input") );
		if( sizeof( $data["data"] )!=0 )
			$GLOBALS ['bar_chart'] = $this->commonfunction->draw_bar_graph( $data );
		else 
			$GLOBALS ['bar_chart'] = "No data found";
		$this->common_model->menu_display ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'speed_violation_graph_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}
}

<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Tracking_Data_ctrl extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );
		$this->common_model->check_session ();
		$this->load->model ( 'tracking_data_model' );
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$this->load->library('fpdf');
		$GLOBALS ['vehicle_id'] = null;
		$GLOBALS ['vehicle_name'] = null;
		$GLOBALS ['result'] = null;
		$GLOBALS ['track_strt_date'] = null;
		$GLOBALS ['track_end_date'] = null;
		$GLOBALS['pdf_vehicle_name']= null;
		$GLOBALS['pdf_imei'] = null;
		$this->load->helper ( array (
				'form',
				'url' 
		) );
		$GLOBALS ['outcome'] = null;
		$GLOBALS ['outcome_with_db_check'] = null;
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['sessUserID'] = $GLOBALS ['ID'] ['sess_userid'];
		$GLOBALS ['sessUserName'] = $GLOBALS ['ID'] ['sess_user_name'];
		$GLOBALS ['no_data'] = false;
		$GLOBALS ['active'] = NOT_ACTIVE;
		$GLOBALS ['go_button'] = null;
		$GLOBALS ['vehicleGroupList'] = $this->tracking_data_model->get_all_vhGp ( $GLOBALS ['sessClientID'], $GLOBALS ['sessClientID'], $GLOBALS ['sessUserID'] );
		$row = $GLOBALS ['vehicleGroupList'];
		$GLOBALS ['vehicleList'] = null;
		if ($GLOBALS ['vehicleGroupList'] != null) {
			$GLOBALS ['vehicleList'] = $this->tracking_data_model->get_all_vhGp_vehicle ( $row [0] ['vehicle_group_id'] );
		}
	}
	public function index() {
		$this->display ();
	}
	/*
	 * This function is used to validate and add the new user in database.
	 */
	public function report_generation() // ($vh_id,$_fdate,$_tdate)
{
		$l_track_strt_date = (null != trim ( $this->input->post ( 'TrackStartDate' ) ) ? trim ( $this->input->post ( 'TrackStartDate' ) ) : null);
		$l_track_end_date = (null != trim ( $this->input->post ( 'TrackEndDate' ) ) ? trim ( $this->input->post ( 'TrackEndDate' ) ) : null);
		$vh_id = (null != trim ( $this->input->post ( 'vehicle' ) ) ? trim ( $this->input->post ( 'vehicle' ) ) : null);
		$fdate = new DateTime ( $l_track_strt_date );
		$tdate = new DateTime ( $l_track_end_date );
		$GLOBALS ['track_strt_date'] = $fdate->format ( 'Y-m-d' );
		$GLOBALS ['track_end_date'] = $tdate->format ( 'Y-m-d' );
		$GLOBALS ['active'] = (null != trim ( $this->input->post ( 'Spot' ) ) ? trim ( $this->input->post ( 'Spot' ) ) : NOT_ACTIVE);
		$GLOBALS ['vh_gp_name'] = (null != trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) ? trim ( $this->input->post ( 'hiddenVehicleGroup' ) ) : null);
		$GLOBALS ['vh_name'] = (null != trim ( $this->input->post ( 'hiddenVehicle' ) ) ? trim ( $this->input->post ( 'hiddenVehicle' ) ) : null);
		$GLOBALS ['client_id'] = (null != ($this->input->post ( 'ClientID' )) ? $this->input->post ( 'ClientID' ) : null);
		$GLOBALS ['vehicle_grp'] = (null != ($this->input->post ( 'vehicleGroup' )) ? $this->input->post ( 'vehicleGroup' ) : null);
		$GLOBALS ['go_button'] = (null != trim ( $this->input->post ( 'hiddenGo' ) ) ? trim ( $this->input->post ( 'hiddenGo' ) ) : null);
		
		//log_message ( "debug", "ABD In report_generation" );
		// $this->form_validation->set_message('required', '%s required');
		
		$this->form_validation->set_rules ( 'ClientID', 'Client','callback_check_client' );
		// Validating dates
		$this->form_validation->set_rules ( 'TrackStartDate', 'Start Date', 'callback_check_strt_dt' );
		$this->form_validation->set_rules ( 'TrackEndDate', 'End Date', 'callback_check_end_dt' );
		// Validating Vehicle Group and Vehicle
		$this->form_validation->set_rules ( 'vehicleGroup', 'Vehicle Group','callback_check_vehiclegp' );
		$this->form_validation->set_rules ( 'vehicle', 'Vehicle','callback_check_vehicle' );
		
		//
		if ($this->form_validation->run () == FALSE) { // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
			//log_message ( "debug", "......form validation....false...  " );
			$this->display ();
		} else {
			//log_message ( "debug", "inside controller......" );
			$GLOBALS ['get_gridDetails'] = $this->tracking_data_model->get_gps_data ( $vh_id, $GLOBALS ['track_strt_date'], $GLOBALS ['track_end_date'] );
			$GLOBALS ['no_data'] = true;
			$arr = $GLOBALS ['get_gridDetails'];
			if ($GLOBALS ['active'] == 1) {
				$spot_add = array ();
				$i = 0;
				if(!empty($arr))
				{
					foreach ( $arr as $arr1 )
					{
						array_push ( $spot_add, $this->common_model->getPlaceName ( $arr1 ['gps_latitude'], $arr1 ['gps_longitude'] ) );
						$arr [$i] ['spot'] = $spot_add [$i];
						$i ++;
					}
				}
				$GLOBALS ['get_gridDetails'] = $arr;
				$query = $arr;
			} else {
				$nospot = array ();
				if ($arr != null) {
					foreach ( $arr as $arr1 ) {
						$arr1 ['spot'] = round ( $arr1 ['gps_latitude'], 4 ) . " , " . round ( $arr1 ['gps_longitude'], 4 );
						array_push ( $nospot, $arr1 );
					}
				}
				$GLOBALS ['get_gridDetails'] = $nospot;
				$query = $nospot;
			}
			// csv pdf
			//log_message ( "debug", "-------inside csv-------" );
			$this->tracking_data_model->InsertOrdelete_vts_tmp_report_tracking ( $query, 'delete', $GLOBALS ['active'] );
			$this->tracking_data_model->InsertOrdelete_vts_tmp_report_tracking ( $query, "insert", $GLOBALS ['active'] );
			
			$report_sub_title = "For the period from " . $GLOBALS ['track_strt_date'] . " to " . $GLOBALS ['track_end_date'];
			$test = false;
			if (isset ( $GLOBALS ['vh_gp_name'] ) || isset ( $GLOBALS ['vh_name'] ))
				$report_sub_title .= " ( ";
			if (isset ( $GLOBALS ['vh_gp_name'] ) && ! isset ( $GLOBALS ['vh_name'] )) {
				$report_sub_title .= "Vehicle Group Name : " . $GLOBALS ['vh_gp_name'];
				$report_sub_title .= ",";
			} else if (isset ( $GLOBALS ['vh_gp_name'] )) {
				$report_sub_title .= "Vehicle Group Name : " . $GLOBALS ['vh_gp_name'];
			}
			if (isset ( $GLOBALS ['vh_name'] ))
				$report_sub_title .= ", Vehicle Name : " . $GLOBALS ['vh_name'];
			if (isset ( $GLOBALS ['vh_gp_name'] ) || isset ( $GLOBALS ['vh_name'] ))
				$report_sub_title .= " )";
			$report_title = "Tracking Report ";
			$this->session->set_userdata ( 'ITEM_1', $report_title );
			$this->session->set_userdata ( 'ITEM_3', $report_sub_title );
			$this->display ();
		}
		// }
		$GLOBALS ['csvpdf_button'] = (null != trim ( $this->input->post ( 'clicked_btn' ) ) ? trim ( $this->input->post ( 'clicked_btn' ) ) : null);
		// log_message("debug", "valueee=== ".$GLOBALS['csvpdf_button']);
		$title = $this->session->userdata ( 'ITEM_1' );
		$sub_title = $this->session->userdata ( 'ITEM_3' );
		$headers = array (
				array (
						"vehicle" => "Vehicle Name",
						"gps_runningno" => "GPS_Running NO",
						"imei" => "IMEI NO",
						"date_time" => "Date/Time",
						"speed" => "Speed(km/h)",
						"status" => "Status",
						"driver_name" => "Driver",
						"spot" => "Spot" 
				) 
		);
		
		$GLOBALS ['tracking_data_fmdb'] = $this->tracking_data_model->get_alltrackingdata ();
		// log_message("debug", "ABD ".print_r($GLOBALS['tracking_data_fmdb'], true));
		$i = 0;
		foreach ( $GLOBALS ['tracking_data_fmdb'] as $row ) {
			$headers [$i + 1] ["vehicle"] = $row ["vehicle"];
			$headers [$i + 1] ["gps_runningno"] = $row ["gps_runningno"];
			$headers [$i + 1] ["imei"] = $row ["imei"];
			$headers [$i + 1] ["date_time"] = $row ["date_time"];
			$headers [$i + 1] ["speed"] = $row ["speed"];
			$headers [$i + 1] ["status"] = $row ["status"];
			$headers [$i + 1] ["driver_name"] = $row ["driver"];
			$headers [$i + 1] ["spot"] = $row ["spot"];
			$i ++;
		}
		if ($GLOBALS ['csvpdf_button'] == 'csv') {
			// $this->csv_create_ctrl ($report_title, $sub_title, $headers);
			$this->load->library ( 'commonfunction' );
			$this->commonfunction->csv_create ( $title, $sub_title, $headers );
		} else if ($GLOBALS ['csvpdf_button'] == 'pdf') {
			
			/*
			* Library: fpdf
			* Author: Naveenkumar T
			* Parmeter: Tracking data from database
			* Description: To generate Tracking data reoprts as PDF, following method would be used.
			*/
			$this->tracking_data_pdf_gen($GLOBALS ['tracking_data_fmdb']);
			
			/*Commented by Naveenkumar T, Since the below functionality written in DOMPdf, is atking long time to generate HTML PDF*/
			
			/* set_time_limit ( 600 );
			$this->load->helper ( array (
					'dompdf',
					'file' 
			) );
			$title = $this->session->userdata ( 'ITEM_1' );
			$sub_title = $this->session->userdata ( 'ITEM_3' );
					$html = " <html>
				<head></head>
				<body>
				<table>
				<tr><td>
				<table>
				<tr><td><b>$title</td></td></tr>
				<tr><td>$sub_title</td><td></td></tr>
				</table>
				</td><br/>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>
				<table>
				<tr>
				<td> <img alt='' src='" . base_url ( 'assets/images/logo.png' ) . "'/></td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				<hr>
				<table> ";
			$table_data = "";
			$i = 0;
			foreach ( $headers as $header ) {
				if ($i == 1) {
					$table_data .= "<tr width:1000%;><td><hr><td><hr><td><hr><td><hr><td><hr><td><hr><td><hr><td><hr></td></td></td></td></td></td></td></td></tr>";
				} else {
					
				}
			    $table_data .= " <tr><td>" . $header ['vehicle'] . "</td><td>" . $header ['gps_runningno'] . "</td><td>" . $header ['imei'] . "</td><td>" . $header ['date_time'] . "</td><td>" . $header ['speed'] . "</td><td>" . $header ['status'] . "</td><td>" . $header ['driver_name'] . "</td><td>" . $header ['spot'] . "</td></tr>";
				$i ++;
			}
			$html_1 = "</table>
				<hr> <table>&nbsp;&nbsp;&nbsp;&nbsp;
						<tr>
							<td><b>Prepared by </b> : " . $GLOBALS ['sessUserName'] . "</td>
							<td></td>
							<td style='padding-left:390px;'><b>" . date ( 'd/m/Y' ) . "</b></td>
								</tr>
										</table>
							</body>
							</html>";
			
			$html = $html . $table_data . $html_1;
			
						
			pdf_create ( $html, str_replace ( ' ', '_', $title ) . '_' . date ( 'Ymd' ) ); */
			
			// $html = $this->load->view ( 'speed_violations_view', $GLOBALS );
			// pdf_create($buffer, 'Tracking Data');
			
			// log_message("debug", "pdf---");
		}
	}
	public function check_client($client) {
		//log_message ( "debug", "----inside error validation client" );
		if (empty ( $client )) {
			$this->form_validation->set_message ( 'check_client', '%s required.' );
			return FALSE;
		}
		return true;
	}
	public function check_strt_dt($start_date) {
		if (empty ( $start_date )) {
			$this->form_validation->set_message ( 'check_strt_dt', '%s required.' );
			return FALSE;
		}
		//$l_track_strt_date = (null != trim ( $this->input->post ( 'TrackStartDate' ) ) ? trim ( $this->input->post ( 'TrackStartDate' ) ) : null);
		
		return true;
	}
	public function check_end_dt($end_date) {
		
		// $start_date = $GLOBALS['spd_vio_strt_date'];
		// $end_date = $GLOBALS['spd_vio_end_date'];
		if (empty ( $end_date )) {
			$this->form_validation->set_message ( 'check_end_dt', '%s required.' );
			return FALSE;
		}
		//$l_track_end_date = (null != trim ( $this->input->post ( 'TrackEndDate' ) ) ? trim ( $this->input->post ( 'TrackEndDate' ) ) : null);
		return true;
	}
	/*check_vehicle
	 * function to validate error message for vehicle grp
	 */
	public function check_vehiclegp($vh_gp) {
		//log_message ( "debug", "----inside error validation client" );
		if (empty ( $vh_gp )) {
			$this->form_validation->set_message ( 'check_vehiclegp', '%s required.' );
			return FALSE;
		}
		return true;
	}
	/*
	 * function to validate error message for vehicle 
	 */
	public function check_vehicle($vh) {
		//log_message ( "debug", "----inside check_vehicle " );
		if (empty ( $vh )) {
			$this->form_validation->set_message ( 'check_vehicle', '%s required.' );
			return FALSE;
		}
		return true;
	}
	
	/*
	 * This function is used to get the vehicle group for the selected client and return back the list of
	 * vehicle group in JSON format with out refresh the page.
	 */
	public function get_client_vhgp($client) {
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['sessUserID'] = $GLOBALS ['ID'] ['sess_userid'];
		$client_vh_gps = $this->tracking_data_model->get_all_vhGp ( $client, $GLOBALS ['sessClientID'], $GLOBALS ['sessUserID'] );
		$output = "";
		if ($client_vh_gps != null) {
			$output = "[";
			foreach ( $client_vh_gps as $row ) {
				if ($output != "[") {
					$output .= ",";
				}
				$output .= '{"vh_gp_id":"' . $row ['vehicle_group_id'] . '",';
				$output .= '"vh_gp_name":"' . $row ['vehicle_group'] . '"}';
			}
			$output .= "]";
		}
		echo ($output);
	}
	
	/*
	 * This function is used to get the vehicle for the selected vehicle group and return back the list of
	 * vehicles in JSON format with out refresh the page.
	 */
	public function get_vhl_grp_vehicle($vhGrp = null) {
		$output = "";
		if ($vhGrp != null) {
			$vhl_grp_vehicle = $this->tracking_data_model->get_all_vhGp_vehicle ( $vhGrp );
			$output = "";
			if ($vhl_grp_vehicle != null) {
				$output = "[";
				foreach ( $vhl_grp_vehicle as $row ) {
					if ($output != "[") {
						$output .= ",";
					}
					$output .= '{"imei_no":"' . $row ['vehicle_id'] . '",';
					$output .= '"vh_name":"' . $row ['vehicle_regnumber'] . '"}';
				}
				$output .= "]";
			}
		}
		echo ($output);
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display() {
		$this->common_model->menu_display ();
		$GLOBALS ['clientList'] = $this->tracking_data_model->get_allClients ();
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'tracking_data_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}
	
	
	public function tracking_data_pdf_gen( $data = NULL ) {
		$GLOBALS['pdf_vehicle_name'] = $data[0]['vehicle'];
		$GLOBALS['pdf_imei'] = $data[0]['imei'];
		
		$this->fpdf->enableheader = 'custom_header';
		$this->fpdf->AddPage();
		$this->fpdf->enablefooter = 'custom_footer';
		
		// Column headings
		$this->fpdf->footer_data = "Vehicle: MaSociete, IMEI: MonAdresse";
		$this->fpdf->SetAutoPageBreak(true, 15);
	
		$this->fpdf->AliasNbPages();
		
		// Cell Width
		$w = array(30, 30, 12, 10, 35, 60);
		
		// Color and font restoration
		$this->fpdf->SetFillColor(255,255,255);
		$this->fpdf->SetTextColor(0);
		$this->fpdf->SetFont('Arial','',7);
		
		// Data
		$cell_h = $GLOBALS ['active']==1?15:6;
		foreach($data as $key=>$row) {
			
			$this->fpdf->Cell($w[0],$cell_h,$row["gps_runningno"],1,0,'L');
			$this->fpdf->Cell($w[1],$cell_h,$row["date_time"],1,0,'L');
			$this->fpdf->Cell($w[2],$cell_h,$row["speed"],1,0,'L');
			$this->fpdf->Cell($w[3],$cell_h,$row["status"],1,0,'L');
			$this->fpdf->Cell($w[4],$cell_h,$row["driver"],1,0,'L');
			$location = $row["spot"];
			if( $GLOBALS ['active']==1 ) {
				$this->fpdf->word_wrap($location, 50);
				$this->fpdf->MultiCell($w[5],5,$location,1,0,'L');
				$this->fpdf->Ln(0);
			} else {
				$this->fpdf->Cell($w[5],$cell_h,$location,1,0,'L');
				$this->fpdf->Ln();
			}
		}
		// Closing line
		$this->fpdf->Cell(array_sum($w),0,'','T');
		
		$this->fpdf->lastPage = $this->fpdf->page;
		$file_name = strtolower('tracking_report_'.$GLOBALS['pdf_vehicle_name'].'_'.date('Ymd'));
		echo $this->fpdf->Output($file_name.'.pdf','D');
	}
	
	// Company
	public function addHeader( $head, $data ) {
		$x1 = 10;
		$y1 = 8;
		//Positionnement en bas
		$this->fpdf->SetXY( $x1, $y1 );
		$this->fpdf->SetFont('Arial','B',10);
		$length = $this->fpdf->GetStringWidth( $head );
		$this->fpdf->Cell(0,0,'Tracking Report',0,1,'C');
		$this->fpdf->Cell( $length, 2, $head);
		$this->fpdf->SetXY( $x1, $y1 + 4 );
		$this->fpdf->SetFont('Arial','',9);
		$length = $this->fpdf->GetStringWidth( $data );
		//Coordonnées de la société
		$lignes = $this->fpdf->sizeOfText( $data, $length) ;
		$this->fpdf->MultiCell($length, 4, $data);
	}
	
	function custom_header($pdf) {
		
		$date = strtotime($GLOBALS ['track_strt_date']) == strtotime($GLOBALS ['track_end_date']) ? $GLOBALS ['track_strt_date']: $GLOBALS ['track_strt_date']." to ".$GLOBALS ['track_end_date'];
		$head = "Vehicle: ".$GLOBALS['pdf_vehicle_name'];
		$sub_head = "IMEI: ".$GLOBALS['pdf_imei']."\n"."Date: ".$date;
		if($this->fpdf->page == 1) {
			$this->addHeader($head,$sub_head);
			// Logo
			$this->fpdf->Image('assets/images/new_logo.jpg',150,0,40);
		}
		$spot = ($GLOBALS ['active']==1) ? "Spot":"Lat, Lang";
		$header = array ("GPS Running No.", "Date and Time","Speed (km/h)","Status","Driver", $spot);
		
		 // Colors, line width and bold font
		$pdf->SetFillColor(255,0,0);
		$pdf->SetTextColor(255);
		$pdf->SetDrawColor(128,0,0);
		$pdf->SetLineWidth(.3);
		$pdf->SetFont('Arial','B',8);
		
		$x = $pdf->GetX();
		$y = $pdf->GetY();
		$y = $this->fpdf->page == 1 ? $y+10: $y;
		// Header
		$w = array(30, 30, 12, 10, 35, 60);
		$h = 14;
		
		$pdf->SetXY($x, $y);
		$pdf->Cell($w[0],$h,$header[0],1,0,'C',true);
		$pdf->Cell($w[1],$h,$header[1],1,0,'C',true);
		$text = $header[2];
		$this->fpdf->word_wrap($text, 20);
		$pdf->MultiCell($w[2],7,$text,1,0,'C',true);
		$pdf->SetXY($x+($w[0]+$w[1]+$w[2]), $y);
		$pdf->Cell($w[3],$h,$header[3],1,0,'C',true);
		$pdf->Cell($w[4],$h,$header[4],1,0,'C',true);
		$pdf->Cell($w[5],$h,$header[5],1,0,'C',true);
		$pdf->Ln();
	}
	
	function custom_footer($pdf){
		$pdf->SetY(-10);
		$pdf->SetFont('Arial','I',8);
				
		$pdf->Cell(10,5,"Vehicle: ".$GLOBALS['pdf_vehicle_name'].", IMEI: ".$GLOBALS['pdf_imei'],'T',0,'L');
		$pdf->Cell(0,12,'Page '.$pdf->PageNo().'/{nb}','T',0,'C');
		$pdf->SetFont('Arial','I',7);
		$pdf->Cell(0,5,'Prepared By & Date: '.$GLOBALS ['sessUserName'].' & '.date("d/m/Y H:i:s"),'T',0,'R');
	}
}
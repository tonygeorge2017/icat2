<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_management_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('user_management_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['checkLoginType']=null;
		$GLOBALS['checkID']=null;
		$GLOBALS['lableName']=null;
		$GLOBALS['userID']=null;
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		$GLOBALS['sessUserLoginType']=$GLOBALS['sessionData']['sess_user_type'];
		$GLOBALS['sessParentID']=$GLOBALS['sessionData']['sess_parentid'];
		$GLOBALS['loginUserTypeList']=array();
		$GLOBALS['loginUserTypeID']=null;		
		$GLOBALS['anyID']=null;
		$GLOBALS['userEmail']=null;
		$GLOBALS['userFullName']=null;
		$GLOBALS['mobileNo']=null;
		$GLOBALS['userRoleID']=null;
		$GLOBALS['selectedVehicleGroup']=null;
		$GLOBALS['vehicleGroupList']=null;
		$GLOBALS['driverGroupList']=null;
		$GLOBALS['timeZoneID']=null;
		$GLOBALS['timeZoneList']=null;
		$GLOBALS['distID']=null;
		$GLOBALS['dealerID']=null;
		$GLOBALS['parentID']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['userList']=array();
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;		
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$GLOBALS['loginUserTypeID']=$GLOBALS['sessUserLoginType'];
		if($GLOBALS['loginUserTypeID']==PARENT_USER)
			$GLOBALS['timeZoneID']=$this->user_management_model->get_ClientsTimeDiff(md5($GLOBALS['sessParentID']),md5($GLOBALS['loginUserTypeID']));
		else
			$GLOBALS['timeZoneID']=$this->user_management_model->get_ClientsTimeDiff(md5($GLOBALS['sessClientID']),md5($GLOBALS['loginUserTypeID']));
		$this->table_pagination(md5($GLOBALS['sessClientID']),md5($GLOBALS['loginUserTypeID']));
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function user_validation()
	{
		$GLOBALS['checkLoginType']=(null!=($this->input->post('CheckIDLogin'))?$this->input->post('CheckIDLogin'):null);
		$GLOBALS['checkID']=(null!=($this->input->post('CheckID'))?$this->input->post('CheckID'):null);
		$GLOBALS['userID']=(null!=($this->input->post('UserID'))?$this->input->post('UserID'):null);
		$GLOBALS['loginUserTypeID']=(null!=($this->input->post('LoginUserTypeID'))?$this->input->post('LoginUserTypeID'):null);
		$GLOBALS['anyID']=(null!=($this->input->post('ClientID'))?trim($this->input->post('ClientID')):null);
		$GLOBALS['userEmail']=(null!=($this->input->post('UserEmail'))?trim($this->input->post('UserEmail')):null);
		$GLOBALS['userFullName']=(null!=($this->input->post('UserFullName'))?trim($this->input->post('UserFullName')):null);
		$GLOBALS['mobileNo']=(null!=($this->input->post('MobileNo'))?trim($this->input->post('MobileNo')):null);
		$GLOBALS['userRoleID']=(null!=($this->input->post('UserRole'))?$this->input->post('UserRole'):null);
		$GLOBALS['selectedVehicleGroup']=(null!=($this->input->post('VehicleGroupID'))?$this->input->post('VehicleGroupID'):null);
		$GLOBALS['selectedDriverGroup']=(null!=($this->input->post('DriverGroupID'))?$this->input->post('DriverGroupID'):null);
		$GLOBALS['timeZoneID']=(null!=($this->input->post('TimeZoneID'))?$this->input->post('TimeZoneID'):null);
		$GLOBALS['active']=(null!=($this->input->post('Active'))?$this->input->post('Active'):NOT_ACTIVE);
		if(empty($GLOBALS['checkID']) && empty($GLOBALS['checkLoginType']))
		{
			$this->form_validation->set_message('required', '%s required.');
			$this->form_validation->set_rules('ClientID', 'Client', 'required');
			//set rule for User Email field.
			$this->form_validation->set_rules('UserEmail', 'Email', 'callback_check_email');
			$this->form_validation->set_rules('UserFullName', 'User Full Name', 'required');
			$this->form_validation->set_rules('MobileNo', 'Mobile No', 'required');
			$this->form_validation->set_rules('TimeZoneID', 'Time Zone', 'required');
			if($GLOBALS['loginUserTypeID']==AUTOGRADE_USER || $GLOBALS['loginUserTypeID']==INDIVIDUAL_CLIENT || $GLOBALS['loginUserTypeID']==OHTER_CLIENT_USER )
			{
				$this->form_validation->set_rules('VehicleGroupID', 'Vehicle Group', 'callback_check_vh_gp');
			}
			//if any of the form rule is failed, then it show the error msg in view.
			//Else update the user table.
			if ($this->form_validation->run() == FALSE) 
			{
				$row=$GLOBALS['selectedVehicleGroup'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['selectedVehicleGroup'][$i]=array('vh_gp_link_vehicle_group_id'=>$_row);
						$i++;
					}
				}
				$row=$GLOBALS['selectedDriverGroup'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['selectedDriverGroup'][$i]=array('dr_gp_link_driver_group_id'=>$_row);
						$i++;
					}
				}
				$this->table_pagination(md5($GLOBALS['anyID']),md5($GLOBALS['loginUserTypeID']));
			}
			else
			{
				if($GLOBALS['loginUserTypeID']==AUTOGRADE_USER || $GLOBALS['loginUserTypeID']==INDIVIDUAL_CLIENT || $GLOBALS['loginUserTypeID']==OHTER_CLIENT_USER )
				{
					$data['user_client_id']=$GLOBALS['anyID'];
					$row=$this->user_management_model->find_MoreDetails(" client_id='".$GLOBALS['anyID']."'");
					$data['user_dealer_id']=$row[0]['client_dealer_id'];
					$data['user_distributor_id']=$row[0]['dealer_distributor_id'];
				}
				elseif($GLOBALS['loginUserTypeID']==DISTRIBUTOR_USER )				
					$data['user_distributor_id']=$GLOBALS['anyID'];
				elseif($GLOBALS['loginUserTypeID']==DEALER_USER )
				{
					$data['user_dealer_id']=$GLOBALS['anyID'];
					$row=$this->user_management_model->find_MoreDetails(" dealer_id='".$GLOBALS['anyID']."'");
					$data['user_distributor_id']=$row[0]['dealer_distributor_id'];
				}
				elseif($GLOBALS['loginUserTypeID']==PARENT_USER )
				{
					$data['user_parent_id']=$GLOBALS['anyID'];
					$row=$this->user_management_model->find_MoreDetails(" parent_id='".$GLOBALS['anyID']."'");
					$data['user_client_id']=$row[0]['client_id'];
					$data['user_dealer_id']=$row[0]['client_dealer_id'];
					$data['user_distributor_id']=$row[0]['dealer_distributor_id'];
				}
				$data['user_type_id']=$GLOBALS['loginUserTypeID'];
				$data['user_email']=$GLOBALS['userEmail'];
				$data['user_user_name']=$GLOBALS['userFullName'];
				$data['user_mobile']=$GLOBALS['mobileNo'];
				$data['user_time_zone_id']=$GLOBALS['timeZoneID'];
				$data['user_is_admin']=	$GLOBALS['userRoleID'];
				$data['user_is_active']=$GLOBALS['active'];
				if($GLOBALS['userID']!=null)	//edit
				{
					$this->user_management_model->delete_userVehicleGroupLink($GLOBALS['userID']);
					$this->user_management_model->Add_userVehicleGroupLink($GLOBALS['userID'],$GLOBALS['selectedVehicleGroup']);
					$this->user_management_model->delete_userDriverGroupLink($GLOBALS['userID']);
					$this->user_management_model->Add_userDriverGroupLink($GLOBALS['userID'],$GLOBALS['selectedDriverGroup']);
					if($this->user_management_model->InsertOrUpdate_user($GLOBALS['userID'],$data,'edit'))
					{
						$GLOBALS['outcome']='<div style="color: green;">Update Succeed.</div>';
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], USERS ,	"Update User:"."  User ID=".trim($GLOBALS['userID'])." Client ID=".trim($GLOBALS['anyID']).", Email=".trim($GLOBALS['userEmail']).", Full name=".trim($GLOBALS['userFullName']));
					}
					else
					{
						$GLOBALS['outcome']='<div style="color: red;">Not Update.</div>';
					}
				}
				else						//insert
				{
					$data['user_password']=mt_rand();
					if($this->user_management_model->InsertOrUpdate_user(null,$data,'insert'))
					{
						$id=$this->user_management_model->get_userList(null,null,null,null,null,$GLOBALS['userEmail']);
						if($id!=null)
						{
							$this->user_management_model->Add_userVehicleGroupLink($id,$GLOBALS['selectedVehicleGroup']);
							$this->user_management_model->Add_userDriverGroupLink($id,$GLOBALS['selectedDriverGroup']);
						}
						$GLOBALS['outcome']='<div style="color: green;">New User Created Successfully.</div>';
						$s_msg='Dear '.$data['user_user_name'].',<br><br>&nbsp;&nbsp; Your email has been registered as a New Login in the Autograde T.A.N.K. System.<br>&nbsp;&nbsp;Please click the below link to set your new password and to start using the system :<br><br>&nbsp;&nbsp;<a href='.base_url("index.php/change_password_ctrl/view/".md5($id)).'>Click Here</a><br><br>Thanks & Regards,<br>Autograde T.A.N.K. Support.';
						$this->load->library ( 'commonfunction' );
						$this->commonfunction->send_mail($GLOBALS['userEmail'],$GLOBALS['userFullName'],CC_TO,BCC_TO,'Vehicle Tracking System : New User Registered',$s_msg,REPLY_TO,'VTS Support');
					}
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], USERS,
							"Create New User:"." User Name=".trim($GLOBALS['anyID']).", Email=".trim($GLOBALS['userEmail']).", Full name=".trim($GLOBALS['userFullName']));
				}
				$this->clear_all();
				$this->table_pagination(md5($GLOBALS['anyID']),md5($GLOBALS['loginUserTypeID']));
			}
		}		
		elseif(!empty($GLOBALS['checkLoginType']))
		{
			$GLOBALS['loginUserTypeID']=$GLOBALS['checkLoginType'];		
			$this->clear_all();
			$client=null;
			$row=$this->user_management_model->get_allClients(md5($GLOBALS['checkLoginType']));
			if($row!=null)
				$client=$row[0]['value'];
			$GLOBALS['timeZoneID']=$this->user_management_model->get_ClientsTimeDiff(md5($client), md5($GLOBALS['loginUserTypeID']));
			//log_message('debug','****Client***'.$client);
			$this->table_pagination(md5($client),md5($GLOBALS['loginUserTypeID']));
		}
		elseif(!empty($GLOBALS['checkID']))
		{
			$GLOBALS['anyID']=$GLOBALS['checkID'];
			$GLOBALS['timeZoneID']=$this->user_management_model->get_ClientsTimeDiff(md5($GLOBALS['anyID']), md5($GLOBALS['loginUserTypeID']));
			$this->clear_all();
			$this->table_pagination(md5($GLOBALS['anyID']),md5($GLOBALS['loginUserTypeID']));
		}
	}
	/*
	 * This function is the call back function of form validation, it is used to validate the vehicle group
	 * @param
	 *  $vh_gp - vehicle group
	 * Return type - bool
	 */
	public function check_vh_gp($vh_gp)
	{
		if($GLOBALS['anyID']!=null)//do the following validation after select client.
		{
			$GLOBALS['vehicleGroupList']=$this->user_management_model->get_allvehicleGroup(md5($GLOBALS['anyID']));
			if($GLOBALS['vehicleGroupList']!=null)
			{
				if (empty($vh_gp)) //Check whether the vehicle group is selected for the user or not
				{
					$this->form_validation->set_message('check_vh_gp', 'Please select at least one vehicle group.');
					$GLOBALS['outcome']='<div style="color: red;">Please select at least one vehicle group.</div>';
					return FALSE;
				}
			}
		}
		return true;
	}
	/*
	 * This function is the callback function of form validation,
	 * it is used to validate the email
	 * @param
	 *  $Email - user email
	 * Return type - bool
	 */
	public function check_email($Email)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['userList']=$this->user_management_model->get_userList();
		if (empty($Email)) //Check whether the user email field is empty
		{
			$this->form_validation->set_message('check_email', '%s required.');
			return FALSE;
		}
		else if(!filter_var($Email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['userEmail']=null;
			$this->form_validation->set_message('check_email', 'Invalid email format.');
			return FALSE;
		}
		else if (strlen($Email)>100) //Check whether the email is >100 characters
		{
			$GLOBALS['userEmail']=null;
			$this->form_validation->set_message('check_email', 'Should not greater than 100 characters.');
			return FALSE;
		}
		else
		{
			if(!empty($GLOBALS['userList']))
			foreach ($GLOBALS['userList'] as $row)
			{
				if($GLOBALS['userID']==null)
				{
					//Check whether user already exist in user table
					if(trim($row['user_email'])==$Email)
					{
						$GLOBALS['userEmail']=null;
						$this->form_validation->set_message('check_email', '%s already exist.');
						return FALSE;
					}
				}
				else
				{
					//Check whether user already exist in user table while update
					if(trim($row['user_email'])==$Email && $GLOBALS['userID']!=trim($row['user_id']))
					{
						$GLOBALS['userEmail']=null;
						$this->form_validation->set_message('check_email', '%s already exist.');
						return FALSE;
					}
				}
			}
		}
		return true;
	}
	/*
	 * This function is used to fetch the data for edit.
	 * @param
	 *  $URLuserID - md5 format of user id
	 *  $URLanyID - md5 of client id
	 * Return type - void
	 */
	public function edit_userManagement($URLuserID, $URLanyID)
	{
		$row=$this->user_management_model->get_userList($URLanyID,null,null,null,$URLuserID);
		if($URLuserID!=null && $URLanyID!=null)
		{
			$GLOBALS['userID']=trim($row['user_id']);
			$GLOBALS['anyID']=trim($row['id']);
			$GLOBALS['userEmail']=trim($row['user_email']);
			$GLOBALS['userFullName']=trim($row['user_user_name']);
			$GLOBALS['userRoleID']=trim($row['user_is_admin']);
			$GLOBALS['mobileNo']=trim($row['user_mobile']);
			$GLOBALS['timeZoneID']=trim($row['user_time_zone_id']);
// 			$GLOBALS['distID']=trim($row['user_distributor_id']);
// 			$GLOBALS['dealerID']=trim($row['user_dealer_id']);
// 			$GLOBALS['parentID']=trim($row['user_parent_id']);
			$GLOBALS['active']=trim($row['user_is_active']);
			$GLOBALS['loginUserTypeID']=trim($row['user_type_id']);
			$GLOBALS['selectedVehicleGroup']=$this->user_management_model->get_userVehicleGroupLink($row['user_id']);
			$GLOBALS['selectedDriverGroup']=$this->user_management_model->get_userDriverGroupLink($row['user_id']);
		}
		$this->table_pagination(md5($GLOBALS['anyID']), md5($GLOBALS['loginUserTypeID']));
	}
	/*
	 * This function is used to clear all global data.
	 */
	public function clear_all()
	{
		$GLOBALS['userID']=null;
		$GLOBALS['userEmail']=null;
		$GLOBALS['userFullName']=null;
		$GLOBALS['userRoleID']=null;
		$GLOBALS['mobileNo']=null;
		$GLOBALS['selectedVehicleGroup']=null;
		$GLOBALS['selectedDriverGroup']=null;
		$GLOBALS['vehicleGroupList']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['userList']=array();
		//$GLOBALS['outcome']=null;
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $client - client id
	 *  $pageNo - page no
	 * Return type - void
	 */
	public function table_pagination($client=null, $type=null, $pageNo=0)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		if($GLOBALS['sessionData']!=null)
		{
			if($type==md5(AUTOGRADE_USER))//if autograde client
			{
				$GLOBALS['lableName']="Administrator";
			}
			elseif($type==md5(INDIVIDUAL_CLIENT) || $type==md5(OHTER_CLIENT_USER))//if other client
			{
				$GLOBALS['lableName']="Client";
			}
			elseif($type==md5(DISTRIBUTOR_USER))//if distributor
			{
				$GLOBALS['lableName']="Distributor";
			}
			elseif(md5($GLOBALS['ID']['sess_user_type'])==md5(AUTOGRADE_USER) && $type != DEALER_USER)//$type==md5(DEALER_USER))//if dealer
			{
				$GLOBALS['lableName']="Dealer";
			}
			elseif(md5($GLOBALS['ID']['sess_user_type'])==md5(DEALER_USER))//if dealer is logged in
			{
				$GLOBALS['lableName']="Client";
			}
			elseif($type==md5(PARENT_USER))//if parent
			{
				$GLOBALS['lableName']="Parent";
			}			
			
			$GLOBALS['timeZoneList']=$this->common_model->get_time_zone();
 			$GLOBALS['anyID']=$client;
 			$GLOBALS['loginUserTypeID']=$type;
 			$GLOBALS['loginUserTypeList']=$this->user_management_model->get_Login_user_type();
			$GLOBALS['vehicleGroupList']=$this->user_management_model->get_allvehicleGroup($client);
			$GLOBALS['driverGroupList']=$this->user_management_model->get_allDriverGroup($client);
			$GLOBALS['clientList']=$this->user_management_model->get_allClients($type);			
			//whether event log is required or not(i.e. 'N' not required, 'Y' required)
			$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");
			$config['uri_segment']=URI_SEGMENT_FOR_FIVE;
			$config['base_url'] = base_url("index.php/user_management_ctrl/table_pagination/".$client."/".$type);
			$GLOBALS['userList']=$this->user_management_model->get_userList($type,$client,null,null,null);
			$config['total_rows'] = count($GLOBALS['userList']);
			$config['per_page']=ROW_PER_PAGE;
			$GLOBALS['userList']=$this->user_management_model->get_userList($type,$client,ROW_PER_PAGE, $pageNo,null);			
			$this->pagination->initialize($config);
			$GLOBALS['pageLink']= $this->pagination->create_links();
			$this->display();
		}
		else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('user_management_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
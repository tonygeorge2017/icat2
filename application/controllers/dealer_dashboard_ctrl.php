<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealer_dashboard_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('dealer_dashboard_model');
		$this->load->helper(array('form', 'url'));
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['clientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['dealerID'] = $GLOBALS ['ID'] ['sess_dealerid'];
		$GLOBALS['sessDealerID']= $GLOBALS ['ID'] ['sess_dealerid'];
		$GLOBALS['dealer_device_details']=array();
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	//rest of code shud come here
public function index() {
		if ($GLOBALS ['sessClientID'] == AUTOGRADE_USER) {
			$GLOBALS ['dealerID']=AUTOGRADE_USER;
		}else {
			$GLOBALS ['dealerID'] = $GLOBALS ['ID'] ['sess_dealerid'];
		}
		$this->display ();
	}
	
	public function view() {
		$GLOBALS ['dealerID'] = (null != trim ( $this->input->post ( 'DealerID' ) ) ? trim ( $this->input->post ( 'DealerID' ) ) : null);
		$this->display ();
	}
	public function display() {
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['timezone'] = ($GLOBALS ['ID'] ['sess_time_zonename']) ? $GLOBALS ['ID'] ['sess_time_zonename'] : ''; // ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			//$GLOBALS ['clientList'] = $this->common_model->get_allClients ();
			$GLOBALS ['dealerList'] = $this->dealer_dashboard_model->get_all_dealers ();
			//$GLOBALS['count_devices_yr']=$this->common_model->get_device_details();
			//$GLOBALS['count_devices_mnth']=$this->common_model->get_device_details();
			$this->common_model->menu_display ();
			$this->get_device_data ();
		}
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'dealer_dashboard_view', $GLOBALS ); // load view Home page
		$this->load->view ( 'header_footer/footer' );
	}
	/*
	 * function to get count of purchase & sales details
	 *
	 */
	public function get_device_data()
	{
		$GLOBALS['dealer_device_details'] = $this->common_model->get_device_dealer_details($GLOBALS ['dealerID']);
			
	}
}
?>

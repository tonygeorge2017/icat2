<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle_ctrl extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct();
		
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('vehicle_model');
		$this->load->helper( array('form', 'url') );
		//the below data will save to the vts_vehicle table
		$GLOBALS['vcle_id']=null;
		$GLOBALS['vcle_reg_number']=null;
		$GLOBALS['vcle_vehiclegroup_id']=null;
		$GLOBALS['vcle_chassis_number']=null;
		$GLOBALS['vcle_engine_number']=null;
		$GLOBALS['vcle_color']=null;
		$GLOBALS['vcle_mfr_year']=null;
		$GLOBALS['vcle_reg_year']=null;
		$GLOBALS['vcle_insurance_agency_id']=null;
		$GLOBALS['vcle_policy_number']=null;
		$GLOBALS['vcle_insurance_policy_type']=null;
		$GLOBALS['vcle_insurance_expiry_date']=null;
		$GLOBALS['vcle_speed_limit']=null;
		$GLOBALS['vcle_insurance_terms']=null;
		$GLOBALS['vcle_remark']=null;
		
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['vcle_client_id']=null;
		$GLOBALS['companyList']=array();
		$GLOBALS['companyID']=null;
		$GLOBALS['isEdit']=NOT_ACTIVE;
		$GLOBALS['modelList']=array();
		$GLOBALS['modelID']=null;
		$GLOBALS['fuel_type']=null;
		$GLOBALS['Mileage']=null;
		$GLOBALS['FuelConsumption']=null;
		$GLOBALS['IdealSpeed']=0;
		$GLOBALS['voltage_type']=null;
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		
		$GLOBALS['clientList']=$this->vehicle_model->get_allClients();
		$GLOBALS['companyList']=$this->vehicle_model->get_allCompanies();
		$GLOBALS['vehicleGroupList']=$this->vehicle_model->get_all_vehicle_group($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['vehicleList']=$this->vehicle_model->get_vts_vehicle(null,null,null,md5($GLOBALS['ID']['sess_clientid']));
		$GLOBALS['insuranceAgencyList']=$this->vehicle_model->get_all_agencies($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['vehicleInsurancePolicyTypeList']=$this->vehicle_model->get_vehiclePolicyType();
		$GLOBALS['FuelTypeList']=$this->vehicle_model->get_FuelType();
		$GLOBALS['VoltageTypeList']=$this->vehicle_model->get_VoltageType();
		
		
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
		
	}

	public function index() {
		
		$this->table_pagination(md5($GLOBALS['ID']['sess_clientid']));
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_vehicle in database.
	 */
	public function vts_vehicle_validation() {
		
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		
		if($GLOBALS['temp']!="-1")	//This condition is to reload the page on selection of the client name from dropdown
			$this->table_pagination(md5($GLOBALS['temp']));
		else {
			
			$GLOBALS['vcle_id']=(null!=trim($this->input->post('VehicleId'))?trim($this->input->post('VehicleId')):null);
			$GLOBALS['vcle_reg_number']=(null!=trim($this->input->post('RegisterNumber'))?trim($this->input->post('RegisterNumber')):null);
			$GLOBALS['vcle_vehiclegroup_id']=(null!=trim($this->input->post('VehicleGroup'))?trim($this->input->post('VehicleGroup')):null);
			$GLOBALS['vcle_chassis_number']=(null!=trim($this->input->post('ChassisNumber'))?trim($this->input->post('ChassisNumber')):null);
			$GLOBALS['vcle_engine_number']=(null!=trim($this->input->post('EngineNumber'))?trim($this->input->post('EngineNumber')):null);
			$GLOBALS['vcle_color']=(null!=trim($this->input->post('Color'))?trim($this->input->post('Color')):null);
			$GLOBALS['vcle_mfr_year']=(null!=trim($this->input->post('MfrYear'))?trim($this->input->post('MfrYear')):null);
			$GLOBALS['vcle_reg_year']=(null!=trim($this->input->post('RegnYear'))?trim($this->input->post('RegnYear')):null);
			$GLOBALS['vcle_insurance_agency_id']=(null!=trim($this->input->post('VehicleInsuranceAgencyId'))?trim($this->input->post('VehicleInsuranceAgencyId')):null);
			$GLOBALS['vcle_policy_number']=(null!=trim($this->input->post('PolicyNumber'))?trim($this->input->post('PolicyNumber')):null);
			$GLOBALS['vcle_insurance_policy_type']=(null!=trim($this->input->post('VehicleInsurancePolicyType'))?trim($this->input->post('VehicleInsurancePolicyType')):null);
			$GLOBALS['companyID']=(null!=trim($this->input->post('CompanyID'))?trim($this->input->post('CompanyID')):null);
			$GLOBALS['modelID']=(null!=trim($this->input->post('ModelID'))?trim($this->input->post('ModelID')):null);
			$GLOBALS['fuel_type']=(null!=trim($this->input->post('FuelType'))?trim($this->input->post('FuelType')):null);
			$GLOBALS['Mileage']=(null!=trim($this->input->post('Mileage'))?trim($this->input->post('Mileage')):null);;
			$GLOBALS['FuelConsumption']=(null!=trim($this->input->post('FuelConsumption'))?trim($this->input->post('FuelConsumption')):null);;
			$GLOBALS['IdealSpeed']=(null!=trim($this->input->post('IdealSpeed'))?trim($this->input->post('IdealSpeed')):0);;
			$GLOBALS['voltage_type']=(null!=trim($this->input->post('VoltageType'))?trim($this->input->post('VoltageType')):null);
			
			$GLOBALS['vcle_insurance_expiry_date']=(null!=trim($this->input->post('ExpiryDate'))?trim($this->input->post('ExpiryDate')):null);
			$GLOBALS['vcle_speed_limit']=(null!=trim($this->input->post('SpeedLimit'))?trim($this->input->post('SpeedLimit')):null);
			$GLOBALS['vcle_insurance_terms']=(null!=trim($this->input->post('Terms'))?trim($this->input->post('Terms')):null);
			$GLOBALS['vcle_remark']=(null!=trim($this->input->post('Remark'))?trim($this->input->post('Remark')):null);
			
			$GLOBALS['active']=(null!=($this->input->post('VehicleIsActive'))?$this->input->post('VehicleIsActive'):NOT_ACTIVE);
		
			if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
				$GLOBALS['vcle_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
			else
				$GLOBALS['vcle_client_id']=$GLOBALS['ID']['sess_clientid'];
		
			$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
			if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
				$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_vehicle_cnt_id');
			
			$this->form_validation->set_rules('VehicleGroup', 'Vehicle Group', 'callback_check_vehicle_group');
			$this->form_validation->set_rules('RegisterNumber', 'Register Number', 'callback_check_reg_number');
			$this->form_validation->set_rules('SpeedLimit','Speed Limit','callback_check_speed_limit');
			$this->form_validation->set_rules('CompanyID', 'Company Name', 'callback_check_company_id');
			$this->form_validation->set_rules('ModelID', 'Model Name', 'callback_check_model_id');

			if(isset($GLOBALS['vcle_chassis_number']))
				$this->form_validation->set_rules('ChassisNumber','Chassis Number','callback_check_chs_nor');
			
			if(isset($GLOBALS['vcle_insurance_agency_id'])) {
				$this->form_validation->set_rules('PolicyNumber','Policy Number','callback_check_plcy_nor');
				$this->form_validation->set_rules('VehicleInsurancePolicyType', 'Policy Type', 'callback_check_ins_type');
				$this->form_validation->set_rules('ExpiryDate','Expiry Date','callback_check_expiry_date');
			}
		
			if(isset($GLOBALS['vcle_policy_number']) || isset($GLOBALS['vcle_insurance_expiry_date']) || isset($GLOBALS['vcle_insurance_policy_type']))
				$this->form_validation->set_rules('VehicleInsuranceAgencyId','Insurance Agency','callback_check_insurance_agency');
		
			if ($this->form_validation->run() == FALSE) {// if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Details in vts_vehicle table.
			
				$this->table_pagination(md5($GLOBALS['vcle_client_id']));
				
			} else {
				
				$data['vehicle_regnumber']=$GLOBALS['vcle_reg_number'];
				$data['vehicle_group_id']=$GLOBALS['vcle_vehiclegroup_id'];
				$data['vehicle_chassisnumber']=$GLOBALS['vcle_chassis_number'];
				$data['vehicle_enginenumber']=$GLOBALS['vcle_engine_number'];
				$data['vehicle_colour']=$GLOBALS['vcle_color'];
				$data['vehicle_yearofmanufacture']=$GLOBALS['vcle_mfr_year'];
				$data['vehicle_yearofregistration']=$GLOBALS['vcle_reg_year'];
				$data['vehicle_insuranceagency_id']=$GLOBALS['vcle_insurance_agency_id'];
				$data['vehicle_insurancepolicytype']=$GLOBALS['vcle_insurance_policy_type'];
	// 			$data['']=$GLOBALS['companyID'];
				$data['vehicle_insurancepolicynumber']=$GLOBALS['vcle_policy_number'];
				$data['vehicle_model_id']=$GLOBALS['modelID'];
				$data['vehicle_fuel_type_id']=$GLOBALS['fuel_type'];
				if($GLOBALS['FuelConsumption']!=null)
					$data['vehicle_fuel_consumption']=$GLOBALS['FuelConsumption'];
				if($GLOBALS['Mileage']!=null)
					$data['vehicle_mileage']=$GLOBALS['Mileage'];
				
				$data['vehicle_idealspeed']=$GLOBALS['IdealSpeed'];

				$data['vehicle_voltage_type_id']=$GLOBALS['voltage_type'];
				
				$data['vehicle_insuranceexpirydate']=$GLOBALS['vcle_insurance_expiry_date'];
				$data['vehicle_speedlimit']=$GLOBALS['vcle_speed_limit'];
				$data['vehicle_insuranceterms']=$GLOBALS['vcle_insurance_terms'];
				$data['vehicle_remarks']=$GLOBALS['vcle_remark'];
				
				$data['vehicle_isactive']=$GLOBALS['active'];
				$data['vehicle_client_id']=$GLOBALS['vcle_client_id'];
				
				if($GLOBALS['vcle_id']!=null)	//edit
				{
					$data['vehicle_id']=$GLOBALS['vcle_id'];
					if ($this->vehicle_model->InsertOrUpdateOrdelete_vts_vehicle($GLOBALS['vcle_id'],$data,'edit'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE,
									"Updated Vehicle: Vehicle ID=".trim($GLOBALS['vcle_id']).", Register Nor=".trim($GLOBALS['vcle_reg_number']).",Vehicle Group Id=".trim($GLOBALS['vcle_vehiclegroup_id']).", Vehicle Chassis Nor=".trim($GLOBALS['vcle_chassis_number']).", Vehicle Engine Nor=".trim($GLOBALS['vcle_engine_number']).", color=".trim($GLOBALS['vcle_color']).", Mfr Year=".trim($GLOBALS['vcle_mfr_year']).", Reg Year=".trim($GLOBALS['vcle_reg_year']).", Insurance Agency=".trim($GLOBALS['vcle_insurance_agency_id']).", Policy Nor=".trim($GLOBALS['vcle_policy_number']).", Policy Type=".trim($GLOBALS['vcle_insurance_policy_type']).", Insurance Expiry Date=".trim($GLOBALS['vcle_insurance_expiry_date']).", Speed Limit=".trim($GLOBALS['vcle_speed_limit']).", Insurance terms=".trim($GLOBALS['vcle_insurance_terms']).", Remarks=".trim($GLOBALS['vcle_remark']).", Vehicle is active=".trim($GLOBALS['active']).", Vehicle Client Id=".trim($GLOBALS['vcle_client_id']).", model Id=".trim($GLOBALS['modelID']).", Fuel type id=".trim($GLOBALS['fuel_type']).", Voltage type id=".trim($GLOBALS['voltage_type']));
		
							$GLOBALS['outcome']="Vehicle Group ".'"'.trim($GLOBALS['vcle_reg_number']) .'"'." updated successfully";
							$GLOBALS['temp']=$GLOBALS['vcle_client_id'];
							$GLOBALS['vcle_id']=null;
							$GLOBALS['vcle_vehiclegroup_id']=null;
							$GLOBALS['vcle_insurance_agency_id']=null;
							$GLOBALS['vcle_insurance_policy_type']=null;
							$GLOBALS['active']=null;
							$GLOBALS['companyID']=null;
							$GLOBALS['modelID']=null;
							$GLOBALS['voltage_type']=null;
							$GLOBALS['Mileage']=null;
							$GLOBALS['FuelConsumption']=null;
							$GLOBALS['IdealSpeed']=0;
					}
				}
				else	//insert
				{
					if($this->vehicle_model->InsertOrUpdateOrdelete_vts_vehicle(null,$data,'insert'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE,
									"Created New Vehicle: Register Nor=".trim($GLOBALS['vcle_reg_number']).",Vehicle Group Id=".trim($GLOBALS['vcle_vehiclegroup_id']).", Vehicle Chassis Nor=".trim($GLOBALS['vcle_chassis_number']).", Vehicle Engine Nor=".trim($GLOBALS['vcle_engine_number']).", color=".trim($GLOBALS['vcle_color']).", Mfr Year=".trim($GLOBALS['vcle_mfr_year']).", Reg Year=".trim($GLOBALS['vcle_reg_year']).", Insurance Agency=".trim($GLOBALS['vcle_insurance_agency_id']).", Policy Nor=".trim($GLOBALS['vcle_policy_number']).", Policy Type=".trim($GLOBALS['vcle_insurance_policy_type']).", Insurance Expiry Date=".trim($GLOBALS['vcle_insurance_expiry_date']).", Speed Limit=".trim($GLOBALS['vcle_speed_limit']).", Insurance terms=".trim($GLOBALS['vcle_insurance_terms']).", Remarks=".trim($GLOBALS['vcle_remark']).", Vehicle is active=".trim($GLOBALS['active']).", Vehicle Client Id=".trim($GLOBALS['vcle_client_id']).", Model Id=".trim($GLOBALS['modelID']).", Fuel Id=".trim($GLOBALS['fuel_type']).", Voltage type Id=".trim($GLOBALS['voltage_type']));
		
								$GLOBALS['outcome']="New Vehicle ".'"'.trim($GLOBALS['vcle_reg_number']).'"'." created successfully";
								$GLOBALS['temp']=$GLOBALS['vcle_client_id'];
								$GLOBALS['vcle_id']=null;
								$GLOBALS['vcle_vehiclegroup_id']=null;
								$GLOBALS['vcle_insurance_agency_id']=null;
								$GLOBALS['vcle_insurance_ploicy_type']=null;
								$GLOBALS['active']=null;
								$GLOBALS['companyID']=null;
								$GLOBALS['modelID']=null;
								$GLOBALS['voltage_type']=null;
								$GLOBALS['Mileage']=null;
								$GLOBALS['FuelConsumption']=null;
								$GLOBALS['IdealSpeed']=0;
					}
				}
				$this->table_pagination(md5($GLOBALS['temp']));
			}
		}
	}
	
	/*
	 * This function is to validate the insurance agency field
	 */
	public function check_insurance_agency($ins_agcy)
	{
		if (empty($ins_agcy))
		{
			$this->form_validation->set_message('check_insurance_agency','%s required');
			return FALSE;
		}
		$GLOBALS['vcle_insurance_agency_id']=(null!=trim($this->input->post('VehicleInsuranceAgencyId'))?trim($this->input->post('VehicleInsuranceAgencyId')):null);
		return true;
	}
	
	/*
	 * This function is to validate the insurance type field
	 */
	public function check_ins_type($ins_type)
	{
		$GLOBALS['vcle_insurance_policy_type']=null;
		if (empty($ins_type)) //Check whether the Vehicle group field is empty
		{
			$this->form_validation->set_message('check_ins_type', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_insurance_policy_type']=(null!=trim($this->input->post('VehicleInsurancePolicyType'))?trim($this->input->post('VehicleInsurancePolicyType')):null);
		return true;
	}
	
	/*
	 * This fucntion is to validate the policy number field
	 */
	public function check_plcy_nor($vcle_plcy_nor)
	{
		$GLOBALS['vcle_policy_number']=null;
		if(empty($vcle_plcy_nor))
		{
			$this->form_validation->set_message('check_plcy_nor', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_policy_number']=(null!=trim($this->input->post('PolicyNumber'))?trim($this->input->post('PolicyNumber')):null);
		return true;
	}
	
	/*
	 * This fucntion is to validate the expiry date field
	 */
	public function check_expiry_date($exp_date)
	{
		$GLOBALS['vcle_insurance_expiry_date']=null;
		if(!isset($exp_date))
		{
			$this->form_validation->set_message('check_expiry_date', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_insurance_expiry_date']=(null!=trim($this->input->post('ExpiryDate'))?trim($this->input->post('ExpiryDate')):null);
		return true;
	}
	
	/*
	 * This function is to validate the chassis number field
	 */
	public function check_chs_nor($vcle_chs_nor)
	{
		$GLOBALS['vcle_chassis_number']=null;
// 		if (!is_numeric($vcle_chs_nor)) //Check whether the Vehicle chassis number field is empty
// 		{
// 			$this->form_validation->set_message('check_chs_nor', 'Chassis number should be numeric value');
// 			return FALSE;
// 		}
		if(preg_match('$\.$', $vcle_chs_nor))
		{
			$this->form_validation->set_message ( 'check_chs_nor', 'Please Enter valid chassis number' );
			return FALSE;
		}
		else if(preg_match("/^[A-Za-z]+$/", $vcle_chs_nor))//&& preg_match("/[.]/", $vcle_reg_nor)
		{
			$this->form_validation->set_message('check_chs_nor','Only alphabets are not allowed');
			return FALSE;
		}
		else if(preg_match("$/w$", $vcle_chs_nor))
		{
			$this->form_validation->set_message('check_chs_nor','Please Enter valid chassis number');
			return FALSE;
		}
		$GLOBALS['vcle_chassis_number']=(null!=trim($this->input->post('ChassisNumber'))?trim($this->input->post('ChassisNumber')):null);
		return true;
	}
	
	/*
	 * This function is to validate the speed limit field
	 */
	public function check_speed_limit($vcle_sp_lmt)
	{
		$GLOBALS['vcle_speed_limit']=null;
		if(empty($vcle_sp_lmt))
		{
			$this->form_validation->set_message('check_speed_limit', 'Speed Limit required');
			return FALSE;
		}
		else if (!is_numeric($vcle_sp_lmt)) //Check whether the Vehicle group field is empty
		{
			$this->form_validation->set_message('check_speed_limit', 'Speed limit should be numeric value');
			return FALSE;
		}
		else if(preg_match('$\.$', $vcle_sp_lmt))
		{
			$this->form_validation->set_message ( 'check_speed_limit', 'Please Enter valid speed limit' );
			return FALSE;
		}
		$GLOBALS['vcle_speed_limit']=(null!=trim($this->input->post('SpeedLimit'))?trim($this->input->post('SpeedLimit')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_vehicle vehicle group
	 */
	public function check_vehicle_group($vcle_grp)
	{
		$GLOBALS['vehicleList']=$this->vehicle_model->get_vts_vehicle(null,null,null,md5($GLOBALS['temp']));
		$GLOBALS['vcle_vehiclegroup_id']=null;
		if (empty($vcle_grp))	//Check whether the Vehicle group field is empty
		{
			$this->form_validation->set_message('check_vehicle_group', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_vehiclegroup_id']=(null!=trim($this->input->post('VehicleGroup'))?trim($this->input->post('VehicleGroup')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of the form validation, it is used to validate company name 
	 */
	public function check_company_id($cmp_id)
	{
		$GLOBALS['companyID']=null;
		if(empty($cmp_id))	//check whether the company name is empty or not
		{
			$this->form_validation->set_message('check_company_id', '%s required');
			return FALSE;
		}
		$GLOBALS['companyID']=(null!=trim($this->input->post('CompanyID'))?trim($this->input->post('CompanyID')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of the form validation, it is used to validate the model name
	 */
	public function check_model_id($mdl_id)
	{
		$GLOBALS['modelID']=null;
		if(empty($mdl_id))	//check whether the model name is empty or not
		{
			$this->form_validation->set_message('check_model_id', '%s required');
			return FALSE;
		}
		$GLOBALS['modelID']=(null!=trim($this->input->post('ModelID'))?trim($this->input->post('ModelID')):null);
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the Registration Number
	 */
	public function check_reg_number($vcle_reg_nor)
	{
		$GLOBALS['vehicleList']=$this->vehicle_model->get_vts_vehicle(null,null,null,md5($GLOBALS['vcle_client_id']));
		$GLOBALS['vcle_reg_number']=null;
		if (empty($vcle_reg_nor))
		{
			$this->form_validation->set_message('check_reg_number', '%s required');
			return FALSE;
		}
		else if (is_numeric($vcle_reg_nor))
		{
			$this->form_validation->set_message('check_reg_number', 'Only numerics are not allowed');
			return FALSE;
		}
		else if (strlen($vcle_reg_nor) < 5 || strlen($vcle_reg_nor) > 15 )//MIN_FIELD_LENGTH MAX_FIELD_LENGTH
		{
			$this->form_validation->set_message ( 'check_reg_number', 'Invalid Registration number' );
			return FALSE;
		}
		else if(preg_match("/([%\$#\*@&<>?.,=])/", $vcle_reg_nor))
		{
			$this->form_validation->set_message ( 'check_reg_number', 'Please enter valid number' );
			return FALSE;
		}
		else if(preg_match("/[+]/", $vcle_reg_nor))
		{
			$this->form_validation->set_message ( 'check_reg_number', 'Please enter valid number' );
			return FALSE;
		}
		else if(preg_match("/^[A-Za-z]+$/", $vcle_reg_nor))//&& preg_match("/[.]/", $vcle_reg_nor)
		{
			$this->form_validation->set_message('check_reg_number','Only alphabets are not allowed');
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['vehicleList'] as $row ) {
				if ($GLOBALS ['vcle_id'] == null) {
					if (trim ( strtoupper ( $row ['vehicle_regnumber'] ) ) == strtoupper ( $vcle_reg_nor )) // Check whether registration number already exist in vehicle table
					{
						$GLOBALS['outcome_with_db_check']="Vehicle Reg Number ".'"'.trim($row['vehicle_regnumber']).'"'." already exist";
						$this->form_validation->set_message('check_reg_number', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['vehicle_regnumber'] ) ) == strtoupper ( $vcle_reg_nor ) && $GLOBALS ['vcle_id'] != trim ( $row ['vehicle_id'] )) // Check whether registration number already exist in vehicle table while updating
					{
						$GLOBALS['outcome_with_db_check']="Vehicle Reg Number ".'"'.trim($row['vehicle_regnumber']).'"'." already exist";
						$this->form_validation->set_message('check_reg_number', '');
						return FALSE;
					}
				}
			}
		}
		$GLOBALS['vcle_reg_number']=(null!=trim($this->input->post('RegisterNumber'))?trim($this->input->post('RegisterNumber')):null);
		return true;
	}
	
	/*
	 * This function is to check the vehicel client dropdown is mandatory field only for autograde users
	 */
	public function check_vehicle_cnt_id($vcle_cnt_id)
	{
		//$GLOBALS['dvr_client_id']=null;
		if (empty($vcle_cnt_id)) //Check whether the Vehicele Client dropdown field is empty
		{
			$this->form_validation->set_message('check_vehicle_cnt_id', '%s required');
			return FALSE;
		}
		$GLOBALS['vcle_client_id']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		return true;
	}

	/*
	 * This function is used to get the vehicle model names for the selected vehicle company and return back the list of
	 * vehicle model in JSON format with out refresh the page.
	 *  $cmpyID - vehicle company name
	 * Return type - JSON string
	 */
	public function get_cmpy_model_name( $cmpyID=null ) {
		
		$cmpy_model_name="";
		
		if($cmpyID!=null) {
			
			$cmpy_model_name = json_encode( $this->vehicle_model->get_all_cmpy_models( $cmpyID ) );
	
		}
		
		echo $cmpy_model_name;
	}
	
	/*
	 * This function used to edit or delete the vts_vehicle
	 */
	public function editOrDelete_vts_vehicle( $id, $opt, $vhId, $model_id=0 ) {
		
		$operation= $opt;			//it hold the operation to perform(i.e. edit or delete).
		$VehicleId= $vhId;			//it hold the vehicle id to edit or delete.
	
		$row=$this->vehicle_model->get_vts_vehicle(null,null,$VehicleId,$id);
		
		$vehicle_details= $this->get_Vehicle_Brand( $model_id );
		
		if($operation==md5('edit') && !empty($row)) {
			
			$GLOBALS['isEdit']=ACTIVE;
			
			$operation=="edit";
			
			$GLOBALS['vcle_id']=trim($row['vehicle_id']);
			$GLOBALS['vcle_reg_number']=trim($row['vehicle_regnumber']);
			$GLOBALS['vcle_vehiclegroup_id']=trim($row['vehicle_group_id']);
			$GLOBALS['vcle_chassis_number']=trim($row['vehicle_chassisnumber']);
			$GLOBALS['vcle_engine_number']=trim($row['vehicle_enginenumber']);
			$GLOBALS['vcle_color']=trim($row['vehicle_colour']);
			$GLOBALS['vcle_mfr_year']=trim($row['vehicle_yearofmanufacture']);
			$GLOBALS['vcle_reg_year']=trim($row['vehicle_yearofregistration']);
			$GLOBALS['vcle_insurance_agency_id']=trim($row['vehicle_insuranceagency_id']);
			$GLOBALS['vcle_policy_number']=trim($row['vehicle_insurancepolicynumber']);
			$GLOBALS['vcle_insurance_ploicy_type']=trim($row['vehicle_insurancepolicytype']);
			$GLOBALS['vcle_insurance_expiry_date']=trim($row['vehicle_insuranceexpirydate']);
			$GLOBALS['vcle_speed_limit']=trim($row['vehicle_speedlimit']);
			$GLOBALS['vcle_insurance_terms']=trim($row['vehicle_insuranceterms']);
			$GLOBALS['vcle_remark']=trim($row['vehicle_remarks']);
			$GLOBALS['active']=trim($row['vehicle_isactive']);
			$GLOBALS['vcle_client_id']=trim($row['vehicle_client_id']);
			$GLOBALS[ 'vcle_insurance_policy_type']=trim($row['vehicle_insurancepolicytype']);
			$GLOBALS['Mileage']=trim($row['vehicle_mileage']);
			$GLOBALS['FuelConsumption']=trim($row['vehicle_fuel_consumption']);
			$GLOBALS['IdealSpeed']=trim($row['vehicle_idealspeed']);
			if( sizeof( $vehicle_details ) >=1 ) {
				
				$GLOBALS['companyID']=trim( $vehicle_details["brand_id"] );
// 				$GLOBALS['modelList'] = json_encode( $this->vehicle_model->get_all_cmpy_models( $GLOBALS['companyID'] ) );//$vehicle_details["brand_id"]
				$GLOBALS['modelList'] = $this->vehicle_model->get_all_cmpy_models( $GLOBALS['companyID'] );
				
			} else {
				$GLOBALS['companyID'] = "";
				$GLOBALS['modelList'] = null;
			}
			
			$GLOBALS['modelID']=trim( $model_id );
			$GLOBALS['fuel_type']=trim($row['vehicle_fuel_type_id']);//It used to fill up the textbox in view.
			$GLOBALS['voltage_type']=trim($row['vehicle_voltage_type_id']);
		}
		else if($operation==md5("delete"))
		{
			try
			{
				if($this->vehicle_model->InsertOrUpdateOrdelete_vts_vehicle($VehicleId,null,'delete'))
				{
					$GLOBALS['outcome_with_db_check']="Vehicle ".'"'.trim($row['vehicle_regnumber']).'"'." deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE,
								"Vehicle is deleted: Vehicle ID=".$VehicleId);
				} else
				{
					$GLOBALS ['outcome_with_db_check'] = trim ( $row ['vehicle_regnumber'] ) . " Can't able to delete(Referred some where else)";
				}
					
			}catch (Exception $ex){
				$this->table_pagination($id);
			}
		}
		$this->table_pagination($id);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id=null,$pageNo=0)
	{
		if($cl_id!=null)
		{
			$GLOBALS['vcle_client_id']=$cl_id;
		}
		$GLOBALS['companyList']=$this->vehicle_model->get_allCompanies();
		$row=$GLOBALS['companyList'];
		if( $GLOBALS['companyList'] != null ) {
			
			$cmpid=$row[0]['fsv_id'];
			$mdlid=($GLOBALS['modelID']!=null)?$GLOBALS['modelID']:-1;
			if($GLOBALS['companyID']!=null)
				$cmpid=$GLOBALS['companyID'];
// 			$GLOBALS['todayDateTime']=$this->common_model->get_usertime();

			log_message("debug", "cmp Id----".$cmpid );
			$GLOBALS['modelList']=$this->vehicle_model->get_all_cmpy_models($cmpid);
			log_message("debug","model list---".print_r($GLOBALS['modelList'], true));
			
				
		}
		
		$GLOBALS['vehicleList']=$this->vehicle_model->get_vts_vehicle(null,null,null,$GLOBALS['vcle_client_id']);
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['insuranceAgencyList']=$this->vehicle_model->get_all_agencies($GLOBALS['vcle_client_id']);
		$GLOBALS['vehicleInsurancePolicyTypeList']=$this->vehicle_model->get_vehiclePolicyType();
		$GLOBALS['FuelTypeList']=$this->vehicle_model->get_FuelType();
		$GLOBALS['VoltageTypeList']=$this->vehicle_model->get_VoltageType();
		
		$GLOBALS['vehicleGroupList']=$this->vehicle_model->get_all_vehicle_group($GLOBALS['vcle_client_id']);
		$GLOBALS['clientList']=$this->vehicle_model->get_allClients();
		
		$config['base_url'] = base_url("index.php/vehicle_ctrl/table_pagination/".$GLOBALS['vcle_client_id']);
		$config['uri_segment'] = URI_SEGMENT_FOR_FOUR;
		
		$config['total_rows'] = count($GLOBALS['vehicleList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['vehicleList']=$this->vehicle_model->get_vts_vehicle(ROW_PER_PAGE,$pageNo,null,$GLOBALS['vcle_client_id']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();

		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display() {
		
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('vehicle_view',$GLOBALS);
		$this->load->view('header_footer/footer');
		
	}
	
	private function get_Vehicle_Brand( $vehicle_model ) {
	
		return $this->vehicle_model->get_Vehicle_Brand_Model( $vehicle_model );
	
	}
	
}

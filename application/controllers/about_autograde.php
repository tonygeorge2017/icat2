<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class About_autograde extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model('common_model');
		
		$GLOBALS['screen']="abtus";
		
		
		//$GLOBALS ['product_name'] = $this->commonmodel->get_setting_value ( 'ProductName' ); // getting browser title in each page.
		//$GLOBALS ['screen_name'] = ABOUT_AUTOGRADE;
	}
	
	function index() {
		if ($this->session->userdata ( 'login' )) {
			//$client_time_zone = $this->common_model->get_location ();
			//$GLOBALS ['timezone'] = ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			
			//$this->common_model->check_session();
			$this->common_model->menu_display();
			
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'about_autograde_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		} else {
			$this->load->view ( 'header_footer/header1',$GLOBALS );
			$this->load->view ( 'about_autograde_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' ); // $this->dispaly();
		}
	}
	
	
}
?>
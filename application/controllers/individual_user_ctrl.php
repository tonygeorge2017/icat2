<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Individual_user_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('individual_user_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		//User details:
		$GLOBALS['userID']=null;
		$GLOBALS['userFullName']=null;
		$GLOBALS['userEmail']=null;
		$GLOBALS['mobileNo']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['userList']=array();
		$GLOBALS['userPageLink']=null;
		$GLOBALS['timeZoneID']=null;
		$GLOBALS['timeZoneList']=null;
		//User Device Details
		$GLOBALS['userDeviceID']=null;
		$GLOBALS['dealerID']=null;
		$GLOBALS['preDeviceID']=null;
		$GLOBALS['deviceID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['vehicleRegNo']=null;
		$GLOBALS['preInstallDate']=null;
		$GLOBALS['installDate']=null;
		$GLOBALS['expireDate']=null;
		$GLOBALS['speedLimit']=null;
		$GLOBALS['dealerList']=array();
		$GLOBALS['deviceList']=array();
		$GLOBALS['userDeviceList']=array();
		$GLOBALS['devicePageLink']=null;
		$GLOBALS['editOrError']=NOT_ACTIVE;
		$GLOBALS['modeloutcome']=null;
		$GLOBALS['clientTimeDiff']=null;
		$GLOBALS['outcome']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination(0);
	}
	/*
	 * This used to pass the received user id to edit function
	 * @param
	 *  $URLuserID - user id
	 */
	public function add_device($URLuserID)
	{
		$GLOBALS['editOrError']=ACTIVE;
		$this->edit_userManagement($URLuserID);
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function user_validation()
	{
		$GLOBALS['userID']=(null!=($this->input->post('UserID'))?$this->input->post('UserID'):null);
		$GLOBALS['userFullName']=(null!=($this->input->post('UserFullName'))?trim($this->input->post('UserFullName')):null);
		$GLOBALS['userEmail']=(null!=($this->input->post('UserEmail'))?trim($this->input->post('UserEmail')):null);
		$GLOBALS['mobileNo']=(null!=($this->input->post('MobileNo'))?trim($this->input->post('MobileNo')):null);
		$GLOBALS['timeZoneID']=(null!=($this->input->post('TimeZoneID'))?$this->input->post('TimeZoneID'):null);
		$GLOBALS['active']=(null!=($this->input->post('Active'))?$this->input->post('Active'):NOT_ACTIVE);
		$this->form_validation->set_message('required', '%s required.');
		$this->form_validation->set_rules('UserFullName', 'User Name', 'required|callback_check_username');//modified by shruthi adding error validation function
		$this->form_validation->set_rules('MobileNo', 'Mobile No', 'required|callback_check_mobile');
		$this->form_validation->set_rules('TimeZoneID', 'Time Zone', 'required');
		$this->form_validation->set_rules('UserEmail', 'Email', 'callback_check_email');
		// if any of the form rule is failed, then it show the error msg in view.
		//Else update the new Department in department table.
		if ($this->form_validation->run() == FALSE)
		{
			$this->table_pagination(0);
		}
		else
		{
			$data['user_client_id']=INDIVIDUAL_CLIENT;
			$data['user_email']=$GLOBALS['userEmail'];
			$data['user_user_name']=$GLOBALS['userFullName'];
			$data['user_mobile']=	$GLOBALS['mobileNo'];
			$data['user_time_zone_id']=$GLOBALS['timeZoneID'];
			$data['user_is_individual_user']=ACTIVE;
			$data['user_is_active']=$GLOBALS['active'];
			$date['user_type_id']=INDIVIDUAL_CLIENT;
			if($GLOBALS['userID']!=null)		//edit
			{
				if($this->individual_user_model->InsertOrUpdate_user($GLOBALS['userID'],$data,'edit'))
				{
					$GLOBALS['outcome']='<div style="color: green;">Successfully Updated.</div>';
					// parameters are user id, ip, screen id, event description
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], INDIVIDUAL_USER,
								"Update User:"."  User ID=".trim($GLOBALS['userID'])." , Email=".trim($GLOBALS['userEmail']).", Full name=".trim($GLOBALS['userFullName']).", Time Zone ID=".trim($GLOBALS['timeZoneID']));
				}
				else
				{
					$GLOBALS['outcome']='<div style="color: red;">Not Updated.</div>';
				}
			}
			else						 		//insert
			{
				$data['user_password']=mt_rand();
				if($this->individual_user_model->InsertOrUpdate_user(null,$data,'insert'))
				{
					$id=$this->individual_user_model->get_userList(null,null,null,strtoupper($GLOBALS['userEmail']));
					if($id!=null)
					{
						$GLOBALS['userID']=$id;
						$this->individual_user_model->Add_userVehicleGroupLink($id);
						$this->individual_user_model->Add_userDriverGroupLink($id);
					}
					$GLOBALS['outcome']='<div style="color: green;">New User has been created.</div>';
					$s_msg='Dear '.$data['user_user_name'].',<br><br>&nbsp; Your Email-Id has been registered as a New Login in the Autograde T.A.N.K. System.<br>&nbsp;&nbsp;Please click the below link to set the new password and access the application with new credentials.<br>&nbsp;&nbsp;<a href='.base_url("index.php/change_password_ctrl/view/".md5($id)).'>Click Here</a><br><br>Thanks & Regards,<br>Autograde T.A.N.K. Support.';
					$this->load->library ( 'commonfunction' );
					$this->commonfunction->send_mail($GLOBALS['userEmail'],$GLOBALS['userFullName'],CC_TO, BCC_TO,'Vehicle Tracking System : New User Registered',$s_msg,REPLY_TO,'VTS Support');
				}// parameters are user id, ip, screen id, event description
				if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
					$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], INDIVIDUAL_USER,
							"Create New User:"." User Name=".trim($GLOBALS['userFullName']).", Email=".trim($GLOBALS['userEmail']).", Full name=".trim($GLOBALS['userFullName']).", Time Zone ID=".trim($GLOBALS['timeZoneID']));
			}
			$this->table_devicePagination($GLOBALS['userID']);
		}
	}
	/*
	 * This function is the callback function of form validation, it is used to validate the email
	 * @param
	 *  $Email - user email id
	 * Return type - bool
	 */
	public function check_email($Email)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$id=$this->individual_user_model->get_userList(null,null,null,strtoupper($Email));
		if (empty($Email)) //Check whether the user email field is empty
		{
			$this->form_validation->set_message('check_email', '%s required.');
			return FALSE;
		}
		else if(!filter_var($Email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['userEmail']=null;
			$this->form_validation->set_message('check_email', 'Invalid email format.');
			return FALSE;
		}
		else if (strlen($Email)>100) //Check whether the email is >100 characters
		{
			$GLOBALS['userEmail']=null;
			$this->form_validation->set_message('check_email', 'Should not greater than 100 characters.');
			return FALSE;
		}
		else
		{
			if(!empty($id))
			{
				//log_message('debug','test-----------userid:'.$GLOBALS['userID'].', ID: '.$id.' newEmail: '.$Email);
				if($GLOBALS['userID']==null)
				{
					$GLOBALS['userEmail']=null;
					$this->form_validation->set_message('check_email', '%s already exist.');
					return FALSE;
				}
				else
				{
					if($GLOBALS['userID'] != $id)//Check whether user already exist in user table while update
					{
						$GLOBALS['userEmail']=null;
						$this->form_validation->set_message('check_email', '%s already exist.');
						return FALSE;
					}
				}
			}
		}
		return true;
	}
	/*created by shruthi
	 * function to validate username 
	 */
	public function check_username($username)
	{
		if (empty ( $username )) {
			$this->form_validation->set_message ( 'check_username', '%s required.' );
			return FALSE;
		}elseif(is_numeric($username))
		{
			$GLOBALS['userFullName']=null;
			$this->form_validation->set_message ( 'check_username', '%s should not be a numeric.' );
			return FALSE;
		}else
		return true;
	}

	/*created by shruthi
	 * function to validate mobile no
	 */
	public function check_mobile($mob)
	{
		if (empty ( $mob )) {
			$this->form_validation->set_message ( 'check_mobile', '%s required.' );
			return FALSE;
		}else if (!is_numeric($mob)) // Check whether the given value is numeric.
		{
			$GLOBALS['mobileNo']=null;
			$this->form_validation->set_message ( 'check_mobile', 'Mobile number should be numeric.' );
			return FALSE;
		}
		else if (strlen($mob) < 10 || strlen($mob) > 10) // Check whether mobile number should allow only 10 numbers
		{
			$GLOBALS['mobileNo']=null;
			$this->form_validation->set_message ( 'check_mobile', 'Please Enter 10 digit valid mobile number.' );
			return FALSE;
		}
		else
			return true;
	}
	/*
	 * This function is used to fetch the data for edit.
	 * @param
	 *  $URLuserID - user id
	 * Return type - void
	 */
	public function edit_userManagement($URLuserID)
	{
		$row=$this->individual_user_model->get_userList(null,null,$URLuserID);
		if($URLuserID!=null)
		{
			$GLOBALS['userID']=trim($row['user_id']);
			$GLOBALS['userEmail']=trim($row['user_email']);
			$GLOBALS['userFullName']=trim($row['user_user_name']);
			$GLOBALS['mobileNo']=trim($row['user_mobile']);
			$GLOBALS['timeZoneID']=$row['user_time_zone_id'];
			$GLOBALS['active']=trim($row['user_is_active']);//It used to fill up the textbox in view.
		}
		$this->table_devicePagination(md5($GLOBALS['userID']));
	}
	/*
	 * This function is used to fetch the data for edit.
	 * @param
	 *  $URLuserID - user id
	 *  $URLuserDeviceID - device id
	 * Return type - void
	 */
	public function edit_deviceManagement($URLuserID,$URLuserDeviceID)
	{
		$row=$this->individual_user_model->get_individualUserDeviceDetails($URLuserID,$URLuserDeviceID);
		if($row!=null)
		{
			$GLOBALS['userDeviceID']=trim($row[0]['user_dev_id']);
			$GLOBALS['dealerID']=trim($row[0]['device_dealer_id']);
			$GLOBALS['deviceID']=trim($row[0]['user_dev_device_id']);
			$GLOBALS['vehicleID']=trim($row[0]['user_dev_vehicle_id']);
			$GLOBALS['vehicleRegNo']=trim($row[0]['vehicle_name']);
			$GLOBALS['installDate']=trim($row[0]['user_dev_install_date']);
			$GLOBALS['speedLimit']=trim($row[0]['vehicle_speedlimit']);
			$GLOBALS['editOrError']=ACTIVE;
		}
		$this->edit_userManagement($URLuserID);
	}
	/*
	 * This function is used to clear all global data.
	 */
	public function clear_all()
	{
		$GLOBALS['userID']=null;
		$GLOBALS['userEmail']=null;
		$GLOBALS['userFullName']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['userList']=null;
		$GLOBALS['mobileNo']=null;
	}
	/*
	 * This function is used to get the device related to selected dealer.
	 * @param
	 *  $URLdealerID - dealer id
	 *  $URLdeviceID - device id
	 * Return type - void
	 */
	public function get_deviceByDealer($URLdealerID=null,$URLdeviceID=null)
	{
		$devices=json_encode($this->individual_user_model->get_allDevice($URLdealerID,$URLdeviceID));
		echo($devices);
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $pageNo - page no
	 * Return type - void
	 */
	public function table_pagination($pageNo=0)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		if($GLOBALS['sessionData']!=null)
		{
			$GLOBALS['timeZoneList']=$this->common_model->get_time_zone();
			$GLOBALS['dealerList']=$this->individual_user_model->get_allDealers();
			$GLOBALS['clientTimeDiff']=$this->individual_user_model->get_time_diff(md5(INDIVIDUAL_CLIENT));
			$config['base_url'] = base_url("index.php/individual_user_ctrl/table_pagination/");
			$GLOBALS['userList']=$this->individual_user_model->get_userList();
			$config['total_rows'] = count($GLOBALS['userList']);
			$config['per_page']=ROW_PER_PAGE;
			$GLOBALS['userList']=$this->individual_user_model->get_userList(ROW_PER_PAGE,$pageNo);
			$this->pagination->initialize($config);
			$GLOBALS['userPageLink']= $this->pagination->create_links();
			$this->display();
		}
		else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
	/*
	 * This function is used to validate and insert the given device
	 * installation data to related tables
	 */
	public function device_installation()
	{
		//Hidden values from user
		$GLOBALS['userID']=(null!=($this->input->post('UserID'))?$this->input->post('UserID'):null);
		$GLOBALS['userFullName']=(null!=($this->input->post('UserFullName'))?trim($this->input->post('UserFullName')):null);
		$GLOBALS['userEmail']=(null!=($this->input->post('UserEmail'))?trim($this->input->post('UserEmail')):null);
		$GLOBALS['mobileNo']=(null!=($this->input->post('MobileNo'))?trim($this->input->post('MobileNo')):null);
		$GLOBALS['userDeviceID']=(null!=($this->input->post('UserDeviceID'))?$this->input->post('UserDeviceID'):null);
		$GLOBALS['preDeviceID']=(null!=($this->input->post('PreDeviceID'))?$this->input->post('PreDeviceID'):null);
		$GLOBALS['vehicleID']=(null!=($this->input->post('VehicleID'))?$this->input->post('VehicleID'):null);
		$GLOBALS['preInstallDate']=(null!=($this->input->post('PreInstallDate'))?trim($this->input->post('PreInstallDate')):null);
		//User Posted values
		$GLOBALS['dealerID']=(null!=($this->input->post('DealerID'))?$this->input->post('DealerID'):null);
		$GLOBALS['deviceID']=(null!=($this->input->post('DeviceID'))?$this->input->post('DeviceID'):null);
		$GLOBALS['vehicleRegNo']=(null!=($this->input->post('VehicleRegNo'))?trim($this->input->post('VehicleRegNo')):null);
		$GLOBALS['installDate']=(null!=($this->input->post('InstallDate'))?trim($this->input->post('InstallDate')):null);
		$GLOBALS['speedLimit']=(null!=($this->input->post('SpeedLimit'))?trim($this->input->post('SpeedLimit')):null);
		if($GLOBALS['installDate']!=null)
		{
			$sdate = new DateTime($GLOBALS['installDate']);
			$GLOBALS['installDate']=$sdate->format('Y-m-d');
			$exdate=new DateTime($GLOBALS['installDate']);
			$exdate=$exdate->modify("1 year");
			$GLOBALS['expireDate']=$exdate->format('Y-m-d');
		}
		$this->form_validation->set_message('required', '%s required.');
		$this->form_validation->set_rules('DealerID', 'Dealer', 'required');
		$this->form_validation->set_rules('DeviceID', 'Device', 'required');
		$this->form_validation->set_rules('VehicleRegNo', 'Vehicle Reg No', 'callback_check_vhReg');
		$this->form_validation->set_rules('InstallDate', 'Install Date', 'callback_check_install_date');
		$this->form_validation->set_rules('SpeedLimit', 'Speed Limit', 'callback_check_maxSpeed');
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Department in department table.
		{
			$GLOBALS['editOrError']=ACTIVE;
			$this->table_devicePagination(md5($GLOBALS['userID']));
		}
		else
		{
			if($GLOBALS['userDeviceID']!=null)//edit
			{
				if($this->individual_user_model->Update_changes($GLOBALS))
				{
					$GLOBALS['outcome']='<div style="color: green;">Successfully Updated.</div>';
					// parameters are user id, ip, screen id, event description
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], INDIVIDUAL_USER,
								"Update Device to Individual User :"." Device ID=".$GLOBALS['deviceID'].", Vehicle Reg.No.=".$GLOBALS['vehicleRegNo'].", Installation Date=".$GLOBALS['installDate'].",  User ID=".trim($GLOBALS['userID'])." , Email=".trim($GLOBALS['userEmail']).", Full name=".trim($GLOBALS['userFullName']));
				}
				else
				{
					$GLOBALS['outcome']='<div style="color: red;">Sorry, Error occured while updating.</div>';
				}
			}
			else 							//insert
			{
				if($this->individual_user_model->Insert_addDevice($GLOBALS))
				{
					$GLOBALS['outcome']='<div style="color: green;">New vehicle and driver has been created and added to the trip register.</div>';
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], "27",
								"Add Device to Individual User:"." Device ID=".$GLOBALS['deviceID'].", Vehicle Reg.No.=".$GLOBALS['vehicleRegNo'].", Installation Date=".$GLOBALS['installDate'].",  User ID=".trim($GLOBALS['userID'])." , Email=".trim($GLOBALS['userEmail']).", Full name=".trim($GLOBALS['userFullName']));
				}
				else {
					$GLOBALS['outcome']='<div style="color: red;">Sorry, Error occured while inserting the data.</div>';
				}
			}
			$this->table_devicePagination(md5($GLOBALS['userID']));
		}
	}
	/*
	 * This function is used to validate the given client installation date&time
	 * @param
	 *  $installdate - date & time
	 * Return type - bool
	 */
	public function check_install_date($installdate)
	{
		$userdate=$this->common_model->get_usertime();
		$date_value=new DateTime($userdate);
		$datevalue=$date_value->format('Y-m-d');
		if (empty($installdate)) //Check whether the device installation date field is empty
		{
			$GLOBALS['modeloutcome']='<div style="color: red;">Install date & time required.</div>';
			return FALSE;
		}
		else if(!empty($installdate))
		{
			$install_date = new DateTime($installdate);
			$clientdate=$install_date->format('Y-m-d');
			if($clientdate > $datevalue)
			{
				$GLOBALS['modeloutcome']='<div style="color: red;">Install date & time must be less than or equal to today'."'".'s date & time.</div>';
				return FALSE;
			}
		}
		else
			return true;
	}
	/*
	 * This function is the callback function of form validation, it is used to validate the vehicle registration no.
	 * @param
	 *  $vhreg - vehicle reg. no
	 * Return type - bool
	 */
	public function check_vhReg($vhreg)
	{
		$vehicleList=$this->individual_user_model->get_allVehicle();
		if (empty($vhreg)) //Check whether the vehicle reg. no. field is empty
		{
			$this->form_validation->set_message('check_vhReg', '%s required.');
			return FALSE;
		}
		else
		{
			if(!empty($vehicleList))
				foreach ($vehicleList as $row)
				{
					if($GLOBALS['vehicleID']==null)
					{
						if(trim(strtoupper($row['vehicle_regnumber']))==strtoupper($vhreg))//Check whether user already exist in user table
						{
							$GLOBALS['vehicleRegNo']=null;
							$this->form_validation->set_message('check_vhReg', '%s already exist.');
							return FALSE;
						}
					}
					else
					{
						if(trim(strtoupper($row['vehicle_regnumber']))==strtoupper($vhreg) && $GLOBALS['vehicleID']!=trim($row['vehicle_id']))//Check whether user already exist in user table while update
						{
							$GLOBALS['vehicleRegNo']=null;
							$this->form_validation->set_message('check_vhReg', '%s already exist.');
							return FALSE;
						}
					}
				}
		}
		return true;
	}
	/*
	 * This function is the callback function of form validation, it is used to validate the vehicle max speed limit.
	 * @param
	 *  $vhmaxspeed - max speed of the vehicle
	 * Return type - bool
	 */
	public function check_maxSpeed($vhmaxspeed)
	{
		if (empty($vhmaxspeed)) //Check whether the vehicle max speed limit field is empty
		{
			$GLOBALS['speedLimit']=null;
			$this->form_validation->set_message('check_maxSpeed', '%s required.');
			return FALSE;
		}
		else if(!is_numeric($vhmaxspeed))
		{
			$GLOBALS['speedLimit']=null;
			$this->form_validation->set_message('check_maxSpeed', '%s must be numeric.');
			return FALSE;
		}
		return true;
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $userID - user id
	 *  $pageNo - page no
	 * Return type - void
	 */
	public function table_devicePagination($userID,$pageNo=0)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		if($GLOBALS['sessionData']!=null)
		{
			$GLOBALS['deviceList']=$this->individual_user_model->get_allDevice($GLOBALS['dealerID'],$GLOBALS['deviceID']);
			$config['uri_segment']=4;
			$config['base_url'] = base_url("index.php/individual_user_ctrl/table_devicePagination/".md5($userID)."/");
			$GLOBALS['userDeviceList']=$this->individual_user_model->get_individualUserDeviceDetails($userID);
			$config['total_rows'] = count($GLOBALS['userDeviceList']);
			$config['per_page']=ROW_PER_PAGE;
			$GLOBALS['userDeviceList']=$this->individual_user_model->get_individualUserDeviceDetails($userID,null,ROW_PER_PAGE,$pageNo);
			$this->pagination->initialize($config);
			$GLOBALS['devicePageLink']=$this->pagination->create_links();
			$this->table_pagination();
		}
		else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header_individual_users',$GLOBALS);
		$this->load->view('individual_user_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
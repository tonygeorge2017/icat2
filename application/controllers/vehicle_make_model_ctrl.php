<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle_make_model_ctrl extends CI_Controller {
/*
 * Class constructor
 */	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('vehicle_make_model_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['companyNameID']="";
		$GLOBALS['companyName']="";
		$GLOBALS['companyList']=[];
		$GLOBALS['modleID']="";
		$GLOBALS['modleName']="";
		$GLOBALS['modleList']=[];
		$GLOBALS['sensorValues']="";
		$GLOBALS["makeModelList"]=[];

	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{		
		$this->table_pagination();
	}
	//This function used to validate & store the model name & sensor values
	public function validate_model_data()
	{
		try 
		{
			$GLOBALS['companyNameID']=trim($this->input->post("CompanyNameID"));
			$GLOBALS['companyName']=trim($this->input->post("CompanyName"));		
			$GLOBALS['modleID']=trim($this->input->post("ModelNameID"));
			$GLOBALS['modleName']=trim($this->input->post("ModelName"));			
			$GLOBALS['sensorValues']=trim($this->input->post("SensorValues"));
			//log_message('debug','companyNameID:'.$GLOBALS['companyNameID'].',companyName:'.$GLOBALS['companyName']).',modleID:'.$GLOBALS['modleID'].',modleName:'.$GLOBALS['modleName'].',sensorValues:'.$GLOBALS['sensorValues'];
			$this->form_validation->set_message('required','%s required');
			$this->form_validation->set_rules('CompanyName', 'Company Name','required|trim');
			$this->form_validation->set_rules('ModelName','Model Name With Mrf.Year','required|trim');
			if($this->form_validation->run())
			{
				if(empty($GLOBALS['modleID']))//insert
				{
					//log_message('debug','*****Into insert');
					$data["companyNameID"]=$GLOBALS['companyNameID'];
					$data["companyName"]=$GLOBALS['companyName'];
					$data["modleName"]=$GLOBALS['modleName'];
					$id=$this->vehicle_make_model_model->insert_newMakeModelDetails($data);					
					$color=($id > 0)?'green':'red';
					$GLOBALS['modeloutcome']='<div style="color: '.$color.';">Data inserted successfully.</div>';
					$data=[];//refresh the array
					$GLOBALS['modleID']=$data['modleID']=$id;
					$data['sensorValues']=$GLOBALS['sensorValues'];
					$response=$this->vehicle_make_model_model->update_fuelvalues($data);
					$color=($response=='Error')?'red':'green';
					$GLOBALS['modeloutcome'].='<div style="color: '.$color.';">'.$response.'</div>';
				}
				else if(!empty($GLOBALS['modleID']))//Update
				{			
					//log_message('debug','*****Into update');
					$data['modleID']=$GLOBALS['modleID'];
					$data['sensorValues']=$GLOBALS['sensorValues'];
					$response=$this->vehicle_make_model_model->update_fuelvalues($data);
					$color=($response=='Error')?'red':'green';
					$GLOBALS['modeloutcome']='<div style="color: '.$color.';">'.$response.'</div>';
				}
			}
			else
			{
				$GLOBALS['modeloutcome']='<div style="color: red;">Please fill required fields.</div>';
			}			
			$this->table_pagination();
		} catch (Exception $e) {
			log_message('error',$e->getMessage());
		}
	}
	
	public function edit_vehicle_make_model($parentid,$childid)
	{
		try {
			$parentRow=$this->vehicle_make_model_model->get_currentParentDetailsToEdit($parentid);
			if($parentRow!=null)
			{
				$GLOBALS['companyNameID']=$parentRow[0]["parent_id"];
				$GLOBALS['companyName']=$parentRow[0]["parent_name"];
			}			
			$list_fuelRow=$this->vehicle_make_model_model->get_currentFuelSensorValues($childid);
			if($list_fuelRow!=null)
			{
				$GLOBALS['modleID']=$list_fuelRow[0]["fsv_id"];
				$GLOBALS['modleName']=$list_fuelRow[0]["fsv_model"];
				foreach ($list_fuelRow as $row)
				{					
					$GLOBALS['sensorValues'].=$row["fs_value_liters"].'-'.$row["fs_value_volts"].'; ';
				}
			}						
		} catch (Exception $e) {
			log_message('error',$e->getMessage());
		}
		$this->table_pagination('0');
	}
	
	public function get_autoCompletionModelName(){
		$parentStartchar=isset($_GET["parent"])?trim($_GET["parent"]):'';
		$isParent=isset($_GET["isparent"])?trim($_GET["isparent"]):'';
		$model_name=$this->vehicle_make_model_model->get_modelForAutoComplete($parentStartchar,$isParent);
		if(count($model_name)>0)
		{
			foreach ($model_name as $row)
			{
				$new_array['value'] = htmlentities(stripslashes($row['fsv_model']));
				$new_array['id'] = htmlentities(stripslashes($row['fsv_id'])); //build an array
				$row_set[] =  $new_array;
			}
			echo json_encode($row_set);
		}
		
	}
	public function table_pagination($pageNo='0')
	{		
		$GLOBALS["makeModelList"]=$this->vehicle_make_model_model->get_makeModelList(false,$pageNo);
		$config['base_url']= base_url("index.php/vehicle_make_model_ctrl/table_pagination/");
		$total_counts=$this->vehicle_make_model_model->get_makeModelList(true);
		$config['total_rows']=$total_counts[0]["total_count"];
		//log_message('debug','--Count:'.$config['total_rows']);
		$config['per_page']=ROW_PER_PAGE;
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('vehicle_make_model_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insurance_agency_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('insurance_agency_model');
		$this->load->helper(array('form', 'url'));
		//the below data will save to the vts_insurance_agency table
		$GLOBALS['insr_agcy_id']=null;
		$GLOBALS['insr_agcy']=null;
		$GLOBALS['insr_agcy_address']=null;
		$GLOBALS['insr_agcy_post_code']=null;
		$GLOBALS['insr_agcy_branch_phone']=null;
		$GLOBALS['insr_agcy_branch_email']=null;
		$GLOBALS['insr_agcy_contact_person']=null;
		$GLOBALS['insr_agcy_contact_designation']=null;
		$GLOBALS['insr_agcy_contact_email']=null;
		$GLOBALS['insr_agcy_contact_phone']=null;
		$GLOBALS['insr_agcy_remarks']=null;

		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['insr_agcy_client_id']=null;
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['insr_agcy_client_id']=$GLOBALS['ID']['sess_clientid'];
		
		$GLOBALS['insuranceAgencyList']=$this->insurance_agency_model->get_vts_insurance_agency(null,null,null,md5($GLOBALS['ID']['sess_clientid']));
		$GLOBALS['clientList']=$this->insurance_agency_model->get_allClients();
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	public function index()
	{
		$this->table_pagination(md5($GLOBALS['ID']['sess_clientid']));
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_insurance_agency in database.
	 */
	public function vts_insurance_agency_validation()
	{
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		if($GLOBALS['temp']!="-1")
			$this->table_pagination(md5($GLOBALS['temp']));
		else
		{
		$GLOBALS['insr_agcy_id']=(null!=trim($this->input->post('InsuranceAgencyId'))?trim($this->input->post('InsuranceAgencyId')):null);
		$GLOBALS['insr_agcy']=(null!=trim($this->input->post('InsuranceAgency'))?trim($this->input->post('InsuranceAgency')):null);
		$GLOBALS['insr_agcy_address']=(null!=trim($this->input->post('InsuranceAgencyAddress'))?trim($this->input->post('InsuranceAgencyAddress')):null);
		$GLOBALS['insr_agcy_post_code']=(null!=trim($this->input->post('InsuranceAgencyPostCode'))?trim($this->input->post('InsuranceAgencyPostCode')):null);
		$GLOBALS['insr_agcy_branch_phone']=(null!=trim($this->input->post('InsuranceAgencyBranchPhone'))?trim($this->input->post('InsuranceAgencyBranchPhone')):null);
		$GLOBALS['insr_agcy_branch_email']=(null!=trim($this->input->post('InsuranceAgencyBranchEmail'))?trim($this->input->post('InsuranceAgencyBranchEmail')):null);
		$GLOBALS['insr_agcy_contact_person']=(null!=trim($this->input->post('InsuranceAgencyContactPerson'))?trim($this->input->post('InsuranceAgencyContactPerson')):null);
		$GLOBALS['insr_agcy_contact_designation']=(null!=trim($this->input->post('InsuranceAgencyContactDesignation'))?trim($this->input->post('InsuranceAgencyContactDesignation')):null);
		$GLOBALS['insr_agcy_contact_email']=(null!=trim($this->input->post('InsuranceAgencyContactEmail'))?trim($this->input->post('InsuranceAgencyContactEmail')):null);
		$GLOBALS['insr_agcy_contact_phone']=(null!=trim($this->input->post('InsuranceAgencyContactPhone'))?trim($this->input->post('InsuranceAgencyContactPhone')):null);
		$GLOBALS['insr_agcy_remarks']=(null!=trim($this->input->post('InsuranceAgencyRemarks'))?trim($this->input->post('InsuranceAgencyRemarks')):null);
		
		$GLOBALS['active']=(null!=($this->input->post('InsuranceAgencyIsActive'))?$this->input->post('InsuranceAgencyIsActive'):NOT_ACTIVE);
		
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$GLOBALS['insr_agcy_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		}
		else
		{
			$GLOBALS['insr_agcy_client_id']=$GLOBALS['ID']['sess_clientid'];
		}
			$GLOBALS['temp']=$GLOBALS['insr_agcy_client_id'];
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('InsuranceAgency', 'Agency Name', 'callback_check_insurance_agency');
		$this->form_validation->set_rules('InsuranceAgencyBranchPhone','Branch Phone','callback_check_insurance_branch_phone');
		$this->form_validation->set_rules('InsuranceAgencyContactPhone','Contact Phone','callback_check_insurance_contact_phone');
		
		if(isset($GLOBALS['insr_agcy_contact_person']))
		{
			$this->form_validation->set_rules('InsuranceAgencyContactPerson','Contact Person','callback_check_contact_person');
		}
		if(isset($GLOBALS['insr_agcy_post_code']))
		{
			$this->form_validation->set_rules('InsuranceAgencyPostCode','Postal Code','callback_check_insurance_postal_code');
		}
		if(isset($GLOBALS['insr_agcy_branch_email']))
		{
			$this->form_validation->set_rules('InsuranceAgencyBranchEmail','Branch Email','callback_check_insurance_branch_email');
		}
		if(isset($GLOBALS['insr_agcy_contact_email']))
		{
			$this->form_validation->set_rules('InsuranceAgencyContactEmail','Contact Email','callback_check_insurance_contact_email');
		}
		if(isset($GLOBALS['insr_agcy_contact_designation']))
		{
			$this->form_validation->set_rules('InsuranceAgencyContactDesignation','Designation','callback_check_insurance_contact_desig');
		}
		
		if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
		{
			$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_cnt_id');
		}

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Insurance Agency in vts_insurance_agency table.
		{
			$this->table_pagination(md5($GLOBALS['insr_agcy_client_id']));
		}
		else
		{
			$data['insurance_agency']=$GLOBALS['insr_agcy'];
			$data['insurance_agency_address']=$GLOBALS['insr_agcy_address'];
			$data['insurance_agency_postcode']=$GLOBALS['insr_agcy_post_code'];
			$data['insurance_agency_branchphone']=$GLOBALS['insr_agcy_branch_phone'];
			$data['insurance_agency_branchemail']=$GLOBALS['insr_agcy_branch_email'];
			$data['insurance_agency_contactperson']=$GLOBALS['insr_agcy_contact_person'];
			$data['insurance_agency_contactdesignation']=$GLOBALS['insr_agcy_contact_designation'];
			$data['insurance_agency_contactemail']=$GLOBALS['insr_agcy_contact_email'];
			$data['insurance_agency_contactphone']=$GLOBALS['insr_agcy_contact_phone'];
			$data['insurance_agency_remarks']=$GLOBALS['insr_agcy_remarks'];
			
			$data['insurance_agency_isactive']=$GLOBALS['active'];
			$data['insurance_agency_client_id']=$GLOBALS['insr_agcy_client_id'];
			
			
			if($GLOBALS['insr_agcy_id']!=null)		//edit
			{
				$data['insurance_agency_id']=$GLOBALS['insr_agcy_id'];
				if ($this->insurance_agency_model->InsertOrUpdateOrdelete_vts_insurance_agency($GLOBALS['insr_agcy_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], INSURANCE_AGENCY,
								"Updated Insurance Agency: Insurance Agency ID=".trim($GLOBALS['insr_agcy_id']).", Insurance Agency=".trim($GLOBALS['insr_agcy']).",Insurance Agency Address=".trim($GLOBALS['insr_agcy_address']).", Insurance Agency PostCode=".trim($GLOBALS['insr_agcy_post_code']).", Branch Phone=".trim($GLOBALS['insr_agcy_branch_phone']).", Branch Email=".trim($GLOBALS['insr_agcy_branch_email']).", Contact Person=".trim($GLOBALS['insr_agcy_contact_person']).", Contact Designation=".trim($GLOBALS['insr_agcy_contact_designation']).", Contact Email=".trim($GLOBALS['insr_agcy_contact_email']).", Contact Phone=".trim($GLOBALS['insr_agcy_contact_phone']).", Remarks=".trim($GLOBALS['insr_agcy_remarks']).", Is Active=".trim($GLOBALS['active']).", Insurance Agency Client Id=".trim($GLOBALS['insr_agcy_client_id']));
	
						$GLOBALS['outcome']="Insurance Agency ".'"'.trim($GLOBALS['insr_agcy']) .'"'." updated successfully";
							$GLOBALS['temp'] = $GLOBALS['insr_agcy_client_id'];
							$GLOBALS['active']=null;
				}
			}
			else	//insert
			{
				if($this->insurance_agency_model->InsertOrUpdateOrdelete_vts_insurance_agency(null,$data,'insert'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], INSURANCE_AGENCY,
								"Created New Insurance Agency: Insurance Agency=".trim($GLOBALS['insr_agcy']).",Insurance Agency Address=".trim($GLOBALS['insr_agcy_address']).", Insurance Agency PostCode=".trim($GLOBALS['insr_agcy_post_code']).", Branch Phone=".trim($GLOBALS['insr_agcy_branch_phone']).", Branch Email=".trim($GLOBALS['insr_agcy_branch_email']).", Contact Person=".trim($GLOBALS['insr_agcy_contact_person']).", Contact Designation=".trim($GLOBALS['insr_agcy_contact_designation']).", Contact Email=".trim($GLOBALS['insr_agcy_contact_email']).", Contact Phone=".trim($GLOBALS['insr_agcy_contact_phone']).", Remarks=".trim($GLOBALS['insr_agcy_remarks']).", Is Active=".trim($GLOBALS['active']).", Insurance Agency Client Id=".trim($GLOBALS['insr_agcy_client_id']));
	
							$GLOBALS['outcome']="New Insurance Agency ".'"'.trim($GLOBALS['insr_agcy']).'"'." created successfully";
							$GLOBALS['temp'] = $GLOBALS['insr_agcy_client_id'];
							$GLOBALS['active']=null;
				}
			}
			$this->table_pagination(md5($GLOBALS['temp']));
		}
		}
	}
	
	/*
	 * This function is to validate the branch code field
	 */
	public function check_insurance_branch_email($ins_brch_eml)
	{
		if(!filter_var($ins_brch_eml, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['insr_agcy_branch_email']=null;
			$this->form_validation->set_message('check_insurance_branch_email', 'Invalid email format');
			return FALSE;
		}
		$GLOBALS['insr_agcy_branch_email']=(null!=trim($this->input->post('InsuranceAgencyBranchEmail'))?trim($this->input->post('InsuranceAgencyBranchEmail')):null);
		return true;
	}
	
	/*
	 * This function is to validate the insurance contact email field
	 */
	public function check_insurance_contact_email($ins_cnt_eml)
	{
		if(!filter_var($ins_cnt_eml, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['insr_agcy_contact_email']=null;
			$this->form_validation->set_message('check_insurance_contact_email', 'Invalid email format');
			return FALSE;
		}
		$GLOBALS['insr_agcy_contact_email']=(null!=trim($this->input->post('InsuranceAgencyContactEmail'))?trim($this->input->post('InsuranceAgencyContactEmail')):null);
		return true;
	}
	
	/*
	 * This function is to validate the postal code field
	 */
	public function check_insurance_postal_code($post_cd)
	{
		$GLOBALS['insr_agcy_post_code']=null;
		if (!preg_match("/^[0-9]+$/", $post_cd))
		{
			$this->form_validation->set_message ( 'check_insurance_postal_code', '%s should be a numeric value' );
			return FALSE;
		}
		$GLOBALS['insr_agcy_post_code']=(null!=trim($this->input->post('InsuranceAgencyPostCode'))?trim($this->input->post('InsuranceAgencyPostCode')):null);
		return true;
	}
	
	/*
	 * This function is to validate the client id field
	 */
	public function check_cnt_id($cnt_id)
	{
		if (empty($cnt_id))
		{
			$this->form_validation->set_message('check_cnt_id', '%s required');
			return FALSE;
		}
		$GLOBALS['insr_agcy_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
		return true;
	}
	
	/*
	 * This function is to validate the contact person field
	 */
	public function check_contact_person($insr_cnt_prsn)
	{
		$GLOBALS['insr_agcy_contact_person']=null;
		$insr_cnt_prsn=str_replace(" ", "", $insr_cnt_prsn);
		
		if(preg_match("/[^a-zA-Z]/", $insr_cnt_prsn))//([!@^&*_\-{}`~<>,\.?%$#\*]\+)
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Please enter valid name' );
			return FALSE;
		}
		else if (strlen($insr_cnt_prsn) < 4 ) // strlen($insr_acy) > 20  Check whether the Agency name is in between 5 and 20 characters
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Should be greater than 3 characters' );
			return FALSE;
		}
		$GLOBALS['insr_agcy_contact_person']=(null!=trim($this->input->post('InsuranceAgencyContactPerson'))?trim($this->input->post('InsuranceAgencyContactPerson')):null);
		return true;
	}
	
	/*
	 * This function is to validate the contact designation
	 */
	public function check_insurance_contact_desig($insr_cnt_des)
	{
		$GLOBALS['insr_agcy_contact_designation']=null;
		$insr_cnt_des=str_replace(" ", "", $insr_cnt_des);
		
		if(preg_match("/[^a-zA-Z]/", $insr_cnt_des))//([!@^&*_\-{}`~<>,\.?%$#\*]\+)
		{
			$this->form_validation->set_message ( 'check_insurance_contact_desig', 'Please enter valid designation' );
			return FALSE;
		}
		else if (strlen($insr_cnt_des) < 4 ) // strlen($insr_acy) > 20  Check whether the contact designation is in between 5 and 20 characters
		{
			$this->form_validation->set_message ( 'check_insurance_contact_desig', 'Should be greater than 3 characters' );
			return FALSE;
		}
		$GLOBALS['insr_agcy_contact_designation']=(null!=trim($this->input->post('InsuranceAgencyContactDesignation'))?trim($this->input->post('InsuranceAgencyContactDesignation')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_insurance_agency name
	 */
	public function check_insurance_agency($insr_acy)
	{
		$GLOBALS['insuranceAgencyList']=$this->insurance_agency_model->get_vts_insurance_agency(null,null,null,md5($GLOBALS['temp']));
		$GLOBALS['insr_agcy']=null;
		$insr_acy1=str_replace(" ", "", $insr_acy);
		if (empty($insr_acy1)) //Check whether the Agency name field is empty
		{
			$this->form_validation->set_message('check_insurance_agency', '%s required');
			return FALSE;
		}
		else if(preg_match("/[^a-zA-Z]/", $insr_acy1))//([!@^&*_\-{}`~<>,\.?%$#\*]\+)
		{
			$this->form_validation->set_message ( 'check_insurance_agency', 'Please enter valid name' );
			return FALSE;
		}
		else if (strlen($insr_acy1) < 4 ) // strlen($insr_acy) > 20  Check whether the Agency name is in between 5 and 20 characters
		{
			$this->form_validation->set_message ( 'check_insurance_agency', 'Agency Name should be greater than 3 characters' );
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['insuranceAgencyList'] as $row ) {
				if ($GLOBALS ['insr_agcy_id'] == null) {
					if (trim ( strtoupper ( $row ['insurance_agency'] ) ) == strtoupper ( $insr_acy )) // Check whether insurance agency already exist in vts_insurance_agency table
					{
						$GLOBALS['outcome_with_db_check']="Insurancy Agency ".'"'.trim($row['insurance_agency']).'"'." already exist";
						$this->form_validation->set_message('check_insurance_agency', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['insurance_agency'] ) ) == strtoupper ( $insr_acy ) && $GLOBALS ['insr_agcy_id'] != trim ( $row ['insurance_agency_id'] )) // Check whether Insurance agency already exist in vts_insurance_agency table while update
					{
						$GLOBALS['outcome_with_db_check']="Insurance Agency ".'"'.trim($row['insurance_agency']).'"'." already exist";
						$this->form_validation->set_message('check_insurance_agency', '');
						return FALSE;
					}
				}
			}
		}
		$GLOBALS['insr_agcy']=(null!=trim($this->input->post('InsuranceAgency'))?trim($this->input->post('InsuranceAgency')):null);
		return true;
	}

	/*
	 * This function is the callback function of form validation, it is used to validate the vts_insurance_agency Branch Phone
	 */
	public function check_insurance_branch_phone($insr_branch_phone)
	{
		$GLOBALS['insr_agcy_branch_phone']=null;
		
		if (empty($insr_branch_phone)) //Check whether the Agency branch phone number field is empty
		{
			$this->form_validation->set_message('check_insurance_branch_phone', 'Branch Phone required');
			return FALSE;
		}
		else if (!is_numeric($insr_branch_phone)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_insurance_branch_phone', 'Phone number should be numeric' );
			return FALSE;
		}
		else if (strlen($insr_branch_phone) < 10 || strlen($insr_branch_phone) > 10) // Check whether the Agency branch number should allow only 10 numbers
		{
			$this->form_validation->set_message ( 'check_insurance_branch_phone', 'Please enter valid phone number' );
			return FALSE;
		}
		else if(preg_match('$\.$', $insr_branch_phone))
		{
			$this->form_validation->set_message ( 'check_insurance_branch_phone', 'Please enter valid phone number' );
			return FALSE;
		}
		$GLOBALS['insr_agcy_branch_phone']=(null!=trim($this->input->post('InsuranceAgencyBranchPhone'))?trim($this->input->post('InsuranceAgencyBranchPhone')):null);
		return true;
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_insurance_agency Contact Phone
	 */
	public function check_insurance_contact_phone($insr_contact_phone)
	{
		$GLOBALS['insr_agcy_contact_phone']=null;
	
		if (empty($insr_contact_phone)) //Check whether the Agency branch phone number field is empty
		{
			$this->form_validation->set_message('check_insurance_contact_phone', '%s required');
			return FALSE;
		}
		else if (!is_numeric($insr_contact_phone)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_insurance_contact_phone', 'Phone number should be numeric' );
			return FALSE;
		}
		else if (strlen($insr_contact_phone) < 10 || strlen($insr_contact_phone) > 10) // Check whether the Agency branch number should allow only 10 numbers
		{
			$this->form_validation->set_message ( 'check_insurance_contact_phone', 'Please enter 10 digit valid phone number' );
			return FALSE;
		}
		else if(preg_match('$\.$', $insr_contact_phone))
		{
			$this->form_validation->set_message ( 'check_insurance_contact_phone', 'Please enter valid phone number' );
			return FALSE;
		}
		$GLOBALS['insr_agcy_contact_phone']=(null!=trim($this->input->post('InsuranceAgencyContactPhone'))?trim($this->input->post('InsuranceAgencyContactPhone')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_insurance_agency
	 */
	public function editOrDelete_vts_insurance_agency($id,$opt,$IaId)
	{
// 		$posted=$opt;//$this->input->get_post('insurance_agency_id');
// 		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$opt;			//it hold the operation to perform(i.e. edit or delete).
		$InsuranceAgencyId=$IaId;			//it hold the vehicle group id to edit or delete.
	
		$row=$this->insurance_agency_model->get_vts_insurance_agency(null,null,$InsuranceAgencyId,$id);
		if($operation==md5('edit'))
		{
			$operation=="edit";
			$GLOBALS=array('insr_agcy_id'=>trim($row['insurance_agency_id']),'insr_agcy'=>trim($row['insurance_agency']),'insr_agcy_address'=>trim($row['insurance_agency_address']),'insr_agcy_post_code'=>trim($row['insurance_agency_postcode']),'insr_agcy_branch_phone'=>trim($row['insurance_agency_branchphone']),'insr_agcy_branch_email'=>trim($row['insurance_agency_branchemail']),'insr_agcy_contact_person'=>trim($row['insurance_agency_contactperson']),'insr_agcy_contact_designation'=>trim($row['insurance_agency_contactdesignation']),'insr_agcy_contact_email'=>trim($row['insurance_agency_contactemail']),'insr_agcy_contact_phone'=>trim($row['insurance_agency_contactphone']),'insr_agcy_remarks'=>trim($row['insurance_agency_remarks']),'active'=>trim($row['insurance_agency_isactive']),'insr_agcy_client_id'=>trim($row['insurance_agency_client_id']));//It used to fill up the textbox in view.
				
		}
		else if($operation==md5('delete'))
		{
			try
			{
				if($this->insurance_agency_model->InsertOrUpdateOrdelete_vts_insurance_agency($InsuranceAgencyId,null,'delete'))
				{
					$GLOBALS['outcome']="Insurance Agency ".'"'.trim($row['insurance_agency']).'"'." Deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], INSURANCE_AGENCY,
								"Insurance Agency deleted: Insurance Agency ID=".trim($GLOBALS['insr_agcy_id']));
				}else
				{
					$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['insurance_agency'] ).'"' . " Can't able to delete(Referred some where else)";
				}
					
			}catch (Exception $ex){
				$this->table_pagination($id);
			}
		}
		$this->table_pagination($id);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id=null,$pageNo=0)
	{
		if($cl_id!=null)
		{
			$GLOBALS['insr_agcy_client_id']=$cl_id;
		}
		$GLOBALS['insuranceAgencyList']=$this->insurance_agency_model->get_vts_insurance_agency(null,null,null,$GLOBALS['insr_agcy_client_id']);
		$GLOBALS['ID'] = $this->session->userdata('login');
		$config['base_url'] = base_url("index.php/insurance_agency_ctrl/table_pagination/".$GLOBALS['insr_agcy_client_id']);
		$GLOBALS['clientList']=$this->insurance_agency_model->get_allClients();
		
		$config['total_rows'] = count($GLOBALS['insuranceAgencyList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['insuranceAgencyList']=$this->insurance_agency_model->get_vts_insurance_agency(ROW_PER_PAGE,$pageNo,null,$GLOBALS['insr_agcy_client_id']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('insurance_agency_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_route_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('vehicle_route_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];	
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		$GLOBALS['clientList']=array();
		$GLOBALS['clientList']=$this->vehicle_route_model->get_allClients();
		if($GLOBALS['sessUserID']==AUTOGRADE_USER)
			$GLOBALS['clientID']=(isset($GLOBALS['clientList'][0]['client_id']))?$GLOBALS['clientList'][0]['client_id']:$GLOBALS['sessionData']['sess_clientid'];
		else 
			$GLOBALS['clientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['VhRouteList']=array();
		$GLOBALS['routeID']=null;
		$GLOBALS['routeName']=null;
		$GLOBALS['routeUserDefineID']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['geofence']=NOT_ACTIVE;
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function home($cl_id)
	{
		$this->table_pagination($cl_id);
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function route_name_validation()
	{
		$GLOBALS['checkSelectedClientID']=(null!=($this->input->post('OnClientID'))?$this->input->post('OnClientID'):null);
		if($GLOBALS['checkSelectedClientID']==-1)
		{
			$GLOBALS['routeID']=(null!=($this->input->post('RouteID'))?$this->input->post('RouteID'):null);
			$GLOBALS['routeUserDefineID']=(null!=($this->input->post('RouteUserDefineID'))?$this->input->post('RouteUserDefineID'):null);
			$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null);
			$GLOBALS['routeName']=(null!=(trim($this->input->post('RouteName',true)))?trim($this->input->post('RouteName',true)):null);
			$GLOBALS['geofence']=(null!=($this->input->post('GeoFence'))?ACTIVE:NOT_ACTIVE);
			$GLOBALS['active']=(null!=($this->input->post('Active'))?ACTIVE:NOT_ACTIVE);
			$this->form_validation->set_message('required', '%s required');
			$this->form_validation->set_rules('RouteName', 'Route Name', 'callback_route_name');
			$this->form_validation->set_rules('RouteUserDefineID', 'User Define Route ID','callback_route_userdefineid');
			if ($this->form_validation->run() != FALSE)
			{
				$data['route_client_id']=$GLOBALS['clientID'];
				$data['route_user_define_id']=$GLOBALS['routeUserDefineID'];
				$data['route_name']=$GLOBALS['routeName'];
				$data['route_is_geofence']=$GLOBALS['geofence'];
				$data['route_is_active']=$GLOBALS['active'];				
				if($GLOBALS['routeID']==null)//insert
				{
					$id=$this->vehicle_route_model->insert_edit_route($data);
					if($id!=0)
					{
						$GLOBALS['routeID']=$id;
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE,
									"New Route Name:"." Route ID=".$id.",Router User ID:".$GLOBALS['routeUserDefineID'].",Route Name=".trim($data['route_name']).", Route Active=".trim($data['route_is_active']).", Client ID=".$data['route_client_id']);
						$GLOBALS['outcome']='<div style="color: green;">New route name created successfully.</div>';
					}
					else{$GLOBALS['outcome']='<div style="color: red;">New route name created failed.</div>';}
				}elseif($GLOBALS['routeID']!=null)//edit
				{
					$status=$this->vehicle_route_model->insert_edit_route($data,$GLOBALS['routeID'],"update");
					if($status!=0)
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE,
									"Update Route Name:"." Route ID=".$GLOBALS['routeID'].",Router User ID:".$GLOBALS['routeUserDefineID'].",Route Name=".trim($data['route_name']).", Route Active=".trim($data['route_is_active']).", Client ID=".$data['route_client_id']);
						$GLOBALS['outcome']='<div style="color: green;">Updated successfully.</div>';					
					}
					else{$GLOBALS['outcome']='<div style="color: red;">Updation failed.</div>';}
				}
			}
		}
		else{
			$GLOBALS['clientID']=$GLOBALS['checkSelectedClientID'];
		}
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	/*
	 * This is the call back function to validate the route name
	 */
	public function route_name($p_route)
	{
		$p_route=trim($p_route);
		$GLOBALS['routeName']=null;
		if (empty($p_route)) //Check whether the route name field is empty
		{
			$this->form_validation->set_message('route_name', '%s required');
			return FALSE;
		}
		else if (is_numeric($p_route)) // Check whether the given route name is numeric.
		{		
			$this->form_validation->set_message ( 'route_name', '%s should not be numeric' );
			return FALSE;
		}
		else{
			$GLOBALS['routeName']=$p_route;
			return true;
		}
	}
	/*
	 * 
	 */
	public function route_userdefineid($p_user_define_routeid){
		$p_user_define_routeid=trim($p_user_define_routeid);
		$GLOBALS['routeUserDefineID']=null;
		if(empty($p_user_define_routeid))
		{
			$this->form_validation->set_message('route_userdefineid','%s required');
			return false;
		}		
		else{
			$GLOBALS['routeUserDefineID']=$p_user_define_routeid;
			return true;
		}
	}
	/*
	 * This function is used to edit the route details
	 * @param
	 *  $p_routeID - route id
	 */
	public function edit_routedetails($p_routeID)
	{
		$result=$this->vehicle_route_model->get_VhRouteList(null,null,null,$p_routeID);
		if($result!=null)
		{
			$GLOBALS['clientID']=$result[0]['route_client_id'];
			$GLOBALS['routeID']=$result[0]['route_id'];
			$GLOBALS['routeUserDefineID']=$result[0]['route_user_define_id'];
			$GLOBALS['routeName']=$result[0]['route_name'];
			$GLOBALS['geofence']=$result[0]['route_is_geofence'];
			$GLOBALS['active']=$result[0]['route_is_active'];			
		}
		$this->table_pagination(md5($GLOBALS['clientID']));
	}
	public function table_pagination($client=null,$pageNo=0)
	{
		$GLOBALS['clientID']=$client;
		$GLOBALS['VhRouteList']=$this->vehicle_route_model->get_VhRouteList($client, ROW_PER_PAGE, $pageNo);
		$config['uri_segment']=URI_SEGMENT_FOR_FOUR;
		$config['base_url'] = base_url("index.php/vehicle_route_ctrl/table_pagination/".$client);
		$config['total_rows'] = count($this->vehicle_route_model->get_VhRouteList($client));
		$config['per_page']=ROW_PER_PAGE;
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('vehicle_route_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	/*
	 * This function is usde to render the vehicle_route_map_view.php view
	 */
	public function map_view($p_routeID)
	{
		$this->common_model->menu_display();
		$result=$this->vehicle_route_model->get_VhRouteList(null,null,null,$p_routeID);
		if($result!=null)
		{
			$GLOBALS['clientID']=$result[0]['route_client_id'];
			$GLOBALS['routeID']=$result[0]['route_id'];
			$GLOBALS['routeUserDefineID']=$result[0]['route_user_define_id'];
			$GLOBALS['routeName']=$result[0]['route_name'];
			$GLOBALS['geofence']=$result[0]['route_is_geofence'];
			$GLOBALS['active']=$result[0]['route_is_active'];
		}		
		$this->load->view('header_footer/header_vehicle_route_map',$GLOBALS);
		$this->load->view('vehicle_route_map_view',$GLOBALS);	
	}
	/*
	 * This function is used to get the current rout id details
	 */
	public function get_route_details($p_routeID)
	{
		$result=$this->vehicle_route_model->get_routeDetails($p_routeID);
		echo json_encode($result);
	}
	/*
	 * This function is used to save the vehicle route markers
	 */
	public function save_marker()
	{
		$json_markerDetails=$this->input->post('marker_details');
		//log_message("debug",'***$json_markerDetails***'.$json_markerDetails);		
		$vhRouteMarkerDetails=json_decode(urldecode($json_markerDetails));
		if(count($vhRouteMarkerDetails->details) > 0)
		{		
			if($this->vehicle_route_model->insert_stop_list($vhRouteMarkerDetails))
			{
				$routeID=$vhRouteMarkerDetails->route_id;
				$stop_ids=$this->vehicle_route_model->get_routeDetails($routeID);
				//log_message('debug',"***Result for ($routeID)".print_r($stop_ids,true));
				$id_list="";
				if($stop_ids!=null)
				{
					foreach($stop_ids as $stpid){
						$id_list.='{"stopid":"'.$stpid['stop_id'].'", "is_busstop":"'.$stpid['is_busstop'].'", "timelimit":"'.$stpid['timelimit'].'"},';
					}
					$id_list=trim($id_list,',');
					//log_message('debug','******'.$id_list);
				}
				if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
				{
					$clientID=$vhRouteMarkerDetails->client_id;
					
					$routeName=$vhRouteMarkerDetails->route_name;
					$latLngDetails=count($vhRouteMarkerDetails->details);
					$this->common_model->insert_event_value($GLOBALS['sessionData']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE,
							"Route Insert Or Update:"." Route ID=".$routeID.", Route Name=".$routeName.", Total No. Of Points=".$latLngDetails.", Client ID=".$clientID);
				}
				if($vhRouteMarkerDetails->details[0]->stop_id=="") {
					//echo('{"msg":"Press OK to save the Route.", "status":"Yes", "stop_id":['.$id_list.']}');
					echo('{"msg":"Your route has been set successfully.", "status":"Yes", "stop_id":['.$id_list.']}');
				} else {
					echo('{"msg":"Your route has been reset successfully.", "status":"Yes", "stop_id":['.$id_list.']}');
				}
			}
			else
				echo('{"msg":"Route insertion failed.", "status":"No", "stop_id":['.$id_list.']}');
		}
		else{echo('{"msg":"Please plot the Route to save.", "status":"No", "stop_id":[]}');}
	}
	/*
	 * This function is used to remove all the markers from
	 * the map
	 */
	public function delete_allMarkers($routeID)
	{
		if($this->vehicle_route_model->delete_markers($routeID))
		{
			if(trim($GLOBALS['eventLogRequired'])==REQUIRED)
			{				
				$this->common_model->insert_event_value($GLOBALS['sessionData']['sess_userid'], $GLOBALS['ip'], VEHICLE_ROUTE,
						"Route Removed:"." Route ID=".$routeID);
			}
			echo('{"msg":"Markers have been removed successfully.", "status":"Yes"}');
		}
		else {
			echo('{"msg":"All markers can\'t remove.", "status":"No"}');
		}
			
	}
	
}
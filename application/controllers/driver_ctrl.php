<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('driver_model');
		$this->load->helper(array('form', 'url'));
		
		//the below data will save to the vts_driver table
		$GLOBALS['dvr_trip_id']=null;
		$GLOBALS['dvr_id']=null;
		$GLOBALS['dvr_client_id']=null;
		$GLOBALS['dvr_name']=null;
		$GLOBALS['dvr_oldid']=null;
		$GLOBALS['dvr_rfid']=null;
		$GLOBALS['dvr_rfvalue']=null;
		$GLOBALS['dvr_drivergroup_id']=null;
		$GLOBALS['dvr_dob']=null;
		$GLOBALS['dvr_address']=null;
		$GLOBALS['dvr_postcode']=null;
		$GLOBALS['dvr_phone']=null;
		$GLOBALS['dvr_mobile_number']=null;
		$GLOBALS['dvr_dl_no']=null;
		$GLOBALS['dvr_dl_details']=null;
		$GLOBALS['dvr_id_type']=null;
		$GLOBALS['dvr_id_no']=null;
		$GLOBALS['dvr_remarks']=null;
		$GLOBALS['active']=NOT_ACTIVE;		
		$GLOBALS['vehicleGroupList']=array();
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleList']=array();
		$GLOBALS['vehicleID']=null;
		$GLOBALS['isEdit']=NOT_ACTIVE;
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['todayDateTime']=$this->common_model->get_usertime();
		$GLOBALS['ID'] = $this->session->userdata('login');	
		$GLOBALS['driverList']=$this->driver_model->get_vts_driver(null,null,null,md5($GLOBALS['ID']['sess_clientid']));
		$GLOBALS['clientList']=$this->driver_model->get_allClients();
		$GLOBALS['driverGroupList']=$this->driver_model->get_driverGroup($GLOBALS['ID']['sess_clientid']);
		$GLOBALS['driverIdTypeList']=$this->driver_model->get_driverIdType();
		
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";

		
	}

	public function index()
	{
		$this->table_pagination(md5($GLOBALS['ID']['sess_clientid']));
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_dealer in database.
	 */
	public function vts_driver_validation()
	{
		
		$GLOBALS['temp']=(null!=($this->input->post('temp'))?$this->input->post('temp'):null);
		if($GLOBALS['temp']!="-1")
			$this->table_pagination(md5($GLOBALS['temp']));
		else
		{
			$GLOBALS['dvr_trip_id']=(null!=($this->input->post('TripID'))?$this->input->post('TripID'):null);
			$GLOBALS['vehicleGroupID']=(null!=($this->input->post('VehicleGroupID'))?$this->input->post('VehicleGroupID'):null);
			$GLOBALS['vehicleID']=(null!=($this->input->post('VehicleID'))?$this->input->post('VehicleID'):null);
			$GLOBALS['dvr_id']=(null!=($this->input->post('DriverId'))?$this->input->post('DriverId'):null);	
			$GLOBALS['dvr_name']=(null!=trim($this->input->post('DriverName'))?trim($this->input->post('DriverName')):null);
			$GLOBALS['dvr_rfid']=(null!=trim($this->input->post('DriverRFID'))?trim($this->input->post('DriverRFID')):null);
			$GLOBALS['dvr_oldid']=(null!=trim($this->input->post('OldId'))?trim($this->input->post('OldId')):null);
			$GLOBALS['dvr_drivergroup_id']=(null!=trim($this->input->post('DriverGroup'))?trim($this->input->post('DriverGroup')):null);
	// 		$GLOBALS['dvr_dob']=(null!=trim($this->input->post('DriverDateOfBirth'))?trim($this->input->post('DriverDateOfBirth')):null);
			$GLOBALS['dvr_address']=(null!=trim($this->input->post('DriverAddress'))?trim($this->input->post('DriverAddress')):null);
			$GLOBALS['dvr_postcode']=(null!=trim($this->input->post('DriverPostCode'))?trim($this->input->post('DriverPostCode')):null);
			$GLOBALS['dvr_phone']=(null!=trim($this->input->post('DriverPhone'))?trim($this->input->post('DriverPhone')):null);
			$GLOBALS['dvr_mobile_number']=(null!=trim($this->input->post('DriverMobileNumber'))?trim($this->input->post('DriverMobileNumber')):null);
			$GLOBALS['dvr_dl_no']=(null!=trim($this->input->post('DriverDLNo'))?trim($this->input->post('DriverDLNo')):null);
			$GLOBALS['dvr_dl_details']=(null!=trim($this->input->post('DriverDLDetails'))?trim($this->input->post('DriverDLDetails')):null);
			$GLOBALS['dvr_id_type']=(null!=trim($this->input->post('DriverIDType'))?trim($this->input->post('DriverIDType')):null);
			$GLOBALS['dvr_id_no']=(null!=trim($this->input->post('DriverIDNo'))?trim($this->input->post('DriverIDNo')):null);
			$GLOBALS['dvr_remarks']=(null!=trim($this->input->post('DriverRemarks'))?trim($this->input->post('DriverRemarks')):null);
			
			$GLOBALS['dobyear'] = ("yyyy"!=trim($this->input->post('dobyear'))?trim($this->input->post('dobyear')):null);
			$GLOBALS['dobmonth'] = ("mm"!=trim($this->input->post('dobmonth'))?trim($this->input->post('dobmonth')):null);
			$GLOBALS['dobday'] = ("dd"!=trim($this->input->post('dobday'))?trim($this->input->post('dobday')):null);
			//log_message("debug", "year--".$GLOBALS['dobday']."  month---".$GLOBALS['dobmonth']."  date---".$GLOBALS['dobday']);
			
			if($GLOBALS['dobyear'] != null && ($GLOBALS['dobmonth'] == null || $GLOBALS['dobday'] ==null ))
			{
				//log_message("debug", "dddd1=== ".$GLOBALS['dobyear']."  month---".$GLOBALS['dobmonth']."  date---".$GLOBALS['dobday']);
				$this->form_validation->set_rules('dobmonth', 'dob month', 'callback_check_dob');
			}
			else {
				$GLOBALS['dvr_dob'] = $GLOBALS['dobyear'].'-'.$GLOBALS['dobmonth'].'-'.$GLOBALS['dobday'];
			}
			$GLOBALS['active']=(null!=($this->input->post('DriverIsActive'))?$this->input->post('DriverIsActive'):NOT_ACTIVE);
			
			
			if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				
				$GLOBALS['dvr_client_id']=(null!=($this->input->post('ClientName'))?$this->input->post('ClientName'):null);
			}
			else
			{
				$GLOBALS['dvr_client_id']=$GLOBALS['ID']['sess_clientid'];
			}
			$GLOBALS['temp']=$GLOBALS['dvr_client_id'];
			$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
		
			$this->form_validation->set_rules('DriverName', 'Name', 'callback_check_driver_name');
			$this->form_validation->set_rules('DriverGroup', 'Driver Group', 'callback_check_driver_group');
			if($GLOBALS['dvr_id_no']!=null)
				$this->form_validation->set_rules('DriverIDType', 'ID Type', 'callback_check_driver_id_type');
			$this->form_validation->set_rules('DriverMobileNumber','Mobile Number','callback_check_driver_mobile_nor');
			$this->form_validation->set_rules('DriverIDNo', 'ID Number', 'callback_check_id_nor');
			
			if (!empty($GLOBALS['dvr_postcode']))
			{
				$this->form_validation->set_rules('DriverPostCode','Postal Code','callback_check_driver_post_code');
			}
			if (!empty($GLOBALS['dvr_phone']))
			{
				$this->form_validation->set_rules('DriverPhone','Phone','callback_check_driver_phone');
			}
			if (!empty($GLOBALS['dvr_dl_no']))
			{
				$this->form_validation->set_rules('DriverDLNo','DL Number','callback_check_driver_dl_nor');
			}
			
			if($GLOBALS['ID']['sess_clientid'] == AUTOGRADE_USER || $GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			{
				$this->form_validation->set_rules('ClientName', 'Client Name', 'callback_check_driver_cnt_id');
			}
	
			if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_driver table.
			{
				$this->table_pagination(md5($GLOBALS['dvr_client_id']));
			}
			else
			{
				$data['driver_client_id']=$GLOBALS['dvr_client_id'];
				$data['driver_drivergroup_id']=$GLOBALS['dvr_drivergroup_id'];
				$data['driver_name']=$GLOBALS['dvr_name'];
				$data['driver_rf_id']=$GLOBALS['dvr_rfid'];

				if($GLOBALS['dobyear'] != null && $GLOBALS['dobmonth'] != null && $GLOBALS['dobday'] !=null )
					$data['driver_dob']=$GLOBALS['dvr_dob'];
				$data['driver_address']=$GLOBALS['dvr_address'];
				$data['driver_postcode']=$GLOBALS['dvr_postcode'];
				$data['driver_phone']=$GLOBALS['dvr_phone'];
				$data['driver_mobile']=$GLOBALS['dvr_mobile_number'];
				$data['driver_dl_number']=$GLOBALS['dvr_dl_no'];
				$data['driver_dl_details']=$GLOBALS['dvr_dl_details'];
				$data['driver_id_type']=$GLOBALS['dvr_id_type'];
				$data['driver_id_number']=$GLOBALS['dvr_id_no'];
				$data['driver_remarks']=$GLOBALS['dvr_remarks'];
				$data['driver_isactive']=$GLOBALS['active'];
				
				if($GLOBALS['dvr_trip_id']==null)
				{
					$tripdata['trip_register_driver_id']=($GLOBALS['dvr_id']!=null)?$GLOBALS['dvr_id']:'-1';
					$tripdata['trip_register_vehicle_id']=$GLOBALS['vehicleID'];					
					$tripdata['trip_register_time_start']=$GLOBALS['todayDateTime'];
					$tripdata['trip_register_remarks']='This trip was registered while Driver creation.';
					$tripdata['trip_register_client_id']=$GLOBALS['dvr_client_id'];
				}
				
				if($GLOBALS['dvr_id']!=null)	//edit
				{					
					$data['driver_id']=$GLOBALS['dvr_id'];
					if ($this->driver_model->InsertOrUpdateOrdelete_vts_driver($GLOBALS['dvr_id'],$data,'edit'))
					{	
						if($GLOBALS['dvr_trip_id']==null)
						{
							$this->driver_model->InsertOrUpdateOrdelete_vts_driver_Tripreg(null, $tripdata, 'insert');
							if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
								$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER,
										"Updated Driver & insert Trip Reg.: Driver ID=".trim($GLOBALS['dvr_id']).", Driver Client Id=".trim($GLOBALS['dvr_client_id']).",Driver Name=".trim($GLOBALS['dvr_name']).", Driver Group Id=".trim($GLOBALS['dvr_drivergroup_id']).", Driver Date od Birth=".trim($GLOBALS['dvr_dob']).", Driver Address=".trim($GLOBALS['dvr_address']).", Driver Post Code=".trim($GLOBALS['dvr_postcode']).", Driver Phone=".trim($GLOBALS['dvr_phone']).", Driver Mobile No=".trim($GLOBALS['dvr_mobile_number']).", Driver DL No=".trim($GLOBALS['dvr_dl_no']).", Driver ID Details=".trim($GLOBALS['dvr_dl_details']).", Driver ID Type=".trim($GLOBALS['dvr_id_type']).", Driver ID No=".trim($GLOBALS['dvr_id_no']).", Driver Remarks=".trim($GLOBALS['dvr_remarks']).", Driver Is Active=".trim($GLOBALS['active']));
						}					
						else if($GLOBALS['dvr_trip_id']!=null)
						{	
							if($GLOBALS['vehicleID']==null)
							{
								$tripdata['trip_register_remarks']='This trip was closed while remove vehicle in driver creation screen.';
								$tripdata['trip_register_time_end']=$GLOBALS['todayDateTime'];
							}
							else
							{
								$tripdata['trip_register_remarks']='This trip was registered while driver was update.';
								$tripdata['trip_register_vehicle_id']=$GLOBALS['vehicleID'];
							}
							$this->driver_model->InsertOrUpdateOrdelete_vts_driver_Tripreg($GLOBALS['dvr_trip_id'], $tripdata, 'edit');
							
							if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
								$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER,
										"Updated Driver & Trip Reg.: Driver ID=".trim($GLOBALS['dvr_id']).", Driver Client Id=".trim($GLOBALS['dvr_client_id']).",Driver Name=".trim($GLOBALS['dvr_name']).", Driver Group Id=".trim($GLOBALS['dvr_drivergroup_id']).", Driver Date od Birth=".trim($GLOBALS['dvr_dob']).", Driver Address=".trim($GLOBALS['dvr_address']).", Driver Post Code=".trim($GLOBALS['dvr_postcode']).", Driver Phone=".trim($GLOBALS['dvr_phone']).", Driver Mobile No=".trim($GLOBALS['dvr_mobile_number']).", Driver DL No=".trim($GLOBALS['dvr_dl_no']).", Driver ID Details=".trim($GLOBALS['dvr_dl_details']).", Driver ID Type=".trim($GLOBALS['dvr_id_type']).", Driver ID No=".trim($GLOBALS['dvr_id_no']).", Driver Remarks=".trim($GLOBALS['dvr_remarks']).", Driver Is Active=".trim($GLOBALS['active']).", Trip ID=".$GLOBALS['dvr_trip_id']." , Vehicle ID=".$GLOBALS['vehicleID']);
						}
						else
						{
							if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
								$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER,
										"Updated Driver: Driver ID=".trim($GLOBALS['dvr_id']).", Driver Client Id=".trim($GLOBALS['dvr_client_id']).",Driver Name=".trim($GLOBALS['dvr_name']).", Driver Group Id=".trim($GLOBALS['dvr_drivergroup_id']).", Driver Date od Birth=".trim($GLOBALS['dvr_dob']).", Driver Address=".trim($GLOBALS['dvr_address']).", Driver Post Code=".trim($GLOBALS['dvr_postcode']).", Driver Phone=".trim($GLOBALS['dvr_phone']).", Driver Mobile No=".trim($GLOBALS['dvr_mobile_number']).", Driver DL No=".trim($GLOBALS['dvr_dl_no']).", Driver ID Details=".trim($GLOBALS['dvr_dl_details']).", Driver ID Type=".trim($GLOBALS['dvr_id_type']).", Driver ID No=".trim($GLOBALS['dvr_id_no']).", Driver Remarks=".trim($GLOBALS['dvr_remarks']).", Driver Is Active=".trim($GLOBALS['active']));
						}
		
						$GLOBALS['outcome']="Driver ".'"'.trim($GLOBALS['dvr_name']) .'"'." Details updated successfully";
						$GLOBALS['temp']=$GLOBALS['dvr_client_id'];
						$GLOBALS['dvr_id']=null;
						$GLOBALS['dvr_drivergroup_id']=null;
						$GLOBALS['vehicleGroupID']=null;
						$GLOBALS['vehicleID']=null;
						$GLOBALS['dvr_id_type']=null;
						$GLOBALS['dvr_dob'] = null;
						$GLOBALS['active']=null;
						$GLOBALS['dvr_trip_id']=null;
		
					}
				}
				else	//insert
				{
					$drivID=$this->driver_model->InsertOrUpdateOrdelete_vts_driver(null,$data,'insert');
					if( $drivID != -1)
					{						
						if($GLOBALS['vehicleID']!=null && $GLOBALS['vehicleGroupID']!=null)
						{
							$tripdata['trip_register_driver_id']=$drivID;
							$this->driver_model->InsertOrUpdateOrdelete_vts_driver_Tripreg(null, $tripdata, 'insert');
							if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
								$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER,
										"Created New Driver + Assign Trip : Driver Client Id=".trim($GLOBALS['dvr_client_id']).", Driver ID=".$drivID.", Driver Name=".trim($GLOBALS['dvr_name']).", Driver Group Id=".trim($GLOBALS['dvr_drivergroup_id']).", Driver Date od Birth=".trim($GLOBALS['dvr_dob']).", Driver Address=".trim($GLOBALS['dvr_address']).", Driver Post Code=".trim($GLOBALS['dvr_postcode']).", Driver Phone=".trim($GLOBALS['dvr_phone']).", Driver Mobile No=".trim($GLOBALS['dvr_mobile_number']).", Driver DL No=".trim($GLOBALS['dvr_dl_no']).", Driver ID Details=".trim($GLOBALS['dvr_dl_details']).", Driver ID Type=".trim($GLOBALS['dvr_id_type']).", Driver ID No=".trim($GLOBALS['dvr_id_no']).", Driver Remarks=".trim($GLOBALS['dvr_remarks']).", Driver Is Active=".trim($GLOBALS['active']).", Vehicle ID=".$GLOBALS['vehicleID']);
						}
						else
						{
							if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
								$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER,
										"Created New Driver: Driver Client Id=".trim($GLOBALS['dvr_client_id']).", Driver ID=".$drivID.", Driver Name=".trim($GLOBALS['dvr_name']).", Driver Group Id=".trim($GLOBALS['dvr_drivergroup_id']).", Driver Date od Birth=".trim($GLOBALS['dvr_dob']).", Driver Address=".trim($GLOBALS['dvr_address']).", Driver Post Code=".trim($GLOBALS['dvr_postcode']).", Driver Phone=".trim($GLOBALS['dvr_phone']).", Driver Mobile No=".trim($GLOBALS['dvr_mobile_number']).", Driver DL No=".trim($GLOBALS['dvr_dl_no']).", Driver ID Details=".trim($GLOBALS['dvr_dl_details']).", Driver ID Type=".trim($GLOBALS['dvr_id_type']).", Driver ID No=".trim($GLOBALS['dvr_id_no']).", Driver Remarks=".trim($GLOBALS['dvr_remarks']).", Driver Is Active=".trim($GLOBALS['active']));
						}
						
								
								$GLOBALS['outcome']="New Driver ".'"'.trim($GLOBALS['dvr_name']).'"'." created successfully";
								$GLOBALS['temp']=$GLOBALS['dvr_client_id'];
								$GLOBALS['dvr_id']=null;
								$GLOBALS['dvr_drivergroup_id']=null;
								$GLOBALS['dvr_id_type']=null;
								$GLOBALS['dvr_dob'] = null;
								$GLOBALS['active']=null;
								$GLOBALS['vehicleID']=null;
								$GLOBALS['vehicleGroupID']=null;
	
					}
				}
				$this->table_pagination(md5($GLOBALS['temp']));
			}
		}
	}
	
	public function check_dob($dob)
	{
		$GLOBALS['dvr_dob'] = null;
		$GLOBALS['outcome_with_db_check']="Date Of Birth not valid";
		$this->form_validation->set_message('check_dob', '');
		return false;
	}
	
	public function check_driver_dob($dvr_dob)
	{
		$GLOBALS['dvr_dob']=null;
		if(preg_match('/[a-zA-Z]/', $dvr_dob))
		{
			$this->form_validation->set_message('check_driver_dob', 'Invalid Date');
			return FALSE;
		}
		$GLOBALS['dvr_dob']=(null!=trim($this->input->post('DriverDateOfBirth'))?trim($this->input->post('DriverDateOfBirth')):null);
		return true;
	} 
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_device IMEI name
	 */
	public function check_driver_name($dvr_nm)
	{
		$GLOBALS['driverList']=$this->driver_model->get_vts_driver(null,null,null,md5($GLOBALS['temp']));
		
		$GLOBALS['dvr_name']=null;
		$dvr_nm1=str_replace(" ", "", $dvr_nm);
		
		if (empty($dvr_nm1)) //Check whether the Driver Name field is empty
		{
			$this->form_validation->set_message('check_driver_name', '%s required');
			return FALSE;
		}
		else if (is_numeric($dvr_nm1)) // Check whether the given Device IMEI is numeric.
		{
			$this->form_validation->set_message ( 'check_driver_name', '%s should not be numeric' );
			return FALSE;
		}
		else if (strlen($dvr_nm1) < 3 || strlen($dvr_nm1) > 40 ) // Check whether the Driver Name should be inbetween 3 to 40 characters
		{
			$this->form_validation->set_message ( 'check_driver_name', 'Driver name is having limit of 3 t0 40' );
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $dvr_nm1))
		{
			$this->form_validation->set_message ( 'check_driver_name', 'Please enter valid name' );
			return FALSE;
		}
		else if(preg_match("/[+]/", $dvr_nm1))
		{
			$this->form_validation->set_message ( 'check_driver_name', 'Please enter valid name' );
			return FALSE;
		}
		else if(preg_match('/^[0-9]/', $dvr_nm1))
		{
			$this->form_validation->set_message ( 'check_driver_name', 'Please enter valid driver name' );
			return FALSE;
		}
		
		$GLOBALS['dvr_name']=(null!=trim($this->input->post('DriverName'))?trim($this->input->post('DriverName')):null);
		return true;
	}
	
	public function check_driver_post_code($dvr_pst_cd)
	{
	//log_message('debug', "post code");
		$GLOBALS['dvr_postcode']=null;
		if (!is_numeric($dvr_pst_cd)) 
		{
			$this->form_validation->set_message ( 'check_driver_post_code', '%s should be a numeric value' );
			return FALSE;
		}
		$GLOBALS['dvr_postcode']=(null!=trim($this->input->post('DriverPostCode'))?trim($this->input->post('DriverPostCode')):null);
		return true;
	}
	
	
	public function check_driver_phone($dvr_pne)
	{
	
		$GLOBALS['dvr_phone']=null;
		if (!is_numeric($dvr_pne))
		{
			$this->form_validation->set_message ( 'check_driver_phone', '%s should be a numeric value' );
			return FALSE;
		}
		$GLOBALS['dvr_phone']=(null!=trim($this->input->post('DriverPhone'))?trim($this->input->post('DriverPhone')):null);
		return true;
	}
	
	public function check_driver_mobile_nor($dvr_mbl_nor)
	{
		$GLOBALS['dvr_mobile_number']=null;
		if (empty($dvr_mbl_nor))
		{
			$this->form_validation->set_message('check_driver_mobile_nor', 'Please enter 10digit mobile number');
			return FALSE;
		}
		else if (!is_numeric($dvr_mbl_nor)) 
		{
			$this->form_validation->set_message ( 'check_driver_mobile_nor', '%s should be a numeric value' );
			return FALSE;
		}
		else if (preg_match("/([!@^&*_-{}`~<>,.?%\$#\*]+)/", $dvr_mbl_nor))
		{
			$this->form_validation->set_message ( 'check_driver_mobile_nor', 'Invalid Mobile Number' );
			return FALSE;
		}
		$GLOBALS['dvr_mobile_number']=(null!=trim($this->input->post('DriverMobileNumber'))?trim($this->input->post('DriverMobileNumber')):null);
		return true;
	}
	
	
	public function check_driver_group($dvr_gup)
	{
	
		$GLOBALS['dvr_drivergroup_id']=null;
		if (empty($dvr_gup))
		{
			$this->form_validation->set_message('check_driver_group', '%s required');
			return FALSE;
		}
		$GLOBALS['dvr_drivergroup_id']=(null!=trim($this->input->post('DriverGroup'))?trim($this->input->post('DriverGroup')):null);
		return true;
	}
	
	public function check_driver_id_type($dvr_id_type)
	{
	
		$GLOBALS['dvr_id_type']=null;
		if (empty($dvr_id_type))
		{
			$this->form_validation->set_message('check_driver_id_type', '%s required');
			return FALSE;
		}
		$GLOBALS['dvr_id_type']=(null!=trim($this->input->post('DriverIDType'))?trim($this->input->post('DriverIDType')):null);
		return true;
	}
	
	public function check_driver_cnt_id($dvr_drv_cnt_id)
	{

// 		$GLOBALS['dvr_client_id']=null;
		if (empty($dvr_drv_cnt_id))
		{
			$this->form_validation->set_message('check_driver_cnt_id', '%s required');
			return FALSE;
		}
		$GLOBALS['dvr_client_id']=(null!=trim($this->input->post('ClientName'))?trim($this->input->post('ClientName')):null);
		return true;
	}
	
	public function check_id_nor($dvr_id_nor)
	{
		$GLOBALS['dvr_id_no']=null;
		$dvr_id_nor=trim($dvr_id_nor);
		if (!empty($dvr_id_nor)) 
		{	
			if($GLOBALS['dvr_id_type']!=null)
			{
				$this->form_validation->set_message('check_id_nor', '%s required');
				return FALSE;
			}
			else if (!preg_match("/^[a-zA-Z0-9]+$/", $dvr_id_nor))
			{
				$this->form_validation->set_message('check_id_nor', 'Invalid ID number');
				return FALSE;
			}
			else if (strlen($dvr_id_nor) < 6) // Check whether the id number is lessthan 6 characters
			{
				$this->form_validation->set_message ( 'check_id_nor', 'ID number should not be less than 6digits' );
				return FALSE;
			}
		}
		$GLOBALS['dvr_id_no']=(null!=trim($this->input->post('DriverIDNo'))?trim($this->input->post('DriverIDNo')):null);
		return true;
	}
	
	public function check_driver_dl_nor($dvr_dl_nor)
	{
		$GLOBALS['dvr_dl_no']=null;
		if (!preg_match("/^[a-zA-Z0-9]+$/", $dvr_dl_nor)) //preg_match("/([!@^&*_-{}`~<>,.?%\$#\*]+)/", $dvr_dl_nor)
		{
			$this->form_validation->set_message('check_driver_dl_nor', 'Invalid DL Number');
			return FALSE;
		}
		$GLOBALS['dvr_dl_no']=(null!=trim($this->input->post('DriverDLNo'))?trim($this->input->post('DriverDLNo')):null);
		return true;
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and return back the list of
	 * vehicles in JSON format with out refresh the page.
	 *  $vhGrp - vehicel group id
	 * Return type - JSON string
	 */
	public function get_vhl_grp_vehicle($vhGrp=null)
	{
		$vhl_grp_vehicle="";
		$GLOBALS['todayDateTime']=$this->common_model->get_usertime();
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=json_encode($this->driver_model->get_all_vhGp_vehicle($vhGrp, $GLOBALS['todayDateTime']));
				
		}
		echo($vhl_grp_vehicle);
	}

	/*
	 * This function used to edit or delete the vts_vehicle_group
	 */
	public function editOrDelete_vts_driver($id,$opt,$dhId)
	{
// 		$posted=$this->input->get_post('driver_id');
// 		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$opt;			//it hold the operation to perform(i.e. edit or delete).
		$DriverId=$dhId;			//it hold the dealer id to edit or delete.
	
		$row=$this->driver_model->get_vts_driver(null,null,$DriverId,$id);
		if($row!=null)
		{
			$GLOBALS['dvr_trip_id']=trim($row['trip_id']);
			$GLOBALS['dvr_id']=trim($row['driver_id']);
			$GLOBALS['dvr_client_id']=trim($row['driver_client_id']);
			$GLOBALS['dvr_name']=trim($row['driver_name']);
			$GLOBALS['dvr_rfid']=trim($row['driver_rf_id']);
			if($GLOBALS['dvr_rfid'])
				$GLOBALS['dvr_rfvalue']=$this->driver_model->get_vts_rf_card($GLOBALS['dvr_client_id'],'id',$GLOBALS['dvr_rfid'], $GLOBALS['dvr_rfid']);
			$GLOBALS['dvr_oldid']=$GLOBALS['dvr_rfid'];
			$GLOBALS['dvr_drivergroup_id']=trim($row['driver_drivergroup_id']);
			$GLOBALS['dvr_dob']=trim($row['driver_dob']);
			$GLOBALS['dvr_address']=trim($row['driver_address']);
			$GLOBALS['dvr_postcode']=trim($row['driver_postcode']);
			$GLOBALS['dvr_phone']=trim($row['driver_phone']);
			$GLOBALS['dvr_mobile_number']=trim($row['driver_mobile']);
			$GLOBALS['dvr_dl_no']=trim($row['driver_dl_number']);
			$GLOBALS['dvr_dl_details']=trim($row['driver_dl_details']);
			$GLOBALS['dvr_id_type']=trim($row['driver_id_type']);
			$GLOBALS['dvr_id_no']=trim($row['driver_id_number']);
			$GLOBALS['dvr_remarks']=trim($row['driver_remarks']);
			$GLOBALS['active']=trim($row['driver_isactive']);//It used to fill up the textbox in view.
			$GLOBALS['vehicleGroupID']=trim($row['vehicle_group_id']);
			$GLOBALS['vehicleID']=trim($row['vehicle_id']);
			if($operation==md5('edit'))
			{
				$operation=="edit";			
				$GLOBALS['isEdit']=ACTIVE;
			}
			else if($operation==md5("delete"))
			{
				try
				{
					if($this->driver_model->InsertOrUpdateOrdelete_vts_driver($DriverId,null,'delete'))
					{
						$this->driver_model->InsertOrUpdateOrdelete_vts_driver_Tripreg($GLOBALS['dvr_trip_id'],null,'delete');
						$GLOBALS['outcome_with_db_check']="Driver ".'"'.trim($row['driver_name']).'"'." deleted";
							
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DRIVER,
									"Driver is deleted: Driver ID=".trim($DriverId));
					}else
					{
						//log_message('debug', "not delete driver");
						$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['driver_name'] ) .'"'. " Can't able to delete(Referred some where else)";
					}
						
				}catch (Exception $ex){
					$this->table_pagination($id);
		
				}
			}
		}
		$this->table_pagination($id);
	}

	public function get_rf_details()
	{		
		$result_set = array();
		if(isset($_GET['rf']))
		{
			log_message('error','**********'.$GLOBALS['ID']['sess_clientid']);
			$result=$this->driver_model->get_vts_rf_card($GLOBALS['ID']['sess_clientid'],'auto',$_GET['rf'],$_GET['opt']);
			if(count($result)>=1)
			{
				foreach ($result as $key => $value) {
					$result_set[]=array('id' => $value['rf_id'], 'value' => $value['rf_card_no'] );
				}
			}
		}
		echo json_encode($result_set);
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($cl_id=null,$pageNo=0)
	{
		if($cl_id!=null)
		{
			$GLOBALS['dvr_client_id']=$cl_id;
		}		
		$GLOBALS['vehicleGroupList']=$this->driver_model->get_all_vhGp($GLOBALS['dvr_client_id'], $GLOBALS['vehicleGroupID']);
		$row=$GLOBALS['vehicleGroupList'];		
		if($GLOBALS['vehicleGroupList']!=null)
		{
			$vhgpid=$row[0]['vh_gp_id'];
			$vhid=($GLOBALS['vehicleID']!=null)?$GLOBALS['vehicleID']:-1;			
			if($GLOBALS['vehicleGroupID']!=null)
				$vhgpid=$GLOBALS['vehicleGroupID'];
			$GLOBALS['todayDateTime']=$this->common_model->get_usertime();			
			$GLOBALS['vehicleList']=$this->driver_model->get_all_vhGp_vehicle($vhgpid, $GLOBALS['todayDateTime'], $vhid, $GLOBALS['isEdit']);
		}
		
		$GLOBALS['driverList']=$this->driver_model->get_vts_driver(null,null,null,$GLOBALS['dvr_client_id']);
		$GLOBALS['clientList']=$this->driver_model->get_allClients();
		$GLOBALS['driverGroupList']=$this->driver_model->get_driverGroup($GLOBALS['dvr_client_id']);
		$GLOBALS['driverIdTypeList']=$this->driver_model->get_driverIdType();
		
		$config['base_url'] = base_url("index.php/driver_ctrl/table_pagination/".$GLOBALS['dvr_client_id']);
		$config['uri_segment'] = URI_SEGMENT_FOR_FOUR;
		
		$config['total_rows'] = count($GLOBALS['driverList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['driverList']=$this->driver_model->get_vts_driver(ROW_PER_PAGE,$pageNo,null,$GLOBALS['dvr_client_id']);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('driver_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
}

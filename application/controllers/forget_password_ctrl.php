<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Forget_password_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('forget_password_model');
		$this->load->model('common_model');
		$GLOBALS['UserName']=null;
		$GLOBALS['UserFullName']=null;
		$GLOBALS['UserID']=null;
		$GLOBALS['screen']="forgotpwd";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->display();
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function validate_user()
	{
		$GLOBALS['UserName']=(null!=($this->input->post('UserName'))?trim($this->input->post('UserName')):null);
		$this->form_validation->set_message('required', '%s required');
		$this->form_validation->set_rules('UserName', 'User Name', 'callback_check_username');
		// if any of the form rule is failed, then it show the error msg in view.
		//Else update the new Department in department table.
		if ($this->form_validation->run() == FALSE)
		{
			$this->display();
		}
		else
		{
			$s_msg='Dear '.$GLOBALS['UserFullName'].', <br><br>&nbsp;&nbsp;Click the below link to reset your password.<br><a href='.base_url("index.php/change_password_ctrl/view/".md5($GLOBALS['UserID'])).'>Click Here</a><br><br>Thanks & Regards,<br>Autograde T.A.N.K. Support.';
			$this->load->library ( 'commonfunction' );
			//send_mail($to, $to_name, $cc, $bcc, $subject, $message, $reply_to, $reply_to_name )
			$this->commonfunction->send_mail($GLOBALS['UserName'],$GLOBALS['UserFullName'],CC_TO,BCC_TO,'Reset password',$s_msg,REPLY_TO,'VTS Support');
			$GLOBALS['title']="Thank you.";
			$GLOBALS['outcome']="The reset password link has been sent to your e-mail (".$GLOBALS['UserName'].").Kindly check your mailbox.";
			$GLOBALS['isErrorMsg']=NOT_ACTIVE;
			$GLOBALS['ctrlName']="login_ctrl";
			$GLOBALS['linkText']="Go to login page.";
			$this->messagePage();
		}
	}
	/*
	 * This function is used to validate the given user email
	 * @param
	 *  $username - email id
	 * Return type - bool
	 */
	public function check_username($username)
	{
		if (empty($username)) //Check whether the user name field is empty
		{
			$this->form_validation->set_message('check_username', '%s required.');
			return FALSE;
		}
		else if(!filter_var($username, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['UserName']=null;
			$this->form_validation->set_message('check_username', 'Invalid email format.');
			return FALSE;
		}
		else if($row=$this->forget_password_model->user_check($GLOBALS['UserName']))
		{
			$GLOBALS['UserName']=trim($row['user_email']);
			$GLOBALS['UserFullName']=trim($row['user_user_name']);
			$GLOBALS['UserID']=trim($row['user_id']);
			return TRUE;
		}
		else
		{
			$GLOBALS['UserName']=null;
			$this->form_validation->set_message('check_username', 'Invalid email.');
			return FALSE;
		}
		return TRUE;
	}
	/*
	 * This function is used to render the view wiht give $date as argument.
	 */
	private function display()
	{
		$this->load->view ( 'header_footer/header1',$GLOBALS );
		$this->load->view ( 'forget_password_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}
	/*
	 * This function is used to render the message page
	 */
	private function messagePage()
	{
		$this->load->view ( 'header_footer/header1',$GLOBALS);
		$this->load->view ( 'message_view', $GLOBALS );
		$this->load->view ( 'header_footer/footer' );
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->model('common_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['outcome']=null;//jsut sent the dummy value to show result late
		$GLOBALS['user_name']=null;
		$GLOBALS['user_password']=null;
		$GLOBALS['user_id']=null;
		$GLOBALS['timezone_id']=null;
		$GLOBALS['timezone_diff']=null;
		$GLOBALS['timezone_name']=null;
		$GLOBALS['dealer_id']=null;
		$GLOBALS['dealer_name']=null;
		$GLOBALS['distributor_id']=null;
		$GLOBALS['distributor_name']=null;
		$GLOBALS['parent_id']=null;
		$GLOBALS['parent_name']=null;
		$GLOBALS['user_type_id']=null;		
		$GLOBALS['client_id']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['is_clientAdmin']=NOT_ACTIVE;
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
		$GLOBALS['screen']="login";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$this->session->unset_userdata ( 'login' );
		$this->session->sess_destroy ();
		$this->display($GLOBALS);
	}
	/*
	 * This function is used to validate and process the date
	 * which is posted by html page.
	 */
	public function validate_user()
	{
		$GLOBALS['user_name']=(null!=($this->input->post('UserName'))?trim($this->input->post('UserName')):null);
		$GLOBALS['user_password']=(null!=($this->input->post('Password'))?trim($this->input->post('Password')):null);
		$this->form_validation->set_message('required', '%s required');
		$this->form_validation->set_rules('UserName', 'Username', 'callback_check_username');
		$this->form_validation->set_rules('Password', 'Password', 'callback_check_password');
		// if any of the form rule is failed, then it show the error msg in view.
		if ($this->form_validation->run() == FALSE)
		{
			$this->display($GLOBALS);
		}
		else
		{
			$sess_array=array('sess_userid'=>$GLOBALS['user_id'],
					'sess_user_name'=>$GLOBALS['user_name'], 
					'sess_clientid'=>$GLOBALS['client_id'],
					'sess_client_name'=>$GLOBALS['client_name'], 
					'sess_client_admin'=>$GLOBALS['is_clientAdmin'], 
					'sess_is_admin'=>$GLOBALS['is_Admin'],
					'sess_time_zoneid'=>$GLOBALS['timezone_id'], 
					'sess_time_zonediff'=>$GLOBALS['timezone_diff'], 
					'sess_time_zonename'=>$GLOBALS['timezone_name'],
					'sess_dealerid'=>$GLOBALS['dealer_id'],
					'sess_dealer_name'=>$GLOBALS['dealer_name'],
					'sess_distributorid'=>$GLOBALS['distributor_id'],
					'sess_distributor_name'=>$GLOBALS['distributor_name'],
					'sess_parentid'=>$GLOBALS['parent_id'],
					'sess_parent_name'=>$GLOBALS['parent_name'],
					'sess_user_type'=>$GLOBALS['user_type_id'],
					'sess_select_client'=>null
					);
			$this->session->set_userdata('login',$sess_array);
			// parameters are user id, ip, screen id, event description
			if (trim ( $GLOBALS ['eventLogRequired'] ) == REQUIRED)
				$this->common_model->insert_event_value ( $GLOBALS ['user_id'], $GLOBALS ['ip'], LOGIN, "User Login: " . "User ID=" . $GLOBALS ['user_id'] . ", User Name=" . $GLOBALS ['user_name'] );
			$this->homePage();
		}
	}
	/*
	 * This function is used to validate the given login email address
	 * @param
	 *  $username - login email address
	 * Return type - bool
	 */
	public function check_username($username)
	{
	if (empty($username)) //Check whether the user name field is empty
		{
			$this->form_validation->set_message('check_username', '%s required');
			return FALSE;
		}
		/*else if(!filter_var($username, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['user_name']=null;
			$this->form_validation->set_message('check_username', 'Invalid email format');
			return FALSE;
		}*/
		return TRUE;
	}
	/*
	 * This function is used to validate the given password
	 * @param
	 *  $pwd - password
	 * Return type - bool
	 */
	public function check_password($pwd)
	{
		if (empty($pwd)) //Check whether the user name field is empty
		{
			$this->form_validation->set_message('check_password', '%s required');
			return FALSE;
		}
		else if($GLOBALS['user_password']!=null)
		{
			$result_value=$this->login_model->login_check($GLOBALS['user_name'],$GLOBALS['user_password']);
			///log_message('error',print_r($result_value,true));
			if($result_value!=null)
			{
				if($result_value['t_result_value']==null || empty($result_value['t_result_value']))
				{
					$GLOBALS['user_id']=$result_value['t_userid'];
					$GLOBALS['client_id']=$result_value['t_clientid'];
					$GLOBALS['client_name']=$result_value['t_clientname'];
					$GLOBALS['is_clientAdmin']=$result_value['t_user_clientadmin'];
					$GLOBALS['is_Admin']=$result_value['t_user_isadmin'];
					$GLOBALS['timezone_id']=$result_value['t_user_timezoneid'];
					$GLOBALS['timezone_diff']=$result_value['t_user_timediff'];
					$GLOBALS['timezone_name']=$result_value['t_user_timezonename'];
					$GLOBALS['distributor_id']=$result_value['t_distid'];
					$GLOBALS['distributor_name']=$result_value['t_distname'];
					$GLOBALS['dealer_id']=$result_value['t_dealerid'];
					$GLOBALS['dealer_name']=$result_value['t_dealername'];
					$GLOBALS['parent_id']=$result_value['t_parentid'];
					$GLOBALS['parent_name']=$result_value['t_parentname'];
					$GLOBALS['user_type_id']=$result_value['t_usertype'];
					return TRUE;
				}else
				{
					$GLOBALS['user_password']=null;
					$this->form_validation->set_message('check_password', $result_value['t_result_value']);
					return FALSE;
				}
			}
		}
	}
	/*
	 * This function is used to render the view wiht give $date as argument.
	 * @param
	 *  $data - array of value
	 */
	private function display($data)
	{
/*ramesh old login view		$this->load->view('header_footer/header1',$data);
		$this->load->view('login_view',$data);//load view login
 		$this->load->view('header_footer/footer');*/
		
		//new html login view
		$this->load->view('new_login_view',$data);//load view login
	}
	/*
	 * This function is used to redirect to home page or individual vehicle tracking page
	 * depending upon the client type(i.e. entrprise or individual user)
	 */
	private function homePage()
	{
		if($GLOBALS['user_type_id'] == AUTOGRADE_USER)
			redirect("home_ctrl");
		elseif( $GLOBALS['user_type_id'] == OHTER_CLIENT_USER)
		{
			if($GLOBALS['is_Admin']=='1')
				redirect("home_ctrl");
			else
				redirect("vehicle_tracking_ctrl");
		}
		else if($GLOBALS['user_type_id'] == INDIVIDUAL_CLIENT)
			redirect("individual_vt_ctrl");
		else if($GLOBALS['user_type_id']==DISTRIBUTOR_USER)
			redirect("distributor_dashboard_ctrl");
		else if($GLOBALS['user_type_id']==DEALER_USER)
			redirect("dealer_dashboard_ctrl");
		else if($GLOBALS['user_type_id']==PARENT_USER)
			redirect("parent_dashboard_ctrl");
	}
}
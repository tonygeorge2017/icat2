<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Device_list_ctrl extends CI_Controller {
/*
 * Class constructor
 */	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_list_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];		
		$GLOBALS['distributorList']=array();
		$GLOBALS['dealerList']=array();
		$GLOBALS['vehicleGroupList']=array();
		$GLOBALS['vehicleList']=array();
		$GLOBALS['deviceDetailList']=array();
		$GLOBALS['imeiNo']=null;
		$GLOBALS['slNo']=null;
		$GLOBALS['distributorID']=null;
		$GLOBALS['dealerID']=null;
		$GLOBALS['clientName']=null;
		$GLOBALS['clientID']=null;
		$GLOBALS['vehicleGroupID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['showGrid']=false;
		if($GLOBALS['ID']['sess_user_type'] == DEALER_USER)
			$GLOBALS['distributorID']=$GLOBALS['ID']['sess_distributorid'];
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
// 		$GLOBALS['deviceDetailList']=$this->device_list_model->get_device_details(null,true);
// 		if($GLOBALS['deviceDetailList']!=null)
			$GLOBALS['showGrid']=true;
		$this->display();
	}
	/*
	 * This function is used to get to query
	 */
	public function fetch_device_list(){
		$this->form_validation->set_rules('AutoClinetName', 'Clinet name', 'callback_check_client');
		$this->form_validation->set_rules('ImeiNo', 'Device IMEI', 'callback_check_device_imei');
		$this->form_validation->set_rules('SlNo', 'Device SLNO', 'callback_check_device_slno');
		$GLOBALS['imeiNo']=(null!=trim($this->input->post('ImeiNo'))?trim($this->input->post('ImeiNo')):null);
		$GLOBALS['slNo']=(null!=trim($this->input->post('SlNo'))?trim($this->input->post('SlNo')):null);
		$GLOBALS['distributorID']=(null!=trim($this->input->post('DistributorID'))?trim($this->input->post('DistributorID')):null);
		$GLOBALS['dealerID']=(null!=trim($this->input->post('DealerID'))?trim($this->input->post('DealerID')):null);
		$GLOBALS['clientID']=(null!=trim($this->input->post('ClinetID'))?trim($this->input->post('ClinetID')):null);
		$GLOBALS['clientName']=(null!=trim($this->input->post('AutoClinetName'))?trim($this->input->post('AutoClinetName')):null);		
		$GLOBALS['vehicleGroupID']=(null!=trim($this->input->post('VehicleGroupID'))?trim($this->input->post('VehicleGroupID')):null);
		$GLOBALS['vehicleID']=(null!=trim($this->input->post('VehicleID'))?trim($this->input->post('VehicleID')):null);
		$GLOBALS['btnClicked']=(null!=trim($this->input->post('ClickedBtn'))?trim($this->input->post('ClickedBtn')):null);

		if($GLOBALS['distributorID']!=null)
			$GLOBALS['dealerList']=$this->device_list_model->get_all_Dealer($GLOBALS['distributorID']);
		if($GLOBALS['clientID']!=null)
			$GLOBALS['vehicleGroupList']=$this->device_list_model->get_all_vhGp($GLOBALS['clientID']);
		if($GLOBALS['vehicleGroupID']!=null)
			$GLOBALS['vehicleList']=$this->device_list_model->get_all_vhGp_vehicle($GLOBALS['vehicleGroupID']);
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view.
		{
			$this->display();
		}
		else
		{
			$data['imeiNo']=$GLOBALS['imeiNo'];
			$data['slNo']=$GLOBALS['slNo'];
			$data['distributorID']=$GLOBALS['distributorID'];
			$data['dealerID']=$GLOBALS['dealerID'];
			$data['clientID']=$GLOBALS['clientID'];
			$data['vehicleGroupID']=$GLOBALS['vehicleGroupID'];
			$data['vehicleID']=$GLOBALS['vehicleID'];
			if($GLOBALS['btnClicked']==null)
				$GLOBALS['deviceDetailList']=$this->device_list_model->get_device_details($data,true);
			else
				$GLOBALS['deviceDetailList']=$this->device_list_model->get_device_details($data);
			$title="Device List"; 
			$sub_title="--";
			if($GLOBALS['btnClicked']==null)
			{
				$GLOBALS['showGrid']=true;
				$this->display();
			}
			else if($GLOBALS['btnClicked']=="csv")
			{
				$this->load->library ( 'commonfunction' );
				array_unshift($GLOBALS['deviceDetailList'], $arr2=array('device_slno'=>'Slno','device_imei'=>'IMEI No.','client_name'=>'Client','vehicle_name'=>'Vehicle','first_data_date'=>'Installed Date & Time','last_data_date'=>'Last Date & Time'));
				$this->commonfunction->csv_create ( $title, $sub_title, $headers=$GLOBALS['deviceDetailList'] ); 
			}
			else if ($GLOBALS ['btnClicked'] == "pdf") 
			{
				array_unshift($GLOBALS['deviceDetailList'], $arr2=array('device_slno'=>'Slno','device_imei'=>'IMEI No.','client_name'=>'Client','vehicle_name'=>'Vehicle','first_data_date'=>'Installed Date & Time','last_data_date'=>'Last Date & Time'));
				$this->load->helper ( array ('dompdf', 'file') );
				
				// starts from here----
				$html = " <html>
				<head></head>
				<body>
				<table border=0>
				<tr><td>
					<table>
					<tr><td><b>$title</td></td></tr>
					<tr><td>$sub_title</td><td></td></tr>
					</table>
				</td><br/>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
				<table>
				<tr>
				<td> <img alt='' src='" . base_url ( 'assets/images/logo.png' ) . "'/></td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				<hr>
				<table cellpadding='14'> ";
				$table_data = "";
				foreach ( $GLOBALS['deviceDetailList'] as $header ) 
				{
					
					$table_data .= " <tr><td>" . $header ['device_slno'] ."</td><td>" . $header ['device_imei'] . "</td><td>" . "</td><td>" . $header ['client_name'] . "</td><td>" . $header ['vehicle_name'] . "</td><td>" . $header ['first_data_date'] . "</td><td>" . $header ['last_data_date'] . "</td></tr>";
				}
				$html_1 = "</table>
									<hr>
									<table>&nbsp;&nbsp;&nbsp;&nbsp;
									<tr>
									<td><b>Prepared by </b> : " . $GLOBALS['ID']['sess_user_name'] . "</td>
											<td></td>
											<td style='padding-left:390px;'><b>" . date ( 'Y-m-d' ) . "</b></td>
									</tr>
							</table>
							</body>
							</html>";
				
				$html = $html . $table_data . $html_1;
				
				// ends here-----------
				
				pdf_create ( $html, str_replace ( ' ', '_', $title ) . '_' . date ( 'Ymd' ) );
			}
		}
	}
	/*
	 * This function is used to validate the givn client name
	 * @param
	 *  $client- given client name by the user
	 * Return type bool
	 */
	public function check_client($client)
	{
		if (empty($client))
		{
			return true;
		}
		elseif (is_numeric($client))
		{
			$GLOBALS['clientName']=null;
			$this->form_validation->set_message ( 'check_client', '%s should not be numeric' );
			return FALSE;
		}
		elseif($GLOBALS['clientID']==null)
		{
			$GLOBALS['clientName']=null;
			$this->form_validation->set_message ( 'check_client', '%s not found' );
			return FALSE;		
		}
		else
			return true;
	}
	/*
	 * This function is the callback function of form validation, 
	 * it is used to validate the vts_device IMEI No.
	 * @param
	 *  $dvs_imei- given imei no by the user
	 * Return type bool
	 */
	public function check_device_imei($dvs_imei)
	{
		if (empty($dvs_imei)) // Check whether the given Device IMEI is empty.
		{
			return true;
		}
		if (!is_numeric($dvs_imei)) // Check whether the given Device IMEI is numeric.
		{
			$GLOBALS['imeiNo']=null;
			$this->form_validation->set_message ( 'check_device_imei', '%s should be numeric' );
			return FALSE;
		}
		else
			return true;
	}
	
	/*
	 * This function is to validate the device slno number
	 */
	public function check_device_slno($dvs_slno)
	{
		if (empty($dvs_slno)) // Check whether the given Device Slno is empty.
		{
			return true;
		}
		if (!is_numeric($dvs_slno)) // Check whether the given Device Slno is numeric.
		{
			$GLOBALS['slnoNo']=null;
			$this->form_validation->set_message ( 'check_device_slno', '%s should be numeric' );
			return FALSE;
		}
		else
			return true;
	}
	
	/*
	 * This function is used to get the dealer for the selected distributor and
	 * return back the list of dealer in JSON format with out refresh the page.
	 * @param
	 *  $distributor - distributor id
	 * Return type - JSON string
	 */
	public function get_dist_dealer($distributor=null)
	{
		$output="";
// 		if($distributor!=null)
// 		{
			$dealers=$this->device_list_model->get_all_Dealer($distributor);
			if($dealers!=null)
			{
				$output=json_encode($dealers);
			}
// 		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle group for the selected client and
	 * return back the list of vehicle group in JSON format with out refresh the page.
	 * @param
	 *  $client - client id
	 * Return type - JSON string
	 */
	public function get_client_vhgp($client)
	{
		$client_vh_gps=$this->device_list_model->get_all_vhGp($client);
		$output="";
		if($client_vh_gps!=null)
		{
			$output=json_encode($client_vh_gps);
		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and
	 * return back the list of vehicles in JSON format with out refresh the page.
	 * @param
	 *  $vhGrp - vehicle group id
	 * Return type - JSON string
	 */
	public function get_vhl_grp_vehicle($vhGrp=null)
	{
		$output="";
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=$this->device_list_model->get_all_vhGp_vehicle($vhGrp);
			if($vhl_grp_vehicle!=null)
			{
				$output=json_encode($vhl_grp_vehicle);
			}
		}
		echo($output);
	}
   /*	
	* function calling from autocomplete function of view	
	* to get values to item field	
	*/	
	public function  get_client_list_name()
	{
		if (isset($_GET['client']))
		{
			$dist=(isset($_GET['client']))?$_GET['dist']:null;
			$deal=(isset($_GET['client']))?$_GET['dealer']:null;
			$clientNameStartWith = strtolower($_GET['client']);
			$query= $this->device_list_model->get_all_Clients($clientNameStartWith,$dist,$deal);
			if(count($query) > 0)
			{
				//$i=0;
				foreach ($query as $row)
				{
					$new_array['value'] = htmlentities(stripslashes($row['client_name']));
					$new_array['id'] = htmlentities(stripslashes($row['client_id'])); //build an array
					$row_set[] =  $new_array;
					//$i++;
				}
				echo json_encode($row_set); //format the array into json data
			}
		}
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if ($this->common_model->check_session())
		{
			$GLOBALS['showGrid']=true;
			$GLOBALS['distributorList']=$this->device_list_model->get_all_Distributor();
			if($GLOBALS['distributorID']!=null)
				$GLOBALS['dealerList']=$this->device_list_model->get_all_Dealer($GLOBALS['distributorID']);
			$this->common_model->menu_display();
			$this->load->view('header_footer/header',$GLOBALS);
			$this->load->view('device_list_view',$GLOBALS);
			$this->load->view('header_footer/footer');
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Parents_vh_stop_ctrl extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('parents_vh_stop_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['outcome']=null;//jsut sent the dummy value to show result late
		$GLOBALS['clientList']=array();
		$GLOBALS['stopList']=array();
		$GLOBALS['vehicleGpList']=array();
		$GLOBALS['vehicleList']=array();
		$GLOBALS['parentList']=array();
		$GLOBALS['selectedParentIDList']=array();
		$GLOBALS['clientID']=null;
		$GLOBALS['stopID']=null;
		$GLOBALS['vehicleGpID']=null;
		$GLOBALS['vehicleID']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * Default function which invoke when we call this ctrl.
	 */
	public function index()
	{
		$GLOBALS['clientID']=($GLOBALS['sessClientID']!=AUTOGRADE_USER)?$GLOBALS['sessClientID']:null;
		$this->display();
	}
	/*
	 * This function is used to validate the posted input and do
	 * the save or update process.
	 * Return type - void
	 */
	public function validate_parent_stop()
	{
		$this->form_validation->set_message('required', '%s required.');
		$this->form_validation->set_rules('ClientID', 'Client', 'required');
		$this->form_validation->set_rules('VehicleGroupID', 'Vehicle group', 'required');
		$this->form_validation->set_rules('VehicleID', 'Vehicle', 'required');
		$this->form_validation->set_rules('StopID', 'Vehicle stop', 'required');
		//assign the values to the $GLOBAL variable which is posted by the form.
		$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))?$this->input->post('ClientID'):null);
		$GLOBALS['vehicleGpID']=(null!=($this->input->post('VehicleGroupID'))?$this->input->post('VehicleGroupID'):null);
		$GLOBALS['vehicleID']=(null!=($this->input->post('VehicleID'))?$this->input->post('VehicleID'):null);
		$GLOBALS['stopID']=(null!=($this->input->post('StopID'))?$this->input->post('StopID'):null);
		$GLOBALS['selectedParentIDList']=(null!=($this->input->post('SelectedParentID'))?$this->input->post('SelectedParentID'):null);
		if($this->form_validation->run()==false)
		{
			$GLOBALS['outcome']='<div style="color: red;">Please fill all requited fields.</div>';
			$this->display();
		}
		else
		{
			if($this->parents_vh_stop_model->save_parent_stop($GLOBALS['stopID'], $GLOBALS['selectedParentIDList']))
			{
				$GLOBALS['outcome']='<div style="color: green;">Updated successfully.</div>';
				if (trim ( $GLOBALS ['eventLogRequired'] ) == REQUIRED) // parameters are user id, ip, screen id, event description
				{
					if($GLOBALS['selectedParentIDList']!=null)
					{
						$l_disc="Stop ID: ".$GLOBALS['stopID']." is assigned to following Parents ID: ";
						foreach ($GLOBALS['selectedParentIDList'] as $row)
							$l_disc.=$row.", ";
					}
					else
						$l_disc="All Parents ID which is belongs to Stop ID: ".$GLOBALS['stopID']." is removed from the vts_parent_stop table.";
					$this->common_model->insert_event_value ( $GLOBALS['sessUserID'], $GLOBALS ['ip'], PARENT_VEHICLE_STOP_MANAGEMENT, $l_disc );
				}
			}
			else
				$GLOBALS['outcome']='<div style="color: red;">Update failed.</div>';
			$this->display();
		}
	}
	/*
	 * This function is used to get the vehicle group for the selected client and
	 * return back the list of vehicle group in JSON format with out refresh the page.
	 * @param
	 *  $client - client id
	 * Return type - JSON string
	 */
	public function get_client_vhgp($client)
	{
		$client_vh_gps=$this->parents_vh_stop_model->get_all_vhGp($client);
		$output="";
		if($client_vh_gps!=null)
		{
			$output=json_encode($client_vh_gps);
		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and
	 * return back the list of vehicles in JSON format with out refresh the page.
	 * @param
	 *  $vhGrp - vehicle group id
	 * Return type - JSON string
	 */
	public function get_vhl_grp_vehicle($vhGrp=null)
	{
		$output="";
		if($vhGrp!=null)
		{
			$vhl_grp_vehicle=$this->parents_vh_stop_model->get_all_vhGp_vehicle($vhGrp);
			if($vhl_grp_vehicle!=null)
			{
				$output=json_encode($vhl_grp_vehicle);
			}
		}
		echo($output);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and
	 * return back the list of vehicles in JSON format with out refresh the page.
	 * @param
	 *  $vhGrp - vehicle group id
	 * Return type - JSON string
	 */
	public function get_vehicle_stop($vehicleID=null)
	{
		$output="";
		if($vehicleID!=null)
		{
			$vehicle_stop=$this->parents_vh_stop_model->get_vh_stop($vehicleID);
			if($vehicle_stop!=null)
			{
				$output=json_encode($vehicle_stop);
			}
		}
		echo($output);
	}
	/*
	 * This function is used to create the table without rsfresh the
	 * page by using the java script
	 * @param
	 *  $clientID - selected client id
	 *  $stopID - selected stop id
	 * Return type - string
	 */
	public function make_table($clientID, $stopID=null, $search=null)
	{
		$output='<table width="100%" class="user-dts">
						<tr>
							<th>Student</th>
							<th>Class</th>
							<th>Parent</th>
							<th>Select</th>
						</tr>';
		if($clientID!=null)
		{
			$parent_list=$this->parents_vh_stop_model->get_parent_list($clientID, $stopID, $search);
			if($parent_list!=null)
			{
				foreach($parent_list as $row)
				{
					$checked=(isset($row["pt_stop_parent_id"]))?'checked':'';
					$output.='<tr>
				            <td>'.$row["parent_student_name"].'</td>
				            <td>'.$row["parent_student_class"].'</td>
				            <td>'.$row["parent_name"].'</td>
				            <td><input name="SelectedParentID[]" type="checkbox" value="'.$row["parent_id"].'" '.$checked.'/></td>
				            </tr>';
				 }
			}
			else{
				$output.='<td colspan="4" style="text-align:center; color:red;">No records found.</td>';
			}
		}
		$output.='</table>';
		echo $output;
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		if ($this->common_model->check_session())
		{
			$this->common_model->menu_display();
			$GLOBALS['clientList']=$this->parents_vh_stop_model->get_allClients();
			if(isset($GLOBALS['clientID']))
			{
				$GLOBALS['vehicleGpList']=$this->parents_vh_stop_model->get_all_vhGp($GLOBALS['clientID']);
				if(isset($GLOBALS['vehicleGpID']))
					$GLOBALS['vehicleList']=$this->parents_vh_stop_model->get_all_vhGp_vehicle($GLOBALS['vehicleGpID']);
				if(isset($GLOBALS['vehicleID']))
					$GLOBALS['stopList']=$this->parents_vh_stop_model->get_vh_stop($GLOBALS['vehicleID']);
				$GLOBALS['parentList']=$this->parents_vh_stop_model->get_parent_list($GLOBALS['clientID'], $GLOBALS['stopID']);
			}
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'parents_vh_stop_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Device_type_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');		
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_type_model');
		$this->load->helper(array('form', 'url'));
		//the below data will save to the vts_device_type table
		$GLOBALS['dvice_type_id']=null;
		$GLOBALS['dvice_type_name']=null;

		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		
		$GLOBALS['DeviceTypeList']=$this->device_type_model->get_vts_device_type();
		$GLOBALS['ID'] = $this->session->userdata('login');
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";

	}

	public function index()
	{
		$this->table_pagination();
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_device_type in database.
	 */
	public function vts_device_type_validation()
	{
		$GLOBALS['dvice_type_id']=(null!=($this->input->post('DeviceTypeId'))?$this->input->post('DeviceTypeId'):null);
		$GLOBALS['dvice_type_name']=(null!=trim($this->input->post('DeviceTypeName'))?trim($this->input->post('DeviceTypeName')):null);

		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('DeviceTypeName', 'Device Name', 'callback_check_device_type_name');

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Device Type in vts_device_type table.
		{
				
			$this->table_pagination();
		}
		else
		{
			$data['device_type_name']=trim($GLOBALS['dvice_type_name']);

			
			if($GLOBALS['dvice_type_id']!=null)		//edit
			{
				$data['device_type_id']=$GLOBALS['dvice_type_id'];
				if ($this->device_type_model->InsertOrUpdateOrdelete_vts_device_type($GLOBALS['dvice_type_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEVICE_TYPE,
								"Updated Device Type: Device Type ID=".trim($GLOBALS['dvice_type_id']).", Device Type Name=".trim($GLOBALS['dvice_type_name']));
	
						$GLOBALS['outcome']="Device Type ".'"'.trim($GLOBALS['dvice_type_name']) .'"'." updated successfully";
						$GLOBALS['dvice_type_id']=null;
	
				}
			}
			else						 		//insert
			{
				if($this->device_type_model->InsertOrUpdateOrdelete_vts_device_type(null,$data,'insert'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEVICE_TYPE,
								"Created New Device Type: Device Type Name=".trim($GLOBALS['dvice_type_name']));
	
							$GLOBALS['outcome']="New Device Type ".'"'.trim($GLOBALS['dvice_type_name']).'"'." created successfully";
							$GLOBALS['dvice_type_id']=null;
				}
			}
			$this->table_pagination();
		}
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_device_type name
	 */
	public function check_device_type_name($dvse_nm)
	{
		$GLOBALS['dvice_type_name']=null;
		if (empty($dvse_nm)) //Check whether the Device name field is empty
		{
			$this->form_validation->set_message('check_device_type_name', '%s required');
			return FALSE;
		}
		else if(strlen($dvse_nm) < MIN_FIELD_LENGTH || strlen($dvse_nm) > MAX_FIELD_LENGTH ) // Check whether the device type name is between 5 and 20 characters
		{
			$this->form_validation->set_message ( 'check_device_type_name', 'Device type name should be in between 5-20 characters' );
			return FALSE;
		}
		//else if(!preg_match("/^[A-Za-z0-9-_]+$/",$dvse_nm))   //(preg_match("$/([!@^&*_\-{}`~<>,\"'\.?%#\*])$",$dvse_nm))
		//{
		//	$this->form_validation->set_message('check_device_type_name','Device type is not valid');
		//	return FALSE;
		//}
		else if(is_numeric($dvse_nm)) // Check whether the given value is numeric.
		{
			$this->form_validation->set_message ( 'check_device_type_name', 'Device name should not be numeric value' );
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['DeviceTypeList'] as $row ) {
				if ($GLOBALS ['dvice_type_id'] == null) {
					if (trim ( strtoupper ( $row ['device_type_name'] ) ) == strtoupper ( $dvse_nm )) // Check whether device type name already exist in vts_device_type table
					{
						$GLOBALS['outcome_with_db_check']="Device Type ".'"'.trim($row['device_type_name']).'"'." already exist";
						$this->form_validation->set_message('check_device_type_name', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['device_type_name'] ) ) == strtoupper ( $dvse_nm ) && $GLOBALS ['dvice_type_id'] != trim ( $row ['device_type_id'] )) // Check whether device type name already exist in vts_device_type table while update
					{
						$GLOBALS['outcome_with_db_check']="Device Type ".'"'.trim($row['device_type_name']).'"'." already exist";
						$this->form_validation->set_message('check_device_type_name', '');
						return FALSE;
					}
				}
			}
		}
		
		$GLOBALS['dvice_type_name']=(null!=trim($this->input->post('DeviceTypeName'))?trim($this->input->post('DeviceTypeName')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_device_type
	 */
	public function editOrDelete_vts_device_type()
	{
		$posted=$this->input->get_post('device_type_id');
		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$splitPosted[0];			//it hold the operation to perform(i.e. edit or delete).
		$DeviceTypeId=$splitPosted[1];			//it hold the device type id to edit or delete.
	
		$row=$this->device_type_model->get_vts_device_type(null,null,$DeviceTypeId);
		if($operation=="edit")
		{
			$GLOBALS=array('dvice_type_id'=>trim($row['device_type_id']),'dvice_type_name'=>trim($row['device_type_name']));//It used to fill up the textbox in view.
				
		}
		else if($operation=="delete")
		{
			try
			{
				if($this->device_type_model->InsertOrUpdateOrdelete_vts_device_type($DeviceTypeId,null,$operation))
				{
					$GLOBALS['outcome_with_db_check']="Device Type ".'"'.trim($row['device_type_name']).'"'." deleted";
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEVICE_TYPE,
								"Device Type is deleted: Device Type ID=".$DeviceTypeId);
					
				} else
				{
					$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['device_type_name'] ).'"' . " Can't able to delete(Referred some where else)";
				}
				
					
			}catch (Exception $ex){
				$this->table_pagination();
	
			}
		}
		$this->table_pagination();
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){
		$config['base_url'] = base_url("index.php/device_type_ctrl/table_pagination/");
		$GLOBALS['DeviceTypeList']=$this->device_type_model->get_vts_device_type();
		$config['total_rows'] = count($GLOBALS['DeviceTypeList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['DeviceTypeList']=$this->device_type_model->get_vts_device_type(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('device_type_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
}

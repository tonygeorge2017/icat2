<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Parent_dashboard_ctrl extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );
		$this->load->model ( 'parent_dashboard_model' );
		$this->common_model->check_session ();
		$GLOBALS ['ID'] = $this->session->userdata ( 'login' );
		$GLOBALS ['sessClientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS ['clientID'] = $GLOBALS ['ID'] ['sess_clientid'];
		$GLOBALS['spot']=array();
		$GLOBALS ['alertConfig']=array();
	}
	
	public function index() {
		$this->display ();
	}
	
	public function view() {
		$GLOBALS ['clientID'] = (null != trim ( $this->input->post ( 'ClientID' ) ) ? trim ( $this->input->post ( 'ClientID' ) ) : null);
		$this->display ();
	}
	
	public function dashboard() {		
		$data = $this->parent_dashboard_model->get_run_data();		
		echo json_encode($data);
	}
	
	public function update() {
		if(isset($_REQUEST['nrfid']) && isset($_REQUEST['nroute']) && isset($_REQUEST['nspeed']))
		{
			$data['fcm_alert_rfid'] = $_REQUEST['nrfid'];
			$data['fcm_alert_route'] = $_REQUEST['nroute'];
			$data['fcm_alert_speed'] = $_REQUEST['nspeed'];
			$this->parent_dashboard_model->update_alert_list($data);
			echo json_encode(array('msg'=>'Success'));
		}else{
			echo json_encode(array('msg' => 'Please check the inputs'));
		}
		
	}	

	public function display() {
		if ($this->session->userdata ( 'login' )) {
			$GLOBALS ['timezone'] = ($GLOBALS ['ID'] ['sess_time_zonename']) ? $GLOBALS ['ID'] ['sess_time_zonename'] : ''; 
			$GLOBALS ['clientList'] = $this->common_model->get_allClients ();
			$this->common_model->menu_display ();
		}
		$GLOBALS ['alertConfig'] = $this->parent_dashboard_model->get_alert_list();
		//log_message('error', print_r($GLOBALS ['alertConfig'], true));
		$this->load->view ( 'header_footer/header', $GLOBALS );
		$this->load->view ( 'parent_dashboard_view', $GLOBALS ); // load view Home page
		$this->load->view ( 'header_footer/footer_rmc' );
	}
	
	public function logout() {
		$this->session->unset_userdata ( 'login' );
		$this->session->sess_destroy ();
		redirect ( 'login_ctrl', 'refresh' );
	}
}
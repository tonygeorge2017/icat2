<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	class Vehicle_summary_report extends CI_Controller {

		public function __construct() {

			parent::__construct ();

			$this->load->model("common_model");

			$GLOBALS['page_title'] = "vehicle Summary Report";
			$GLOBALS['ID'] = $this->session->userdata('login');
			$session_userid = $GLOBALS['ID']['sess_userid'];

			// to get user ip and host name
			$host_name = exec ( "hostname" ); // to get "hostname"
			$host_name = trim ( $host_name ); // remove any spaces before and after
			$ip = gethostbyname ( $host_name );
			$GLOBALS ['ip'] = $host_name . "[" . $ip . "]";
		}

		/*
		 * Function to display
		 * the view
		 */
		public function index() {
			$this->display();
		}

		public function stats() {

			$data = array(
					array(
							"id"=> '1',
							"date"=> "2016-12-01",
							"vehicle_name"=> "Vehicle1",
							"route_name"=> "Route 1",
							"no_of_stops"=> "15",
							"ex_stops"=> "5",
							"distance" => "60"							
					),
					array(
							"id"=> '2',
							"date"=> "2016-12-01",
							"vehicle_name"=> "Vehicle1",
							"route_name"=> "Route 2",
							"no_of_stops"=> "10",
							"ex_stops"=> "0",
							"distance" => "35"
					),
					array(
							"id"=> '2',
							"date"=> "2016-12-01",
							"vehicle_name"=> "Vehicle3",
							"route_name"=> "Route 1",
							"no_of_stops"=> "15",
							"ex_stops"=> "0",
							"distance" => "32"
					),
					array(
							"id"=> '3',
							"date"=> "2016-12-01",
							"vehicle_name"=> "Vehicle4",
							"route_name"=> "Route 1",
							"no_of_stops"=> "15",
							"ex_stops"=> "1",
							"distance" => "26"
					),
					array(
							"id"=> '4',
							"date"=> "2016-12-01",
							"vehicle_name"=> "Vehicle5",
							"route_name"=> "Route 1",
							"no_of_stops"=> "15",
							"ex_stops"=> "3",
							"distance" => "35"
					)
						
			);

			echo json_encode($data);
		}

		/*
		 * This function is used to render the view
		 */
		private function display() {
			$this->common_model->menu_display ();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'vehicle_summary_report_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer_rmc' );
		}

	}

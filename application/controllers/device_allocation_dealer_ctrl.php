<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_allocation_dealer_ctrl extends CI_Controller {
	/*
	 * constructor for this class
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('device_allocation_dealer_model');
		$this->load->helper(array('form', 'url'));
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['editing']=NOT_ACTIVE;
		$GLOBALS['checkID']=null;
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		$GLOBALS['distributorID']=null;
		$GLOBALS['dealerID']=null;
		$GLOBALS['selectedDevice']=null;
		$GLOBALS['availableDeviceList']=null;
		$GLOBALS['deviceCount']=NOT_ACTIVE;
		$GLOBALS['givenDate']=null;
		$GLOBALS['deviceDetailList']=$this->device_allocation_dealer_model->get_deviceDetailList($GLOBALS['dealerID'],null,null,null);
		$GLOBALS['outcome']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['distributorList']=null;
// 		$GLOBALS['dealerList']=$this->device_allocation_dealer_model->get_allDealers();
		$GLOBALS['dateToEdit']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		$GLOBALS['isformError']=NOT_ACTIVE;
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$GLOBALS['distributorID']=AUTOGRADE_USER;
		$this->table_pagination($GLOBALS['distributorID'],0);
	}
	/*
	 * This function is used to validate and add the new user in database.
	 */
	public function device_allocation_dealer_validation()
	{
		$GLOBALS['checkID']=trim((null!=($this->input->post('CheckID'))?$this->input->post('CheckID'):null));
		$GLOBALS['distributorID']=trim((null!=($this->input->post('DistributorId'))?$this->input->post('DistributorId'):null));
		$GLOBALS['selectedDevice']=(null!=($this->input->post('deviceID'))?$this->input->post('deviceID'):null);
		$GLOBALS['dealerID']=trim((null!=($this->input->post('DealerID'))?$this->input->post('DealerID'):null));
		$GLOBALS['editing']=trim((null!=($this->input->post('IsEditing'))?$this->input->post('IsEditing'):null));
		$GLOBALS['deviceCount']=trim((null!=($this->input->post('DeviceCount'))?$this->input->post('DeviceCount'):NOT_ACTIVE));
		if(empty($GLOBALS['checkID']))
		{
			$GLOBALS['givenDate']=trim((null!=($this->input->post('givenDateTime'))?trim($this->input->post('givenDateTime')):null));
			if($GLOBALS['givenDate'])
			{
				$give_date = new DateTime($GLOBALS['givenDate']);
				$GLOBALS['givenDate']=$give_date->format('Y-m-d H:i:s');
			}
			$this->form_validation->set_message('required', '%s required');
			$this->form_validation->set_rules('DealerID', 'Dealer', 'required');
			$this->form_validation->set_rules('givenDateTime', 'Dealer reales date', 'callback_check_dealer_date');
			$this->form_validation->set_rules('deviceID', 'Available', 'callback_check_Avail_Device');
			if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view.
			{
				$GLOBALS['isformError']=ACTIVE;
				$row=$GLOBALS['selectedDevice'];
				if(!empty($row))
				{
					$i=0;
					foreach ($row as $_row)
					{
						$GLOBALS['selectedDevice'][$i]=array('device_id'=>$_row);
						$i++;
					}
				}
				$this->table_pagination($GLOBALS['distributorID'],0);
			}
			else
			{
				$data['device_distributor_id']=$GLOBALS['distributorID'];
				$data['device_dealer_id']=$GLOBALS['dealerID'];
				$data['device_release_dealer_date']=$GLOBALS['givenDate'];
// 				$dvs_to_cnt = $this->device_allocation_dealer_model->getDevice
				if(!empty($GLOBALS['selectedDevice']))
				{
					if($this->device_allocation_dealer_model->Update_deviceToDealer($data,$GLOBALS['selectedDevice']))
					{
						$GLOBALS['outcome']='<div style="color: green;">Device has been allocated to the dealer</div>';
						$GLOBALS['givenDate']=null;
					}
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_ALLOCATION_TO_DEALER,
								"Device Allocation To Dealer:"." DistributorID=".trim($GLOBALS['distributorID']).", DealerID=".trim($GLOBALS['dealerID']).", RealesDate=".trim($GLOBALS['givenDate']).", No.of Device=".count($GLOBALS['selectedDevice']));
					$this->clear_all();
				}
				else
				{
					$GLOBALS['outcome']='<div style="color: red;">Device(s) are not avialable for the dealer</div>';
				}
				
				$this->table_pagination($GLOBALS['distributorID'],0);
			}
		}
		else
		{
			$GLOBALS['distributorID']=$GLOBALS['checkID'];
			$this->clear_all();
			$this->table_pagination($GLOBALS['distributorID'],0);
		}
	}
	/*
	 * This function is used to validate the given dealer reales date&time
	 * @param
	 *  $date - date & time
	 * Return type - bool
	 */
	public function check_dealer_date($date)
	{
		if (empty($date)) //Check whether the device dealer reales date field is empty
		{
			$GLOBALS['outcome']='<div style="color: red;">Date & time is required</div>';
			return FALSE;
		}
		else
			return true;
	}
	/*
	 * This function is the call back function of form validation, it is used to validate the vehicle group
	 * @param
	 *  $device - device id
	 * Return type - bool
	 */
	public function check_Avail_Device($device)
	{
		if($GLOBALS['deviceCount']>0)
		{
			if (empty($device))
			{
				$this->form_validation->set_message('check_Avail_Device', 'Please select at least one device from Available Device(s)');
				$GLOBALS['outcome']='<div style="color: red;">Please select at least one device from Available Device(s)</div><br />';
				return FALSE;
			}
		}
		return true;
	}
	/*
	 * This function is used to fetch the data for edit.
	 * @param
	 *  $URLdistributorID - md5 of distributor id
	 *  $URLdealerID - md5 dealer id
	 *  $URLdate - md5 of date & time
	 *  $URLopt - md5 of option(edit or delete)
	 * Return type - bool
	 */
	public function edit_deviceAllocation($URLdistributorID, $URLdealerID, $URLdate, $URLimei, $URLopt)
	{
		if($URLdistributorID!=null && $URLdealerID!=null && $URLimei!=null && $URLdate!=null)
		{
			$row=$this->device_allocation_dealer_model->get_deviceDetailList($URLdealerID,null,null,$URLdate,$URLdistributorID);
			if($row!=NULL)
			{
				$GLOBALS['distributorID']=trim($row[0]['device_distributor_id']);
				$GLOBALS['givenDate'] =trim($row[0]['device_release_dealer_date']);//It used to fill up the textbox in view.
				
				if($URLopt==md5('edit'))
				{
					$GLOBALS['dealerID']=trim($row[0]['device_dealer_id']);
					$GLOBALS['selectedDevice']=$row;
					$GLOBALS['editing']=ACTIVE;
					$GLOBALS['dateToEdit']=$URLdate;
				}
				else if($URLopt==md5('delete'))
				{
					if($this->device_allocation_dealer_model->delete_deviceToDealer($URLdistributorID, $URLdealerID, $URLimei, $URLdate))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['sessUserID'], $GLOBALS['ip'], DEVICE_ALLOCATION_TO_DEALER,
									"Remove Device Allocation To Dealer:"." DistributorID=".trim($row[0]['device_distributor_id']).", DealerID=".trim($row[0]['device_dealer_id']).", RealesDate=".trim($row[0]['device_release_dealer_date']).", No.of Device=".count($row));
						$GLOBALS['editing']=NOT_ACTIVE;
						$GLOBALS['outcome']='<div style="color: green;">Device(s) has been deallocated to the dealer</div>';
						$GLOBALS['givenDate']=null;
					}
					else 
					{
						$GLOBALS['outcome']='<div style="color: red;">Device cant be deallocated</div>';
						$GLOBALS['givenDate']=null;
					}
					
				}
			}
		}
		$GLOBALS['isformError']=NOT_ACTIVE;
		$this->table_pagination(trim($row[0]['device_distributor_id']),0);
	}
	/*
	 * This function is used to clear all global data.
	 */
	public function clear_all()
	{
		$GLOBALS['editing']=NOT_ACTIVE;
		$GLOBALS['selectedDevice']=null;
		$GLOBALS['distributorList']=null;
		$GLOBALS['deviceDetailList']=null;
	}
	/*
	 * This function is used for pagination
	 * @param
	 *  $distributor - distributor id
	 *  $pageNo - page no
	 * Return type - void
	 */
	public function table_pagination($distributor,$pageNo=0)
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['sessionData']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['sessionData']['sess_userid'];
		$GLOBALS['distributorList']=$this->device_allocation_dealer_model->get_allDistributor();
		
		if($GLOBALS['sessionData']!=null)
		{
			$userdate=$this->common_model->get_usertime();
			$date_value=new DateTime($userdate);
			$datevalue=$date_value->format('Y-m-d H:i:s');
			$GLOBALS['currDate']=new DateTime($datevalue);
			if($GLOBALS['isformError']==NOT_ACTIVE)
			{
				$GLOBALS['availableDeviceList']=$this->device_allocation_dealer_model->get_availableDevice($distributor,$GLOBALS['editing'],$GLOBALS['dateToEdit'],$GLOBALS['dealerID']);
			}
			else
			{
				if($GLOBALS['editing'])
					$GLOBALS['availableDeviceList']=$this->device_allocation_dealer_model->get_availableDevice($distributor,$GLOBALS['editing'],md5($GLOBALS['givenDate']),$GLOBALS['dealerID']);
				else
					$GLOBALS['availableDeviceList']=$this->device_allocation_dealer_model->get_availableDevice($distributor,NOT_ACTIVE);
			}
			$GLOBALS['distributorList']=$this->device_allocation_dealer_model->get_allDistributor();
			$GLOBALS['dealerList']=$this->device_allocation_dealer_model->get_allDealers($distributor);
			$config['uri_segment']=URI_SEGMENT_FOR_FOUR;
			$config['base_url'] = base_url("index.php/device_allocation_dealer_ctrl/table_pagination/".$distributor."/");
			$GLOBALS['deviceDetailList']=$this->device_allocation_dealer_model->get_deviceDetailList(null,null,null,null,$distributor);
			$config['total_rows'] = count($GLOBALS['deviceDetailList']);
			$config['per_page']=ROW_PER_PAGE;
			$GLOBALS['deviceDetailList']=$this->device_allocation_dealer_model->get_deviceDetailList(null,ROW_PER_PAGE,$pageNo,null,$distributor);
			$this->pagination->initialize($config);
			$GLOBALS['pageLink']= $this->pagination->create_links();
			$this->display();
		}
		else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('device_allocation_dealer_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
}
<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Daily_activity_rpt_ctrl extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->helper(array('form','url'));
		$GLOBALS['ID'] = $this->session->userdata('login');
		$session_userid = $GLOBALS['ID']['sess_userid'];
		$GLOBALS['sess_clientid']=$GLOBALS['ID']['sess_clientid'];
		$this->load->model('daily_activity_rpt_model');		
	}
	
	public function index()
	{   
		$this->display();
	}
	
	public function get_client()
	{		
		$result=json_encode($this->daily_activity_rpt_model->get_allClient());
		//log_message('debug',$result);
		echo $result;
	}

	public function get_driver($id)
	{		
		$result=json_encode($this->daily_activity_rpt_model->get_alldriver($id));		
		echo $result;
	}

	public function get_vehiclegp($id)
	{
		if($id)
		{			
			$result=array();
			$result=$this->daily_activity_rpt_model->get_allvhgp($id);
			if(count($result) >= 1)
				$result=json_encode($result);
			else
				$result=json_encode(array('id'=>null,'value'=>'-- No Group Found --'));
			//log_message('debug',$result);
			echo $result;
		}
		else{
			echo json_encode(array('id'=>null,'value'=>'-- Client Required --'));
		}
	}
	

	public function get_vehicle($id)
	{
		if($id)
		{
			$result=array();
			$result=$this->daily_activity_rpt_model->get_allvh($id);
			if(count($result) >= 1)
				$result=json_encode($result);
			else
				$result=json_encode(array('id'=>null,'value'=>'-- No Vehicle Found --'));
			//log_message('debug',$result);
			echo $result;
		}
		else{
			echo json_encode(array('id'=>null,'value'=>'-- Vehicle Group Required --'));
		}
	}

	public function fetch_vhroute_data($vh_id)
	{		
		$outp=json_encode($this->daily_activity_rpt_model->get_vh_route($vh_id));
		$outp=($outp!=null)?$outp:'[]';		
		echo($outp);
	}

	public function get_vehicle_run_data($id, $f_date)
	{					
		$result=array();
		$result=$this->daily_activity_rpt_model->get_run_data($id, $f_date);
		if(count($result) >= 1)
			$result=json_encode($result);
		else
			$result=json_encode(array());
		//log_message('debug',$result);
		echo $result;		
	}

	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('daily_activity_rpt_view',$GLOBALS);
		$this->load->view('header_footer/footer',$GLOBALS);
	}
}
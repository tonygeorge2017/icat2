<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
class Contact_us extends CI_Controller {
	/*
	 * class constructor
	 */
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'common_model' );
		$this->load->model ( 'contactus_model' );
		$GLOBALS['screen']="contactus";
		$GLOBALS['clientID']="";
		$GLOBALS['userID']="";
		$GLOBALS['userName']="";
		$GLOBALS['userEmail']="";
		$GLOBALS['mobileNo']="";
		$GLOBALS['message']="";
		$GLOBALS['outcome']=null;
		$GLOBALS['supportEmail']=$this->common_model->get_setting_value("VTSSupportMailAddress");
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['ID'] = $this->session->userdata('login');
		if($this->session->userdata('login')!=null)
			$GLOBALS['user_type_id']=$GLOBALS['ID']['sess_user_type'];
		$GLOBALS['screen']="contactus";
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}
	/*
	 *This function call automatically when call this ctrl
	 */
	public function index()
	{
		if ($this->session->userdata ( 'login' ))
		{
			$row=$this->contactus_model->get_userDetails();
			if($row!=null)
			{
				$GLOBALS['clientID']=trim($row['client_name'].' (ID: '.$row['user_client_id'].')');
				$GLOBALS['userID']=trim($row['user_id']);
				$GLOBALS['userName']=trim($row['user_user_name']);
				$GLOBALS['userEmail']=trim($row['user_email']);
				$GLOBALS['mobileNo']=trim($row['user_mobile']);
				$GLOBALS['message']="";
			}
		}
		$this->dispaly();
	}
	/*
	 * This function is used to catch the posted value from the
	 * html page and validate the input and throw erro if any or
	 * save it database
	 */
	public function validate_contactus()
	{
		$GLOBALS['clientID']=(null!=($this->input->post('ClientID'))? trim($this->input->post('ClientID')):null);
		$GLOBALS['userID']=(null!=($this->input->post('UserID'))? trim($this->input->post('UserID')):null);
		$GLOBALS['userName']=(null!=($this->input->post('UserFullName'))? trim($this->input->post('UserFullName')):null);
		$GLOBALS['userEmail']=(null!=($this->input->post('UserEmail'))? trim($this->input->post('UserEmail')):null);
		$GLOBALS['mobileNo']=(null!=($this->input->post('MobileNo'))? trim($this->input->post('MobileNo')):null);
		$GLOBALS['message']=(null!=($this->input->post('Message'))? trim($this->input->post('Message')):null);
		$this->form_validation->set_message('required', '%s required.');
		$this->form_validation->set_rules('UserEmail', 'Email', 'callback_check_email'); //set rule for User Email field.
		$this->form_validation->set_rules('UserFullName', 'Name', 'required');
		$this->form_validation->set_rules('MobileNo', 'Mobile No', 'required');
		$this->form_validation->set_rules('Message', 'Message', 'required');
		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Department in department table.
		{
			$this->dispaly();
		}
		else
		{
			if($GLOBALS['userID']!=null)
				$GLOBALS['userMailName'] = $GLOBALS['userName'];
				$GLOBALS['userName']=$GLOBALS['userName'].' (ID: '.$GLOBALS['userID'].')';
			if($GLOBALS['clientID']==null)
				$GLOBALS['clientID']='(Not Specified)';
			$s_msg='Hi VTS Support, <br><br><br>&nbsp;&nbsp;&nbsp;Client: '.$GLOBALS['clientID'].'<br><br>&nbsp;&nbsp;&nbsp;Name: '.$GLOBALS['userName'].'<br><br>&nbsp;&nbsp;&nbsp;E-mail: '.$GLOBALS['userEmail'].'<br><br>&nbsp;&nbsp;&nbsp;Mobile no.: '.$GLOBALS['mobileNo'].'<br><br>&nbsp;&nbsp;&nbsp;Message:<br>'.$GLOBALS['message'] ;
			if(trim($GLOBALS['eventLogRequired'])==REQUIRED && isset($GLOBALS['ID']['sess_userid']))	// parameters are user id, ip, screen id, event description
				$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'],CONTACT_US,$s_msg);
			$this->load->library ( 'commonfunction' );
			$this->commonfunction->send_mail($GLOBALS['supportEmail'], 'VTS Support', CC_TO,BCC_TO,'Vehicle Tracking System : Support Msg',$s_msg, $GLOBALS['userEmail'], $GLOBALS['userMailName']);
			$GLOBALS['title']="Thank you.";
			$GLOBALS['outcome']="Your message successfully sent to our support team. We will contact you shortly.";
			$GLOBALS['isErrorMsg']=NOT_ACTIVE;
			$this->messagePage();
		}
	}
	/*
	 * This function is the callback function of form validation,
	 * it is used to validate the email
	 * @param
	 *  $Email - email ide
	 * Return type - bool
	 */
	public function check_email($Email)
	{
		if (empty($Email)) //Check whether the user email field is empty
		{
			$this->form_validation->set_message('check_email', '%s required.');
			return FALSE;
		}
		else if(!filter_var($Email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
			$GLOBALS['userEmail']=null;
			$this->form_validation->set_message('check_email', 'Invalid email format.');
			return FALSE;
		}
		else if (strlen($Email)>100) //Check whether the email is >100 characters
		{
			$GLOBALS['userEmail']=null;
			$this->form_validation->set_message('check_email', 'Should not greater than 100 characters.');
			return FALSE;
		}
		return true;
	}
	/*
	 * This function is used to render the view
	 */
	function dispaly()
	{
		if ($this->session->userdata ( 'login' ))
		{
			$this->common_model->check_session();
			$client_time_zone = $this->common_model->get_location ();
			$GLOBALS ['timezone'] = ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			$this->common_model->menu_display();
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'contact_us_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		} else {
			$this->load->view ( 'header_footer/header1',$GLOBALS );
			$this->load->view ( 'contact_us_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
	}
	/*
	 * This function is used to render the view of message page
	 * @param
	 *  $data - array of info to render the view
	 */
	private function messagePage()
	{
		if ($this->session->userdata ( 'login' ))
		{
			$client_time_zone = $this->common_model->get_location ();
			$GLOBALS ['timezone'] = ($client_time_zone) ? $client_time_zone ['client_time_zone'] : "";
			$this->common_model->menu_display();
			$GLOBALS['linkText']="Go to home page.";			
			if($GLOBALS['user_type_id'] == AUTOGRADE_USER || $GLOBALS['user_type_id'] == OHTER_CLIENT_USER)
				$GLOBALS['ctrlName']="home_ctrl";
			else if($GLOBALS['user_type_id'] == INDIVIDUAL_CLIENT)
				$GLOBALS['ctrlName']="individual_vt_ctrl";
			else if($GLOBALS['user_type_id']==DISTRIBUTOR_USER)
				$GLOBALS['ctrlName']="distributor_dashboard_ctrl";
			else if($GLOBALS['user_type_id']==DEALER_USER)
				$GLOBALS['ctrlName']="dealer_dashboard_ctrl";
			else if($GLOBALS['user_type_id']==PARENT_USER)
				$GLOBALS['ctrlName']="individual_vt_ctrl";			
			$this->load->view ( 'header_footer/header', $GLOBALS );
			$this->load->view ( 'message_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		} 
		else 
		{
			$GLOBALS['linkText']="Go to login page.";
			$GLOBALS['ctrlName']="login_ctrl";
			$this->load->view ( 'header_footer/header1',$GLOBALS );
			$this->load->view ( 'message_view', $GLOBALS );
			$this->load->view ( 'header_footer/footer' );
		}
	}
}
?>
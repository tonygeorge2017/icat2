<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributor_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('distributor_model');
		$this->load->helper(array('form', 'url'));
		
		//the below data will save to the vts_distributor table
		$GLOBALS['distr_id']=null;
		$GLOBALS['distr_name']=null;
		$GLOBALS['distr_address']=null;
		$GLOBALS['distr_post_code']=null;
		$GLOBALS['distr_contact_person']=null;
		$GLOBALS['distr_contact_designation']=null;
		$GLOBALS['distr_contact_email']=null;
		$GLOBALS['distr_contact_phone']=null;
		$GLOBALS['distr_remarks']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['distr_location']=null;
		$GLOBALS['user_type_id']= DISTRIBUTOR_USER;// dealer user id is 4 which is defined in constants.
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		

		$GLOBALS['DistributorList']=$this->distributor_model->get_vts_distributor();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS ['timezone_id'] = $GLOBALS ['ID'] ['sess_time_zoneid'];
		$GLOBALS ['timezone'] = $GLOBALS ['ID'] ['sess_time_zonename'];
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
	}

	public function index()
	{
		$this->table_pagination();
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_distributor in database.
	 */
	public function vts_distributor_validation()
	{
		$GLOBALS['distr_id']=(null!=($this->input->post('DistributorId'))?$this->input->post('DistributorId'):null);
		$GLOBALS['distr_name']=(null!=trim($this->input->post('DistributorName'))?trim($this->input->post('DistributorName')):null);
		$GLOBALS['distr_address']=(null!=trim($this->input->post('DistributorAddress'))?trim($this->input->post('DistributorAddress')):null);
		$GLOBALS['distr_post_code']=(null!=trim($this->input->post('DistributorPostCode'))?trim($this->input->post('DistributorPostCode')):null);
		$GLOBALS['distr_contact_person']=(null!=trim($this->input->post('DistributorContactPerson'))?trim($this->input->post('DistributorContactPerson')):null);
		$GLOBALS['distr_contact_designation']=(null!=trim($this->input->post('DistributorContactDesignation'))?trim($this->input->post('DistributorContactDesignation')):null);
		$GLOBALS['distr_contact_email']=(null!=trim($this->input->post('DistributorContactEmail'))?trim($this->input->post('DistributorContactEmail')):null);
		$GLOBALS['distr_contact_phone']=(null!=trim($this->input->post('DistributorContactPhone'))?trim($this->input->post('DistributorContactPhone')):null);
		$GLOBALS['distr_remarks']=(null!=trim($this->input->post('DistributorRemarks'))?trim($this->input->post('DistributorRemarks')):null);
		$GLOBALS['active']=(null!=($this->input->post('DistributorIsActive'))?$this->input->post('DistributorIsActive'):NOT_ACTIVE);
		$GLOBALS['distr_location']=(null!=trim($this->input->post('DistributorLocation'))?trim($this->input->post('DistributorLocation')):null);
		
// 		$GLOBALS['vcle_group_client_id']=$GLOBALS['ID']['sess_clientid'];
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('DistributorName', 'Name', 'callback_check_distributor_name');
		if(isset($GLOBALS['distr_location']))
		{
			$this->form_validation->set_rules('DistributorLocation','Location','callback_check_distributor_location');
		}
		if(isset($GLOBALS['distr_post_code']))
		{
			$this->form_validation->set_rules('DistributorPostCode','Postal Code','callback_check_distributor_postal_code');			
		}
		
		$this->form_validation->set_rules('DistributorContactPerson', 'Contact Person', 'callback_check_contact_person');
		if(isset($GLOBALS['distr_contact_designation']))
		{
			$this->form_validation->set_rules('DistributorContactDesignation','Contact Designation','callback_check_contact_designation');			
		}
		$this->form_validation->set_rules('DistributorContactEmail', 'Email', 'callback_check_contact_email');
		$this->form_validation->set_rules('DistributorContactPhone', 'Mobile Number', 'callback_check_contact_phone');

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
		{
			$this->table_pagination();
		}
		else
		{
			$data['dist_name']=$GLOBALS['distr_name'];
			$data['dist_address']=$GLOBALS['distr_address'];
			$data['dist_post_code']=$GLOBALS['distr_post_code'];
			$data['dist_contact_person']=$GLOBALS['distr_contact_person'];
			$data['dist_contact_designation']=$GLOBALS['distr_contact_designation'];
			$data['dist_contact_email']=$GLOBALS['distr_contact_email'];
			$data['dist_contact_phone']=$GLOBALS['distr_contact_phone'];
			$data['dist_remarks']=$GLOBALS['distr_remarks'];
			$data['dist_isactive']=$GLOBALS['active'];
			$data['dist_location']=$GLOBALS['distr_location'];
			
			
			if($GLOBALS['distr_id']!=null)		//edit
			{
				$data['dist_id']=$GLOBALS['distr_id'];
				if ($this->distributor_model->InsertOrUpdateOrdelete_vts_distributor($GLOBALS['distr_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DISTRIBUTOR,
								"Updated Distributor: Distributor ID=".trim($GLOBALS['distr_id']).", Distributor Name=".trim($GLOBALS['distr_name']).",Distributor Address=".trim($GLOBALS['distr_address']).", Distributor Post Code=".trim($GLOBALS['distr_post_code']).", Distributor Contact Person=".trim($GLOBALS['distr_contact_person']).", Distributor Contact Designation=".trim($GLOBALS['distr_contact_designation']).", Distributor Contact Mail=".trim($GLOBALS['distr_contact_email']).", Distributor Contact Phone=".trim($GLOBALS['distr_contact_phone']).", Distributor Remarks=".trim($GLOBALS['distr_remarks']).", Distributor Is Active=".trim($GLOBALS['active']).", Distributor Location=".trim($GLOBALS['distr_location']));
				//	if($this->distributor_model->insert_user_value($GLOBALS['distr_id'], $data['dist_contact_person'], $data['dist_contact_email'], $data['dist_isactive'],$GLOBALS ['timezone_id'], $data['dist_contact_phone'],$GLOBALS['user_type_id'], 'edit'))
					//{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DISTRIBUTOR,
									"Update User:"." User Distributor Id=".trim($GLOBALS['distr_id']).",User Name=".trim($data['dist_contact_person']).", Contact Email=".trim($data['dist_contact_email']).",User Is Active=".trim($data['dist_isactive']));
	
						$GLOBALS['outcome']="Distributor details ".'"'.trim($GLOBALS['distr_name']) .'"'." updated successfully.";
						$GLOBALS['distr_id']=null;
						$GLOBALS['active']=null;
					//}
				}
			}
			else	//insert
			{
				if($this->distributor_model->InsertOrUpdateOrdelete_vts_distributor(null,$data,'insert'))
				{
					$distributor_id = $this->distributor_model->get_max_distributor_id();
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DISTRIBUTOR,
								"Created New Distributor: Distributor Name=".trim($GLOBALS['distr_name']).",Distributor Address=".trim($GLOBALS['distr_address']).", Distributor Post Code=".trim($GLOBALS['distr_post_code']).", Distributor Contact Person=".trim($GLOBALS['distr_contact_person']).", Distributor Contact Designation=".trim($GLOBALS['distr_contact_designation']).", Distributor Contact Mail=".trim($GLOBALS['distr_contact_email']).", Distributor Contact Phone=".trim($GLOBALS['distr_contact_phone']).", Distributor Remarks=".trim($GLOBALS['distr_remarks']).", Distributor Is Active=".trim($GLOBALS['active']).", Distributor Location=".trim($GLOBALS['distr_location']));
					
					if($this->distributor_model->insert_user_value($distributor_id,$data['dist_contact_person'], $data['dist_contact_email'], $data['dist_isactive'],$GLOBALS ['timezone_id'], $data['dist_contact_phone'], $GLOBALS['user_type_id'],'insert'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DISTRIBUTOR,
									"Create New User:"." User Dealer ID=".trim($distributor_id).",User Name=".trim($data['dist_contact_person']).", Contact Email=".trim($data['dist_contact_email']).",User Is Active=".trim($data['dist_isactive']));
							
						$users_id = $this->distributor_model->get_user_id(trim($data['dist_contact_email']));
						$s_msg='Dear '.$data['dist_contact_person'].',<br><br>&nbsp; Your email has been registered as a New Login in the Autograde T.A.N.K. System.<br>&nbsp;&nbsp;Please click the below link to set your new password and to start using the System :<br>&nbsp;&nbsp;<a href='.base_url("index.php/change_password_ctrl/view/".md5($users_id)).'>Click Here</a><br><br>Thanks & Regards,<br>Autograde T.A.N.K. Support.';
						$this->load->library ( 'commonfunction' );
						$this->commonfunction->send_mail($GLOBALS['distr_contact_email'],$data['dist_contact_person'],CC_TO,BCC_TO,'Vehicle Tracking System : New User Registered',$s_msg, REPLY_TO,'VTS Support');
						
							$GLOBALS['outcome']="New Distributor ".'"'.trim($GLOBALS['distr_name']).'"'." created successfully and mail has been sent to given Mail ID.";
							$GLOBALS['distr_id']=null;
							$GLOBALS['active']=null;
					}
				}
			}
			$this->table_pagination();
		}
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_distributor name
	 */
	public function check_distributor_name($distr_nm)
	{
		$GLOBALS['distr_name']=null;
		if (empty($distr_nm)) //Check whether the Distributor name field is empty
		{
			$this->form_validation->set_message('check_distributor_name', '%s required');
			return FALSE;
		}
		else if (is_numeric($distr_nm)) // Check whether the given Distributor name is numeric.
		{
			$this->form_validation->set_message ( 'check_distributor_name', '%s should not be numeric' );
			return FALSE;
		}
		else if (strlen($distr_nm) < MIN_FIELD_LENGTH || strlen($distr_nm) > MAX_FIELD_LENGTH ) // Check whether the Distributor name should be between 5 - 20 characters
		{
			$this->form_validation->set_message ( 'check_distributor_name', 'Distributor name should be in between 5-20 characters' );
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['DistributorList'] as $row ) {
				if ($GLOBALS ['distr_id'] == null) {
					if (trim ( strtoupper ( $row ['dist_name'] ) ) == strtoupper ( $distr_nm )) // Check whether Distributor name already exist in vts_distributor table
					{
						$GLOBALS['outcome_with_db_check']="Distributor ".'"'.trim($row['dist_name']).'"'." already exist";
						$this->form_validation->set_message('check_distributor_name', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['dist_name'] ) ) == strtoupper ( $distr_nm ) && $GLOBALS ['distr_id'] != trim ( $row ['dist_id'] )) // Check whether distributor name already exist in vts_distributor table while update
					{
						$GLOBALS['outcome_with_db_check']="Distributor ".'"'.trim($row['dist_name']).'"'." already exist";
						$this->form_validation->set_message('check_distributor_name', '');
						return FALSE;
					}
				}
			}
		}
		
		$GLOBALS['distr_name']=(null!=trim($this->input->post('DistributorName'))?trim($this->input->post('DistributorName')):null);
		return true;
	}
	
	public function check_contact_person($distr_cnt_person)
	{
		$GLOBALS['distr_contact_person']=null;
		if (empty($distr_cnt_person)) //Check whether the Distributor contact person field is empty
		{
			$this->form_validation->set_message('check_contact_person', '%s required');
			return FALSE;
		}
		else if (is_numeric($distr_cnt_person)) // Check whether the given Distributor name is numeric.
		{
			$this->form_validation->set_message ( 'check_contact_person', '%s should not be numeric' );
			return FALSE;
		}
		else if (preg_match("/([%\$#\/*@&<>?.,=])/", $distr_cnt_person))
		{
			$this->form_validation->set_message('check_contact_person','Special charecters are not allowed');
			return FALSE;
		}
		else if(!preg_match('/[a-zA-Z]/',$distr_cnt_person))
		{
			$this->form_validation->set_message('check_contact_person','Invalid contact name');
			return FALSE;
		}
		else if(preg_match("/[^a-zA-Z]/", $distr_cnt_person))//([!@^&*_\-{}`~<>,\.?%$#\*]\+)
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Please enter valid name' );
			return FALSE;
		}
		$GLOBALS['distr_contact_person']=(null!=trim($this->input->post('DistributorContactPerson'))?trim($this->input->post('DistributorContactPerson')):null);
		return true;
	}
	
	
	public function check_contact_designation($distr_cnt_des)
	{
		$GLOBALS['distr_contact_designation']=null;
		$distr_cnt_des=str_replace(" ", "", $distr_cnt_des);
		
		if(preg_match("/[^a-zA-Z]/", $distr_cnt_des))//([!@^&*_\-{}`~<>,\.?%$#\*]\+)
		{
			$this->form_validation->set_message ( 'check_contact_designation', 'Please enter valid designation' );
			return FALSE;
		}
		else if (strlen($distr_cnt_des) < 4 )
		{
			$this->form_validation->set_message ( 'check_contact_designation', 'Should be greater than 3 characters' );
			return FALSE;
		}
		$GLOBALS['distr_contact_designation']=(null!=trim($this->input->post('DistributorContactDesignation'))?trim($this->input->post('DistributorContactDesignation')):null);
		return true;
	}
	
	
	public function check_contact_email($distr_cnt_email)
	{
		$GLOBALS['distr_contact_email']=null;
		if (empty($distr_cnt_email)) //Check whether the Distributor contact person field is empty
		{
			$this->form_validation->set_message('check_contact_email', '%s required');
			return FALSE;
		}
		else if(!filter_var($distr_cnt_email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
// 			$GLOBALS['client_email']=null;
			$this->form_validation->set_message('check_contact_email', 'Invalid email format');
			return FALSE;
		}
// 		else {
// 			foreach ( $GLOBALS ['DistributorList'] as $row ) {
// 				if ($GLOBALS ['distr_id'] == null) {
// 					if (trim ( strtoupper ( $row ['dist_contact_email'] ) ) == strtoupper ( $distr_cnt_email )) // Check whether Distributor name already exist in vts_distributor table
// 					{
// 						$GLOBALS['outcome_with_db_check']="Distributor Email ".'"'.trim($row['dist_contact_email']).'"'." already exist";
// 						$this->form_validation->set_message('check_contact_email', '');
// 						return FALSE;
// 					}
// 				} else {
// 					if (trim ( strtoupper ( $row ['dist_contact_email'] ) ) == strtoupper ( $distr_cnt_email ) && $GLOBALS ['distr_id'] != trim ( $row ['dist_id'] )) // Check whether distributor name already exist in vts_distributor table while update
// 					{
// 						$GLOBALS['outcome_with_db_check']="Distributor ".'"'.trim($row['dist_contact_email']).'"'." already exist";
// 						$this->form_validation->set_message('check_contact_email', '');
// 						return FALSE;
// 					}
// 				}
// 			}
// 		}
		else {
			foreach ( $GLOBALS ['DistributorList'] as $row ) {
			if ($GLOBALS ['distr_id'] == null) {
			if(!$this->common_model->check_user_email($distr_cnt_email))
			{
				$GLOBALS['outcome_with_db_check']="Given email-id already exists.";
				$this->form_validation->set_message('check_contact_email', '');
				return FALSE;
			}
		}else {
			$user_mail = $this->common_model->check_user_email($distr_cnt_email);
			if ($user_mail == strtoupper ( $distr_cnt_email )) // Check whether distributor name already exist in vts_dealer table while update
			{
				if (trim ( strtoupper ( $row ['dist_contact_email'] ) ) == strtoupper ( $distr_cnt_email ) && $GLOBALS ['distr_id'] != trim ( $row ['dist_id'] )) // Check whether distributor name already exist in vts_dealer table while update
				{
			
				$GLOBALS['outcome_with_db_check']="Given email-id already exists.";
				$this->form_validation->set_message('check_contact_email', '');
				return FALSE;
				}
			}
		}	
	}	
		$GLOBALS['distr_contact_email']=(null!=trim($this->input->post('DistributorContactEmail'))?trim($this->input->post('DistributorContactEmail')):null);
		return true;
	}
}
	
	public function check_contact_phone($distr_cnt_phone)
	{
		$GLOBALS['distr_contact_phone']=null;
		if (empty($distr_cnt_phone)) //Check whether the Distributor contact person field is empty
		{
			$this->form_validation->set_message('check_contact_phone', '%s required');
			return FALSE;
		}
		else if (!is_numeric($distr_cnt_phone)) // Check whether the given Distributor name is numeric.
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Invalid Mobile number' );
			return FALSE;
		}
		else if (strlen($distr_cnt_phone) != "10" ) // Check whether the Distributor name should be between 5 - 20 characters
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Please Enter correct Phone number' );
			return FALSE;
		}
		
		$GLOBALS['distr_contact_phone']=(null!=trim($this->input->post('DistributorContactPhone'))?trim($this->input->post('DistributorContactPhone')):null);
		return true;
	}
	
	public function check_distributor_location($distr_lction)
	{
		$GLOBALS['distr_location']=null;
		if(is_numeric($distr_lction))
		{
			$this->form_validation->set_message('check_distributor_location', 'Enter valid Location value');
			return FALSE;
		}
		else if (preg_match('/[^a-zA-Z]/',$distr_lction))
		{
			$this->form_validation->set_message('check_distributor_location','Invalid location value');
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $distr_lction))
		{
			$this->form_validation->set_message('check_distributor_location','Special charecters are not valid');
			return FALSE;
		}
		$GLOBALS['distr_location']=(null!=trim($this->input->post('DistributorLocation'))?trim($this->input->post('DistributorLocation')):null);
		return true;
	}
	
	public function check_distributor_postal_code($distr_pst_cd)
	{
		$GLOBALS['distr_post_code']=null;
		if (!preg_match("/^[0-9]+$/", $distr_pst_cd))
		{
			$this->form_validation->set_message ( 'check_distributor_postal_code', '%s should be a numeric value' );
			return FALSE;
		}
		$GLOBALS['distr_post_code']=(null!=trim($this->input->post('DistributorPostCode'))?trim($this->input->post('DistributorPostCode')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_vehicle_group
	 */
	public function editOrDelete_vts_distributor()
	{
		$posted=$this->input->get_post('dist_id');
		$splitPosted=explode("-", $posted);		//split the strig in to array
		$operation=$splitPosted[0];		//it hold the operation to perform(i.e. edit or delete).
		$DistributorId=$splitPosted[1];		//it hold the distributor id to edit or delete.
	
		$row=$this->distributor_model->get_vts_distributor(null,null,$DistributorId);
		if($operation=="edit")
		{
			$GLOBALS=array('distr_id'=>trim($row['dist_id']),'distr_name'=>trim($row['dist_name']),'distr_address'=>trim($row['dist_address']),'distr_post_code'=>trim($row['dist_post_code']),'distr_contact_person'=>trim($row['dist_contact_person']),'distr_contact_designation'=>trim($row['dist_contact_designation']),'distr_contact_email'=>trim($row['dist_contact_email']),'distr_contact_phone'=>trim($row['dist_contact_phone']),'distr_remarks'=>trim($row['dist_remarks']),'active'=>trim($row['dist_isactive']),'distr_location'=>trim($row['dist_location']));//It used to fill up the textbox in view.
				
		}
		else if($operation=="delete")
		{
			try
			{
				if($this->distributor_model->InsertOrUpdateOrdelete_vts_distributor($DistributorId,null,$operation))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DISTRIBUTOR,
								"Distributor is deleted: Distributor ID=".trim($GLOBALS['distr_id']).", Distributor Name=".trim($GLOBALS['distr_name']).",Distributor Address=".trim($GLOBALS['distr_address']).", Distributor Post Code=".trim($GLOBALS['distr_post_code']).", Distributor Contact Person=".trim($GLOBALS['distr_contact_person']).", Distributor Contact Designation=".trim($GLOBALS['distr_contact_designation']).", Distributor Contact Mail=".trim($GLOBALS['distr_contact_email']).", Distributor Contact Phone=".trim($GLOBALS['distr_contact_phone']).", Distributor Remarks=".trim($GLOBALS['distr_remarks']).", Distributor Is Active=".trim($GLOBALS['active']).", Distributor Location=".trim($GLOBALS['distr_location']));
					$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['dist_name'] ).'"' . " Can't able to Delete(Referred some where else)";
					
						
				}else
					$GLOBALS['outcome']="Distributor ".'"'.trim($row['dist_name']).'"'." deleted";
			}catch (Exception $ex){
				$this->table_pagination();
	
			}
		}
		$this->table_pagination();
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){
		
		$config['base_url'] = base_url("index.php/distributor_ctrl/table_pagination/");
		$GLOBALS['DistributorList']=$this->distributor_model->get_vts_distributor();
		$config['total_rows'] = count($GLOBALS['DistributorList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['DistributorList']=$this->distributor_model->get_vts_distributor(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('distributor_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
}

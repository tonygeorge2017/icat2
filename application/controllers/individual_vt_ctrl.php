<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Individual_vt_ctrl extends CI_Controller {
	/*
	 * class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->common_model->check_session();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS['sessClientID']=$GLOBALS['ID']['sess_clientid'];
		$GLOBALS['sessUserID']=$GLOBALS['ID']['sess_userid'];
		$GLOBALS['sessUserType']=$GLOBALS['ID']['sess_user_type'];	
		$this->load->model('individual_vt_model');
		$GLOBALS['userList']=$this->individual_vt_model->get_allIndividualUsers($GLOBALS['sessClientID'],$GLOBALS['sessUserID']);
		$row=$GLOBALS['userList'];
		$GLOBALS['vehicleList']=null;
		if($GLOBALS['userList']!=null)
		{
			$GLOBALS['vehicleList']=$this->individual_vt_model->get_all_user_vehicle($row[0]['id']);
		}
	}
	/*
	 * This function call automatically when call this ctrl
	 */
	public function index()
	{
		$userdate=$this->common_model->get_usertime();
		$date_value=new DateTime($userdate);
		$datevalue=$date_value->format('Y-m-d');
		$GLOBALS['startDate']=new DateTime($datevalue.' 00:00:00');
		$GLOBALS['endDate']=new DateTime($datevalue.' 23:59:59');
		$this->display();
	}
	/*
	 * This function is used to fetch gps data from databse
	 * @param
	 *  $vh_id - vehicle id
	 *  $_fdate - from date
	 *  $_tdate - to date
	 *  $track_type - track type(i.e live track or tack history)
	 *  $userID - user id
	 * Return type - json string
	 */
	public function fetch_gps_data($vh_id,$_fdate,$_tdate,$track_type,$userID,$step_by_step=null)
	{
		$get_in=false;
		$diff=$GLOBALS['ID']['sess_time_zonediff'];
		$fdate = new DateTime($_fdate); $tdate = new DateTime($_tdate);
		$check_diff=date_diff($fdate,$tdate);
		$diff_in_str=$check_diff->format("%R%a days");		
		$fdate_rtn = new DateTime($_fdate); $tdate_rtn = new DateTime($_tdate);
		$fdate_rtn=$fdate_rtn->format('Y-m-d H:i:s');
		$tdate_rtn=$tdate_rtn->format('Y-m-d H:i:s');
		$outp='[{"lat":"Na","lng":"Na","speed":"Na","fdateTime":"'.$fdate_rtn.'","tdateTime":"'.$tdate_rtn.'"}]';
		if($step_by_step==null)
			$get_in=true;
		else if($diff_in_str > 0)
		{
			$get_in=false;
			$outp='[{"lat":"Na","lng":"TimeOutRange","speed":"Na","fdateTime":"'.$fdate_rtn.'","tdateTime":"'.$tdate_rtn.'"}]';
		}
		else
			$get_in=true;
		if($get_in)
		{
			$fdate=$this->common_model->get_dateTime($fdate->format('Y-m-d H:i:s'),'1');
			$tdate=$this->common_model->get_dateTime($tdate->format('Y-m-d H:i:s'),'1');
			if($track_type=="live" && $vh_id==0)//For all vehicle live gps current position data
			{
				$gps_result=$this->individual_vt_model->get_all_live_vehicle_data($userID);
			}
			else if($track_type=="live" && $vh_id!=0)//For all vehicle live gps current position data
			{
				$gps_result=$this->individual_vt_model->get_all_live_vehicle_data($userID,$vh_id);
			}
			else
			{
				$gps_result=$this->individual_vt_model->get_gps_data($vh_id,$fdate,$tdate);
			}			
			if($gps_result!=null)
			{
				$outp = json_encode($gps_result);
			}
		}
		echo($outp);
	}
	/*
	 * This function is used to get the vehicle for the selected vehicle group and return back the list of
	 * vehicles in JSON format with out refresh the page.
	 * @param
	 *  $URLuserID - user id
	 * Return type -  JSON string
	 */
	public function get_individual_user_vehicle($URLuserID)
	{	
		$vhl_grp_vehicle='';
		if($URLuserID!=null)
		{
			$vhl_grp_vehicle=json_encode($this->individual_vt_model->get_all_user_vehicle($URLuserID));
		}
		echo($vhl_grp_vehicle);
	}
	/*
	 * This function is usde to respond the from date and to date to the user for the selected time period
	 * @param
	 *  $timeSelection - selected time period(today, yesterday, last week)
	 * Return type -  JSON string
	 */
	public function get_time($timeSelection)
	{
		$userdate=$this->common_model->get_usertime();
		$date_value=new DateTime($userdate);
		$datevalue=$date_value->format('Y-m-d');
		$datevalue=$date_value->format('Y-m-d');
		$fdate = new DateTime($datevalue.' 00:00:00'); $tdate = new DateTime($datevalue.' 23:59:59');
		if($timeSelection=='yesterday')
		{
			$fdate=$fdate->modify("-1 days");
			$tdate=$tdate->modify("-1 days");
		}
		else if($timeSelection=='lastweek')
		{
			$fdate=$fdate->modify("-7 days");
			$tdate=$tdate->modify("-1 days");
		}
		$outp='[{"FromDate":"'.$fdate->format('Y-m-d').'T'.$fdate->format('H:i').'","ToDate":"'.$tdate->format('Y-m-d').'T'.$tdate->format('H:i').'"}]';
		echo($outp);
	}
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$GLOBALS['sessionData'] = $this->session->userdata('login');
		if($GLOBALS['sessionData']!=null)
		{
			$this->common_model->menu_display();
			$this->load->view('header_footer/header_individual_track',$GLOBALS);
			$this->load->view('individual_vt_view',$GLOBALS);
		}else
		{
			$this->session->unset_userdata ( 'login' );
			$this->session->sess_destroy ();
			redirect ( 'login_ctrl', 'refresh' );
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealer_ctrl extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');	
		$this->load->model('common_model');
		$this->common_model->check_session();
		$this->load->model('dealer_model');
		$this->load->helper(array('form', 'url'));
		
		//the below data will save to the vts_dealer table
		$GLOBALS['dler_id']=null;
		$GLOBALS['dler_name']=null;
		$GLOBALS['dler_address']=null;
		$GLOBALS['dler_post_code']=null;
		$GLOBALS['dler_contact_person']=null;
		$GLOBALS['dler_contact_designation']=null;
		$GLOBALS['dler_contact_email']=null;
		$GLOBALS['dler_contact_phone']=null;
		$GLOBALS['dler_remarks']=null;
		$GLOBALS['active']=NOT_ACTIVE;
		$GLOBALS['dler_location']=null;
		$GLOBALS['dealer_distr_id']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['user_type_id']= DEALER_USER;// dealer user id is 5 which is defined in constants.
		//log_message("debug","--dealer user---".$GLOBALS['user_type_id']);
		
		$GLOBALS['outcome']=null;
		$GLOBALS['outcome_with_db_check']=null;
		$GLOBALS['pageLink']=null;
		$GLOBALS['eventLogRequired']=$this->common_model->get_setting_value("ActivityLoggingRequired");//whether event log is required or not(i.e. 'N' not required, 'Y' required)
		

		$GLOBALS['DealerList']=$this->dealer_model->get_vts_dealer();
		$GLOBALS['ID'] = $this->session->userdata('login');
		$GLOBALS ['timezone_id'] = $GLOBALS ['ID'] ['sess_time_zoneid'];
		$GLOBALS ['timezone'] = $GLOBALS ['ID'] ['sess_time_zonename'];
		//to get user ip and host name
		$host_name = exec("hostname"); //to get "hostname"
		$host_name = trim($host_name); //remove any spaces before and after
		$ip = gethostbyname($host_name);
		$GLOBALS['ip']= $host_name."[".$ip."]";
		
// 		$GLOBALS['ID'] = $this->session->userdata('login');
// 		//log_message('error','********'.$GLOBALS['ID']['sess_userid'].','.$GLOBALS['ID']['sess_clientid']);
// 		$GLOBALS['vcle_group_client_id']=$GLOBALS['ID']['sess_clientid'];
	}

	public function index()
	{
		$this->table_pagination();
		//$this->display();
	}
	
	/*
	 * This function is used to validate and add the new vts_dealer in database.
	 */
	public function vts_dealer_validation()
	{
		$GLOBALS['dler_id']=(null!=($this->input->post('DealerId'))?$this->input->post('DealerId'):null);
		$GLOBALS['dler_name']=(null!=trim($this->input->post('DealerName'))?trim($this->input->post('DealerName')):null);
		$GLOBALS['dler_address']=(null!=trim($this->input->post('DealerAddress'))?trim($this->input->post('DealerAddress')):null);
		$GLOBALS['dler_post_code']=(null!=trim($this->input->post('DealerPostCode'))?trim($this->input->post('DealerPostCode')):null);
		$GLOBALS['dler_contact_person']=(null!=trim($this->input->post('DealerContactPerson'))?trim($this->input->post('DealerContactPerson')):null);
		$GLOBALS['dler_contact_designation']=(null!=trim($this->input->post('DealerContactDesignation'))?trim($this->input->post('DealerContactDesignation')):null);
		$GLOBALS['dler_contact_email']=(null!=trim($this->input->post('DealerContactEmail'))?trim($this->input->post('DealerContactEmail')):null);
		$GLOBALS['dler_contact_phone']=(null!=trim($this->input->post('DealerContactPhone'))?trim($this->input->post('DealerContactPhone')):null);
		$GLOBALS['dler_remarks']=(null!=trim($this->input->post('DealerRemarks'))?trim($this->input->post('DealerRemarks')):null);
		$GLOBALS['active']=(null!=($this->input->post('DealerIsActive'))?$this->input->post('DealerIsActive'):NOT_ACTIVE);
		$GLOBALS['dler_location']=(null!=trim($this->input->post('DealerLocation'))?trim($this->input->post('DealerLocation')):null);
		$GLOBALS['dealer_distr_id']=(null!=trim($this->input->post('DistributorId'))?trim($this->input->post('DistributorId')):null);
		
// 		$GLOBALS['vcle_group_client_id']=$GLOBALS['ID']['sess_clientid'];
		
		$this->form_validation->set_message('required', '%s required');//this will help to the change the message to display 'required' form validation rule.
	
		$this->form_validation->set_rules('DealerName', 'Dealer Name', 'callback_check_dealer_name');
		if(isset($GLOBALS['dler_location']))
		{
			$this->form_validation->set_rules('DealerLocation','Dealer Location','callback_check_dealer_location');
		}
		if(isset($GLOBALS['dler_post_code']))
		{
			$this->form_validation->set_rules('DealerPostCode','Postal Code','callback_check_dealer_postal_code');			
		}
		
		$this->form_validation->set_rules('DealerContactPerson', 'Dealer Contact Person', 'callback_check_contact_person');
		if(isset($GLOBALS['dler_contact_designation']))
		{
			$this->form_validation->set_rules('DealerContactDesignation','DealerContactDesignation','callback_check_contact_designation');			
		}
		$this->form_validation->set_rules('DealerContactEmail', 'Dealer Contact Email', 'callback_check_contact_email');
		$this->form_validation->set_rules('DealerContactPhone', 'Dealer Contact Phone', 'callback_check_contact_phone');
		$this->form_validation->set_rules('DistributorId', 'Distributor Name', 'callback_check_distributor_id');

		if ($this->form_validation->run() == FALSE) // if any of the form rule is failed, then it show the error msg in view. Else update the new Vehicle Group in vts_vehicle_group table.
		{
			$this->table_pagination();
		}
		else
		{
			$data['dealer_name']=$GLOBALS['dler_name'];
			$data['dealer_address']=$GLOBALS['dler_address'];
			$data['dealer_post_code']=$GLOBALS['dler_post_code'];
			$data['dealer_contact_person']=$GLOBALS['dler_contact_person'];
			$data['dealer_contact_designation']=$GLOBALS['dler_contact_designation'];
			$data['dealer_contact_email']=$GLOBALS['dler_contact_email'];
			$data['dealer_contact_phone']=$GLOBALS['dler_contact_phone'];
			$data['dealer_remarks']=$GLOBALS['dler_remarks'];
			$data['dealer_isactive']=$GLOBALS['active'];
			$data['dealer_location']=$GLOBALS['dler_location'];
			$data['dealer_distributor_id']=$GLOBALS['dealer_distr_id'];
			
			
			if($GLOBALS['dler_id']!=null)	//edit
			{
				$data['dealer_id']=$GLOBALS['dler_id'];
				if ($this->dealer_model->InsertOrUpdateOrdelete_vts_dealer($GLOBALS['dler_id'],$data,'edit'))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEALER,
								"Updated Dealer: Dealer ID=".trim($GLOBALS['dler_id']).", Dealer Name=".trim($GLOBALS['dler_name']).",Dealer Address=".trim($GLOBALS['dler_address']).", Dealer Post Code=".trim($GLOBALS['dler_post_code']).", Dealer Contact Person=".trim($GLOBALS['dler_contact_person']).", Dealer Contact Designation=".trim($GLOBALS['dler_contact_designation']).", Dealer Contact Mail=".trim($GLOBALS['dler_contact_email']).", Dealer Contact Phone=".trim($GLOBALS['dler_contact_phone']).", Dealer Remarks=".trim($GLOBALS['dler_remarks']).", Dealer Is Active=".trim($GLOBALS['active']).", Dealer Location=".trim($GLOBALS['dler_location']).", Dealer Distributor Id=".trim($GLOBALS['dealer_distr_id']));
					
// 					if($this->dealer_model->insert_user_value($GLOBALS['dler_id'],$data['dealer_distributor_id'], $data['dealer_contact_person'], $data['dealer_contact_email'], $data['dealer_isactive'],$GLOBALS ['timezone_id'], $data['dealer_contact_phone'],$GLOBALS['user_type_id'], 'edit'))
// 					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEALER,
									"Update User:"." User Dealer Id=".trim($GLOBALS['dler_id']).",User Name=".trim($data['dealer_contact_person']).", Contact Email=".trim($data['dealer_contact_email']).",User Is Active=".trim($data['dealer_isactive']));
						$GLOBALS['outcome']="Dealer ".'"'.trim($GLOBALS['dler_name']) .'"'."updated successfully";
						$GLOBALS['dler_id']=null;
						$GLOBALS['active']=null;
						$GLOBALS['dealer_distr_id']=null;
					//}
				}
			}
			else	//insert
			{
				if($this->dealer_model->InsertOrUpdateOrdelete_vts_dealer(null,$data,'insert'))
				{
					$dealer_id = $this->dealer_model->get_max_dealer_id();
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEALER,
								"Created New Dealer: Dealer Name=".trim($GLOBALS['dler_name']).",Dealer Address=".trim($GLOBALS['dler_address']).", Dealer Post Code=".trim($GLOBALS['dler_post_code']).", Dealer Contact Person=".trim($GLOBALS['dler_contact_person']).", Dealer Contact Designation=".trim($GLOBALS['dler_contact_designation']).", Dealer Contact Mail=".trim($GLOBALS['dler_contact_email']).", Dealer Contact Phone=".trim($GLOBALS['dler_contact_phone']).", Dealer Remarks=".trim($GLOBALS['dler_remarks']).", Dealer Is Active=".trim($GLOBALS['active']).", Dealer Location=".trim($GLOBALS['dler_location']).", Dealer Distributor Id=".trim($GLOBALS['dealer_distr_id']));
					
					if($this->dealer_model->insert_user_value($dealer_id,$data['dealer_distributor_id'], $data['dealer_contact_person'], $data['dealer_contact_email'], $data['dealer_isactive'],$GLOBALS ['timezone_id'], $data['dealer_contact_phone'], $GLOBALS['user_type_id'],'insert'))
					{
						if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
							$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEALER,
									"Create New User:"." User Dealer ID=".trim($dealer_id).",User Name=".trim($data['dealer_contact_person']).", Contact Email=".trim($data['dealer_contact_email']).",User Is Active=".trim($data['dealer_isactive']));
					
						$users_id = $this->dealer_model->get_user_id(trim($data['dealer_contact_email']));
					//log_message("debug","---success---inserted");
						$s_msg='Dear '.$data['dealer_contact_person'].',<br><br>&nbsp; Your email has been registered as a New Login in the Autograde T.A.N.K. System.<br>&nbsp;&nbsp;Please click the below link to set your new password and to start using the system :<br>&nbsp;&nbsp;<a href='.base_url("index.php/change_password_ctrl/view/".md5($users_id)).'>Click Here</a><br><br>Thanks and Regards,<br>Autograde T.A.N.K. Support.';
						$this->load->library ( 'commonfunction' );
						$this->commonfunction->send_mail($GLOBALS['dler_contact_email'],$data['dealer_contact_person'],CC_TO,BCC_TO,'Vehicle Tracking System : New User Registered',$s_msg, REPLY_TO,'VTS Support');
	
							$GLOBALS['outcome']="New Dealer ".'"'.trim($GLOBALS['dler_name']).'"'." created successfully and mail has been sent to given Mail ID.";
							$GLOBALS['dler_id']=null;
							$GLOBALS['active']=null;
							$GLOBALS['dealer_distr_id']=null;
					}

				}
			}
			$this->table_pagination();
		}
	}
	
	/*
	 * This function is the callback function of form validation, it is used to validate the vts_dealer name
	 */
	public function check_dealer_name($dler_nm)
	{
		$GLOBALS['dler_name']=null;
		if (empty($dler_nm)) //Check whether the Dealer name field is empty
		{
			$this->form_validation->set_message('check_dealer_name', '%s required');
			return FALSE;
		}
		else if (is_numeric($dler_nm)) // Check whether the given Dealer name is numeric.
		{
			$this->form_validation->set_message ( 'check_dealer_name', '%s should not be numeric' );
			return FALSE;
		}
		else if (strlen($dler_nm) < MIN_FIELD_LENGTH || strlen($dler_nm) > MAX_FIELD_LENGTH ) // Check whether the Dealer name should be between 5 - 20 characters
		{
			$this->form_validation->set_message ( 'check_dealer_name', 'Dealer name should be in between 5-20 characters' );
			return FALSE;
		}
		else {
			foreach ( $GLOBALS ['DealerList'] as $row ) {
				if ($GLOBALS ['dler_id'] == null) {
					if (trim ( strtoupper ( $row ['dealer_name'] ) ) == strtoupper ( $dler_nm )) // Check whether Dealer name already exist in vts_dealer table
					{
						$GLOBALS['outcome_with_db_check']="Dealer ".'"'.trim($row['dealer_name']).'"'." already exist";
						$this->form_validation->set_message('check_dealer_name', '');
						return FALSE;
					}
				} else {
					if (trim ( strtoupper ( $row ['dealer_name'] ) ) == strtoupper ( $dler_nm ) && $GLOBALS ['dler_id'] != trim ( $row ['dealer_id'] )) // Check whether dealer name already exist in vts_dealer table while update
					{
						$GLOBALS['outcome_with_db_check']="Dealer ".'"'.trim($row['dealer_name']).'"'." already exist";
						$this->form_validation->set_message('check_dealer_name', '');
						return FALSE;
					}
				}
			}
		}
		
		$GLOBALS['dler_name']=(null!=trim($this->input->post('DealerName'))?trim($this->input->post('DealerName')):null);
		return true;
	}
	
	public function check_contact_person($dler_cnt_person)
	{
		$GLOBALS['dler_contact_person']=null;
		if (empty($dler_cnt_person)) //Check whether the Dealer contact person field is empty
		{
			$this->form_validation->set_message('check_contact_person', '%s required');
			return FALSE;
		}
		else if (is_numeric($dler_cnt_person)) // Check whether the given Dealer name is numeric.
		{
			$this->form_validation->set_message ( 'check_contact_person', '%s should not be numeric' );
			return FALSE;
		}
		else if (preg_match("/([%\$#\/*@&<>?.,=])/", $dler_cnt_person))
		{
			$this->form_validation->set_message('check_contact_person','Special charecters are not allowed');
			return FALSE;
		}
// 		else if(!preg_match('/[a-zA-Z]/',$dler_cnt_person))
// 		{
// 			$this->form_validation->set_message('check_contact_person','Invalid contact name');
// 			return FALSE;
// 		}
		else if(!preg_match("/^[A-Za-z ]+$/i", $dler_cnt_person))
		{
			$this->form_validation->set_message ( 'check_contact_person', 'Please enter valid name' );
			return FALSE;
		}
		$GLOBALS['dler_contact_person']=(null!=trim($this->input->post('DealerContactPerson'))?trim($this->input->post('DealerContactPerson')):null);
		return true;
	}
	
	
	public function check_contact_designation($dler_cnt_des)
	{
		$GLOBALS['dler_contact_designation']=null;
		$dler_cnt_des=str_replace(" ", "", $dler_cnt_des);
		
		if(preg_match("/[^a-zA-Z]/", $dler_cnt_des))//([!@^&*_\-{}`~<>,\.?%$#\*]\+)
		{
			$this->form_validation->set_message ( 'check_contact_designation', 'Please enter valid designation' );
			return FALSE;
		}
		else if (strlen($dler_cnt_des) < 4 )
		{
			$this->form_validation->set_message ( 'check_contact_designation', 'Should be greater than 3 characters' );
			return FALSE;
		}
		$GLOBALS['dler_contact_designation']=(null!=trim($this->input->post('DealerContactDesignation'))?trim($this->input->post('DealerContactDesignation')):null);
		return true;
	}
	
	
	public function check_contact_email($dler_cnt_email)
	{
		$GLOBALS['dler_contact_email']=null;
		if (empty($dler_cnt_email)) //Check whether the Dealer contact person field is empty
		{
			$this->form_validation->set_message('check_contact_email', '%s required');
			return FALSE;
		}
		else if(!filter_var($dler_cnt_email, FILTER_VALIDATE_EMAIL))//Check whether the given value is valid email or not.
		{
// 			$GLOBALS['client_email']=null;
			$this->form_validation->set_message('check_contact_email', 'Invalid email format');
			return FALSE;
		}
		else {
// 			foreach ( $GLOBALS ['DealerList'] as $row ) {
// 				if ($GLOBALS ['dler_id'] == null) {
// 					if (trim ( strtoupper ( $row ['dealer_contact_email'] ) ) == strtoupper ( $dler_cnt_email )) // Check whether Dealer name already exist in vts_dealer table
// 					{
// 						$GLOBALS['outcome_with_db_check']="Dealer Email ".'"'.trim($row['dealer_contact_email']).'"'." already Exist";
// 						$this->form_validation->set_message('check_contact_email', '');
// 						return FALSE;
// 					}
// 				} else {
// 					if (trim ( strtoupper ( $row ['dealer_contact_email'] ) ) == strtoupper ( $dler_cnt_email ) && $GLOBALS ['dler_id'] != trim ( $row ['dealer_id'] )) // Check whether dealer name already exist in vts_dealer table while update
// 					{
// 						$GLOBALS['outcome_with_db_check']="Dealer ".'"'.trim($row['dealer_contact_email']).'"'." already Exist";
// 						$this->form_validation->set_message('check_contact_email', '');
// 						return FALSE;
// 					}
// 				}
// 			}
			foreach ( $GLOBALS ['DealerList'] as $row ) {
			if ($GLOBALS ['dler_id'] == null) {
				if(!$this->common_model->check_user_email($dler_cnt_email))
				{
				$GLOBALS['outcome_with_db_check']="Given email-id already exists";
				$this->form_validation->set_message('check_contact_email', '');
				return FALSE;
				}
			}
			else {
				$user_mail = $this->common_model->check_user_email($dler_cnt_email);
				//log_message("debug","----testing-------".print_r($user_mail,true)."-------view mail---".$dler_cnt_email);
				if ($user_mail == strtoupper ( $dler_cnt_email )) // Check whether dealer name already exist in vts_dealer table while update
				{
					if (trim ( strtoupper ( $row ['dealer_contact_email'] ) ) == strtoupper ( $dler_cnt_email ) && $GLOBALS ['dler_id'] != trim ( $row ['dealer_id'] )) // Check whether dealer name already exist in vts_dealer table while update
					 {
				//log_message("debug","----testing-----if--".print_r($user_mail,true));
				$GLOBALS['outcome_with_db_check']="Given email-id already exists";
				$this->form_validation->set_message('check_contact_email', '');
				return FALSE;
					}
				}
			}
		}
			
		$GLOBALS['dler_contact_email']=(null!=trim($this->input->post('DealerContactEmail'))?trim($this->input->post('DealerContactEmail')):null);
		return true;
	}
	}
	
	
	public function check_contact_phone($dler_cnt_phone)
	{
		$GLOBALS['dler_contact_phone']=null;
		if (empty($dler_cnt_phone)) //Check whether the Dealer contact person field is empty
		{
			$this->form_validation->set_message('check_contact_phone', '%s required');
			return FALSE;
		}
		else if (!is_numeric($dler_cnt_phone)) // Check whether the given Dealer name is numeric.
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Invalid Mobile number' );
			return FALSE;
		}
		else if (strlen($dler_cnt_phone) != "10" ) // Check whether the Dealer name should be between 5 - 20 characters
		{
			$this->form_validation->set_message ( 'check_contact_phone', 'Please Enter correct Phone number' );
			return FALSE;
		}
		
		$GLOBALS['dler_contact_phone']=(null!=trim($this->input->post('DealerContactPhone'))?trim($this->input->post('DealerContactPhone')):null);
		return true;
	}
	
	public function check_dealer_location($dler_lction)
	{
		$GLOBALS['dler_location']=null;
		if(is_numeric($dler_lction))
		{
			$this->form_validation->set_message('check_dealer_location', 'Enter valid Location value');
			return FALSE;
		}
		else if (preg_match('/[^a-zA-Z]/',$dler_lction))
		{
			$this->form_validation->set_message('check_dealer_location','Invalid location value');
			return FALSE;
		}
		else if(preg_match("/([%\$#\/*@&<>?.,=])/", $dler_lction))
		{
			$this->form_validation->set_message('check_dealer_location','Special charecters are not valid');
			return FALSE;
		}
		$GLOBALS['dler_location']=(null!=trim($this->input->post('DealerLocation'))?trim($this->input->post('DealerLocation')):null);
		return true;
	}
	
	public function check_dealer_postal_code($dler_pst_cd)
	{
		$GLOBALS['dler_post_code']=null;
		if (!preg_match("/^[0-9]+$/", $dler_pst_cd))
		{
			$this->form_validation->set_message ( 'check_dealer_postal_code', '%s should be a numeric value' );
			return FALSE;
		}
		$GLOBALS['dler_post_code']=(null!=trim($this->input->post('DealerPostCode'))?trim($this->input->post('DealerPostCode')):null);
		return true;
	}
	
	public function check_distributor_id($dis_id)
	{
		$GLOBALS['dealer_distr_id'] = null;
		if (empty($dis_id))
		{
			$this->form_validation->set_message('check_distributor_id', '%s required');
			return FALSE;
		}
		$GLOBALS['dealer_distr_id']=(null!=trim($this->input->post('DistributorId'))?trim($this->input->post('DistributorId')):null);
		return true;
	}
	
	/*
	 * This function used to edit or delete the vts_vehicle_group
	 */
	public function editOrDelete_vts_dealer()
	{
		$posted=$this->input->get_post('dealer_id');
		$splitPosted=explode("-", $posted);	//split the strig in to array
		$operation=$splitPosted[0];			//it hold the operation to perform(i.e. edit or delete).
		$DealerId=$splitPosted[1];			//it hold the dealer id to edit or delete.
	
		$row=$this->dealer_model->get_vts_dealer(null,null,$DealerId);
		if($operation=="edit")
		{
			$GLOBALS=array('dler_id'=>trim($row['dealer_id']),'dler_name'=>trim($row['dealer_name']),'dler_address'=>trim($row['dealer_address']),'dler_post_code'=>trim($row['dealer_post_code']),'dler_contact_person'=>trim($row['dealer_contact_person']),'dler_contact_designation'=>trim($row['dealer_contact_designation']),'dler_contact_email'=>trim($row['dealer_contact_email']),'dler_contact_phone'=>trim($row['dealer_contact_phone']),'dler_remarks'=>trim($row['dealer_remarks']),'active'=>trim($row['dealer_isactive']),'dler_location'=>trim($row['dealer_location']), 'dealer_distr_id'=>trim($row['dealer_distributor_id']));//It used to fill up the textbox in view.
				
		}
		else if($operation=="delete")
		{
			try
			{
				if($this->dealer_model->InsertOrUpdateOrdelete_vts_dealer($DealerId,null,$operation))
				{
					if(trim($GLOBALS['eventLogRequired'])==REQUIRED)	// parameters are user id, ip, screen id, event description
						$this->common_model->insert_event_value($GLOBALS['ID']['sess_userid'], $GLOBALS['ip'], DEALER,
								"Dealer is deleted: Dealer ID=".trim($GLOBALS['dler_id']).", Dealer Name=".trim($GLOBALS['dler_name']).",Dealer Address=".trim($GLOBALS['dler_address']).", Dealer Post Code=".trim($GLOBALS['dler_post_code']).", Dealer Contact Person=".trim($GLOBALS['dler_contact_person']).", Dealer Contact Designation=".trim($GLOBALS['dler_contact_designation']).", Dealer Contact Mail=".trim($GLOBALS['dler_contact_email']).", Dealer Contact Phone=".trim($GLOBALS['dler_contact_phone']).", Dealer Remarks=".trim($GLOBALS['dler_remarks']).", Dealer Is Active=".trim($GLOBALS['active']).", Dealer Location=".trim($GLOBALS['dler_location']).", Dealer Distributor Id=".trim($GLOBALS['dealer_distr_id']));
					
						
						$GLOBALS ['outcome_with_db_check'] = '"'.trim ( $row ['dealer_name'] ).'"' . " Can't delete(Referred some where else)";
				}else {
					$GLOBALS['outcome']="Dealer ".'"'.trim($row['dealer_name']).'"'." deleted";
				}
			}catch (Exception $ex){
				$this->table_pagination();
	
			}
		}
		$this->table_pagination();
	}
	
	/*
	 * This function is used for pagination
	 */
	public function table_pagination($pageNo=0){
		
		$config['base_url'] = base_url("index.php/dealer_ctrl/table_pagination/");
		$GLOBALS['DealerList']=$this->dealer_model->get_vts_dealer();
		$GLOBALS['distributorList']=$this->dealer_model->get_allDistributorNames();
		$config['total_rows'] = count($GLOBALS['DealerList']);
		$config['per_page']=ROW_PER_PAGE;
		$GLOBALS['DealerList']=$this->dealer_model->get_vts_dealer(ROW_PER_PAGE,$pageNo,null);
		$this->pagination->initialize($config);
		$GLOBALS['pageLink']= $this->pagination->create_links();
		$this->display();
	}
	
	/*
	 * This function is used to render the view
	 */
	private function display()
	{
		$this->common_model->menu_display();
		$this->load->view('header_footer/header',$GLOBALS);
		$this->load->view('dealer_view',$GLOBALS);
		$this->load->view('header_footer/footer');
	}
	
	
	
}
